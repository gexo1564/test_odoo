from odoo import models, fields,api,_
from odoo.exceptions import ValidationError

class DepositCertificateType(models.Model):

    _inherit = 'deposit.certificate.type'

    current_account_id = fields.Many2one("account.account", "Current Account")
    previous_account_id = fields.Many2one("account.account", "Previous Account")