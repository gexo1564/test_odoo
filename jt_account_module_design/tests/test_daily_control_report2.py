from odoo.tests import TransactionCase
from odoo.tests import tagged
from unittest.mock import patch, MagicMock
from psycopg2 import sql

TEST_INYECCION_SQL = 'SELECT id FROM productive_account where id = 1"'
FAILED_QUERY_MESSAGE = "La consulta query no se ejecutó."
STR_BANK = MagicMock(get_list_str = (2, 19, 24))
BANCOS_ORIGEN = STR_BANK
BANCOS_DESTINO = STR_BANK
STR_ACC = MagicMock(get_list_str = (330, 599))
ACC_ORIGEN = STR_ACC
ACC_DESTINO = STR_ACC
FOLIOS = MagicMock(get_list_str = (13, 14, 15))
STR_TRANS = MagicMock(get_list_str = ('investments', 'projects','finances'))
TRANSFER = STR_TRANS
START_DATE = '2023-01-01'
END_DATE = '2023-07-06'
@tagged('jt_account_module_design')
class GeneratePayrollWizard(TransactionCase):
    def test__search_info_query1(self):
        # Intento de inyección
        END_DATE += TEST_INYECCION_SQL
           
        current_query = """
            SELECT DISTINCT robf.id, robf.operation_number FROM request_open_balance_finance robf, account_payment ap, rel_req_payment rrp WHERE 
            ((robf.date_required >= %(start)s AND robf.date_required <= %(end)s) OR (robf.date >= %(start)s AND robf.date <= %(end)s) OR 
            robf.id=rrp.payment_id AND rrp.payment_request_rel_id = ap.id AND( ap.payment_date >= %(start)s AND ap.payment_date <= %(end)s )) AND 
            robf.state = 'confirmed' ORDER BY robf.operation_number ASC;
        """
        params = {'start': START_DATE,
                  'end': END_DATE}
        # Simular la ejecución de la consulta
        with patch.object(self.env.cr, 'execute') as mock_execute:
            # Ejecutar la prueba
            self.env.cr.execute(current_query, params)
            # Asegurar que la consulta se haya ejecutado al menos una vez
            self.assertTrue(mock_execute.called, "La consulta current_query no se ejecutó.")
            print(F"PRUEBA DE QUERY SANITIZADO 1 test__search_info_query1\n{current_query}")


    def test__search_info_query2(self):
        # Intento de inyección
        END_DATE += TEST_INYECCION_SQL
           
        query = sql.SQL("""SELECT DISTINCT robf.id, robf.operation_number 
            FROM request_open_balance_finance robf, account_payment ap, rel_req_payment rrp, account_journal aj, res_partner_bank rpb, res_bank rb 
            WHERE ((robf.date_required >= %(start)s 
            AND robf.date_required <= %(end)s) OR (robf.date >= %(start)s 
            AND robf.date <= %(end)s) OR robf.id=rrp.payment_id 
            AND rrp.payment_request_rel_id = ap.id 
            AND( ap.payment_date >= %(start)s AND ap.payment_date <= %(end)s )) 
            AND  ((robf.bank_account_id = aj.id 
            AND aj.bank_account_id = rpb.id 
            AND rpb.bank_id = rb.id AND rb.id in {})OR(robf.desti_bank_account_id = aj.id AND 
            aj.bank_account_id = rpb.id AND rpb.bank_id = rb.id AND rb.id in {})) AND robf.state = 'confirmed' 
            ORDER BY robf.operation_number ASC;""").format(
            sql.Identifier(BANCOS_ORIGEN), 
            sql.Identifier(BANCOS_DESTINO)
            )
        
        params = {'start': START_DATE,
                  'end': END_DATE,
                  }
        # Simular la ejecución de la consulta
        with patch.object(self.env.cr, 'execute') as mock_execute:
            # Ejecutar la prueba
            self.env.cr.execute(query, params)
            # Asegurar que la consulta se haya ejecutado al menos una vez
            self.assertTrue(mock_execute.called, FAILED_QUERY_MESSAGE)
            print(F"PRUEBA DE QUERY SANITIZADO 1 test__search_info_query2\n{query}")


    def test__search_info_query3(self):
        # Intento de inyección
        END_DATE += TEST_INYECCION_SQL
           
        query= sql.SQL(""""
            SELECT DISTINCT robf.id, robf.operation_number 
            FROM request_open_balance_finance robf, 
            account_payment ap, rel_req_payment rrp, account_journal aj, res_partner_bank rpb, res_bank rb WHERE 
            ((robf.date_required >= %(start)s AND robf.date_required <= %(end)s) OR (robf.date >= %(start)s AND robf.date <= %(end)s) OR 
            robf.id=rrp.payment_id AND rrp.payment_request_rel_id = ap.id AND( ap.payment_date >= %(start)s AND ap.payment_date <= %(end)s )) AND  
            ((robf.bank_account_id = aj.id AND aj.bank_account_id = rpb.id AND rpb.bank_id = rb.id AND rb.id in {})OR(robf.desti_bank_account_id = aj.id 
            AND aj.bank_account_id = rpb.id AND rpb.bank_id = rb.id AND rb.id in {})) 
            AND (robf.id in {}) AND robf.state = 'confirmed' ORDER BY robf.operation_number ASC; """).format(
            sql.Identifier(BANCOS_ORIGEN), 
            sql.Identifier(BANCOS_DESTINO),
            sql.Identifier(FOLIOS)
            )

        params = {'start': START_DATE,
                  'end': END_DATE,
                  }
        # Simular la ejecución de la consulta
        with patch.object(self.env.cr, 'execute') as mock_execute:
            # Ejecutar la prueba
            self.env.cr.execute(query, params)
            # Asegurar que la consulta se haya ejecutado al menos una vez
            self.assertTrue(mock_execute.called, FAILED_QUERY_MESSAGE)
            print(F"PRUEBA DE QUERY SANITIZADO 3 test__search_info_query3\n{query}")


    def test__search_info_query4(self):
        # Intento de inyección
        END_DATE += TEST_INYECCION_SQL
           
        query= sql.SQL(""""
            SELECT DISTINCT robf.id, robf.operation_number 
            FROM request_open_balance_finance robf, account_payment ap, rel_req_payment rrp, account_journal aj, res_partner_bank rpb, res_bank rb 
            WHERE ((robf.date_required >= %(start)s 
            AND robf.date_required <= %(end)s) OR (robf.date >= %(start)s 
            AND robf.date <= %(end)s) OR robf.id=rrp.payment_id 
            AND rrp.payment_request_rel_id = ap.id 
            AND( ap.payment_date >= %(start)s 
            AND ap.payment_date <= %(end)s )) 
            AND  ((robf.bank_account_id = aj.id 
            AND aj.bank_account_id = rpb.id 
            AND rpb.bank_id = rb.id 
            AND rb.id in {}) OR(robf.desti_bank_account_id = aj.id 
            AND aj.bank_account_id = rpb.id AND rpb.bank_id = rb.id 
            AND rb.id in {})) AND (robf.id in {}) 
            AND ((robf.bank_account_id = aj.id  
            AND aj.id in {})OR(robf.desti_bank_account_id = aj.id  
            AND aj.id in {})) AND robf.state = 'confirmed' 
            ORDER BY robf.operation_number ASC; 
            """).format(
            sql.Identifier(BANCOS_ORIGEN),
            sql.Identifier(BANCOS_DESTINO),
            sql.Identifier(FOLIOS),
            sql.Identifier(ACC_ORIGEN),
            sql.Identifier(ACC_DESTINO)
            )

        params = {'start': START_DATE,
                  'end': END_DATE,
                  }
        # Simular la ejecución de la consulta
        with patch.object(self.env.cr, 'execute') as mock_execute:
            # Ejecutar la prueba
            self.env.cr.execute(query, params)
            # Asegurar que la consulta se haya ejecutado al menos una vez
            self.assertTrue(mock_execute.called, FAILED_QUERY_MESSAGE)
            print(F"PRUEBA DE QUERY SANITIZADO 4 test__search_info_query4\n{query}")    
    

    def test__search_info_query5(self):
        # Intento de inyección
        END_DATE += TEST_INYECCION_SQL
           
        query= sql.SQL(""""
            SELECT DISTINCT robf.id, robf.operation_number FROM request_open_balance_finance robf, account_payment ap, rel_req_payment rrp, account_journal aj, res_partner_bank rpb, res_bank rb 
            WHERE ((robf.date_required >= %(start)s AND robf.date_required <= %(end)s) OR (robf.date >= %(start)s AND robf.date <= %(end)s) 
            OR robf.id=rrp.payment_id AND rrp.payment_request_rel_id = ap.id 
            AND( ap.payment_date >= %(start)s AND ap.payment_date <= %(end)s )) AND  ((robf.bank_account_id = aj.id AND aj.bank_account_id = rpb.id 
            AND rpb.bank_id = rb.id AND rb.id in {})
            OR(robf.desti_bank_account_id = aj.id AND aj.bank_account_id = rpb.id AND rpb.bank_id = rb.id AND rb.id in {})) 
            AND (robf.id in {}) AND ((robf.bank_account_id = aj.id AND aj.id in {})OR(robf.desti_bank_account_id = aj.id AND 
            aj.id in {})) AND (robf.trasnfer_request in {}) AND robf.state = 'confirmed' ORDER BY robf.operation_number ASC; 
            """).format(
            sql.Identifier(BANCOS_ORIGEN),
            sql.Identifier(BANCOS_DESTINO),
            sql.Identifier(FOLIOS),
            sql.Identifier(ACC_ORIGEN),
            sql.Identifier(ACC_DESTINO),
            sql.Identifier(TRANSFER),
            )

        params = {'start': START_DATE,
                  'end': END_DATE,
                  }
        # Simular la ejecución de la consulta
        with patch.object(self.env.cr, 'execute') as mock_execute:
            # Ejecutar la prueba
            self.env.cr.execute(query, params)
            # Asegurar que la consulta se haya ejecutado al menos una vez
            self.assertTrue(mock_execute.called, FAILED_QUERY_MESSAGE)
            print(F"PRUEBA DE QUERY SANITIZADO 5 test__search_info_query5\n{query}")    