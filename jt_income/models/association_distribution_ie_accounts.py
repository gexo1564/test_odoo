from odoo import models, fields,api,_
from odoo.exceptions import ValidationError, UserError

class AssociationDistributionIEAccounts(models.Model):

    _name = 'association.distribution.ie.accounts'
    _description = "Association Distribution IE Accounts"
    _rec_name = 'ie_key'

    ie_key = fields.Char("IE Key")
    desc = fields.Char("Description")
    scope_of_application = fields.Selection([('income','Income')],string="Scope Of Application")
    proration_calculation = fields.Selection([('percentage_price','Percentage of the price')],string="Proration calculation")
    amount = fields.Float('Amount')
    active = fields.Boolean('Active',default=True)
    ie_account_line_ids = fields.One2many('association.distribution.ie.accounts.line','ie_account_id','Definition')

    @api.model
    def create(self,vals):
        assdistributionie_obj = self.env['association.distribution.ie.accounts']
        is_exist_record = assdistributionie_obj.search([('ie_key','=',vals.get('ie_key')),('desc','=',vals.get('desc'))],limit=1)
        if is_exist_record:
            raise ValidationError(_('There is already a record with that ie account and that description'))

        is_exist_ie_key = assdistributionie_obj.search([('ie_key','=',vals.get('ie_key'))],limit=1)
        if is_exist_ie_key:
            raise ValidationError(_('The extraordinary income key is already registered in the module'))

        is_exist_description = assdistributionie_obj.search([('desc','=',vals.get('desc'))],limit=1)
        if is_exist_description:
            raise ValidationError(_('A record with the same description already exists'))
        
        return super(AssociationDistributionIEAccounts,self).create(vals)


    def unlink(self):
        for this_account_ie in self:
            is_exist_asign_account_ie = self.env['account.move.line'].search([('account_ie_id','=',this_account_ie.id)],limit=1)
            is_exist_asign_account_ie_2 = self.env['account.move'].search([('afectation_account_id','=',this_account_ie.id)],limit=1)
            if is_exist_asign_account_ie:
                raise ValidationError(_('You cannot delete an IE account when it is already being used in the Apuntes Contables records.'))
            if is_exist_asign_account_ie_2:
                raise ValidationError(_('You cannot delete an IE account when it is already being used in Education Services records.'))
        return super(AssociationDistributionIEAccounts,self).unlink()

    
class AssociationDistributionIEAccountsLines(models.Model):
    
    _name = 'association.distribution.ie.accounts.line'
    _description = "Association Distribution IE Accounts Lines"
    
    ie_account_id = fields.Many2one('association.distribution.ie.accounts',string='IE Account',ondelete='cascade')
    percentage = fields.Float('%')
    account_id = fields.Many2one("account.account",'Accounts')
    
    