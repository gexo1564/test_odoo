import calendar
from odoo import models, fields, api, _
from datetime import date, datetime
from odoo.exceptions import UserError, ValidationError

class CollaborationMonthlyPerformance(models.Model):
    _name = 'bases.collaboration.monthly.performance'
    _description = 'Collaboration Monthly Performance'

    date = fields.Date(string='Date')
    performance = fields.Float(string='Performance')
    average_rate_in_month = fields.Float(string='Average Rate in Month', digits=(12, 4))
    average_balance_in_month = fields.Float(string='Average Balance in Month')
    days_of_period = fields.Integer(string='Days of Period')




