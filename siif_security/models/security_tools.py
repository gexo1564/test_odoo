from odoo.exceptions import UserError, ValidationError, UserError
from odoo import models, api, _

class SecurityTools(models.AbstractModel):

    _name =  "security.tools"

    def check_user_permissions(self,list_users):
        user_permision = any(self.env.user.has_group(group) for group in list_users)

        if not user_permision:
           raise UserError(_("You don't have permission to perform this action."))