{
    'name': 'SIIF Budget',
    'summary': "SIIF Budget is used to improve functional requirements for budget's module through inherit",
    'version': '13.0.0.1.0',
    'category': 'Accounting',
    'author': 'SIIF UNAM',
    'maintainer': 'SIIF UNAM',
    'website': '',
    'license': 'AGPL-3',
    'depends': ['jt_budget_mgmt',],
    'data': [
        'data/sequences.xml',
        'views/employee_payroll_file_view.xml',

        # Security Files
        'security/security.xml',
        'security/ir.model.access.csv',
        # CRI
        'views/income_budgets_view.xml',
        'views/income_adequacies_view.xml',
        'views/relation_modules_view.xml',
        'views/relation_modules_process_view.xml',
        'views/relation_journal_process_view.xml',
        'views/res_users_dependencies_views.xml',
        'views/res_users_subdependencies_views.xml',
        'views/res_users_programs_views.xml',
        'views/res_users_subprograms_views.xml',
        'views/res_users_departure_groups_views.xml',
        'views/users_security_menus.xml',
        # wizards
        'wizard/import_budgets_line_view.xml',
        'wizard/import_income_adequacies_line_view.xml',
        'wizard/notification_wizard_view.xml',
    ],
    'application': False,
    'installable': True,
    'auto_install': False,
}