from odoo import models, fields,api,_
from odoo.exceptions import ValidationError

class FiscalRegimeAccount(models.Model):

    _name = 'fiscal.regime.account'
    _description = "Fiscal regime account"
    _rec_name = 'fiscal_regime'

    fiscal_regime =  fields.Char("Fiscal regime")
    iva_account = fields.Many2one('account.account', string='IVA retention account')
    isr_account = fields.Many2one('account.account', string='ISR retention account')
