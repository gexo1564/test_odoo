from odoo import fields, models


class ResPartner(models.Model):
    _name = 'res.partner'
    _inherit = ['res.partner', 'excel.detailed.information']

    def _compute_excel_display_name(self):
        for record in self:
            record.excel_display_name = record.name
