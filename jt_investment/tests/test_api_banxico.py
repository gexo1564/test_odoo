import json
import logging
import unittest
from odoo.exceptions import UserError,ValidationError
from datetime import datetime
from odoo.tests import tagged

from odoo.tests import common
import requests
_logger = logging.getLogger(__name__)

@tagged('-at_install','post_install','investment_udibonos','jt_investment')
class InvestmentUdibonosTest(common.TransactionCase):
    
    def setUp(self):
        super(InvestmentUdibonosTest,self).setUp()
        self.model = self.env['investment.udibonos']
        

    def test_get_udi_value_on_date(self):
        today = datetime.today().strftime('%Y-%m-%d')       
        token = self.env['ir.config_parameter'].get_param('banxico_token')
        url_banxico = self.env['ir.config_parameter'].get_param('banxico_api_url')
        url = f"{url_banxico}SP68257/datos/{today}/{today}?token={token}"
        response = requests.get(url,timeout=10)

        if response.status_code == 200:
            data = json.loads(response.content)
            udibono = data["bmx"]["series"][0]["datos"][0]["dato"]
            self.udi_value = udibono
            
        else:
            raise UserError(("Request Error"))
        
        print(f'-------------------------UDI VALUE {today}: {self.udi_value}-------------------------')
        
    def test_wizard_function(self):
        wizard = self.env['udi.liquidate.wizard']
        wizard.get_udi_value_on_date()
        

        
        print('-------------------------Test Data Create Result:-------------------------OK')
