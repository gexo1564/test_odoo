# -*- coding: utf-8 -*-
import logging
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError


class OperationsTypeCatolog(models.Model):
    _name = 'operations.type.catalog'
    _description = 'Operations type catalog'
    _rec_name = 'description'
    _inherit = ['mail.thread', 'mail.activity.mixin']


    flow = fields.Selection([('agreement', 'Agreements'), ('trust', 'Trusts'), ('endowment', 'Endowments'),],'Flow', required=True)
    description = fields.Char('Description', required = True)
    operation_type = fields.Selection([('decrease', 'Decrease'), ('increase', 'Increase'),],'Operation type', required=True)
    accounting_account_operation_id = fields.Many2one('account.account', string='Accounting account of the operation')
    