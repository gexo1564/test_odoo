import logging
from lxml import etree
from odoo import models, fields, api

class AccountJournal(models.AbstractModel):

	_inherit = 'account.journal'

	@api.model
	def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
		def set_atribs(nodes, value):
			for node in nodes:
				node.set('create', value)
				node.set('export_xlsx', value)
				node.set('delete', value)
				node.set('duplicate', value)
				node.set('arch', value)
				node.set('edit', value)
				node.set('active', value)

		def hide_buttons(node, value):
			node.set('create', value)
			node.set('import', value)
			node.set('export_xlsx', value)
			node.set('delete', value)
			node.set('edit', value)

		res = super().fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
		doc = etree.XML(res['arch'])
		#Subsidio Federal
		is_admin = self.env.user.has_group('jt_payroll_payment.group_finance_gestion')
		#Finanzas
		is_admin_finance = self.env.user.has_group('jt_payroll_payment.group_on_finance_admin')
		is_finance_user = self.env.user.has_group('jt_payroll_payment.group_admin_finance_user')
		

		#Finanzas/Diario de Cuentas Bancarias
		views_finance_diarios=[
		"account.bank.journal.tree.finance.protect",
		"account.journal.dashboard.kanban",
		"account.bank.journal.form.finance.protect"]

		#Finanzas/Configuración/Cuentas Bancarias
		views_finance_config_bank=[
		"account.bank.journal.tree.finance",
		"account.bank.journal.kanban.finance",
		"account.bank.journal.form.finance"]

		logging.critical(res['name'])
		# Módulo de finanzas
		if '.finance' in res['name']:
			if not is_admin:
				set_atribs(doc.xpath("//" + view_type), '0')
			if '.protect' in res['name'] :
				set_atribs(doc.xpath("//" + view_type), '0')

			#Finanzas/Diario de Cuentas Bancarias
			if is_admin_finance or is_finance_user:
				
				#Diarios de Cuentas Bancarias
				if res['name'] in views_finance_diarios:
					for node in doc.xpath("//" + view_type):
						if is_finance_user:
							set_atribs(doc.xpath("//" + view_type), '1')
							node.set('export', '1')
						elif is_admin_finance:
							hide_buttons(node, '1')
				#Cnfiguración/Cuentas Bancarias
				elif res['name'] in views_finance_config_bank:
					set_atribs(doc.xpath("//" + view_type), '1')

		# Modulo de contabilidad
		elif '.accounting' in res['name']:
			# Validación de Usuarios
			is_admin_contabilidad = self.env.user.has_group('jt_account_base.group_account_admin_user')
			is_training_contabilidad = self.env.user.has_group('jt_account_base.group_account_training_user')
			# Aplica para la vista de Diarios
			if not (is_admin_contabilidad or is_training_contabilidad):
				for node in doc.xpath("//" + view_type):
					# Ocultar botones de las vistas Odoo
					hide_buttons(node, '0')
		# Modulo de Finanzas
		# Aplica para todos los diarios que se abren mediante un campo Many2One
		elif 'account.journal.form' == res['name']:
			"""
			self.env.context = dict(self.env.context)
			self.env.context.update({
			'module': 'Finanzas',
			})
			"""
			for node in doc.xpath("//" + view_type):
				# Ocultar botones de las vistas Odoo
				hide_buttons(node, '0')
		# Modulo CONAC
		elif '.conac' in res['name']:
			# Validacion de Usuarios
			is_admin_contabilidad = self.env.user.has_group('jt_account_base.group_account_admin_user')
			is_training_contabilidad = self.env.user.has_group('jt_account_base.group_account_training_user')
			is_aprobacion_contabilidad = self.env.user.has_group('jt_account_base.group_account_approval_user')
			# Aplica para la vista de Diarios
			if view_type == 'tree':
				if not (is_admin_contabilidad or is_training_contabilidad):
					for node in doc.xpath("//" + view_type):
						node.set('export', 'false')
						node.set('edit', 'false')
			if view_type == 'form':
				if not (is_admin_contabilidad or is_training_contabilidad or is_aprobacion_contabilidad):
					for node in doc.xpath("//" + view_type):
						node.set('edit', 'false')
		res['arch'] = etree.tostring(doc)
		return res
