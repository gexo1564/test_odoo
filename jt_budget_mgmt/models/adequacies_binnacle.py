from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from datetime import datetime


class AdequaciesBinnacle(models.Model):

	_name = 'adequacies.binnacle'
	_description = 'Adequacies Binnacle'

	adequacie_folio = fields.Char("Adequacie folio")
	decrease_code = fields.Char("Decrease code")
	decrease_amount = fields.Float("Decrease amount")
	increase_code = fields.Char("Increase code")
	increase_amount = fields.Float("Increase amount")
	send_date = fields.Date("Send date")
	service_answer = fields.Char("Service answer")
	send_state = fields.Selection([('send', 'Send'), ('error', 'Error'), ('finished', 'Finished')])