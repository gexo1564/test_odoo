# -*- coding: utf-8 -*-
from datetime import datetime
import logging
from odoo.tests import common
from odoo.exceptions import ValidationError, ValidationError

DATE_FORMAT = "%Y-%m-%d"
class TestConversionResourceOrigin(common.TransactionCase):

    def setUp(self):
        super().setUp()
        self.resource_origin = self.env['resource.origin'].create({'key_origin': '99'})

    def test_check_end_date(self):
        """ Validación de las fechas de inicio y fin de vigencia. """
        catalogo = self.env['conversion.resource.origin'].create({
            'resource_origin': self.resource_origin.id,
        })
        """
        Escenario 1: Regla sin fecha de inicio
        """
        with self.assertRaises(ValidationError):
            catalogo.check_end_date()

        """
        Escenario 2: Regla con fecha de inicio, pero sin fecha de fin
        """
        catalogo.start_date = datetime.strptime("2022-01-01", DATE_FORMAT)

        """
        Escenario 3: Regla con fecha de inicio, y fecha fin menor a la fecha inicio
        """
        with self.assertRaises(ValidationError):
            catalogo.end_date = datetime.strptime("2021-12-31", DATE_FORMAT)

        """
        Escenario 4: Regla con fecha de inicio, y fecha fin igual a la fecha inicio
        """
        with self.assertRaises(ValidationError):
            catalogo.end_date = datetime.strptime("2022-01-01", DATE_FORMAT)

        """
        Escenario 5: Regla con fecha de inicio, y fecha fin igual a la fecha inicio
        """
        catalogo.end_date = datetime.strptime("2022-01-31", DATE_FORMAT)

