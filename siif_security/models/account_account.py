from lxml import etree
from odoo import models, fields, api

class AccountAccount(models.AbstractModel):
    _inherit = 'account.account'

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super().fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])
        is_admin_contabilidad = self.env.user.has_group('jt_account_base.group_account_admin_user')
        is_training_contabilidad = self.env.user.has_group('jt_account_base.group_account_training_user')
        is_register_contabilidad = self.env.user.has_group('jt_account_base.group_account_register_user')
        is_consulta_contabilidad = self.env.user.has_group('jt_account_base.group_account_guest_user')
        # Módulo de CONAC
        if 'account.account.list' == res['name']:
            if is_register_contabilidad or is_consulta_contabilidad:
                for node in doc.xpath("//" + view_type):
                    # Ocultar opcion de exportar de las vistas Odoo
                    node.set('export', 'false')
            if not (is_admin_contabilidad or is_training_contabilidad):
                if res.get('toolbar') and res['toolbar'].get('action'):
                    res['toolbar']['action'] = []
        res['arch'] = etree.tostring(doc)
        return res
