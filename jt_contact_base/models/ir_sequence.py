# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class IrSequence(models.Model):
    _inherit = 'ir.sequence'

    _sql_constraints = [('unique_code', 'unique(code)', _('Sequence: Sequence code must be unique.'))]