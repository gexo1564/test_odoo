# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields, api
from lxml import etree
class StatesAndProgram(models.Model):
    _name = 'states.program'
    _description = 'States and Program'
    _rec_name = 'concept'

    code = fields.Char(string='Code')
    concept = fields.Char(string='Concepto')
    approved_amt = fields.Float(string='Aprobado')
    ext_and_red_amt = fields.Float(string='Ampliaciones/ (Reducciones)')
    modified_amt = fields.Float(string='Modificado')
    accrued_amt = fields.Float(string='Devengado')
    paid_out_amt = fields.Float(string='Pagado')
    sub_exercise = fields.Char(string='Subejercicio')
    parent_id = fields.Many2one('states.program', string='Parent')

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super().fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])
        is_admin_contabilidad = self.env.user.has_group('jt_account_base.group_account_admin_user')
        is_training_contabilidad = self.env.user.has_group('jt_account_base.group_account_training_user')
        if not (is_admin_contabilidad or is_training_contabilidad):
            for node in doc.xpath("//" + view_type):
                # Ocultar opcion de exportar de las vistas Odoo
                node.set('export', 'false')
        res['arch'] = etree.tostring(doc)
        return res