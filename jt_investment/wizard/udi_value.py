import json
import logging
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
import datetime
import requests

class UDIValueWizard(models.TransientModel):
    _name = 'udi.value.wizard'
    _description = 'Wizard para registrar el valor de la udi al incio y fin de mes'

    msj_on_wizard = fields.Char(string=" ", readonly=True)
    id_udibono = fields.Integer()

    udi_value_first_day_month = fields.Float(
        "UDI Value First Day Month", digits=(4, 6))
    udi_value_end_month = fields.Float("UDI Value end Month", digits=(4, 6))

    udi_value = fields.Char(string="UDI Value")
    date_consult = fields.Date("Date", default=datetime.date.today())

    @api.onchange('date_consult')
    def get_udi_value_on_date(self):
        today = self.date_consult          
        token = self.env['ir.config_parameter'].get_param('banxico_token')
        url_banxico = self.env['ir.config_parameter'].get_param('banxico_api_url')
        url = f"{url_banxico}SP68257/datos/{today}/{today}?token={token}"
        response = requests.get(url,timeout=10)

        if response.status_code == 200:
            data = json.loads(response.content)
            udibono = data["bmx"]["series"][0]["datos"][0]["dato"]
            self.udi_value = udibono
            
        else:
            raise UserError(_("Request Error"))

    def update_udi_value(self):
        if self.udi_value_first_day_month <= 0 or self.udi_value_end_month <= 0:
            raise UserError(_('The UDI value cannot be negative or 0.'))
        udibonos_record = self.env['investment.udibonos'].search(
            [('state', '=', 'active')])
        for rec in udibonos_record:
            rec.udi_value_first_day_month = self.udi_value_first_day_month
            rec.udi_value_end_month = self.udi_value_end_month