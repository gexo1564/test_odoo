# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields,api,_
from odoo.exceptions import ValidationError
from datetime import datetime
import base64
import io
import pytz
import logging
import pdb

# Define Constant
CONTROL_AMOUNTS_RECEIVED_MODEL = 'control.amounts.received'
ACCOUNT_MOVE_MODEL = 'account.move'
CALENDAR_ASSIGNED_AMOUNTS_LINES_MODEL = 'calendar.assigned.amounts.lines'
SHCP_CODE = 'shcp.code'
ACCOUNT_FISCAL_YEAR = 'account.fiscal.year'
CLASSIFIER_INCOME_ITEM = 'classifier.income.item'
CONTROL_AMOUNTS_RECEIVED_LINE_MODEL = 'control.amounts.received.line'
RES_PARTNER_BANK = 'res.partner.bank'


class ControlAmountsReceived(models.Model):

    _name = CONTROL_AMOUNTS_RECEIVED_MODEL
    _description = 'Control of amounts received'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'folio'
    

    
    def _get_amount(self):
        for record in self:
            record.amount_to_receive = sum(
                record.line_ids.mapped('amount_assigned'))
            record.amount_received = sum(
                record.line_ids.mapped('amount_deposited'))
            record.amount_pending = record.amount_to_receive - record.amount_received

    fiscal_year = fields.Many2one(comodel_name = "account.fiscal.year", string = "Año Fiscal", readonly="1")
    date = fields.Date(string='Deposit date')
    folio = fields.Integer(string='Folio')
    budget_id = fields.Many2one('expenditure.budget', string='Budget')
    journal_id = fields.Many2one("account.journal", string="Journal")
    file = fields.Binary(string='Seasonal file')
    made_by_id = fields.Many2one('res.users', string='Made by')
    observations = fields.Text(string='Observations')
    line_ids = fields.One2many(CONTROL_AMOUNTS_RECEIVED_LINE_MODEL,
                               'control_id', string='Control of amounts received lines',domain=[('state', 'in', ('success','manual'))])

    import_line_ids = fields.One2many(CONTROL_AMOUNTS_RECEIVED_LINE_MODEL,
                               'control_id', string='Imported Lines',domain=[('state', 'in', ('draft','fail'))])
    
    calendar_assigned_amount_id = fields.Many2one(
        'calendar.assigned.amounts', string='Calendar of assigned amount')
    state = fields.Selection([('draft', 'Draft'), ('validate', 'Accrued'), ('collected', 'Collected'), ('cancelled', 'Cancelled')], default='draft',string='Status', track_visibility='onchange')

    file = fields.Binary(string='Seasonal File', copy=False)
    filename = fields.Char(string="File Name", copy=False)
    import_date = fields.Date(string='Import Date', copy=False,default=fields.Date.context_today)
    user_id = fields.Many2one('res.users', string='Responsible',
                              default=lambda self: self.env.user, tracking=True, copy=False)
    obs_calender_amount = fields.Text(string='Observations')
    obs_cont_amount = fields.Text(string='Observations')
    amount_to_receive = fields.Float(
        string='Amount to receive', compute='_get_amount')
    amount_received = fields.Float(
        string='Amount received', compute='_get_amount')
    amount_pending = fields.Float(
        string='Amount pending', compute='_get_amount')

    move_line_ids = fields.One2many('account.move.line', 'control_id', string="Journal Items")

    move_line = fields.One2many(ACCOUNT_MOVE_MODEL, 'control_id', string="Asientos Contables")
    move_line_documental_support = fields.One2many(ACCOUNT_MOVE_MODEL, 'control_id_documentary', string="Soporte Documental")

    name = fields.Char(string="Número")
    ref = fields.Char(string="Referencia")

    import_status = fields.Selection([
        ('draft', 'Draft'),
        ('in_progress', 'In Progress'),
        ('done', 'Completed')], default='draft', copy=False)     


    @api.model
    def load(self, fields, data):
        """ Sobrescribe la función de importación y cambia el valor de CTA_BANCARIA """
        fsp = self._get_validated_federal_subsidy_parameter()
        data = self._modify_data(data, fsp)

        rslt = super(ControlAmountsReceived, self).load(fields, data)
        return rslt


    def _get_validated_federal_subsidy_parameter(self):
        """Obtiene el parámetro de subsidio federal validado"""
        fsp = self.env['federal.subsidy.parameter'].search([('name', '=', 'SUBFED/P001'), ('state', '=', 'validated')])
        if not fsp:
            raise ValidationError(_("There is no bank account in the catalog of parameters of the federal subsidy!"))
        return fsp


    def _modify_data(self, data, fsp):
        """Modifica los datos según el valor de CTA_BANCARIA del parámetro de subsidio federal"""
        modified_data = []
        for row in data:
            modified_row = row[:] # Crea una copia de la sublista para modificarla
            if modified_row[7]:
                modified_row[7] = str(fsp.res_partner_bank_id.acc_number)
                modified_data.append(modified_row)

        return modified_data


    def _compute_total_rows(self):
        for budget in self:
            budget.failed_rows = self.env[CONTROL_AMOUNTS_RECEIVED_LINE_MODEL].search_count(
                [('control_id', '=', budget.id), ('state', '=', 'fail')])
            budget.success_rows = self.env[CONTROL_AMOUNTS_RECEIVED_LINE_MODEL].search_count(
                [('control_id', '=', budget.id), ('state', '=', 'success')])
            budget.total_rows = self.env[CONTROL_AMOUNTS_RECEIVED_LINE_MODEL].search_count(
                [('control_id', '=', budget.id),('state', '!=', 'manual')])

    failed_rows = fields.Integer(
        string='Failed Rows', compute="_compute_total_rows")
    success_rows = fields.Integer(
        string='Success Rows', compute="_compute_total_rows")
    total_rows = fields.Integer(
        string="Total Rows", compute="_compute_total_rows")

    failed_row_file = fields.Binary(string='Failed Rows File')
    fialed_row_filename = fields.Char(
        string='File name', default=lambda self: _("Failed_Rows.txt"))
    affectation_date = fields.Date(string = "Affectation Date", default = False) 
    proposed_date = fields.Date(string = "Proposed Date", readonly="1", store=True)
    documentary_support = fields.Many2many("ir.attachment", relation="control_amounts_received_documents_rel", column1="document_id", column2="attachment_id", string="Add Documentary Support")
        
    @api.depends('date')
    def _compute_month_int(self):
        for record in self:
            record.month_int = 0
            if record.date:
                record.month_int = record.date.month

    month_int = fields.Integer(string='Month Integer', store=True, compute='_compute_month_int')

    def unlink(self):
        for control in self:
            if control.state in ('validate'):
                if self.env.user.lang == 'es_MX':
                    raise ValidationError(_("No puedes eliminar un Registro del Subsidio Federal en estatus Validado."))
                else:
                    raise ValidationError('You cannot delete a Registration of Federal Subsidy in Validated status!')
        return super(ControlAmountsReceived, self).unlink()

    @api.model
    def default_get(self, fields):
        res = super(ControlAmountsReceived, self).default_get(fields)
        daily_income_jour = self.env.ref('jt_conac.daily_income_jour')
        if daily_income_jour:
            res.update({'journal_id': daily_income_jour.id})
        return res

    @api.model
    def create(self, vals):
        vals['folio'] = self.env['ir.sequence'].next_by_code(
            'control.amount.received') or _('New')
        res = super(ControlAmountsReceived, self).create(vals)
        return res

    def validate_import_line(self):
        if len(self.import_line_ids.ids) > 0:

            counter = 0
            failed_row = ""
            failed_line_ids = []
            success_line_ids = []
            # object
            bank_account_obj = self.env[RES_PARTNER_BANK].search_read([], fields=['id', 'acc_number'])
            shcp_code_obj = self.env[SHCP_CODE].search_read([], fields=['id', 'name'])
            departure_conversion_obj = self.env['departure.conversion'].search_read([], fields=['id', 'federal_part'])
            
            
            for line in self.import_line_ids:
                counter += 1
                line_vals = [line.branch_cr, line.unit_cr, line.folio_clc, line.clc_status, line.deposit_date, line.application_date,
                             line.currency_name, line.bank_account, line.year, line.branch,
                             line.unit, line.month_no, line.line_f, line.sfa,
                             line.sfe , line.prg, line.ai, line.ip,line.line_p, line.tg,line.ff,line.ef,line.amount_deposited,line.proposed_date]

                # Validation Authorized Amount
                amount_deposited = 0
                try:
                    amount_deposited = float(line.amount_deposited)
                    if amount_deposited <= 0:
                        failed_row += str(line_vals) + \
                                      "------>> Deposited Amount should be greater than 0!"
                        failed_line_ids.append(line.id)
                        continue
                except:
                    failed_row += str(line_vals) + \
                                  "------>> Invalid Deposited Amount Format"
                    failed_line_ids.append(line.id)
                    continue
                
                # Validate Bank Account
                bank_account_no = False
                if line.bank_account:
                    bank_account_no = list(filter(lambda prog: prog['acc_number'] == line.bank_account, bank_account_obj))
                    bank_account_no = bank_account_no[0]['id'] if bank_account_no else False
                if not bank_account_no:
                    failed_row += str(line_vals) + \
                                  "------>> Bank Account not exist\n"
                    failed_line_ids.append(line.id)
                    continue

                # Validate SHCP Code
                shcp_id = False
                if line.ip and line.line_p:
                    shcp_code = line.ip + line.line_p
                    shcp_id = list(filter(lambda prog: prog['name'] == shcp_code, shcp_code_obj))
                    shcp_id = shcp_id[0]['id'] if shcp_id else False
                if not shcp_id:
                    failed_row += str(line_vals) + \
                                  "------>> Invalid SHCP Program Code Format\n"
                    failed_line_ids.append(line.id)
                    continue

                # Validate Federal Item
#                 federal_item = False
#                 if line.ogto:
#                     federal_item = list(filter(lambda prog: prog['federal_part'] == line.ogto, departure_conversion_obj))
#                     federal_item = federal_item[0]['id'] if federal_item else False
#                 if not federal_item:
#                     failed_row += str(line_vals) + \
#                                   "------>> Invalid SHCP Games(CONPA) Format\n"
#                     failed_line_ids.append(line.id)
#                     continue

                # Validate Month
                if line.month_no and line.month_no < 1 and line.month_no > 12:
                    failed_row += str(line_vals) + \
                                  "------>> Invalid Month No. Format \n"
                    failed_line_ids.append(line.id)
                    continue
                
                if bank_account_no:
                    line.bank_account_id = bank_account_no
                    bank_account_rec = self.env[RES_PARTNER_BANK].browse(bank_account_no)
                    line.bank_id = bank_account_rec and bank_account_rec.bank_id and bank_account_rec.bank_id.id or False
                line.shcp_id = shcp_id
                success_line_ids.append(line.id)
                
            failed_lines = self.env[CONTROL_AMOUNTS_RECEIVED_LINE_MODEL].search(
                [('control_id', '=', self.id), ('id', 'in', failed_line_ids)])
            success_lines = self.env[CONTROL_AMOUNTS_RECEIVED_LINE_MODEL].search(
                [('control_id', '=', self.id), ('id', 'in', success_line_ids)])
            success_lines.write({'state': 'success'})
            for l in failed_lines:
                if l.state == 'draft':
                    l.state = 'fail'

            failed_data = False
            vals = {}
            if failed_row != "":
                content = ""
                if self.failed_row_file:
                    file_data = base64.b64decode(self.failed_row_file)
                    content += io.StringIO(file_data.decode("utf-8")).read()
                content += "\n"
                content += "...................Failed Rows " + \
                           str(datetime.today()) + "...............\n"
                content += str(failed_row)
                failed_data = base64.b64encode(content.encode('utf-8'))
                vals['failed_row_file'] = failed_data
                
            if vals.get('failed_row_file'):
                self.write(vals)
            if self.failed_rows == 0:
                self.import_status = 'done'

    def update_calendar_deposite_amount(self):
        count = 0
        for line in self.line_ids:
            count += 1    
            same_folio_line = self.env[CONTROL_AMOUNTS_RECEIVED_LINE_MODEL].search_count(
                [('control_id', '!=',False),('control_id', '!=', self.id), ('folio_clc', '=', line.folio_clc), ('year', '=', line.year), ('state_id', 'in', ('validate', 'collected'))])
            if same_folio_line:
                if self.env.user.lang == 'es_MX':
                    raise ValidationError(_("Actualmente, el Folio  %s ya está registrado!")%(line.folio_clc))
                else:
                    raise ValidationError(_("Currently Folio %s is already registered!")%(line.folio_clc))
            calendar_line = self.env[CALENDAR_ASSIGNED_AMOUNTS_LINES_MODEL].search([('budgetary_program','=',line.shcp_id.name),('new_item_id','=',line.new_item_id.id),('calendar_assigned_amount_id.state','=','validate'), ('year', '=', line.year)],limit=1)
            
            if not calendar_line:
                budgetary_program_name =  line.shcp_id and line.shcp_id.name or ''
                item_name = line.new_item_id and line.new_item_id.conversion_key or ''
                if self.env.user.lang == 'es_MX':
                    raise ValidationError(_("No se encuentra el calendario de la cantidad asignada para el programa presupuestario %s y la partida de gastos %s")%(budgetary_program_name,item_name))
                else:
                    raise ValidationError(_("Calendar of Assigned Amount not found for Budgetary Program %s and Item of Expenditure %s")%(budgetary_program_name,item_name))
            
            if calendar_line:
                if line.month_no==1:
                    if line.amount_deposited <= calendar_line.collect_january:
                        calendar_line.amount_deposite_january += line.amount_deposited
                        calendar_line.collect_january -= line.amount_deposited
                    elif line.amount_deposited > calendar_line.collect_january:
                        raise ValidationError(_('Error el monto que requiere comprobar como depositado en Enero. Error en linea %s' % count))
                elif line.month_no==2:
                    if line.amount_deposited <= calendar_line.collect_february:
                        calendar_line.amount_deposite_february += line.amount_deposited
                        calendar_line.collect_february -= line.amount_deposietd
                    elif line.amount_deposited > calendar_line.collect_february:
                        raise ValidationError(_('Error el monto que requiere comprobar como depositado en Febrero. Error en linea %s' % count))
                elif line.month_no==3:
                    if line.amount_deposited <= calendar_line.collect_march:
                        calendar_line.amount_deposite_march += line.amount_deposited
                        calendar_line.collect_march -= line.amount_deposited
                    elif line.amount_deposited > calendar_line.collect_march:
                        raise ValidationError(_('Error el monto que requiere comprobar como depositado en Marzo. Error en linea %s' % count))
                elif line.month_no==4:
                    if line.amount_deposited <= calendar_line.collect_april:
                        calendar_line.amount_deposite_april += line.amount_deposited
                        calendar_line.collect_april -= line.amount_deposited
                    elif line.amount_deposited > calendar_line.collect_april:
                        raise ValidationError(_('Error el monto que requiere comprobar como depositado en Abril. Error en linea %s' % count))
                elif line.month_no==5:
                    if line.amount_deposited <= calendar_line.collect_may:
                        calendar_line.amount_deposite_may += line.amount_deposited
                        calendar_line.collect_may -= line.amount_deposited
                    elif line.amount_deposited > calendar_line.collect_may:
                        raise ValidationError(_('Error el monto que requiere comprobar como depositado en Mayo. Error en linea %s' % count))
                elif line.month_no==6:
                    if line.amount_deposited <= calendar_line.collect_june:
                        calendar_line.amount_deposite_june += line.amount_deposited
                        calendar_line.collect_june -= line.amount_deposited
                    elif line.amount_deposited > calendar_line.collect_june:
                        raise ValidationError(_('Error el monto que requiere comprobar como depositado en Junio. Error en linea %s' % count))
                elif line.month_no==7:
                    if line.amount_deposited <= calendar_line.collect_july:
                        calendar_line.amount_deposite_july += line.amount_deposited
                        calendar_line.collect_july -= line.amount_deposited
                    elif line.amount_deposited > calendar_line.collect_july:
                        raise ValidationError(_('Error el monto que requiere comprobar como depositado en Julio. Error en linea %s' % count))
                elif line.month_no==8:
                    if line.amount_deposited <= calendar_line.collect_august:
                        calendar_line.amount_deposite_august += line.amount_deposited
                        calendar_line.collect_august -= line.amount_deposited
                    elif line.amount_deposited > calendar_line.collect_august:
                        raise ValidationError(_('Error el monto que requiere comprobar como depositado en Agosto. Error en linea %s' % count))
                elif line.month_no==9:
                    if line.amount_deposited <= calendar_line.collect_september:
                        calendar_line.amount_deposite_september += line.amount_deposited
                        calendar_line.collect_september -= line.amount_deposited
                    elif line.amount_deposited > calendar_line.collect_september:
                        raise ValidationError(_('Error el monto que requiere comprobar como depositado en Septiembre. Error en linea %s' % count))
                elif line.month_no==10:
                    if line.amount_deposited <= calendar_line.collect_october:
                        calendar_line.amount_deposite_october += line.amount_deposited
                        calendar_line.collect_october -= line.amount_deposited
                    elif line.amount_deposited > calendar_line.collect_october:
                        raise ValidationError(_('Error el monto que requiere comprobar como depositado en Octubre. Error en linea %s' % count))
                elif line.month_no==11:
                    if line.amount_deposited <= calendar_line.collect_november:
                        calendar_line.amount_deposite_november += line.amount_deposited
                        calendar_line.collect_november -= line.amount_deposited
                    elif line.amount_deposited > calendar_line.collect_november:
                        raise ValidationError(_('Error el monto que requiere comprobar como depositado en Noviembre. Error en linea %s' % count))
                elif line.month_no==12:
                    if line.amount_deposited <= calendar_line.collect_december:
                        calendar_line.amount_deposite_december += line.amount_deposited
                        calendar_line.collect_december -= line.amount_deposited
                    elif line.amount_deposited > calendar_line.collect_december:
                        raise ValidationError(_('Error el monto que requiere comprobar como depositado en Diciembre. Error en linea %s' % count))
                
                if line.bank_id:
                    calendar_line.bank_id = line.bank_id.id
                if line.bank_account_id:
                    calendar_line.bank_account_id = line.bank_account_id.id
                line.calendar_assigned_amount_line_id = calendar_line.id
                line.calendar_assigned_amount_id = calendar_line.calendar_assigned_amount_id and calendar_line.calendar_assigned_amount_id.id or False

#     def create_accured_journal_entry(self,partner_id,amount,journal,today,control_amount,company_id):
#         move_obj = self.env['account.move']
#         if not journal.accured_credit_account_id or not journal.conac_accured_credit_account_id \
#                 or not journal.accured_debit_account_id or not journal.conac_accured_debit_account_id:
#             if self.env.user.lang == 'es_MX':
#                 raise ValidationError(_("Por favor configure Devengado la cuenta UNAM y CONAC en diario!"))
#             else:
#                 raise ValidationError(_("Please configure Accured UNAM and CONAC account in journal!"))
#         unam_move_val = {'ref': self.folio, 'control_id': control_amount.id, 'conac_move': True,
#                          'date': today, 'journal_id': journal.id, 'company_id': company_id,
#                          'line_ids': [(0, 0, {
#                              'account_id': journal.accured_credit_account_id.id,
#                              'coa_conac_id': journal.conac_accured_credit_account_id.id,
#                              'credit': amount, 'control_id': control_amount.id,
#                              'partner_id': partner_id
#                          }), (0, 0, {
#                              'account_id': journal.accured_debit_account_id.id,
#                              'coa_conac_id': journal.conac_accured_debit_account_id.id,
#                              'debit': amount, 'control_id': control_amount.id,
#                              'partner_id': partner_id
#                          })]}
#         unam_move = move_obj.create(unam_move_val)
#         unam_move.action_post()
#         
#     def create_collected_journal_entry(self,partner_id,amount,journal,today,control_amount,company_id):
#         move_obj = self.env['account.move']
#         if not journal.collected_credit_account_id or not journal.conac_collected_credit_account_id \
#                 or not journal.collected_debit_account_id or not journal.conac_collected_debit_account_id:
#             if self.env.user.lang == 'es_MX':
#                 raise ValidationError(_("Por favor configure Recaudado la cuenta UNAM y CONAC en diario!"))
#             else:
#                 raise ValidationError(_("Please configure Collected UNAM and CONAC account in journal!"))
#         unam_move_val = {'ref': self.folio, 'control_id': control_amount.id, 'conac_move': True,
#                          'date': today, 'journal_id': journal.id, 'company_id': company_id,
#                          'line_ids': [(0, 0, {
#                              'account_id': journal.collected_credit_account_id.id,
#                              'coa_conac_id': journal.conac_collected_credit_account_id.id,
#                              'credit': amount, 'control_id': control_amount.id,
#                              'partner_id': partner_id
#                          }), (0, 0, {
#                              'account_id': journal.collected_debit_account_id.id,
#                              'coa_conac_id': journal.conac_collected_debit_account_id.id,
#                              'debit': amount, 'control_id': control_amount.id,
#                              'partner_id': partner_id
#                          })]}
#         unam_move = move_obj.create(unam_move_val)
#         unam_move.action_post()
        
    def create_account_journal_entry(self,cri_name,partner_id,amount,journal,today,control_amount,company_id,reference, collected = False):
        move_obj = self.env[ACCOUNT_MOVE_MODEL]
        if not journal.default_debit_account_id or not journal.default_credit_account_id \
                or not journal.conac_debit_account_id or not journal.conac_credit_account_id:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("Por favor configure la cuenta UNAM y CONAC en diario!"))
            else:
                raise ValidationError(_("Please configure UNAM and CONAC account in journal!"))
        if not journal.collected_credit_account_id or not journal.conac_collected_credit_account_id \
                or not journal.collected_debit_account_id or not journal.conac_collected_debit_account_id:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("Por favor configure Recaudado la cuenta UNAM y CONAC en diario!"))
            else:
                raise ValidationError(_("Please configure Collected UNAM and CONAC account in journal!"))
        if not journal.accured_credit_account_id or not journal.conac_accured_credit_account_id \
                or not journal.accured_debit_account_id or not journal.conac_accured_debit_account_id:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("Por favor configure Devengado la cuenta UNAM y CONAC en diario!"))
            else:
                raise ValidationError(_("Please configure Accured UNAM and CONAC account in journal!"))

        if collected:
            unam_move_val = {'ref': reference, 'control_id': control_amount.id, 'conac_move': True,
                             'journal_id': journal.id, 'company_id': company_id,'date':self.affectation_date,
                             'line_ids': [
                                 (0, 0, {
                                     'account_id': journal.default_credit_account_id.id,
                                     'coa_conac_id': journal.conac_credit_account_id.id,
                                     'credit': amount, 'control_id': control_amount.id,
                                     'partner_id': partner_id,
                                     'cri': cri_name.name
                                 }), 
                                 (0, 0, {
                                     'account_id': journal.default_debit_account_id.id,
                                     'coa_conac_id': journal.conac_debit_account_id.id,
                                     'debit': amount, 'control_id': control_amount.id,
                                     'partner_id': partner_id,
                                     
                              }),
                                 (0, 0, {
                                     'account_id': journal.collected_credit_account_id.id,
                                     'coa_conac_id': journal.conac_collected_credit_account_id.id,
                                     'credit': amount, 'control_id': control_amount.id,
                                     'partner_id': partner_id,
                                     'cri': cri_name.name
                                 }), 
                                 (0, 0, {
                                     'account_id': journal.collected_debit_account_id.id,
                                     'coa_conac_id': journal.conac_collected_debit_account_id.id,
                                     'debit': amount, 'control_id': control_amount.id,
                                     'partner_id': partner_id,
                                     'cri': cri_name.name
                                 }),                              
                             ]}
        else:
            unam_move_val = {'ref': reference, 'control_id': control_amount.id, 'conac_move': True,
                             'journal_id': journal.id, 'company_id': company_id,'date':today,
                             'line_ids': [
                                 # Apuntes contables (811 y 813) Devengado
                                 (0, 0, {
                                     'account_id': journal.accured_credit_account_id.id,
                                     'coa_conac_id': journal.conac_accured_credit_account_id.id,
                                     'credit': amount, 'control_id': control_amount.id,
                                     'partner_id': partner_id,
                                     'cri': cri_name.name
                                 }), 
                                 (0, 0, {
                                     'account_id': journal.accured_debit_account_id.id,
                                     'coa_conac_id': journal.conac_accured_debit_account_id.id,
                                     'debit': amount, 'control_id': control_amount.id,
                                     'partner_id': partner_id,
                                     'cri': cri_name.name
                                 })                                 
                             ]}            

        unam_move = move_obj.create(unam_move_val)
        unam_move.action_post()

        # Afectación al presupuesto
        if collected:
            type_affectation = 'dev-rec'
            date_affectation = self.affectation_date
            date_accrued = self.line_ids[0].proposed_date
            movement = 'accrued_collected'            
        else:
            type_affectation = 'eje-dev'
            # Crea objeto de las lineas cargadas
            date_affectation = self.line_ids[0].proposed_date
            date_accrued = False
            movement = 'for_exercised_accrued'
        cri_id = cri_name.id        
        self.env['affectation.income.budget'].search([]).affectation_income_budget(type_affectation,cri_id, date_accrued, date_affectation,amount)
        obj_logbook = self.env['income.adequacies.function']
        obj_recived_lines = self.env[CONTROL_AMOUNTS_RECEIVED_LINE_MODEL]
        line_search = obj_recived_lines.search([('control_id','=', self.id)])
        # Guarda la bitacora de la adecuación
        # Objeto de la tabla año fiscal
        obj_fiscal_year = self.env[ACCOUNT_FISCAL_YEAR]
        # Busca el id del año fiscal
        fiscal_year_id = obj_fiscal_year.search([('name', '=', line_search[0].year)])
        
        proposed_date = date_affectation
        movement_type = 'income'
        origin_type = 'income-sf'
        movement_id = unam_move.name
        journal_id = journal.id
        update_date_record = datetime.today()
        update_date_income = datetime.today()
        obj_logbook.search([]).logbook_income_budget(fiscal_year_id.id,cri_id,proposed_date,movement_type,movement,origin_type,movement_id,journal_id,update_date_record,amount,update_date_income)

    def agruped_dict(self):
        folio_data = {}
        for data in self.line_ids:
            folio = data.folio_clc
            amount = data.amount_deposited
            date = data.proposed_date
            today = date
            reference = str(data.branch_cr) + "-" + str(data.unit_cr) + "-" + str(data.folio_clc).zfill(3)      

            if folio in folio_data:
                # Si el folio ya existe, agregamos el aumento al total existente
                folio_data[folio]["amount"] += amount
            else:
                # Si el folio es nuevo, creamos una nueva entrada en el diccionario
                folio_data[folio] = {"folio": folio, "amount": amount, "date": date, "today": date, "reference": reference}

        return folio_data

    def oldest_proposed_date(self):
        oldest_proposed_date = None
        for data in self.line_ids:
            proposed_date = data.proposed_date
            if oldest_proposed_date is None or proposed_date < oldest_proposed_date:
                # Si encontramos una fecha propuesta más antigua, actualizamos la variable
                oldest_proposed_date = proposed_date
        return oldest_proposed_date

    def most_recent_date(self):
        most_recent_date = None
        for data in self.line_ids:
            proposed_date = data.proposed_date
            if most_recent_date is None or proposed_date > most_recent_date:
                # Si encontramos una fecha propuesta más antigua, actualizamos la variable
                most_recent_date = proposed_date
        return most_recent_date


    def validate(self):
        self.ensure_one()
        if self.total_rows != self.success_rows:
            raise ValidationError(_("Please validate all import lines!"))
        if not self.documentary_support:
            raise ValidationError(_("Please add in Documentary Support the file with which the CLC was loaded!"))
        move_obj = self.env[ACCOUNT_MOVE_MODEL]
        control_amount = self
        journal = control_amount.journal_id
        today = datetime.today()
        tz = self.env.user.tz or 'UTC'
        new_tz = pytz.timezone(tz)
        today = new_tz.localize(today).date()      
        user = self.env.user
        partner_id = user.partner_id.id

        amount = 0
        company_id = user.company_id.id
        # Diccionario agrupado por folio
        folio_data = self.agruped_dict()
        proposed = self.oldest_proposed_date()

        for rec in folio_data.values():
     
            amount = rec['amount']
            today = rec['today']
            reference = rec['reference']
        
            cri = '0'
            # Si no esta registrado el CRI, busca el código CRI vinculado a la cuenta contable del diario
            if not cri or cri == '0':
                # Extrae las cuentas contables del diario
                default_credit = journal.default_credit_account_id.id
                default_debit = journal.default_debit_account_id.id
                # Objeto de account account
                obj_account = self.env['relation.account.cri']
                # Busca si el código de la cuenta contable del diario comienza con 4
                cri_account = obj_account.search([('cuentas_id', '=', default_credit),('cuentas_id', 'like', '4%')])
                if not cri_account:
                    cri_account = obj_account.search([('cuentas_id', '=', default_debit),('cuentas_id', 'like', '4%')])
                    if not cri_account:
                        raise ValidationError(_("No se encontró el CRI vinculado a la cuenta contable del diario!"))
                # Envia todos los parametros para crear el asiento contable
                
                cri_name = self.env[CLASSIFIER_INCOME_ITEM].search([('id', '=', cri_account.cri.id)])
                if not cri_name:
                    raise ValidationError(f"No se encontró el CRI con id {cri_account.cri.id}")   
                self.create_account_journal_entry(cri_name,partner_id,amount,journal,today,control_amount,company_id,reference)
        
        fiscal_year = self.env[ACCOUNT_FISCAL_YEAR].search([('name', '=', today.year)])

        self.fiscal_year = fiscal_year.id
        self.proposed_date = proposed
        
        # Copia los asientos del devengado en soporte documental para cargar a cada uno archivo
        self._copy_move_line_to_documental_support()
        self.update_state('validate')


    def _copy_move_line_to_documental_support(self):
        self.move_line_documental_support = self.move_line



    def reverse(self):
        # Si hay asiento del devengado
        revert = "Reversión "
        if self.move_line:
            for move_id in self.move_line:
                reverse_move_line_ids = []

                for move_line in move_id.line_ids:
                    reverse_move_line_ids.append((0, 0, {
                        'account_id': move_line.account_id.id,
                        'coa_conac_id': move_line.coa_conac_id.id,
                        'debit': move_line.credit,
                        'credit': move_line.debit,
                        'control_id': move_line.control_id.id,
                        'partner_id': move_line.partner_id.id,
                        'cri': move_line.cri,
                    }))

                unam_move_val = {
                    'ref': revert + '(' + move_id.name + ')',
                    'control_id': move_id.control_id.id,
                    'conac_move': False,
                    'journal_id': move_id.journal_id.id,
                    'company_id': move_id.company_id.id,
                    'date': datetime.today().date(),
                    'line_ids': reverse_move_line_ids,
                }

                # Crea una nueva póliza por cada asiento
                unam_move = self.env[ACCOUNT_MOVE_MODEL].create(unam_move_val)
                unam_move.action_post()

                # Afectación al presupuesto
                type_affectation = 'eje-dev'
                # Crea objeto de las lineas cargadas
                date_affectation = self.line_ids[0].proposed_date
                date_accrued = False
                movement = 'for_exercised_accrued'
                amount = -unam_move.amount_total_signed
                cri_name = self.move_line_ids[0].cri
                cri = self.env[CLASSIFIER_INCOME_ITEM].search([('name', '=', cri_name)])
                cri_id = cri.id
                self.env['affectation.income.budget'].search([]).affectation_income_budget(type_affectation,cri_id, date_accrued, date_affectation,amount)
                obj_logbook = self.env['income.adequacies.function']
                obj_recived_lines = self.env[CONTROL_AMOUNTS_RECEIVED_LINE_MODEL]
                line_search = obj_recived_lines.search([('control_id','=', self.id)])
                # Guarda la bitacora de la adecuación
                # Objeto de la tabla año fiscal
                obj_fiscal_year = self.env[ACCOUNT_FISCAL_YEAR]
                # Busca el id del año fiscal
                fiscal_year_id = obj_fiscal_year.search([('name', '=', line_search[0].year)])
                
                proposed_date = date_affectation
                type_movement = 'income'
                origin_type = 'income-sf'
                cve_id = unam_move.name
                journal_id = self.journal_id.id
                update_date_record = datetime.today()
                update_date_income = datetime.today()
                obj_logbook.search([]).logbook_income_budget(fiscal_year_id.id,cri_id,proposed_date,type_movement,movement,origin_type,cve_id,journal_id,update_date_record,amount,update_date_income)

            self.update_state('cancelled')


    def validate_bank_witness(self):
        uploaded_documents = set()

        for line in self.move_line_documental_support:
            if not line.documentary_support:
                raise ValidationError(_("Add a bank witness for the reference %s.") % line.ref)

            # Check if the same document has been uploaded again
            if line.documentary_support in uploaded_documents:
                raise ValidationError(_("The document for reference %s is duplicated.") % line.ref)
            else:
                uploaded_documents.add(line.documentary_support)


    def update_state(self, new_state):
        for rec in self:
            rec.state = new_state
     

    def import_lines(self):
        return {
            'name': "Import and Validate File",
            'type': 'ir.actions.act_window',
            'res_model': 'import.control.amount.lines',
            'view_mode': 'form',
            'view_type': 'form',
            'views': [(False, 'form')],
            'target': 'new',
        }

    def collected(self):
        # Valida que cada asiento por folio tenga testigo bancario
        self.validate_bank_witness()

        self.ensure_one()
        if not self.affectation_date:
            raise ValidationError("¡Introduce una fecha de afectación!")
        if self.affectation_date.year != int(self.fiscal_year.name):
            raise ValidationError("¡La fecha de afectación debe ser del mismo año fiscal!")
        if self.affectation_date < self.proposed_date:
            raise ValidationError(_("The affectation date must be greater than or equal to the proposed date of the CLC"))
        self.update_calendar_deposite_amount()
        move_obj = self.env[ACCOUNT_MOVE_MODEL]
        control_amount = self
        journal = control_amount.journal_id
        today = datetime.today()
        tz = self.env.user.tz or 'UTC'
        new_tz = pytz.timezone(tz)
        today = new_tz.localize(today).date()      
        user = self.env.user
        partner_id = user.partner_id.id
        count = 0
        amount = 0
        company_id = user.company_id.id
        # Diccionario agrupado por folio
        folio_data = self.agruped_dict()
        most_recent_proposed_date = self.most_recent_date()

        for rec in folio_data.values():     
            amount = rec['amount']
            today = rec['today']
            reference = rec['reference']   
  
            cri = '0'
            # Si no esta registrado el CRI, busca el código CRI vinculado a la cuenta contable del diario
            if not cri or cri == '0':
                # Extrae las cuentas contables del diario
                default_credit = journal.default_credit_account_id.id
                default_debit = journal.default_debit_account_id.id
                # Objeto de account account
                obj_account = self.env['relation.account.cri']
                # Busca si el código de la cuenta contable del diario comienza con 4
                cri_account = obj_account.search([('cuentas_id', '=', default_credit),('cuentas_id', 'like', '4%')])
                if not cri_account:
                    cri_account = obj_account.search([('cuentas_id', '=', default_debit),('cuentas_id', 'like', '4%')])
                    if not cri_account:
                        raise ValidationError(_("No se encontró el CRI vinculado a la cuenta contable del diario!"))
                # Envia todos los parametros para crear el asiento contable
                
                cri_name = self.env[CLASSIFIER_INCOME_ITEM].search([('id', '=', cri_account.cri.id)])
                if not cri_name:
                    raise ValidationError(f"No se encontró el CRI con id {cri_account.cri.id}")   
                self.create_account_journal_entry(cri_name,partner_id,amount,journal,today,control_amount,company_id,reference, True)
        if self.affectation_date < most_recent_proposed_date:
            raise ValidationError(_("¡La fecha propuesta más reciente es %s por lo tanto la fecha de afectación debe ser mayor o igual a la fecha propuesta más reciente!") % (most_recent_proposed_date))
        
        self.update_state('collected')



class ControlAmountsReceivedLine(models.Model):

    _name = CONTROL_AMOUNTS_RECEIVED_LINE_MODEL
    _description = 'Control of amounts received lines'
    _rec_name = 'deposit_date'

    def _get_amount_pending(self):
        for record in self:
            record.amount_pending = record.amount_assigned - record.amount_deposited

    
    month = fields.Selection([
        ('january', 'January'),
        ('february', 'February'),
        ('march', 'March'),
        ('april', 'April'),
        ('may', 'May'),
        ('june', 'June'),
        ('july', 'July'),
        ('august', 'August'),
        ('september', 'September'),
        ('october', 'October'),
        ('november', 'November'),
        ('december', 'December')], string='Month of the amount', default='january')

    
    amount_assigned = fields.Integer(string='Amount assigned')
    amount_pending = fields.Integer(string='Amount pending', compute='_get_amount_pending')
    observations = fields.Text(string='Comments')
    control_id = fields.Many2one(
        CONTROL_AMOUNTS_RECEIVED_MODEL, string='Control of amounts received',ondelete='cascade')
    calendar_assigned_amount_id = fields.Many2one(
        'calendar.assigned.amounts', string='Calendar of assigned amount')
    calendar_assigned_amount_line_id = fields.Many2one(
        CALENDAR_ASSIGNED_AMOUNTS_LINES_MODEL, string='Calendar of assigned amount Line')

    # ======Import Line Data ====== #
    is_imported = fields.Boolean('Imported',default=False)
    state = fields.Selection([('manual', 'Manual'), ('draft', 'Draft'), (
        'fail', 'Fail'), ('success', 'Success')], string='Status', default='manual')

    shcp_id = fields.Many2one(
        SHCP_CODE, string='Budgetary Program')
    amount_deposited = fields.Float(string='Amount deposited')
    branch_cr = fields.Integer('Branch CR')
    unit_cr = fields.Char('Unit CR')
    folio_clc = fields.Integer('Folio CLC')
    clc_status = fields.Char('CLC Status')
    deposit_date = fields.Date(string='Deposit date')
    application_date = fields.Date(string='Application date')
    proposed_date = fields.Date(string='Proposed date')
    currency_name = fields.Many2one(
        'res.currency', default=lambda self: self.env.company.currency_id,string="Currency")

    bank_account_id = fields.Many2one(RES_PARTNER_BANK, string='Bank account')
    bank_id = fields.Many2one('res.bank', related='bank_account_id.bank_id')
    # Se omite el campo bank_account, ya que solo se usa en el flujo donde se importa
    # el archivo al crear manualmente el registro.
    # Para creación mediente el botón de odoo para importar en la vista de lista,
    # se usa bank_account_id
    # bank_account = fields.Char('Bank account')
    year = fields.Char('Year')
    branch = fields.Integer('Branch')
    unit = fields.Char('Unit')
    month_no = fields.Integer('Month')
    line_f = fields.Integer('F')
    sfa = fields.Integer('SFA')
    sfe = fields.Integer('SFE')
    prg = fields.Integer('PRG')
    ai = fields.Integer('AI')
    ip = fields.Char('IP',Size=1)
    line_p = fields.Char('P',size=3)
    conversion_name = fields.Char(string='Conversion Name',size=5)
    conpa_id = fields.Many2one('departure.conversion','OGTO')
    new_item_id = fields.Many2one('federal.departure.finance','Federal departure')
    tg = fields.Integer('TG')
    ff = fields.Integer('FF')
    ef = fields.Integer('EF')
    is_manual_line = fields.Boolean('Manual Add Line',default=False)
    state_id = fields.Selection(string='Status', related="control_id.state", store=True)
    
    @api.constrains('folio_clc')
    def check_folio_unique(self):
        if self.folio_clc: 
            same_folio_line = self.env[CONTROL_AMOUNTS_RECEIVED_LINE_MODEL].search_count(
                    [('control_id', '!=',False),('control_id', '!=', self.control_id.id), ('folio_clc', '=', self.folio_clc),  ('year', '=', self.year), ('state_id', 'in', ('validate', 'collected'))])
            if same_folio_line:
                if self.env.user.lang == 'es_MX':
                    raise ValidationError(_("Actualmente, el Folio  %s ya está registrado en el año %s") % (self.folio_clc, self.year))
                else:
                    raise ValidationError(_("Currently Folio %s is already registered!")%(self.folio_clc))
            same_folio_line_cancel = self.env[CONTROL_AMOUNTS_RECEIVED_LINE_MODEL].search_count(
                    [('control_id', '!=',False),('control_id', '!=', self.control_id.id), ('folio_clc', '=', self.folio_clc),  ('year', '=', self.year), ('state_id', '=', 'cancelled')])
            if same_folio_line_cancel and self.env.user.lang == 'es_MX':
                raise ValidationError(_("Currently, Folio %s is canceled in the year %s!") % (self.folio_clc, self.year))

    @api.model
    def create(self, vals):
        res = super(ControlAmountsReceivedLine, self).create(vals)
        vals = self._process_line_p(vals)
        self._update_shcp_id(res, vals)
        self._check_conpa_validity(vals)

        return res
    

    def _check_conpa_validity(self, vals):
        conpa = self.env['federal.departure.finance'].browse(vals['new_item_id'])
        conpa_validity = self.env[CALENDAR_ASSIGNED_AMOUNTS_LINES_MODEL].get_conpa_validity_from_federal_departure_finance(conpa.conversion_key)
        if not conpa_validity:
            raise ValidationError(_("There aren't current dates for %s") % conpa.conversion_key)


    def _process_line_p(self, vals):
        if vals.get('line_p', False):
            line_p = str(vals['line_p']).zfill(3)
            vals.update({'line_p': line_p})
        return vals


    def _update_shcp_id(self, record, vals):
        ip = vals['ip']
        line_p = vals['line_p']
        code = str(ip) + str(line_p)
        id_shcp = self.env[SHCP_CODE].search([('name', '=', code), '&', ('start_date', '<=', datetime.now()), '|', ('end_date', '=', False), ('end_date', '>=', datetime.now())])
        record.shcp_id = id_shcp.id

    
    def write(self,vals):
        if vals.get('line_p',False):
            line_p = vals.get('line_p',False)
            line_p = str(line_p).zfill(3)
            vals.update({'line_p':line_p})
        return super(ControlAmountsReceivedLine,self).write(vals)

         
class AccountMove(models.Model):

    _inherit = ACCOUNT_MOVE_MODEL

    control_id = fields.Many2one(CONTROL_AMOUNTS_RECEIVED_MODEL)
    control_id_documentary = fields.Many2one(CONTROL_AMOUNTS_RECEIVED_MODEL)
    documentary_support = fields.Binary(string="Agregar Testigo Bancario", store=True)
    filename = fields.Char(string='File name')
    
    
class AccountMoveLine(models.Model):

    _inherit = 'account.move.line'

    control_id = fields.Many2one(CONTROL_AMOUNTS_RECEIVED_MODEL)
