import logging
import re
from odoo.exceptions import UserError,ValidationError
from datetime import datetime
from odoo.tests import tagged
from odoo.tests import common

_logger = logging.getLogger(__name__)

@tagged('-at_install','post_install','account_account','siif_account_design')
class TestAccountAccount(common.TransactionCase):
    
    def setUp(self):
        super(TestAccountAccount,self).setUp()

    def test_search_no_valid(self):

        obj = self.env['account.account'].search([])

        for rec in obj:
            pattern = r'^\d{3}\.\d{3}\.\d{3}\.\d{3}$'
            if not re.match(pattern, rec.code):
                print(f"Registro {rec.code}")