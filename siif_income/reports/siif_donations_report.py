from odoo import models,api,_
from odoo.tools.misc import formatLang
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.tools.misc import xlsxwriter
import io
import base64
from odoo.tools import config, date_utils, get_lang
import lxml.html
import logging

_logger = logging.getLogger(__name__)
date_format = '%Y-%m-%d'
modelos_repetidos = [
    'ir.actions.report',            # 0
    'web.minimal_layout',           # 1
    ]


class SiifDonationsReport(models.AbstractModel):
    _name ='siif.donations.report'
    _inherit = 'account.coa.report'
    _description = 'Siif Donations Report'

    filter_date = {'mode': 'range', 'filter': 'this_month'}
    filter_comparison = None
    filter_all_entries = None
    filter_journals = None
    filter_analytic = None
    filter_unfold_all = None
    filter_cash_basis = None
    filter_hierarchy = None
    filter_unposted_in_period = None
    MAX_LINES = None

    filter_limit_uma = [
        {'id': "1", 'name': 'Sobrepasa el valor de UMAS','selected':False},
        {'id': "0", 'name': 'De bajo del valor de UMAS','selected':False},
    ]

    def _get_reports_buttons(self):
        users = self.env['res.users']
        user = users.browse(self._uid)
        is_group_income_admin_user = user.has_group('jt_income.group_income_admin_user')
        is_group_income_guest_user = user.has_group('jt_income.group_income_guest_user')
        is_group_budget_catalogs_admin_user = user.has_group('jt_income.group_income_admin_catalog_user')
        is_group_admin_finance_user = user.has_group('jt_income.group_income_admin_finance_user')
        is_group_income_dependence_user = user.has_group('jt_income.group_income_dependence_user')



        if is_group_income_admin_user or is_group_income_guest_user or is_group_budget_catalogs_admin_user or is_group_admin_finance_user:
            return [
                {'name': _('Export to PDF'), 'sequence': 1, 'action': 'print_pdf', 'file_export_type': _('PDF')},
                {'name': _('Export (XLSX)'), 'sequence': 2, 'action': 'print_xlsx', 'file_export_type': _('XLSX')},
            ]
        else:
            if is_group_income_dependence_user:
                return [
                    {'name': _('Export (XLSX)'), 'sequence': 2, 'action': 'print_xlsx', 'file_export_type': _('XLSX')},
                ]
            else:
                return[]

    def _get_templates(self):
        templates = super(SiifDonationsReport, self)._get_templates()
        templates[
            'main_table_header_template'] = 'account_reports.main_table_header'
        templates['main_template'] = 'account_reports.main_template'
        return templates

    def _get_columns_name(self, options):
        return [ {'name': _('Dependencias & Sub-Dependencias')},
            {'name': _('Fecha')},
            {'name': _('Número de Donativo')},
            {'name': _('Tipo')},
            {'name': _('Convenio')},
            {'name': _('RFC')},
            {'name': _('Moneda')},
            {'name': _('Monto')},
            {'name': _('Estado')}
        ]

    def _format(self, value,figure_type):
        if self.env.context.get('no_format'):
            return value
        value['no_format_name'] = value['name']
        
        if figure_type == 'float':
            currency_id = self.env.company.currency_id
            if currency_id.is_zero(value['name']):
                # don't print -0.0 in reports
                value['name'] = abs(value['name'])
                value['class'] = 'number text-muted'
            value['name'] = formatLang(self.env, value['name'], currency_obj=currency_id)
            value['class'] = 'number'
            return value
        if figure_type == 'percents':
            value['name'] = str(round(value['name'] * 100, 1)) + '%'
            value['class'] = 'number'
            return value
        value['name'] = round(value['name'], 1)
        return value
    
    def _get_lines(self, options, line_id=None):
        lines = []
        start = datetime.strptime(
            str(options['date'].get('date_from')), date_format).date()
        end = datetime.strptime(
            options['date'].get('date_to'), date_format).date()

        domain = [('emission_date', '>=', start), ('emission_date', '<=', end)]
        state_domain = []
        bol_uma_true = False
        state_select = options.get('limit_uma')
        for p_type in state_select:
            if p_type.get('selected',False):
                if p_type.get('id') == "1":
                    state_domain.append(True)
                    bol_uma_true = True
                elif p_type.get('id') == "0":
                    state_domain.append(False)
                    bol_uma_true = False
        
        if state_domain and len(state_domain) == 1:
                domain += [('limit_value_uma','in',state_domain)]

        if len(state_domain) == 1 :
            self.env.cr.execute('''SELECT DISTINCT ON (dependancy_id) dependancy_id FROM donations WHERE emission_date BETWEEN %s and %s AND limit_value_uma = %s''',(start,end,bol_uma_true))
            dependencys =self.env.cr.fetchall()
            self.env.cr.execute('''SELECT DISTINCT ON (sub_dependancy_id) sub_dependancy_id FROM donations WHERE emission_date BETWEEN %s and %s AND limit_value_uma = %s''',(start,end,bol_uma_true))
            sub_dependencys = self.env.cr.fetchall()
        else:
            self.env.cr.execute('''SELECT DISTINCT ON (dependancy_id) dependancy_id FROM donations WHERE emission_date BETWEEN %s and %s''',(start,end))
            dependencys = None
            dependencys =self.env.cr.fetchall()
            self.env.cr.execute('''SELECT DISTINCT ON (sub_dependancy_id) sub_dependancy_id FROM donations WHERE emission_date BETWEEN %s and %s''',(start,end))
            sub_dependencys = None
            sub_dependencys = self.env.cr.fetchall()
        origin_datas = self.env['donations'].search(domain)
        total_donations = 0

        for dep in dependencys:
            dependency_id = self.env['dependency'].browse(dep)
            lines.append({
                    'id':'hierarchy0_dependency',
                    'name': (str(dependency_id.dependency)+' '+str(dependency_id.description)),
                    'columns': [{'name':''},
                                {'name': ''},
                                {'name': ''},
                                {'name': ''},
                                {'name': ''},
                                {'name': ''},
                                {'name': ''}
                                ],
                    'level': 1,
                    'unfoldable': False,
                    'unfolded': False,
                    })
            for sub_dep in sub_dependencys:
                sub_dependency_id = self.env['sub.dependency'].browse(sub_dep)
                if dependency_id.id == sub_dependency_id.dependency_id.id:
                    lines.append({
                        'id':'hierarchy1_sub_dependency',
                        'name': str(sub_dependency_id.sub_dependency)+' '+str(sub_dependency_id.description),
                        'columns': [{'name':''}],
                        'level': 2,
                        'unfoldable': False,
                        'unfolded': False,
                        'parent_id': 'hierarchy0_dependency'
                        })
                for donation in origin_datas:
                    if donation.dependancy_id.id == dependency_id.id and donation.sub_dependancy_id.id == sub_dependency_id.id:
                        status = ''
                        if donation.donation_status == 'draft':
                            status = 'Borrador'
                        elif donation.donation_status == 'in_review':
                            status = 'En Revisión'
                        elif donation.donation_status == 'accepted':
                            status = 'Aprobado'
                        elif donation.donation_status == 'refused':
                            status = 'Rechazado'
                        elif donation.donation_status == 'with_reference':
                            status = 'Con Referencia'
                        elif donation.donation_status == 'received':
                            status = 'Recibido'
                        elif donation.donation_status == 'invoiced':
                            status = 'Facturado'
                        elif donation.donation_status == 'charged':
                            status = 'Cobrado'
                        else: 
                            status = ''
                        lines.append({
                            'id':'hierarchy2_data',
                            'name': '',
                            'columns': [{'name':donation.emission_date.strftime("%d/%m/%Y")},
                                        {'name':donation.name},
                                        {'name':donation.type_donation_id.name_type},
                                        {'name':donation.agreement},
                                        {'name':donation.benefactor_rfc},
                                        {'name':donation.currency_id.name},
                                        {'name':'${:,.2f}'.format(donation.total_donation_amount)},
                                        {'name': status},
                                        ],
                            'level': 3,
                            'unfoldable': False,
                            'unfolded': False,
                            'parent_id': 'hierarchy1_sub_dependency'
                            })
                        total_donations += donation.total_donation_amount
        lines.append({
            'id':'hierarchy0_total',
            'name': '',
            'columns': [{'name':''},
                        {'name':''},
                        {'name':''},
                        {'name':''},
                        {'name':''},
                        {'name':''},
                        {'name':''},
                        {'name':''},
                        ],
                            'level': 1,
                            'unfoldable': False,
                            'unfolded': False,
        })
        lines.append({
            'id':'hierarchy0_total_amount',
            'name': '',
            'columns': [{'name':''},
                        {'name':''},
                        {'name':''},
                        {'name':''},
                        {'name':''},
                        {'name':'TOTAL'},
                        {'name':'${:,.2f}'.format(total_donations)},
                        {'name':''},
                        ],
            'level': 2,
            'unfoldable': False,
            'unfolded': False,
            'parent_id': 'hierarchy0_total'

        })
        

        return lines

    def _get_report_name(self):
        return _("SIIF Donations Report")
    
    @api.model
    def _get_super_columns(self, options):
        date_cols = options.get('date') and [options['date']] or []
        date_cols += (options.get('comparison') or {}).get('periods', [])
        columns = reversed(date_cols)
        return {'columns': columns, 'x_offset': 1, 'merge': 5}

    def get_month_name(self,month):
        month_name = ''
        if month==1:
            month_name = 'Enero'
        elif month==2:
            month_name = 'Febrero'
        elif month==3:
            month_name = 'Marzo'
        elif month==4:
            month_name = 'Abril'
        elif month==5:
            month_name = 'Mayo'
        elif month==6:
            month_name = 'Junio'
        elif month==7:
            month_name = 'Julio'
        elif month==8:
            month_name = 'Agosto'
        elif month==9:
            month_name = 'Septiembre'
        elif month==10:
            month_name = 'Octubre'
        elif month==11:
            month_name = 'Noviembre'
        elif month==12:
            month_name = 'Diciembre'
            
        return month_name.upper()
    
    def get_header_year_list(self,options):
        start = datetime.strptime(
            str(options['date'].get('date_from')), date_format).date()
        end = datetime.strptime(
            options['date'].get('date_to'), date_format).date()
        
        str1 = ''
        if start.year == end.year:
            if start.month != end.month:
                str1 = self.get_month_name(start.month) + "-" +self.get_month_name(end.month) + " " + str(start.year)
            else:   
                str1 = self.get_month_name(start.month) + " " + str(start.year)
        else:
            str1 = self.get_month_name(start.month)+" "+ str(start.year) + "-" +self.get_month_name(end.month) + " " + str(end.year)
        return str1


    def get_xlsx(self, options, response=None):
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        sheet = workbook.add_worksheet(self._get_report_name()[:31])
 
        date_default_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2, 'num_format': 'yyyy-mm-dd'})
        date_default_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': 'yyyy-mm-dd'})
        default_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2})
        default_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'})
        title_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'bottom': 2})
        super_col_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'align': 'center'})
        level_0_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 13, 'bottom': 6, 'font_color': '#666666'})
        level_1_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 13, 'bottom': 1, 'font_color': '#666666'})
        level_2_col1_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 1})
        level_2_col1_total_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666'})
        level_2_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666'})
        level_3_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2})
        level_3_col1_total_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 1})
        level_3_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'})
        
        #Set the first column width to 50
#         sheet.set_column(0, 0,15)
#         sheet.set_column(0, 1,20)
#         sheet.set_column(0, 2,20)
#         sheet.set_column(0, 3,12)
#         sheet.set_column(0, 4,20)
        #sheet.set_row(0, 0,50)
        #sheet.col(0).width = 90 * 20
        #sheet.write(y_offset, 0,'', title_style)
        y_offset = 0
        col = 0
        
#         sheet.merge_range(y_offset, col, 6, col, '')
#         if self.env.user and self.env.user.company_id and self.env.user.company_id.header_logo:
#             filename = 'logo.png'
#             image_data = io.BytesIO(base64.standard_b64decode(self.env.user.company_id.header_logo))
#             sheet.insert_image(0,0, filename, {'image_data': image_data,'x_offset':8,'y_offset':3,'x_scale':0.6,'y_scale':0.6})
        
        header_title = '''Reporte de donativos\n%s'''%(self.get_header_year_list(options))
        sheet.merge_range(y_offset, col, 5, col+16, header_title,super_col_style)
        y_offset += 6
#         col=1
#         currect_time_msg = "Fecha y hora de impresión: "
#         currect_time_msg += datetime.today().strftime('%d/%m/%Y %H:%M')
#         sheet.merge_range(y_offset, col, y_offset, col+17, currect_time_msg,currect_date_style)
#         y_offset += 1
        
#         x = super_columns.get('x_offset', 0)
#         for super_col in super_columns.get('columns', []):
#             cell_content = super_col.get('string', '').replace('<br/>', ' ').replace('&nbsp;', ' ')
#             x_merge = super_columns.get('merge')
#             if x_merge and x_merge > 1:
#                 sheet.merge_range(0, x, 0, x + (x_merge - 1), cell_content, super_col_style)
#                 x += x_merge
#             else:
#                 sheet.write(0, x, cell_content, super_col_style)
#                 x += 1
        for row in self.get_header(options):
            x = 0
            for column in row:
                colspan = column.get('colspan', 1)
                header_label = column.get('name', '').replace('<br/>', ' ').replace('&nbsp;', ' ')
                if colspan == 1:
                    sheet.write(y_offset, x, header_label, title_style)
                else:
                    sheet.merge_range(y_offset, x, y_offset, x + colspan - 1, header_label, title_style)
                x += colspan
            y_offset += 1
        ctx = self._set_context(options)
        ctx.update({'no_format':True, 'print_mode':True, 'prefetch_fields': False})
        # deactivating the prefetching saves ~35% on get_lines running time
        lines = self.with_context(ctx)._get_lines(options)
 
        if options.get('hierarchy'):
            lines = self._create_hierarchy(lines, options)
        if options.get('selected_column'):
            lines = self._sort_lines(lines, options)
 
        #write all data rows
        for y in range(0, len(lines)):
            level = lines[y].get('level')
            if lines[y].get('caret_options'):
                style = level_3_style
                col1_style = level_3_col1_style
            elif level == 0:
                y_offset += 1
                style = level_0_style
                col1_style = style
            elif level == 1:
                style = level_1_style
                col1_style = style
            elif level == 2:
                style = level_2_style
                col1_style = 'total' in lines[y].get('class', '').split(' ') and level_2_col1_total_style or level_2_col1_style
            elif level == 3:
                style = level_3_style
                col1_style = 'total' in lines[y].get('class', '').split(' ') and level_3_col1_total_style or level_3_col1_style
            else:
                style = default_style
                col1_style = default_col1_style
 
            #write the first column, with a specific style to manage the indentation
            cell_type, cell_value = self._get_cell_type_value(lines[y])
            if cell_type == 'date':
                sheet.write_datetime(y + y_offset, 0, cell_value, date_default_col1_style)
            else:
                sheet.write(y + y_offset, 0, cell_value, col1_style)
 
            #write all the remaining cells
            for x in range(1, len(lines[y]['columns']) + 1):
                cell_type, cell_value = self._get_cell_type_value(lines[y]['columns'][x - 1])
                if cell_type == 'date':
                    sheet.write_datetime(y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, date_default_style)
                else:
                    sheet.write(y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, style)
 
        workbook.close()
        output.seek(0)
        generated_file = output.read()
        output.close()
        return generated_file    
        
    
    def get_pdf(self, options, minimal_layout=True):
        # As the assets are generated during the same transaction as the rendering of the
        # templates calling them, there is a scenario where the assets are unreachable: when
        # you make a request to read the assets while the transaction creating them is not done.
        # Indeed, when you make an asset request, the controller has to read the `ir.attachment`
        # table.
        # This scenario happens when you want to print a PDF report for the first time, as the
        # assets are not in cache and must be generated. To workaround this issue, we manually
        # commit the writes in the `ir.attachment` table. It is done thanks to a key in the context.
        minimal_layout = False
        if not config['test_enable']:
            self = self.with_context(commit_assetsbundle=True)

        base_url = self.env['ir.config_parameter'].sudo().get_param('report.url') or self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        rcontext = {
            'mode': 'print',
            'base_url': base_url,
            'company': self.env.company,
        }

        body = self.env['ir.ui.view'].render_template(
            "account_reports.print_template",
            values=dict(rcontext),
        )
        body_html = self.with_context(print_mode=True).get_html(options)
        body = body.replace(b'<body class="o_account_reports_body_print">', b'<body class="o_account_reports_body_print">' + body_html)
        
        if minimal_layout:
            header = ''
            footer = self.env[modelos_repetidos[0]].render_template("web.internal_layout", values=rcontext)
            spec_paperformat_args = {'data-report-margin-top': 10, 'data-report-header-spacing': 10}
            footer = self.env[modelos_repetidos[0]].render_template(modelos_repetidos[1], values=dict(rcontext, subst=True, body=footer))
        else:
            rcontext.update({
                    'css': '',
                    'o': self.env.user,
                    'res_company': self.env.company,
                })
#             header = self.env[modelos_repetidos[0]].render_template("jt_income.pdf_external_layout_income_annual_report_new", values=rcontext)
#             
#             header = header.decode('utf-8') # Ensure that headers and footer are correctly encoded
#             current_filtered = self.get_header_year_list(options)
#             header.replace("REAL", "Test")
            header='''<div class="header">
                        <div class="row">
                            <div class="col-12 text-center">
                                   <span style="font-size:16px;">DIRECCIÓN GENERAL DE FINANZAS</span><br/>
                                   <span style="font-size:14px;">DIRECCIÓN DE INGRESOS Y OPERACIÓN FINANCIERA</span><br/>
                                   <span style="font-size:12px;">DEPARTAMENTO DE INGRESOS</span><br/>
                                   <span style="font-size:12px;">INFORME ANUAL INGRESOS</span><br/>
                                   <span style="font-size:12px;">INGRESOS POR SERVICIOS DE EDUCACIÓN Y PATRIMONIALES</span><br/>
                                   <span style="font-size:12px;">%s</span><br/>
                                   <span style="font-size:12px;">REAL</span><br/>
                            </div>
                        </div>
                    </div>
                    <div class="article" data-oe-model="res.users" data-oe-id="2" data-oe-lang="en_US">
                      
                    </div>
                '''%(self.get_header_year_list(options))
            spec_paperformat_args = {}
            # Default header and footer in case the user customized web.external_layout and removed the header/footer
            headers = header.encode()
            footer = b''
            # parse header as new header contains header, body and footer
            try:
                root = lxml.html.fromstring(header)
                match_klass = "//div[contains(concat(' ', normalize-space(@class), ' '), ' {} ')]"

                for node in root.xpath(match_klass.format('header')):
                    headers = lxml.html.tostring(node)
                    headers = self.env[modelos_repetidos[0]].render_template(modelos_repetidos[1], values=dict(rcontext, subst=True, body=headers))

                for node in root.xpath(match_klass.format('footer')):
                    footer = lxml.html.tostring(node)
                    footer = self.env[modelos_repetidos[0]].render_template(modelos_repetidos[1], values=dict(rcontext, subst=True, body=footer))

            except lxml.etree.XMLSyntaxError:
                headers = header.encode()
                footer = b''
            header = headers

        landscape = False
        if len(self.with_context(print_mode=True).get_header(options)[-1]) > 5:
            landscape = True

        return self.env[modelos_repetidos[0]]._run_wkhtmltopdf(
            [body],
            header=header, footer=footer,
            landscape=landscape,
            specific_paperformat_args=spec_paperformat_args
        )

    def get_html(self, options, line_id=None, additional_context=None):
        '''
        return the html value of report, or html value of unfolded line
        * if line_id is set, the template used will be the line_template
        otherwise it uses the main_template. Reason is for efficiency, when unfolding a line in the report
        we don't want to reload all lines, just get the one we unfolded.
        '''
        # Check the security before updating the context to make sure the options are safe.
        self._check_report_security(options)

        # Prevent inconsistency between options and context.
        self = self.with_context(self._set_context(options))

        templates = self._get_templates()
        report_manager = self._get_report_manager(options)
        report = {'name': self._get_report_name(),
                'summary': report_manager.summary,
                'company_name': self.env.company.name,}
        #options.get('date',{}).update({'string':''}) 
        lines = self._get_lines(options, line_id=line_id)

        if options.get('hierarchy'):
            lines = self._create_hierarchy(lines, options)
        if options.get('selected_column'):
            lines = self._sort_lines(lines, options)

        footnotes_to_render = []
        if self.env.context.get('print_mode', False):
            # we are in print mode, so compute footnote number and include them in lines values, otherwise, let the js compute the number correctly as
            # we don't know all the visible lines.
            footnotes = dict([(str(f.line), f) for f in report_manager.footnotes_ids])
            number = 0
            for line in lines:
                f = footnotes.get(str(line.get('id')))
                if f:
                    number += 1
                    line['footnote'] = str(number)
                    footnotes_to_render.append({'id': f.id, 'number': number, 'text': f.text})

        rcontext = {'report': report,
                    'lines': {'columns_header': self.get_header(options), 'lines': lines},
                    'options': {},
                    'context': self.env.context,
                    'model': self,
                }
        if additional_context and type(additional_context) == dict:
            rcontext.update(additional_context)
        if self.env.context.get('analytic_account_ids'):
            rcontext['options']['analytic_account_ids'] = [
                {'id': acc.id, 'name': acc.name} for acc in self.env.context['analytic_account_ids']
            ]

        render_template = templates.get('main_template', 'account_reports.main_template')
        if line_id is not None:
            render_template = templates.get('line_template', 'account_reports.line_template')
        html = self.env['ir.ui.view'].render_template(
            render_template,
            values=dict(rcontext),
        )
        if self.env.context.get('print_mode', False):
            for k,v in self._replace_class().items():
                html = html.replace(k, v)
            # append footnote as well
            html = html.replace(b'<div class="js_account_report_footnotes"></div>', self.get_html_footnotes(footnotes_to_render))
        return html