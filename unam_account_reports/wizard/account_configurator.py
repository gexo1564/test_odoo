from odoo import api, fields, models, _


class AccountConfigurator(models.TransientModel):
    _name = "account.configurator"
    _description = 'Wizard to add accounts to annex'

    account_group_ids = fields.Many2many('account.group')

    def action_add_accounts(self):
        annex = self.env['account.financial.html.report.line'].browse(self._context.get('active_id'))
        accounts = self.env['account.account'].search([('group_id', 'in', self.account_group_ids.ids)])
        annex.write({'accounts_ids': [(4, account.id, 0) for account in accounts]})
