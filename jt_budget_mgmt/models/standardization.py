# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import base64
from calendar import month
import io
import logging
import math
from datetime import datetime, timedelta
from xlrd import open_workbook
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import re

"""Modelos"""
PROGRAM_CODE_MODEL = 'program.code'

STANDARDIZATION_LINE='standardization.line'
IR_ACTIONS_ACT_WINDOW = 'ir.actions.act_window'
IR_SEQUENCE_MODEL = 'ir.sequence'

MSG_CHANGE_OF_STATUS = _("You can only change the status of the rescheduling lines for confirmed rescheduling")
MSG_CHANGE_OF_STATUS_ES = "Solo puede cambiar el status de las líneas, cuando la recalendarización esta en confirmado."

class Standardization(models.Model):

    _name = 'standardization'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Re-standardization'
    _rec_name = 'folio'

    def _get_count(self):
        for record in self:
            record.record_number = len(record.line_ids)
            record.all_line_count = len(record.line_ids)
            record.imported_record_number = len(
                record.line_ids.filtered(lambda l: l.imported == True))
            record.draft_count = len(
                record.line_ids.filtered(lambda l: l.state == 'draft'))
            record.received_count = len(
                record.line_ids.filtered(lambda l: l.state == 'received'))
            record.in_process_count = len(
                record.line_ids.filtered(lambda l: l.state == 'in_process'))
            record.authorized_count = len(
                record.line_ids.filtered(lambda l: l.state == 'authorized'))
            record.cancelled_count = len(
                record.line_ids.filtered(lambda l: l.state == 'cancelled'))

    cron_running = fields.Boolean(string='Running CRON?')
    recalendaring_name = fields.Text(string='Recalendaring Name', readonly=True, tracking=True)
    fiscal_year = fields.Many2one("account.fiscal.year", string = "Fiscal Year", required=True)
    folio = fields.Char(string='Folio', states={
        'confirmed': [('readonly', True)], 'cancelled': [('readonly', True)]}, tracking=True)
    record_number = fields.Integer(
        string='Number of records', compute='_get_count')
    imported_record_number = fields.Integer(
        string='Number of imported records', compute='_get_count')
    observations = fields.Text(string='Observations', tracking=True)
    select_box = fields.Boolean(string='Select Box')
    line_ids = fields.One2many(
        STANDARDIZATION_LINE, 'standardization_id', string='Standardization lines', states={'cancelled': [('readonly', True)]})
    success_line_ids = fields.One2many(
        STANDARDIZATION_LINE, 'standardization_id', string='Standardization lines', domain=[('line_state','not in',('draft',))],states={'cancelled': [('readonly', True)]})

    import_line_ids = fields.One2many(
        STANDARDIZATION_LINE, 'standardization_id',domain=[('line_state','in',('draft','success'))] ,string='Standardization lines', states={'cancelled': [('readonly', True)]})

    state = fields.Selection(
        [
            ('draft', 'Draft'),
            ('confirmed', 'Confirmed'),
            ('accepted', 'Accepted'),
            ('cancelled', 'Cancelled')
        ], default='draft', required=True, string='State', tracking=True
    )

    # Counter fields for line stage
    draft_count = fields.Integer(string='Draft', compute='_get_count', track_visibility='onchange')
    received_count = fields.Integer(string='Received', compute='_get_count', track_visibility='onchange')
    in_process_count = fields.Integer(
        string='In process', compute='_get_count', track_visibility='onchange')
    authorized_count = fields.Integer(
        string='Authorized', compute='_get_count', track_visibility='onchange')
    cancelled_count = fields.Integer(string='Cancelled', compute='_get_count', track_visibility='onchange')
    all_line_count = fields.Integer(string='Cancelled', compute='_get_count')
    check_line_state = fields.Boolean(store=True)
    is_dgpo_authorization = fields.Boolean("DGPo authorization of games",copy=False,default=False)
    

    
    @api.model
    def fields_get(self, fields=None, attributes=None):
        
        no_selectable_fields = ['check_line_state', 'message_main_attachment_id' , 'filename', 'cron_running', 'select_box', 'allow_upload', 
                                'pointer_row', 'import_status', 'success_row_ids', 'message_needaction', 'activity_ids', 'activity_exception_decoration', 'failed_row_file', 'budget_file',
                                'check_line_state', 'message_is_follower', 'failed_row_ids', 'import_line_ids', 'line_ids', 'success_line_ids',
                                'message_has_error', 'message_has_sms_error', 'activity_date_deadline', 'activity_summary', 'activity_type_id', 'fialed_row_filename', 'message_channel_ids',
                                'message_follower_ids', 'message_partner_ids', 'activity_user_id']
        
        no_sortable_field = ['check_line_state', 'message_main_attachment_id' , 'filename', 'cron_running', 'select_box', 'allow_upload', 'pointer_row', 'import_status']

        res = super(Standardization, self).fields_get(fields, attributes=attributes)
        
        for key, value in res.items():
            if key in no_selectable_fields:
                value.update({'selectable': False})
        
        for key, value in res.items():
            if key in no_sortable_field:
                value.update({'sortable': False})
                
        return res

    def _month_to_quarter(self, month):
        return [1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4][month-1]

    #@api.depends('line_ids', 'line_ids.state')
    def check_line_state(self):
        for standardization in self:
            standardization.check_line_state = False
            lines = standardization.line_ids.filtered(
                lambda l: l.amount_effected == False and l.state == 'authorized')
            params = []
            query_available = """
                select available, write_date from expenditure_budget_line
                where program_code_id = %s and extract(quarter from start_date) = %s
                and budget_line_yearly_id is not null;
            """
            query_reschedule_inc = """
                update expenditure_budget_line
                    set available = coalesce(available,0)::numeric + %s::numeric,
                    write_date = now(),
                    write_uid = %s
                where program_code_id = %s and extract(quarter from start_date) = %s
                and budget_line_yearly_id is not null
                returning 1;
            """
            query_reschedule_dec = """
                update expenditure_budget_line
                    set available = coalesce(available,0)::numeric - %s::numeric,
                    write_date = now(),
                    write_uid = %s
                where program_code_id = %s and extract(quarter from start_date) = %s
                and budget_line_yearly_id is not null
                and (write_date = %s or available >= %s)
                returning 1;
            """
            query_trimestralidad_inc = """
                update control_assigned_amounts_lines
                    set available = coalesce(available,0)::numeric + %s::numeric,
                    write_date = now(),
                    write_uid = %s
                where program_code_id = %s and extract(quarter from start_date) = %s
                returning 1;
            """
            query_trimestralidad_dec = """
                update control_assigned_amounts_lines
                    set available = coalesce(available,0)::numeric - %s::numeric,
                    write_date = now(),
                    write_uid = %s
                where program_code_id = %s and extract(quarter from start_date) = %s
                returning 1;
            """
            if lines:
                logging.debug("Iniciando proceso de recalendarización %s." % self.folio)
                program_code_lines = {}
                # Primero se valida la suficiencia presupuestal, calculando el total
                # de disminuciones que se hacen a un mismo código en un mismo trimestre
                # Esto es, se hace una agrupación por código programático y trimestre
                # de origen y se calcula el total de reducciones.
                for line in lines:
                    if line.origin_id:
                        date_start = str(line.origin_id.start_date).split('/')
                        src_month = int(date_start[1])
                        src_quarter = self._month_to_quarter(src_month)
                        key = (line.code_id, src_quarter)
                        value = program_code_lines.get(key, 0)
                        program_code_lines[key] = value + line.amount
                # Se itera sobre cada combinación de códigos y trimestres de origen
                # y para comparar el monto requerido con el disponible. Si el disponible
                # es menor se lanza una un ValidationError
                for k,v in program_code_lines.items():
                    self._cr.execute(query_available, (k[0].id, k[1]))
                    res = self.env.cr.dictfetchall()
                    # Validar suficiencia
                    available = 0
                    if len(res) == 1:
                        available = res[0]['available']
                    if round(available, 2) < round(v, 2):
                        quarter =  "trimestre %s" % k[1]
                        raise ValidationError("Insuficiencia Presupuestal\r\nCódigo programático: %s.\r\nEl monto requerido es de $%s y el monto disponible en el %s es de $%s." % (
                                    k[0].program_code,
                                    format(v, ",.2f"),
                                    quarter,
                                    format(available, ",.2f"),
                            )
                        )
                # Pasada la validación presupuesta, se generan los parámetros de
                # las sentencias SQL para ejecutar la actualización del disponible.
                for line in lines:
                    if line.origin_id and line.quarter:
                        src_month = dst_month = None
                        date_start = str(line.origin_id.start_date).split('/')
                        if date_start:
                            src_month = int(date_start[1])
                        date_end = str(line.quarter.start_date).split('/')
                        if date_end:
                            dst_month = int(date_end[1])
                        src_quarter = self._month_to_quarter(src_month)
                        dst_quarter = self._month_to_quarter(dst_month)
                        params.append((line.amount, line.code_id, src_quarter, dst_quarter, res[0]['write_date']))
                        line.amount_effected = True
                ok = True
                # Ejecución del conjunto de Consultas SQL
                msg = ""
                for p in params:
                    # Ejecución de los SQL para modificar expenditure_budget
                    self.env.cr.execute(query_reschedule_dec, (p[0], self.env.user.id, p[1].id, p[2], p[4], p[0]))
                    res = self.env.cr.fetchone()
                    if not res:
                        ok = False
                        self._cr.execute(query_available, (p[1].id, p[2]))
                        res = self.env.cr.dictfetchall()
                        # Validar suficiencia
                        available = 0
                        if len(res) == 1:
                            available = res[0]['available']
                        msg = "Insuficiencia Presupuestal\r\nCódigo programático: %s.\r\nEl monto requerido es de $%s y el monto disponible es de $%s." % (
                                p[1].program_code,
                                format(p[0], ",.2f"), format(available, ",.2f"),
                        )
                        break
                    self.env.cr.execute(query_reschedule_inc, (p[0], self.env.user.id, p[1].id, p[3]))
                    res = self.env.cr.fetchone()
                    if not res:
                        ok = False
                        break
                    # Ejecución de los SQL para modificar las lineas de las trimestralidades
                    self.env.cr.execute(query_trimestralidad_dec, (p[0], self.env.user.id, p[1].id, p[2]))
                    res = self.env.cr.fetchone()
                    if not res:
                        ok = False
                        break
                    self.env.cr.execute(query_trimestralidad_inc, (p[0], self.env.user.id, p[1].id, p[3]))
                    res = self.env.cr.fetchone()
                    if not res:
                        ok = False
                        break
                if not ok:
                    try:
                        # Se provoca un excepción para deshacer los cambios
                        # y en el bloque finally actualizar el status de las lineas
                        raise ValidationError('')
                    except ValidationError:
                        # Al lanzar esta excepción se hace rollback
                        raise ValidationError("Error al ejecuatar la recalendarización.\r\n" + msg)
                    finally:
                        # Y finalmente se regresa el estado de las lineas a en proceso
                        for line in lines:
                            line.state = 'in_process'
                else:
                    if len(lines) == len(standardization.line_ids):
                        self.state = 'accepted'
                logging.debug("Proceso de recalendarización %s concluido." % self.folio)

    _sql_constraints = [
        ('folio_uniq_const', 'unique(folio)', 'The folio must be unique.')]

    @api.constrains('folio')
    def _check_folio(self):
        if not str(self.folio).isnumeric():
            if self.env.user.lang == 'es_MX':
                raise ValidationError('Folio Debe ser un valor numérico!')
            else:
                raise ValidationError('Folio Must be numeric value!')
        folio = self.search(
            [('id', '!=', self.id), ('folio', '=', self.folio)], limit=1)
        if folio:
            if self.env.user.lang == 'es_MX':
                raise ValidationError('El folio debe ser único!')
            else:
                raise ValidationError('Folio Must be unique!')  




    def _compute_total_rows(self):
        for record in self:
            record.failed_rows = self.env[STANDARDIZATION_LINE].search_count(
                [('standardization_id', '=', record.id), ('line_state', '=', 'fail')])
            record.success_rows = self.env[STANDARDIZATION_LINE].search_count(
                [('standardization_id', '=', record.id), ('line_state', '=', 'success')])
            record.total_rows = len(record.line_ids.filtered(lambda l: l.imported == True))

    # Import process related fields
    allow_upload = fields.Boolean(string='Allow Update XLS File?')
    budget_file = fields.Binary(string='Uploaded File', states={
        'confirmed': [('readonly', True)], 'cancelled': [('readonly', True)]})
    filename = fields.Char(string='File name')
    import_status = fields.Selection([
        ('draft', 'Draft'),
        ('in_progress', 'In Progress'),
        ('done', 'Completed')], default='draft', copy=False)
    failed_row_file = fields.Binary(string='Failed Rows File')
    fialed_row_filename = fields.Char(
        string='File name', default=lambda self: _("Failed_Rows.txt"))
    failed_rows = fields.Integer(
        string='Failed Rows',compute='_compute_total_rows')
    success_rows = fields.Integer(
        string='Success Rows', compute='_compute_total_rows')
    success_row_ids = fields.Text(
        string='Success Row Ids', default="[]", copy=False)
    failed_row_ids = fields.Text(
        string='Failed Row Ids', default="[]", copy=False)
    pointer_row = fields.Integer(
        string='Current Pointer Row', default=1, copy=False)
    total_rows = fields.Integer(string="Total Rows", copy=False,compute='_compute_total_rows')


    def check_program_item_games(self,program_code,item_name=False):
        if not item_name and program_code and program_code.item_id:
            item_name = program_code.item_id.item
        program_code_msg = program_code and program_code.program_code or ''
        user_lang = self.env.user.lang
        
        if item_name and not self.is_dgpo_authorization:
            if item_name >= '100' and item_name <= '199':
                if item_name not in ('186','187','191','154','196','197'):
                    if user_lang == 'es_MX':
                        raise ValidationError(
                            _("Del grupo de partida 100 solo se permiten registrar las partidas(186, 187, 191, 154, 196 y 197): \n %s" %
                              program_code_msg))
                    else:
                        raise ValidationError(_("Form the group 100,only the following games are allowed (186,187,191,154,196 and 197): \n %s" %
                                                program_code_msg))
                    
            if item_name >= '700' and item_name <= '799':
                if item_name == '711':
                    if user_lang == 'es_MX':
                        raise ValidationError(
                            _("Del grupo de partida 700, la partida 711 no está permitida: \n %s" %
                              program_code_msg))
                    else:
                        raise ValidationError(_("Form the group 700, only 711 game is not allowed: \n %s" %
                                                program_code_msg))

            if item_name >= '300' and item_name <= '399':
                if user_lang == 'es_MX':
                    raise ValidationError(
                        _("No se pueden crear recalendarizaciones de la partida 300: \n %s" %
                          program_code_msg))
                else:
                    raise ValidationError(_("Cannot create reschedule of party group 300: \n %s" %
                                            program_code_msg))

    def check_year_exist(self, line):

        if len(str(line.year)) > 3:
            year_str = str(line.year)[:4]
            if year_str.isnumeric():
                year_obj = self.env['year.configuration'].search_read([], fields=['id', 'name'])
                if not list(filter(lambda yr: yr['name']==year_str, year_obj)):
                    self.env['year.configuration'].create({'name': year_str}).id
        else:
            raise ValidationError('Invalid Year Format Of line one!')
        

    def get_last_fiscalyear_lock_date(self):
        last_fiscalyear_lock_date = None
        acld = self.env['account.change.lock.date'].search([])
        
        if acld:
            last_fiscalyear_lock_date = acld[-1].fiscalyear_lock_date
            
        return last_fiscalyear_lock_date


    def validate_and_add_budget_line(self, record_id=False, cron_id=False):
        last_fiscalyear_lock_date = self.get_last_fiscalyear_lock_date()
        fiscal_year = self.fiscal_year.name

        if record_id:
            self = self.env['standardization'].browse(int(record_id))
        lines_to_validate = self.line_ids.filtered(lambda x:x.line_state in ('draft','fail'))
        # When import file #
        if len(lines_to_validate) > 0 and self.budget_file:
            counter = 0
            failed_line_ids = []
            success_line_ids = []
            failed_row = ""
            self.check_year_exist(lines_to_validate[0])
            # Objects
            year_obj = self.env['year.configuration'].search_read([], fields=['id', 'name'])
            program_obj = self.env['program'].search_read(['&', ('program_key_id','!=',False), '&', ('start_date', '<=', datetime.now()), '|', ('end_date', '=', False), ('end_date', '>=', datetime.now())], fields=['id', 'key_unam','program_key_id'])
            subprogram_obj = self.env['sub.program'].search_read(['&', ('dependency_id','!=',False),('sub_dependency_id','!=',False),('unam_key_id','!=',False),('geographic_location_id','!=',False), '&', ('start_date', '<=', datetime.now()), '|', ('end_date', '=', False), ('end_date', '>=', datetime.now())], fields=['id','dependency_id','sub_dependency_id' ,'unam_key_id', 'sub_program', 'geographic_location_id'])
            dependancy_obj = self.env['dependency'].search_read(['&', ('start_date', '<=', datetime.now()), '|', ('end_date', '=', False), ('end_date', '>=', datetime.now())], fields=['id', 'dependency'])
            subdependancy_obj = self.env['sub.dependency'].search_read(['&', ('start_date', '<=', datetime.now()), '|', ('end_date', '=', False), ('end_date', '>=', datetime.now())],
                                                                       fields=['id', 'dependency_id', 'sub_dependency'])
            item_obj = self.env['expenditure.item'].search_read(['&', ('start_date', '<=', datetime.now()), '|', ('end_date', '=', False), ('end_date', '>=', datetime.now())], fields=['id', 'item', 'exercise_type'])
            origin_obj = self.env['resource.origin'].search_read(['&', ('start_date', '<=', datetime.now()), '|', ('end_date', '=', False), ('end_date', '>=', datetime.now())], fields=['id', 'key_origin'])
            activity_obj = self.env['institutional.activity'].search_read(['&', ('start_date', '<=', datetime.now()), '|', ('end_date', '=', False), ('end_date', '>=', datetime.now())], fields=['id', 'number'])
            
            shcp_obj = self.env['budget.program.conversion'].search_read(['&', ('shcp_name','!=',False),('program_key_id','!=',False),('conversion_key_id','!=',False), '&', ('start_date', '<=', datetime.now()), '|', ('end_date', '=', False), ('end_date', '>=', datetime.now())], fields=['id','program_key_id', 'shcp_name','conversion_key_id','federal_part'])
            dpc_obj = self.env['departure.conversion'].search_read(['&', ('item_id','!=',False), '&', ('start_date', '<=', datetime.now()), '|', ('end_date', '=', False), ('end_date', '>=', datetime.now())], fields=['id', 'federal_part','item_id'])
            expense_type_obj = self.env['expense.type'].search_read(['&', ('start_date', '<=', datetime.now()), '|', ('end_date', '=', False), ('end_date', '>=', datetime.now())], fields=['id', 'key_expenditure_type'])
            location_obj = self.env['geographic.location'].search_read(['&', ('start_date', '<=', datetime.now()), '|', ('end_date', '=', False), ('end_date', '>=', datetime.now())], fields=['id', 'state_key'])
            wallet_obj = self.env['key.wallet'].search_read(['&', ('start_date', '<=', datetime.now()), '|', ('end_date', '=', False), ('end_date', '>=', datetime.now())], fields=['id', 'wallet_password'])
            project_type_obj = self.env['project.type'].search_read([],
                                                                    fields=['id', 'project_type_identifier', 'number'])
            stage_obj = self.env['stage'].search_read([], fields=['id', 'stage_identifier'])
            agreement_type_obj = self.env['agreement.type'].search_read([], fields=['id', 'agreement_type',
                                                                                    'number_agreement'])
            
            quarter_budget_obj = self.env['quarter.budget'].search_read([], fields=['id', 'name'])
            program_code_obj = self.env[PROGRAM_CODE_MODEL]
#             cron = False
#             if cron_id:
#                 cron = self.env['ir.cron'].sudo().browse(int(cron_id))

            budget_obj = self.env['expenditure.budget'].sudo(
            ).with_context(from_adequacies=True)

            # If user re-scan for failed rows
            #re_scan_failed_rows_ids = eval(self.failed_lline.id)
            lines_to_execute = lines_to_validate
            for line in lines_to_execute:
                counter += 1
                line_vals = [line.year, line.program, line.subprogram, line.dependency, line.subdependency, line.item,
                             line.dv, line.origin_resource, line.ai, line.conversion_program,
                             line.departure_conversion, line.expense_type, line.location, line.portfolio,
                             line.project_type, line.project_number, line.stage, line.agreement_type,
                             line.agreement_number, line.exercise_type,line.folio,line.budget,line.origin,line.quarter_data]
                
                program_code_str = "".join(line_vals[0:19])
                program_code = self.env[PROGRAM_CODE_MODEL].sudo().search(
                    [('program_code', '=', program_code_str)],
                    limit=1
                )
                # Validate year format
                if len(str(line.year)) > 3:
                    year_str = str(line.year)[:4]
                    if year_str.isnumeric():
                        year = list(filter(lambda yr: yr['name'] == year_str, year_obj))
                        if year:
                            year = year[0]['id']
                        else:
                            if not self._context.get('from_adequacies'):
                                year = self.env['year.configuration'].create({'name': year_str}).id
                    if not year:
                        failed_row += str(line_vals) + \
                            _("------>> Invalid Year Format\n")
                        failed_line_ids.append(line.id)
                        continue

                # Validate Program(PR)
                program = False
                program_key_id = False
                if len(str(line.program)) > 1:
                    program_str = str(line.program).zfill(2)
                    if program_str.isnumeric():
                        program = list(filter(lambda prog: prog['key_unam'] == program_str, program_obj))
                        program_key_id = program[0]['program_key_id'][0] if program else False
                        program = program[0]['id'] if program else False
                if not program:
                    failed_row += str(line_vals) + \
                        _("------>> Invalid Program(PR) Format, check the validity\n")
                    failed_line_ids.append(line.id)
                    continue

                # Validation Conversion Program SHCP
                shcp = False
                conversion_item = False
                
                departure_conversion_str = False
                if len(str(line.departure_conversion)) > 4:
                    departure_conversion_str = str(line.departure_conversion).zfill(4)
                    
                program_str = False
                if len(str(line.program)) > 1:
                    program_str = str(line.program).zfill(2)
                    
                if len(str(line.conversion_program)) > 3:
                    shcp_str = str(line.conversion_program)
                    if len(shcp_str) == 4 and (re.match("[A-Z]{1}\d{3}", str(shcp_str).upper())):
                        shcp = list(
                            filter(lambda tmp: tmp['shcp_name'] == shcp_str and tmp['program_key_id'][0] == program_key_id and tmp['federal_part']==departure_conversion_str,
                                shcp_obj))
                        shcp = shcp[0]['id'] if shcp else False
                        
                if not shcp:
                    failed_row += str(line_vals) + \
                                _("------>> Invalid Conversion Program SHCP(CONPP) Format, check the validity\n")
                    failed_line_ids.append(line.id)
                    continue


                # Validate Dependency
                dependency = False
                if len(str(line.dependency)) > 2:
                    dependency_str = str(line.dependency).zfill(3)
                    if dependency_str.isnumeric():
                        dependency = list(filter(lambda dep: dep['dependency'] == dependency_str, dependancy_obj))
                        dependency = dependency[0]['id'] if dependency else False
                if not dependency:
                    failed_row += str(line_vals) + \
                        _("------>> Invalid Dependency(DEP) Format, check the validity\n")
                    failed_line_ids.append(line.id)
                    continue

                # Validate Sub-Dependency
                subdependency = False
                subdependency_str = str(line.subdependency).zfill(2)
                if subdependency_str.isnumeric():
                    subdependency = list(filter(
                        lambda sdo: sdo['sub_dependency'] == subdependency_str and sdo['dependency_id'][
                            0] == dependency, subdependancy_obj))
                    subdependency = subdependency[0]['id'] if subdependency else False
                if not subdependency:
                    failed_row += str(line_vals) + \
                        _("------>> Invalid Sub Dependency(SUB-DEP) Format, check the validity\n")
                    failed_line_ids.append(line.id)
                    continue

                # Validate Sub-Program
                geo_location = False
                if len(str(line.location)) > 1:
                    location_str = str(line.location).zfill(2)
                    if location_str.isnumeric():
                        geo_location = list(filter(lambda geol: geol['state_key'] == location_str, location_obj))
                        geo_location = geo_location[0]['id'] if geo_location else False
                subprogram = False
                if len(str(line.subprogram)) > 1:
                    subprogram_str = str(line.subprogram).zfill(2)
                    if subprogram_str.isnumeric():
                        subprogram = list(filter(lambda subp: subp['sub_program'] == subprogram_str and subp['unam_key_id'][0] == program and subp['dependency_id'][0] == dependency and subp['sub_dependency_id'][0] == subdependency, subprogram_obj))
                        subprogram = subprogram[0]['id'] if subprogram else False
                if not subprogram:
                    failed_row += str(line_vals) + \
                        _("------>> Invalid SubProgram(SP) Format, check the validity\n")
                    failed_line_ids.append(line.id)
                    continue
                
                #Validate item games group
                if len(str(line.item)) > 2 and not self.is_dgpo_authorization:
                    user_lang = self.env.user.lang
                    item_name =  str(line.item).zfill(3) 
                    if item_name >= '100' and item_name <= '199':
                        if item_name not in ('180','191','154','196','197'):
                            if user_lang == 'es_MX':
                                failed_row += str(line_vals) + \
                                            "------>> Del grupo de partida 100 solo se permiten registrar las partidas(180,191, 154, 196 y 197)\n"
                                failed_line_ids.append(line.id)
                                continue
                            else:
                                failed_row += str(line_vals) + \
                                            "------>> Form the group 100,only the following games are allowed (180,191,154,197 and 197):\n"
                                failed_line_ids.append(line.id)
                                continue
                                                            
                    elif item_name >= '700' and item_name <= '799':
                        if item_name == '711':                        
                            if user_lang == 'es_MX':
                                failed_row += str(line_vals) + \
                                            "------>> Forma el grupo 700, solo el Partida 711 no está permitido\n"
                                failed_line_ids.append(line.id)
                                continue
                            else:
                                failed_row += str(line_vals) + \
                                            "------>> Form the group 700, only 711 game is not allowed:\n"
                                failed_line_ids.append(line.id)
                                continue
        
                    elif item_name >= '300' and item_name <= '399':
                        if user_lang == 'es_MX':
                            failed_row += str(line_vals) + \
                                        "------>> No se pueden crear recalendarizaciones de la partida 300\n"
                            failed_line_ids.append(line.id)
                            continue
                        else:
                            failed_row += str(line_vals) + \
                                        "------>> Cannot create reschedule of party group 300:\n"
                            failed_line_ids.append(line.id)
                            continue
                    
                # Validate Item
                item = False
                if len(str(line.item)) > 2:
                    item_string = str(line.item).zfill(3)
                    typee = str(line.exercise_type).lower()
                    if typee not in ['r', 'c', 'd']:
                        typee = 'r'
                    if item_string.isnumeric():
                        item = list(filter(lambda itm: itm['item'] == item_string and itm['exercise_type'] == typee,
                                        item_obj))
                        if not item:
                            item = list(filter(lambda itm: itm['item'] == item_string, item_obj))
                        if item:
                            item = item[0]['id']
           
                if not item:
                    failed_row += str(line_vals) + \
                        _("------>> Invalid Expense Item(PAR) Format, check the validity\n")
                    failed_line_ids.append(line.id)
                    continue

                if not line.dv:
                    failed_row += str(line_vals) + \
                        _("------>> Digito Verificador is not added!\n")
                    failed_line_ids.append(line.id)
                    continue

                origin_resource = False
                if len(str(line.origin_resource)) > 0:
                    origin_resource_str = str(line.origin_resource).replace('.', '').zfill(2)
                    if origin_resource_str.isnumeric():
                        origin_resource = list(
                            filter(lambda ores: ores['key_origin'] == origin_resource_str, origin_obj))
                        origin_resource = origin_resource[0]['id'] if origin_resource else False
                if not origin_resource:
                    failed_row += str(line_vals) + \
                        _("------>> Invalid Origin Of Resource(OR) Format, check the validity\n")
                    failed_line_ids.append(line.id)
                    continue

                # Validation Institutional Activity Number
                institutional_activity = False
                if len(str(line.ai)) > 2:
                    institutional_activity_str = str(line.ai).zfill(5)
                    if institutional_activity_str.isnumeric():
                        institutional_activity = list(
                            filter(lambda inact: inact['number'] == institutional_activity_str, activity_obj))
                        institutional_activity = institutional_activity[0][
                            'id'] if institutional_activity else False
                if not institutional_activity:
                    failed_row += str(line_vals) + \
                        _("------>> Invalid Institutional Activity Number(AI) Format, check the validity\n")
                    failed_line_ids.append(line.id)
                    continue

                # Validation Federal Item
                conversion_item = False
                if len(str(line.departure_conversion)) > 4:
                    conversion_item_str = str(line.departure_conversion).zfill(4)
                    if conversion_item_str.isnumeric():
                        conversion_item = list(filter(lambda coit: coit['federal_part'] == conversion_item_str and coit['item_id'][0]==item, dpc_obj))
                        conversion_item = conversion_item[0]['id'] if conversion_item else False
                if not conversion_item:
                    failed_row += str(line_vals) + \
                        _("------>> Invalid SHCP Games(CONPA) Format, check the validity\n")
                    failed_line_ids.append(line.id)
                    continue

                # Validation Expense Type
                expense_type = False
                if len(str(line.expense_type)) > 1:
                    expense_type_str = str(line.expense_type).zfill(2)
                    if expense_type_str.isnumeric():
                        expense_type = list(
                            filter(lambda exty: exty['key_expenditure_type'] == expense_type_str, expense_type_obj))
                        expense_type = expense_type[0]['id'] if expense_type else False
                if not expense_type:
                    failed_row += str(line_vals) + \
                        _("------>> Invalid Expense Type(TG) Format, check the validity\n")
                    failed_line_ids.append(line.id)
                    continue

                # Validation Geographic Location
                geo_location = False
                if len(str(line.location)) > 1:
                    location_str = str(line.location).zfill(2)
                    if location_str.isnumeric():
                        geo_location = list(filter(lambda geol: geol['state_key'] == location_str, location_obj))
                        geo_location = geo_location[0]['id'] if geo_location else False
                if not geo_location:
                    failed_row += str(line_vals) + \
                        _("------>> Invalid Geographic Location (UG) Format, check the validity\n")
                    failed_line_ids.append(line.id)
                    continue

                # Validation Wallet Key
                wallet_key = False
                if len(str(line.portfolio)) > 3:
                    wallet_key_str = str(line.portfolio).zfill(4)
                    if wallet_key_str.isnumeric():
                        wallet_key = list(
                            filter(lambda wlke: wlke['wallet_password'] == wallet_key_str, wallet_obj))
                        wallet_key = wallet_key[0]['id'] if wallet_key else False
                if not wallet_key:
                    failed_row += str(line_vals) + \
                        _("------>> Invalid Wallet Key(CC) Format, check the validity\n")
                    failed_line_ids.append(line.id)
                    continue

                # Validaciones de catálogos compuestos #
                failed_row, failed_line_ids = self.env['adequacies'].check_composite_catalogs_program_code(program_code_obj, line_vals, failed_row, failed_line_ids, line.id)

                # Nuevas consultas
                # Catalogo de tipos de proyectos
                project_type_custom = self.env['project.type.custom'].sudo().search(
                    [('name', '=', line.project_type)],
                    limit=1
                )
                if not project_type_custom:
                    failed_row += str(line_vals) + \
                                _("------>> Invalid Project Type(TP) Format, check the validity\n")
                    failed_line_ids.append(line.id)
                    continue

                custom_stage = self.env['project.custom.stage'].sudo().search(
                    [('name', '=', line.stage)], limit=1
                )
                if not custom_stage:
                    failed_row += str(line_vals) + \
                                "------>> Invalid Stage\n"
                    failed_line_ids.append(line.id)
                    continue

                project = self.env['project.project'].sudo().search(
                    [
                        ('number', '=', line.project_number),
                        ('custom_project_type_id', '=', project_type_custom.id),
                        ('custom_stage_id', '=', custom_stage.id)
                    ],
                    limit=1
                )
                if not project:
                    failed_row += str(line_vals) + \
                                _("------>> Invalid Project\n")
                    failed_line_ids.append(line.id)
                    continue

                # Tipo de Proyecto
                project_type = False
                project_type = self.env['project.type'].sudo().search(
                    [('project_id', '=', project.id)],
                    limit=1
                )
                if not project_type:
                    failed_row += str(line_vals) + \
                                _("------>> Invalid Project Type(TP) Format, check the validity\n")
                    failed_line_ids.append(line.id)
                    continue
                # Etapa
                stage = self.env['stage'].sudo().search(
                    [('project_id', '=', project.id)],
                    limit=1
                )
                if not stage:
                    failed_row += str(line_vals) + \
                                _("------>> Invalid Stage(E) Format\n")
                    failed_line_ids.append(line.id)
                    continue
                # Acuerdo
                agreement_type = self.env['agreement.type'].sudo().search(
                    [('project_id', '=', project.id)],
                    limit=1
                )
                if not agreement_type:
                    failed_row += str(line_vals) + \
                                _("------>> Invalid Agreement Type(TC) Format, check the validity\n")
                    failed_line_ids.append(line.id)
                    continue

                # Validation Amount
                asigned_amount = 0
                try:
                    asigned_amount = float(line.amount)
                    if asigned_amount < 0:
                        failed_row += str(line_vals) + \
                            _("------>> Amount should be greater than 0\n")
                        failed_line_ids.append(line.id)
                        continue
                except:
                    failed_row += str(line_vals) + \
                        _("------>> Invalid Amount Format")
                    failed_line_ids.append(line.id)
                    continue

                # Validation Folio
                folio = line.folio
                if folio:
                    try:
                        folio = int(float(folio))
                    except:
                        failed_row += str(line_vals) + \
                            _("------>> Folio Must Be Numeric")
                        failed_line_ids.append(line.id)
                        continue
                    line_standardization = self.env[STANDARDIZATION_LINE].search(
                        [('folio', '=', str(folio)),('id','!=',line.id)], limit=1)
                    if line_standardization:
                        failed_row += str(line_vals) + \
                            _("------>> Folio Must Be Unique")
                        failed_line_ids.append(line.id)
                        continue
                else:
                    failed_row += str(line_vals) + \
                        _("------>> Invalid Folio Format")
                    failed_line_ids.append(line.id)
                    continue

                # Validation Budget
                budget_str = line.budget
                budget = budget_obj.search(
                    [('name', '=', budget_str)], limit=1)
                if not budget:
                    failed_row += str(line_vals) + \
                        _("------>> Budget Not Found")
                    failed_line_ids.append(line.id)
                    continue

                # Validate Origin
                origin = False
                if line.origin:
                    origin = list(
                                filter(lambda wlke: wlke['name'] == line.origin, quarter_budget_obj))
                    origin = origin[0]['id'] if origin else False

                if not origin:
                    failed_row += str(line_vals) + \
                        _("------>> Origin Not Found\n")
                    failed_line_ids.append(line.id)
                    continue

                quarter_data = False
                if line.quarter_data:
                    quarter_data = list(
                                filter(lambda wlke: wlke['name'] == line.quarter_data, quarter_budget_obj))
                    quarter_data = quarter_data[0]['id'] if quarter_data else False

                if not quarter_data:
                    failed_row += str(line_vals) + \
                        _("------>> Quarter Not Found\n")
                    failed_line_ids.append(line.id)
                    continue

                # Check if last_fiscalyear_lock_date is within the range of any quarter #
                if last_fiscalyear_lock_date:
                    quarters = {
                        "Trimestre-1": (f"{fiscal_year}-01-01", f"{fiscal_year}-03-31"),
                        "Trimestre-2": (f"{fiscal_year}-04-01", f"{fiscal_year}-06-30"),
                        "Trimestre-3": (f"{fiscal_year}-07-01", f"{fiscal_year}-09-30"),
                        "Trimestre-4": (f"{fiscal_year}-10-01", f"{fiscal_year}-12-31")
                    }

                    for quarter, (start_date, end_date) in quarters.items(): 
                        if (
                            (line.origin and line.origin == quarter and start_date <= str(last_fiscalyear_lock_date)) or
                            (line.quarter_data and line.quarter_data == quarter and start_date <= str(last_fiscalyear_lock_date))
                        ):
                            failed_row += str(line_vals) + _("------>> The movement cannot be executed because it is within the blocking date.\n")
                            failed_line_ids.append(line.id)

                                
                self.check_program_item_games(program_code,False)
                budget_line = self.env['expenditure.budget.line'].sudo().search(
                    [('program_code_id', '=', program_code.id), ('expenditure_budget_id', '=', budget.id)], limit=1)
                if not budget_line:
                    failed_row += str(line_vals) + \
                        _("------>> Budget line not found for selected program code!")
                    failed_line_ids.append(line.id)
                    continue

                pc = program_code
                dv_obj = self.env['verifying.digit']
                if pc.program_id and pc.sub_program_id and pc.dependency_id and \
                        pc.sub_dependency_id and pc.item_id:
                    vd = dv_obj.check_digit_from_codes(
                        pc.program_id, pc.sub_program_id, pc.dependency_id, pc.sub_dependency_id,
                        pc.item_id)
                    if vd and line.dv:
                        line_dv = line.dv
                        if len(line.dv) == 1:
                            line_dv = '0' + line.dv
                        if vd != line_dv:
                            failed_row += str(line_vals) + \
                                        _("------>> Digito Verificador is not matched! \n")
                            failed_line_ids.append(line.id)
                            continue
                success_line_ids.append(line.id)

#                if self._context.get('re_scan_failed'):
#                    failed_line_ids_eval_refill = eval(self.failed_lline.id)
#                    failed_line_ids_eval_refill.remove(line.id)
#                    self.write({'failed_line_idline.id(
#                        list(set(failed_line_ids_eval_refline.id)
                line.write({
                    #'folio': folio,
                    'code_id': program_code.id,
                    'budget_id': budget.id,
                    #'amount': amount,
                    'origin_id': origin,
                    'quarter': quarter_data,
                    'imported': True,
                })
                

            success_lines = self.env[STANDARDIZATION_LINE].browse(success_line_ids)
            success_lines.write({'line_state': 'success'})
            failed_lines = self.env[STANDARDIZATION_LINE].browse(failed_line_ids)
            failed_lines.write({'line_state': 'fail'})

            if self.failed_rows == 0:
                self.import_status = 'done'

            vals = {}
            failed_data = False
            
            self.create_failed_row_file(failed_row, self.budget_file, vals)

            if vals:
                self.write(vals)
                return self.wizard_error_message(self.budget_file)
            
        # When manual #    
        else:
            self.validate_catalog_validity()       


    def wizard_error_message(self, imported_file):
        message_content = _('Se detectaron errores, revise la pestaña "Estado de recalendarización" en el apartado Filas Erroneas Encontradas.')

        if imported_file:
            message_content = _('Se detectaron errores en la importación, revise la pestaña "Estado de recalendarización" en el apartado Filas Erroneas Encontradas.')
        
        return {
                'name': _('Error al validar el archivo'),
                'type': IR_ACTIONS_ACT_WINDOW,
                'res_model': 'wizard.for.warnings.budget',
                'view_mode': 'form',
                'view_type': 'form',
                'context': {
                    'default_message': message_content
                },
                'views': [(False, 'form')],
                'target': 'new'
                }


    def check_movement_execution(self, line_id, line_origin, line_quarter_data ,fiscal_year, last_fiscalyear_lock_date, line_vals, failed_rows, failed_line_ids):   
        quarters = {
            "Trimestre-1": (f"{fiscal_year}-01-01", f"{fiscal_year}-03-31"),
            "Trimestre-2": (f"{fiscal_year}-04-01", f"{fiscal_year}-06-30"),
            "Trimestre-3": (f"{fiscal_year}-07-01", f"{fiscal_year}-09-30"),
            "Trimestre-4": (f"{fiscal_year}-10-01", f"{fiscal_year}-12-31")
        }

        for quarter, (start_date, end_date) in quarters.items():
            if (
                (line_origin and line_origin == quarter and start_date <= str(last_fiscalyear_lock_date)) or
                (line_quarter_data and line_quarter_data == quarter and start_date <= str(last_fiscalyear_lock_date))
            ):
                failed_rows = str(line_vals) + _("------>> The movement cannot be executed because it is within the blocking date.\n")
                failed_line_ids.append(line_id)
                

        return failed_rows, failed_line_ids


    def remove_cron_records(self):
        crons = self.env['ir.cron'].sudo().search([('model_id', '=', self.env.ref('jt_budget_mgmt.model_standardization').id)])
        for cron in crons:
            if cron.standardization_id and not cron.standardization_id.cron_running:
                try:
                    cron.sudo().unlink()
                except Exception as e:
                    raise ValidationError(e)

    def send_notification_msg(self, user, failed, successed):
        ch_obj = self.env['mail.channel']
        base_user = self.env.ref('base.user_root')
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        url = base_url + '/web#id=%s&view_type=form&model=standardization' % (self.id)
        body = (_("Re-standardization Validation Process is Completed for \
                    <a href='%s' target='new'>%s</a>") % (url, self.folio))
        body += (_("<ul><li>Total Successed Lines: %s </li>\
            <li>Total Failed Lines: %s </li></ul>") % (str(successed), str(failed)))
        if user:
            ch = ch_obj.sudo().search([('name', '=', str(base_user.name + ', ' + user.name)),
                                       ('channel_type', '=', 'chat')], limit=1)
            if not ch:
                ch = ch_obj.create({
                    'name': 'OdooBot, ' + user.name,
                    'public': 'private',
                    'channel_type': 'chat',
                    'channel_last_seen_partner_ids': [(0, 0, {'partner_id': user.partner_id.id,
                                                              'partner_email': user.partner_id.email}),
                                                      (0, 0, {'partner_id': base_user.partner_id.id,
                                                              'partner_email': base_user.partner_id.email})
                                                      ]
                })
            ch.message_post(attachment_ids=[], body=body, content_subtype='html',
                            message_type='comment', partner_ids=[], subtype='mail.mt_comment',
                            email_from=base_user.partner_id.email, author_id=base_user.partner_id.id)
        return True

    def validate_draft_lines(self):
        
        if self.line_ids.filtered(lambda x:x.line_state in ('draft','fail')):
            res = self.validate_and_add_budget_line()
            if res:
                return res
        if self.failed_rows ==0:
            self.import_status = 'done'
            # Total CRON to create
#             data = base64.decodestring(self.budget_file)
#             book = open_workbook(file_contents=data or b'')
#             sheet = book.sheet_by_index(0)
#             total_sheet_rows = sheet.nrows - 1
#             total_cron = math.ceil(total_sheet_rows / 5000)
#             msg = (_("Re-standardization Validation Process Started at %s" % datetime.strftime(
#                 datetime.now(), DEFAULT_SERVER_DATETIME_FORMAT)))
#             self.env['mail.message'].create({'model': 'standardization', 'res_id': self.id,
#                                              'body': msg})
#             if total_cron == 1:
#                 self.validate_and_add_budget_line()
#                 msg = (_("Re-standardization Validation Process Ended at %s" % datetime.strftime(
#                     datetime.now(), DEFAULT_SERVER_DATETIME_FORMAT)))
#                 self.env['mail.message'].create({'model': 'standardization', 'res_id': self.id,
#                                                  'body': msg})
#             else:
#                 self.write({'cron_running': True})
#                 prev_cron_id = False
#                 for seq in range(1, total_cron + 1):
#                     # Create CRON JOB
#                     cron_name = str(self.folio).replace(' ', '') + "_" + str(datetime.now()).replace(' ', '')
#                     nextcall = datetime.now()
#                     nextcall = nextcall + timedelta(seconds=10)
# 
#                     cron_vals = {
#                         'name': cron_name,
#                         'state': 'code',
#                         'nextcall': nextcall,
#                         'nextcall_copy': nextcall,
#                         'numbercall': -1,
#                         'code': "model.validate_and_add_budget_line()",
#                         'model_id': self.env.ref('jt_budget_mgmt.model_standardization').id,
#                         'user_id': self.env.user.id,
#                         'standardization_id': self.id
#                     }
# 
#                     # Final process
#                     cron = self.env['ir.cron'].sudo().create(cron_vals)
#                     cron.write({'code': "model.validate_and_add_budget_line(" + str(self.id) + "," + str(cron.id) + ")"})
#                     if prev_cron_id:
#                         cron.write({'prev_cron_id': prev_cron_id, 'active': False})
#                     prev_cron_id = cron.id

    def validate_data(self):
        user_lang = self.env.user.lang
        if len(self.line_ids.ids) == 0:
            raise ValidationError(_("Please Add Standardization Lines"))

        lines = self.line_ids.filtered(lambda l: l.amount == 0)
        if lines:
            raise ValidationError(_("Amount must be greater than 0"))

        if self.total_rows > 0 and self.success_rows != self.total_rows:
            raise ValidationError(
                "Total imported rows not matched with total standardization lines!")

        bugdet_l_obj = self.env['expenditure.budget.line']
        program_code_lines = {}
        for line in self.line_ids:
#            if not line.selected:
#                continue
            if line.code_id:
                self.check_program_item_games(line.code_id)
            if line.amount and line.origin_id and line.code_id and line.budget_id:
                budget_lines = bugdet_l_obj.search([('expenditure_budget_id', '=', line.budget_id.id),
                                                    ('program_code_id', '=', line.code_id.id)])
                origin_start_date_day = False
                origin_start_date_month = False
                origin_end_date_day = False
                origin_end_date_month = False

                dest_start_date_day = False
                dest_start_date_month = False
                dest_end_date_day = False
                dest_end_date_month = False

                date_start = str(line.origin_id.start_date).split('/')
                if len(date_start) > 1:
                    origin_start_date_day = date_start[0]
                    origin_start_date_month = date_start[1]
                date_end = str(line.origin_id.end_date).split('/')
                if len(date_end) > 1:
                    origin_end_date_day = date_end[0]
                    origin_end_date_month = date_end[1]
                date_start = str(line.quarter.start_date).split('/')
                if len(date_start) > 1:
                    dest_start_date_day = date_start[0]
                    dest_start_date_month = date_start[1]
                date_end = str(line.quarter.end_date).split('/')
                if len(date_end) > 1:
                    dest_end_date_day = date_end[0]
                    dest_end_date_month = date_end[1]
                origin_budget_line = False
                dest_budget_line = False
                for budget_line in budget_lines:
                    if not origin_budget_line:
                        if budget_line.start_date and str(budget_line.start_date.day).zfill(
                                2) == origin_start_date_day and str(budget_line.start_date.month).zfill(
                                2) == origin_start_date_month and budget_line.end_date and str(
                                budget_line.end_date.day).zfill(2) == origin_end_date_day and str(
                                budget_line.end_date.month).zfill(2) == origin_end_date_month:
                            origin_budget_line = budget_line
                    if not dest_budget_line:
                        if budget_line.start_date and str(budget_line.start_date.day).zfill(
                                2) == dest_start_date_day and str(budget_line.start_date.month).zfill(
                                2) == dest_start_date_month and budget_line.end_date and str(
                                budget_line.end_date.day).zfill(2) == dest_end_date_day and str(
                                budget_line.end_date.month).zfill(2) == dest_end_date_month:
                            dest_budget_line = budget_line
                if not dest_budget_line:
                    if user_lang == 'es_MX':
                        raise ValidationError(
                            _("La línea presupuestaria trimestral no se crea para este código de programa: \n %s" %
                            line.code_id.program_code))
                    else:
                        raise ValidationError(_("Quarter budget line is not created for this program code: \n %s" %
                                                line.code_id.program_code))
                if not origin_budget_line:
                    if user_lang == 'es_MX':
                        raise ValidationError(_("La línea presupuestaria Origen no se crea para este código de "
                                                "programa: \n %s" % line.code_id.program_code))
                    raise ValidationError(_("Origin budget line is not created for this program code: \n %s" %
                                            line.code_id.program_code))
                program_code_lines[origin_budget_line] = round(program_code_lines.get(origin_budget_line, 0) + line.amount, 2)
        for k,v in program_code_lines.items():
            if v > k.available:
                quarter =  "trimestre %s" % self.get_quarter_from_date(k.start_date)
                raise ValidationError("Insuficiencia Presupuestal\r\nCódigo programático: %s.\r\nEl monto requerido es de $%s y el monto disponible en el %s es de $%s." % (
                            k.program_code_id.program_code,
                            format(v, ",.2f"),
                            quarter,
                            format(k.available, ",.2f"),
                    )
                )

    def get_quarter_from_date(self, date):
        return self._month_to_quarter(date.month)

    def confirm(self):
        self.validate_catalog_validity()
        self.validate_data()
        if self.failed_rows == 0:
            self.state = 'confirmed'
            self.line_ids.write({'state': 'draft'})


    def validate_catalog_validity(self):
        program_code_obj = self.env[PROGRAM_CODE_MODEL]
        # Solo considera solo el año del presupuesto que se va afectar
        year_obj = self.env['year.configuration'].search_read(
                [('name', '=', self.fiscal_year.name)],
                fields=['id', 'name'],
                limit=1
        )
        today = datetime.now()
        program_obj = self.env['program'].search_read(
            [
            '&',('program_key_id','!=',False),
                '&', ('start_date', '<=', today),
                    '|', ('end_date', '=', False), ('end_date', '>=', today)

            ], 
            fields=['id', 'key_unam']
        )
        program_dict = {p['key_unam']: p['id'] for p in program_obj}

        item_obj = self.env['expenditure.item'].search_read(
            [
            '&', ('start_date', '<=', today),
                    '|', ('end_date', '=', False), ('end_date', '>=', today) 
            ], 
            fields=['id', 'item', 'exercise_type'])
        item_dict = {i['item']: i['id'] for i in item_obj}
        dv_obj = self.env['verifying.digit']

        dependency_obj = self.env['dependency'].search_read([
            '&', ('start_date', '<=', today),
                    '|', ('end_date', '=', False), ('end_date', '>=', today) 
            ], 
            fields=['id', 'dependency'])
        dependency_dict = {d['dependency']: d['id'] for d in dependency_obj}

        sub_dependency_obj = self.env['sub.dependency'].search_read(
            [
            '&', ('start_date', '<=', today),
                    '|', ('end_date', '=', False), ('end_date', '>=', today) 
            ],
            fields=['id', 'dependency_id', 'sub_dependency']
        )
        sub_dependency_dict = {(d['dependency_id'][0], d['sub_dependency']): d['id'] for d in sub_dependency_obj}

        sub_program_obj = self.env['sub.program'].search_read(
            [
            ('dependency_id', '!=', False),
            ('sub_dependency_id', '!=', False),
            ('unam_key_id', '!=', False),
            '&',
            ('start_date', '<=', today),
            '|',
            ('end_date', '=', False),
            ('end_date', '>=', today)
            ],
            fields=['id', 'dependency_id', 'sub_dependency_id', 'unam_key_id', 'sub_program']
        )
        sub_program_dict = {(s['unam_key_id'][0], s['sub_program'], s['dependency_id'][0], s['sub_dependency_id'][0]) : s['id'] for s in sub_program_obj}

        origin_obj = self.env['resource.origin'].search_read(
            [
            '&', ('start_date', '<=', today),
                    '|', ('end_date', '=', False), ('end_date', '>=', today) 
            ], fields=['id', 'key_origin'])
        origin_dict = {o['key_origin']: o['id'] for o in origin_obj}

        activity_obj = self.env['institutional.activity'].search_read(
            [
            '&', ('start_date', '<=', today),
                    '|', ('end_date', '=', False), ('end_date', '>=', today) 
            ], 
            fields=['id', 'number'])
        activity_dict = {a['number']: a['id'] for a in activity_obj}

        conpa_obj = self.env['departure.conversion'].search_read(
            [
                '&', ('item_id', '!=', False),
                    '&', ('start_date', '<=', today),
                        '|', ('end_date', '=', False), ('end_date', '>=', today)
            ],
            fields=['id', 'item_id', 'federal_part']
        )
        conpa_dict = {(c['item_id'][0], c['federal_part']): c['id'] for c in conpa_obj}

        conpp_obj = self.env['budget.program.conversion'].search_read(
            [
            '&', ('start_date', '<=', today),
                    '|', ('end_date', '=', False), ('end_date', '>=', today) 
            ],
            fields=['id', 'program_key_id', 'conversion_key_id', 'shcp']
        )
        conpp_dict = {(c['program_key_id'][1], c['conversion_key_id'][1], c['shcp'][1]): c['id'] for c in conpp_obj}

        expense_type_obj = self.env['expense.type'].search_read(
            [
            '&', ('start_date', '<=', today),
                    '|', ('end_date', '=', False), ('end_date', '>=', today) 
            ], 
            fields=['id', 'key_expenditure_type'])
        expense_type_dict = {e['key_expenditure_type']: e['id'] for e in expense_type_obj}

        geo_location_obj = self.env['geographic.location'].search_read(
            [
            '&', ('start_date', '<=', today),
                    '|', ('end_date', '=', False), ('end_date', '>=', today) 
            ], 
            fields=['id', 'state_key'])
        geo_location_dict = {l['state_key']: l['id'] for l in geo_location_obj}

        wallet_obj = self.env['key.wallet'].search_read(
            [
            '&', ('start_date', '<=', today),
                    '|', ('end_date', '=', False), ('end_date', '>=', today) 
            ], 
            fields=['id', 'wallet_password'])
        wallet_dict = {w['wallet_password']: w['id'] for w in wallet_obj}
        project_obj = self.env['project.project'].search_read(
            [('number', '!=', False)],
            fields=['id', 'number', 'custom_project_type_id', 'custom_stage_id']
        )
        project_dict = {(p['number'], p['custom_project_type_id'][1], p['custom_stage_id'][1]): p['id'] for p in project_obj}

        project_type_obj = self.env['project.type'].search_read([], fields=['id', 'project_id'])
        project_type_dict = {p['project_id'][0]: p['id'] for p in project_type_obj}

        stage_obj = self.env['stage'].search_read([], fields=['id', 'project_id'])
        stage_dict = {s['project_id'][0]: s['id'] for s in stage_obj}

        agreement_obj = self.env['agreement.type'].search_read(
            [],
            fields=['id', 'project_id', 'agreement_type', 'number_agreement']
        )
        agreement_dict = {(a['project_id'][0], a['agreement_type'], a['number_agreement']): a['id'] for a in agreement_obj}

        program_code_dicts = {  'year': year_obj,
                                'check_digit': dv_obj,
                                'program_dict': program_dict,
                                'sub_program_dict': sub_program_dict,
                                'dependency_dict': dependency_dict,
                                'sub_dependency_dict': sub_dependency_dict,
                                'expenditure_item_dict': item_dict,
                                'resource_origin_dict': origin_dict,
                                'institutional_activity_dict': activity_dict,
                                'conpp_dict': conpp_dict,
                                'conpa_dict': conpa_dict,
                                'expense_type_dict': expense_type_dict,
                                'geographic_location_dict': geo_location_dict,
                                'key_wallet_dict': wallet_dict,
                                'project_dict': project_dict,
                                'project_type_dict': project_type_dict,
                                'stage_dict': stage_dict,
                                'agreement_dict': agreement_dict
                                }
        
        last_fiscalyear_lock_date = self.get_last_fiscalyear_lock_date()
        fiscal_year = self.fiscal_year.name
        failed_row = ""
        success_line_ids = []
        failed_line_ids = []
        exercise_key = ''
        str_list_folios = []
        pointer = 0
        # SI hay lineas de adecuacion pero no por archivo importado
        if self.success_line_ids and not self.budget_file:
            for line in self.success_line_ids:
                pointer = line.id
                line_vals = [line.code_id.year.name, line.code_id.program_id.key_unam, line.code_id.sub_program_id.sub_program, line.code_id.dependency_id.dependency, line.code_id.sub_dependency_id.sub_dependency, line.code_id.item_id.item,
                             line.code_id.check_digit, line.code_id.resource_origin_id.key_origin, line.code_id.institutional_activity_id.number, line.code_id.budget_program_conversion_id.shcp.name,
                             line.code_id.conversion_item_id.federal_part, line.code_id.expense_type_id.key_expenditure_type, line.code_id.location_id.state_key, line.code_id.portfolio_id.wallet_password,
                             line.code_id.project_type_id.project_type_identifier, line.code_id.project_number, line.code_id.stage_id.stage_identifier, line.code_id.agreement_type_id.agreement_type,
                             line.code_id.number_agreement, exercise_key, line.folio, line.budget_id.name, line.origin_id.name, line.quarter.name]
                # List to check unique folios
                str_list_folios.append(str(int(line.folio)))
                # validate lock_date #
                if last_fiscalyear_lock_date:
                    failed_row, failed_line_ids  = self.check_movement_execution(line.id, line.origin_id.name, line.quarter.name, fiscal_year, last_fiscalyear_lock_date, line_vals, failed_row, failed_line_ids)

                # Validaciones de vigencia de cada modelo que conforma el codigo #
                failed_row, failed_line_ids =self.env['adequacies'].check_validity_program_code_sections(line_vals, failed_row, failed_line_ids, pointer, program_code_dicts)
                # Validaciones de catálogos compuestos #
                failed_row, failed_line_ids = self.env['adequacies'].check_composite_catalogs_program_code(program_code_obj, line_vals, failed_row, failed_line_ids, line.id)
                # If all is ok# 
                success_line_ids.append(line.id)

            # Validate that the folios is unique among rescheduling lines #
            self.env[STANDARDIZATION_LINE].validate_unique_folios(str_list_folios)

            success_lines = self.env[STANDARDIZATION_LINE].browse(success_line_ids)
            success_lines.write({'line_state': 'success'})
            failed_lines = self.env[STANDARDIZATION_LINE].browse(failed_line_ids)
            failed_lines.write({'line_state': 'fail'})

            if len(failed_line_ids) > 0:
                    # Si hay errores se crea el archivo de texto
                    self.create_failed_row_file(failed_row, self.budget_file)
                    return self.wizard_error_message(self.budget_file)
            

    def create_failed_row_file(self, failed_row, imported, vals = None):
            if failed_row != "":
                # Crear un conjunto para eliminar duplicados
                failed_row_set = set(failed_row.split("\n"))
                # Convertir el conjunto nuevamente en una cadena
                cleaned_failed_row = "\n".join(failed_row_set)
                content = "\n"
                content += _("...................Failed Rows ") + \
                        str(datetime.today()) + "...............\r\n"
                content += str(cleaned_failed_row)
                failed_data = base64.b64encode(content.encode('utf-8'))

                if imported:
                    vals['failed_row_file'] = failed_data
                else:    
                    self.failed_row_file = failed_data


    def cancel(self):
        self.state = 'cancelled'

    def import_lines(self):
        return {
            'name': _('Import Standardization Lines'),
            'type': IR_ACTIONS_ACT_WINDOW,
            'res_model': 'import.standardization.line',
            'view_mode': 'form',
            'view_type': 'form',
            'views': [(False, 'form')],
            'target': 'new',
        }
    
    def create_message_status(self, folios, old, new):
        msg = '<ul class="o_mail_thread_message_tracking">'
        for folio in folios:
            msg += (f"""
            <li>
                Recalendarización {folio}:
                <span> {old} </span>
                <span class="fa fa-long-arrow-right" role="img" aria-label="Cambiado" title="Cambiado"></span>
                <span> {new} </span>
            </li>
            """)
        msg += '</ul>'
        return msg

    def action_draft(self):
        if self.state != 'confirmed':
            if self.env.user.lang == 'es_MX':
                raise ValidationError(MSG_CHANGE_OF_STATUS_ES)
            else:
                raise ValidationError(MSG_CHANGE_OF_STATUS)
        
        lines = self.line_ids.filtered(
            lambda l: l.selected == True and l.state != False and l.state != 'draft')
        if lines:
            if self.env.user.lang == 'es_MX':
                raise ValidationError("No puedes saltar o devolver el estado de una línea de Recalendarización")
            else:
                raise ValidationError("You cannot skip or return the status of a Re-scheduling line")

        
        lines = self.line_ids.filtered(lambda l: l.selected == True and l.state == False)
        for line in lines:
            line.state = 'draft'

    def action_received(self):
        if self.state != 'confirmed':
            if self.env.user.lang == 'es_MX':
                raise ValidationError(MSG_CHANGE_OF_STATUS_ES)
            else:
                raise ValidationError(MSG_CHANGE_OF_STATUS)

        lines = self.line_ids.filtered(
            lambda l: l.selected == True and l.state not in ('draft','received'))
        if lines:
            if self.env.user.lang == 'es_MX':
                raise ValidationError("No puedes saltar o devolver el estado de una línea de Recalendarización")
            else:
                raise ValidationError("You cannot skip or return the status of a Re-scheduling line")
                
        lines = self.line_ids.filtered(
            lambda l: l.selected == True and l.state == 'draft')

        folios = []
        for line in lines:
            folios.append(line.folio)
            line.state = 'received'
        if folios:
            self.message_post(body=self.create_message_status(folios, 'Borrador', 'Recibido'))

    def action_in_process(self):
        if self.state != 'confirmed':
            if self.env.user.lang == 'es_MX':
                raise ValidationError(MSG_CHANGE_OF_STATUS_ES)
            else:
                raise ValidationError(MSG_CHANGE_OF_STATUS)

        lines = self.line_ids.filtered(
            lambda l: l.selected == True and l.state not in ('in_process','received'))
        if lines:
            if self.env.user.lang == 'es_MX':
                raise ValidationError("No puedes saltar o devolver el estado de una línea de Recalendarización")
            else:
                raise ValidationError("You cannot skip or return the status of a Re-scheduling line")
        
        lines = self.line_ids.filtered(
            lambda l: l.selected == True and l.state == 'received')

        folios = []
        for line in lines:
            folios.append(line.folio)
            line.state = 'in_process'
        if folios:
            self.message_post(body=self.create_message_status(folios, 'Recibido', 'En Proceso'))

    def action_authorized(self):
        if self.state != 'confirmed':
            if self.env.user.lang == 'es_MX':
                raise ValidationError(MSG_CHANGE_OF_STATUS_ES)
            else:
                raise ValidationError(MSG_CHANGE_OF_STATUS)

        lines = self.line_ids.filtered(
            lambda l: l.selected == True and l.state not in ('in_process','authorized'))
        if lines:
            if self.env.user.lang == 'es_MX':
                raise ValidationError("No puedes saltar o devolver el estado de una línea de Recalendarización")
            else:
                raise ValidationError("You cannot skip or return the status of a Re-scheduling line")
        
        lines = self.line_ids.filtered(
            lambda l: l.selected == True and l.state == 'in_process')

        folios = []
        for line in lines:
            folios.append(line.folio)
            line.state = 'authorized'
        if folios:
            self.message_post(body=self.create_message_status(folios, 'En Proceso', 'Autorizada'))

        self.check_line_state()

        # if lines and self.journal_id:
        #     move_obj = self.env['account.move']
        #     journal = self.journal_id
        #     today = datetime.today().date()
        #     user = self.env.user
        #     partner_id = user.partner_id.id
        #     amount = sum(lines.mapped('amount'))
        #     company_id = user.company_id.id
        #     if not journal.default_debit_account_id or not journal.default_credit_account_id \
        #             or not journal.conac_debit_account_id or not journal.conac_credit_account_id:
        #         if self.env.user.lang == 'es_MX':
        #             raise ValidationError(_("Por favor configure la cuenta UNAM y CONAC en diario!"))
        #         else:
        #             raise ValidationError(_("Please configure UNAM and CONAC account in budget journal!"))
        #     unam_move_val = {'ref': self.folio, 'budget_standardization_id': self.id, 'conac_move': True,
        #                      'date': today, 'journal_id': journal.id, 'company_id': company_id,
        #                      'line_ids': [(0, 0, {
        #                          'account_id': journal.default_credit_account_id.id,
        #                          'coa_conac_id': journal.conac_credit_account_id.id,
        #                          'credit': amount, 'budget_standardization_id': self.id,
        #                          'partner_id': partner_id
        #                      }), (0, 0, {
        #                          'account_id': journal.default_debit_account_id.id,
        #                          'coa_conac_id': journal.conac_debit_account_id.id,
        #                          'debit': amount, 'budget_standardization_id': self.id,
        #                          'partner_id': partner_id
        #                      })]}
        #     unam_move = move_obj.create(unam_move_val)
        #     unam_move.action_post()

    def action_cancelled(self):
        if self.state != 'confirmed':
            if self.env.user.lang == 'es_MX':
                raise ValidationError(MSG_CHANGE_OF_STATUS_ES)
            else:
                raise ValidationError(MSG_CHANGE_OF_STATUS)

        lines = self.line_ids.filtered(lambda l: l.selected == True and l.state == 'authorized')
        if lines:
            if self.env.user.lang == 'es_MX':
                raise ValidationError("No puede cancelar la línea de Recalendarización autorizada")
            else:
                raise ValidationError("You cannot Canceled the Authorized  Re-scheduling line")

        query = "update standardization_line set state = 'cancelled' where standardization_id = %s and selected is True and state != 'authorized'"
        self.env.cr.execute(query, (self.id,))
        self.state = 'cancelled'
        return {
            'type': IR_ACTIONS_ACT_WINDOW,
            'res_model': 'reject.standardization.line',
            'view_mode': 'form',
            'view_type': 'form',
            'views': [(False, 'form')],
            'target': 'new',
            'context': {'parent_id': self.id, }
        }

    def draft_button(self):
        action = self.env.ref(
            'jt_budget_mgmt.action_standardization_lines').read()[0]
        action['view_mode'] = 'tree'
        action['domain'] = [
            ('standardization_id', '=', self.id), ('state', '=', 'draft')]
        return action

    def received_button(self):
        action = self.env.ref(
            'jt_budget_mgmt.action_standardization_lines').read()[0]
        action['view_mode'] = 'tree'
        action['domain'] = [
            ('standardization_id', '=', self.id), ('state', '=', 'received')]
        return action

    def in_process_button(self):
        action = self.env.ref(
            'jt_budget_mgmt.action_standardization_lines').read()[0]
        action['view_mode'] = 'tree'
        action['domain'] = [
            ('standardization_id', '=', self.id), ('state', '=', 'in_process')]
        return action

    def authorized_button(self):
        action = self.env.ref(
            'jt_budget_mgmt.action_standardization_lines').read()[0]
        action['view_mode'] = 'tree'
        action['domain'] = [
            ('standardization_id', '=', self.id), ('state', '=', 'authorized')]
        return action

    def cancelled_button(self):
        action = self.env.ref(
            'jt_budget_mgmt.action_standardization_lines').read()[0]
        action['view_mode'] = 'tree'
        action['domain'] = [
            ('standardization_id', '=', self.id), ('state', '=', 'cancelled')]
        return action

    def all_lines_button(self):
        action = self.env.ref(
            'jt_budget_mgmt.action_standardization_lines').read()[0]
        action['view_mode'] = 'tree'
        action['domain'] = [
            ('standardization_id', '=', self.id)]
        return action

    def select_deselect_checkbox(self):
        if self.select_box:
            self.select_box = False
        else:
            self.select_box = True

        self.line_ids.write({'selected': self.select_box})

    def unlink(self):
        for record in self:
            if record.state != 'draft':
                if self.env.user.lang == 'es_MX':
                    raise ValidationError(
                        'No se puede eliminar una Recalendarización validada!')
                else:
                    raise ValidationError(
                        'You can not delete confirmed Re-standardization!')
            # Borrado de las líneas de recalendarización
            query = """
                delete from standardization_line where standardization_id = %s
            """
            self.env.cr.execute(query, (record.id,))
            # Borrado de la cabecera de la de recalendarización
            query = """
                delete from standardization where id = %s
            """
            self.env.cr.execute(query, (record.id,))

    @api.model
    def create(self,vals):
        if not vals.get('name',False):
            name = self.env[IR_SEQUENCE_MODEL].next_by_code('re.standardization.folio')
            vals.update({'folio':name})
        res = super(Standardization,self).create(vals)

        year_fiscal = self.env['account.fiscal.year'].search([('id', '=', vals['fiscal_year'])])
        year = year_fiscal.name
        fiscal_year = ''
        year_sequence = self.env[IR_SEQUENCE_MODEL]
        if year:
            existing_sequence = self.env[IR_SEQUENCE_MODEL].search([('name', 'like', f'Secuencia Recalendarizaciones de Egresos {year}')])
            if existing_sequence:
                fiscal_year = existing_sequence[0].code
            else:
                new_sequence = year_sequence.create({
                    'name': f'Secuencia Recalendarizaciones de Egresos {year}',
                    'code': f'seq.recalendaring.name.{year}',
                    'padding': 5,
                    'implementation': 'standard',
                })
                fiscal_year = new_sequence.code
        seq = self.env[IR_SEQUENCE_MODEL].next_by_code(fiscal_year)
        res.recalendaring_name = "REEG" + "/" + str(year) + "/" + str(seq)

        return res

class StandardizationLine(models.Model):

    _name = 'standardization.line'
    _description = 'Re-standardization Lines'
    _rec_name = 'folio'

    @api.model
    def init(self):
        self._cr.execute("""
            create index if not exists standardization_line_write_date_idx
            on standardization_line(write_date);
        """)

    @api.depends('origin_id','origin_id.end_date','origin_id.start_date')
    def get_origin_data(self):
        for line in self:
            if line.origin_id and line.origin_id.end_date:
                date_end = str(line.origin_id.end_date).split('/')
                if len(date_end) > 1:
                    line.origin_id_end_day = int(date_end[0])
                    line.origin_id_end_month = int(date_end[1])
                    
            if line.origin_id and line.origin_id.start_date:
                start_end = str(line.origin_id.start_date).split('/')
                if len(start_end) > 1:
                    line.origin_id_start_day = int(start_end[0])
                    line.origin_id_start_month = int(start_end[1])


    @api.depends('quarter','quarter.end_date','quarter.start_date')
    def get_quarter_data(self):
        for line in self:
            if line.quarter and line.quarter.end_date:
                date_end = str(line.quarter.end_date).split('/')
                if len(date_end) > 1:
                    line.quarter_end_day = int(date_end[0])
                    line.quarter_end_month = int(date_end[1])
                    
            if line.quarter and line.quarter.start_date:
                start_end = str(line.quarter.start_date).split('/')
                if len(start_end) > 1:
                    line.quarter_start_day = int(start_end[0])
                    line.quarter_start_month = int(start_end[1])
                
    folio = fields.Char(string='Folio')
    budget_id = fields.Many2one('expenditure.budget', string='Budget')
    code_id = fields.Many2one(PROGRAM_CODE_MODEL, string='Code', domain="[('budget_id', '=', budget_id)]", index=True)
    amount = fields.Monetary(string='Amount', currency_field='currency_id')
    origin_id = fields.Many2one('quarter.budget', string='Origin')
    quarter = fields.Many2one('quarter.budget', string='Quarter')
    origin_id_start_month = fields.Integer('Origin Month',compute='get_origin_data',store=True)
    origin_id_start_day = fields.Integer('Origin Day',compute='get_origin_data',store=True)
    origin_id_end_month = fields.Integer('Origin Month',compute='get_origin_data',store=True)
    origin_id_end_day = fields.Integer('Origin Day',compute='get_origin_data',store=True)
    
    quarter_start_month = fields.Integer('quarter Month',compute='get_quarter_data',store=True)
    quarter_start_day = fields.Integer('quarter Day',compute='get_quarter_data',store=True)
    quarter_end_month = fields.Integer('quarter Month',compute='get_quarter_data',store=True)
    quarter_end_day = fields.Integer('quarter Day',compute='get_quarter_data',store=True)
    
    reason = fields.Text(string='Reason for rejection')
    standardization_id = fields.Many2one('standardization', string='Standardization', ondelete="cascade", index=True)
    currency_id = fields.Many2one(
        'res.currency', default=lambda self: self.env.user.company_id.currency_id)
    imported = fields.Boolean(default=False)
    amount_effected = fields.Boolean(string='Amount Effected?')
    selected = fields.Boolean(default=False)
    state = fields.Selection(
        [
            ('draft', 'Draft'),
            ('received', 'Received'),
            ('in_process', 'In process'),
            ('authorized', 'Authorized'),
            ('cancelled', 'Cancelled')
        ], string='State',
    )

    fiscal_year = fields.Many2one("account.fiscal.year", string = "Fiscal Year", related="standardization_id.fiscal_year")

    # Fields for imported data

    line_state = fields.Selection([('manual', 'Manual'), ('draft', 'Draft'), (
        'fail', 'Fail'), ('success', 'Success')], string='Line Status', default='manual')
        
    year = fields.Char(string='Year')
    program = fields.Char(string='Program')
    subprogram = fields.Char(string='Sub-Program')
    dependency = fields.Char(string='Dependency')
    subdependency = fields.Char(string='Sub-Dependency')
    item = fields.Char(string='Expense Item')
    dv = fields.Char(string='Digit Varification')
    origin_resource = fields.Char(string='Origin Resource')
    ai = fields.Char(string='Institutional Activity')
    conversion_program = fields.Char(string='Conversion Program')
    departure_conversion = fields.Char(string='Federal Item')
    expense_type = fields.Char(string='Expense Type')
    location = fields.Char(string='State Code')
    portfolio = fields.Char(string='Key portfolio')
    project_type = fields.Char(string='Type of Project')
    project_number = fields.Char(string='Project Number')
    stage = fields.Char(string='Stage Identifier')
    agreement_type = fields.Char(string='Type of Agreement')
    agreement_number = fields.Char(string='Agreement number')
    exercise_type = fields.Char(string='Exercise type')
    budget = fields.Char(string='Budget')
    origin = fields.Char(string='Origin')
    quarter_data = fields.Char(string='Quarter')
    
    _sql_constraints = [('uniq_program_per_standardization_id', 'unique(id,code_id,standardization_id)',
                         'The program code must be unique per Standardization'),
                        ('folio_uniq', 'unique(folio)', 'The folio must be unique.')]
    
    state_related = fields.Selection(string="Estado", related="standardization_id.state", store=True)
    dependency_id = fields.Many2one('dependency', string='Dependency', related="code_id.dependency_id", store=True)
    sub_dependency_id = fields.Many2one('sub.dependency', string='Sub-Dependency', related="code_id.sub_dependency_id", store=True)
    item_id = fields.Many2one('expenditure.item', string='Item', related="code_id.item_id", store=True)
    departure_group_id = fields.Many2one('departure.group', string = 'Departure group', related="item_id.departure_group_id", store=True)
        
    @api.model
    def fields_get(self, fields=None, attributes=None):
        
        no_selectable_fields = ['origin_id_end_day', 'origin_id_start_day', 'standardization_id', 'line_state', 'origin_id_end_month',
                                'origin_id_start_month', 'budget_id', 'selected', 'origin_id', 'quarter_data', 'quarter_end_day', 'quarter_end_month','quarter_start_day',
                                'quarter_start_month', 'imported' , 'amount_effected', 'code_id', 'location', 'exercise_type', 'conversion_program']
        
        no_sortable_field = ['origin_id_end_day', 'origin_id_start_day', 'standardization_id', 'line_state', 'origin_id_end_month', 
                             'origin_id_start_month', 'budget_id', 'selected', 'origin_id', 'quarter_data', 'quarter_end_day', 'quarter_end_month',
                             'quarter_start_day', 'quarter_start_month', 'amount_effected', 'imported', 'code_id', 'location', 'exercise_type', 'conversion_program']
        
        fields_no_search = ['id']

        res = super(StandardizationLine, self).fields_get(fields, attributes=attributes)
        
        for key, value in res.items():
            if key in no_selectable_fields:
                value.update({'selectable': False})
        
        for key, value in res.items():
            if key in no_sortable_field:
                value.update({'sortable': False})
        
        for key, value in res.items():
            if key in fields_no_search:
                value.update({'searchable': False})
                
        return res
    


    @api.constrains('folio')
    def _check_folio(self):
        for line in self:
            if not str(line.folio).isnumeric():
                if self.env.user.lang == 'es_MX':
                    raise ValidationError('Folio Debe ser un valor numérico!')
                else:
                    raise ValidationError('Folio Must be numeric value!')

    @api.onchange('state')
    def _onchange_state(self):
        state = self._origin.state
        if state and state == 'draft' and self.state not in ['draft', 'received', 'cancelled']:
            if self.env.user.lang == 'es_MX':
                raise ValidationError("No puedes saltar o devolver el estado de una línea de Recalendarización")
            else:
                raise ValidationError(
                "You cannot skip or return the status of a Re-scheduling line")
        if state and state == 'received' and self.state not in ['received', 'in_process', 'cancelled']:
            if self.env.user.lang == 'es_MX':
                raise ValidationError("No puedes saltar o devolver el estado de una línea de Recalendarización")
            else:            
                raise ValidationError(
                    "You cannot skip or return the status of a Re-scheduling line")
        if state and state == 'in_process' and self.state not in ['in_process', 'authorized', 'cancelled']:

            if self.env.user.lang == 'es_MX':
                raise ValidationError("No puedes saltar o devolver el estado de una línea de Recalendarización")
            else:
                raise ValidationError(
                    "You cannot skip or return the status of a Re-scheduling line")
        if state and state == 'authorized' and self.state not in ['authorized']:
            if self.env.user.lang == 'es_MX':
                raise ValidationError("No puedes saltar o devolver el estado de una línea de Recalendarización")
            else:
                raise ValidationError("You cannot skip or return the status of a Re-scheduling line")
        if state and state == 'cancelled' and self.state not in ['cancelled']:
            if self.env.user.lang == 'es_MX':
                raise ValidationError("No puedes saltar o devolver el estado de una línea de Recalendarización")
            else:
                raise ValidationError("You cannot skip or return the status of a Re-scheduling line")


    def validate_unique_folios(self, str_list_folios):
        """
        Validate that the folios are unique among rescheduling lines.

        :param str_list_folios: List of folios to check for uniqueness.
        :type str_list_folios: list

        :raises: ValidationError if any folio is already registered in any rescheduling line.

        Example:
            str_list_folios = ['24112', '24113', '24114']
            self.validate_unique_folios(str_list_folios)
        """ 
        same_folio_lines = self.search([('folio', 'in', str_list_folios), ('state', '=', 'authorized')])
        
        if same_folio_lines:
            error_messages = []
            
            for line in same_folio_lines:
                error_messages.append(
                    _("The folio %s is already registered in the rescheduling %s") % (line.folio, line.standardization_id.recalendaring_name)
                )
            
            raise ValidationError("\n".join(error_messages))  


    def logged_user_domain(self):
        domain =  self.env['res.users'].get_user_dependencies_subdependencies_domain_budget() 
        
        return {
            'name': _('Expenditure Standardization Lines'),
            "res_model": "standardization.line",
            "domain": domain,
            "type": "ir.actions.act_window",
            "view_mode": "tree,search",
            "views": [(self.env.ref('jt_budget_mgmt.standardization_lines_tree_view').id, 'tree'),
                      (self.env.ref('jt_budget_mgmt.standardization_lines_search_view').id, 'search')
                     ],
        }


