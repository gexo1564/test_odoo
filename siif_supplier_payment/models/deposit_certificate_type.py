from odoo import models, fields,api,_

class DepositCertificateType(models.Model):

    _inherit = 'deposit.certificate.type'

    account_return_catalog = fields.Boolean(string="Account Return Catalog",  default=False)

    @api.depends('name')
    def name_get(self):
        res=[]
        if self.env.context and self.env.context.get('show_type_certificate', True):
            result = []
            for rec in self:
                if rec.name and rec.description:
                    result.append((rec.id, rec.name+' '+rec.description))
                else:
                    result.append((rec.id, rec.name))
            return result
        else:
            return res
