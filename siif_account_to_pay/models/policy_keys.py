# -*- coding: utf-8 -*-
import logging
from odoo import models, fields, api, _

class PolicyKeys(models.Model):
    _inherit = 'policy.keys'

    allowed_users_cxp = fields.Many2many('res.users','allowed_users_upa_cxp_rel', string="User Allowed")


    #Función para cambio de nombre en la selección de Catálogo UPA
    def name_get(self):
        result = []
        for rec in self:
            name = rec.origin or ''
            if rec.organization and self.env.context:
                name = rec.organization
            result.append((rec.id, name))
        return result