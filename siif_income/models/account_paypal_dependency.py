# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
from datetime import datetime
import time
import json
import logging

class SiifAccountPaypalDependency(models.Model):
    _name = "siif.income.account.paypal.dependency"
    _description = "Cuentas Paypal-Dependencia"
    _rec_name = "id_cta_paypal"

    id_cta_paypal = fields.Char(string = "Cuenta PayPal")
    tpo_cta_paypal = fields.Selection([('hija', 'Hija'),
                                       ('mother', 'Madre')
                                      ], string="Tipo de Cuenta PayPal")
    cve_dep = fields.Many2one(comodel_name = "dependency", string = "Clave Dependencia")	
    cve_sdep = fields.Many2one(comodel_name = "sub.dependency", string = "Clave Subdependencia")
    domain_cve_sdep = fields.Char(compute="_compute_domain_sub_dependency", store=False)

    @api.depends('cve_dep')
    def _compute_domain_sub_dependency(self):
        for record in self:
            domain = [('dependency_id', '=', self.cve_dep.id)]
            if not self.cve_dep.id in self.env.user.dependency_income_ids.ids:
                domain.append(('id', 'in', self.env.user.sub_dependency_income_ids.ids))
            record.domain_cve_sdep = json.dumps(domain)