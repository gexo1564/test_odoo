# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields, api, _,tools
from psycopg2 import sql
class IncomeAnnualReport(models.Model):
    
    _name = 'income.annual.report'
    _description = 'Income Annual Report'
    _order = 'year'
    _auto = False

    sub_origin_resource_id = fields.Many2one('sub.origin.resource', "Name")
    sub_origin_name = fields.Char("Name")
    sub_origin_name_group_by = fields.Char("Name Group")
    journal_id = fields.Many2one("account.journal","Accounting Account Description")
    bank_account_id = fields.Many2one(related="journal_id.bank_account_id",string="Bank Account")
    bank_account_name = fields.Char(related="sub_origin_resource_id.report_account_name",string="Bank Account")
    account_code = fields.Char(related="sub_origin_resource_id.report_account_code",string="Account")
    year = fields.Char('Year')
    january = fields.Float('January')
    february = fields.Float('February')
    march = fields.Float('March')
    april = fields.Float('April')
    may = fields.Float('May')
    june = fields.Float('June')
    july = fields.Float('July')
    august = fields.Float('August')
    september = fields.Float('September')
    october = fields.Float('October')
    november = fields.Float('November')
    december = fields.Float('December')
    total = fields.Float('Total')
    
    def get_header_year_list(self,docs):
        year_list = self.env['income.annual.report'].search([('id','>',0)]).mapped('year')
        max_year = max(year_list)
        min_year = min(year_list)
        str1 = ''
        if max_year == min_year:
            str1 = "ENERO-DICIEMBRE "+str(min_year)
        else:
            str1 = "ENERO "+str(min_year)+ " A " +"DICIEMBRE "+str(max_year)
            
        return str1
     
    def get_origin_records(self,records):
        return records.mapped('sub_origin_resource_id')
    
    def get_origin_records_data(self,origin_id,docs):
        records = docs.filtered(lambda x:x.sub_origin_resource_id.id==origin_id.id)
        journal_ids = records.mapped('journal_id')
        data_list = []
        for journal in journal_ids:
            inner_list = []
            if not data_list:
                inner_list.append(origin_id.report_name)
            else:
                inner_list.append('')
                
            inner_list.append(journal.bank_account_id.acc_number)
            inner_list.append(origin_id.report_account_code)
            inner_list.append(origin_id.report_account_name)
            
             
            inner_list.append(sum(x.january for x in records.filtered(lambda a:a.journal_id.id==journal.id)))
            inner_list.append(sum(x.february for x in records.filtered(lambda a:a.journal_id.id==journal.id)))
            inner_list.append(sum(x.march for x in records.filtered(lambda a:a.journal_id.id==journal.id)))
            inner_list.append(sum(x.april for x in records.filtered(lambda a:a.journal_id.id==journal.id)))
            inner_list.append(sum(x.may for x in records.filtered(lambda a:a.journal_id.id==journal.id)))
            inner_list.append(sum(x.june for x in records.filtered(lambda a:a.journal_id.id==journal.id)))
            inner_list.append(sum(x.july for x in records.filtered(lambda a:a.journal_id.id==journal.id)))
            inner_list.append(sum(x.august for x in records.filtered(lambda a:a.journal_id.id==journal.id)))
            inner_list.append(sum(x.september for x in records.filtered(lambda a:a.journal_id.id==journal.id)))
            inner_list.append(sum(x.october for x in records.filtered(lambda a:a.journal_id.id==journal.id)))
            inner_list.append(sum(x.november for x in records.filtered(lambda a:a.journal_id.id==journal.id)))
            inner_list.append(sum(x.december for x in records.filtered(lambda a:a.journal_id.id==journal.id)))
            inner_list.append(sum(x.total for x in records.filtered(lambda a:a.journal_id.id==journal.id)))
            data_list.append(inner_list)
            
        return data_list
        

# group by item_id_first,item_id_second