from odoo import models, fields, api

class NotificationWizard(models.TransientModel):
    _name = 'notification.wizard'

    notification_message = fields.Text(string="Mensaje", readonly=True)