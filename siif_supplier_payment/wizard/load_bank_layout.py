# -*- coding: utf-8 -*-
import base64
from datetime import datetime
import logging
from odoo import api, models, fields, _

from odoo.addons.siif_payroll_payment.tools.layouts import Context
from odoo.addons.siif_payroll_payment.tools.layouts.bancos import LayoutBanorte, LayoutBBVASIT, LayoutBBVA, LayoutHSBC, LayoutSantanderH2H, LayoutSantander, LayoutBanamex, LayoutInbursa, LayoutScotiabank

# CONSTANT MODEL
LOAD_BANK_LAYOUT_SUPPLIER_PAYMENT = 'load.bank.layout.supplier.payment'
IR_ACTION_WINDOW = 'ir.actions.act_window'

class LoadBankTransferLayout(models.TransientModel):

    _name = 'load.bank.transfer.layout'

    batch_management_id = fields.Many2one('batch.management')
    payment_type = fields.Selection([('general', 'General'), ('payroll', 'Payroll')])
    hide_filed_file = fields.Boolean(default=True)

    file_name = fields.Char('Filename')
    file_data = fields.Binary('Upload File')

    failed_file_name = fields.Char('Failed Filename', default=lambda self: _("Failed_Rows.txt"))
    failed_file_data = fields.Binary('Failed File')

    @api.onchange('file_name')
    def onchange_file_name(self):
        self.write({'failed_file_data': None, 'hide_filed_file': True})

    def open_wizard(self, **kwargs):
        return {
            'name': _('Cargar Layout de Respuesta'),
            'res_model': 'load.bank.transfer.layout',
            'view_mode': 'form',
            'view_id': self.env.ref('siif_supplier_payment.load_bank_transfer_layout_form').id,
            'context': kwargs,
            'target': 'new',
            'type': 'ir.actions.act_window',
            'res_id': self.id if self else None
        }

    def _import_payroll_layout(self):
        batch_management_id = self.batch_management_id
        layout_mapping = {
            "banorte": LayoutBanorte(),
            "bbva_232": LayoutBBVASIT(),
            "bbva_nomina": LayoutBBVA(),
            "hsbc": LayoutHSBC(),
            "santander_h2h": LayoutSantanderH2H(),
            "santander": LayoutSantander(),
            "banamex": LayoutBanamex(),
            "inbursa": LayoutInbursa(),
            "scotiabank": LayoutScotiabank()
        }
        layout_type = layout_mapping.get(batch_management_id.journal_id.payroll_load_bank_format)

        if not layout_type:
            return

        attachment_content = base64.b64decode(self.file_data)

        return Context(self.env, layout_type).read_layout_from_payment(batch_management_id.folio, attachment_content)

    def action_import_layouts(self):
        # Validate layouts
        if self.payment_type == 'general':
            pass
        elif self.payment_type == 'payroll':
            lines_ok, lines_err = self._import_payroll_layout()

        if lines_err:
            content = "................... Registros erróneos " + str(datetime.today()) + "...................\r\n"
            content += "\r\n".join([e.error_message for e in lines_err])
            self.failed_file_data = base64.b64encode(content.encode('utf-8'))
            self.hide_filed_file = False
            return self.open_wizard()
        # Update batch managment and payments
        self.batch_management_id.process_payment(self.file_name, self.file_data, lines_ok)

# class LoadBankLayoutSupplierPayment(models.TransientModel):

#     _inherit = LOAD_BANK_LAYOUT_SUPPLIER_PAYMENT

#     def post_payments_from_layouts(self):
#         errors = self.env['account.payment'].post_payments_with_electronic_transfers_from_layouts(
#             self.journal_id,
#             base64.b64decode(self.file_data)
#         )
#         return self._load_bank_layout(errors)

#     def create_and_post_payments_from_layouts(self):
#         errors = self.env['account.payment'].payments_with_electronic_transfers_from_layouts(
#             self.journal_id,
#             base64.b64decode(self.file_data)
#         )
#         return self._load_bank_layout(errors)

#     def _load_bank_layout(self, errors):
#         if errors:
#             content = "................... Registros erróneos " + str(datetime.today()) + "...................\r\n"
#             content += "\r\n".join([e.error_message for e in errors])
#             self.failed_file_data = base64.b64encode(content.encode('utf-8'))
#             self.is_hide_failed = False
#             return {
#                 'name': _('Load Bank Layout'),
#                 'type': 'ir.actions.act_window',
#                 'res_model': LOAD_BANK_LAYOUT_SUPPLIER_PAYMENT,
#                 'view_mode': 'form',
#                 'view_id': self.env.ref('jt_supplier_payment.view_load_bank_layout_supplier_payment_form').id,
#                 'views': [(False, 'form')],
#                 'context': {},
#                 'target': 'new',
#                 'res_id': self.id,
#             }

#     def validate_load_bank_layout(self):
#         # Identificación de la cuenta bancaria
#         bank_format = self.journal_id.load_bank_format
#         failed_content = ""
#         # Identificación del formato del layout
#         layout_type = None
#         if bank_format == "bbva_bancomer":
#             layout_type = LayoutBBVASIT()
#         elif bank_format == "santander":
#             failed_content=self.load_layout_santander_prov()
                
#         if bank_format == "bbva_bancomer":
#             # Lectura y obtención de los ids de los recibos de nómina
#             logging.info("Lectura y obtención de los ids de los recibos de nómina ...")
#             __, lines_err = Context(self.env, layout_type).load_layout(base64.b64decode(self.file_data))
#             if lines_err:
#                 content = "................... Registros erróneos " + str(datetime.today()) + "...................\r\n"
#                 content += "\r\n".join([e.error_message for e in lines_err])
#                 self.failed_file_data = base64.b64encode(content.encode('utf-8'))
#                 self.is_hide_failed = False
#                 return {
#                     'name': _('Load Bank Layout'),
#                     'type': 'ir.actions.act_window',
#                     'res_model': LOAD_BANK_LAYOUT_SUPPLIER_PAYMENT,
#                     'view_mode': 'form',
#                     'view_id': self.env.ref('jt_supplier_payment.view_load_bank_layout_payroll_payment_form').id,
#                     'views': [(False, 'form')],
#                     'context': {},
#                     'target': 'new',
#                     'res_id': self.id,
#                 }
#         elif bank_format == "santander":
#             if failed_content:
#                 self.failed_file_data = base64.b64encode(failed_content.encode('utf-8'))
#                 self.failed_file_name = 'Data_not_process.csv'
#                 self.is_hide_failed = False
#                 return {
#                     'name': _('Load Bank Layout'),
#                     'type': 'ir.actions.act_window',
#                     'res_model': LOAD_BANK_LAYOUT_SUPPLIER_PAYMENT,
#                     'view_mode': 'form',
#                     'view_id': self.env.ref('jt_supplier_payment.view_load_bank_layout_payroll_payment_form').id,
#                     'views': [(False, 'form')],
#                     'context': {},
#                     'target': 'new',
#                     'res_id': self.id,
#                 }