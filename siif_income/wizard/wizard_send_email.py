from odoo import models, fields, api, _
import base64
from odoo.modules.module import get_resource_path
from xlrd import open_workbook
from odoo.exceptions import UserError, ValidationError
import logging


class WizardSendEmail(models.TransientModel):

    _name = 'wizard.send.email'
    _description = 'Wizard Send Email'

    email = fields.Char(string='Email:')
    msj = fields.Html(string='Messange in email')
    boletos_id = fields.Integer(string='Boletos ID')
    aviso = fields.Char(readonly=True)

    def button_send(self):
        boletos = self.env['siif.income.solicitud.boletaje'].search([('id', '=', self.boletos_id)])
        boletos.write([{'emails': self.email}])
        boletos.set_solicite(self.msj)