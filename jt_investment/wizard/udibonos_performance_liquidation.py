from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
import datetime
import pandas
from dateutil.relativedelta import relativedelta


class UDIPerformanceWizard(models.TransientModel):
    _name = 'udibonos.performance.liquidation.wizard'
    _description = 'Wizard para Rendimiento en liquidación'


    id_udibono = fields.Integer()

    date_consult = fields.Date(
        default=datetime.date.today())

    previous_month_titles_value = fields.Float(digits=(8, 6))
    current_month_titles_value = fields.Float(digits=(8, 6))
    
    @api.constrains('previous_month_titles_value')
    def check_previous_month_titles_value(self):
        if self.previous_month_titles_value <= 0:
            raise UserError(
                _('Negative values or values equal to zero are not allowed'))

    @api.constrains('current_month_titles_value')
    def check_current_month_titles_value(self):
        if self.current_month_titles_value <= 0:
            raise UserError(
                _('Negative values or values equal to zero are not allowed'))
          
    def performance_liquidation(self):
        
            # Seguridad de la función
        if not self.env.user.has_group('jt_investment.group_investment_admin_user'):
            raise UserError(_("You don't have permission to perform this action."))

        # Se obtiene el id del udibono actual
        self.id_udibono = self.env.context.get('active_id')

        # objeto de tipo udibono para consultar el id del udibono actual
        udibono = self.env['investment.udibonos'].search(
            [('id', '=', self.id_udibono)])


        # Asigna valores para realizar el calculo 
        udibono.movement_date = self.date_consult
        udibono.previous_month_titles_value = self.previous_month_titles_value
        udibono.current_month_titles_value = self.current_month_titles_value

        # Se llama a la funcion para realizar el calculo de rendimiento 
        udibono.calculate_performance_liquidation()

        return True