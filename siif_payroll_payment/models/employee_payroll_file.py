# -*- coding: utf-8 -*-

import logging
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from datetime import datetime
from odoo.addons.sd_utils.models import message_wizard

class EmployeePayrollFile(models.Model):

    _inherit = 'employee.payroll.file'

    def action_all_payment_request_generate_payment_request(self):
        active_id = self.env.context.get('active_id')
        res_ids = self.env.context.get('active_ids')
        active_model = self.env.context.get('active_model')

        if active_id:
            payroll_id = self.env[active_model].browse([active_id])
            if payroll_id:
                payroll_id = payroll_id.payroll_processing_id
            return self.create_payroll_payment_request_action(payroll_id.id, res_ids)

    def create_payroll_payment_request_action(self, payroll_id, res_ids):
        """
        Crea una solicitud de pago que engloba todos los recibos de nómina.
        """

        # Devuelve las lineas con las afectaciones totales a los códigos programáticos
        def get_program_code_lines(payroll_id):
            # query_percepciones = """
            #     select p.id program_code_id,
            #         p.dependency_id dependency_id,
            #         p.sub_dependency_id sub_dependency_id,
            #         i.unam_account_id account_id,
            #         sum(amount)::numeric price_unit
            #     from employee_payroll_file r, preception_line l, program_code p, expenditure_item i
            #     where r.id = l.payroll_id
            #         and l.program_code_id = p.id
            #         and p.item_id = i.id
            #         and r.state = 'revised'
            #         and r.payroll_processing_id = %s
            #     group by p.id, p.dependency_id, p.sub_dependency_id, i.unam_account_id;
            # """
            query_percepciones = """
                select p.id program_code_id,
                    p.dependency_id dependency_id,
                    p.sub_dependency_id sub_dependency_id,
                    i.unam_account_id account_id,
                    sum(q.amount)::numeric price_unit
                from (
                    select l.program_code_id, amount
                    from employee_payroll_file r, additional_payments_line l
                    where r.id = l.payroll_id
                        and r.state = 'revised'
                        and r.payroll_processing_id = %s
                    union all
                    select l.program_code_id, l.amount
                    from employee_payroll_file r, preception_line l
                    where r.id = l.payroll_id
                        and r.state = 'revised'
                        and r.payroll_processing_id = %s
                ) q, program_code p, expenditure_item i
                where q.program_code_id = p.id
                    and p.item_id = i.id
                group by p.id, p.dependency_id, p.sub_dependency_id, i.unam_account_id;
            """
            self.env.cr.execute(query_percepciones, (payroll_id,payroll_id))
            # res =  # estructura de res: [(program_code_id, amount)]
            lines = []
            for line in self.env.cr.dictfetchall():
                line.update({
                    'quantity': 1,
                    'exclude_from_invoice_tab': False,
                })
                lines.append(line)
            return lines

        # Devuelve las lineas con las afectaciones totales a los cuentas contables
        def get_account_lines(payroll_id):
            # Valid that all deduction lines have an account
            query_validate_accounts = """
                select count(*)
                from employee_payroll_file r, deduction d, deduction_line l
                where d.id = l.deduction_id
                    and r.id = l.payroll_id
                    and r.state = 'revised'
                    and r.payroll_processing_id = %s
                    and d.credit_account_id is null
            """
            self.env.cr.execute(query_validate_accounts, (payroll_id,))
            if (self.env.cr.fetchone() or [0])[0] > 0:
                raise ValidationError(_("All deductions must have their accounting account."))
            # Get the totals for deductions as a dict
            query_deducciones = """
                select credit_account_id account_id, -sum(amount)::numeric price_unit
                from employee_payroll_file r, deduction d, deduction_line l
                where d.id = l.deduction_id
                and r.id = l.payroll_id
                and r.state = 'revised'
                and r.payroll_processing_id = %s
                and credit_account_id is not null
                group by credit_account_id;
            """
            self.env.cr.execute(query_deducciones, (payroll_id,))
            lines = []
            for line in self.env.cr.dictfetchall():
                line.update({
                    'quantity': 1,
                    'exclude_from_invoice_tab': False,
                })
                lines.append(line)
            return lines
        # Nómina
        payroll_processing_id = self.env['custom.payroll.processing'].browse([payroll_id])
        # Total de recibos de nómina
        query_total_recibos = "select count(*) from employee_payroll_file where payroll_processing_id = %s"
        self.env.cr.execute(query_total_recibos, (payroll_id,))
        number_of_payment_receipts = self.env.cr.fetchone()[0]
        # Total de recibos de nómina en status revisados
        query_total_recibos_revisados = """
            select count(*) from employee_payroll_file
            where payroll_processing_id = %s and state = 'revised'
        """
        self.env.cr.execute(query_total_recibos_revisados, (payroll_id,))
        number_of_payment_receipts_revised = self.env.cr.fetchone()[0]
        # Se valida que todos los recibos de la nómina se encuentren revisados
        if number_of_payment_receipts_revised < number_of_payment_receipts:
            raise ValidationError("Asegúrese que todos los recibos se encuentran en el estado revisado.")
        # Beneficiario
        parameters = self.env['payroll.payment.parameters'].search([])
        if not (parameters and parameters.partner_id):
            raise ValidationError("Configure el beneficiario de la Solicitud de Pago en el Ménu de Parámetros de Pago de Nómina.")
        partner_id = parameters.partner_id
        if not partner_id.property_account_payable_id:
            raise ValidationError("Configure la cuenta a pagar del beneficiario de la Solicitud de Pago.")
        # Diario de la solicitud de pago
        journal_id = parameters.journal_id
        if not journal_id:
            raise ValidationError("Configure el diario de la Solicitud de Pago en el Ménu de Parámetros de Pago de Nómina.")
        # Líneas de la factura de la solicitud
        invoice_lines = get_program_code_lines(payroll_id) + get_account_lines(payroll_id)
        # Datos del objeto account.move a crear
        vals = {
            'payroll_processing_id': payroll_processing_id.id,
            'partner_id': partner_id.id,
            'invoice_date': fields.Date.today(),
            'journal_id': journal_id.id if journal_id else None,
            'fornight' : payroll_processing_id.fornight,
            'payroll_register_user_id': self.env.user.id,
            'payroll_send_user_id': self.env.user.id,
            'number_of_payment_receipts': number_of_payment_receipts,
            'number_of_validated_receipts': 0,
            'is_payroll_payment_request': True,
            'is_pension_payment_request' : False,
            'type' : 'in_invoice',
            'period_start' : payroll_processing_id.period_start or None,
            'period_end' : payroll_processing_id.period_end or None,
            # 'invoice_line_ids': invoice_lines,
        }
        # Creación de la solicitud de pago
        # logging.critical("Líneas generadas")
        payroll_payment_request_id = self.env['account.move'].create_payroll_payment_request(vals, invoice_lines)
        # Actualización de los status de los recibos de pago a 'HECHO'
        query_update_status_recibos = """
            update employee_payroll_file set state = 'done'
            where payroll_processing_id = %s returning 1;
        """
        self.env.cr.execute(query_update_status_recibos, (payroll_id,))
        if not self.env.cr.fetchone():
            raise ValidationError("Error al actualizar el estado de los Recibos de Pago.")
        # Crea donativos por descuento de nómina si el recibo en sus deducciones contiene la clave 409 o 452
        self.create_donations_payroll_deduction(payroll_id, res_ids, payroll_payment_request_id)

        return message_wizard.display_modal(self,
            'Solicitud de Pago de Nómina',
            "La solicitud de pago de nómina se creo exitosamente",
            refresh_page=True
        )


    def create_donations_payroll_deduction(self, payroll_id, res_ids, payroll_payment_request_id):
        employee_payroll_file = self.env['employee.payroll.file'].search([('id', 'in', res_ids)])
        donations_obj = self.env['donations']
        valid_deduction_keys = {'409', '452'}
        custom_payroll_processing = self.env['custom.payroll.processing'].search([('id', '=', payroll_id)], limit=1)
        donations_vals_list = []
        sequence = self.env['ir.sequence'].search([('code', '=', 'donations.sequence')])
        date_today = datetime.today()
        type_donation_id = self.get_id_donation_payroll_discount()
    
        for values in employee_payroll_file:
            deduction_records = self.get_deductions_key_amount(values.id)
            for i in range(len(deduction_records)):
                deduction_id = deduction_records[i][0]
                deduction_key = deduction_records[i][1]
                deduction_amount = deduction_records[i][2]

                if deduction_key in valid_deduction_keys:  
                    next_sequence_number = sequence.number_next_actual + len(donations_vals_list)
                    request_letter = 'QNA{}{}{}'.format(date_today.strftime('%Y'), custom_payroll_processing.fornight, next_sequence_number)

                    donations_vals = {
                        'donation_status': 'draft',
                        'payroll_payment_request_id': payroll_payment_request_id.id,
                        'type_donation_id': type_donation_id,
                        'dependancy_id': values.employee_id.dependancy_id.id,
                        'sub_dependancy_id': values.employee_id.sub_dependancy_id.id,
                        'benefactor': values.employee_id.emp_partner_id.id,
                        'total_donation_amount': deduction_amount,
                        'deduction_id': deduction_id,
                        'fortnight': custom_payroll_processing.fornight,
                        'excercise': date_today.year,
                        'request_letter': request_letter,
                    }
                    donations_vals_list.append(donations_vals)
          
        donations_obj.create(donations_vals_list)


    def get_deductions_key_amount(self, employee_payroll_file_id):
        query_key_amount = """
            SELECT d.id, d.key, dl.amount
            FROM deduction_line dl
            JOIN deduction d ON d.id = dl.deduction_id
            WHERE dl.payroll_id = %s
        """
        self.env.cr.execute(query_key_amount, (employee_payroll_file_id,))
        deduction_records = self.env.cr.fetchall()

        return deduction_records


    def get_id_donation_payroll_discount(self):
        # ID del donativo por descuento de nómina
        query_type_donation = """
            SELECT id
            FROM type_donations
            WHERE name_type = %s;
        """
        self.env.cr.execute(query_type_donation, ('Donativos por descuento de nómina',))
        type_donation_result = self.env.cr.fetchone()
        type_donation_id = type_donation_result[0] 
        if not type_donation_id:
            raise ValidationError(_('Not found Payroll Discount Donations in Income/Settings/Types of Donations.'))

        return type_donation_id



