import logging
from odoo import models, fields,api,_
from odoo.exceptions import ValidationError, UserError
from lxml import etree

class AssociationDistributionIEAccounts(models.Model):

    _inherit = 'association.distribution.ie.accounts'

    base_amount = fields.Float('Base Amount of Income')
    type_of_currency = fields.Selection(
        [('national', 'National Currency'), ('foreign', 'Foreign Currency')])
    sum_percentage = fields.Float("Sum Percentage", compute="_compute_percentage")
    revenue_recognition_account_id = fields.Many2one("account.account",'Revenue recognition account')


    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        res = super().fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])

        is_group_income_fac_administrator_user = self.env.user.has_group('jt_income.group_income_fac_administrator_user')
        is_group_income_fac_finance_user = self.env.user.has_group('jt_income.group_income_fac_finance_user')
        is_group_income_fac_accounting_user = self.env.user.has_group('jt_income.group_income_fac_accounting_user')
     
        if is_group_income_fac_administrator_user:
            for node in doc.xpath("//" + view_type):
                node.set('export', '1')
                node.set('delete', '1')

        if (is_group_income_fac_finance_user or is_group_income_fac_accounting_user):
            for node in doc.xpath("//" + view_type):
                node.set('export', '1')
                node.set('delete', '0')
                
        res['arch'] = etree.tostring(doc)
        return res

    @api.constrains('ie_key')
    def _check_federal_part(self):
        if self.ie_key == None or self.ie_key == '' or not self.ie_key:
            raise ValidationError(_('Please add the affectation key'))

    @api.constrains('base_amount')
    def _check_base_amount(self):
        if self.base_amount<0.0:
            raise ValidationError(_('The base amount cannot be negative'))

    @api.depends('ie_account_line_ids')
    def _compute_percentage(self):
        self.sum_percentage = sum(x.percentage for x in self.ie_account_line_ids)

    @api.constrains('sum_percentage')
    def _check_sum_percentage(self):
        if self.sum_percentage != 0 and (self.sum_percentage < 100 or self.sum_percentage>100):
            if self.sum_percentage != 100:
                raise ValidationError(_('Please configure IE Accounts 100% Percentage'))
    
    '''@api.constrains('ie_key')
    def _validate_ie_key(self):
        if self.ie_key and not self.ie_key.isnumeric():
            raise ValidationError(_('The IE Key must be numeric value'))
    '''



class AssociationDistributionIEAccountsLines(models.Model):
    
    _inherit = 'association.distribution.ie.accounts.line'

    bridge_account_id = fields.Many2one("account.account",'Bridge Account')
    percentage_bridge_account = fields.Float("% Bridge Account")
    main_account = fields.Boolean("Main account")