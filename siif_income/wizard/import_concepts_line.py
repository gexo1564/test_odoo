# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields, api, _
import base64
from odoo.modules.module import get_resource_path
from xlrd import open_workbook
from odoo.exceptions import UserError, ValidationError
import logging

# Define Constant
SIIF_INCOME_SERIES_DOTACIONES_LINES = 'siif.income.series.dotaciones.lines'


class ImportConceptsLine(models.TransientModel):

    _name = 'import.concepts.line'
    _description = 'Import Concepts Line'

    domain_clv_dotacion = fields.Char('Domain Clave Dotacion')
    clv_dotacion = fields.Many2one(comodel_name="siif.income.series.dotaciones.lines", string = 'Clave Dotación')
    record_number = fields.Integer(string='Número de Registros')
    concepto_dot = fields.Char(string = 'Concepto Dotación')

    domain_factura_cliente = fields.Char('Factura Cliente')
    cfdi_id = fields.Many2one(comodel_name="account.move", string='Factura Cliente Asociada')

    file = fields.Binary(string='Archivo')
    download_file = fields.Binary(string='Descargar Archivo')
    download_filename = fields.Char(string='Download File name')

    importe_cfdi_id = fields.Integer('Importe CFDI Asociado')
    total_boletos_comp_dotacion = fields.Integer('Total Boletos Por Comprobar', store=True)
    importe_corte = fields.Float(string = 'Importe por Boleto')

    @api.onchange('clv_dotacion')
    def onchange_clv_dotacion_corte(self):
        if self.clv_dotacion:
            dotacion_serie = self.env[SIIF_INCOME_SERIES_DOTACIONES_LINES].search([('id', '=', self.clv_dotacion.id)])
            concep = dotacion_serie.concepto
            self.concepto_dot = concep
    
    @api.onchange('cfdi_id')
    def onchange_nombre_cfdi_id(self):
        if self.cfdi_id:
            cliente_id = self.env['account.move'].search([('id', '=', self.cfdi_id.id), ('state', '=', 'posted')])
            self.importe_cfdi_id = cliente_id.amount_total

    @api.onchange('clv_dotacion')
    def onchange_clv_dotacion_solicitud(self):
        if self.clv_dotacion:
            dotacion_serie = self.env[SIIF_INCOME_SERIES_DOTACIONES_LINES].search([('id', '=', self.clv_dotacion.id)])
            self.total_boletos_comp_dotacion = dotacion_serie.folios_rest
            if dotacion_serie.id:
                dotacion_solicitud = self.env['siif.income.solicitud.dotaciones.lines'].search([('clv_dotacion', '=', dotacion_serie.id)])[-1]
                importe = dotacion_solicitud.import_boleto
                self.importe_corte = importe

    
    def import_line(self):
        if not self.file:
            raise UserError(_('Please Upload File.'))
            
        try:
            data = base64.decodestring(self.file)
            book = open_workbook(file_contents=data or b'')
        except UserError as e:
            raise UserError(e)

        sheet = book.sheet_by_index(0)

        total_rows = self.record_number + 1
        if sheet.nrows != total_rows:
            raise UserError(_('Number of records do not match with file'))


        lines = []
        corte_caja_id = self.env['siif.income.corte.caja'].browse(self._context.get('active_id'))
        series_ids = self.env['siif.income.series.dependencia.boletaje'].search([('id', '=', corte_caja_id.serie.id)])

        nombre_cfdis = set()
        sum_amount_total_conceptos = 0
        for row in range(1, sheet.nrows):
            clave = self.env[SIIF_INCOME_SERIES_DOTACIONES_LINES].search([('my_id', '=', series_ids.id), ('clv_dotacion', '=', sheet.cell_value(row, 0))])
            if not clave:
                raise ValidationError(_("La clave dotación no existe para la serie seleccionada!!!"))

            # hay que revisar
            cfdi_folio = self.env['account.move'].search([('state', '=', 'posted'), ('type_of_revenue_collection', '=', 'billing'), ('invoice_payment_state', '=', 'paid'), ('payment_of_id', '=', 63), ('cfdi_folio', '=', sheet.cell_value(row, 2))])
            
            if cfdi_folio.is_in_cash_closing == True:
                raise ValidationError(_("La Factura con CFDI Folio %s que desea comprobar ya ha sido utilizado en otro corte de caja." % (cfdi_folio.cfdi_folio)))


            lines.append((0,0,{
                'my_id': corte_caja_id.id,
                'clv_dotacion': clave.id,
                'concepto_dot': clave.concepto,
                'concepto_boleto': sheet.cell_value(row, 1),
                'cfdi_id': cfdi_folio.id,
                'poliza_contable': cfdi_folio.name + ' ' + '(' + cfdi_folio.ref + ')',
                'importe_cfdi_id': cfdi_folio.amount_total,
                'total_boletos_comp_dotacion': clave.folios_rest,
                'boleto_sold': sheet.cell_value(row, 3),
                'boleto_cancelated': sheet.cell_value(row, 4),
                'boleto_courtesy': sheet.cell_value(row, 5),
                'boleto_reprinted': sheet.cell_value(row, 6),
                'rob_extrav': sheet.cell_value(row, 7),
                'boleto_other': sheet.cell_value(row, 8),
                'folio_inicial': sheet.cell_value(row, 9),
                'folio_final': int(sheet.cell_value(row, 3)) + int(sheet.cell_value(row, 4)) + int(sheet.cell_value(row, 5)) + int(sheet.cell_value(row, 7)) + int(sheet.cell_value(row, 8)) + int(sheet.cell_value(row, 9)) - int(1),
                'importe_boleto': sheet.cell_value(row, 11),
                'total_cost_for_boletos': sheet.cell_value(row, 12),
            }))

            sum_amount_total_conceptos += float(sheet.cell_value(row, 12))
            
            nombre_cfdis.add(cfdi_folio)

        lines_cfdi = []
        sum_amount_total_cfdis = 0
        for cfdis in nombre_cfdis:

            descrip = self.env['income.invoice.move.line'].search([('move_id', '=', cfdis.id)], limit = 1)
            

            lines_cfdi.append((0,0,{
                'my_ids': corte_caja_id.id,
                'cfdi_id': cfdis.id,
                'poliza_contable': cfdis.name + ' ' + '(' + cfdis.ref + ')',
                'cliente': cfdis.partner_id.name,
                'description': descrip.name,
                'invoice_uuid': cfdis.invoice_uuid,
                'importe_cfdi': cfdis.amount_total,

            }))

            sum_amount_total_cfdis += float(cfdis.amount_total)

        if sum_amount_total_conceptos != sum_amount_total_cfdis:
            raise ValidationError(_("Los CFDI's que desea comprobar tienen un monto total de $%s y la suma de Importes Totales de los Conceptos es $%s. Favor de verificar los montos de conceptos correspondan a los CFDI's que desea comprobar!!!" % (sum_amount_total_cfdis,sum_amount_total_conceptos)))


        corte_caja_id.write({'line_ids': lines})

        corte_caja_id.write({'line_ids_cfdi': lines_cfdi})

