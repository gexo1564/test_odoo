# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
########################
######################################################
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
import re
import logging
from datetime import datetime

class RequestOpen(models.Model):
    _inherit = 'request.open.balance.finance'

    

    def _check_bank_accounts(self,rec):
        origen = bool(rec.bank_account_id)
        destino = bool(rec.desti_bank_account_id)
        
        if origen and destino:
            return True,""
        elif origen and not destino:
            if rec.state in ('draft','requested'):
                return False,"El Banco Destino no está asignado, revisar."
            else:
                return True,""
        elif not origen and destino:
            if rec.state=='requested':
                return False,"El Banco Origen no está asignado, revisar."
            else:
                return True,""
        elif not origen and not destino:
            return False,"Los Bancos Origen y destino no estan asignados, revisar."
        
    def request_finance(self):
        ok,msg=self._check_bank_accounts(self)
        if ok:
            self.state = 'requested'
        else:
            raise ValidationError(msg)
        