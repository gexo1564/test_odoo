# -*- coding: utf-8 -*-

from odoo import models, fields

class BaseSubDependency(models.Model):
    _name = 'base.sub.dependency'
    _rec_name = 'sub_dependency'
    _order = 'sub_dependency asc'

    sub_dependency = fields.Char('Sub Dependency', size=2)

    _sql_constraints = [
        ('sub_dependency_uk', 'unique(sub_dependency)', 'Sub Dependency exists!')
    ]

    def name_get(self):
        return [(rec.id, rec.sub_dependency) for rec in self] 