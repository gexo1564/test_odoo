# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import calendar
from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
from lxml import etree

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
from odoo.osv import expression

# CONSTANTS FOR MODELS
ACCOUNT_ACCOUNT_MODEL = 'account.account'
ACCOUNT_JOURNAL_MODEL = 'account.journal'
ACCOUNT_MOVE_LINE_MODEL = 'account.move.line'
ACCOUNT_MOVE_MODEL = 'account.move'
ACCOUNT_PAYMENT_MODEL = 'account.payment'
AGREEMENT_AGREEMENT_TYPE_MODEL = 'agreement.agreement.type'
AGREEMENT_FUND_MODEL = 'agreement.fund'
AGREEMENT_TRUST_MODEL = 'agreement.trust'
BASES_COLLABORATION_BALANCE_HISTORY_MODEL = 'bases.collaboration.balance.history'
BASES_COLLABORATION_MODEL = 'bases.collaboration'
BASES_COLLABORATION_MODIFICATION_MODEL = 'bases.collaboration.modification'
BASES_COLLABORATION_MONTHLY_PERFORMANCE_MODEL = 'bases.collaboration.monthly.performance'
COLLABORATION_BENEFICIARY_MODEL = 'collaboration.beneficiary'
COLLABORATION_PROVIDERS_MODEL = 'collaboration.providers'
FUND_TYPE_MODEL = 'fund.type'
HR_EMPLOYEE_MODEL = 'hr.employee'
ORIGIN_RESOURCE_LINE_MODEL = 'origin.resource.line'
PATRIMONIAL_RESOURCES_MODEL = 'patrimonial.resources'
PAYMENT_REQUEST_MODEL = 'payment.request'
REQUEST_OPEN_BALANCE_FINANCE_MODEL = 'request.open.balance.finance'
REQUEST_OPEN_BALANCE_MODEL = 'request.open.balance'
RES_CURRENCY_MODEL = 'res.currency'
RES_PARTNER_BANK_MODEL = 'res.partner.bank'
RES_PARTNER_MODEL = 'res.partner'
RES_USERS_MODEL = 'res.users'
SUB_DEPENDENCY_MODEL = 'sub.dependency'
SUB_ORIGIN_RESOURCE_MODEL = 'sub.origin.resource'
REQUEST_OPEN_BALANCE_INVEST = 'request.open.balance.invest'
IR_MODEL = 'ir.model'
MAIL_THREAD_MODEL = 'mail.thread'
MAIL_ACTIVITY_MIXIN_MODEL = 'mail.activity.mixin'

# CONSTANTS FOR VIEW MODE
VIEW_MODE_TREE_FORM = 'tree,form'
SIIF_SUPPLIER_PAYMENT_PAYMENT_REQUEST_GENERAL_FORM_VIEW = 'siif_supplier_payment.payment_requests_general'
JT_SUPPLIER_PAYMENT_PAYMENT_REQ_REQ_TREE_VIEW = 'jt_supplier_payment.payment_req_tree_view'

# CONSTANTS FOR MESSAGES
ERROR_SEQUENCE_MESSAGE = 'Please define a sequence on your journal'
NO_BENEFICIARY_PROVIDER_DEFINED_ERROR_MESSAGE = 'Please define a beneficiary or provider'
ONLY_ONE_BENEFICIARY_PROVIDER_ALLOWED_ERROR_MESSAGE = 'Please define only one beneficiary or provider'
OPERATION_NUMBER_NOT_NUMERIC_ERROR_MESSAGE = 'Operation Number must be Numeric.'
WITHDRAWAL_AMOUNT_EXCEEDS_BALANCE_ERROR_MESSAGE = 'The amount to withdraw is greater than the available balance'
TYPE_OF_OPERATION_VALID_ERROR = "Type of Operation must be 'Withdrawal Due to Cancellation' for this operation!"
CANT_CREATE_OPERATION_VALID_ERROR = "Can't create Operation with 'Withdrawal Due to Cancellation' Type of Operation manually!"
AVAILABLE_BALANCE_VALID_ERROR = '​Available Balance Is Less Then Requested Balance!'


# CONSTANTS FOR FIELDS DESCRIPTION
AVAILABILITY_ACCOUNT_FIELD_DESCRIPTION = "Availability Accounting Account"
CBC_FORMAT_FIELD_DESCRIPTION = "CBC Format"
CBC_SHIPPING_OFFICE_FIELD_DESCRIPTION = "CBC Shipping Office"
INTEREST_ACCOUNT_FIELD_DESCRIPTION = "Interest Accounting Account"
INVESTMENT_ACCOUNT_FIELD_DESCRIPTION = "Investment Accounting Account"
LIABILITY_ACCOUNT_FIELD_DESCRIPTION = "Liability Accounting Account"
ORIGIN_RESORCE_FIELD_DESCRIPTION = "Origin of the resource"
SUPPORTING_DOCUMENTATION_FIELD_DESCRIPTION = "Supporting Documentation"
OPERATION_NUMBER = "Operation Number"
AGGREMENT_NUMBER = "Agreement Number"
TYPE_OF_OPERATION = "Type of Operation"

# CONSTANTS FOR SELECT FILES DESCRIPTION
OPENING_BALANCE = 'Opening Balance'
WITHDRAWAL_CANCEL = 'Withdrawal Due to Cancellation'


# CONSTANTS FOR ACTIONS
IR_ACTIONS_ACT_WINDOW = 'ir.actions.act_window' 

# CONSTANTS FOR FIELDS
BASES_COLLABORATION_ID = 'bases_collaboration_id.id'
MAIL_ACTIVITY_DATA = 'mail.mail_activity_data_todo'
MAIL_ACTIVITY = 'mail.activity'


class BasesCollabration(models.Model):

    _name = 'bases.collaboration'
    _inherit = [MAIL_THREAD_MODEL,MAIL_ACTIVITY_MIXIN_MODEL]
    _description = "Bases of collaboration"
    
    accounting_catalog = fields.Many2one(ACCOUNT_ACCOUNT_MODEL)
    name = fields.Char("Agreement Name")
    convention_no = fields.Char("Convention No.", copy=False)
    currency_id = fields.Many2one(
        RES_CURRENCY_MODEL, default=lambda self: self.env.user.company_id.currency_id)
    opening_bal = fields.Monetary("Opening Balance")
    available_bal = fields.Monetary("Available Balance", copy=False)
    agreement_type_id = fields.Many2one(
        AGREEMENT_AGREEMENT_TYPE_MODEL, 'Agreement Type')
    agreement_type_group = fields.Char("Agreement Type Group",size=1)
    
    fund_type_id = fields.Many2one(FUND_TYPE_MODEL, "Fund Type")
    fund_id = fields.Many2one(AGREEMENT_FUND_MODEL, 'Fund')
    dependency_obs = fields.Text("Dependency Observations")
    dependency_id = fields.Many2one('dependency', "Unit No.")
    desc_dependency = fields.Text("Description Dependency")
    subdependency_id = fields.Many2one(SUB_DEPENDENCY_MODEL, "Sub Dependency", domain="[('dependency_id','=',dependency_id)]")
    desc_subdependency = fields.Text("Sub-unit Name")
    origin_resource_id = fields.Many2one(
        SUB_ORIGIN_RESOURCE_MODEL, ORIGIN_RESORCE_FIELD_DESCRIPTION)
    goals = fields.Char("Goals")
    registration_date = fields.Date("Date of registration in the system")
    is_specific = fields.Boolean(string='Specific', default=False)
    liability_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, LIABILITY_ACCOUNT_FIELD_DESCRIPTION)
    investment_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, INVESTMENT_ACCOUNT_FIELD_DESCRIPTION)
    interest_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, INTEREST_ACCOUNT_FIELD_DESCRIPTION)
    availability_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, AVAILABILITY_ACCOUNT_FIELD_DESCRIPTION)
    state = fields.Selection([('draft', 'Draft'),
                              ('valid', 'Valid'),
                              ('in_force', 'In Force'),
                              ('to_be_cancelled', 'To Be Cancelled'),
                              ('cancelled', 'Cancelled')], "Status", default='draft')
    total_operations = fields.Integer(
        "Operations", compute="compute_operations")
    total_modifications = fields.Integer(
        "Modifications", compute="compute_modifications")
    request_open_balance_ids = fields.One2many(
        REQUEST_OPEN_BALANCE_MODEL, 'bases_collaboration_id')

    employee_id = fields.Many2one(HR_EMPLOYEE_MODEL, 'Holder of the unit')
    job_id = fields.Many2one('hr.job', string="Market Stall")
    phone = fields.Char(related="employee_id.work_phone",
                        string="Telephone of the unit holder")
    holder_email = fields.Char(
        related="employee_id.work_email", string="Email")

    administrative_secretary_id = fields.Many2one(
        HR_EMPLOYEE_MODEL, "Administrative Secretary")
    administrative_secretary_phone = fields.Char(
        related="administrative_secretary_id.work_phone", string="Administrative Secretary Telephone")
    administrative_secretary_email = fields.Char(
        related="administrative_secretary_id.work_email", string="Administrative Secretary Email")

    direct_manager_cbc_id = fields.Many2one(
        HR_EMPLOYEE_MODEL, "Direct manager of CBC")
    cbc_responsible_phone = fields.Char(
        related="direct_manager_cbc_id.work_phone", string="CBC responsible phone number")
    email = fields.Char(
        related="direct_manager_cbc_id.work_email", string="Email")
    unit_address = fields.Text("Unit Address")
    additional_observation = fields.Text(
        "Additional observations of the agency")
    cbc_format = fields.Binary(CBC_FORMAT_FIELD_DESCRIPTION)
    cbc_shipping_office = fields.Binary(CBC_SHIPPING_OFFICE_FIELD_DESCRIPTION)
    committe_ids = fields.One2many(
        'committee', 'collaboration_id', string="Committees")

    no_beneficiary_allowed = fields.Integer("Number of allowed beneficiaries")
    beneficiary_ids = fields.One2many(
        COLLABORATION_BENEFICIARY_MODEL, 'collaboration_id')
    provider_ids = fields.One2many(
        COLLABORATION_PROVIDERS_MODEL, 'collaboration_id')

    cancel_date = fields.Date("Cancellation date")
    supporing_doc = fields.Binary(SUPPORTING_DOCUMENTATION_FIELD_DESCRIPTION)
    reason_cancel = fields.Text("Reason for Cancellations")

    fund_name_transfer_id = fields.Many2one(
        BASES_COLLABORATION_MODEL, 'Fund name for transfers')
    closing_amt = fields.Monetary("Amount")

    interest_date = fields.Date(string="Interest Date")
    interest_rate = fields.Monetary(string="Interest Rate")
    yields = fields.Monetary(string="Yields")

    report_start_date = fields.Date("Report Start Date")
    report_end_date = fields.Date("Report End Date")
    n_report = fields.Char(string="N° para reporte")
    next_no = fields.Integer(string="Next Number")
    journal_id = fields.Many2one(ACCOUNT_JOURNAL_MODEL)
    move_line_ids = fields.One2many(
        ACCOUNT_MOVE_LINE_MODEL, 'collaboration_id', string="Journal Items")
    rate_base_ids = fields.One2many('interest.rate.base','base_id')

    periods_closing_ids = fields.One2many('periods.closing', 'collaboration_id', "Closing Periods")
    origin_resource_ids = fields.One2many(ORIGIN_RESOURCE_LINE_MODEL, 'collaboration_id', 'Origin Resource')

    
    _sql_constraints = [
        ('folio_convention_no', 'unique(convention_no)', 'The Convention No. must be unique.')]

    origin_code = fields.Char(String="Code")

    total_request_payment = fields.Integer(compute="compute_total_request_payment")

    line_ids_base_collaboration_balance_history = fields.One2many(BASES_COLLABORATION_BALANCE_HISTORY_MODEL, 'bases_collaboration_id')

    line_ids_base_collaboration_montly_performance = fields.One2many(BASES_COLLABORATION_MONTHLY_PERFORMANCE_MODEL, 'bases_collaboration_id')

    transfer_all_interest = fields.Boolean(string="Transfer all interest", default=False)

    close_journal_id = fields.Many2one(ACCOUNT_JOURNAL_MODEL)

    def compute_total_request_payment(self):
        # busca en el modelo open.request.balance los registros que esten viculados con self.id y los almacena en base_operation
        base_operation = self.env[REQUEST_OPEN_BALANCE_MODEL].search([('bases_collaboration_id', '=', self.id)])

        # crear un nuevo objeto donde se almacenan los registros de account.move que esten vinculados con cada uno de los registros en base_operation
        payment_reqs = self.env[ACCOUNT_MOVE_MODEL]
        for rec in base_operation:
            payment_reqs |= self.env[ACCOUNT_MOVE_MODEL].search([('collaboration_base_payment_request_id', '=', rec.id)])
        self.total_request_payment = len(payment_reqs)

    def get_all_base_collaboration_request_payment(self):

        # busca en el modelo open.request.balance los registros que esten viculados con self.id y los almacena en base_operation
        base_operation = self.env[REQUEST_OPEN_BALANCE_MODEL].search([('bases_collaboration_id', '=', self.id)])

        # crear un nuevo objeto donde se almacenan los registros de account.move que esten vinculados con cada uno de los registros en base_operation
        payment_reqs = self.env[ACCOUNT_MOVE_MODEL]
        for rec in base_operation:
            payment_reqs |= self.env[ACCOUNT_MOVE_MODEL].search([('collaboration_base_payment_request_id', '=', rec.id)])

        # Definir valores predeterminados en el contexto
        default_context = {
            'payment_request': True,
            'show_for_supplier_payment': True,
            'default_is_payment_request': 1,
            'from_move': 1,
            'group_by': 'l10n_mx_edi_payment_method_id',
            'default_type': 'in_invoice',
            'create': False,

        }

        return {
            'name': 'Solicitudes de Pago',
            'view_mode': VIEW_MODE_TREE_FORM,
            'res_model': ACCOUNT_MOVE_MODEL,
            'domain': [('id', 'in', payment_reqs.ids)],
            'type': IR_ACTIONS_ACT_WINDOW,
            'views': [(self.env.ref(JT_SUPPLIER_PAYMENT_PAYMENT_REQ_REQ_TREE_VIEW).id, 'tree'), (self.env.ref(SIIF_SUPPLIER_PAYMENT_PAYMENT_REQUEST_GENERAL_FORM_VIEW).id, 'form')],
            'context': default_context,
        }


    @api.model
    def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
        args = args or []
        domain = []
        if name:
            domain = ['|', ('convention_no', '=ilike', name), ('name', operator, name)]
            if operator in expression.NEGATIVE_TERM_OPERATORS:
                domain = ['&', '!'] + domain[1:]
        base_ids = self._search(expression.AND([domain, args]), limit=limit, access_rights_uid=name_get_uid)
        return models.lazy_name_get(self.browse(base_ids).with_user(name_get_uid))
        
    def name_get(self):
        if not 'show_agreement_name' in self._context:
            res = []
            for base in self:
                base_name = ''
                if base.convention_no:
                    base_name = base.convention_no
                res.append((base.id, base_name))
        else:
            res = super(BasesCollabration, self).name_get()
        return res

    def unlink(self):
        for rec in self:
            if rec.state not in ['draft']:
                raise UserError(_('You can erase only draft status data.'))
        return super(BasesCollabration, self).unlink()

    @api.constrains('n_report')
    def _check_n_report(self):
        if self.n_report and not self.n_report.isnumeric():
            raise ValidationError(_('N° para reporte must be Numeric.'))
        if self.n_report and len(self.n_report) != 8:
            raise ValidationError(_('N° para reporte must be 8 Digits.'))

    @api.onchange('dependency_id', 'agreement_type_id')
    def onchange_dependency_agrtype(self):
        for rec in self:
            if rec.dependency_id and self.agreement_type_id and self.agreement_type_id.group:
                rec.convention_no = '%s' % (
                    rec.dependency_id.dependency) + '%s' % (rec.agreement_type_id.group)

    @api.onchange('dependency_id', 'agreement_type_id', 'subdependency_id','convention_no')
    def onchange_deb_sub_agr(self):
        for rec in self:
            if rec.dependency_id and rec.agreement_type_id and rec.subdependency_id:
                group = rec.agreement_type_id.group and rec.agreement_type_id.group or ''
                con = ''
                if rec.convention_no and len(rec.convention_no)==6:
                   con =  rec.convention_no[-2:]
                rec.n_report = '%s' % (rec.dependency_id.dependency) + '%s' % (
                    rec.subdependency_id.sub_dependency) + '%s' % (group) + '%s' % (con)

    def get_month_name(self, month):
        month_name = ''
        if month == 1:
            month_name = 'Enero'
        elif month == 2:
            month_name = 'Febrero'
        elif month == 3:
            month_name = 'Marzo'
        elif month == 4:
            month_name = 'Abril'
        elif month == 5:
            month_name = 'Mayo'
        elif month == 6:
            month_name = 'Junio'
        elif month == 7:
            month_name = 'Julio'
        elif month == 8:
            month_name = 'Agosto'
        elif month == 9:
            month_name = 'Septiembre'
        elif month == 10:
            month_name = 'Octubre'
        elif month == 11:
            month_name = 'Noviembre'
        elif month == 12:
            month_name = 'Diciembre'

        return month_name.upper()

    def get_amount_invest_dict(self, collaborations, start_date, end_date):
        balance_dict = {}
        increments = intial_bal = withdrawals = interest = 0
        for collab in collaborations:
            collaboration = self.browse(collab)
            deposite = sum(
              x.opening_balance for x in collaboration.request_open_balance_ids.filtered(lambda x: x.state == 'confirmed'
                                             and x.request_date and x.request_date < start_date and \
                                                x.type_of_operation in ('open_bal', 'increase', 'increase_by_closing')))
            retiros = sum(x.opening_balance for x in collaboration.request_open_balance_ids.filtered(lambda x:
                                            x.state == 'confirmed'
                                             and x.request_date and x.request_date < start_date and \
                    x.type_of_operation in ('retirement', 'withdrawal', 'withdrawal_cancellation', 'withdrawal_closure')))
            intial_bal = deposite - retiros
            operations = collaboration.request_open_balance_ids.filtered(lambda x:x.state=='confirmed'
                    and x.request_date and x.request_date >= start_date and x.request_date <= end_date)
            for operation in operations:
                if operation.type_of_operation in ('open_bal', 'increase','increase_by_closing'):
                    increments += operation.opening_balance
                elif operation.type_of_operation in ('retirement', 'withdrawal_cancellation', 'withdrawal',
                                                     'withdrawal_closure'):
                    withdrawals += operation.opening_balance
            interest = sum(x.interest_rate for x in collaboration.rate_base_ids.filtered(lambda x:x.interest_date and x.interest_date >= start_date and x.interest_date <= end_date))
            interest = round(interest,2)
        balance_dict.update({
            'intial_bal': intial_bal,
            'increments': increments,
            'sub_total': intial_bal + increments,
            'withdrawals': withdrawals,
            'balance_at_the_end': (intial_bal + increments) - withdrawals,
            'interest': interest,
            'to_be_invested': ((intial_bal + increments) - withdrawals) + interest

        })
        return balance_dict

    def get_interes(self):
        interest = sum(x.interest_rate for x in self.rate_base_ids.filtered(lambda x:x.interest_date and x.interest_date >= self.report_start_date and x.interest_date <= self.report_end_date))
        interest = round(interest,2)
        
        return interest

    def get_next_year_name(self, date):
        year_name = ''
        if date:
            year = date.year + 1
            year_name = str(year)
        return year_name

    def get_date_name(self, date):
        period_name = ''
        if date:
            period_name += str(date.day) + ' ' + self.get_month_name(date.month) + ' ' + str(date.year)
        return period_name

    def get_period_name_amount_invest(self, start_date, end_date):
        period_name = ''
        if start_date and end_date:
            period_name += "Del " + str(start_date.day)

            if start_date.month != end_date.month:
                if start_date.year == end_date.year:
                    period_name += " de " + \
                                   self.get_month_name(start_date.month)

            if start_date.year != end_date.year:
                period_name += " de " + \
                               self.get_month_name(
                                   start_date.month) + " de" + str(start_date.year)

            period_name += " al " + str(end_date.day) + " de " + self.get_month_name(end_date.month) + " " + \
                           str(end_date.year)

        return period_name

    def get_period_name(self):
        period_name = ''
        if self.report_start_date and self.report_end_date:
            period_name += "Del " + str(self.report_start_date.day)

            if self.report_start_date.month != self.report_end_date.month:
                if self.report_start_date.year == self.report_end_date.year:
                    period_name += " de " + \
                        self.get_month_name(self.report_start_date.month)

            if self.report_start_date.year != self.report_end_date.year:
                period_name += " de " + \
                    self.get_month_name(
                        self.report_start_date.month) + " de" + str(self.report_start_date.year)

            period_name += " al " + str(self.report_end_date.day) + " de " + self.get_month_name(
                self.report_end_date.month) + " " + str(self.report_end_date.year)

        return period_name

    def get_opening_balance(self):
        deposite = sum(x.opening_balance for x in self.request_open_balance_ids.filtered(lambda x: x.state == 'confirmed'
              and x.request_date and x.request_date < self.report_start_date and \
              x.type_of_operation in ('open_bal', 'increase', 'increase_by_closing')))
        retiros = sum(x.opening_balance for x in self.request_open_balance_ids.filtered(lambda x: x.state == 'confirmed'
              and x.request_date and x.request_date < self.report_start_date and \
              x.type_of_operation in ('retirement', 'withdrawal', 'withdrawal_cancellation', 'withdrawal_closure')))
        bal = deposite - retiros
        return bal

    def get_deposite(self):
        deposite = sum(x.opening_balance for x in self.request_open_balance_ids.filtered(lambda x: x.state == 'confirmed' and x.request_date and x.request_date >=
                                                                                         self.report_start_date and x.request_date <= self.report_end_date and x.type_of_operation in ('open_bal', 'increase', 'increase_by_closing')))
        return deposite

    def get_retiros(self):
        retiros = sum(x.opening_balance for x in self.request_open_balance_ids.filtered(lambda x: x.state == 'confirmed' and x.request_date and x.request_date >=
                                                                                        self.report_start_date and x.request_date <= self.report_end_date and x.type_of_operation in ('retirement', 'withdrawal', 'withdrawal_cancellation', 'withdrawal_closure')))
        return retiros
    
    def opening_opt_number(self):
        opt_ids = self.request_open_balance_ids.filtered(lambda x:x.type_of_operation=='open_bal')
        opt_number = ''
        if opt_ids:
            opt_number = opt_ids[0].operation_number
        return opt_number

    def opening_opt_date(self):
        opt_ids = self.request_open_balance_ids.filtered(lambda x:x.type_of_operation=='open_bal')
        opt_date = ''
        if opt_ids:
            opt_date = opt_ids[0].request_date
        return opt_date
    
    def get_contract_assistant_report_lines(self):

        req_date = self.request_open_balance_ids.filtered(lambda x: self.report_start_date and self.report_end_date \
            and x.state=='confirmed' and x.type_of_operation in ('increase','retirement') and \
            x.request_date and x.request_date >= self.report_start_date and x.request_date <= self.report_end_date).mapped('request_date')

        req_date += self.rate_base_ids.filtered(lambda x: self.report_start_date and self.report_end_date and \
            x.interest_date >= self.report_start_date and \
           x.interest_date <= self.report_end_date).mapped('interest_date')

        lang = self.env.user.lang

        if req_date:
            req_date = list(set(req_date))
            req_date =  sorted(req_date)
        
        final = 0
        lines = []
        for req in req_date:
            opt_lines = self.request_open_balance_ids.filtered(lambda x:x.state=='confirmed' and x.request_date == req)
            for line in opt_lines:
                opt = dict(line._fields['type_of_operation'].selection).get(line.type_of_operation)
                if lang == 'es_MX':
                    if line.type_of_operation=='increase':
                        opt = 'Incremento'
                    elif line.type_of_operation=='open_bal':
                        opt = 'Importe de apertura'
                    elif line.type_of_operation=='retirement':
                        opt = 'Retiro'
                debit = 0
                credit = 0  
                bal_final = 0

                if line.type_of_operation in ('increase'):         
                    final += line.opening_balance
                    debit = line.opening_balance
                elif line.type_of_operation in ('retirement'):
                    final -= line.opening_balance
                    credit = line.opening_balance
                intial_bal = 0
                intial_bal = debit - credit
                bal_final = intial_bal + debit - credit 
                lines.append({
                              'date':line.request_date,
                              'operation_number':line.operation_number,
                              'inital_bal':intial_bal,
                              'opt': opt,
                              'debit':debit,
                              'credit' : credit,
                              'final' : final,
                              'bal_final':bal_final
                              })

            for line in self.rate_base_ids.filtered(lambda x:x.interest_date == req):
                final += line.interest_rate
                lines.append({
                              'date':line.interest_date,
                              'operation_number':'',
                              'opt': 'Intereses' if lang == 'es_MX' else 'Interest',
                              'debit':line.interest_rate,
                              'credit' : 0.0,
                              'final' : final,

                              })
        
        return lines

    def get_report_lines(self):

        req_date = self.request_open_balance_ids.filtered(lambda x: self.report_start_date and self.report_end_date \
            and x.state=='confirmed' and \
            x.request_date and x.request_date >= self.report_start_date and x.request_date <= self.report_end_date).mapped('request_date')

        lang = self.env.user.lang
        req_date += self.rate_base_ids.filtered(lambda x: self.report_start_date and self.report_end_date and \
            x.interest_date >= self.report_start_date and \
           x.interest_date <= self.report_end_date).mapped('interest_date')

        if req_date:
            req_date = list(set(req_date))
            req_date =  sorted(req_date)
        
        final = 0
        lines = []
        for req in req_date:
            opt_lines = self.request_open_balance_ids.filtered(lambda x:x.state=='confirmed' and x.request_date == req)
            for line in opt_lines:
                opt = dict(line._fields['type_of_operation'].selection).get(line.type_of_operation)
                if lang == 'es_MX':
                    if line.type_of_operation=='open_bal':
                        opt = 'Importe de apertura'
                    elif line.type_of_operation=='increase':
                        opt = 'Incremento'
                    elif line.type_of_operation=='retirement':
                        opt = 'Retiro'
                    elif line.type_of_operation=='withdrawal':
                        opt = 'Retiro por liquidación'
                    elif line.type_of_operation=='withdrawal_cancellation':
                        opt = 'Retiro por cancelación'
                    elif line.type_of_operation=='withdrawal_closure':
                        opt = 'Retiro por cierre'
                    elif line.type_of_operation=='increase_by_closing':
                        opt = 'Incremento por cierre'
                debit = 0
                credit = 0  
                bal_final = 0
                if line.type_of_operation in ('open_bal','increase','increase_by_closing'):         
                    final += line.opening_balance
                    debit = line.opening_balance
                elif line.type_of_operation in ('withdrawal','retirement','withdrawal_cancellation','withdrawal_closure'):
                    final -= line.opening_balance
                    credit = line.opening_balance
                intial_bal = 0
                intial_bal = debit - credit
                bal_final = intial_bal + debit - credit 
                lines.append({
                              'date':line.request_date,
                              'operation_number':line.operation_number,
                              'inital_bal':intial_bal,
                              'opt': opt,
                              'debit':debit,
                              'credit' : credit,
                              'final' : final,
                              'bal_final':bal_final
                              })

            for line in self.rate_base_ids.filtered(lambda x:x.interest_date == req):
                final += line.interest_rate
                lines.append({
                              'date':line.interest_date,
                              'opt': 'Intereses' if lang == 'es_MX' else 'Interest',
                              'debit':line.interest_rate,
                              'credit' : 0.0,
                              'final' : final,

                              })
        
        return lines

    @api.model
    def create(self, vals):
        res = super(BasesCollabration, self).create(vals)
        if res.dependency_id:
            res.desc_dependency = res.dependency_id.description
        if res.subdependency_id:
            res.desc_subdependency = res.subdependency_id.description
        if res.agreement_type_id and res.agreement_type_group:
            type_id = self.env[AGREEMENT_AGREEMENT_TYPE_MODEL].search([('group','=',res.agreement_type_group),('name','=',res.agreement_type_id.name)],limit=1)
            if type_id:
                res.agreement_type_id = type_id.id
                
        if res and res.beneficiary_ids:
            if not res.no_beneficiary_allowed or (res.no_beneficiary_allowed and
                                                  res.no_beneficiary_allowed < len(res.beneficiary_ids)):
                raise ValidationError(_("You can add only %s Beneficiaries which is mentined in "
                                        "'Number of allowed beneficiaries'" % res.no_beneficiary_allowed))
        no = 0
        for ben in res.beneficiary_ids:
            no = no + 1
            ben.sequence = no
        return res

    def write(self, vals):
        res = super(BasesCollabration, self).write(vals)
        for rec in self:
            if rec and rec.beneficiary_ids:
                if not rec.no_beneficiary_allowed or (rec.no_beneficiary_allowed and
                                                      rec.no_beneficiary_allowed < len(rec.beneficiary_ids)):
                    raise ValidationError(_("You can add only %s Beneficiaries which is mentined in "
                                            "'Number of allowed beneficiaries'" % rec.no_beneficiary_allowed))
        if vals.get('beneficiary_ids'):
            for rec in self:
                no = 0
                for ben in rec.beneficiary_ids:
                    no = no + 1
                    ben.sequence = no

        return res

    def compute_operations(self):
        operation_obj = self.env[REQUEST_OPEN_BALANCE_MODEL]
        for rec in self:
            if rec.name:
                operations = operation_obj.search(
                    [('bases_collaboration_id', '=', rec.id)])
                rec.total_operations = len(operations)

    def compute_modifications(self):
        modification_obj = self.env[BASES_COLLABORATION_MODIFICATION_MODEL]
        for rec in self:
            if rec.name:
                modifications = modification_obj.search(
                    [('bases_collaboration_id', '=', rec.id)])
                rec.total_modifications = len(modifications)

    def action_closing_collaboration(self):
        return {
            'name': 'Closing Collaboration',
            'view_mode': 'form',
            'view_id': self.env.ref('jt_agreement.closing_collaboration_form_view').id,
            'res_model': 'closing.collaboration',
            'type': IR_ACTIONS_ACT_WINDOW,
            'target': 'new',
            'context': {'active_ids': self.ids}
        }

    def cancel(self):
        return {
            'name': 'Cancel Collaboration',
            'view_mode': 'form',
            'view_id': self.env.ref('jt_agreement.cancel_collaboration_form_view').id,
            'res_model': 'cancel.collaboration',
            'type': IR_ACTIONS_ACT_WINDOW,
            'target': 'new'
        }

    
        
    def action_operations(self):
        operation_obj = self.env[REQUEST_OPEN_BALANCE_MODEL]
        operations = operation_obj.search([('bases_collaboration_id', '=', self.id)])

        view_mode = 'form'
        if operations:
            view_mode = VIEW_MODE_TREE_FORM

        return {
            'name': 'Operaciones',
            'view_mode': view_mode,
            'views': [
                (self.env.ref("jt_agreement.view_req_open_balance_tree").id, 'tree'),
                (self.env.ref("jt_agreement.view_req_open_balance_form").id, 'form')
            ],
            'res_model': REQUEST_OPEN_BALANCE_MODEL,
            'domain': [('bases_collaboration_id', '=', self.id)],
            'type': IR_ACTIONS_ACT_WINDOW,
            'context': {
                'default_bases_collaboration_id': self.id,
                'default_apply_to_basis_collaboration': True,
                'default_agreement_number': self.convention_no,
                'default_name': self.name,
                'default_operation_number': self.next_no,
                'default_opening_balance': 0 if operations else self.opening_bal,
                'default_liability_account_id': self.liability_account_id.id or False,
                'default_interest_account_id': self.interest_account_id.id or False,
                'default_investment_account_id': self.investment_account_id.id or False,
                'default_availability_account_id': self.availability_account_id.id or False,
                'default_journal_id': self.journal_id.id,
                'default_accounting_catalog': self.accounting_catalog.id,
            }
        }
    

    def action_modifications(self):
        modification_obj = self.env[BASES_COLLABORATION_MODIFICATION_MODEL]
        modifications = modification_obj.search(
            [('bases_collaboration_id', '=', self.id)])
        if modifications:
            return {
                'name': 'Modifications',
                'view_type': 'form',
                'view_mode': VIEW_MODE_TREE_FORM,
                'res_model': BASES_COLLABORATION_MODIFICATION_MODEL,
                'domain': [('bases_collaboration_id', '=', self.id)],
                'type': IR_ACTIONS_ACT_WINDOW,
                'context': {'default_dependency_id': self.dependency_id and self.dependency_id.id or False,
                            'default_bases_collaboration_id': self.id,
                            'default_current_target': self.goals,
                            'from_modification': True,
                            'show_agreement_name': True
                            }
            }
        else:
            return {
                'name': 'Modifications',
                'view_mode': 'form',
                'res_model': BASES_COLLABORATION_MODIFICATION_MODEL,
                'domain': [('bases_collaboration_id', '=', self.id)],
                'type': IR_ACTIONS_ACT_WINDOW,
                'context': {'default_dependency_id': self.dependency_id and self.dependency_id.id or False,
                            'default_bases_collaboration_id': self.id,
                            'default_current_target': self.goals,
                            'from_modification': True
                            }
            }

    def confirm(self):
        self.state = 'valid'

        if self.opening_bal == 0:
            raise ValidationError(_("Please add the opening balance amount"))

    def action_schedule_withdrawal(self):
        req_obj = self.env[REQUEST_OPEN_BALANCE_MODEL]
        for collaboration in self:
            for beneficiary in collaboration.beneficiary_ids:
                if beneficiary.validity_start and beneficiary.validity_final_beneficiary and beneficiary.withdrawal_sch_date and beneficiary.payment_rule_id:

                    total_month = (beneficiary.validity_final_beneficiary.year - beneficiary.validity_start.year) * 12 + (
                        beneficiary.validity_final_beneficiary.month - beneficiary.validity_start.month)
                    start_date = beneficiary.validity_start
                    req_date = start_date.replace(
                        day=beneficiary.withdrawal_sch_date.day)

                    need_skip = 1
                    if beneficiary.payment_rule_id.payment_period == 'bimonthly':
                        need_skip = 2
                    elif beneficiary.payment_rule_id.payment_period == 'quarterly':
                        need_skip = 3
                    elif beneficiary.payment_rule_id.payment_period == 'biquarterly':
                        need_skip = 6
                    elif beneficiary.payment_rule_id.payment_period == 'annual':
                        need_skip = 12
                    elif beneficiary.payment_rule_id.payment_period == 'biannual':
                        need_skip = 24

                    count = 0

                    for month in range(total_month + 1):
                        if month != 0:
                            req_date = req_date + relativedelta(months=1)

                        if count != 0:
                            count += 1
                            if count == need_skip:
                                count = 0
                            continue

                        count += 1
                        if count == need_skip:
                            count = 0
                        if beneficiary.partner_id:
                            partner_id = beneficiary.partner_id.id
                        else: 
                            partner_id = beneficiary.employee_id and beneficiary.employee_id.user_id and beneficiary.employee_id.user_id.partner_id and beneficiary.employee_id.user_id.partner_id.id or False
                        req_obj.create({
                            'bases_collaboration_id': collaboration.id,
                            'collaboration_beneficiary_id' : beneficiary.id,
                            'apply_to_basis_collaboration': True,
                            'agreement_number': collaboration.convention_no,
                            'opening_balance': beneficiary.amount,
                            'supporting_documentation': collaboration.cbc_format,
                            'type_of_operation': 'retirement',
                            'beneficiary_id': partner_id,
                            'name': collaboration.name,
                            'request_date': req_date,
                            'liability_account_id': collaboration.liability_account_id.id if collaboration.liability_account_id
                            else False,
                            'interest_account_id': collaboration.interest_account_id.id if collaboration.interest_account_id
                            else False,
                            'investment_account_id': collaboration.investment_account_id.id if collaboration.investment_account_id
                            else False,
                            'availability_account_id': collaboration.availability_account_id.id if collaboration.availability_account_id
                            else False
                        })
        print ("Calllll===")
        notification = {
            'type': 'ir.actions.client',
            'tag': 'display_notification',
            'params': {
                'title': _('¡Operations were successful!'),
                'message': _('Enter the BD to continue with the process'),
                'type': 'success',  # types: success,warning,danger,info
                'sticky': True,  # True/False will display for few seconds if false
            },
        }
        return notification

    @api.constrains('convention_no')
    def _check_convention_no(self):
        if self.convention_no and not self.convention_no.isnumeric():
            raise ValidationError(_('Convention No must be Numeric.'))
        if self.convention_no and len(self.convention_no) != 6:
            raise ValidationError(_('Convention No must be 6 characters.'))
        if self.dependency_id and self.agreement_type_id and self.agreement_type_id.group:
            name = self.dependency_id.dependency + self.agreement_type_id.group
            if not self.convention_no.startswith(name):
                raise ValidationError(
                    _('First 4 character of Convention must be Dependency and Group.'))

    @api.onchange('dependency_id', 'subdependency_id')
    def onchange_dep_subdep(self):
        if self.dependency_id or self.subdependency_id:
            number = ''
            if self.dependency_id:
                number += self.dependency_id.dependency
                self.desc_dependency = self.dependency_id.description
            if self.subdependency_id:
                number += self.subdependency_id.sub_dependency
                self.desc_subdependency = self.subdependency_id.description

    @api.onchange('agreement_type_id')
    def onchange_agreement_type_id(self):
        if self.agreement_type_id and self.agreement_type_id.fund_type_id:
            self.fund_type_id = self.agreement_type_id.fund_type_id.id

    def cron_capture_base_collaboration_balance(self):
        date = fields.Date.today()
        obj_productive_account = self.env['productive.account']
        rate = obj_productive_account.get_tiie_today(date)
        obj_base = self.env[BASES_COLLABORATION_BALANCE_HISTORY_MODEL]
        obj = self.search([('state','=','valid')])
        for record in obj:
            if not record.verify_duplicate_balance_history_record(date):
                obj_base.create_balance_register(record.available_bal,rate,record.id,date)

    def verify_duplicate_balance_history_record(self,date):
        if self.env[BASES_COLLABORATION_BALANCE_HISTORY_MODEL].search([('date','=',date),(BASES_COLLABORATION_ID,'=',self.id)]):
            return True

    # funcion para retornar una vista que manda los saldos de account.balance.history relacionados con el campo bases_collaboration_id.id
    def get_record_base_balance_history(self):
        return {
            'name': 'Historial de saldos',
            'view_mode': VIEW_MODE_TREE_FORM,
            'res_model': BASES_COLLABORATION_BALANCE_HISTORY_MODEL,
            'domain': [(BASES_COLLABORATION_ID, '=', self.id)],
            'type': IR_ACTIONS_ACT_WINDOW,
        }
    
    def calculate_interest_distribution(self,date_for_calculate):
        obj_balance_history = self.env[BASES_COLLABORATION_BALANCE_HISTORY_MODEL]
        average_balance  = obj_balance_history.get_average_balance_history_in_month(date_for_calculate.month,date_for_calculate.year,self.id)
        if average_balance and self.state == 'valid':

            obj_productive_account = self.env['productive.account']
            average_rate = obj_productive_account.get_rate_in_month('TIIE',date_for_calculate) 
            average_rate = float(average_rate)
            days = calendar.monthrange(date_for_calculate.year,date_for_calculate.month)[1]        
            interest = average_balance *(average_rate/100)/360*days

            data = {
                    'date': date_for_calculate,
                    'bases_collaboration_id': self.id,
                    'performance': interest,
                    'average_rate_in_month': average_rate,
                    'average_balance_in_month': average_balance,
                    'days_of_period': days
                }

            self.env[BASES_COLLABORATION_MONTHLY_PERFORMANCE_MODEL].create(data)


            # Cierre de mes           
            
            if not self.is_specific and self.fund_name_transfer_id:
                if self.transfer_all_interest:

                    self.fund_name_transfer_id.available_bal += interest
                    self.accounting_action(self.journal_id.default_debit_account_id.code, self.close_journal_id.default_credit_account_id.code, interest, "Cierre de mes",date_for_calculate)
                elif self.closing_amt > 0:
                    self.available_bal += interest - self.closing_amt

                    self.fund_name_transfer_id.available_bal += self.closing_amt
                    self.accounting_action(self.journal_id.default_debit_account_id.code, self.close_journal_id.default_credit_account_id.code, self.closing_amt, "Cierre de mes",date_for_calculate)
            else:
                self.available_bal += interest


            
    
    def calculate_all_interest_distribution_monthly(self,calculation_date):

        if not self.validate_last_day_of_month(calculation_date):
            raise UserError(_('The calculation date must be the last day of the month.'))

        obj = self.search([('state','=','valid')])
        for record in obj:
            if not record.validate_duplicate_record_base_collaboration_monthly_performance(calculation_date):
                record.calculate_interest_distribution(calculation_date)

    def action_interest_distribution(self,calculation_date):
        active_ids = self._context.get('active_ids')
        collaborations = self.env[BASES_COLLABORATION_MODEL].browse(active_ids)

        if not self.validate_last_day_of_month(calculation_date):
            raise UserError(_('The calculation date must be the last day of the month.'))
        
        for record in collaborations:
            if not record.validate_duplicate_record_base_collaboration_monthly_performance(calculation_date):
                record.calculate_interest_distribution(calculation_date)

    def validate_duplicate_record_base_collaboration_monthly_performance(self,date):
        if self.env[BASES_COLLABORATION_MONTHLY_PERFORMANCE_MODEL].search([('date','=',date),(BASES_COLLABORATION_ID,'=',self.id)]):
            return True

    def validate_last_day_of_month(self,date):
        if date.day == calendar.monthrange(date.year,date.month)[1]:
            return True
        
    def call_bases_collaboration_action_interest_distribution_view(self):
        active_ids = self._context.get('active_ids', [])
        return {
            'name': 'Calcular distribucion de intereses',
            'view_mode': 'form',
            'res_model': 'calculate.interest.distribution.wizard',
            'type': IR_ACTIONS_ACT_WINDOW,
            'target': 'new',
            'view_type': 'form',
            'views': [(self.env.ref("jt_agreement.bases_collaboration_action_interest_distribution_view").id, 'form')],
            'context': {
            'active_ids': active_ids,
            },
        }
    
    def call_bases_collaboration_calculate_interest_distribution_view(self):
        return {
            'name': 'Calcular distribucion de intereses',
            'view_mode': 'form',
            'res_model': 'calculate.interest.distribution.wizard',
            'type': IR_ACTIONS_ACT_WINDOW,
            'target': 'new',
            'view_type': 'form',
            'views': [(self.env.ref("jt_agreement.bases_collaboration_calculate_interest_distribution_wizard").id, 'form')],

        }
    
    def accounting_action(self, debit, credit, amount, description, date_register):
        #Debit es donde se va a cargar el dinero
        #Credit es donde se va a abonar el dinero
        move_id = ''
        today = date_register
        partner_id = self.env.user.partner_id
        account_account_obj = self.env[ACCOUNT_ACCOUNT_MODEL]

        debit_account = account_account_obj.search([('code', '=', debit)])
        credit_account = account_account_obj.search([('code', '=', credit)])

        move_vals = {'ref': '', 'conac_move': False,
                            'date': today, 'journal_id': self.journal_id.id, 'company_id': self.env.user.company_id.id,
                            'line_ids': [(0, 0, {
                                'name':	 description,
                                'account_id': debit_account.id,
                                'debit': amount,
                                'partner_id': partner_id.id,
                                'collaboration_id': self.id,
                            }),
                                (0, 0, {
                                    'name':	 description,
                                    'account_id': credit_account.id,
                                    'credit': amount,
                                    'partner_id': partner_id.id,
                                    'collaboration_id': self.id,
                                }),
                            ]}

        move_obj = self.env[ACCOUNT_MOVE_MODEL]
        move_id = move_obj.create(move_vals)
        move_id.action_post()
        return move_id
    

class BasesCollaborationBalanceHistory(models.Model):
    _inherit = 'bases.collaboration.balance.history'
    bases_collaboration_id = fields.Many2one(BASES_COLLABORATION_MODEL, string='Bases collaboration')

class BasesCollaborationMonthlyInterest(models.Model):
    _inherit = 'bases.collaboration.monthly.performance'
    bases_collaboration_id = fields.Many2one(BASES_COLLABORATION_MODEL, string='Bases Collaboration')

class InterestRateBase(models.Model):
    _name = 'interest.rate.base'
    _rec_name = 'interest_date'
    
    interest_date = fields.Date('Interest Date')
    interest_rate = fields.Float('Interest Rate')
    cuenta = fields.Char('Cuenta')
    tipo = fields.Char('Tipo')
    base_id = fields.Many2one(BASES_COLLABORATION_MODEL,'Base')

class Committe(models.Model):

    _name = 'committee'
    _description = "Committee"

    column_id = fields.Many2one(HR_EMPLOYEE_MODEL, "Column Name")
    column_position_id = fields.Many2one(
        'hr.job', "Job")
    collaboration_id = fields.Many2one(BASES_COLLABORATION_MODEL)
    position_column = fields.Char("Position / Appointment column")
    
    @api.onchange('column_id')
    def onchange_column_id(self):
        if self.column_id and self.column_id.job_id:
            self.column_position_id = self.column_id.job_id.id


class Providers(models.Model):
    _name = 'collaboration.providers'
    _description = "Collaboration Providers"

    collaboration_id = fields.Many2one(BASES_COLLABORATION_MODEL)
    partner_id = fields.Many2one(RES_PARTNER_MODEL, "Name")
    bank_id = fields.Many2one(RES_PARTNER_BANK_MODEL, "Bank")
    account_number = fields.Char("Account Number")

    @api.onchange('bank_id')
    def onchage_bank(self):
        if self.bank_id:
            self.account_number = self.bank_id.acc_number


class Beneficiary(models.Model):
    _name = 'collaboration.beneficiary'
    _description = "Collaboration Beneficiary"

    collaboration_id = fields.Many2one(BASES_COLLABORATION_MODEL)
    employee_id = fields.Many2one(HR_EMPLOYEE_MODEL, "Employee")
    partner_id =  fields.Many2one(RES_PARTNER_MODEL, "Contact")
    is_employee = fields.Boolean('Employee',copy=False,default=False)
    is_contact = fields.Boolean('Contact',copy=False,default=False
                                 )
    bank_id = fields.Many2one(RES_PARTNER_BANK_MODEL, "Bank")
    bank_account_ids = fields.Many2many(RES_PARTNER_BANK_MODEL,'rel_bank_account_ben','account_id','ben_id',compute='get_bank_account_ids')
    account_number = fields.Char("Account Number")
    currency_id = fields.Many2one(
        RES_CURRENCY_MODEL, default=lambda self: self.env.user.company_id.currency_id)
    amount = fields.Monetary("Payment Amount")
    payment_rule_id = fields.Many2one(
        'recurring.payment.template', "Payment Rule")
    validity_start = fields.Date("Validity of the beneficiary start")
    validity_final_beneficiary = fields.Date(
        "Validity of the Final Beneficiary")
    withdrawal_sch_date = fields.Date("Withdrawal scheduling date")
    sequence = fields.Integer()

    @api.depends('is_contact', 'is_employee', 'partner_id','employee_id')
    def get_bank_account_ids(self):
        for rec in self:
            if rec.is_contact and rec.partner_id:
                rec.bank_account_ids = [
                    (6, 0, rec.partner_id.bank_ids.ids)]
            else:
                rec.bank_account_ids = [(6, 0, rec.employee_id.bank_ids.ids)]

    @api.onchange('is_employee')
    def onchange_is_employee(self):
        if self.is_employee:
            self.is_contact = False
            self.partner_id = False
    @api.onchange('is_contact')
    def onchange_is_contact(self):
        if self.is_contact:
            self.is_employee = False
            self.employee_id = False
            
    @api.onchange('bank_id')
    def onchage_bank(self):
        if self.bank_id:
            self.account_number = self.bank_id.acc_number


class ResPartnerBank(models.Model):

    _inherit = 'res.partner.bank'

    def name_get(self):
        if 'from_agreement' in self._context:
            res = []
            for bank in self:
                bank_name = ''
                if bank.bank_id:
                    bank_name = bank.bank_id.name
                res.append((bank.id, bank_name))
        else:
            res = super(ResPartnerBank, self).name_get()
        return res

    for_agreements = fields.Boolean("Agreements",copy=False,default=False)
    for_investments = fields.Boolean("Investments",copy=False,default=False)

class ResPartner(models.Model):

    _inherit = 'res.partner'

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        if 'from_retier_operation' in self._context and 'collaboration_id' in self._context:
            collaboration = self.env[BASES_COLLABORATION_MODEL].browse(
                self._context.get('collaboration_id'))
            partner_ids = []
            if collaboration and collaboration.provider_ids:
                for provider in collaboration.provider_ids:
                    partner_ids.append(provider.partner_id.id)
            args = [['id', 'in', partner_ids]]

        res = super(ResPartner, self).name_search(
            name, args=args, operator=operator, limit=limit)
        return res


class RequestOpenBalance(models.Model):

    _name = REQUEST_OPEN_BALANCE_MODEL
    _inherit = [MAIL_THREAD_MODEL, MAIL_ACTIVITY_MIXIN_MODEL]
    _description = "Request to Open Balance"

    _rec_name = 'first_number'

    first_number = fields.Char('First Number:')
    date_test = fields.Date("Date")

    origin_code = fields.Char(String="Code")
    operations_type_catalog_id = fields.Many2one('operations.type.catalog', string='Operation type')


    accounting_catalog = fields.Many2one(ACCOUNT_ACCOUNT_MODEL)
    origin_resource_ids = fields.One2many(ORIGIN_RESOURCE_LINE_MODEL, 'collaboration_request_id', 'Origin Resource')
    dependency_id = fields.Many2one(related="bases_collaboration_id.dependency_id", store=False)
    subdependency_id = fields.Many2one(related="bases_collaboration_id.subdependency_id", store=False)
    operation_type = fields.Selection(
        related='operations_type_catalog_id.operation_type',
        selection=[('decrease', 'Decrease'), ('increase', 'Increase')],
        string='Operation Type',
        store=False
    )


    def action_from_origin_resource(self, origin_resource_ids, operation_type, date_approve = datetime.today().date()):
        lines_pa = []
        lines_ie = []
        lines_cc = []

        if not self.request_date:
            raise ValidationError(_('Add request date.'))
        # Trimestre de afectación del presupuesto
        month = self.request_date.month

        if 1 <= month <= 3:
            quarter = 1
        elif 4 <= month <= 6:
            quarter = 2
        elif 7 <= month <= 9:
            quarter = 3
        elif 10 <= month <= 12:
            quarter = 4
 
        for line in origin_resource_ids:
            amount = round(line.amount, 2)
            # Línea de Presupuesto asignado
            if line.egress_key_id.key == 'PA':
                lines_pa.append((line.program_code_id, amount))
            # Línea de Ingresos extraordinarios
            elif line.egress_key_id.key in ('IEMN'):
                lines_ie.append((line.account_ie_id, line.program_code_id, amount))
            # Línea de Cuenta contable
            elif line.egress_key_id.key == 'CC':
                lines_cc.append((line.account_id, line.dependency_id, line.sub_dependency_id, amount))    

        
        # Validación de la disponibilidad por cuenta contable
        message = self.env[ACCOUNT_MOVE_MODEL].validate_available_account(lines_cc)
        if len(message) > 0 and 'Insuficiencia Contable' in message[0]:
            raise ValidationError(_(message[0]))
        
        if len(lines_pa) > 0:
            # Disminución del disponible de códigos programáticos de PA
            ok, res = self.env[ACCOUNT_MOVE_MODEL].update_available_program_code_pa(lines_pa, quarter)
            if not ok:
                self.env.cr.rollback()
                return False, res
        
        if len(lines_ie) > 0:
            # Disminución del disponible de códigos programáticos de Ingresos extraordinarios
            ok, res = self.env[ACCOUNT_MOVE_MODEL].update_available_program_code_ie(lines_ie, quarter, date_approve)
            if not ok:
                self.env.cr.rollback()
                return False, res

        # Crea apuntes contables
        self.accounting_with_increase(lines_cc, lines_pa, lines_ie)
        # Actualiza el saldo disponible
        self.update_available_bal_amount(operation_type)

    def activate_open_bal(self):
        operation_type = self.operations_type_catalog_id.operation_type
        origin_resource_ids = self.origin_resource_ids

        if operation_type == 'increase' and not origin_resource_ids and int(self.operation_number) > 1:
            raise ValidationError(_('Add at least one origin resource.'))
        elif operation_type == 'increase':
            # Realiza accion dependiendo si es ingreso_extraordinario, cuenta contable
            self.action_from_origin_resource(origin_resource_ids, operation_type)
        elif operation_type == 'decrease':
            # Apunte contable en base a la cuenta configurada y diario/cuenta 
            self.accounting_with_operations_type_catalog(operation_type)
            self.create_payment_request = True
            
        self.state = 'approved'


    def do_increase(self):
        base = self.env[BASES_COLLABORATION_MODEL].search(
            [('id', '=', self.bases_collaboration_id.id)])
        base.available_bal += self.opening_balance

        self.accounting_action(self.journal_id.default_credit_account_id.code, 
                               self.journal_id.default_debit_account_id.code, self.opening_balance, "Incremento")

        self.state = 'approved'


    def do_retirement(self):
        base = self.env[BASES_COLLABORATION_MODEL].search(
            [('id', '=', self.bases_collaboration_id.id)])
        
        if base.available_bal < self.opening_balance:
            raise UserError(_(WITHDRAWAL_AMOUNT_EXCEEDS_BALANCE_ERROR_MESSAGE))
        
        if not self.beneficiary_id and not self.provider_id:
            raise UserError(_(NO_BENEFICIARY_PROVIDER_DEFINED_ERROR_MESSAGE))
        
        if self.beneficiary_id and self.provider_id:
            raise UserError(_(ONLY_ONE_BENEFICIARY_PROVIDER_ALLOWED_ERROR_MESSAGE))
        
        if self.beneficiary_id:
                self.accounting_action(self.beneficiary_id.property_account_receivable_id.code,
                            self.journal_id.default_credit_account_id.code, self.opening_balance, "Retiro")
                
        if self.provider_id:
                        self.accounting_action(self.provider_id.property_account_receivable_id.code,
                                    self.journal_id.default_credit_account_id.code, self.opening_balance, "Retiro")
            
        base.available_bal -= self.opening_balance

        self.state = 'approved'
        self.create_payment_request = True



    def do_withdrawal(self):
        base = self.env[BASES_COLLABORATION_MODEL].search(
            [('id', '=', self.bases_collaboration_id.id)])
        
        if base.available_bal < self.opening_balance:
            raise UserError(_(WITHDRAWAL_AMOUNT_EXCEEDS_BALANCE_ERROR_MESSAGE))
        
        if not self.beneficiary_id and not self.provider_id:
            raise UserError(_(NO_BENEFICIARY_PROVIDER_DEFINED_ERROR_MESSAGE))
        
        if self.beneficiary_id and self.provider_id:
            raise UserError(_(ONLY_ONE_BENEFICIARY_PROVIDER_ALLOWED_ERROR_MESSAGE))
        
        if self.beneficiary_id:
                self.accounting_action(self.beneficiary_id.property_account_receivable_id.code,
                            self.journal_id.default_credit_account_id.code, self.opening_balance, "Retiro por finiquito")
                
        if self.provider_id:
                        self.accounting_action(self.provider_id.property_account_receivable_id.code,
                                    self.journal_id.default_credit_account_id.code, self.opening_balance, "Retiro por finiquito")
            
        base.available_bal -= self.opening_balance

        self.state = 'approved'
        self.create_payment_request = True


    def do_withdrawal_cancellation(self):
        base = self.env[BASES_COLLABORATION_MODEL].search(
            [('id', '=', self.bases_collaboration_id.id)])
        
        if base.available_bal < self.opening_balance:
            raise UserError(_(WITHDRAWAL_AMOUNT_EXCEEDS_BALANCE_ERROR_MESSAGE))
        
        if not self.beneficiary_id and not self.provider_id:
            raise UserError(_(NO_BENEFICIARY_PROVIDER_DEFINED_ERROR_MESSAGE))
        
        if self.beneficiary_id and self.provider_id:
            raise UserError(_(ONLY_ONE_BENEFICIARY_PROVIDER_ALLOWED_ERROR_MESSAGE))
        
        if self.beneficiary_id:
                self.accounting_action(self.beneficiary_id.property_account_receivable_id.code,
                            self.journal_id.default_credit_account_id.code, self.opening_balance, "Retiro por cancelación")
                
        if self.provider_id:
                        self.accounting_action(self.provider_id.property_account_receivable_id.code,
                                    self.journal_id.default_credit_account_id.code, self.opening_balance, "Retiro por cancelación")
            
        base.available_bal -= self.opening_balance

        self.state = 'approved'
        self.create_payment_request = True


    def do_withdrawal_closure(self):
        base = self.env[BASES_COLLABORATION_MODEL].search(
            [('id', '=', self.bases_collaboration_id.id)])
        
        if base.available_bal < self.opening_balance:
            raise UserError(_(WITHDRAWAL_AMOUNT_EXCEEDS_BALANCE_ERROR_MESSAGE))
        
        if not self.beneficiary_id and not self.provider_id:
            raise UserError(_(NO_BENEFICIARY_PROVIDER_DEFINED_ERROR_MESSAGE))
        
        if self.beneficiary_id and self.provider_id:
            raise UserError(_(ONLY_ONE_BENEFICIARY_PROVIDER_ALLOWED_ERROR_MESSAGE))
        
        if self.beneficiary_id:
                self.accounting_action(self.beneficiary_id.property_account_receivable_id.code,
                            self.journal_id.default_credit_account_id.code, self.opening_balance, "Retiro por cierre")
                
        if self.provider_id:
                        self.accounting_action(self.provider_id.property_account_receivable_id.code,
                                    self.journal_id.default_credit_account_id.code, self.opening_balance, "Retiro por cierre")
            
        base.available_bal -= self.opening_balance

        self.state = 'approved'
        self.create_payment_request = True



    def accounting_action(self, debit, credit, amount, description):
        move_val = []
        move_obj = self.env[ACCOUNT_MOVE_MODEL]
        
        move_id = ''

        if not self.request_date:
            raise UserError(_('Please define a date'))

        today = self.request_date #datetime.today()
        partner_id = self.env.user.partner_id

        account_account_obj = self.env[ACCOUNT_ACCOUNT_MODEL]

        sequence = self.journal_id and self.journal_id.sequence_id or False
        if not sequence:
            raise UserError(_(ERROR_SEQUENCE_MESSAGE))
        
        name = sequence.with_context(
            ir_sequence_date=today).next_by_id()

        debit_account = account_account_obj.search([('code', '=', debit)])
        credit_account = account_account_obj.search([('code', '=', credit)])

        move_vals = {'ref': '', 'conac_move': False, 'type': 'entry','name':name,
                            'date': today, 'journal_id': self.journal_id.id, 'company_id': self.env.user.company_id.id,
                            'line_ids': [(0, 0, {
                                'name':	 description,
                                'account_id': debit_account.id,
                                'debit': amount,
                                'partner_id': partner_id.id,
                                'request_id': self.id,
                            }),
                                (0, 0, {
                                    'name':	 description,
                                    'account_id': credit_account.id,
                                    'credit': amount,
                                    'partner_id': partner_id.id,
                                    'request_id': self.id,
                                }),
                            ]}
        
        move_val.append(move_vals)
        move_id = move_obj.create(move_val)
        move_id.action_post()
        return move_id
    
    
    def accounting_with_increase(self, lines_cc, lines_pa, lines_ie):
        today = datetime.today().date()
        company_id = self.env.user.company_id.id
        partner_id = self.env.user.partner_id.id
        request_id = self.id
        journal_id = self.journal_id.id
        account_credit_id = self.operations_type_catalog_id.accounting_account_operation_id.id
        lines = []
        move_obj = self.env[ACCOUNT_MOVE_MODEL]
        total_amount = 0

        sequence = self.journal_id and self.journal_id.sequence_id or False
        if not sequence:
            raise UserError(_(ERROR_SEQUENCE_MESSAGE))
        
        for values in lines_cc:
            account_debit_id = values[0].id
            dependency = values[1].id
            subdependency = values[2].id
            amount = values[3]
            total_amount += amount
 
            lines.append((0, 0, {
                'name':	 '',    
                'account_id': account_debit_id,
                'debit': amount,
                'partner_id': partner_id,
                'dependency_id': dependency,
                'sub_dependency_id': subdependency,
                'request_id': request_id,
            }))

            
        for values in lines_pa:
            account_debit_id = values[0].item_id.unam_account_id.id
            amount = values[1]
            total_amount += amount
 
            lines.append((0, 0, {
                'name':	 '',    
                'account_id': account_debit_id,
                'debit': amount,
                'partner_id': partner_id,
                'request_id': request_id,
            }))

        for values in lines_ie:
            account_debit_id = values[1].item_id.unam_account_id.id
            amount = values[2]
            total_amount += amount
 
            lines.append((0, 0, {
                'name':	 '',    
                'account_id': account_debit_id,
                'debit': amount,
                'partner_id': partner_id,
                'request_id': request_id,
            }))


        lines.append((0, 0, {
            'name':	 '',    
            'account_id': account_credit_id,
            'credit': total_amount,
            'partner_id': partner_id,
            'request_id': request_id,
        }))

        name = sequence.with_context(ir_sequence_date=today).next_by_id()                        
        move_dict = {
            'ref': '',
            'conac_move': False,
            'type': 'entry',
            'name': name,
            'date': today,
            'journal_id': journal_id,
            'company_id': company_id,
            'line_ids': lines,
        }
        move_id = move_obj.create(move_dict)
        move_id.action_post()

        # Segunda parte de apuntes
        move_list_2 = []
        name_2 = sequence.with_context(ir_sequence_date=today).next_by_id() 
        move_list_2.append({'ref': '', 'conac_move': False, 'type': 'entry', 'name': name_2,
                            'date': today, 'journal_id': journal_id, 'company_id': company_id,
                            'line_ids': [(0, 0, {
                                'name':	 '',    
                                'account_id': self.accounting_catalog.id,
                                'debit': total_amount,
                                'partner_id': partner_id,
                                'request_id': request_id,
                                }),
                                (0, 0, {
                                'name':	 '',
                                'account_id': account_credit_id,
                                'credit': total_amount,
                                'partner_id': partner_id,
                                'request_id': request_id,
                                }),
                        ]})
        move_id_2 = move_obj.create(move_list_2)
        move_id_2.action_post()
    

    def accounting_with_operations_type_catalog(self, operation_type):
        move_id = ''
        today = datetime.today().date()
        partner_id = self.env.user.partner_id
        request_id = self.id

        sequence = self.journal_id and self.journal_id.sequence_id or False
        if not sequence:
            raise UserError(_(ERROR_SEQUENCE_MESSAGE))
        name = sequence.with_context(ir_sequence_date=today).next_by_id()
        
        account_debit_id, account_credit_id = self.type_operation_catalog()

        move_vals = {'ref': '', 'conac_move': False,
                            'date': today, 'journal_id': self.journal_id.id, 'company_id': self.env.user.company_id.id, 'name':name,
                            'line_ids': [(0, 0, {
                                'name':	 '',    
                                'account_id': account_debit_id,
                                'debit': self.opening_balance,
                                'partner_id': partner_id.id,
                                'request_id': request_id,
                                }),
                                (0, 0, {
                                'name':	 '',
                                'account_id': account_credit_id,
                                'credit': self.opening_balance,
                                'partner_id': partner_id.id,
                                'request_id': request_id,
                                }),
                    ]}
                    
        move_obj = self.env[ACCOUNT_MOVE_MODEL]
        move_id = move_obj.create(move_vals)
        move_id.action_post()
      
        self.update_available_bal_amount(operation_type)

        return move_id
        

    def type_operation_catalog(self):
        operation_type = self.operations_type_catalog_id.operation_type

        if operation_type == 'decrease':
            account_credit_id = self.operations_type_catalog_id.accounting_account_operation_id.id
            account_debit_id = self.accounting_catalog.id
               

        return account_debit_id, account_credit_id
    

    def update_available_bal_amount(self, operation_type):
        base_collaboration = self.bases_collaboration_id
        available_balance = base_collaboration.available_bal
        opening_balance = base_collaboration.opening_bal
    
        if opening_balance == None:
           raise UserError(_("There isn't opening balance."))
          
        if self.operation_number == '1' and operation_type == 'increase':
            if self.opening_balance > opening_balance:
                raise UserError(_("Operation balance cant´n be greater than opening balance."))
            available_balance = self.opening_balance
        elif operation_type == 'decrease':
            available_balance -= self.opening_balance
        elif operation_type == 'increase':
            total_amount = sum(self.origin_resource_ids.mapped('amount'))
            available_balance += total_amount
       
        self.bases_collaboration_id.available_bal = available_balance
                

    def action_create_payment_request(self):
        # Función para creación de solicitud de pago desde retiro de base de colaboración

        # Buscar solicitudes de pago existentes
        payment_reqs = self.env[ACCOUNT_MOVE_MODEL].search(
            [('collaboration_base_payment_request_id', '=', self.id)])
        
        # Establecer el ID del socio
        partner_id = self.beneficiary_id.id if self.beneficiary_id else self.provider_id.id
        
        # Definir valores predeterminados en el contexto
        default_context = {
            'payment_request': True,
            'show_for_supplier_payment': True,
            'default_is_payment_request': 1,
            'from_move': 1,
            'group_by': 'l10n_mx_edi_payment_method_id',
            'default_collaboration_base_payment_request_id': self.id,
            'default_type': 'in_invoice',
            'default_partner_id': partner_id,
            'default_dependancy_id': self.bases_collaboration_id.dependency_id.id,
            'default_sub_dependancy_id': self.bases_collaboration_id.subdependency_id.id,
        }

        if payment_reqs:
            return {
                'name': _('Payment Requests'),
                'view_mode': VIEW_MODE_TREE_FORM,
                'res_model': ACCOUNT_MOVE_MODEL,
                'domain': [('collaboration_base_payment_request_id', '=', self.id)],
                'type': IR_ACTIONS_ACT_WINDOW,
                'views': [(self.env.ref(JT_SUPPLIER_PAYMENT_PAYMENT_REQ_REQ_TREE_VIEW).id, 'tree'), (self.env.ref(SIIF_SUPPLIER_PAYMENT_PAYMENT_REQUEST_GENERAL_FORM_VIEW).id, 'form')],
                'context': default_context
            }
        else:
            return {
                'name': 'Payment Request',
                'view_mode': 'form',
                'res_model': ACCOUNT_MOVE_MODEL,
                'domain': [('collaboration_base_payment_request_id', '=', self.id)],
                'type': IR_ACTIONS_ACT_WINDOW,
                'view_id': self.env.ref(SIIF_SUPPLIER_PAYMENT_PAYMENT_REQUEST_GENERAL_FORM_VIEW).id,
                'context': default_context
            }
        

    @api.depends('type_of_operation')
    def get_order_seq(self):
        for rec in self:
            seq = 0
            if rec.type_of_operation and rec.type_of_operation == 'open_bal':
                seq = 1
            elif rec.type_of_operation and rec.type_of_operation == 'increase':
                seq = 2
            elif rec.type_of_operation and rec.type_of_operation == 'increase_by_closing':
                seq = 3
            elif rec.type_of_operation and rec.type_of_operation == 'retirement':
                seq = 4
            elif rec.type_of_operation and rec.type_of_operation == 'withdrawal_cancellation':
                seq = 5
            elif rec.type_of_operation and rec.type_of_operation == 'withdrawal':
                seq = 6
            elif rec.type_of_operation and rec.type_of_operation == 'withdrawal_closure':
                seq = 7
        rec.order_seq = seq

    name = fields.Char("Name")
    bases_collaboration_id = fields.Many2one(BASES_COLLABORATION_MODEL)
    operation_number = fields.Char(OPERATION_NUMBER)
    agreement_number = fields.Char(AGGREMENT_NUMBER)
    type_of_operation = fields.Selection([('open_bal', OPENING_BALANCE),
                                          ('increase', 'Increase'),
                                          ('retirement', 'Retirement'),
                                          ('withdrawal', 'Withdrawal for settlement'),
                                          ('withdrawal_cancellation',
                                           WITHDRAWAL_CANCEL),
                                          ('withdrawal_closure',
                                           'Withdrawal due to closure')],
                                         string=TYPE_OF_OPERATION)

    type_of_operation_trust = fields.Selection([('open_bal', OPENING_BALANCE),
                                                ('increase', 'Increase'),
                                                ('retirement', 'Retirement'),
                                                ('withdrawal_cancellation',
                                                 WITHDRAWAL_CANCEL),
                                                ],
                                               string=TYPE_OF_OPERATION)

    journal_id = fields.Many2one(ACCOUNT_JOURNAL_MODEL)
    collaboration_beneficiary_id = fields.Many2one(COLLABORATION_BENEFICIARY_MODEL,'Collaboration Beneficiary')
    move_line_ids = fields.One2many(
        ACCOUNT_MOVE_LINE_MODEL, 'request_id', string="Journal Items")

    apply_to_basis_collaboration = fields.Boolean(
        "Apply to Basis of Collaboration")
    order_seq = fields.Integer(compute='get_order_seq', store=True, copy=False)

    origin_resource_id = fields.Many2one(
        SUB_ORIGIN_RESOURCE_MODEL, ORIGIN_RESORCE_FIELD_DESCRIPTION)
    
    state = fields.Selection([('draft', 'Draft'),
                              ('requested', 'Requested'),
                              ('rejected', 'Rejected'),
                              ('approved', 'Approved'),
                              ('confirmed', 'Confirmed'),
                              ('done', 'Done'),
                              ('canceled', 'Canceled')], string="Status", default="draft")

    request_date = fields.Date("Request Date")
    trade_number = fields.Char("Trade Number")
    currency_id = fields.Many2one(
        RES_CURRENCY_MODEL, default=lambda self: self.env.user.company_id.currency_id)
    opening_balance = fields.Monetary("Opening Amount")
    observations = fields.Text("Observations")
    user_id = fields.Many2one(RES_USERS_MODEL, default=lambda self: self.env.user.id,
                              string="Requesting User")

    cbc_format = fields.Binary(CBC_FORMAT_FIELD_DESCRIPTION)
    cbc_shipping_office = fields.Binary(CBC_SHIPPING_OFFICE_FIELD_DESCRIPTION)
    liability_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, LIABILITY_ACCOUNT_FIELD_DESCRIPTION)
    investment_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, INVESTMENT_ACCOUNT_FIELD_DESCRIPTION)
    interest_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, INTEREST_ACCOUNT_FIELD_DESCRIPTION)
    availability_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, AVAILABILITY_ACCOUNT_FIELD_DESCRIPTION)
    reason_rejection = fields.Text("Reason for Rejection")
    supporting_documentation = fields.Binary(SUPPORTING_DOCUMENTATION_FIELD_DESCRIPTION)
    create_payment_request = fields.Boolean("Create Payment Request", copy=False)
    beneficiary_id = fields.Many2one(RES_PARTNER_MODEL, "Beneficiary")
    provider_id = fields.Many2one(RES_PARTNER_MODEL, "Provider")
    is_cancel_collaboration = fields.Boolean(
        "Operation of cancel collaboration", default=False)

    #==== fields for trust investment =======#

    trust_id = fields.Many2one(AGREEMENT_TRUST_MODEL, 'Trust')

    patrimonial_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, "Patrimonial Account")
    investment_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, "Investment Account")
    interest_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, INTEREST_ACCOUNT_FIELD_DESCRIPTION)
    honorary_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, "Honorary Accounting Account")
    availability_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, AVAILABILITY_ACCOUNT_FIELD_DESCRIPTION)

    trust_agreement_file = fields.Binary("Trustee Agreement")
    trust_agreement_file_name = fields.Char("Trust Agreement File Name")
    trust_office_file = fields.Binary("Trust Contract Official Letter")
    trust_office_file_name = fields.Char("Trust Office File Name")

    origin_journal_id = fields.Many2one(
        ACCOUNT_JOURNAL_MODEL, 'Origin Bank Account')
    origin_bank_account_id = fields.Many2one(
        related='origin_journal_id.bank_account_id')
    destination_journal_id = fields.Many2one(
        ACCOUNT_JOURNAL_MODEL, 'Destination Bank Account')
    destination_bank_account_id = fields.Many2one(
        related='destination_journal_id.bank_account_id')

    trust_provider_ids = fields.Many2many(
        RES_PARTNER_MODEL, 'rel_req_bal_trust_partner', 'partner_id', 'req_id', compute="get_trust_provider_ids")
    trust_beneficiary_ids = fields.Many2many(
        RES_PARTNER_MODEL, 'rel_req_bal_trust_beneficiary', 'partner_id', 'req_id', compute="get_trust_beneficiary_ids")
    bases_collaboration_beneficiary_ids = fields.Many2many(
        RES_PARTNER_MODEL, 'rel_req_bal_bases_collaboration_beneficiary', 'partner_id', 'req_id', compute="get_bases_collaboration_beneficiary_ids")

    #==== fields for patrimonial =======#

    patrimonial_resources_id = fields.Many2one(
        PATRIMONIAL_RESOURCES_MODEL, 'Patrimonial Resources')
    patrimonial_equity_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, "Equity accounting account")
    patrimonial_yield_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, "Yield account of the productive investment account")

    specifics_project_id = fields.Many2one(
        'specific.project', 'Specific project')
    background_project_id = fields.Many2one(
        'background.project', 'Background Project', related="specifics_project_id.backgound_project_id")

    @api.onchange('type_of_operation')
    def onchange_type_of_operation_amount(self):
        if self.bases_collaboration_id and self.type_of_operation :
            if self.type_of_operation == 'open_bal':
                self.opening_balance = self.bases_collaboration_id.opening_bal
            else:
                self.opening_balance = 0.0

        elif self.patrimonial_resources_id and self.type_of_operation:
            if self.type_of_operation == 'open_bal':
                self.opening_balance = self.patrimonial_resources_id.opening_balance
            else:
                self.opening_balance = 0.0
        else:
            if self.trust_id and self.type_of_operation :
                if self.type_of_operation == 'open_bal':
                    self.opening_balance = self.trust_id.opening_balance
                else:
                    self.opening_balance = 0.0

    @api.onchange('type_of_operation_trust')
    def type_of_operation_trust_change(self):
        self.type_of_operation = self.type_of_operation_trust

    def unlink(self):
        for rec in self:
            if rec.state not in ['draft']:
                raise UserError(
                    _('You cannot delete an entry which has been requested.'))
        return super(RequestOpenBalance, self).unlink()

    @api.constrains('type_of_operation')
    def _check_type_of_operation(self):
        if self.type_of_operation and self.bases_collaboration_id and self.type_of_operation == 'open_bal':
            records = self.env[REQUEST_OPEN_BALANCE_MODEL].search([('id', '!=', self.id), ('bases_collaboration_id', '=', self.bases_collaboration_id.id), (
                'type_of_operation', '=', 'open_bal'), ('state', 'not in', ('rejected', 'canceled'))])
            if records:
                raise ValidationError(
                    _('Operation of opening balance can be performed just one time in each agreement'))

    @api.depends('trust_id', 'trust_id.provider_ids', 'trust_id.provider_ids.partner_id')
    def get_trust_provider_ids(self):
        for rec in self:
            if rec.trust_id and rec.trust_id.provider_ids:
                rec.trust_provider_ids = [
                    (6, 0, rec.trust_id.provider_ids.mapped('partner_id').ids)]
            else:
                rec.trust_provider_ids = [(6, 0, [])]

    @api.depends('trust_id', 'trust_id.beneficiary_ids', 'trust_id.beneficiary_ids.employee_id')
    def get_trust_beneficiary_ids(self):
        for rec in self:
            partner_ids = []
            if rec.trust_id and rec.trust_id.beneficiary_ids:

                for emp in rec.trust_id.beneficiary_ids.mapped('employee_id'):
                    if emp.user_id and emp.user_id.partner_id:
                        partner_ids.append(emp.user_id.partner_id.id)
                for pat in rec.trust_id.beneficiary_ids.mapped('partner_id'):
                    partner_ids.append(pat.id)
            rec.trust_beneficiary_ids = [(6, 0, partner_ids)]

    @api.depends('bases_collaboration_id', 'bases_collaboration_id.beneficiary_ids', 'bases_collaboration_id.beneficiary_ids.employee_id')
    def get_bases_collaboration_beneficiary_ids(self):
        for rec in self:
            partner_ids = []
            if rec.bases_collaboration_id and rec.bases_collaboration_id.beneficiary_ids:
                for emp in rec.bases_collaboration_id.beneficiary_ids.mapped('employee_id'):
                    if emp.user_id and emp.user_id.partner_id:
                        partner_ids.append(emp.user_id.partner_id.id)
                for pat in rec.bases_collaboration_id.beneficiary_ids.mapped('partner_id'):
                    partner_ids.append(pat.id)
            rec.bases_collaboration_beneficiary_ids = [(6, 0, partner_ids)]

    @api.model
    def default_get(self, fields):
        res = super(RequestOpenBalance, self).default_get(fields)
        if res.get('bases_collaboration_id', False):

            base_id = self.env[BASES_COLLABORATION_MODEL].browse(
                res.get('bases_collaboration_id'))
            number = 0
            if base_id:
                number = base_id.next_no + 1

            res.update({
                'operation_number': str(number)
            })

        elif res.get('trust_id', False):
            trust_id = self.env[AGREEMENT_TRUST_MODEL].browse(res.get('trust_id'))
            number = 0
            if trust_id:
                number = trust_id.next_no + 1

            res.update({
                'operation_number': str(number)
            })
        elif res.get('patrimonial_resources_id', False):
            patrimonial_resources_id = self.env[PATRIMONIAL_RESOURCES_MODEL].browse(
                res.get('patrimonial_resources_id'))
            number = 0
            if patrimonial_resources_id:
                number = patrimonial_resources_id.next_no + 1

            res.update({
                'operation_number': str(number)
            })

        else:
            res.update({
                'operation_number': str(0)
            })

        return res

    @api.model
    def create(self, vals):
        res = super(RequestOpenBalance, self).create(vals)
        if res and res.is_cancel_collaboration and res.type_of_operation != 'withdrawal_cancellation':
            raise ValidationError(
                _(TYPE_OF_OPERATION_VALID_ERROR))
        if res and not res.is_cancel_collaboration and res.type_of_operation == 'withdrawal_cancellation':
            raise ValidationError(
                _(CANT_CREATE_OPERATION_VALID_ERROR))
        if self.env.context and not self.env.context.get('call_from_closing', False) and res.type_of_operation == 'withdrawal_closure':
            raise ValidationError(
                _("Can't create Operation with 'Withdrawal due to closure' Type of Operation manually!"))
        if self.env.context and not self.env.context.get('call_from_closing', False) and res.type_of_operation == 'increase_by_closing':
            raise ValidationError(
                _("Can't create Operation with 'Increase by closing' Type of Operation manually!"))
        if res.bases_collaboration_id and res.bases_collaboration_id.state in ('cancelled', 'to_be_cancelled'):
            raise ValidationError(
                _("Can't create Operation for Cancelled Bases of Collabration!"))
        if res.trust_id and res.trust_id.state in ('cancelled', 'to_be_cancelled'):
            raise ValidationError(
                _("Can't create Operation for Cancelled Trust!"))
        if res.patrimonial_resources_id and res.patrimonial_resources_id.state in ('cancelled', 'to_be_cancelled'):
            raise ValidationError(
                _("Can't create Operation for Cancelled Patrimonial resources!"))

        if res.bases_collaboration_id:
            res.bases_collaboration_id.next_no += 1
            res.operation_number = res.bases_collaboration_id.next_no
            #===========set Document data=============
            if not res.cbc_format:
                res.cbc_format = res.bases_collaboration_id.cbc_format
            if not res.supporting_documentation:
                res.supporting_documentation = res.bases_collaboration_id.cbc_format
            if not res.cbc_shipping_office:
                res.cbc_shipping_office = res.bases_collaboration_id.cbc_shipping_office
             
        if res.trust_id:
            res.trust_id.next_no += 1
            res.operation_number = res.trust_id.next_no
            if not res.trust_agreement_file:
                res.trust_agreement_file = res.trust_id.trust_agreement_file
                res.trust_agreement_file_name = res.trust_id.trust_agreement_file_name
            if not res.trust_office_file:
                res.trust_office_file = res.trust_id.trust_office_file
                res.trust_office_file_name = res.trust_id.trust_office_file_name
            if not res.supporting_documentation:
                res.supporting_documentation = res.trust_id.trust_office_file
                
        if res.patrimonial_resources_id:
            res.patrimonial_resources_id.next_no += 1
            res.operation_number = res.patrimonial_resources_id.next_no
            #===== Set Documents data================
            if not res.trust_agreement_file:
                res.trust_agreement_file = res.patrimonial_resources_id.fund_registration_file
                res.trust_agreement_file_name = res.patrimonial_resources_id.fund_registration_file_name
            if not res.trust_office_file:
                res.trust_office_file = res.patrimonial_resources_id.fund_office_file
                res.trust_office_file_name = res.patrimonial_resources_id.fund_office_file_name
            if not res.supporting_documentation:
                res.supporting_documentation = res.patrimonial_resources_id.fund_registration_file

        if not res.type_of_operation_trust and res.type_of_operation and res.trust_id:
            if res.type_of_operation in ('open_bal', 'increase', 'retirement', 'withdrawal_cancellation'):
                res.type_of_operation_trust = res.type_of_operation


        return res

    def write(self, vals):
        res = super(RequestOpenBalance, self).write(vals)
        for rec in self:
            if rec.is_cancel_collaboration and rec.type_of_operation != 'withdrawal_cancellation':
                raise ValidationError(
                    _(TYPE_OF_OPERATION_VALID_ERROR))
            if not rec.is_cancel_collaboration and rec.type_of_operation == 'withdrawal_cancellation':
                raise ValidationError(
                    _(CANT_CREATE_OPERATION_VALID_ERROR))
            
        if self.origin_resource_ids:    
            opening_bal = self.bases_collaboration_id.opening_bal
            total_amount_origin_resource = sum(self.origin_resource_ids.mapped('amount'))
            if total_amount_origin_resource > opening_bal:
                raise ValidationError(_("The total amount of origin of the resource is greater than the opening balance."))
  
        return res

    def action_create_payment_req(self):
        payment_req_obj = self.env[PAYMENT_REQUEST_MODEL]
        payment_reqs = payment_req_obj.search(
            [('balance_req_id', '=', self.id)])
        beneficiary = False
        bank_id = False
        account_number = False
        partner_id = False
        if self.beneficiary_id:
            partner_id = self.beneficiary_id.id
        if self.beneficiary_id and self.bases_collaboration_id:
            user = self.env[RES_USERS_MODEL].sudo().search(
                [('partner_id', '=', self.beneficiary_id.id)], limit=1)
            if user:
                emp_id = self.env[HR_EMPLOYEE_MODEL].sudo().search(
                    [('user_id', '=', user.id)], limit=1)
                if emp_id:
                    beneficiary = self.env[COLLABORATION_BENEFICIARY_MODEL].search([
                        ('collaboration_id', '=', self.bases_collaboration_id.id), ('employee_id', '=', emp_id.id)])
        if not beneficiary and self.beneficiary_id and self.bases_collaboration_id:
            beneficiary = self.env[COLLABORATION_BENEFICIARY_MODEL].search([
                ('collaboration_id', '=', self.bases_collaboration_id.id), ('partner_id', '=', self.beneficiary_id.id)])
        if beneficiary:
            bank_id = beneficiary.bank_id.id if beneficiary and beneficiary.bank_id else False
            account_number = beneficiary.account_number if beneficiary else '',
        elif self.beneficiary_id:
            bank_id = self.beneficiary_id and self.beneficiary_id.bank_ids and self.beneficiary_id.bank_ids[0].bank_id and self.beneficiary_id.bank_ids[0].id or False 
            account_number = self.beneficiary_id and self.beneficiary_id.bank_ids and self.beneficiary_id.bank_ids[0] and self.beneficiary_id.bank_ids[0].acc_number or '' 
        
        if not self.beneficiary_id and self.provider_id and self.patrimonial_resources_id:
            partner_id = self.provider_id.id
            col_provider = self.env[COLLABORATION_PROVIDERS_MODEL].search([
                ('patrimonial_id', '=', self.patrimonial_resources_id.id),('partner_id', '=', self.provider_id.id)],limit=1)
            if col_provider:
                bank_id = col_provider.bank_id and col_provider.bank_id.id or False
                account_number = col_provider.account_number
                 
        is_balance_req_id_reject = False
        if self.state and self.state=='rejected':
            is_balance_req_id_reject = True
         
        if payment_reqs:
            
            return {
                'name': _('Payment Requests'),
                'view_type': 'form',
                'view_mode': VIEW_MODE_TREE_FORM,
                'res_model': PAYMENT_REQUEST_MODEL,
                'domain': [('balance_req_id', '=', self.id)],
                'type': IR_ACTIONS_ACT_WINDOW,
                'context': {'default_balance_req_id': self.id,
                            'default_name': self.name,
                            'default_type_of_operation': 'retirement',
                            'default_operation_number': self.operation_number,
                            'default_amount': self.opening_balance,
                            'default_beneficiary_id': partner_id,
                            'default_bank_id': bank_id,
                            'default_account_number': account_number,
                            'from_agreement': True,
                            'default_is_balance_req_id_reject':is_balance_req_id_reject,
                            }
            }
        else:
            return {
                'name': 'Payment Request',
                'view_mode': 'form',
                'res_model': PAYMENT_REQUEST_MODEL,
                'domain': [('balance_req_id', '=', self.id)],
                'type': IR_ACTIONS_ACT_WINDOW,
                'context': {'default_balance_req_id': self.id,
                            'default_name': self.name,
                            'default_type_of_operation': 'retirement',
                            'default_operation_number': self.operation_number,
                            'default_amount': self.opening_balance,
                            'default_beneficiary_id': partner_id,
                            'default_bank_id': bank_id,
                            'default_account_number': account_number,
                            'from_agreement': True,
                            'default_is_balance_req_id_reject':is_balance_req_id_reject,
                            }
            }

    @api.constrains('operation_number')
    def _check_operation_number(self):
        if self.operation_number and not self.operation_number.isnumeric():
            raise ValidationError(_(OPERATION_NUMBER_NOT_NUMERIC_ERROR_MESSAGE))

    def action_confirmed(self):
        self.state = 'confirmed'
        if self.patrimonial_resources_id or self.bases_collaboration_id:
            if self.journal_id:
                journal = self.journal_id
                if not journal.default_debit_account_id or not journal.default_credit_account_id \
                        or not journal.conac_debit_account_id or not journal.conac_credit_account_id:
                    if self.env.user.lang == 'es_MX':
                        raise ValidationError(
                            _("Por favor configure la cuenta UNAM y CONAC en diario!"))
                    else:
                        raise ValidationError(
                            _("Please configure UNAM and CONAC account in journal!"))

                today = datetime.today().date()
                user = self.env.user
                partner_id = user.partner_id.id
                amount = self.opening_balance
                name = ''
                dep = False
                sub_dep = False
                if self.bases_collaboration_id:
                    dep = self.bases_collaboration_id.dependency_id and self.bases_collaboration_id.dependency_id.id or False
                    sub_dep = self.bases_collaboration_id.subdependency_id and self.bases_collaboration_id.subdependency_id.id or False
                if self.patrimonial_resources_id:
                    print ("callll====")
                    dep = self.patrimonial_resources_id.dependency_id and self.patrimonial_resources_id.dependency_id.id or False
                    sub_dep = self.patrimonial_resources_id.subdependency_id and self.patrimonial_resources_id.subdependency_id.id or False
                
                     
                if self.name:
                    name += self.name
                if self.operation_number:
                    name += "-" + self.operation_number

                if self.type_of_operation in ('open_bal', 'increase', 'increase_by_closing'):
                    unam_move_val = {'name': name, 'ref': name,  'conac_move': True,
                                     'dependancy_id' : dep,
                                     'sub_dependancy_id' : sub_dep,
                                     'date': today, 'journal_id': journal.id, 'company_id': self.env.user.company_id.id,
                                     'line_ids': [(0, 0, {
                                         'account_id': journal.default_credit_account_id.id,
                                         'coa_conac_id': journal.conac_credit_account_id.id,
                                         'credit': amount,
                                         'partner_id': partner_id,
                                         'request_id': self.id,
                                     }),
                                         (0, 0, {
                                             'account_id': journal.default_debit_account_id.id,
                                             'coa_conac_id': journal.conac_debit_account_id.id,
                                             'debit': amount,
                                             'partner_id': partner_id,
                                             'request_id': self.id,
                                         }),
                                     ]}
                    move_obj = self.env[ACCOUNT_MOVE_MODEL]
                    unam_move = move_obj.create(unam_move_val)
                    unam_move.action_post()
                elif self.type_of_operation in ('retirement', 'withdrawal', 'withdrawal_cancellation', 'withdrawal_closure'):
                    unam_move_val = {'name': name, 'ref': name,  'conac_move': True,
                                     'date': today, 'journal_id': journal.id, 'company_id': self.env.user.company_id.id,
                                     'line_ids': [(0, 0, {
                                         'account_id': journal.default_debit_account_id.id,
                                         'coa_conac_id': journal.conac_debit_account_id.id,
                                         'credit': amount,
                                         'partner_id': partner_id,
                                         'request_id': self.id,
                                     }),
                                         (0, 0, {
                                             'account_id': journal.default_credit_account_id.id,
                                             'coa_conac_id': journal.conac_credit_account_id.id,
                                             'debit': amount,
                                             'partner_id': partner_id,
                                             'request_id': self.id,
                                         }),
                                     ]}
                    move_obj = self.env[ACCOUNT_MOVE_MODEL]
                    unam_move = move_obj.create(unam_move_val)
                    unam_move.action_post()

    def request(self):
        if self.type_of_operation in ('retirement','withdrawal','withdrawal_cancellation','withdrawal_closure') and self.bases_collaboration_id and self.bases_collaboration_id.available_bal < self.opening_balance:
            raise ValidationError(_(AVAILABLE_BALANCE_VALID_ERROR))

        elif self.type_of_operation in ('retirement','withdrawal','withdrawal_cancellation','withdrawal_closure') and self.trust_id and self.trust_id.available_bal < self.opening_balance:
            raise ValidationError(_(AVAILABLE_BALANCE_VALID_ERROR))

        elif self.type_of_operation in ('retirement','withdrawal','withdrawal_cancellation','withdrawal_closure') and self.patrimonial_resources_id and self.patrimonial_resources_id.available_bal < self.opening_balance:
            raise ValidationError(_(AVAILABLE_BALANCE_VALID_ERROR))
        
        req_open_bal_id = False
        if self.state=='rejected':
            req_open_bal_id = self.env[REQUEST_OPEN_BALANCE_INVEST].search([('state','=','rejected'),('balance_req_id','=',self.id)],limit=1)
        
        if req_open_bal_id:
            req_open_bal_id.type_of_operation = self.type_of_operation  
            req_open_bal_id.state = 'requested'
            req_open_bal_id.opening_balance = self.opening_balance 
            req_open_bal_id.origin_resource_id = self.origin_resource_id and self.origin_resource_id.id or False
            req_open_bal_id.beneficiary_id = self.beneficiary_id.id
        else:
            self.env[REQUEST_OPEN_BALANCE_INVEST].create({
                'name': self.name,
                'operation_number': self.operation_number,
                'agreement_number': self.agreement_number,
                'is_cancel_collaboration': True if self.type_of_operation == 'withdrawal_cancellation' else False,
                'type_of_operation': self.type_of_operation,
                'apply_to_basis_collaboration': self.apply_to_basis_collaboration,
                'origin_resource_id': self.origin_resource_id and self.origin_resource_id.id or False,
                'state': 'requested',
                'request_date': self.request_date,
                'trade_number': self.trade_number,
                'currency_id': self.currency_id and self.currency_id.id or False,
                'opening_balance': self.opening_balance,
                'observations': self.observations,
                'user_id': self.user_id and self.user_id.id or False,
                'cbc_format': self.cbc_format,
                'cbc_shipping_office': self.cbc_shipping_office,
                'liability_account_id': self.liability_account_id and self.liability_account_id.id or False,
                'investment_account_id': self.investment_account_id and self.investment_account_id.id or False,
                'availability_account_id': self.availability_account_id and self.availability_account_id.id or False,
                'balance_req_id': self.id,
                'patrimonial_account_id': self.patrimonial_account_id and self.patrimonial_account_id.id or False,
                'interest_account_id': self.interest_account_id and self.interest_account_id.id or False,
                'honorary_account_id': self.honorary_account_id and self.honorary_account_id.id or False,
                'trust_agreement_file': self.trust_agreement_file,
                'trust_agreement_file_name': self.trust_agreement_file_name,
                'trust_office_file': self.trust_office_file,
                'trust_office_file_name': self.trust_office_file_name,
                'trust_id': self.trust_id and self.trust_id.id or False,
                'origin_journal_id': self.origin_journal_id and self.origin_journal_id.id or False,
                "destination_journal_id": self.destination_journal_id and self.destination_journal_id.id or False,
                'patrimonial_id': self.patrimonial_resources_id and self.patrimonial_resources_id.id or False,
                'patrimonial_yield_account_id': self.patrimonial_yield_account_id and self.patrimonial_yield_account_id.id or False,
                'patrimonial_equity_account_id': self.patrimonial_equity_account_id and self.patrimonial_equity_account_id.id or False,
                'bases_collaboration_id': self.bases_collaboration_id and self.bases_collaboration_id.id or False,
                'fund_type_id': self.bases_collaboration_id and self.bases_collaboration_id.fund_type_id and self.bases_collaboration_id.fund_type_id.id or False,
                'type_of_agreement_id': self.bases_collaboration_id and self.bases_collaboration_id.agreement_type_id and self.bases_collaboration_id.agreement_type_id.id or False,
                'fund_id':  self.bases_collaboration_id and self.bases_collaboration_id.fund_id and self.bases_collaboration_id.fund_id.id or False,
                'beneficiary_id': self.beneficiary_id.id,
                'provider_id': self.provider_id.id,
                'specifics_project_id': self.specifics_project_id and self.specifics_project_id.id or False,
            })
        self.state = 'requested'
        if self.type_of_operation == 'withdrawal_cancellation' and self.bases_collaboration_id:
            self.bases_collaboration_id.state = 'to_be_cancelled'
        if self.type_of_operation == 'withdrawal_cancellation' and self.trust_id:
            self.trust_id.action_to_be_cancelled()
        if self.type_of_operation == 'withdrawal_cancellation' and self.patrimonial_resources_id:
            self.patrimonial_resources_id.action_to_be_cancelled()
        elif self.type_of_operation == 'retirement':
            self.create_payment_request = True


class RequestOpenBalanceInvestment(models.Model):

    _name = 'request.open.balance.invest'
    _inherit = [MAIL_THREAD_MODEL, MAIL_ACTIVITY_MIXIN_MODEL]
    _description = "Request to Open Balance Investment"

    _rec_name = 'first_number'

    first_number = fields.Char('First Number:')
    new_journal_id = fields.Many2one("account.journal", 'Journal')

    name = fields.Char("Name")
    balance_req_id = fields.Many2one(
        REQUEST_OPEN_BALANCE_MODEL, "Opening Balance Request")
    operation_number = fields.Char(OPERATION_NUMBER)
    agreement_number = fields.Char(AGGREMENT_NUMBER)
    type_of_operation = fields.Selection([('open_bal', OPENING_BALANCE),
                                          ('increase', 'Increase'),
                                          ('retirement', 'Retirement'),
                                          ('withdrawal', 'Withdrawal for settlement'),
                                          ('withdrawal_cancellation', WITHDRAWAL_CANCEL)],
                                         string=TYPE_OF_OPERATION)
    apply_to_basis_collaboration = fields.Boolean(
        "Apply to Basis of Collaboration")
    apply_to_trust = fields.Boolean(
        related='apply_to_basis_collaboration', string="Apply to Trust")

    apply_to_patrimonial = fields.Boolean(
        related='apply_to_basis_collaboration', string="Apply to patrimonial resources")

    origin_resource_id = fields.Many2one(
        SUB_ORIGIN_RESOURCE_MODEL, ORIGIN_RESORCE_FIELD_DESCRIPTION)
    state = fields.Selection([('draft', 'Draft'),
                              ('requested', 'Requested'),
                              ('rejected', 'Rejected'),
                              ('approved', 'Approved'),
                              ('confirmed', 'Confirmed'),
                              ('done', 'Done'),
                              ('canceled', 'Canceled')], string="Status", default="draft")

    request_date = fields.Date("Request Date")
    trade_number = fields.Char("Trade Number")
    currency_id = fields.Many2one(
        RES_CURRENCY_MODEL, default=lambda self: self.env.user.company_id.currency_id)
    opening_balance = fields.Monetary("Opening Amount")
    observations = fields.Text("Observations")
    user_id = fields.Many2one(RES_USERS_MODEL, default=lambda self: self.env.user.id,
                              string="Requesting User")
    is_manually = fields.Float("Manually Records", default=False)

    cbc_format = fields.Binary(CBC_FORMAT_FIELD_DESCRIPTION)
    cbc_shipping_office = fields.Binary(CBC_SHIPPING_OFFICE_FIELD_DESCRIPTION)
    liability_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, LIABILITY_ACCOUNT_FIELD_DESCRIPTION)
    investment_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, INVESTMENT_ACCOUNT_FIELD_DESCRIPTION)
    interest_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, INTEREST_ACCOUNT_FIELD_DESCRIPTION)
    availability_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, AVAILABILITY_ACCOUNT_FIELD_DESCRIPTION)
    supporting_documentation = fields.Binary(SUPPORTING_DOCUMENTATION_FIELD_DESCRIPTION)

    origin_journal_id = fields.Many2one(
        ACCOUNT_JOURNAL_MODEL, 'Origin Bank Account')
    destination_journal_id = fields.Many2one(
        ACCOUNT_JOURNAL_MODEL, 'Destination Bank Account')

    patrimonial_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, "Patrimonial Account")
    investment_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, "Investment Account")
    interest_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, INTEREST_ACCOUNT_FIELD_DESCRIPTION)
    honorary_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, "Honorary Accounting Account")
    availability_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, AVAILABILITY_ACCOUNT_FIELD_DESCRIPTION)
    liability_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, "Liability Account")

    trust_agreement_file = fields.Binary("Trustee Agreement")
    trust_agreement_file_name = fields.Char("Trust Agreement File Name")
    trust_office_file = fields.Binary("Trust Contract Official Letter")
    trust_office_file_name = fields.Char("Trust Office File Name")

    trust_id = fields.Many2one(AGREEMENT_TRUST_MODEL, 'Trust')

    patrimonial_id = fields.Many2one(PATRIMONIAL_RESOURCES_MODEL, 'Patrimonial')
    patrimonial_yield_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, "Yield account of the productive investment account")
    patrimonial_equity_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, "Equity accounting account")

    specifics_project_id = fields.Many2one(
        'specific.project', 'Specific project')
    background_project_id = fields.Many2one(
        'background.project', 'Background Project', related="specifics_project_id.backgound_project_id")

    reason_rejection = fields.Text("Reason for Rejection")
    is_cancel_collaboration = fields.Boolean(
        "Operation of cancel collaboration", default=False)
    yield_id = fields.Many2one('yield.destination', 'Yield Destination')

    #====== fields for the Investment Funds View   ====>

    fund_id = fields.Many2one(AGREEMENT_FUND_MODEL, 'Fund')
    fund_key = fields.Char(related='fund_id.fund_key',
                           string="Password of the Fund")

    fund_type_id = fields.Many2one(FUND_TYPE_MODEL, 'Type of Fund')
    type_of_agreement_id = fields.Many2one(
        AGREEMENT_AGREEMENT_TYPE_MODEL, 'Type of Agreement')
    bases_collaboration_id = fields.Many2one(
        BASES_COLLABORATION_MODEL, 'Name of Agreement')
    is_fund = fields.Boolean(default=False, string="Fund")

    fund_request_date = fields.Date('Request')
    dependency_id = fields.Many2one('dependency', "Dependency")
    subdependency_id = fields.Many2one(SUB_DEPENDENCY_MODEL, "Sub Dependency")
    dependency_holder = fields.Char("Dependency Holder")
    responsible_user_id = fields.Many2one(RES_USERS_MODEL, string='Responsible')
    type_of_resource = fields.Char("Type Of Resource")

    bank_id = fields.Many2one(RES_PARTNER_BANK_MODEL, "Bank")
    account_number = fields.Char(
        related="bank_id.acc_number", string='Account Number')
    request_office = fields.Char("Request Office")
    permanent_instructions = fields.Text("Permanent Instructions")
    fund_observation = fields.Text("Observations")

    hide_is_manually = fields.Boolean(
        'Hide Manully Button', default=True, compute='get_record_state_change')
    hide_is_auto = fields.Boolean(
        'Hide Auto Button', default=True, compute='get_record_state_change')
    beneficiary_id = fields.Many2one(RES_PARTNER_MODEL, "Beneficiary")
    provider_id = fields.Many2one(RES_PARTNER_MODEL, "Provider")
    payment_request_id = fields.Many2one(PAYMENT_REQUEST_MODEL,copy=False)
    
    @api.depends('state', 'is_manually')
    def get_record_state_change(self):
        for rec in self:
            hide_is_manually = True
            hide_is_auto = True
            if rec.is_manually and rec.state == 'draft':
                hide_is_manually = False
            if not rec.is_manually and rec.state == 'requested':
                hide_is_auto = False
            rec.hide_is_manually = hide_is_manually
            rec.hide_is_auto = hide_is_auto

    @api.model
    def create(self, vals):
        res = super(RequestOpenBalanceInvestment, self).create(vals)
        if res and res.is_cancel_collaboration and res.type_of_operation != 'withdrawal_cancellation':
            raise ValidationError(
                _(TYPE_OF_OPERATION_VALID_ERROR))
        if res and not res.is_cancel_collaboration and res.type_of_operation == 'withdrawal_cancellation':
            raise ValidationError(
                _(CANT_CREATE_OPERATION_VALID_ERROR))

        if res.new_journal_id:
            sequence = res.new_journal_id and res.new_journal_id.sequence_id or False
            if not sequence:
                raise UserError(_('Please define a sequence on your journal.'))

            res.first_number = sequence.with_context(
                ir_sequence_date=res.request_date).next_by_id()

        return res

    def write(self, vals):
        res = super(RequestOpenBalanceInvestment, self).write(vals)
        for rec in self:
            if rec.is_cancel_collaboration and rec.type_of_operation != 'withdrawal_cancellation':
                raise ValidationError(
                    _(TYPE_OF_OPERATION_VALID_ERROR))
            if not rec.is_cancel_collaboration and rec.type_of_operation == 'withdrawal_cancellation':
                raise ValidationError(
                    _(CANT_CREATE_OPERATION_VALID_ERROR))
        return res

    @api.constrains('operation_number')
    def _check_operation_number(self):
        if self.operation_number and not self.operation_number.isnumeric():
            raise ValidationError(_(OPERATION_NUMBER_NOT_NUMERIC_ERROR_MESSAGE))

    def reject_request(self):

        activity_type = self.env.ref(MAIL_ACTIVITY_DATA).id
        summary = "Rejection'" + str(self.name) + "'Increases and withdrawals"
        activity_obj = self.env[MAIL_ACTIVITY]
        model_id = self.env[IR_MODEL].sudo().search([('model', '=', REQUEST_OPEN_BALANCE_INVEST)]).id
        activity_obj.create({'activity_type_id': activity_type,
                           'res_model': REQUEST_OPEN_BALANCE_INVEST, 'res_id': self.id,
                           'res_model_id':model_id,
                           'summary': summary, 'user_id': self.user_id.id})


        return {
            'name': 'Reason for Rejection',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': False,
            'res_model': 'reason.rejection.open.bal',
            'type': IR_ACTIONS_ACT_WINDOW,
            'target': 'new'
        }

    def approve_investment(self):
        today = datetime.today().date()
        user = self.env.user
        unit_req_transfer_id = False
        employee = self.env[HR_EMPLOYEE_MODEL].search(
            [('user_id', '=', user.id)], limit=1)
        if employee and employee.dependancy_id:
            unit_req_transfer_id = employee.dependancy_id.id
        is_agr = True
        is_balance = False
        dependency_id = False
        if self.trust_id:
            is_agr = False
            is_balance = True
            dependency_id = self.trust_id and self.trust_id.dependency_id and self.trust_id.dependency_id.id or False
        activity_type = self.env.ref(MAIL_ACTIVITY_DATA).id
        summary = "Approve'" + str(self.name) + "'Increases and withdrawals"
        activity_obj = self.env[MAIL_ACTIVITY]
        model_id = self.env[IR_MODEL].sudo().search([('model', '=', REQUEST_OPEN_BALANCE_INVEST)]).id
        activity_obj.create({'activity_type_id': activity_type,
                           'res_model': REQUEST_OPEN_BALANCE_INVEST, 'res_id': self.id,
                           'res_model_id':model_id,
                           'summary': summary, 'user_id': self.user_id.id})

        return {
            'name': 'Approve Request',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': False,
            'res_model': 'approve.investment.bal.req',
            'type': IR_ACTIONS_ACT_WINDOW,
            'target': 'new',
            'context': {
                'default_operation_number': self.operation_number,
                'default_agreement_number': self.agreement_number,
                'default_amount': self.opening_balance,
                'default_date': today,
                'default_employee_id': employee.id if employee else False,
                'default_fund_type': self.fund_type_id and self.fund_type_id.id or False,
                'show_for_agreement': 1,
                'show_agreement_name': 1,
                'default_bank_account_id': self.origin_journal_id and self.origin_journal_id.id or False,
                'default_desti_bank_account_id': self.destination_journal_id and self.destination_journal_id.id or False,
                'default_unit_req_transfer_id': unit_req_transfer_id,
                'default_fund_id': self.fund_id and self.fund_id.id or False,
                'default_agreement_type_id': self.type_of_agreement_id and self.type_of_agreement_id.id or False,
                'default_base_collabaration_id': self.bases_collaboration_id and self.bases_collaboration_id.id or False,
                'default_type_of_operation': self.type_of_operation,
                'default_origin_resource_id': self.origin_resource_id and self.origin_resource_id.id or False,
                'default_is_agr': is_agr,
                'default_is_balance': is_balance,
                'default_dependency_id': dependency_id,
                'default_patrimonial_id': self.patrimonial_id.id if self.patrimonial_id else False,
                'default_trust_id': self.trust_id.id if self.trust_id else False
            }
        }


class Project(models.Model):
    _inherit = 'project.project'

    def name_get(self):
        result = []
        for rec in self:
            name = rec.name
            if rec.number_agreement and self.env.context and self.env.context.get('from_agreement', True):
               
                name = rec.number_agreement
            result.append((rec.id, name))
        return result


class RequestOpenBalanceFinance(models.Model):

    _name = 'request.open.balance.finance'
    _inherit = [MAIL_THREAD_MODEL, MAIL_ACTIVITY_MIXIN_MODEL]
    _description = "Request to Open Balance For Finanace"
    _rec_name = 'operation_number'

    request_id = fields.Many2one(REQUEST_OPEN_BALANCE_INVEST, "Request")
    invoice = fields.Char("Invoice")
    operation_number = fields.Char(OPERATION_NUMBER)
    agreement_number = fields.Char(AGGREMENT_NUMBER)
    bank_account_id = fields.Many2one(
        ACCOUNT_JOURNAL_MODEL, "Origin Bank",domain="[('type', '=', 'bank')]")
    origin_account = fields.Many2one(related="bank_account_id.bank_account_id" , string="Account Origin")
    desti_bank_account_id = fields.Many2one(
        ACCOUNT_JOURNAL_MODEL, "Destination Bank", domain="[('type', '=', 'bank')]")
    desti_account = fields.Many2one(related="desti_bank_account_id.bank_account_id" , string="Destination Account")
    currency_id = fields.Many2one(
        RES_CURRENCY_MODEL, default=lambda self: self.env.user.company_id.currency_id)
    amount = fields.Monetary("Amount")
    dependency_id = fields.Many2one('dependency', "Dependency")
    sub_dependency_id = fields.Many2one(SUB_DEPENDENCY_MODEL, "Subdependency")
    date = fields.Date("Application date",default=fields.Date.today)
    concept = fields.Text("Application Concept")
    user_id = fields.Many2one(
        RES_USERS_MODEL, default=lambda self: self.env.user.id, string="Applicant")
    unit_req_transfer_id = fields.Many2one(
        'dependency', string="Unit requesting the transfer")
    date_required = fields.Date("Date Required")
    fund_type = fields.Many2one(FUND_TYPE_MODEL, "Background")
    investment_fund_id = fields.Many2one('investment.funds', 'Investment Fund')

    agreement_type_id = fields.Many2one(
        AGREEMENT_AGREEMENT_TYPE_MODEL, 'Agreement Type')
    fund_id = fields.Many2one(AGREEMENT_FUND_MODEL, 'Fund')
    base_collabaration_id = fields.Many2one(
        BASES_COLLABORATION_MODEL, 'Name Of Agreements')

    reason_rejection = fields.Text("Reason Rejection")
    state = fields.Selection([('draft', 'Draft'),
                              ('requested', 'Requested'),
                              ('rejected', 'Rejected'),
                              ('approved', 'Approved'),
                              ('sent', 'Sent'),
                              ('confirmed', 'Confirmed'),
                              ('done', 'Done'),
                              ('canceled', 'Canceled')], string="Status", default="draft")
    payment_ids = fields.Many2many(ACCOUNT_PAYMENT_MODEL, 'rel_req_payment', 'payment_id', 'payment_request_rel_id',
                                   "Payments")
    payment_count = fields.Integer(compute="count_payment", string="Payments")

    #=== New fields OS-Odoo-06 =========#
    def _get_prepare_by_employe(self):
        emp_id = self.env[HR_EMPLOYEE_MODEL].search([('user_id','=',self.env.user.id)],limit=1)
        if emp_id:
            return emp_id.id
        else:
            return False
        
    trasnfer_request = fields.Selection([('investments','Investments'),('projects','Projects'),('finances','Finances')],string='Transfer Request')
    is_manual = fields.Boolean(string="Manual Registration",copy=False,default=False)
    attention_to_emp_id = fields.Many2one(HR_EMPLOYEE_MODEL,"Attention to")
    prepared_by_emp_id = fields.Many2one(HR_EMPLOYEE_MODEL,"Prepared by",default=_get_prepare_by_employe)
    prepared_by_user_id = fields.Many2one("res.users",string="Creation Users",default=lambda self: self.env.user.id)
    prepared_by_dept_id = fields.Many2one(related="prepared_by_user_id.department_id",string="Department to which it belongs")
    
    authorized_by_emp_id = fields.Many2one(HR_EMPLOYEE_MODEL,"Authorized by")
    authorized_by_user_id = fields.Many2one("res.users",string="Authorized Users")
    authorized_by_dept_id = fields.Many2one(related="authorized_by_user_id.department_id",string="Department to which the authorizing person belongs")
    check = fields.Boolean('Check', default=False)
    from_app_request = fields.Boolean("From app_request",default=False)
    from_money_fund  = fields.Selection([('add', 'Increment'),
                              ('min', 'Ministration'),
                              ('refund','Return Refund')],
                              string="Is For Money Fund?", required=False)

    @api.constrains('operation_number')
    def _check_operation_number(self):
        if self.operation_number and not self.operation_number.isnumeric():
            raise ValidationError(_(OPERATION_NUMBER_NOT_NUMERIC_ERROR_MESSAGE))
        

    @api.constrains('amount')
    def _check_operation_number(self):
        if self.amount<=0:
            raise ValidationError(_('The amount need to be upper than zero.'))
        

    @api.model
    def create(self, vals):
        res = super(RequestOpenBalanceFinance, self).create(vals)
        if res.is_manual:
            name = self.env['ir.sequence'].next_by_code('request.open.balance.finance.manual')
            res.invoice = name
        if not res.operation_number:
            rec = self.env['ir.sequence'].next_by_code(REQUEST_OPEN_BALANCE_FINANCE_MODEL )
            res.operation_number = rec
        return res

    def open_payments(self):
        action = self.env.ref('account.action_account_payments').read()[0]
        action['context'] = {'default_payment_type': 'inbound',
                             'default_partner_type': 'customer',
                             'show_for_agreement': True
                             }

        if self.payment_ids:
            action['domain'] = [('id', 'in', self.payment_ids.ids)]
        else:
            action['domain'] = [('id', 'in', [])]
        return action

    def count_payment(self):
        for rec in self:
            rec.payment_count = len(self.payment_ids)

    def reject_request(self):
        other_rec = self.filtered(lambda x:x.state!='requested')
        if other_rec:
           raise ValidationError(_('This action can only be performed for transfers in Requested status')) 
        
        return {
            'name': 'Reason for Rejection',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': False,
            'res_model': 'reason.rejection.open.bal',
            'type': IR_ACTIONS_ACT_WINDOW,
            'target': 'new',
            'context':{'active_model':REQUEST_OPEN_BALANCE_FINANCE_MODEL ,'active_ids':self.ids}
        }

    def mass_approve_finance(self):
        other_rec = self.filtered(lambda x:x.state!='requested')
        if other_rec:
           raise ValidationError(_('This action can only be performed for transfers in Requested status'))
        return {
            'name': _('Approve Transfers'),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': False,
            'res_model': 'mass.approve.transfer',
            'type': IR_ACTIONS_ACT_WINDOW,
            'target': 'new',
            'context':{'active_model':REQUEST_OPEN_BALANCE_FINANCE_MODEL ,'active_ids':self.ids}
        }
        
            
    def approve_finance(self):
        ok,msg=self._check_bank_accounts(self)
        if ok:
            self.state = 'approved'
            self.authorized_by_user_id = self.env.user.id
            emp_id = self.env[HR_EMPLOYEE_MODEL].search([('user_id','=',self.env.user.id)],limit=1)
            if emp_id:
                self.authorized_by_emp_id = emp_id.id
            if self.prepared_by_user_id:
                activity_type = self.env.ref(MAIL_ACTIVITY_DATA).id
                summary = "Approve Transfer Request"
                if self.invoice:
                    summary = _("Approve '%s' Transfer Request") % str(self.invoice)
                
                activity_obj = self.env[MAIL_ACTIVITY]
                model_id = self.env[IR_MODEL].sudo().search([('model', '=', REQUEST_OPEN_BALANCE_FINANCE_MODEL )]).id
                activity_obj.create({'activity_type_id': activity_type,
                                'res_model': REQUEST_OPEN_BALANCE_FINANCE_MODEL , 'res_id': self.id,
                                'res_model_id':model_id,
                                'summary': summary, 'user_id': self.prepared_by_user_id.id})
        else:
            raise ValidationError(msg)
    def canceled_finance(self):
        self.state = 'canceled'

    def confirmed_finance(self):
        self.state = 'confirmed'

    def reject_finance(self):
        self.state = 'rejected'
        if self.prepared_by_user_id:
            activity_type = self.env.ref(MAIL_ACTIVITY_DATA).id
            summary = "Reject Transfer Request"
            if self.invoice:
                summary = "Reject '" + str(self.invoice) + "' Transfer Request"
            
            activity_obj = self.env[MAIL_ACTIVITY]
            model_id = self.env[IR_MODEL].sudo().search([('model', '=', REQUEST_OPEN_BALANCE_FINANCE_MODEL )]).id
            activity_obj.create({'activity_type_id': activity_type,
                               'res_model': REQUEST_OPEN_BALANCE_FINANCE_MODEL , 'res_id': self.id,
                               'res_model_id':model_id,
                               'summary': summary, 'user_id': self.prepared_by_user_id.id})

    def action_schedule_transfers(self):
        payment_obj = self.env[ACCOUNT_PAYMENT_MODEL]
        today = datetime.today().date()
        data = {}
        for rec in self:
            if rec.state == 'approved' and rec.bank_account_id:
                bank_acc = rec.bank_account_id
                if bank_acc not in data.keys():
                    data.update({
                        bank_acc: [rec]
                    })
                else:
                    data.update({
                        bank_acc: data.get(bank_acc) + [rec]
                    })
        for acc, rec_list in data.items():
            dest_acc = {}
            for rec in rec_list:
                if rec.desti_bank_account_id:
                    dest_bank_acc = rec.desti_bank_account_id
                    if dest_bank_acc not in dest_acc.keys():
                        dest_acc.update({
                            dest_bank_acc: [rec]
                        })
                    else:
                        dest_acc.update({
                            dest_bank_acc: dest_acc.get(dest_bank_acc) + [rec]
                        })
            for dest_acc, rec_list in dest_acc.items():
                amt = 0
                for rec in rec_list:
                    amt += rec.amount
                dep_id = False
                sub_dep_id = False
                payment_date = fields.Date().today()
                if rec_list:
                    dep_id = rec_list[0].dependency_id and rec_list[
                        0].dependency_id.id or False
                    sub_dep_id = rec_list[0].sub_dependency_id and rec_list[
                        0].sub_dependency_id.id or False
                    payment_date = rec_list[0].date_required
                    
                payment = payment_obj.create({
                    'payment_type': 'transfer',
                    'amount': amt,
                    'journal_id': acc.id,
                    'destination_journal_id': dest_acc.id,
                    'payment_date': today,
                    'payment_method_id': self.env.ref('account.account_payment_method_manual_in').id,
                    'dependancy_id': dep_id,
                    'sub_dependancy_id': sub_dep_id,
                    'payment_date':payment_date,
                })
                if payment:
                    for rec in rec_list:
                        rec.payment_ids = [(4, payment.id)]
                        rec.state = 'sent'
    #Sección de bloqueo de acciones por Seguridad-Finanzas
    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super().fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])
        is_admin= self.env.user.has_group('jt_payroll_payment.group_on_finance_admin')
        is_approve_user = self.env.user.has_group('jt_currency_purchase_req.group_request_approval')
        """
        Sección de Seguridad aplicado para:
			Módulo de Finanzas
				-Solicitud de Transferencia Bancaria/Solicitud de transferencia           
		"""
        views=['Trasfer Request List view','Transfer Request Form View']
        buttons=['request_finance','approve_finance','reject_request']
        
        if not (is_admin or is_approve_user):
            if res['name'] in views:
                for node in doc.xpath("//" + view_type):
                    node.set('edit', '0')
                    node.set('create', '0')
                    node.set('delete', '0')
                    node.set('import', '0')
                    node.set('export_xlsx', '0')
                    node.set('request','0')
                    node.set('export','0')
            for node in doc.xpath("//button"):
                if ('Transfer Request Form View' == res['name']) and (node.attrib['name'] in buttons):
                    node.set('modifiers','{"invisible":true}')
        res['arch'] = etree.tostring(doc)
        return res

class AccountPayment(models.Model):
    _inherit = 'account.payment'

    def post(self):
        res = super(AccountPayment, self).post()
        finance_req_obj = self.env[REQUEST_OPEN_BALANCE_FINANCE_MODEL ]
        for payment in self:
            #================Validate the request payments====================#
            for inv in payment.invoice_ids.filtered(lambda x:x.is_project_payment or x.is_payment_request and x.invoice_payment_state=='paid' and x.folio):
                pay_req_ids = self.env[PAYMENT_REQUEST_MODEL].search([('counter_receipt_sheet','=',inv.folio),('state','=','requested')])
                for pay_req in pay_req_ids:
                    pay_req.action_paid()
            #========================END=========================#
            finance_reqs = finance_req_obj.search(
                [('payment_ids', 'in', payment.id)])
            for fin_req in finance_reqs:
                fin_req.confirmed_finance()
                if fin_req.request_id:
                    fin_req.request_id.state = 'done'
                    if fin_req.request_id.balance_req_id:
                        print ("=======",fin_req.request_id.balance_req_id)
                        balance_req = fin_req.request_id.balance_req_id
                        balance_req.action_confirmed()
                        if balance_req.bases_collaboration_id:
                            if balance_req.type_of_operation in ('withdrawal_cancellation','withdrawal'):
                                balance_req.bases_collaboration_id.available_bal = 0
                                balance_req.bases_collaboration_id.state = 'cancelled'
                            elif balance_req.type_of_operation == 'retirement':
                                balance_req.create_payment_request = True
                                balance_req.bases_collaboration_id.available_bal -= fin_req.amount
                            else:
                                balance_req.bases_collaboration_id.available_bal += fin_req.amount

                        if balance_req.patrimonial_resources_id:
                            if balance_req.type_of_operation == 'withdrawal_cancellation':
                                balance_req.patrimonial_resources_id.available_bal = 0
                                balance_req.patrimonial_resources_id.action_set_cancel()
                            elif balance_req.type_of_operation == 'withdrawal':
                                balance_req.patrimonial_resources_id.available_bal = 0
                                balance_req.patrimonial_resources_id.action_set_cancel()
                            elif balance_req.type_of_operation == 'retirement':
                                balance_req.create_payment_request = True
                                balance_req.patrimonial_resources_id.available_bal -= fin_req.amount
                            else:
                                balance_req.patrimonial_resources_id.available_bal += fin_req.amount

        return res


class AccountMoveLine(models.Model):

    _inherit = 'account.move.line'

    collaboration_id = fields.Many2one(BASES_COLLABORATION_MODEL)
    patrimonial_id = fields.Many2one(
        PATRIMONIAL_RESOURCES_MODEL, 'Patrimonial Resources')
    request_id = fields.Many2one(REQUEST_OPEN_BALANCE_MODEL, 'Operation Request')

class PeriodsClosing(models.Model):

    _name = 'periods.closing'
    _description = "Closing Period"

    start_date = fields.Date("Start Date")
    end_date = fields.Date("End Date")
    collaboration_id = fields.Many2one(BASES_COLLABORATION_MODEL)


class AccountMove(models.Model):

    _inherit = ACCOUNT_MOVE_MODEL

    #Campo para vincular con solicitud de pago desde retiro de base de colaboración
    collaboration_base_payment_request_id = fields.Integer()



class OriginResourceLine(models.Model):
    _name = ORIGIN_RESOURCE_LINE_MODEL


    egress_key_id = fields.Many2one("egress.keys", string="Egress key")
    egress_key = fields.Char(related='egress_key_id.key', store=False)
    account_id = fields.Many2one(ACCOUNT_ACCOUNT_MODEL, string="Account accountant")
    account_ie_id = fields.Many2one('association.distribution.ie.accounts', string='Account IE')
    origin_resource_id = fields.Many2one('resource.origin', 'Origin resource') ##
    dependency_id = fields.Many2one('dependency',string="Dependency")
    sub_dependency_id = fields.Many2one(SUB_DEPENDENCY_MODEL, string="Subdependency")
    program_code_id = fields.Many2one('program.code','Program code')
    currency_id = fields.Many2one(RES_CURRENCY_MODEL, string='Currency')
    amount = fields.Monetary(string="Amount", currency_field='currency_id')
    collaboration_id = fields.Many2one(BASES_COLLABORATION_MODEL)
    collaboration_request_id = fields.Many2one(REQUEST_OPEN_BALANCE_MODEL)
    trust_id = fields.Many2one(AGREEMENT_TRUST_MODEL)
    patrimonial_resource_id = fields.Many2one(PATRIMONIAL_RESOURCES_MODEL)