from odoo import models, fields,api,_
from odoo.exceptions import ValidationError

class ComissionSettings(models.Model):

    _name = 'comission.settings'
    _description = "Comission Settings"
    _rec_name = 'comission_description'

    type_of_revenue_collection = fields.Selection([('billing', 'Billing'),
                                                   ('deposit_cer', 'Certificates of deposit'),
                                                   ('dgae_ref', 'Reference of DGAE'),
                                                   ('dgoae_trades', 'Trades DGOAE'),
                                                   ('education_servs', 'Education Services'),
                                                   ('aspirants', 'Aspirants'),
                                                   ('exam_prep', 'Exam Preparation')], string="Type of Revenue Collection",copy=False)
    income_bank_journal_id = fields.Many2one('account.journal', "Bank")
    income_bank_account = fields.Many2one(related="income_bank_journal_id.bank_account_id", string="Bank Account")
    l10n_mx_edi_payment_method_id = fields.Many2one('l10n_mx_edi.payment.method', "Payment Method")
    payment_method_code = fields.Char(related='l10n_mx_edi_payment_method_id.code', string="ID Payment Method")
    comission_description = fields.Char('Description')
    type_of_currency = fields.Selection(
        [('national', 'National Currency'), ('foreign', 'Foreign Currency')])
    comission_calculation = fields.Selection(
        [('fixed', 'Fixed')])
    comission_amount = fields.Float('Comission amount')
    comission_line_ids = fields.One2many('comission.settings.line','comission_settings_id','Definition')
    sum_percentage = fields.Float("Sum Percentage", compute="_compute_percentage")
    is_retention = fields.Boolean("Take comission?")



    @api.constrains('comission_amount')
    def _check_base_amount(self):
        if self.comission_amount<0.0:
            raise ValidationError(_('The comission amount cannot be negative'))

    @api.depends('comission_line_ids')
    def _compute_percentage(self):
        self.sum_percentage = sum(x.comission_percentage for x in self.comission_line_ids)

    @api.constrains('sum_percentage')
    def _check_sum_percentage(self):
        if self.sum_percentage != 0 and (self.sum_percentage < 100 or self.sum_percentage>100):
            if self.sum_percentage != 100:
                raise ValidationError(_('Please configure comission 100% Percentage'))

class ComissionSettingsLine(models.Model):
    
    _name = 'comission.settings.line'
    _description = "Comission Settings Line"
    
    comission_settings_id = fields.Many2one('comission.settings',string='Comission',ondelete='cascade')
    comission_percentage = fields.Float('%')
    account_id = fields.Many2one("account.account",'Accounts')