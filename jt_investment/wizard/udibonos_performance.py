
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
import datetime
import pandas
from dateutil.relativedelta import relativedelta


class UDIPerformanceWizard(models.TransientModel):
    _name = 'udibonos.performance.wizard'
    _description = 'Wizard para Rendimiento'


    id_udibono = fields.Integer()

    date_consult = fields.Date(
        default=datetime.date.today())

    previous_month_titles_value = fields.Float(digits=(8, 6))
    current_month_titles_value = fields.Float(digits=(8, 6))
    
    @api.constrains('previous_month_titles_value')
    def check_previous_month_titles_value(self):
        if self.previous_month_titles_value <= 0:
            raise UserError(
                _('Negative values or values equal to zero are not allowed'))

    @api.constrains('current_month_titles_value')
    def check_current_month_titles_value(self):
        if self.current_month_titles_value <= 0:
            raise UserError(
                _('Negative values or values equal to zero are not allowed'))
          
    def performance(self):
        

            # Seguridad de la función
        if not self.env.user.has_group('jt_investment.group_investment_admin_user'):
            raise UserError(_("You don't have permission to perform this action."))

        # Se obtiene el id del udibono actual
        self.id_udibono = self.env.context.get('active_id')

        # objeto de tipo udibono para consultar el id del udibono actual
        udibono = self.env['investment.udibonos'].search(
            [('id', '=', self.id_udibono)])

        # Verifica que la fecha sea el ultimo dia habil del mes
        current_date = pandas.Timestamp(self.date_consult)
        if not current_date.is_month_end:
            raise UserError(_("The date is not the last day of the month."))
        

        # Verificar que el registro inicie en una fecha posterior a cuando se activa la inversión
        if udibono.date_days_period_elapsed > self.date_consult:
            raise UserError("Lo siento, no es posible realizar acciones antes de la fecha de inicio de la inversión. Por favor, asegúrate de que la fecha de la acción sea posterior a la fecha de inicio de la inversión.")
        

        # Dias del periodo transcurridos
        udibono.days_period_elapsed = ((self.date_consult-udibono.date_days_period_elapsed).days)+udibono.add_days_of_periot
        # Verificar que primero se haya realizado un pago de cupón
        if udibono.days_period_elapsed >= udibono.days_of_periot:
            raise UserError(_(f"Primero debe realizar un Pago el Pago de Cupón. Los días del periodo transcurridos ({udibono.days_period_elapsed}) son mayores a los días del periodo ( {udibono.days_of_periot}). "))
        

        
        # objeto de tipo accrued interest para consultar la fecha
        date_duplicate = self.env['udibonos.performance'].search(
            [('udibonos_id', '=', udibono.id), ('description', '=', 'Rendimiento')])
        

        if len(date_duplicate)== 0:
            # Verificar que el registro inicie en una fecha no mayor a un mes que la fecha se se activa la inversión
            if udibono.date_days_period_elapsed.month != self.date_consult.month and udibono.date_days_period_elapsed.year != self.date_consult.year:
                raise UserError(
                    'Tienes movimientos pendientes que aún no han sido procesados. Por favor, revisa las fechas y asegúrate de que estén dentro del período correspondiente.')


        # Verificar que no se haya creado un registro para la fecha actual
        for rec in date_duplicate:
            if rec.date == self.date_consult and rec.description == 'Rendimiento':
                raise UserError(_("Ya se ha creado registro para esta fecha"))

        # Verificar que no se haya espacio de más de un mes entre dos movimientos haciendo resta entre meses
        if date_duplicate:
            last_date = date_duplicate[-1].date
            # Se restan los años y se multiplica por 12, luego se le suma la diferencia de meses para obtener el total de meses
            diff_months = (self.date_consult.year - last_date.year) * 12 + (self.date_consult.month - last_date.month)
            if diff_months != 1:
                raise UserError('No puede haber más de un mes de diferencia entre dos movimientos.')

        # Verificar que el movimiento no coincida con el mes y año de udibono.expiry_date
        if self.date_consult.month == udibono.expiry_date.month and self.date_consult.year == udibono.expiry_date.year:
            raise UserError(
                'No puede haber movimientos en el mes de vencimiento.')

        # Asigna valores para realizar el calculo 
        udibono.movement_date = self.date_consult
        udibono.previous_month_titles_value = self.previous_month_titles_value
        udibono.current_month_titles_value = self.current_month_titles_value

        # Se llama a la funcion para realizar el calculo de rendimiento 
        udibono.calculate_performance()

        return True

