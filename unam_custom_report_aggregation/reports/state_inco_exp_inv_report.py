# -*- coding: utf-8 -*-
from datetime import datetime
from odoo import api, fields, models, _


class DetailStatementOfIncomeExpensesandInvestment(models.AbstractModel):
    _inherit = 'jt_account_module_design.stat.inc.exp.inv.report'

    filter_all_entries = False


    def _get_lines(self, options, line_id=None):
        posted_only = not options.get('all_entries')

        lines = []

        start = datetime.strptime(
            str(options['date'].get('date_from')), '%Y-%m-%d').date()
        end = datetime.strptime(
            options['date'].get('date_to'), '%Y-%m-%d').date()

        if options.get('all_entries') is False:
            move_state_domain = ('move_id.state', '=', 'posted')
        else:
            move_state_domain = ('move_id.state', '!=', 'cancel')

        domain = [('date', '<=', end),move_state_domain]
        
        concept_ids = self.env['detailed.statement.income'].search([('inc_exp_type','!=',False)])
        
        
        list_data = ['income','expenses','investments','other expenses']
        
        remant_authorized = 0
        remant_transfers = 0
        remant_assign = 0
        remant_contable_exercised = 0
        remant_e_income = 0
        remant_extra_book = 0
        remant_exercised = 0
        remant_to_exercised = 0

        expenses_authorized = 0
        expenses_transfers = 0
        expenses_assign = 0
        expenses_contable_exercised = 0
        expenses_e_income = 0
        expenses_extra_book = 0
        expenses_exercised = 0
        expenses_to_exercised = 0

        year_authorized = 0
        year_transfers = 0
        year_assign = 0
        year_contable_exercised = 0
        year_e_income = 0
        year_extra_book = 0
        year_exercised = 0
        year_to_exercised = 0
        
        for type in list_data:
            type_concept_ids = concept_ids.filtered(lambda x:x.inc_exp_type == type)
            major_ids = type_concept_ids.mapped('major_id')

            if type_concept_ids:
                name = type.upper()
                if self.env.lang == 'es_MX' and type == 'income':
                    str1 = 'INGRESOS'
                    name = str1.upper()
                if self.env.lang == 'es_MX' and type == 'expenses':
                    str2 = 'GASTOS'
                    name = str2.upper()
                if self.env.lang == 'es_MX' and type == 'investments':
                    str3 = 'INVERSIONES'
                    name = str3.upper()
                if self.env.lang == 'es_MX' and type == 'other expenses':
                    str4 = 'OTROS GASTOS'
                    name = str4.upper()

                lines.append({
                    'id': type,
                    'name': name,
                    'columns': [
                                {'name': ''},
                                {'name': ''},
                                {'name': ''},
                                {'name': ''},
                                {'name': ''},
                                {'name': ''},
                                {'name': ''},
                                {'name': ''},
                                {'name': ''},
                                {'name': ''},
                                ],
    
                    'level': 1,
                    'unfoldable': False,
                    'unfolded': True,
                })
            
            for major in major_ids:  
                                
                total_per = 100


                cons = type_concept_ids.filtered(lambda x:x.major_id.id == major.id)
                lines.append(self.format_line(maj=major, children=cons,
                    cols=[
                        {'name': ''},
                        {'name': ''},
                        {'name': ''},
                        {'name': ''},
                        {'name': ''},
                        {'name': ''},
                        {'name': ''},
                        {'name': ''},
                        {'name': ''},
                        {'name': ''},
                    ],
                    extra_info={
                        'class':'text-left'
                    }
                ))
                
                for con in cons:
                    account_lines = []
                    total_authorized = 0
                    total_transfers = 0
                    total_assign = 0
                    total_contable_exercised = 0
                    total_e_income = 0
                    total_extra_book = 0
                    total_exercised = 0
                    total_to_exercised = 0
                                        

                    for acc in con.account_ids:
                        authorized = 0
                        assign = 0
                        transfers = 0
                        # ======Budget Data ======#
                        if con.item_ids:
                            acc_item_ids = con.item_ids.filtered(lambda x:x.unam_account_id.id==acc.id)
                            if acc_item_ids:
                                program_code_ids = self.env['program.code'].search([('item_id','in',acc_item_ids.ids)])
                                if program_code_ids:
                                    #self.env.cr.execute("select coalesce(sum(ebl.authorized),0) from expenditure_budget_line ebl where ebl.program_code_id in %s and ebl.imported_sessional IS NULL and start_date >= %s and end_date <= %s", (tuple(program_code_ids.ids),start,end))
                                    self.env.cr.execute("select coalesce(sum(ebl.authorized),0) from expenditure_budget_line ebl where ebl.program_code_id in %s and ebl.imported_sessional IS NULL and end_date <= %s", (tuple(program_code_ids.ids),end))
                                    my_datas = self.env.cr.fetchone()
                                    if my_datas:
                                        authorized = my_datas[0]
        
                                    self.env.cr.execute("select coalesce(SUM(CASE WHEN al.line_type = %s THEN al.amount ELSE -al.amount END),0) from adequacies_lines al,adequacies a where a.state=%s and al.program in %s and a.id=al.adequacies_id", ('increase','accepted',tuple(program_code_ids.ids)))
                                    my_datas = self.env.cr.fetchone()
                                    if my_datas:
                                        assign = my_datas[0]
                                
                            transfers = assign
                            assign += authorized
                            
                            authorized = authorized/1000
                            assign = assign/1000
                            transfers = transfers/1000
                            
                        total_authorized += authorized
                        
                        total_transfers += transfers

                        total_assign += assign

                        values= self.env['account.move.line'].search(domain + [('account_id', 'in', acc.ids)])
                        contable_exercised = sum(x.credit - x.debit for x in values)
                        #contable_exercised = abs(contable_exercised)
                        contable_exercised = contable_exercised/1000
                        
                        #contable_exercised = 0
                        total_contable_exercised += contable_exercised

                        e_income = 0
                        total_e_income += e_income
                        
                        extra_book = 0
                        total_extra_book += extra_book
                        
                        values= self.env['account.move.line'].search(domain + [('move_id.payment_state','in',('for_payment_procedure','payment_not_applied')),('account_id', 'in', acc.ids)])
                        exercised = sum(x.debit-x.credit for x in values)
                        exercised = exercised/1000
                        total_exercised += exercised

                        values= self.env['account.move.line'].search(domain + [('budget_id','!=',False),('account_id', 'in', acc.ids)])
                        to_exercised = sum(x.debit-x.credit for x in values)
                        to_exercised = to_exercised/1000
                        
                        total_to_exercised += to_exercised
                         
                            
                        per = 0.00
                        if exercised:
                            per = 100.00
                        if assign > 0:
                            per = (exercised/assign)*100
                        
                        dep_domain = [
                            ('date', '>=', start),
                            ('date', '<=', end),
                            ('parent_state', '=', 'posted') if posted_only else ('parent_state', '!=', 'cancel')
                        ]
                        deps = self.env['dependency'].get_dependencies_for_account(acc, dep_domain)

                        account_lines.append(self.format_line(maj=major, con=con, acc=acc, children=deps, cols=[
                            self._format({'name': authorized},figure_type='float'),
                            self._format({'name': transfers},figure_type='float'),
                            self._format({'name': assign},figure_type='float'),
                            {'name': ''},
                            self._format({'name': contable_exercised},figure_type='float'),
                            self._format({'name': e_income},figure_type='float'),
                            self._format({'name': extra_book},figure_type='float'),
                            self._format({'name': exercised},figure_type='float'),
                            {'name': per,'class':'number'},
                            self._format({'name': to_exercised},figure_type='float'),
                        ]))
                        ################################
                        ####### DEP CALCULATIONS #######
                        ################################
                        for dep in deps:

                            dep_authorized = 0
                            dep_assign = 0
                            dep_transfers = 0
                            # ======Budget Data ======#
                            if con.item_ids:
                                acc_item_ids = con.item_ids.filtered(lambda x:x.unam_account_id.id==acc.id)
                                if acc_item_ids:
                                    program_code_ids = self.env['program.code'].search([
                                        ('item_id','in', acc_item_ids.ids),
                                        ('dependency_id', '=', dep.id)])
                                    if program_code_ids:
                                        self.env.cr.execute("select coalesce(sum(ebl.authorized),0) from expenditure_budget_line ebl where ebl.program_code_id in %s and ebl.imported_sessional IS NULL and end_date <= %s", (tuple(program_code_ids.ids),end))
                                        my_datas = self.env.cr.fetchone()
                                        if my_datas:
                                            dep_authorized = my_datas[0]

                                        self.env.cr.execute("select coalesce(SUM(CASE WHEN al.line_type = %s THEN al.amount ELSE -al.amount END),0) from adequacies_lines al,adequacies a where a.state=%s and al.program in %s and a.id=al.adequacies_id", ('increase','accepted',tuple(program_code_ids.ids)))
                                        my_datas = self.env.cr.fetchone()
                                        if my_datas:
                                            dep_assign = my_datas[0]
                                    
                                dep_transfers = dep_assign
                                dep_assign += dep_authorized
                                
                                dep_authorized /= 1000
                                dep_assign /= 1000
                                dep_transfers /= 1000

                            dep_contable_exercised = sum(x.credit - x.debit for x in self.env['account.move.line'].search(domain + [
                                ('account_id', 'in', acc.ids),
                                ('dependency_id', '=', dep.id)])) / 1000

                            dep_exercised = sum(x.debit-x.credit for x in self.env['account.move.line'].search(domain + [
                                ('move_id.payment_state','in',('for_payment_procedure','payment_not_applied')),
                                ('account_id', 'in', acc.ids),
                                ('dependency_id', '=', dep.id)])) /1000

                            dep_to_exercised = sum(l.debit - l.credit for l in self.env['account.move.line'].search(
                                domain + [
                                ('budget_id','!=',False),
                                ('account_id', 'in', acc.ids),
                                ('dependency_id', '=', dep.id)])) / 1000


                            dep_e_income = 0
                            dep_extra_book = 0


                            dep_per = 0.00
                            if dep_exercised:
                                dep_per = 100.00
                            if dep_assign > 0:
                                dep_per = (dep_exercised/dep_assign)*100
                        ################################
                        ##### END DEP CALCULATIONS #####
                        ################################
                            subdep_domain = [
                                ('date', '>=', start),
                                ('date', '<=', end),
                                ('parent_state', '=', 'posted') if posted_only else ('parent_state', '!=', 'cancel')
                            ]


                            subdeps = self.env['sub.dependency'].get_subdependencies_for_account(acc, dep, subdep_domain)

                            account_lines.append(self.format_line(maj=major, con=con, acc=acc, dep=dep, children=subdeps, cols=[
                                self._format({'name': dep_authorized},figure_type='float'),
                                self._format({'name': dep_transfers},figure_type='float'),
                                self._format({'name': dep_assign},figure_type='float'),
                                {'name': ''},
                                self._format({'name': dep_contable_exercised},figure_type='float'),
                                self._format({'name': dep_e_income},figure_type='float'),
                                self._format({'name': dep_extra_book},figure_type='float'),
                                self._format({'name': dep_exercised},figure_type='float'),
                                {'name': dep_per,'class':'number'},
                                self._format({'name': dep_to_exercised},figure_type='float'),
                            ]))

                            for subdep in subdeps:
                                ################################
                                ###### SUBDEP CALCULATIONS #####
                                ################################
                                subdep_authorized = 0
                                subdep_assign = 0
                                subdep_transfers = 0
                                # ======Budget Data ======#
                                if con.item_ids:
                                    acc_item_ids = con.item_ids.filtered(lambda x:x.unam_account_id.id==acc.id)
                                    if acc_item_ids:
                                        program_code_ids = self.env['program.code'].search([
                                            ('item_id','in', acc_item_ids.ids),
                                            ('dependency_id', '=', dep.id),
                                            ('sub_dependency_id', '=', subdep.id)])
                                        if program_code_ids:
                                            self.env.cr.execute("select coalesce(sum(ebl.authorized),0) from expenditure_budget_line ebl where ebl.program_code_id in %s and ebl.imported_sessional IS NULL and end_date <= %s", (tuple(program_code_ids.ids),end))
                                            my_datas = self.env.cr.fetchone()
                                            if my_datas:
                                                subdep_authorized = my_datas[0]

                                            self.env.cr.execute("select coalesce(SUM(CASE WHEN al.line_type = %s THEN al.amount ELSE -al.amount END),0) from adequacies_lines al,adequacies a where a.state=%s and al.program in %s and a.id=al.adequacies_id", ('increase','accepted',tuple(program_code_ids.ids)))
                                            my_datas = self.env.cr.fetchone()
                                            if my_datas:
                                                subdep_assign = my_datas[0]
                                        
                                    subdep_transfers = subdep_assign
                                    subdep_assign += subdep_authorized
                                    
                                    subdep_authorized /= 1000
                                    subdep_assign /= 1000
                                    subdep_transfers /= 1000

                                subdep_contable_exercised = sum(x.credit - x.debit for x in self.env['account.move.line'].search(domain + [
                                    ('account_id', 'in', acc.ids),
                                    ('dependency_id', '=', dep.id),
                                    ('sub_dependency_id', '=', subdep.id)])) / 1000

                                subdep_exercised = sum(x.debit-x.credit for x in self.env['account.move.line'].search(domain + [
                                    ('move_id.payment_state','in',('for_payment_procedure','payment_not_applied')),
                                    ('account_id', 'in', acc.ids),
                                    ('dependency_id', '=', dep.id),
                                    ('sub_dependency_id', '=', subdep.id)])) /1000

                                subdep_to_exercised = sum(l.debit - l.credit for l in self.env['account.move.line'].search(
                                    domain + [
                                    ('budget_id','!=',False),
                                    ('account_id', 'in', acc.ids),
                                    ('dependency_id', '=', dep.id),
                                    ('sub_dependency_id', '=', subdep.id)])) / 1000


                                subdep_e_income = 0
                                subdep_extra_book = 0


                                subdep_per = 0.00
                                if subdep_exercised:
                                    subdep_per = 100.00
                                if subdep_assign > 0:
                                    subdep_per = (subdep_exercised/subdep_assign)*100
                                ################################
                                #### END SUBDEP CALCULATIONS ###
                                ################################

                                account_lines.append(self.format_line(maj=major, con=con, acc=acc, dep=dep, subdep=subdep, cols=[
                                    self._format({'name': subdep_authorized},figure_type='float'),
                                    self._format({'name': subdep_transfers},figure_type='float'),
                                    self._format({'name': subdep_assign},figure_type='float'),
                                    {'name': ''},
                                    self._format({'name': subdep_contable_exercised},figure_type='float'),
                                    self._format({'name': subdep_e_income},figure_type='float'),
                                    self._format({'name': subdep_extra_book},figure_type='float'),
                                    self._format({'name': subdep_exercised},figure_type='float'),
                                    {'name': subdep_per,'class':'number'},
                                    self._format({'name': subdep_to_exercised},figure_type='float'),
                                ]))

                    if type == 'income':
                        remant_authorized += total_authorized
                        remant_transfers += total_transfers
                        remant_assign += total_assign
                        remant_contable_exercised += total_contable_exercised
                        remant_e_income += total_e_income
                        remant_extra_book += total_extra_book
                        remant_exercised += total_exercised
                        remant_to_exercised += total_to_exercised

                        year_authorized += total_authorized
                        year_transfers += total_transfers
                        year_assign += total_assign
                        year_contable_exercised += total_contable_exercised
                        year_e_income += total_e_income
                        year_extra_book += total_extra_book
                        year_exercised += total_exercised
                        year_to_exercised += total_to_exercised
                        
                    elif type == 'expenses':
                        remant_authorized -= total_authorized
                        remant_transfers -= total_transfers
                        remant_assign -= total_assign
                        remant_contable_exercised -= total_contable_exercised
                        remant_e_income -= total_e_income
                        remant_extra_book -= total_extra_book
                        remant_exercised -= total_exercised
                        remant_to_exercised -= total_to_exercised
                        
                    if type != 'income':
                        expenses_authorized += total_authorized
                        expenses_transfers += total_transfers
                        expenses_assign += total_assign
                        expenses_contable_exercised += total_contable_exercised
                        expenses_e_income += total_e_income
                        expenses_extra_book += total_extra_book
                        expenses_exercised += total_exercised
                        expenses_to_exercised += total_to_exercised

                        year_authorized -= total_authorized
                        year_transfers -= total_transfers
                        year_assign -= total_assign
                        year_contable_exercised -= total_contable_exercised
                        year_e_income -= total_e_income
                        year_extra_book -= total_extra_book
                        year_exercised -= total_exercised
                        year_to_exercised -= total_to_exercised

                    total_per = 0.00
                    if total_exercised:
                        total_per = 100.00
                    if total_assign > 0:
                        total_per = (total_exercised/total_assign)*100

                    lines.append(self.format_line(maj=major, con=con, children=con.account_ids,
                        cols=[
                            self._format({'name': total_authorized},figure_type='float'),
                            self._format({'name': total_transfers},figure_type='float'),
                            self._format({'name': total_assign},figure_type='float'),
                            {'name': ''},
                            self._format({'name': total_contable_exercised},figure_type='float'),
                            self._format({'name': total_e_income},figure_type='float'),
                            self._format({'name': total_extra_book},figure_type='float'),
                            self._format({'name': total_exercised},figure_type='float'),
                            {'name': total_per,'class':'number'},
                            self._format({'name': total_to_exercised},figure_type='float'),
                        ],
                        extra_info={
                            'class': 'text-left'
                        }))
                    lines += account_lines

                    lines.append({
                        'id': 'group_total',
                        'name': 'SUMA',
                        'columns': [
                            self._format({'name': total_authorized},figure_type='float'),
                            self._format({'name': total_transfers},figure_type='float'),
                            self._format({'name': total_assign},figure_type='float'),
                            {'name': ''},
                            self._format({'name': total_contable_exercised},figure_type='float'),
                            self._format({'name': total_e_income},figure_type='float'),
                            self._format({'name': total_extra_book},figure_type='float'),
                            self._format({'name': total_exercised},figure_type='float'),
                            {'name': total_per,'class':'number'},
                            self._format({'name': total_to_exercised},figure_type='float'),
                            ],
                        
                        'level': 1,
                        'unfoldable': False,
                        'unfolded': True,
                        'class':'text-right',
                        'parent_id': 'con' + self.str_id(con) + self.str_id(major),
                    })
            if type=="expenses":

                remant_per = 0.00
                if remant_exercised:
                    remant_per = 100.00
                if remant_assign > 0:
                    remant_per = (remant_exercised/remant_assign)*100
                
                lines.append({
                    'id': 'REMNANT',
                    'name': _('REMAINING BEFORE INVESTMENTS'),
                    'columns': [
                            self._format({'name': remant_authorized},figure_type='float'),
                            self._format({'name': remant_transfers},figure_type='float'),
                            self._format({'name': remant_assign},figure_type='float'),
                            {'name': ''},
                            self._format({'name': remant_contable_exercised},figure_type='float'),
                            self._format({'name': remant_e_income},figure_type='float'),
                            self._format({'name': remant_extra_book},figure_type='float'),
                            self._format({'name': remant_exercised},figure_type='float'),
                            {'name': remant_per,'class':'number'},
                            self._format({'name': remant_to_exercised},figure_type='float'),
                            ],
                
                'level': 1,
                'unfoldable': False,
                'unfolded': True,
                'class':'text-right'
            })

        expenses_per = 0.00
        if expenses_exercised:
            expenses_per = 100.00
        if expenses_assign > 0:
            expenses_per = (expenses_exercised/expenses_assign)*100

        lines.append({
            'id': 'Total EXPENSES',
            'name': _('TOTAL EXPENSES, INVESTMENTS AND OTHER EXPENSES'),
            'columns': [
                    self._format({'name': expenses_authorized},figure_type='float'),
                    self._format({'name': expenses_transfers},figure_type='float'),
                    self._format({'name': expenses_assign},figure_type='float'),
                    {'name': ''},
                    self._format({'name': expenses_contable_exercised},figure_type='float'),
                    self._format({'name': expenses_e_income},figure_type='float'),
                    self._format({'name': expenses_extra_book},figure_type='float'),
                    self._format({'name': expenses_exercised},figure_type='float'),
                    {'name': expenses_per,'class':'number'},
                    self._format({'name': expenses_to_exercised},figure_type='float'),
                    ],
        
            'level': 1,
            'unfoldable': False,
            'unfolded': True,
            'class':'text-right'
        })

        year_per = 0.00
        if year_exercised:
            year_per = 100.00
        if year_assign > 0:
            year_per = (year_exercised/year_assign)*100

        lines.append({
            'id': 'Total Year',
            'name': _('REMAINING OF THE YEAR'),
            'columns': [
                    self._format({'name': year_authorized},figure_type='float'),
                    self._format({'name': year_transfers},figure_type='float'),
                    self._format({'name': year_assign},figure_type='float'),
                    {'name': ''},
                    self._format({'name': year_contable_exercised},figure_type='float'),
                    self._format({'name': year_e_income},figure_type='float'),
                    self._format({'name': year_extra_book},figure_type='float'),
                    self._format({'name': year_exercised},figure_type='float'),
                    {'name': year_per,'class':'number'},
                    self._format({'name': year_to_exercised},figure_type='float'),
                    ],
        
            'level': 1,
            'unfoldable': False,
            'unfolded': True,
            'class':'text-right'
        })
                
        return lines