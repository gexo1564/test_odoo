# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields, api, _,tools
from xlrd import *
import xlwt
import base64
from io import BytesIO
import math
from psycopg2 import sql

CONTROL_AMOUNTS_RECEIVED_LINE = 'control.amounts.received.line'

class ReportCalendarAmountAssign(models.Model):
    
    _name = 'report.calendar.amount.assign.line'
    _description = 'Report Calendar Amount Assign Line'
    _auto = False
    _order = 'item_first,item_second'
    
    item_first = fields.Char('Grouping by Object of Expenditure First')
    item_second = fields.Char('Grouping by Object of Expenditure') 
    line_id = fields.Many2one('calendar.assigned.amounts.lines',"Lines")

    year = fields.Char('Year')
    #item_id = fields.Many2one('departure.conversion', 'Item')
    new_item_id = fields.Many2one('federal.departure.finance','Federal departure')
    january = fields.Float(string='January')
    modifications_january = fields.Float(string='Modifications January')
    amount_deposite_january = fields.Float(string='Amount deposited January')
    pending_january = fields.Float(string='Receivable January')
    february = fields.Float(string='February')
    modifications_february = fields.Float(string='Modifications February')
    amount_deposite_february = fields.Float(string='Amount deposited February')
    pending_february = fields.Float(string='Receivable February')
    march = fields.Float(string='March')
    modifications_march = fields.Float(string='Modifications March')
    amount_deposite_march = fields.Float(string='Amount deposited March')
    pending_march = fields.Float(string='Receivable March')
    april = fields.Float(string='April')
    modifications_april = fields.Float(string='Modifications April')
    amount_deposite_april = fields.Float(string='Amount deposited April')
    pending_april = fields.Float(string='Receivable April')
    may = fields.Float(string='May')
    modifications_may = fields.Float(string='Modifications May')
    amount_deposite_may = fields.Float(string='Amount deposited May')
    pending_may = fields.Float(string='Receivable May')
    june = fields.Float(string='June')
    modifications_june = fields.Float(string='Modifications June')
    amount_deposite_june = fields.Float(string='Amount deposited June')
    pending_june = fields.Float(string='Receivable June')
    july = fields.Float(string='July')
    modifications_july = fields.Float(string='Modifications July')
    amount_deposite_july = fields.Float(string='Amount deposited July')
    pending_july = fields.Float(string='Receivable July')
    august = fields.Float(string='August')
    modifications_august = fields.Float(string='Modifications August')
    amount_deposite_august = fields.Float(string='Amount deposited August')
    pending_august = fields.Float(string='Receivable August')
    september = fields.Float(string='September')
    modifications_september = fields.Float(string='Modifications September')
    amount_deposite_september = fields.Float(string='Amount deposited September')
    pending_september = fields.Float(string='Receivable September')
    october = fields.Float(string='October')
    modifications_october = fields.Float(string='Modifications October')
    amount_deposite_october = fields.Float(string='Amount deposited October')
    pending_october = fields.Float(string='Receivable October')
    november = fields.Float(string='November')
    modifications_november = fields.Float(string='Modifications November')
    amount_deposite_november = fields.Float(string='Amount deposited November')
    pending_november = fields.Float(string='Receivable November')
    december = fields.Float(string='December')
    modifications_december = fields.Float(string='Modifications December')
    amount_deposite_december = fields.Float(string='Amount deposited December')
    pending_december = fields.Float(string='Receivable December')
    
    annual_amount = fields.Float(string='Annual Amount')
    annual_amount_modification = fields.Float(string='Annual Amount Modification')

    clc_january = fields.Char('CLC January',compute="get_clc_folio",compute_sudo=True)
    clc_february = fields.Char('CLC February',compute="get_clc_folio",compute_sudo=True)
    clc_march = fields.Char('CLC March',compute="get_clc_folio",compute_sudo=True)
    clc_april = fields.Char('CLC April',compute="get_clc_folio",compute_sudo=True)
    clc_may = fields.Char('CLC May',compute="get_clc_folio",compute_sudo=True)
    clc_june = fields.Char('CLC June',compute="get_clc_folio",compute_sudo=True)
    clc_july = fields.Char('CLC July',compute="get_clc_folio",compute_sudo=True)
    clc_august = fields.Char('CLC August',compute="get_clc_folio",compute_sudo=True)
    clc_september = fields.Char('CLC September',compute="get_clc_folio",compute_sudo=True)
    clc_october = fields.Char('CLC October',compute="get_clc_folio",compute_sudo=True)
    clc_november = fields.Char('CLC November',compute="get_clc_folio",compute_sudo=True)
    clc_december = fields.Char('CLC December',compute="get_clc_folio",compute_sudo=True)
 
    def get_clc_folio(self):
        for line in self:
            line.clc_january = ''
            line.clc_february = ''
            line.clc_march = ''
            line.clc_april = ''
            line.clc_may = ''
            line.clc_june = ''
            line.clc_july = ''
            line.clc_august = ''
            line.clc_september = ''
            line.clc_october = ''
            line.clc_november = ''
            line.clc_december = ''
            
            assing_lines = self.env['calendar.assigned.amounts.lines'].search([('new_item_id','=',line.new_item_id.id),('year', '=', line.year)])
            if assing_lines:
                recived_lines = self.env[CONTROL_AMOUNTS_RECEIVED_LINE].search([('calendar_assigned_amount_line_id','in',assing_lines.ids),('month_no','=',1),('year', '=', line.year)])
                if recived_lines:
                    line.clc_january = str(recived_lines.mapped('folio_clc')).strip('[]')
                recived_lines = self.env[CONTROL_AMOUNTS_RECEIVED_LINE].search([('calendar_assigned_amount_line_id','in',assing_lines.ids),('month_no','=',2),('year', '=', line.year)])
                if recived_lines:
                    line.clc_february = str(recived_lines.mapped('folio_clc')).strip('[]')
                recived_lines = self.env[CONTROL_AMOUNTS_RECEIVED_LINE].search([('calendar_assigned_amount_line_id','in',assing_lines.ids),('month_no','=',3),('year', '=', line.year)])
                if recived_lines:
                    line.clc_march = str(recived_lines.mapped('folio_clc')).strip('[]')
                recived_lines = self.env[CONTROL_AMOUNTS_RECEIVED_LINE].search([('calendar_assigned_amount_line_id','in',assing_lines.ids),('month_no','=',4),('year', '=', line.year)])
                if recived_lines:
                    line.clc_april = str(recived_lines.mapped('folio_clc')).strip('[]')
                recived_lines = self.env[CONTROL_AMOUNTS_RECEIVED_LINE].search([('calendar_assigned_amount_line_id','in',assing_lines.ids),('month_no','=',5),('year', '=', line.year)])
                if recived_lines:
                    line.clc_may = str(recived_lines.mapped('folio_clc')).strip('[]')
                recived_lines = self.env[CONTROL_AMOUNTS_RECEIVED_LINE].search([('calendar_assigned_amount_line_id','in',assing_lines.ids),('month_no','=',6),('year', '=', line.year)])
                if recived_lines:
                    line.clc_june = str(recived_lines.mapped('folio_clc')).strip('[]')
                recived_lines = self.env[CONTROL_AMOUNTS_RECEIVED_LINE].search([('calendar_assigned_amount_line_id','in',assing_lines.ids),('month_no','=',7),('year', '=', line.year)])
                if recived_lines:
                    line.clc_july = str(recived_lines.mapped('folio_clc')).strip('[]')
                recived_lines = self.env[CONTROL_AMOUNTS_RECEIVED_LINE].search([('calendar_assigned_amount_line_id','in',assing_lines.ids),('month_no','=',8),('year', '=', line.year)])
                if recived_lines:
                    line.clc_august = str(recived_lines.mapped('folio_clc')).strip('[]')
                recived_lines = self.env[CONTROL_AMOUNTS_RECEIVED_LINE].search([('calendar_assigned_amount_line_id','in',assing_lines.ids),('month_no','=',9),('year', '=', line.year)])
                if recived_lines:
                    line.clc_september = str(recived_lines.mapped('folio_clc')).strip('[]')
                recived_lines = self.env[CONTROL_AMOUNTS_RECEIVED_LINE].search([('calendar_assigned_amount_line_id','in',assing_lines.ids),('month_no','=',10),('year', '=', line.year)])
                if recived_lines:
                    line.clc_october = str(recived_lines.mapped('folio_clc')).strip('[]')
                recived_lines = self.env[CONTROL_AMOUNTS_RECEIVED_LINE].search([('calendar_assigned_amount_line_id','in',assing_lines.ids),('month_no','=',11),('year', '=', line.year)])
                if recived_lines:
                    line.clc_november = str(recived_lines.mapped('folio_clc')).strip('[]')
                recived_lines = self.env[CONTROL_AMOUNTS_RECEIVED_LINE].search([('calendar_assigned_amount_line_id','in',assing_lines.ids),('month_no','=',12),('year', '=', line.year)])
                if recived_lines:
                    line.clc_december = str(recived_lines.mapped('folio_clc')).strip('[]')

    def init(self):
        tools.drop_view_if_exists(self.env.cr,self._table)
        query = sql.SQL('''
            CREATE OR REPLACE VIEW {} AS (
            select max(id) as id, year, new_item_id, item_id_first as item_first,item_id_second as item_second,max(id) as line_id,
                sum(annual_amount) as annual_amount,sum(modifications_annual_amount) as annual_amount_modification,
                sum(january) as january,
                (COALESCE(sum(january),0)+COALESCE(sum(amount_modifications_january),0)) as modifications_january,
                sum(amount_deposite_january) as amount_deposite_january,sum(collect_january) as pending_january,
                sum(february) as february,
                (COALESCE(sum(february),0)+COALESCE(sum(amount_modifications_february),0)) as modifications_february,
                sum(amount_deposite_february) as amount_deposite_february,sum(collect_february) as pending_february,
                sum(march) as march,
                (COALESCE(sum(march),0)+COALESCE(sum(amount_modifications_march),0)) as modifications_march,
                sum(amount_deposite_march) as amount_deposite_march,sum(collect_march) as pending_march,
                sum(april) as april,
                (COALESCE(sum(april),0)+COALESCE(sum(amount_modifications_april),0)) as modifications_april,
                sum(amount_deposite_april) as amount_deposite_april,sum(collect_april) as pending_april,
                sum(may) as may,
                (COALESCE(sum(may),0)+COALESCE(sum(amount_modifications_may),0)) as modifications_may,
                sum(amount_deposite_may) as amount_deposite_may,sum(collect_may) as pending_may,
                sum(june) as june,
                (COALESCE(sum(june),0)+COALESCE(sum(amount_modifications_june),0)) as modifications_june,
                sum(amount_deposite_june) as amount_deposite_june,sum(collect_june) as pending_june,
                sum(july) as july,
                (COALESCE(sum(july),0)+COALESCE(sum(amount_modifications_july),0)) as modifications_july,
                sum(amount_deposite_july) as amount_deposite_july,sum(collect_july) as pending_july,
                sum(august) as august,
                (COALESCE(sum(august),0)+COALESCE(sum(amount_modifications_august),0)) as modifications_august,
                sum(amount_deposite_august) as amount_deposite_august,sum(collect_august) as pending_august,
                sum(september) as september,
                (COALESCE(sum(september),0)+COALESCE(sum(amount_modifications_september),0)) as modifications_september,
                sum(amount_deposite_september) as amount_deposite_september,sum(collect_september) as pending_september,
                sum(october) as october,
                (COALESCE(sum(october),0)+COALESCE(sum(amount_modifications_october),0)) as modifications_october,
                sum(amount_deposite_october) as amount_deposite_october,sum(collect_october) as pending_october,
                sum(november) as november,
                (COALESCE(sum(november),0)+COALESCE(sum(amount_modifications_november),0)) as modifications_november,
                sum(amount_deposite_november) as amount_deposite_november,sum(collect_november) as pending_november,
                sum(december) as december,
                (COALESCE(sum(december),0)+COALESCE(sum(amount_modifications_december),0)) as modifications_december,
                sum(amount_deposite_december) as amount_deposite_december,sum(collect_december) as pending_december 
            from calendar_assigned_amounts_lines
            group by year, new_item_id, item_id_first,item_id_second
            )''').format(
                sql.Identifier(self._table)
            )
        self.env.cr.execute(query)

    def action_get_excel_report(self):
        
        wb1 = xlwt.Workbook(encoding='utf-8')
        if self.env.user.lang == 'es_MX':
            ws1 = wb1.add_sheet('Reporte Detallado de Presupuesto')
        else:         
            ws1 = wb1.add_sheet('Details Budget Report')
        fp = BytesIO()
        header_style = xlwt.easyxf('font: bold 1')
        float_sytle = xlwt.easyxf(num_format_str = '$#,##0.00')
        row = 0
        col = 0
        col_width = 256 * 25
        ws1.col(col).width = col_width
        
        if self.env.user.lang == 'es_MX':
            ws1.write(row, col, 'Esperado Orginal', header_style)
            row+=1
            ws1.write(row, col, 'Total Recibido', header_style)
            row+=1
            ws1.write(row, col, 'Total Diferencia', header_style)
            row+=1
        else:
            ws1.write(row, col, 'Expected Original', header_style)
            row+=1
            ws1.write(row, col, 'Total Received', header_style)
            row+=1
            ws1.write(row, col, 'Total Difference', header_style)
            row+=1
            
        col+=1
        row = 0
        ws1.col(col).width = col_width
        if self.env.context and self.env.context.get('active_ids'):
            line_records = self.env['report.calendar.amount.assign.line'].browse(self.env.context.get('active_ids'))
            total_annual = sum(x.annual_amount for x in line_records) 
            total_deposite = sum(x.amount_deposite_january+x.amount_deposite_february
                                 + x.amount_deposite_march +x.amount_deposite_april+x.amount_deposite_may
                                 + x.amount_deposite_june + x.amount_deposite_july +x.amount_deposite_august
                                 + x.amount_deposite_september + x.amount_deposite_october 
                                 + x.amount_deposite_november + x.amount_deposite_december for x in line_records)
            
            diff = total_annual - total_deposite
             
            ws1.write(row, col, total_annual, float_sytle)
            row+=1
            ws1.write(row, col, total_deposite, float_sytle)
            row+=1
            ws1.write(row, col, diff, float_sytle)
            row+=1
            
        wb1.save(fp)
        out = base64.encodestring(fp.getvalue())
        report_file = out
        if self.env.user.lang == 'es_MX':
            name = 'exportar_resumen.xls'
        else:
            name = 'export_summary.xls'
            
        rec = self.env['excel.export.assign.report'].create({'fec_data':report_file,'filename':name})
        return {
            'name': _('Export Summary'),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': False,
            'res_model': 'excel.export.assign.report',
            'domain': [],
            'type': 'ir.actions.act_window',
            'target': 'new',
            'res_id': rec.id,
        }    
    
    
    
    
    