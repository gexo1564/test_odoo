# -*- coding: utf-8 -*-
import base64
from datetime import datetime
import logging
from odoo import api, models, fields, _

from odoo.addons.siif_payroll_payment.tools.layouts import Context
from odoo.addons.siif_payroll_payment.tools.layouts.bancos import LayoutBanorte, LayoutBBVASIT, LayoutBBVA, LayoutHSBC, LayoutSantanderH2H, LayoutSantander

LOAD_BANK_LAYOUT_SUPPLIER_PAYMENT = 'load.bank.layout.supplier.payment'

class LoadBankLayoutSupplierPayment(models.TransientModel):

    _inherit = LOAD_BANK_LAYOUT_SUPPLIER_PAYMENT

    payment_request_id = fields.Many2one('account.move')

    @api.onchange('file_data')
    def onchange_field(self):
        self.failed_file_data = None
        self.is_hide_failed = True

    def load_payroll_payment_bank_layout(self):
        errors = self.env['account.move'].payroll_payments_with_electronic_transfers_from_layouts(
            self.payment_request_id,
            self.journal_id,
            base64.b64decode(self.file_data)
        )
        if errors:
            content = "................... Registros erróneos " + str(datetime.today()) + "...................\r\n"
            content += "\r\n".join([e.error_message for e in errors])
            self.failed_file_data = base64.b64encode(content.encode('utf-8'))
            self.is_hide_failed = False
            return {
                'name': _('Load Bank Layout'),
                'type': 'ir.actions.act_window',
                'res_model': LOAD_BANK_LAYOUT_SUPPLIER_PAYMENT,
                'view_mode': 'form',
                'view_id': self.env.ref('jt_supplier_payment.view_load_bank_layout_payroll_payment_form').id,
                'views': [(False, 'form')],
                'context': {},
                'target': 'new',
                'res_id': self.id,
            }

    def validate_payroll_payment_bank_layout(self):
        # Identificación de la nómina
        payroll_processing_id = self.payment_request_id.payroll_processing_id.id
        # Identificación de la cuenta bancaria
        payroll_bank_format = self.journal_id.payroll_load_bank_format
        # Identificación del formato del layout
        layout_type = None
        if payroll_bank_format == "banorte":
            layout_type = LayoutBanorte()
        elif payroll_bank_format == "bbva_232":
            layout_type = LayoutBBVASIT()
        elif payroll_bank_format == "bbva_nomina":
            layout_type = LayoutBBVA()
        elif payroll_bank_format == "hsbc":
            layout_type = LayoutHSBC()
        elif payroll_bank_format == "santander_h2h":
            layout_type = LayoutSantanderH2H()
        elif payroll_bank_format == "santander":
            layout_type = LayoutSantander()
        else:
            return
        # Lectura y obtención de los ids de los recibos de nómina
        
        __, lines_err = Context(self.env, layout_type).read_layout_from_payroll_file(payroll_processing_id, base64.b64decode(self.file_data))
        if lines_err:
            content = "................... Registros erróneos " + str(datetime.today()) + "...................\r\n"
            content += "\r\n".join([e.error_message for e in lines_err])
            self.failed_file_data = base64.b64encode(content.encode('utf-8'))
            self.is_hide_failed = False
            return {
                'name': _('Load Bank Layout'),
                'type': 'ir.actions.act_window',
                'res_model': LOAD_BANK_LAYOUT_SUPPLIER_PAYMENT,
                'view_mode': 'form',
                'view_id': self.env.ref('jt_supplier_payment.view_load_bank_layout_payroll_payment_form').id,
                'views': [(False, 'form')],
                'context': {},
                'target': 'new',
                'res_id': self.id,
            }