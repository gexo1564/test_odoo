# 1 : imports of python lib

# 2 : imports of odoo
import logging
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
# 3 : imports from odoo addons


class DepartureGroup(models.Model):
    _name = 'departure.group'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Departure group'
    _rec_name = 'group'


    group = fields.Integer(string = 'Group', size = 3)
    description_group = fields.Char('Description group', track_visibility = "onchange")

    _sql_constraints = [('unique_group', 'UNIQUE(group)', 'Group must be unique'),]



    @api.constrains('group')
    def _check_group_numeric_length(self):
        group = self.group
        if not str(group).isnumeric():
            raise ValidationError(_('Group must be numeric value'))
        if len(str(group)) != 3:
            raise ValidationError(_('Group must be 3 digits'))


    @api.model
    def create(self, vals):
        if 'group' in vals and self.search([('group', '=', vals['group'])], limit=1):
            raise ValidationError(_('Group already exists'))
    
        result = super(DepartureGroup, self).create(vals)
        return result    