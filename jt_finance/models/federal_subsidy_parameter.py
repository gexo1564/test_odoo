# -*- coding: utf-8 -*-

import logging
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

class FederalSubsidyParameter(models.Model):
    _name = "federal.subsidy.parameter"
    _description = "Configuration of bank accounts of federal subsidies"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char(compute = "_compute_name", store = True, readonly = False, string = "Nombre")
    description = fields.Char(string = "Descripción", track_visibility="onchange")
    code = fields.Char(string = "Código", required = True)
    res_partner_bank_id = fields.Many2one("res.partner.bank", string = "Cuenta", required = True, track_visibility="onchange")
    bank_id = fields.Many2one("res.bank", related = "res_partner_bank_id.bank_id", string = "Banco")
    clabe = fields.Char(related = "res_partner_bank_id.l10n_mx_edi_clabe")
    partner_id = fields.Many2one("res.partner", related = "res_partner_bank_id.partner_id", string = "Titular")
    minimum_balance = fields.Float(related = "res_partner_bank_id.minimum_balance", string = "Saldo mínimo")
    state = fields.Selection(selection = [("draft", "Borrador"), ("validated", "Validado")], string = "Estado", default = "draft")

    
    @api.constrains('name')
    def _check_duplicate_name(self):
        if self.env['federal.subsidy.parameter'].search_count([('name', '=', 'SUBFED/P001')]) > 1:
            raise ValidationError(_('The code of the bank account that is used for the Control of Amounts Received from the Federal Subsidy already exists. Edit it if necessary.'))
        

    def unlink(self):
        for parameter in self:
            if parameter.state in ('validated'):
                if self.env.user.lang == 'es_MX':
                    raise ValidationError("No puedes eliminar el parámetro si ya ha sido Validado.")
                else:
                    raise ValidationError("You cannot delete this configuration if it has been Validated!")
        return super(FederalSubsidyParameter, self).unlink()

    def validate(self):
        self.ensure_one()
        self._check_existing_name(self.name)
        self.state = "validated"
    
    @api.model
    def create(self, vals):
        res = super(FederalSubsidyParameter, self).create(vals)

        res.name = res.code + "/"
        return res
    
    def write(self,vals):
        if 'name' in vals and self.name:
            self._check_existing_name(vals['name'])

        res = super(FederalSubsidyParameter,self).write(vals)
        return res
            
    
    @api.depends("code")
    def _compute_name(self):
        for record in self:
            if record.name:
                record.name = self.code + "/"


    def _check_existing_name(self, name):
        existing_record = self.env['federal.subsidy.parameter'].search([('name', '=', name)], limit=1)
        if existing_record:
            raise ValidationError(_('This name already exists for %s') % existing_record.description)            
