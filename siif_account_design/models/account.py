# -*- coding: utf-8 -*-

import time
import math
import re
import logging

from odoo.osv import expression
from odoo.tools.float_utils import float_round as round, float_compare
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, remove_accents
from odoo.exceptions import UserError, ValidationError
from odoo import api, fields, models, _, tools
from odoo.tests.common import Form

class AccountAccount(models.Model):
    _inherit = "account.account"

    dep_subdep_flag = fields.Boolean("Dependancy/Subdependancy")
    is_payment_account = fields.Boolean("Use to payment account")
    is_income_account = fields.Boolean("Use to income account")
    revenue_recognition_account_id = fields.Many2one("account.account",'Revenue recognition account')
    description_revenue_recognition = fields.Char(related="revenue_recognition_account_id.name", string="Name revenue recognition")
    coa_conac_id = fields.Many2one('coa.conac', string="CODE CONAC")
    cog_conac_id = fields.Many2one('cog.conac', string="COG CONAC")
    budget_registry =  fields.Boolean('Budget registry')

    def write(self, vals):
        """
        Se sobreescribe el método write para mantener actualizados los ids
        de cog_conac y coa conac en el cátalogo de partidas de gasto en el módulo
        de presupuesto.
        """
        res = super().write(vals)
        update = []
        if 'cog_conac_id' in vals:
            update.append(('heading', vals['cog_conac_id']))
        if 'coa_conac_id' in vals:
            update.append(('cog_id', vals['coa_conac_id']))
        if update:
            columns = ",".join([f"{c} = %s" for c, p in update])
            params = [p for c, p in update]
            query = f"update expenditure_item set {columns} where unam_account_id = %s"
            self.env.cr.execute(query, tuple(params) + (self.id,))
        return res

    # Validate that the account code is in the correct format.
    @api.constrains('code')
    def _check_code(self):
        pattern = r'^\d{3}\.\d{3}\.\d{3}\.\d{3}$'
        for account in self:
            if "(copia)" in account.code:
                account.code = "000.000.000.000"
            elif not re.match(pattern, account.code):
                raise ValidationError(_('The code must be in the format xxx.xxx.xxx.xxx, where x is a digit between 0 and 9!'))
            
