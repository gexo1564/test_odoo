# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields, api, _
import base64
import xlrd
import openpyxl
import sqlite3
import tempfile
import io
from datetime import datetime
from odoo.modules.module import get_resource_path
from xlrd import open_workbook
from odoo.exceptions import UserError, ValidationError
from odoo.tools.misc import ustr
import copy
import json


REBATE_PROGRAM_CODE_PATTERN = '________0000099900000000000000000000000000000000000000000000'

class AdjustedPayrollWizard(models.TransientModel):

    _name = 'adjusted.payroll.wizard'
    _description = "Adjusted Payroll Wizard"

    type_of_movement = fields.Selection([('adjustment','Adjustment'),
                                         ('perception_adjustment_detail','Perception Adjustment Detail'),
                                         ('deduction_adjustment_detail','Deduction Adjustment Detail'),
                                         ('detail_alimony_adjustments','Detail of Alimony Adjustments'),
                                         ],string='Adjustment Type')
    
    file = fields.Binary('File to import')
    filename = fields.Char('FileName')
    
    employee_ids = fields.Many2many('hr.employee','employee_adjusted_payroll_wizard_rel','employee_id','wizard_id','Employees')
    payroll_process_id = fields.Many2one('custom.payroll.processing','Payroll Process')
    
    def update_employee_payroll_pension(self,rec,payment_method_id,deposit_number,check_number,bank_account_id,bank_key,bank_id):

        log = False
        bank_records = self.env['res.bank'].search_read([], fields=['id', 'l10n_mx_edi_code'])
        if bank_key and check_number:
            if type(bank_key) is int or type(bank_key) is float:
                bank_key = int(bank_key)

            log,from_check= self.env['custom.payroll.processing'].get_perception_check_log(check_number,bank_key)
        
        exit_vals = {'l10n_mx_edi_payment_method_id' : payment_method_id,
                     'deposite_number' : deposit_number,
                     'check_number' : check_number,
                     'bank_key' : bank_key,
                     'receiving_bank_acc_pay_id' : bank_account_id,
        
                    }
        if log:
            exit_vals.update({'check_folio_id':log.id})
        rec.write(exit_vals)
    
    def update_employee_payroll_perception(self,emp_payroll_ids,payment_method,bank_key,check_number,deposite_number,ben):

        bank_records = self.env['res.bank'].search_read([], fields=['id', 'l10n_mx_edi_code'])
        bank_account_records = self.env['res.partner.bank'].search_read([], fields=['id', 'acc_number'])
        payment_method_records = self.env['l10n_mx_edi.payment.method'].search_read([], fields=['id', 'name'])

        rec_check_number = check_number
        rec_deposite_number = deposite_number
        rec_bank_key = bank_key
        bank_account = ben
        bank_account_id = False
        log = False
        payment_method_id = False
            
        if payment_method and str(payment_method).isalnum():    
            if  type(payment_method) is int or type(payment_method) is float:
                payment_method = int(payment_method)
        else:
            payment_method = str(payment_method)
              
        payment_method_id = list(filter(lambda pm: pm['name'] == str(payment_method), payment_method_records))
        payment_method_id = payment_method_id[0]['id'] if payment_method_id else False
        
        if check_number:
            if  type(check_number) is int or type(check_number) is float:
                rec_check_number = int(check_number)

        if deposite_number:
            if  type(deposite_number) is int or type(deposite_number) is float:
                rec_deposite_number = int(deposite_number)

        if bank_key and rec_check_number:
            if type(bank_key) is int or type(bank_key) is float:
                rec_bank_key = int(bank_key)

            bank_id_1 = list(filter(lambda b: b['l10n_mx_edi_code'] == rec_bank_key, bank_records))
            bank_id_1 = bank_id_1[0]['id'] if bank_id_1 else False

            log,from_check= self.env['custom.payroll.processing'].get_perception_check_log(rec_check_number,rec_bank_key)
            
        if bank_account and str(bank_account).isalnum():
            if  type(bank_account) is int or type(bank_account) is float:
                bank_account = int(bank_account)
        elif bank_account and str(bank_account).isnumeric():
            if  type(bank_account) is int or type(bank_account) is float:
                bank_account = int(bank_account)
        if  type(bank_account) is int or type(bank_account) is float:
            bank_account = int(bank_account)
                
        bank_account_id = list(filter(lambda b: b['acc_number'] == str(bank_account), bank_account_records))
        bank_account_id = bank_account_id[0]['id'] if bank_account_id else False
        
        exit_vals = {'l10n_mx_edi_payment_method_id' : payment_method_id,
                     'bank_key' : rec_bank_key,
                     'receiving_bank_acc_pay_id' : bank_account_id
                    }
        check_payment_method = self.env.ref('l10n_mx_edi.payment_method_cheque')
        if not payment_method_id:
            exit_vals.update({'deposite_number' : rec_deposite_number,'check_number' : rec_check_number})
            if log:
                exit_vals.update({'check_folio_id':log.id})
            
        elif check_payment_method and payment_method_id and check_payment_method.id!=payment_method_id:
            exit_vals.update({'deposite_number' : rec_deposite_number,'check_number' : rec_check_number,})
            if log:
                exit_vals.update({'check_folio_id':log.id})
            
        emp_payroll_ids.write(exit_vals)
        
    def generate(self):
        if self.file:
            data = base64.decodestring(self.file)
            book = open_workbook(file_contents=data or b'')
            sheet = book.sheet_by_index(0)

            current_query = f'SELECT COUNT(fpe.id) as total FROM employee_payroll_file fpe WHERE fpe.payroll_processing_id = {self.payroll_process_id.id}'
            self.env.cr.execute(current_query)
            postgres_employee_payroll_file_total = self.env.cr.fetchall()
            postgres_employee_payroll_file_total = int(postgres_employee_payroll_file_total[0][0])

            current_query = f'SELECT COUNT(*) as total FROM deduction_line ld INNER JOIN employee_payroll_file fpe ON ld.payroll_id = fpe.id WHERE fpe.payroll_processing_id = {self.payroll_process_id.id}'
            self.env.cr.execute(current_query)
            postgres_employee_deduction_line_total = self.env.cr.fetchall()
            postgres_employee_deduction_line_total = int(postgres_employee_deduction_line_total[0][0])

            current_query = f'SELECT COUNT(fpe.id) as total FROM employee_payroll_file fpe WHERE fpe.payroll_processing_id = {self.payroll_process_id.id} AND fpe.payroll_payment_request_with_adjustment = true'
            self.env.cr.execute(current_query)
            postgres_employee_payroll_file_with_adjustment_total = self.env.cr.fetchall()
            postgres_employee_payroll_file_with_adjustment_total = int(postgres_employee_payroll_file_with_adjustment_total[0][0])

            current_query = f"""
                    SELECT COUNT(*) FROM preception_line lp
                    INNER JOIN employee_payroll_file fpe ON lp.payroll_id = fpe.id
                    INNER JOIN custom_payroll_processing ppc ON fpe.payroll_processing_id  = ppc.id
                    WHERE is_adjustment_update = true AND ppc.id = {self.payroll_process_id.id};
                """
            self.env.cr.execute(current_query)
            postgres_employee_payroll_file_perception_adjustment_total = self.env.cr.fetchall()
            postgres_employee_payroll_file_perception_adjustment_total = int(postgres_employee_payroll_file_perception_adjustment_total[0][0])

            if self.type_of_movement == 'adjustment':

                if postgres_employee_payroll_file_total == 0 or postgres_employee_deduction_line_total == 0:
                    return {
                        'name': 'Importación',
                        'type': 'ir.actions.act_window',
                        'view_mode': 'form',
                        'res_model': 'payroll.payment.message.wizard',
                        'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Error en importación de ajustes de nómina, debe importar primero percepciones y deducciones.'}).id,
                        'target': 'new'
                    }

                elif postgres_employee_payroll_file_with_adjustment_total != 0:
                    return {
                        'name': 'Importación',
                        'type': 'ir.actions.act_window',
                        'view_mode': 'form',
                        'res_model': 'payroll.payment.message.wizard',
                        'res_id': self.env['payroll.payment.message.wizard'].create({'message': f'Error en importación de ajustes de nómina, la nómina actual[{self.payroll_process_id.name}] ya tiene ajustes cargados.'}).id,
                        'target': 'new'
                    }
                else:
                    adjustment_header = ['cause', 'check_number', 'bank_key', 'new_check_number', 'new_bank_key', 'employee_number', 'fortnight_year', 'new_fortnight_year', 'rfc_employee', 'original_net', 'new_net', ]
                    adjustment_without_check_and_bank = ['B', 'U']
                    file_path_db = tempfile.mkdtemp()
                    fortnight = self.payroll_process_id.fornight
                    year = self.payroll_process_id.fornight
                    adjustment_table_name = f'adjustment_{fortnight}{year}'

                    db_payroll_path = f"{file_path_db}/payroll_{fortnight}{year}.db"
                    con = sqlite3.connect(db_payroll_path)
                    cur = con.cursor()


                    current_query = f"DROP TABLE IF EXISTS {adjustment_table_name};"
                    cur.execute(current_query)
                    adjustment_header_table_creation = [element + ' TEXT' for element in adjustment_header]
                    adjustment_header_table_creation = ','.join(adjustment_header_table_creation)
                    current_query = f'CREATE TABLE {adjustment_table_name}(id INTEGER PRIMARY KEY,{adjustment_header_table_creation});'
                    cur.execute(current_query)


                    # To open the workbook
                    # Workbook object is created
                    wb_obj = openpyxl.load_workbook(filename=io.BytesIO(base64.b64decode(self.file)), read_only=True, data_only=True, keep_links=False)

                    # Get workbook active sheet object
                    # from the active attribute
                    sheet_obj = wb_obj.active

                    max_row = sheet_obj.max_row
                    max_column = sheet_obj.max_column

                    if max_column == len(adjustment_header):
                        counter = 1
                        for i in sheet_obj.iter_rows(values_only=True, min_row=2):
                            cause = str(i[0]).strip() if i[0] is not None else ''
                            check_number = str(i[1]).strip() if i[1] is not None else ''
                            bank_key = str(i[2]).strip() if i[2] is not None else ''
                            new_check_number = str(i[3]).strip() if i[3] is not None else ''
                            new_bank_key = str(i[4]).strip() if i[4] is not None else ''
                            employee_number = str(i[5]).strip() if i[5] is not None else ''
                            fortnight_year = str(i[6]).strip() if i[6] is not None else ''
                            new_fortnight_year = str(i[7]).strip() if i[7] is not None else ''
                            rfc_employee = str(i[8]).strip() if i[8] is not None else ''
                            original_net = str(i[9]).strip() if i[9] is not None else ''
                            new_net = str(i[10]).strip() if i[10] is not None else ''
                            current_query = f"INSERT INTO {adjustment_table_name} VALUES({counter}, \'{cause}\', \'{check_number}\', \'{bank_key}\', \'{new_check_number}\', \'{new_bank_key}\', \'{employee_number}\', \'{fortnight_year}\', \'{new_fortnight_year}\', \'{rfc_employee}\', printf('%.2f', {original_net}), printf('%.2f', {new_net}));"
                            cur.execute(current_query)
                            counter = counter + 1
                            
                    
                    
                        con.commit()

                        data_not_found_dict = {
                            'cause' : [],
                            'employee' : [],
                            'employee_rfc' : [],
                            'original_data_no_coincide_a_d_r_e_p' : [],
                            'employee_without_additional_payment' : [],
                            'bank' : [],
                            'check' : {},
                            'new_bank' : [],
                            'new_check' : {},
                            'general' : [],
                            'already_reissued_original_bank_check' : {},
                            'already_reissued_new_bank_check' : {},
                        }


                        # Postgres employees dict
                        postgres_employee_rfc_list = self.env['hr.employee'].search_read([], fields=['rfc', 'id'])
                        postgres_employee_rfc_dict = {}
                        for postgres_employee_rfc_list_element in postgres_employee_rfc_list:
                            postgres_employee_rfc_dict[postgres_employee_rfc_list_element['rfc']] = {'id' : postgres_employee_rfc_list_element['id']}

                        # Postgres banks, bank dict
                        current_query = f'SELECT l10n_mx_edi_code, name, id FROM res_bank WHERE l10n_mx_edi_code IS NOT NULL AND LENGTH(TRIM(l10n_mx_edi_code)) > 1;'
                        self.env.cr.execute(current_query)
                        postgres_employee_bank_list = self.env.cr.fetchall()
                        postgres_employee_bank_dict = {}
                        for postgres_employee_bank_element in postgres_employee_bank_list:
                            postgres_employee_bank_dict[postgres_employee_bank_element[0]] = {'name' : postgres_employee_bank_element[1], 'id' : postgres_employee_bank_element[2]}

                        # Postgres payment methods, payment method dict
                        current_query = f'SELECT name, code, id FROM l10n_mx_edi_payment_method;'
                        self.env.cr.execute(current_query)
                        postgres_employee_payment_method_list = self.env.cr.fetchall()
                        postgres_employee_payment_method_dict = {}
                        for postgres_employee_payment_method_list_element in postgres_employee_payment_method_list:
                            postgres_employee_payment_method_dict[postgres_employee_payment_method_list_element[0]] = {'code' : postgres_employee_payment_method_list_element[1], 'id' : postgres_employee_payment_method_list_element[2]}

                        # Causes list from Odoo postgres
                        postgres_payroll_payment_adjustment_case_list = self.env['adjustment.cases'].search_read([], fields=['case', 'id', 'is_adjustment_for_reissue'])
                        postgres_payroll_payment_adjustment_case = {}
                        for postgres_payroll_payment_adjustment_case_list_element in postgres_payroll_payment_adjustment_case_list:
                            postgres_payroll_payment_adjustment_case[postgres_payroll_payment_adjustment_case_list_element['case']] = {'id' : postgres_payroll_payment_adjustment_case_list_element['id'], 'is_adjustment_for_reissue' : postgres_payroll_payment_adjustment_case_list_element['is_adjustment_for_reissue']}

                        # Postgres employee perception dict
                        postgres_employee_rfc_perception_list = self.env['employee.payroll.file'].search([('payroll_processing_id', '=', self.payroll_process_id.id)])
                        postgres_employee_rfc_employee_payroll_file_dict = {}
                        for postgres_employee_rfc_perception_list_element in postgres_employee_rfc_perception_list:
                            postgres_employee_rfc_employee_payroll_file_dict[postgres_employee_rfc_perception_list_element.rfc] = postgres_employee_rfc_perception_list_element

                        # Dict from file, all data
                        current_query = f"SELECT {', '.join(adjustment_header)} FROM {adjustment_table_name};"
                        cur.execute(current_query)
                        lite_from_file_data_list = cur.fetchall()
                        lite_from_file_data_dict = {}
                        for lite_from_file_data_list_element in lite_from_file_data_list:
                            if lite_from_file_data_list_element[8] in lite_from_file_data_dict:
                                lite_from_file_data_dict[lite_from_file_data_list_element[8]].append((lite_from_file_data_list_element[0], lite_from_file_data_list_element[1], lite_from_file_data_list_element[2], lite_from_file_data_list_element[3], lite_from_file_data_list_element[4], lite_from_file_data_list_element[5], lite_from_file_data_list_element[6], lite_from_file_data_list_element[7], lite_from_file_data_list_element[9], lite_from_file_data_list_element[10]))
                            else:
                                lite_from_file_data_dict[lite_from_file_data_list_element[8]] = [(lite_from_file_data_list_element[0], lite_from_file_data_list_element[1], lite_from_file_data_list_element[2], lite_from_file_data_list_element[3], lite_from_file_data_list_element[4], lite_from_file_data_list_element[5], lite_from_file_data_list_element[6], lite_from_file_data_list_element[7], lite_from_file_data_list_element[9], lite_from_file_data_list_element[10])]


                        # causes list from file not found
                        current_query = f'SELECT DISTINCT(cause) FROM {adjustment_table_name};'
                        cur.execute(current_query)
                        lite_from_file_causes_list = cur.fetchall()
                        for lite_from_file_causes_list_element in lite_from_file_causes_list:
                            if lite_from_file_causes_list_element[0] not in postgres_payroll_payment_adjustment_case:
                                data_not_found_dict['cause'].append(lite_from_file_causes_list_element[0])

                        # employee rfc list not found in current payroll
                        for lite_from_file_data_dict_element in lite_from_file_data_dict:
                            for lite_from_file_data_dict_element_internal in lite_from_file_data_dict[lite_from_file_data_dict_element]:
                                if lite_from_file_data_dict_element not in postgres_employee_rfc_employee_payroll_file_dict and postgres_payroll_payment_adjustment_case[lite_from_file_data_dict_element_internal[0]]['is_adjustment_for_reissue'] == False:
                                    data_not_found_dict['employee_rfc'].append(lite_from_file_data_dict_element)
                                elif lite_from_file_data_dict_element not in postgres_employee_rfc_dict:
                                    data_not_found_dict['employee'].append(lite_from_file_data_dict_element)


                        for employee_not_found_element in data_not_found_dict['employee_rfc']:
                            lite_from_file_data_dict.pop(employee_not_found_element, None)

                        for employee_not_found_element in data_not_found_dict['employee']:
                            lite_from_file_data_dict.pop(employee_not_found_element, None)


                        current_query = f"SELECT bank_key, GROUP_CONCAT(check_number, ',') as check_number FROM {adjustment_table_name} WHERE cause NOT IN {tuple(adjustment_without_check_and_bank)} GROUP BY bank_key;"
                        cur.execute(current_query)
                        lite_bank_check_number_list = cur.fetchall()
                        lite_bank_key_check_number_dict = {}
                        for lite_bank_check_number_list_element in lite_bank_check_number_list:
                            if lite_bank_check_number_list_element[0] in postgres_employee_bank_dict:
                                lite_bank_key_check_number_dict[lite_bank_check_number_list_element[0]] = lite_bank_check_number_list_element[1].split(',')
                            else:
                                data_not_found_dict['bank'].append(lite_bank_check_number_list_element[0])

                        lite_original_bank_key = tuple(lite_bank_key_check_number_dict.keys())

                        # Postgres import.payroll.check.issue, original
                        current_query = """
                            SELECT br.l10n_mx_edi_code as bank_key, STRING_AGG(lc.folio::VARCHAR, ',') as folio 
                            FROM import_payroll_check_issue icpi
                            INNER JOIN res_bank br ON icpi.original_bank_id = br.id
                            INNER JOIN check_log lc ON icpi.original_check_id = lc.id
                            WHERE br.l10n_mx_edi_code IN %(lite_original_bank_key)s
                            GROUP BY br.l10n_mx_edi_code;
                        """

                        params = {'lite_original_bank_key': lite_original_bank_key}
                        self.env.cr.execute(current_query, params)
                        
                        postgres_import_payroll_check_issue_original_bank_check_list =  self.env.cr.fetchall()
                        postgres_import_payroll_check_issue_original_bank_check_dict = {}
                        for postgres_import_payroll_check_issue_original_bank_check_list_element in postgres_import_payroll_check_issue_original_bank_check_list:
                            postgres_import_payroll_check_issue_original_bank_check_dict[postgres_import_payroll_check_issue_original_bank_check_list_element[0]] = postgres_import_payroll_check_issue_original_bank_check_list_element[1].split(',')

                        for lite_bank_key_check_number_dict_element in lite_bank_key_check_number_dict:
                            if lite_bank_key_check_number_dict_element in postgres_import_payroll_check_issue_original_bank_check_dict:
                                data_not_found_dict['already_reissued_original_bank_check'][lite_bank_key_check_number_dict_element] = list(set(lite_bank_key_check_number_dict[lite_bank_key_check_number_dict_element]) & set(postgres_import_payroll_check_issue_original_bank_check_dict[lite_bank_key_check_number_dict_element]))

                        # Get check list from Odoo database
                        postgres_employee_bank_id_list = [postgres_employee_bank_dict[lite_employee_bank_key_dict_element]['id'] for lite_employee_bank_key_dict_element in lite_bank_key_check_number_dict]
                        current_query = """SELECT rb.l10n_mx_edi_code, STRING_AGG(concat(cl.folio, ',', cl.id), ';') 
                            FROM check_log cl INNER JOIN res_partner_bank rpb on cl.bank_account_id = rpb.id 
                            INNER JOIN res_bank rb on rpb.bank_id = rb.id where cl.status IN 
                            ('Printed') 
                            AND cl.general_status = 'assigned' AND rb.id IN %(postgres_employee_bank_id_list_element)s group by rb.l10n_mx_edi_code"""
                        
                        
                        params = {'postgres_employee_bank_id_list_element': tuple(postgres_employee_bank_id_list)}
                        self.env.cr.execute(current_query, params)

                        postgres_employee_check_log_list = self.env.cr.fetchall()
                        postgres_employee_check_log_dict = {}

                        for postgres_employee_check_log_list_element in postgres_employee_check_log_list:
                            postgres_employee_check_log_dict[postgres_employee_check_log_list_element[0]] = dict((key, value) for key, value in [element.split(',') for element in postgres_employee_check_log_list_element[1].split(';')])

                        for lite_bank_key_check_number_dict_element in lite_bank_key_check_number_dict:
                            if lite_bank_key_check_number_dict_element in postgres_employee_check_log_dict:
                                for lite_bank_key_check_number_dict_check in lite_bank_key_check_number_dict[lite_bank_key_check_number_dict_element]:
                                    if lite_bank_key_check_number_dict_check not in postgres_employee_check_log_dict[lite_bank_key_check_number_dict_element]:
                                        if lite_bank_key_check_number_dict_element in data_not_found_dict['check']:
                                            data_not_found_dict['check'][lite_bank_key_check_number_dict_element].append(lite_bank_key_check_number_dict_check)
                                        else:
                                            data_not_found_dict['check'].update({lite_bank_key_check_number_dict_element: [lite_bank_key_check_number_dict_check]})
                            else:
                                data_not_found_dict['check'].update({lite_bank_key_check_number_dict_element : lite_bank_key_check_number_dict[lite_bank_key_check_number_dict_element]})


                        current_query = f"SELECT new_bank_key, GROUP_CONCAT(new_check_number, ',') as new_check_number FROM {adjustment_table_name} GROUP BY new_bank_key;"
                        cur.execute(current_query)
                        lite_new_bank_check_number_list = cur.fetchall()
                        lite_new_bank_key_check_number_dict = {}
                        for lite_new_bank_check_number_list_element in lite_new_bank_check_number_list:
                            if lite_new_bank_check_number_list_element[0] in postgres_employee_bank_dict:
                                lite_new_bank_key_check_number_dict[lite_new_bank_check_number_list_element[0]] = lite_new_bank_check_number_list_element[1].split(',')
                            else:
                                data_not_found_dict['new_bank'].append(lite_new_bank_check_number_list_element[0])

                        
                        lite_new_bank_key = tuple(lite_new_bank_key_check_number_dict.keys())

                        # Postgres import.payroll.check.issue new
                        current_query = """
                            SELECT br.l10n_mx_edi_code as bank_key, STRING_AGG(lc.folio::VARCHAR, ',') as folio 
                            FROM import_payroll_check_issue icpi
                            INNER JOIN res_bank br ON icpi.new_bank_id = br.id
                            INNER JOIN check_log lc ON icpi.new_check_id = lc.id
                            WHERE br.l10n_mx_edi_code IN %(lite_original_bank_key)s
                            GROUP BY br.l10n_mx_edi_code;
                        """

                        params =  {'lite_new_bank_key': lite_new_bank_key, 'lite_original_bank_key': lite_original_bank_key,}
                        self.env.cr.execute(current_query, params)

                        postgres_import_payroll_check_issue_new_bank_check_list =  self.env.cr.fetchall()
                        postgres_import_payroll_check_issue_new_bank_check_dict = {}
                        for postgres_import_payroll_check_issue_new_bank_check_list_element in postgres_import_payroll_check_issue_new_bank_check_list:
                            postgres_import_payroll_check_issue_new_bank_check_dict[postgres_import_payroll_check_issue_new_bank_check_list_element[0]] = postgres_import_payroll_check_issue_new_bank_check_list_element[1].split(',')

                        for lite_new_bank_key_check_number_dict_element in lite_new_bank_key_check_number_dict:
                            if lite_new_bank_key_check_number_dict_element in postgres_import_payroll_check_issue_new_bank_check_dict:
                                data_not_found_dict['already_reissued_new_bank_check'][lite_new_bank_key_check_number_dict_element] = list(set(lite_new_bank_key_check_number_dict[lite_new_bank_key_check_number_dict_element]) & set(postgres_import_payroll_check_issue_new_bank_check_dict[lite_new_bank_key_check_number_dict_element]))

                        # Get check list from Odoo database
                        postgres_employee_new_bank_id_list = [postgres_employee_bank_dict[lite_employee_new_bank_key_dict_element]['id'] for lite_employee_new_bank_key_dict_element in lite_new_bank_key_check_number_dict]
                        current_query = """SELECT rb.l10n_mx_edi_code, STRING_AGG(concat(cl.folio, ',', cl.id), ';') 
                            FROM check_log cl INNER JOIN res_partner_bank rpb on cl.bank_account_id = rpb.id 
                            INNER JOIN res_bank rb on rpb.bank_id = rb.id where cl.status IN 
                            ('Available for printing') 
                            AND cl.general_status = 'available' AND rb.id IN %(postgres_employee_new_bank_id_list_element)s group by rb.l10n_mx_edi_code"""
                        
                        params =  {'postgres_employee_new_bank_id_list_element': tuple(postgres_employee_new_bank_id_list)}
                        self.env.cr.execute(current_query, params)

                        postgres_employee_new_check_log_list = self.env.cr.fetchall()
                        postgres_employee_new_check_log_dict = {}

                        for postgres_employee_new_check_log_list_element in postgres_employee_new_check_log_list:
                            postgres_employee_new_check_log_dict[postgres_employee_new_check_log_list_element[0]] = dict((key, value) for key, value in [element.split(',') for element in postgres_employee_new_check_log_list_element[1].split(';')])

                        for lite_new_bank_key_check_number_dict_element in lite_new_bank_key_check_number_dict:
                            if lite_new_bank_key_check_number_dict_element in postgres_employee_new_check_log_dict:
                                for lite_new_bank_key_check_number_dict_check in lite_new_bank_key_check_number_dict[lite_new_bank_key_check_number_dict_element]:
                                    if lite_new_bank_key_check_number_dict_check not in postgres_employee_new_check_log_dict[lite_new_bank_key_check_number_dict_element]:
                                        if lite_new_bank_key_check_number_dict_element in data_not_found_dict['new_check']:
                                            data_not_found_dict['new_check'][lite_new_bank_key_check_number_dict_element].append(lite_new_bank_key_check_number_dict_check)
                                        else:
                                            data_not_found_dict['new_check'].update({lite_new_bank_key_check_number_dict_element: [lite_new_bank_key_check_number_dict_check]})
                            else:
                                data_not_found_dict['new_check'].update({lite_new_bank_key_check_number_dict_element : lite_new_bank_key_check_number_dict[lite_new_bank_key_check_number_dict_element]})



                        # Validate data content between adjustment file and current payroll
                        for lite_from_file_data_dict_element in lite_from_file_data_dict:
                            for lite_from_file_data_dict_element_internal in lite_from_file_data_dict[lite_from_file_data_dict_element]:
                                # Cases when we need to validate columns B, C, J and change their values with D, E, K and we don't need to validate columns F, G, H. Exeption case E, ignore new import
                                if lite_from_file_data_dict_element_internal[0] == 'A' or lite_from_file_data_dict_element_internal[0] == 'D' or lite_from_file_data_dict_element_internal[0] == 'R' or lite_from_file_data_dict_element_internal[0] == 'E' or lite_from_file_data_dict_element_internal[0] == 'P':
                                    postgres_if_check = (str(postgres_employee_rfc_employee_payroll_file_dict[lite_from_file_data_dict_element].check_folio_id.folio or False), str(postgres_employee_rfc_employee_payroll_file_dict[lite_from_file_data_dict_element].bank_key or False), str(postgres_employee_rfc_employee_payroll_file_dict[lite_from_file_data_dict_element].net_salary or False))
                                    postgres_if_transfer = (str(postgres_employee_rfc_employee_payroll_file_dict[lite_from_file_data_dict_element].deposite_number or False), str(postgres_employee_rfc_employee_payroll_file_dict[lite_from_file_data_dict_element].bank_key or False), str(postgres_employee_rfc_employee_payroll_file_dict[lite_from_file_data_dict_element].net_salary or False))
                                    lite_if_check_or_tranfer = (str(lite_from_file_data_dict_element_internal[1]), str(lite_from_file_data_dict_element_internal[2]), str(float(lite_from_file_data_dict_element_internal[8])))
                                    if not (postgres_if_check == lite_if_check_or_tranfer or postgres_if_transfer == lite_if_check_or_tranfer):
                                        data_not_found_dict['original_data_no_coincide_a_d_r_e_p'].append((lite_from_file_data_dict_element_internal[0], lite_from_file_data_dict_element, lite_from_file_data_dict_element_internal[8]))
                                        
                                # Only require that employee is in current payroll and he has aditional payments
                                elif lite_from_file_data_dict_element_internal[0] == 'B':
                                    if lite_from_file_data_dict_element not in postgres_employee_rfc_employee_payroll_file_dict or not postgres_employee_rfc_employee_payroll_file_dict[lite_from_file_data_dict_element].additional_payments_line_ids:
                                        data_not_found_dict['employee_without_additional_payment'].append(lite_from_file_data_dict_element)
                                # Verify wheter data exists in Odoo database, creditors UTICT
                                elif lite_from_file_data_dict_element_internal[0] == 'C' or lite_from_file_data_dict_element_internal[0] == 'F' or lite_from_file_data_dict_element_internal[0] == 'H' or lite_from_file_data_dict_element_internal[0] == 'L' or lite_from_file_data_dict_element_internal[0] == 'S' or lite_from_file_data_dict_element_internal[0] == 'U' or lite_from_file_data_dict_element_internal[0] == 'V' or lite_from_file_data_dict_element_internal[0] == 'Z':
                                    pass
                                else:
                                    data_not_found_dict['general'].append((lite_from_file_data_dict_element_internal[0], lite_from_file_data_dict_element, lite_from_file_data_dict_element_internal[8]))


                        there_is_error = False
                        content = f'###################################################\nDATOS ERRÓNEOS AJUSTES : {datetime.now()}\n##################################################\n\n\n'
                        if len(data_not_found_dict['cause']) > 0:
                            content = content  + f"\n################################################################\nCasos de ajustes no encontrados en sistema.\n################################################################\n{', '.join(data_not_found_dict['cause'])}" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['employee']) > 0:
                            content = content  + f"\n#######################################################\nEmpleados no encontrados es sistema para reexpedición.\n#######################################################\n[{', '.join(list(dict.fromkeys(data_not_found_dict['employee'])))}]" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['employee_rfc']) > 0:
                            content = content  + f"\n#######################################################\nEmpleados no encontrados en la nómina actual para ajustes.\n#######################################################\n[{', '.join(data_not_found_dict['employee_rfc'])}]" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['original_data_no_coincide_a_d_r_e_p']) > 0:
                            content = content  + f"\n########################################################\nDatos que no coinciden con archivo y nómina actual.\n########################################################\n{data_not_found_dict['original_data_no_coincide_a_d_r_e_p']}" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['employee_without_additional_payment']) > 0:
                            content = content  + f"\n###############################################\nEmpleados en archivo y en la nómina actual pero sin pagos de prestación adicionales registrados.\n###############################################\n{data_not_found_dict['employee_without_additional_payment']}" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['bank']) > 0:
                            content = content  + f"\n###############################################\nClaves no banco no encontradas en el sistema.\n###############################################\n{data_not_found_dict['bank']}" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['new_bank']) > 0:
                            content = content  + f"\n###############################################\nClaves de nuevos bancos no encontradas en el sistema.\n###############################################\n{data_not_found_dict['new_bank']}" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['check']) > 0:
                            content = content  + f"\n###############################################\nNúmeros de cheque no encontrados en sistema o con estado no valido.\n###############################################\n[{data_not_found_dict['check']}]" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['new_check']) > 0:
                            content = content  + f"\n###############################################\nNúmeros de cheques nuevos no encontrados en sistema o con estado no valido.\n###############################################\n[{data_not_found_dict['new_check']}]" + '\n\n\n'
                            there_is_error = True
                        if sum([len(data_not_found_dict['already_reissued_original_bank_check'][dnfd_check]) or 0 for dnfd_check in data_not_found_dict['already_reissued_original_bank_check']]) > 0:
                            content = content  + f"\n###############################################\nNúmeros de cheques originales ya asociados a una reexpedición en sistema.\n###############################################\n[{data_not_found_dict['already_reissued_original_bank_check']}]" + '\n\n\n'
                            there_is_error = True
                        if sum([len(data_not_found_dict['already_reissued_new_bank_check'][dnfd_check]) or 0 for dnfd_check in data_not_found_dict['already_reissued_new_bank_check']]) > 0:
                            content = content  + f"\n###############################################\nNúmeros de cheques nuevos ya asociados a una reexpedición en sistema.\n###############################################\n[{data_not_found_dict['already_reissued_new_bank_check']}]" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['general']) > 0:
                            content = content  + f"\n###############################################\nCasos no excluidos y sin reglas definidas.\n###############################################\n[{data_not_found_dict['general']}]" + '\n\n\n'
                            there_is_error = True

                        if not there_is_error:
                            for lite_from_file_data_dict_element in lite_from_file_data_dict:
                                for lite_from_file_data_dict_element_internal in lite_from_file_data_dict[lite_from_file_data_dict_element]:
                                    postgres_adjustment_create_update_dict = {}
                                    if postgres_payroll_payment_adjustment_case[lite_from_file_data_dict_element_internal[0]]['is_adjustment_for_reissue'] == False:
                                        if lite_from_file_data_dict_element_internal[0] == 'B':
                                            postgres_adjustment_create_update_dict = {'additional_lending_l10n_mx_edi_payment_method_id' : postgres_employee_payment_method_dict['Cheque nominativo']['id'], 'additional_lending_res_bank_id' : postgres_employee_bank_dict[lite_from_file_data_dict_element_internal[4]]['id'], 'additional_lending_check_id' : postgres_employee_new_check_log_dict[lite_from_file_data_dict_element_internal[4]][lite_from_file_data_dict_element_internal[3]], 'adjustment_case_id' : postgres_payroll_payment_adjustment_case[lite_from_file_data_dict_element_internal[0]]['id'], 'payroll_payment_request_with_adjustment' : True}
                                            postgres_employee_rfc_employee_payroll_file_dict[lite_from_file_data_dict_element].write(postgres_adjustment_create_update_dict)
                                        elif postgres_employee_rfc_employee_payroll_file_dict[lite_from_file_data_dict_element].l10n_mx_edi_payment_method_id.id == postgres_employee_payment_method_dict['Cheque nominativo']['id']:
                                            postgres_adjustment_create_update_dict = {'l10n_mx_edi_payment_method_id' : postgres_employee_payment_method_dict['Cheque nominativo']['id'], 'bank_key' : lite_from_file_data_dict_element_internal[4], 'bank_key_name' : postgres_employee_bank_dict[lite_from_file_data_dict_element_internal[4]]['name'], 'deposite_number' : '', 'check_final_folio_id' : postgres_employee_new_check_log_dict[lite_from_file_data_dict_element_internal[4]][lite_from_file_data_dict_element_internal[3]], 'adjustment_case_id' : postgres_payroll_payment_adjustment_case[lite_from_file_data_dict_element_internal[0]]['id'] , 'net_salary' : lite_from_file_data_dict_element_internal[9] if float(lite_from_file_data_dict_element_internal[9]) > 0.0 else lite_from_file_data_dict_element_internal[8], 'payroll_payment_request_with_adjustment' : True}
                                            postgres_employee_rfc_employee_payroll_file_dict[lite_from_file_data_dict_element].write(postgres_adjustment_create_update_dict)
                                        else:
                                            postgres_adjustment_create_update_dict = {'l10n_mx_edi_payment_method_id' : postgres_employee_payment_method_dict['Cheque nominativo']['id'], 'bank_key' : lite_from_file_data_dict_element_internal[4], 'bank_key_name' : postgres_employee_bank_dict[lite_from_file_data_dict_element_internal[4]]['name'], 'check_final_folio_id' : postgres_employee_new_check_log_dict[lite_from_file_data_dict_element_internal[4]][lite_from_file_data_dict_element_internal[3]], 'adjustment_case_id' :postgres_payroll_payment_adjustment_case[lite_from_file_data_dict_element_internal[0]]['id'] , 'net_salary' : lite_from_file_data_dict_element_internal[9] if float(lite_from_file_data_dict_element_internal[9]) > 0.0 else lite_from_file_data_dict_element_internal[8], 'payroll_payment_request_with_adjustment' : True}
                                            postgres_employee_rfc_employee_payroll_file_dict[lite_from_file_data_dict_element].write(postgres_adjustment_create_update_dict)
                                    else:
                                        postgres_adjustment_create_update_dict = {'case_id' : postgres_payroll_payment_adjustment_case[lite_from_file_data_dict_element_internal[0]]['id'], 'original_bank_id' : postgres_employee_bank_dict[lite_from_file_data_dict_element_internal[2]]['id'] if lite_from_file_data_dict_element_internal[2] else False, 'original_check_id' : postgres_employee_check_log_dict[lite_from_file_data_dict_element_internal[2]][lite_from_file_data_dict_element_internal[1]] if lite_from_file_data_dict_element_internal[2] else False, 'new_bank_id' : postgres_employee_bank_dict[lite_from_file_data_dict_element_internal[4]]['id'], 'new_check_id' : postgres_employee_new_check_log_dict[lite_from_file_data_dict_element_internal[4]][lite_from_file_data_dict_element_internal[3]], 'employee_id' : postgres_employee_rfc_dict[lite_from_file_data_dict_element]['id'], 'original_fortnight' : lite_from_file_data_dict_element_internal[6][4:6], 'new_fortnight' : lite_from_file_data_dict_element_internal[7][4:6], 'original_amount' : lite_from_file_data_dict_element_internal[8], 'new_amount' : lite_from_file_data_dict_element_internal[9], 'payroll_processing_id' : self.payroll_process_id.id}
                                        self.env['import.payroll.check.issue'].create(postgres_adjustment_create_update_dict)

                            return {
                                'name': 'Importación exitosa',
                                'type': 'ir.actions.act_window',
                                'view_mode': 'form',
                                'res_model': 'payroll.payment.message.wizard',
                                'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Importación exitosa de ajustes.'}).id,
                                'target': 'new'
                            }

                        else:
                            self.payroll_process_id.update_failed_file(content, 'payroll_adjustment', 'completed', self.env.user.id, self.filename)
                            return {
                                'name': 'Error en validación',
                                'type': 'ir.actions.act_window',
                                'view_mode': 'form',
                                'res_model': 'payroll.payment.message.wizard',
                                'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Se encontraron errores durante la validación del archivo de ajustes, corriga los errores e intente nuevamente.'}).id,
                                'target': 'new'
                            }

                    else:
                        return {
                            'name': 'Error en columnas de archivo',
                            'type': 'ir.actions.act_window',
                            'view_mode': 'form',
                            'res_model': 'payroll.payment.message.wizard',
                            'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Número de columnas del archivo de ajustes, no coincide con las columnas esperadas.'}).id,
                            'target': 'new'
                        }

            if self.type_of_movement == 'perception_adjustment_detail':
                if postgres_employee_payroll_file_total == 0 or postgres_employee_deduction_line_total == 0:
                    return {
                        'name': 'Importación',
                        'type': 'ir.actions.act_window',
                        'view_mode': 'form',
                        'res_model': 'payroll.payment.message.wizard',
                        'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Error en importación de ajustes de percepciones de nómina, debe importar primero percepciones y deducciones.'}).id,
                        'target': 'new'
                    }
                elif postgres_employee_payroll_file_with_adjustment_total == 0:
                    return {
                        'name': 'Importación',
                        'type': 'ir.actions.act_window',
                        'view_mode': 'form',
                        'res_model': 'payroll.payment.message.wizard',
                        'res_id': self.env['payroll.payment.message.wizard'].create({'message': f'Error en importación de ajustes de percepciones de nómina, debe importar primero ajustes.'}).id,
                        'target': 'new'
                    }
                else:
                    perception_adjustment_header = ['rfc', 'category', 'program_code', 'payment_place', 'medium', 'bank_key', 'check_number', 'deposit_number', 'deposit_account_number', 'perception_key', 'amount']
                    defined_rules_for_payment_method = ['Transferencia electrónica de fondos', 'Cheque nominativo']
                    file_path_db = tempfile.mkdtemp()
                    fortnight = self.payroll_process_id.fornight
                    year = self.payroll_process_id.fornight
                    perception_adjustment_table_name = f'adjustment_perception_{fortnight}{year}'

                    db_payroll_path = f"{file_path_db}/payroll_{fortnight}{year}.db"
                    con = sqlite3.connect(db_payroll_path)
                    cur = con.cursor()


                    current_query = f"DROP TABLE IF EXISTS {perception_adjustment_table_name};"
                    cur.execute(current_query)
                    perception_adjustment_header_table_creation = [element + ' TEXT' for element in perception_adjustment_header]
                    perception_adjustment_header_table_creation = ','.join(perception_adjustment_header_table_creation)
                    current_query = f'CREATE TABLE {perception_adjustment_table_name}(id INTEGER PRIMARY KEY,{perception_adjustment_header_table_creation});'
                    cur.execute(current_query)


                    # To open the workbook
                    # Workbook object is created
                    wb_obj = openpyxl.load_workbook(filename=io.BytesIO(base64.b64decode(self.file)), read_only=True, data_only=True, keep_links=False)

                    # Get workbook active sheet object
                    # from the active attribute
                    sheet_obj = wb_obj.active

                    max_row = sheet_obj.max_row
                    max_column = sheet_obj.max_column

                    if max_column == len(perception_adjustment_header):
                        data = []
                        rfc_tmp = ''
                        counter = 1
                        for i in sheet_obj.iter_rows(values_only=True, min_row=2):
                            rfc = str(i[0]).strip() if i[0] is not None else ''
                            category = str(i[1]).strip() if i[1] is not None else ''
                            program_code = str(i[2]).strip() if i[2] is not None else ''
                            payment_place = str(i[3]).strip() if i[3] is not None else ''
                            medium = str(i[4]).strip() if i[4] is not None else ''
                            bank_key = str(i[5]).strip() if i[5] is not None else ''
                            check_number = str(i[6]).strip() if i[6] is not None else ''
                            deposit_number = str(i[7]).strip() if i[7] is not None else ''
                            deposit_account_number = str(i[8]).strip() if i[8] is not None else ''
                            perception_key = str(i[9]).strip() if i[9] is not None else ''
                            amount = str(i[10]).strip() if i[10] is not None else ''
                            if rfc != '':
                                rfc_tmp = rfc
                            current_query = f"INSERT INTO {perception_adjustment_table_name} VALUES({counter}, \'{rfc_tmp}\', \'{category}\', \'{program_code}\', \'{payment_place}\', \'{medium}\', \'{bank_key}\', \'{check_number}\', \'{deposit_number}\', \'{deposit_account_number}\', \'{perception_key}\', \'{amount}\');"
                            counter = counter + 1
                            cur.execute(current_query)
                    
                    
                        con.commit()

                        postgres_employee_payroll_file_list = self.env['employee.payroll.file'].search([('payroll_processing_id', '=', self.payroll_process_id.id)])
                        postgres_employee_payroll_file_dict = {}
                        for postgres_employee_payroll_file_list_element in postgres_employee_payroll_file_list:
                            postgres_employee_payroll_file_dict[postgres_employee_payroll_file_list_element.rfc] = postgres_employee_payroll_file_list_element

                        # Postgres program codes, program code dict
                        current_query = f'SELECT cp.program_code, cp.id, ie.unam_account_id FROM program_code cp LEFT JOIN expenditure_item ie ON cp.item_id = ie.id WHERE cp.state = \'validated\';'
                        self.env.cr.execute(current_query)
                        postgres_program_code_list = self.env.cr.fetchall()
                        postgres_program_code_dict = {}
                        for postgres_program_code_element in postgres_program_code_list:
                            postgres_program_code_dict[postgres_program_code_element[0]] = {'program_code_id' : postgres_program_code_element[1], 'unam_account_id' : postgres_program_code_element[2] or ''}


                        # Postgres payment methods, payment method dict
                        current_query = f'SELECT name, code, id FROM l10n_mx_edi_payment_method;'
                        self.env.cr.execute(current_query)
                        postgres_payment_method_list = self.env.cr.fetchall()
                        postgres_payment_method_dict = {}
                        for postgres_payment_method_list_element in postgres_payment_method_list:
                            postgres_payment_method_dict[postgres_payment_method_list_element[0]] = {'code' : postgres_payment_method_list_element[1], 'id' : postgres_payment_method_list_element[2]}


                        # Postgres banks, bank dict
                        current_query = f'SELECT l10n_mx_edi_code, name, id FROM res_bank WHERE l10n_mx_edi_code IS NOT NULL AND LENGTH(TRIM(l10n_mx_edi_code)) > 1;'
                        self.env.cr.execute(current_query)
                        postgres_bank_list = self.env.cr.fetchall()
                        postgres_bank_dict = {}
                        for postgres_bank_element in postgres_bank_list:
                            postgres_bank_dict[postgres_bank_element[0]] = {'name' : postgres_bank_element[1], 'id' : postgres_bank_element[2]}

                        # Postgres perception key dict
                        current_query = f'SELECT key, id FROM preception;'
                        self.env.cr.execute(current_query)
                        postgres_perception_list = self.env.cr.fetchall()
                        postgres_perception_dict = {}
                        for postgres_perception_list_element in postgres_perception_list:
                            postgres_perception_dict[postgres_perception_list_element[0]] = postgres_perception_list_element[1]

                        # Postgres deduction catalog dict
                        current_query = f'SELECT key, id FROM deduction;'
                        self.env.cr.execute(current_query)
                        postgres_employee_deduction_list = self.env.cr.fetchall()
                        postgres_employee_deduction_dict = {}
                        for postgres_employee_deduction_list_element in postgres_employee_deduction_list:
                            postgres_employee_deduction_dict[postgres_employee_deduction_list_element[0]] = {'deduction_id' : postgres_employee_deduction_list_element[1]}

                        postgres_adjustment_case_list = self.env['adjustment.cases'].search_read([], fields=['case', 'is_adjustment_for_reissue'])
                        postgres_adjustment_case_dict = {}
                        for postgres_adjustment_case_list_element in postgres_adjustment_case_list:
                            postgres_adjustment_case_dict[postgres_adjustment_case_list_element['case']] = {'is_adjustment_for_reissue' : postgres_adjustment_case_list_element['is_adjustment_for_reissue']}

                        # Lite data
                        current_query = f'SELECT DISTINCT(rfc) FROM {perception_adjustment_table_name};'
                        cur.execute(current_query)
                        lite_employee_rfc_list = cur.fetchall()

                        current_query = f'SELECT DISTINCT(program_code) FROM {perception_adjustment_table_name} WHERE program_code NOT LIKE \'{REBATE_PROGRAM_CODE_PATTERN}\';'
                        cur.execute(current_query)
                        lite_program_code_list = cur.fetchall()

                        current_query = f'SELECT DISTINCT(program_code) FROM {perception_adjustment_table_name} WHERE program_code LIKE \'{REBATE_PROGRAM_CODE_PATTERN}\';'
                        cur.execute(current_query)
                        lite_program_code_rebate_list = cur.fetchall()

                        current_query = f'SELECT DISTINCT(medium) FROM {perception_adjustment_table_name} WHERE LENGTH(TRIM(medium)) > 0;'
                        cur.execute(current_query)
                        lite_medium_list = cur.fetchall()

                        current_query = f'SELECT DISTINCT(bank_key) FROM {perception_adjustment_table_name} WHERE LENGTH(TRIM(bank_key)) > 0;'
                        cur.execute(current_query)
                        lite_bank_key_list = cur.fetchall()

                        lite_bank_key_dict = {}
                        for lite_bank_key_element in lite_bank_key_list:
                            lite_bank_key_dict[lite_bank_key_element[0]] = ''


                        current_query = f'SELECT DISTINCT(perception_key) FROM {perception_adjustment_table_name};'
                        cur.execute(current_query)
                        lite_perception_list = cur.fetchall()


                        data_not_found_dict = {
                            'employee' : [],
                            'employee_payroll_file_adjustment_no_marked' : [],
                            'program_code' : [],
                            'payment_method' : [],
                            'bank_key' : [],
                            'perception_key' : [],
                            'check' : {},
                            'employee_bank_account' : [],
                            'rebate' : {},
                        }

                        for lite_employee_rfc_list_element in lite_employee_rfc_list:
                            if lite_employee_rfc_list_element[0] not in postgres_employee_payroll_file_dict:
                                data_not_found_dict['employee'].append(lite_employee_rfc_list_element[0])

                        # Employee found but payment request is not marked as adjustment
                        for lite_employee_rfc_list_element in lite_employee_rfc_list:
                            if lite_employee_rfc_list_element[0] in postgres_employee_payroll_file_dict and postgres_employee_payroll_file_dict[lite_employee_rfc_list_element[0]].payroll_payment_request_with_adjustment == False:
                                data_not_found_dict['employee_payroll_file_adjustment_no_marked'].append(lite_employee_rfc_list_element[0])


                        for lite_program_code_list_element in lite_program_code_list:
                            if lite_program_code_list_element[0] not in postgres_program_code_dict:
                                data_not_found_dict['program_code'].append(lite_program_code_list_element[0])

                        for lite_program_code_list_rebate_element in lite_program_code_rebate_list:
                            lite_program_code_deduction = lite_program_code_list_rebate_element[0][4:8].strip('0')                           
                            if lite_program_code_deduction not in postgres_employee_deduction_dict:
                                data_not_found_dict['rebate'].update({lite_program_code_list_rebate_element : lite_program_code_deduction})

                        for lite_medium_list_element in lite_medium_list:
                            if lite_medium_list_element[0] not in postgres_payment_method_dict:
                                data_not_found_dict['payment_method'].append(lite_medium_list_element[0])

                        for lite_bank_key_list_element in lite_bank_key_list:
                            if lite_bank_key_list_element[0] not in postgres_bank_dict:
                                data_not_found_dict['bank_key'].append(lite_bank_key_list_element[0])

                        for data_not_found_dict_element in data_not_found_dict['bank_key']:
                            lite_bank_key_dict.pop(data_not_found_dict_element)


                        for lite_perception_list_element in lite_perception_list:
                            if lite_perception_list_element[0] not in postgres_perception_dict:
                                data_not_found_dict['perception_key'].append(lite_perception_list_element[0])


                        # Get check list from Odoo database
                        postgres_bank_id_list = [postgres_bank_dict[lite_bank_key_dict_element]['id'] for lite_bank_key_dict_element in lite_bank_key_dict]
                        current_query = """SELECT rb.l10n_mx_edi_code, STRING_AGG(concat(cl.folio, ',', cl.id), ';') 
                        FROM check_log cl INNER JOIN res_partner_bank rpb on cl.bank_account_id = rpb.id 
                        INNER JOIN res_bank rb on rpb.bank_id = rb.id where cl.status IN 
                        ('Available for printing') 
                        AND cl.general_status = 'available' AND rb.id IN %(postgres_bank_id_list)s group by rb.l10n_mx_edi_code"""
                        
                        params = {'postgres_bank_id_list' : tuple(postgres_bank_id_list)}
                        self.env.cr.execute(current_query, params)

                        postgres_check_log_list = self.env.cr.fetchall()
                        postgres_check_log_dict = {}

                        for postgres_check_log_list_element in postgres_check_log_list:
                            postgres_check_log_dict[postgres_check_log_list_element[0]] = dict((key, value) for key, value in [element.split(',') for element in postgres_check_log_list_element[1].split(';')])


                        # Check folios list per bank, perception file
                        current_query = f'SELECT bank_key, GROUP_CONCAT(check_number, \',\') as checks FROM {perception_adjustment_table_name} WHERE TRIM(medium) = \'Cheque nominativo\' GROUP BY bank_key ORDER BY bank_key;'
                        cur.execute(current_query)
                        lite_check_per_bank_list = cur.fetchall()
                        lite_check_folio_per_bank_dict = {}
                        for lite_check_per_bank_list_element in lite_check_per_bank_list:
                            lite_check_folio_per_bank_dict[lite_check_per_bank_list_element[0]] = dict((employee_check_per_bank_folio, '') for employee_check_per_bank_folio in lite_check_per_bank_list_element[1].split(','))

                        for lite_check_folio_per_bank_dict_element in lite_check_folio_per_bank_dict:
                            data_not_found_dict['check'].update({lite_check_folio_per_bank_dict_element : {}})


                        lite_check_folio_deep_copy_tmp = copy.deepcopy(lite_check_folio_per_bank_dict)
                        for lite_check_folio_per_bank_dict_element in lite_check_folio_deep_copy_tmp:
                            if lite_check_folio_per_bank_dict_element in postgres_check_log_dict:
                                for lite_check_folio_element in lite_check_folio_deep_copy_tmp[lite_check_folio_per_bank_dict_element]:
                                    if lite_check_folio_element not in postgres_check_log_dict[lite_check_folio_per_bank_dict_element]:
                                        data_not_found_dict['check'][lite_check_folio_per_bank_dict_element].update({lite_check_folio_element : lite_check_folio_per_bank_dict[lite_check_folio_per_bank_dict_element].pop(lite_check_folio_element)})
                            else:
                                data_not_found_dict['check'][lite_check_folio_per_bank_dict_element] = lite_check_folio_per_bank_dict.pop(lite_check_folio_per_bank_dict_element)

                        lite_check_folio_deep_copy_tmp = {}

                        there_is_error = False
                        content = f'###################################################\nDATOS ERRÓNEOS PERCEPCIONES AJUSTES : {datetime.now()}\n##################################################\n\n\n'
                        if len(data_not_found_dict['employee']) > 0:
                            content = content  + f"\n################################################################\nEmpleados no encontrados en nómina actual\n################################################################\n{data_not_found_dict['employee']}" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['employee_payroll_file_adjustment_no_marked']) > 0:
                            content = content  + f"\n################################################################\nEmpleados encontrados en nómina actual pero no especificado para ajuste\n################################################################\n{data_not_found_dict['employee_payroll_file_adjustment_no_marked']}" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['program_code']) > 0:
                            content = content  + f"\n#######################################################\nCódigos programáticos no encontrados en sistema\n#######################################################\n{data_not_found_dict['program_code']}" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['payment_method']) > 0:
                            content = content  + f"\n########################################################\nMétodos de pago no encontradas en sistema\n########################################################\n{data_not_found_dict['payment_method']}" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['bank_key']) > 0:
                            content = content  + f"\n###############################################\nClaves de banco no encontrados en sistema\n###############################################\n{data_not_found_dict['bank_key']}" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['perception_key']) > 0:
                            content = content  + f"\n###############################################\nClaves de percepciónes no encontrados en sistema\n###############################################\n{data_not_found_dict['perception_key']}" + '\n\n\n'
                            there_is_error = True
                        if sum([len(data_not_found_dict['check'][dnfd_check]) or 0 for dnfd_check in data_not_found_dict['check']]) > 0:
                            content = content  + f"\n##################################################################\nNúmeros de cheque no encontrados en sistema o con estado no valido\n##################################################################\n[{json.dumps(data_not_found_dict['check'])}]" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['rebate']) > 0:
                            content = content  + f"\n##############################################\nClaves de deducciones para devoluciones no encontradas en sistema\n##############################################\n[{', '.join(data_not_found_dict['rebate'].values())}]" + '\n\n\n'
                            there_is_error = True


                        if not there_is_error:
                            current_query = f"SELECT {','.join(perception_adjustment_header)} FROM {perception_adjustment_table_name};"
                            cur.execute(current_query)
                            lite_perception_adjustment_list = cur.fetchall()

                            lite_perception_adjustment_dict = {}
                            for lite_perception_adjustment_list_element in lite_perception_adjustment_list:
                                if lite_perception_adjustment_list_element[0] in lite_perception_adjustment_dict:
                                    lite_perception_adjustment_dict[lite_perception_adjustment_list_element[0]].append((lite_perception_adjustment_list_element[1], lite_perception_adjustment_list_element[2], lite_perception_adjustment_list_element[3], lite_perception_adjustment_list_element[4], lite_perception_adjustment_list_element[5], lite_perception_adjustment_list_element[6], lite_perception_adjustment_list_element[7], lite_perception_adjustment_list_element[8], lite_perception_adjustment_list_element[9], lite_perception_adjustment_list_element[10]))
                                else:
                                    lite_perception_adjustment_dict[lite_perception_adjustment_list_element[0]] = [(lite_perception_adjustment_list_element[1], lite_perception_adjustment_list_element[2], lite_perception_adjustment_list_element[3], lite_perception_adjustment_list_element[4], lite_perception_adjustment_list_element[5], lite_perception_adjustment_list_element[6], lite_perception_adjustment_list_element[7], lite_perception_adjustment_list_element[8], lite_perception_adjustment_list_element[9], lite_perception_adjustment_list_element[10])]

                            current_query = f"""SELECT rfc, printf('%.2f', sum(cast(amount as decimal))) AS total_perception_amount
                                                FROM {perception_adjustment_table_name} GROUP BY rfc;
                                            """
                            cur.execute(current_query)
                            lite_perception_adjustment_total_amount_list = cur.fetchall()

                            lite_perception_adjustment_total_amount_dict = {}
                            for lite_perception_adjustment_total_amount_list_element in lite_perception_adjustment_total_amount_list:
                                lite_perception_adjustment_total_amount_dict[lite_perception_adjustment_total_amount_list_element[0]] = {'total_perception_amount' : lite_perception_adjustment_total_amount_list_element[1]}


                            postgres_employee_payroll_file_perception_line_unlink_list = []
                            for lite_perception_adjustment_dict_element in lite_perception_adjustment_dict:
                                postgres_employee_payroll_file_perception_line_unlink_list.extend(postgres_employee_payroll_file_dict[lite_perception_adjustment_dict_element].preception_line_ids.ids)

                            self.env['preception.line'].search([('id', 'in', postgres_employee_payroll_file_perception_line_unlink_list)]).unlink()
                            
                            current_query_general_value = """
                                    (nextval('preception_line_id_seq'), {}, (now() at time zone 'UTC'), {}, 
                                    (now() at time zone 'UTC'), {}, true, true, 
                                    {}, {}, {}, {}),"""

                            current_query = """
                                INSERT INTO "preception_line" ("id", "create_uid", "create_date", "write_uid", 
                                    "write_date", "amount", "is_adjustment_update", "is_create_from_this", 
                                    "payroll_id", "preception_id", "program_code_id", "account_id") 
                                VALUES """

                            rebate_current_query_general_value = """
                                    (nextval('rebate_line_id_seq'), {}, {}, {}, 
                                    {}, {}, (now() at time zone 'UTC'), {}, 
                                    (now() at time zone 'UTC')),
                            """

                            rebate_current_query = """
                                INSERT INTO "rebate_line" ("id", "payroll_id", "perception_key_id", "deduction_key_id", 
                                    "amount", "create_uid", "create_date", "write_uid", 
                                    "write_date")
                                VALUES """


                            current_query_general_value_tmp = ""
                            rebate_current_query_general_value_tmp = ""
                            for lite_perception_adjustment_dict_element in lite_perception_adjustment_dict:
                                for lite_perception_adjustment_dict_internal in lite_perception_adjustment_dict[lite_perception_adjustment_dict_element]:
                                    if lite_perception_adjustment_dict_internal[1].find(REBATE_PROGRAM_CODE_PATTERN.strip('_')) != -1:
                                        rebate_current_query_general_value_tmp = rebate_current_query_general_value_tmp + rebate_current_query_general_value.format(postgres_employee_payroll_file_dict[lite_perception_adjustment_dict_element].id, postgres_perception_dict[lite_perception_adjustment_dict_internal[8]], postgres_employee_deduction_dict[lite_perception_adjustment_dict_internal[1][4:8].strip('0')]['deduction_id'],
                                            lite_perception_adjustment_dict_internal[9], self.env.user.id, self.env.user.id)
                                    else:
                                        current_query_general_value_tmp = current_query_general_value_tmp + current_query_general_value.format(self.env.user.id, self.env.user.id, 
                                        lite_perception_adjustment_dict_internal[9],
                                        postgres_employee_payroll_file_dict[lite_perception_adjustment_dict_element].id, postgres_perception_dict[lite_perception_adjustment_dict_internal[8]], postgres_program_code_dict[lite_perception_adjustment_dict_internal[1]]['program_code_id'], postgres_program_code_dict[lite_perception_adjustment_dict_internal[1]]['unam_account_id'])

                            if len(current_query_general_value_tmp) > 0:
                                current_query = current_query + current_query_general_value_tmp
                                current_query = current_query.strip().strip(',')
                                self.env.cr.execute(current_query)

                            if len(rebate_current_query_general_value_tmp) > 0:
                                rebate_current_query = rebate_current_query + rebate_current_query_general_value_tmp
                                rebate_current_query = rebate_current_query.strip().strip(',')
                                self.env.cr.execute(rebate_current_query)

                            for lite_perception_adjustment_total_amount_dict_element in lite_perception_adjustment_total_amount_dict:
                                postgres_employee_payroll_file_dict[lite_perception_adjustment_total_amount_dict_element].total_preception = lite_perception_adjustment_total_amount_dict[lite_perception_adjustment_total_amount_dict_element]['total_perception_amount']

                            self.env.cr.commit()

                            return {
                                'name': 'Importación exitosa',
                                'type': 'ir.actions.act_window',
                                'view_mode': 'form',
                                'res_model': 'payroll.payment.message.wizard',
                                'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Importación exitosa de ajustes de percepciones.'}).id,
                                'target': 'new'
                            }

                        else:
                            self.payroll_process_id.update_failed_file(content, 'payroll_perception_adjustment', 'completed', self.env.user.id, self.filename)
                            return {
                                'name': 'Error en validación',
                                'type': 'ir.actions.act_window',
                                'view_mode': 'form',
                                'res_model': 'payroll.payment.message.wizard',
                                'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Se encontraron errores durante la validación del archivo de ajustes de percepciones, corriga los errores e intente nuevamente.'}).id,
                                'target': 'new'
                            }
                    else:
                        return {
                            'name': 'Error en columnas de archivo',
                            'type': 'ir.actions.act_window',
                            'view_mode': 'form',
                            'res_model': 'payroll.payment.message.wizard',
                            'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Número de columnas del archivo de ajustes de percepciones, no coincide con las columnas esperadas.'}).id,
                            'target': 'new'
                        }


            if self.type_of_movement == 'deduction_adjustment_detail':
                if postgres_employee_payroll_file_total == 0 or postgres_employee_deduction_line_total == 0:
                    return {
                        'name': 'Importación',
                        'type': 'ir.actions.act_window',
                        'view_mode': 'form',
                        'res_model': 'payroll.payment.message.wizard',
                        'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Error en importación de ajustes de deducciones de nómina, debe importar primero percepciones y deducciones.'}).id,
                        'target': 'new'
                    }
                elif postgres_employee_payroll_file_with_adjustment_total == 0:
                    return {
                        'name': 'Importación',
                        'type': 'ir.actions.act_window',
                        'view_mode': 'form',
                        'res_model': 'payroll.payment.message.wizard',
                        'res_id': self.env['payroll.payment.message.wizard'].create({'message': f'Error en importación de ajustes de deducciones de nómina, debe importar primero ajustes.'}).id,
                        'target': 'new'
                    }
                elif postgres_employee_payroll_file_perception_adjustment_total == 0:
                    return {
                        'name': 'Importación',
                        'type': 'ir.actions.act_window',
                        'view_mode': 'form',
                        'res_model': 'payroll.payment.message.wizard',
                        'res_id': self.env['payroll.payment.message.wizard'].create({'message': f'Error en importación de ajustes de deduction de nómina, debe importar primero ajustes de percepciones.'}).id,
                        'target': 'new'
                    }
                else:
                    deduction_adjustment_header = ['rfc', 'account_accountant', 'deduction_key', 'amount', 'net_salary']
                    file_path_db = tempfile.mkdtemp()
                    fortnight = self.payroll_process_id.fornight
                    year = self.payroll_process_id.fornight
                    deduction_adjustment_table_name = f'deduction_adjustment_{fortnight}{year}'

                    db_payroll_path = f"{file_path_db}/payroll_{fortnight}{year}.db"
                    con = sqlite3.connect(db_payroll_path)
                    cur = con.cursor()


                    current_query = f"DROP TABLE IF EXISTS {deduction_adjustment_table_name};"
                    cur.execute(current_query)
                    deduction_header_table_creation = [element + ' TEXT' for element in deduction_adjustment_header]
                    deduction_header_table_creation = ','.join(deduction_header_table_creation)
                    current_query = f'CREATE TABLE {deduction_adjustment_table_name}(id INTEGER PRIMARY KEY,{deduction_header_table_creation});'
                    cur.execute(current_query)


                    # To open the workbook
                    # Workbook object is created
                    wb_obj = openpyxl.load_workbook(filename=io.BytesIO(base64.b64decode(self.file)), read_only=True, data_only=True, keep_links=False)

                    # Get workbook active sheet object
                    # from the active attribute
                    sheet_obj = wb_obj.active

                    max_row = sheet_obj.max_row
                    max_column = sheet_obj.max_column

                    if max_column == len(deduction_adjustment_header):
                        data = []
                        rfc_tmp = ''
                        counter = 1
                        for i in sheet_obj.iter_rows(values_only=True, min_row=2):
                            rfc = str(i[0]).strip() if i[0] is not None else ''
                            account_accountant = str(i[1]).strip() if i[1] is not None else ''
                            deduction_key = str(i[2]).strip() if i[2] is not None else ''
                            amount = str(i[3]).strip() if i[3] is not None else ''
                            net_salary = str(i[4]).strip() if i[4] is not None else ''
                            if rfc != '':
                                rfc_tmp = rfc

                            current_query = f"INSERT INTO {deduction_adjustment_table_name} VALUES({counter}, \'{rfc_tmp}\', \'{account_accountant}\', \'{deduction_key}\', \'{amount}\', \'{net_salary}\');"
                            counter = counter + 1
                            cur.execute(current_query)
                    
                    
                        con.commit()

                        postgres_employee_payroll_file_whole_list = self.env['employee.payroll.file'].search([('payroll_processing_id', '=', self.payroll_process_id.id)])
                        postgres_employee_payroll_file_whole_dict = {}
                        for postgres_employee_payroll_file_whole_list_element in postgres_employee_payroll_file_whole_list:
                            postgres_employee_payroll_file_whole_dict[postgres_employee_payroll_file_whole_list_element.rfc] = postgres_employee_payroll_file_whole_list_element


                        # Postgres deduction catalog dict
                        current_query = f'SELECT key, id FROM deduction;'
                        self.env.cr.execute(current_query)
                        postgres_deduction_list = self.env.cr.fetchall()
                        postgres_deduction_dict = {}
                        for postgres_deduction_list_element in postgres_deduction_list:
                            postgres_deduction_dict[postgres_deduction_list_element[0]] = {'deduction_id' : postgres_deduction_list_element[1]}


                        data_not_found_dict = {
                            'employee' : [],
                            'employee_adjustment_no_marked' : [],
                            'employee_perception_adjustment' : [],
                            'deduction_key' : [],
                        }

                        current_query = f'SELECT DISTINCT(rfc) FROM {deduction_adjustment_table_name};'
                        cur.execute(current_query)
                        lite_employee_rfc_list = cur.fetchall()
                        lite_employee_rfc_dict = {}
                        for lite_employee_rfc_list_element in lite_employee_rfc_list:
                            lite_employee_rfc_dict[lite_employee_rfc_list_element[0]] = ''

                        current_query = f'SELECT DISTINCT(deduction_key) FROM {deduction_adjustment_table_name};'
                        cur.execute(current_query)
                        lite_deduction_list = cur.fetchall()

                        for lite_employee_rfc_dict_element in lite_employee_rfc_dict:
                            if lite_employee_rfc_dict_element not in postgres_employee_payroll_file_whole_dict:
                                data_not_found_dict['employee'].append(lite_employee_rfc_dict_element)

                        for data_not_found_dict_element in data_not_found_dict['employee']:
                            lite_employee_rfc_dict.pop(data_not_found_dict_element)

                        for lite_employee_rfc_dict_element in lite_employee_rfc_dict:
                            if lite_employee_rfc_dict_element in postgres_employee_payroll_file_whole_dict and postgres_employee_payroll_file_whole_dict[lite_employee_rfc_dict_element].payroll_payment_request_with_adjustment == False:
                                data_not_found_dict['employee_adjustment_no_marked'].append(lite_employee_rfc_dict_element)

                        current_query = f"""
                                SELECT STRING_AGG(DISTINCT(fpe.id::varchar), ',') as employee_payrol_file_id FROM preception_line lp
                                INNER JOIN employee_payroll_file fpe ON lp.payroll_id = fpe.id
                                INNER JOIN custom_payroll_processing ppc ON fpe.payroll_processing_id  = ppc.id
                                WHERE is_adjustment_update = true AND ppc.id = {self.payroll_process_id.id};
                            """
                        self.env.cr.execute(current_query)
                        
                        postgres_employee_payroll_file_perception_adjustment_list = self.env.cr.fetchall()
                        postgres_employee_payroll_file_perception_adjustment_list = postgres_employee_payroll_file_perception_adjustment_list[0][0] or []

                        if postgres_employee_payroll_file_perception_adjustment_list:
                            postgres_employee_payroll_file_perception_adjustment_list = postgres_employee_payroll_file_perception_adjustment_list.split(',')

                        postgres_employee_payroll_file_perception_adjustment_list_object = self.env['employee.payroll.file'].search([('id', 'in', postgres_employee_payroll_file_perception_adjustment_list)])
                        postgres_employee_payroll_file_perception_adjustment_dict_object = {}
                        for postgres_employee_payroll_file_perception_adjustment_list_object_element in postgres_employee_payroll_file_perception_adjustment_list_object:
                            postgres_employee_payroll_file_perception_adjustment_dict_object[postgres_employee_payroll_file_perception_adjustment_list_object_element.rfc] = postgres_employee_payroll_file_perception_adjustment_list_object_element


                        for lite_employee_rfc_dict_element in lite_employee_rfc_dict:
                            if lite_employee_rfc_dict_element not in postgres_employee_payroll_file_perception_adjustment_dict_object:
                                data_not_found_dict['employee_perception_adjustment'].append(lite_employee_rfc_dict_element)


                        for lite_deduction_list_element in lite_deduction_list:
                            if lite_deduction_list_element[0] not in postgres_deduction_dict:
                                data_not_found_dict['deduction_key'].append(lite_deduction_list_element[0])

                        there_is_error = False
                        content = f'###################################################\nDATOS ERRÓNEOS DEDUCCIONES AJUSTES : {datetime.now()}\n##################################################\n\n\n'
                        if len(data_not_found_dict['employee']) > 0:
                            content = content  + f"\n################################################################\nEmpleados no encontrados en nómina actual\n################################################################\n{data_not_found_dict['employee']}" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['employee_adjustment_no_marked']) > 0:
                            content = content  + f"\n#######################################################\nEmpleados sin ajustes cargados\n#######################################################\n{data_not_found_dict['employee_adjustment_no_marked']}" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['employee_perception_adjustment']) > 0:
                            content = content  + f"\n########################################################\nEmpleados sin ajustes de percepciones cargadas\n########################################################\n{data_not_found_dict['employee_perception_adjustment']}" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['deduction_key']) > 0:
                            content = content  + f"\n###############################################\nClaves de deducciones no encontradas en sistema\n###############################################\n{data_not_found_dict['deduction_key']}" + '\n\n\n'
                            there_is_error = True

                        if not there_is_error:
                            current_query = f"""
                                SELECT STRING_AGG(ld.id::varchar, ',') as deduction_line_id  FROM deduction_line ld inner join employee_payroll_file fpe 
                                on ld.payroll_id = fpe.id inner join custom_payroll_processing pcc 
                                on fpe.payroll_processing_id  = pcc.id where pcc.id = {self.payroll_process_id.id} and fpe.payroll_payment_request_with_adjustment = true AND
                                fpe.employee_id in (SELECT id FROM hr_employee WHERE rfc IN {tuple(lite_employee_rfc_dict.keys())});"""
                            self.env.cr.execute(current_query);
                            postgres_employee_payroll_file_deduction_line_list =  self.env.cr.fetchall()
                            postgres_employee_payroll_file_deduction_line_list = postgres_employee_payroll_file_deduction_line_list[0][0] or []


                            if postgres_employee_payroll_file_deduction_line_list:
                                postgres_employee_payroll_file_deduction_line_list = postgres_employee_payroll_file_deduction_line_list.split(',')

                            postgres_employee_payroll_file_deduction_line_object = self.env['deduction.line'].search([('id', 'in', postgres_employee_payroll_file_deduction_line_list)])
                            postgres_employee_payroll_file_deduction_line_object.unlink()

                            postgres_employee_payroll_file_with_adjustment_list = self.env['employee.payroll.file'].search([('payroll_processing_id', '=', self.payroll_process_id.id), ('payroll_payment_request_with_adjustment', '=', True)])
                            postgres_employee_payroll_file_with_adjustment_dict = {}

                            for postgres_employee_payroll_file_with_adjustment_list_element in postgres_employee_payroll_file_with_adjustment_list:
                                postgres_employee_payroll_file_with_adjustment_dict[postgres_employee_payroll_file_with_adjustment_list_element.rfc] = postgres_employee_payroll_file_with_adjustment_list_element

                            current_query = f"""
                                    SELECT rfc, printf('%.2f', SUM(CAST(amount as DECIMAL))) AS total_deduction_amount
                                    FROM {deduction_adjustment_table_name}
                                    GROUP BY rfc;
                                """
                            cur.execute(current_query)
                            lite_deduction_adjustment_total_amount_list = cur.fetchall()

                            lite_deduction_adjustment_total_amount_dict = {}
                            for lite_deduction_adjustment_total_amount_list_element in lite_deduction_adjustment_total_amount_list:
                                lite_deduction_adjustment_total_amount_dict[lite_deduction_adjustment_total_amount_list_element[0]] = {'total_deduction_amount' : lite_deduction_adjustment_total_amount_list_element[1]}

                            current_query = f"SELECT {','.join(deduction_adjustment_header)} FROM {deduction_adjustment_table_name};"
                            cur.execute(current_query)
                            lite_deduction_adjustment_file_list = cur.fetchall()
                            lite_deduction_adjustment_file_dict = {}

                            for lite_deduction_adjustment_file_list_element in lite_deduction_adjustment_file_list:
                                if lite_deduction_adjustment_file_list_element[0] in lite_deduction_adjustment_file_dict:
                                    lite_deduction_adjustment_file_dict[lite_deduction_adjustment_file_list_element[0]].append((lite_deduction_adjustment_file_list_element[1], lite_deduction_adjustment_file_list_element[2], lite_deduction_adjustment_file_list_element[3]))
                                else:
                                    lite_deduction_adjustment_file_dict[lite_deduction_adjustment_file_list_element[0]] = [(lite_deduction_adjustment_file_list_element[1], lite_deduction_adjustment_file_list_element[2], lite_deduction_adjustment_file_list_element[3])]

                            current_query_general_value = """
                                    (nextval('deduction_line_id_seq'), {}, (now() at time zone 'UTC'), {}, 
                                    (now() at time zone 'UTC'), {}, true, true, 
                                    {}, {}, {}),
                            """

                            current_query = """
                                INSERT INTO "deduction_line" ("id", "create_uid", "create_date", "write_uid", 
                                    "write_date", "amount", "is_adjustment_update", "is_create_from_this", 
                                    "payroll_id", "deduction_id", "net_salary") 
                                VALUES """

                            value_tmp = ""
                            for lite_deduction_adjustment_file_dict_element in lite_deduction_adjustment_file_dict:
                                for deduction_element in lite_deduction_adjustment_file_dict[lite_deduction_adjustment_file_dict_element]:
                                    value_tmp = current_query_general_value.format(self.env.user.id, self.env.user.id,
                                        deduction_element[2],
                                        postgres_employee_payroll_file_with_adjustment_dict[lite_deduction_adjustment_file_dict_element].id, postgres_deduction_dict[deduction_element[1]]['deduction_id'], deduction_element[2])

                                    current_query = current_query + value_tmp

                            current_query = current_query.strip().strip(',')
                            self.env.cr.execute(current_query)

                            for lite_deduction_adjustment_total_amount_dict_element in lite_deduction_adjustment_total_amount_dict:
                                postgres_employee_payroll_file_with_adjustment_dict[lite_deduction_adjustment_total_amount_dict_element].total_deduction = lite_deduction_adjustment_total_amount_dict[lite_deduction_adjustment_total_amount_dict_element]['total_deduction_amount']

                            self.env.cr.commit()

                            return {
                                'name': 'Importación exitosa',
                                'type': 'ir.actions.act_window',
                                'view_mode': 'form',
                                'res_model': 'payroll.payment.message.wizard',
                                'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Importación exitosa de ajustes de deducciones.'}).id,
                                'target': 'new'
                            }

                        else:
                            self.payroll_process_id.update_failed_file(content, 'payroll_deduction_adjustment', 'completed', self.env.user.id, self.filename)
                            return {
                                'name': 'Error en validación',
                                'type': 'ir.actions.act_window',
                                'view_mode': 'form',
                                'res_model': 'payroll.payment.message.wizard',
                                'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Se encontraron errores durante la validación del archivo de ajustes de deducciones, corriga los errores e intente nuevamente.'}).id,
                                'target': 'new'
                            }

                        
            if self.type_of_movement == 'detail_alimony_adjustments':
                current_query = f"""
                        SELECT count(*) FROM pension_payment_line lpp
                        INNER JOIN employee_payroll_file fpe ON lpp.payroll_id = fpe.id
                        INNER JOIN custom_payroll_processing ppc ON fpe.payroll_processing_id = ppc.id
                        WHERE ppc.id = {self.payroll_process_id.id}
                    """
                self.env.cr.execute(current_query)
                postgres_employee_alimony_total = self.env.cr.fetchall()
                postgres_employee_alimony_total = int(postgres_employee_alimony_total[0][0])
                if postgres_employee_alimony_total == 0:
                    return {
                        'name': 'Importación',
                        'type': 'ir.actions.act_window',
                        'view_mode': 'form',
                        'res_model': 'payroll.payment.message.wizard',
                        'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Error en importación de ajustes de pensión alimenticia, debe importar primero el archivo de pensión alimenticia.'}).id,
                        'target': 'new'
                    }

                else:
                    alimony_adjustment_header = ['beneficiary', 'medium', 'original_bank_key', 'new_bank_key', 'original_check_number', 'new_check_number', 'deposit_number', 'deposit_account_number', 'original_amount']
                    file_path_db = tempfile.mkdtemp()
                    fortnight = self.payroll_process_id.fornight
                    year = self.payroll_process_id.fornight
                    alimony_adjustment_table_name = f'alimony_adjustment_{fortnight}{year}'

                    db_payroll_path = f"{file_path_db}/payroll_{fortnight}{year}.db"
                    con = sqlite3.connect(db_payroll_path)
                    cur = con.cursor()


                    current_query = f"DROP TABLE IF EXISTS {alimony_adjustment_table_name};"
                    cur.execute(current_query)
                    alimony_header_table_creation = [element + ' TEXT' for element in alimony_adjustment_header]
                    alimony_header_table_creation = ','.join(alimony_header_table_creation)
                    current_query = f'CREATE TABLE {alimony_adjustment_table_name}(id INTEGER PRIMARY KEY,{alimony_header_table_creation});'
                    cur.execute(current_query)


                    # To open the workbook
                    # Workbook object is created
                    wb_obj = openpyxl.load_workbook(filename=io.BytesIO(base64.b64decode(self.file)), read_only=True, data_only=True, keep_links=False)

                    # Get workbook active sheet object
                    # from the active attribute
                    sheet_obj = wb_obj.active

                    max_row = sheet_obj.max_row
                    max_column = sheet_obj.max_column

                    if max_column == len(alimony_adjustment_header):
                        counter = 1
                        for i in sheet_obj.iter_rows(values_only=True, min_row=2):
                            beneficiary = str(i[0]).strip() if i[0] is not None else ''
                            medium = str(i[1]).strip() if i[1] is not None else ''
                            original_bank_key = str(i[2]).strip() if i[2] is not None else ''
                            new_bank_key = str(i[3]).strip() if i[3] is not None else ''
                            original_check_number = str(i[4]).strip() if i[4] is not None else ''
                            new_check_number = str(i[5]).strip() if i[5] is not None else ''
                            deposit_number = str(i[6]).strip() if i[6] is not None else ''
                            deposit_account_number = str(i[7]).strip() if i[7] is not None else ''
                            original_amount = str(i[8]).strip() if i[8] is not None else ''

                            current_query = f"INSERT INTO {alimony_adjustment_table_name} VALUES({counter}, \'{beneficiary}\', \'{medium}\', \'{original_bank_key}\', \'{new_bank_key}\', \'{original_check_number}\', \'{new_check_number}\', \'{deposit_number}\', \'{deposit_account_number}\', printf('%.2f', {original_amount}));"
                            counter = counter + 1
                            cur.execute(current_query)
                    
                        con.commit()

                        data_not_found_dict = {
                            'beneficiary' : [],
                            'new_bank' : [],
                            'new_check' : {},
                        }

                        current_query = f"SELECT {','.join(alimony_adjustment_header)} FROM {alimony_adjustment_table_name};"
                        cur.execute(current_query)
                        lite_alimony_adjustment_file_list = cur.fetchall()
                        lite_alimony_adjustment_file_dict = {}
                        for lite_alimony_adjustment_file_list_element in lite_alimony_adjustment_file_list:
                            lite_alimony_adjustment_file_dict[str(lite_alimony_adjustment_file_list_element[0]) + str(lite_alimony_adjustment_file_list_element[4]) + str(float(lite_alimony_adjustment_file_list_element[8]))] = [lite_alimony_adjustment_file_list_element[0], lite_alimony_adjustment_file_list_element[3], lite_alimony_adjustment_file_list_element[4], lite_alimony_adjustment_file_list_element[5], lite_alimony_adjustment_file_list_element[8]]

                        lite_alimony_adjustment_file_list = [] 

                        # Postgres banks, bank dict
                        current_query = f'SELECT l10n_mx_edi_code, name, id FROM res_bank WHERE l10n_mx_edi_code IS NOT NULL AND LENGTH(TRIM(l10n_mx_edi_code)) > 1;'
                        self.env.cr.execute(current_query)
                        postgres_employee_bank_list = self.env.cr.fetchall()
                        postgres_employee_bank_dict = {}
                        for postgres_employee_bank_element in postgres_employee_bank_list:
                            postgres_employee_bank_dict[postgres_employee_bank_element[0]] = {'name' : postgres_employee_bank_element[1], 'id' : postgres_employee_bank_element[2]}

                        current_query = f"SELECT new_bank_key, GROUP_CONCAT(new_check_number, ',') new_check_number_list FROM {alimony_adjustment_table_name} GROUP BY new_bank_key";
                        cur.execute(current_query)

                        # Postgres payment methods, payment method dict
                        current_query = f'SELECT name, code, id FROM l10n_mx_edi_payment_method;'
                        self.env.cr.execute(current_query)
                        postgres_employee_payment_method_list = self.env.cr.fetchall()
                        postgres_employee_payment_method_dict = {}
                        for postgres_employee_payment_method_list_element in postgres_employee_payment_method_list:
                            postgres_employee_payment_method_dict[postgres_employee_payment_method_list_element[0]] = {'code' : postgres_employee_payment_method_list_element[1], 'id' : postgres_employee_payment_method_list_element[2]}

                        lite_alimony_adjustment_new_bank_new_check_list = cur.fetchall()
                        lite_alimony_adjustment_new_bank_new_check_dict = {}
                        for lite_alimony_adjustment_new_bank_new_check_list_element in lite_alimony_adjustment_new_bank_new_check_list:
                            lite_alimony_adjustment_new_bank_new_check_dict[lite_alimony_adjustment_new_bank_new_check_list_element[0]] = lite_alimony_adjustment_new_bank_new_check_list_element[1].split(',')


                        current_query = """
                            SELECT lpp.id FROM pension_payment_line lpp
                            INNER JOIN employee_payroll_file fpe ON lpp.payroll_id = fpe.id
                            INNER JOIN custom_payroll_processing ppc ON fpe.payroll_processing_id = ppc.id
                            WHERE ppc.id = %(payroll_process_id)s;
                        """
    
                        params = {'payroll_process_id': self.payroll_process_id.id}
                        self.env.cr.execute(current_query, params)

                        postgres_pension_payment_line_id_list = self.env.cr.fetchall()
                        postgres_pension_payment_line_id_list = [postgres_pension_payment_line_id_list_element[0] for postgres_pension_payment_line_id_list_element in postgres_pension_payment_line_id_list]
                        postgres_pension_payment_line_obj_list = self.env['pension.payment.line'].search([('id', 'in', postgres_pension_payment_line_id_list)])
                        postgres_pension_payment_line_obj_dict = {}

                        for postgres_pension_payment_line_obj_list_element in postgres_pension_payment_line_obj_list:
                            postgres_pension_payment_line_obj_dict[str(postgres_pension_payment_line_obj_list_element.partner_id.name) + str(postgres_pension_payment_line_obj_list_element.deposit_number if postgres_pension_payment_line_obj_list_element.deposit_number else postgres_pension_payment_line_obj_list_element.check_folio_id.folio) +  str(float(postgres_pension_payment_line_obj_list_element.total_pension))] = postgres_pension_payment_line_obj_list_element


                        # Get check list from Odoo database
                        postgres_employee_new_bank_id_list = [postgres_employee_bank_dict[lite_employee_new_bank_key_dict_element]['id'] for lite_employee_new_bank_key_dict_element in lite_alimony_adjustment_new_bank_new_check_dict]
                        current_query = """SELECT rb.l10n_mx_edi_code, STRING_AGG(concat(cl.folio, ',', cl.id), ';') 
                            FROM check_log cl INNER JOIN res_partner_bank rpb on cl.bank_account_id = rpb.id 
                            INNER JOIN res_bank rb on rpb.bank_id = rb.id where cl.status IN 
                            ('Available for printing') 
                            AND cl.general_status = 'available' AND rb.id IN ({}) group by rb.l10n_mx_edi_code""".format(', '.join('{}'.format(postgres_employee_new_bank_id_list_element) for postgres_employee_new_bank_id_list_element in postgres_employee_new_bank_id_list))
                        self.env.cr.execute(current_query)
                        postgres_employee_new_check_log_list = self.env.cr.fetchall()
                        postgres_employee_new_check_log_dict = {}

                        for postgres_employee_new_check_log_list_element in postgres_employee_new_check_log_list:
                            postgres_employee_new_check_log_dict[postgres_employee_new_check_log_list_element[0]] = dict((key, value) for key, value in [element.split(',') for element in postgres_employee_new_check_log_list_element[1].split(';')])

                        for lite_alimony_adjustment_new_bank_new_check_dict_element in lite_alimony_adjustment_new_bank_new_check_dict:
                            if lite_alimony_adjustment_new_bank_new_check_dict_element in postgres_employee_new_check_log_dict:
                                for internal_element in lite_alimony_adjustment_new_bank_new_check_dict[lite_alimony_adjustment_new_bank_new_check_dict_element]:
                                    if internal_element not in postgres_employee_new_check_log_dict[lite_alimony_adjustment_new_bank_new_check_dict_element]:
                                        if lite_alimony_adjustment_new_bank_new_check_dict_element not in data_not_found_dict['new_check']:
                                            data_not_found_dict['new_check'].update({lite_alimony_adjustment_new_bank_new_check_dict_element : [internal_element]})
                                        else:
                                            data_not_found_dict['new_check'][lite_alimony_adjustment_new_bank_new_check_dict_element].append(internal_element)
                            else:
                                data_not_found_dict['new_check'][lite_alimony_adjustment_new_bank_new_check_dict_element] = lite_alimony_adjustment_new_bank_new_check_dict[lite_alimony_adjustment_new_bank_new_check_dict_element]

                        for lite_alimony_adjustment_file_dict_element in lite_alimony_adjustment_file_dict:
                            if lite_alimony_adjustment_file_dict_element not in postgres_pension_payment_line_obj_dict:
                                data_not_found_dict['beneficiary'].append(lite_alimony_adjustment_file_dict[lite_alimony_adjustment_file_dict_element][0])

                        for lite_alimony_adjustment_new_bank_new_check_dict_element in lite_alimony_adjustment_new_bank_new_check_dict:
                            if lite_alimony_adjustment_new_bank_new_check_dict_element not in postgres_employee_bank_dict:
                                data_not_found_dict['new_bank'].append(lite_alimony_adjustment_new_bank_new_check_dict_element)


                        there_is_error = False
                        content = f'###################################################\nDATOS ERRÓNEOS AJUSTES PENSIÓN : {datetime.now()}\n##################################################\n\n\n'
                        if len(data_not_found_dict['beneficiary']) > 0:
                            content = content  + f"\n################################################################\nBeneficiarios no encontrados en nómina actual o con datos no coincidentes\n################################################################\n{data_not_found_dict['beneficiary']}" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['new_bank']) > 0:
                            content = content  + f"\n#######################################################\nBancos no encontrados en sistema\n#######################################################\n{data_not_found_dict['new_bank']}" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['new_check']) > 0:
                            content = content  + f"\n########################################################\nCheques no encontrados en sistema o con estado no válido\n########################################################\n{data_not_found_dict['new_check']}" + '\n\n\n'
                            there_is_error = True

                        if not there_is_error:
                            for lite_alimony_adjustment_file_dict_element in lite_alimony_adjustment_file_dict:
                                postgres_pension_payment_line_update_dict =  {'l10n_mx_edi_payment_method_id': postgres_employee_payment_method_dict['Cheque nominativo']['id'], 'bank_key' : lite_alimony_adjustment_file_dict[lite_alimony_adjustment_file_dict_element][1], 'bank_id' : postgres_employee_bank_dict[lite_alimony_adjustment_file_dict[lite_alimony_adjustment_file_dict_element][1]]['id'], 'deposit_number' : '', 'check_folio_id' : postgres_employee_new_check_log_dict[lite_alimony_adjustment_file_dict[lite_alimony_adjustment_file_dict_element][1]][lite_alimony_adjustment_file_dict[lite_alimony_adjustment_file_dict_element][3]], 'total_pension' : lite_alimony_adjustment_file_dict[lite_alimony_adjustment_file_dict_element][4]}
                                postgres_pension_payment_line_obj_dict[lite_alimony_adjustment_file_dict_element].write(postgres_pension_payment_line_update_dict)

                            return {
                                'name': 'Importación exitosa',
                                'type': 'ir.actions.act_window',
                                'view_mode': 'form',
                                'res_model': 'payroll.payment.message.wizard',
                                'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Importación exitosa de ajustes de pensión.'}).id,
                                'target': 'new'
                            }
                        else:
                            self.payroll_process_id.update_failed_file(content, 'payroll_adjustment', 'completed', self.env.user.id, self.filename)
                            return {
                                'name': 'Error en validación',
                                'type': 'ir.actions.act_window',
                                'view_mode': 'form',
                                'res_model': 'payroll.payment.message.wizard',
                                'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Se encontraron errores durante la validación del archivo de ajustes de pension, corriga los errores e intente nuevamente.'}).id,
                                'target': 'new'
                            }

                    else:
                        return {
                            'name': 'Error en columnas de archivo',
                            'type': 'ir.actions.act_window',
                            'view_mode': 'form',
                            'res_model': 'payroll.payment.message.wizard',
                            'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Número de columnas del archivo de ajustes de pensión alimenticia, no coincide con las columnas esperadas.'}).id,
                            'target': 'new'
                        }