from . import account_move
from . import money_funds
from . import money_fund_req
from . import signatories
from . import bank_reference
from . import register_mail_format
from . import res_users_dep_money_fund