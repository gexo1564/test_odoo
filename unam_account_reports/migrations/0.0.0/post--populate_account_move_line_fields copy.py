import logging
import itertools

from odoo import api, SUPERUSER_ID

_logger = logging.getLogger(__name__)

def migrate(cr, version):
    return
    _logger.info("Starting updating related fields for account move lines")

    query = """
            UPDATE account_move_line
            SET group_id = group
            FROM (
                SELECT aml.id as aml_id, aa.group_id as group
                FROM account_account aa
                INNER JOIN account_move_line aml ON aml.account_id = aa.id
                WHERE aml.account_id is not null
            ) AS aml_id
            WHERE account_move_line.id = aml_id;
        """

    cr.execute(query = """
            UPDATE account_move_line
            SET resource_origin_id = pc_resource_origin_id,
                expenditure_item_id = pc_item_id,
                program_code_dependency_id = pc_dependency_id,
                provider_id = aml_partner_id,
                unam_account_id = aml_account_id
            FROM (
                SELECT aml.id as aml_id, aml.partner_id as aml_partner_id, aml.account_id as aml_account_id, 
                        pc.resource_origin_id as pc_resource_origin_id, pc.item_id as pc_item_id, pc.dependency_id as pc_dependency_id
                FROM account_move_line aml
                INNER JOIN program_code pc ON aml.program_code_id = pc.id
                WHERE aml.program_code_id is not null
            ) AS aml_id
            WHERE account_move_line.id = aml_id;
        """)
        
    _logger.info("Finished updating related fields for account move lines")
