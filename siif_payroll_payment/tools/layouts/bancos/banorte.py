import io
import csv
from ..layouts import Layout, ResultLine

class LayoutBanorte(Layout):

    def load_file(self, file_data):
        # io.StringIO(file_data.decode("utf-8"))
        self.file = io.StringIO(file_data.decode("ISO-8859-1")) #open(file, encoding="ISO-8859-1")

    def load_body(self):
        return self.file.readlines()[1:]

    def close_file(self):
        self.file.close()

    def read_line(self, line):
        amount = float("%s.%s" % (line[99:113], line[113:115]))
        beneficiary = line[9:20]
        account_bank = line[119:138]
        status = line[7]
        ok = status == "APLICADO"
        return ResultLine(ok, "pension", {'beneficiary': beneficiary, 'account_bank': account_bank, 'amount': amount}, status)

    def get_ids_search_employee_payroll_file(self, payroll_processing_id: int, lines: list):
        payment_method_id = self.env.ref('l10n_mx_edi.payment_method_transferencia').id
        query = """
            select lpad(b.acc_number, 18, '0') account, p.id partner_id
            from res_partner_bank b, res_partner p, pension_payment_line l, employee_payroll_file r
            where p.id = b.partner_id
                and l.partner_id = p.id
                and l.payroll_id = r.id
                and r.payroll_processing_id = %s
                and beneficiary_type = 'alimony'
                and l.l10n_mx_edi_payment_method_id = %s
        """
        self.env.cr.execute(query, (payroll_processing_id, payment_method_id))
        result_set = {r[0]: r[1:] for r in self.env.cr.fetchall()}
        lines_ok, lines_err = [], []
        for line in lines:
            if line.payment_type == "pension":
                # Busqueda para PENSION
                account_bank = line.columns.get('account_bank')
                res = result_set.get(account_bank, None)
                if res:
                    line.res_id = res[0]
                    lines_ok.append(line)
                else:
                    line.res_id = None
                    line.error_message = "No se encontró un registro la cuenta %s" % (
                        account_bank
                    )
                    lines_err.append(line)
            else:
                line.error_message = "No es pago de pensión."
                lines_err.append(line)
        return lines_ok, lines_err

    def get_ids_search_account_payment(self, batch_folio: str, lines: list):
        # Se obtienen los ids que hacen match con el criterio de búsqueda
        query = """
            select lpad(a.acc_number, 18, '0') bank_account, p.id, p.amount
            from account_payment p, res_partner_bank a
            where p.payment_bank_account_id = a.id
            and batch_folio = %s
        """
        self.env.cr.execute(query, (batch_folio,))
        result_set = {r[0]: r[1:] for r in self.env.cr.fetchall()}
        lines_ok, lines_err = [], []
        for line in lines:
            beneficiary = line.columns.get('beneficiary')
            res = result_set.get(line.columns.get('bank_account'), None)
            if res:
                if res[1] == line.columns.get('amount'):
                    line.res_id = res[0]
                    lines_ok.append(line)
                else:
                    line.res_id = None
                    line.error_message = f"El monto del pago ({line.columns.get('amount'):,.2f}) del beneficiario {beneficiary} no coincide con el registrado ({res[1]:,.2f})."
                    lines_err.append(line)
            else:
                line.res_id = None
                line.error_message = f"No se encontró un registro para el beneficiario {beneficiary}"
                lines_err.append(line)
        return lines_ok, lines_err