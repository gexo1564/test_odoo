# -*- coding: utf-8 -*-
from odoo import models, fields, api, _



class HandbookVideoBase(models.Model):

    _name = 'siif.handbook.video'
    _description='Videos de Manuales/tutoriales'

    name = fields.Char(String="Alias", required=True )
    video = fields.Text(string="Video", help="Ponga el link de video", required=True)
    