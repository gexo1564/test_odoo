# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import logging
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

from odoo.addons.siif_security.tools.utils import check_authorization, check_only_allowed_fields
from datetime import datetime
class SubDependency(models.Model):

    _name = 'sub.dependency'
    _description = 'Sub-Dependency'
    _rec_name = 'sub_dependency'

    dependency_id = fields.Many2one('dependency', string='Dependency')
    sub_dependency = fields.Char(string='Sub dependency', size=2)
    description = fields.Text(string='Sub dependency description')
    active = fields.Boolean('Active', default=True)

    allowed_users_cxp = fields.Many2many('res.users',  'allowed_users_sd_cxp_rel', string ='Allowed users')

    _sql_constraints = [('sub_dependency_dependency_id', 'unique(sub_dependency,dependency_id, start_date, end_date)',
                         'The sub dependency must be unique per dependency')]

    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')

    @api.constrains('start_date', 'end_date')
    def check_end_date(self):
        if not self.start_date:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("Configure la fecha inicial"))
            else:
                raise ValidationError(_("The start date of validity is required"))
        if self.end_date and self.end_date <= self.start_date:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("La fecha de fin de vigencia no puede ser menor o igual a la fecha de inicio de vigencia"))
            else:
                raise ValidationError(_("The effective end date cannot be less than or equal to the effective date"))


    def name_get(self):
        result = []
        for rec in self:
            name = rec.sub_dependency or ''
            if rec.description and self.env.context and (self.env.context.get('show_for_supplier_payment',False) or
                        self.env.context.get('show_for_agreement',False)): 
                name += ' ' + rec.description
            result.append((rec.id, name))
        return result

    @api.constrains('sub_dependency')
    def _check_sub_dependency(self):
        if not str(self.sub_dependency).isnumeric():
            raise ValidationError(_('The Sub Dependency must be numeric value'))

    def get_sub_dep_records(self,sub_dep,dependency_id):
        sub_id = self.env['sub.dependency'].search([('dependency_id','=',dependency_id),('sub_dependency','=',sub_dep)],limit=1)
        if sub_id:
            return sub_id.id
        else:
            return False
        
    def fill_zero(self, code):
        return str(code).zfill(2)

    @api.model
    def create(self, vals):
        if vals.get('sub_dependency') and len(vals.get('sub_dependency')) != 2:
            vals['sub_dependency'] = self.fill_zero(vals.get('sub_dependency'))
        res = super(SubDependency, self).create(vals)
        base_sd = self.env['base.sub.dependency'].sudo()
        already_exists = base_sd.search([('sub_dependency', '=', res.sub_dependency)], limit=1)
        if not already_exists:
            base_sd.create({'sub_dependency' : res.sub_dependency})
        return res

    @check_authorization(
        'jt_budget_mgmt.group_budget_catalogs_admin_user',
        'jt_budget_mgmt.group_budget_admin_user'
    )
    @check_only_allowed_fields('description', 'start_date', 'end_date')
    def write(self, vals):
        res = super(SubDependency, self).write(vals)
        if res == True:
            return res
        base_sd = self.env['base.sub.dependency'].sudo()
        already_exists = base_sd.search([('sub_dependency', '=', res.sub_dependency)], limit=1)
        if not already_exists:
            base_sd.create({'sub_dependency' : res.sub_dependency})
        return res

    @check_authorization(
        'jt_budget_mgmt.group_budget_catalogs_admin_user',
        'jt_budget_mgmt.group_budget_admin_user'
    )
    def unlink(self):
        for subdependency in self:
            program_code = self.env['program.code'].search([('sub_dependency_id', '=', subdependency.id)], limit=1)
            if program_code:
                raise ValidationError('No puedes eliminar una Subdependencia que este ligado a un Código Programático!')
        return super(SubDependency, self).unlink()

    def validate_subdependency(self, subdependency_string, dependency):
        if len(str(subdependency_string)) > 1:
            subdependency_str = str(subdependency_string).zfill(2)
            if subdependency_str.isnumeric():
                subdependency = self.search(
                    [('dependency_id', '=', dependency.id), ('sub_dependency', '=', subdependency_string)], limit=1)
                if subdependency:
                    return subdependency
        return False
    
    # if not return value is not valid #
    def get_validity_sub_dependency(self, dependency, sub_dependency, today = datetime.now()):
        query = """
                SELECT sd.id
                FROM sub_dependency sd
                JOIN dependency d on sd.dependency_id = d.id
                WHERE 
                    (
                        (sd.start_date IS NULL AND sd.end_date IS NULL AND d.dependency  = %(dependency)s  AND sd.sub_dependency  = %(sub_dependency)s) OR
                        (sd.start_date IS NOT NULL AND sd.end_date IS NULL AND d.dependency  = %(dependency)s  AND sd.sub_dependency  = %(sub_dependency)s AND sd.start_date <= %(today)s) OR
                        (sd.start_date IS NOT NULL AND sd.end_date IS NOT NULL AND d.dependency  = %(dependency)s  AND sd.sub_dependency  = %(sub_dependency)s AND sd.start_date <= %(today)s AND sd.end_date >= %(today)s)
                    )
                """
        params = {'dependency': str(dependency),
                  'sub_dependency': str(sub_dependency),
                  'today': today}
        
        self.env.cr.execute(query, params)
        sub_dependency_validity = self.env.cr.fetchall()

        return sub_dependency_validity
