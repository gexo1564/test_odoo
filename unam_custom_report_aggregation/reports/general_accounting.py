# -*- coding: utf-8 -*-
import io
import base64
from datetime import datetime
from odoo import models, api, fields, _
from odoo.tools.misc import xlsxwriter

class AccountChartOfAccountReport(models.AbstractModel):
    _inherit = 'jt_account_module_design.account_coa.inherit'

    padding_cols = [{}, {}, {}, {}]



    @api.model
    def _get_general_ledger_lines(self, options, line_id=None):
        lines = []
        aml_lines = []
        options_list = self._get_options_periods_list(options)
        unfold_all = options.get('unfold_all') or (self._context.get('print_mode') and not options['unfolded_lines'])
        date_from = fields.Date.from_string(options['date']['date_from'])
        date_to = fields.Date.from_string(options['date']['date_to'])
        cur_period = [[date_from, date_to]]
        
        company_currency = self.env.company.currency_id

        expanded_account = line_id and self.env['account.account'].browse(int(line_id[8:]))
        accounts_results, taxes_results = self._do_query(options_list, expanded_account=expanded_account)

        total_debit = total_credit = total_balance = 0.0
        for account, periods_results in accounts_results:

            # No comparison allowed in the General Ledger. Then, take only the first period.
            results = periods_results[0]
            is_unfolded = 'account_%s' % account.id in options['unfolded_lines']

            # account.move.line record lines.
            amls = results.get('lines', [])

            # account.account record line.
            account_sum = results.get('sum', {})
            account_un_earn = results.get('unaffected_earnings', {})

            # Check if there is sub-lines for the current period.
            max_date = account_sum.get('max_date')
            has_lines = max_date and max_date >= date_from or False

            amount_currency = account_sum.get('amount_currency', 0.0) + account_un_earn.get('amount_currency', 0.0)
            debit = account_sum.get('debit', 0.0) + account_un_earn.get('debit', 0.0)
            credit = account_sum.get('credit', 0.0) + account_un_earn.get('credit', 0.0)
            balance = account_sum.get('balance', 0.0) + account_un_earn.get('balance', 0.0)

            lines.append(self._get_account_title_line(options, account, amount_currency, debit, credit, balance, has_lines))

            total_debit += debit
            total_credit += credit
            total_balance += balance

            if has_lines and (unfold_all or is_unfolded):
                '''
                Initial Balance Line
                '''
                account_init_bal = results.get('initial_balance', {})
                cumulated_balance = account_init_bal.get('balance', 0.0) + account_un_earn.get('balance', 0.0)

                init_bal_line = self._get_initial_balance_line(
                    options, account,
                     str(account_init_bal.get('amount_currency', 0.0)) + str(account_un_earn.get('amount_currency', 0.0)),
                    account_init_bal.get('debit', 0.0) + account_un_earn.get('debit', 0.0),
                    account_init_bal.get('credit', 0.0) + account_un_earn.get('credit', 0.0),
                    cumulated_balance,
                )
                init_bal_line['level'] = 2
                lines.append(init_bal_line)

                '''
                Dependency and Subdependency Lines
                '''
                dep_balance_opts = {
                    'posted_only': True,
                    'init_balance': False,
                    'subdep_balance': True
                }
                account_dep_balances = self.env['dependency'].get_account_balances(account, cur_period, dep_balance_opts)
                for dep, dep_balance in account_dep_balances:

                    subdep_lines = []

                    for subdep in dep_balance.get('subdeps', []):
                        def get_matching_amls(aml, dep, subdep):
                            dep_id = dep.id if dep.id else None
                            subdep_id = subdep.id if subdep.id else None
                            return aml.get('dependency_id') == dep_id and aml.get('sub_dependency_id') == subdep_id

                        subdep_amls = list(filter(lambda aml: get_matching_amls(aml, dep, subdep[-1].get('subdep')), amls))

                        aml_lines = []
                        for aml in subdep_amls:
                            cumulated_balance += aml['balance']
                            aml_line = self._get_aml_line(options, account, aml, company_currency.round(cumulated_balance))
                            aml_line['level'] = 5
                            subdep_str = str(subdep[-1].get('subdep').id) if subdep[-1].get('subdep') else 'None'
                            dep_str = str(dep.id) if dep.id else 'None'
                            aml_line['parent_id'] = 'subdep_' + subdep_str  + '_' + dep_str + '_' + str(account.id)
                            aml_lines.append(aml_line)

                        subdep_line = self.format_line(acc=account, dep=dep, subdep=subdep[-1].get('subdep'), children=subdep_amls, cols=self.padding_cols + [
                            self._format({'name': subdep[-1]['debit']}, figure_type='float'),
                            self._format({'name': subdep[-1]['credit']}, figure_type='float'),
                            self._format({'name': cumulated_balance}, figure_type='float'),
                        ], extra_info={
                            'level': 4,
                            'unfolded': True
                        })
                        subdep_lines.append(subdep_line)
                        subdep_lines += aml_lines

                    dep_line = self.format_line(acc=account, dep=dep, children=dep_balance.get('subdeps'), cols=self.padding_cols + [
                            self._format({'name': dep_balance.get('deps')[-1]['debit']}, figure_type='float'),
                            self._format({'name': dep_balance.get('deps')[-1]['credit']}, figure_type='float'),
                            self._format({'name': cumulated_balance}, figure_type='float'),
                        ], extra_info={
                            'level': 3,
                            'unfolded': True
                    })
                    lines.append(dep_line)
                    lines += subdep_lines
                '''
                If the account isn't grouped by dependency/subdependency then
                just proceed as usual and list all of the aml's
                '''
                if not account.dep_subdep_flag:

                    # account.move.line record lines.
                    amls = results.get('lines', [])

                    load_more_remaining = len(amls)
                    load_more_counter = self._context.get('print_mode') and load_more_remaining or self.MAX_LINES

                    for aml in amls:
                        # Don't show more line than load_more_counter.
                        if load_more_counter == 0:
                            break

                        cumulated_balance += aml['balance']
                        lines.append(self._get_aml_line(options, account, aml, company_currency.round(cumulated_balance)))

                        load_more_remaining -= 1
                        load_more_counter -= 1
                        aml_lines.append(aml['id'])

                    if load_more_remaining > 0:
                        # Load more line.
                        lines.append(self._get_load_more_line(
                            options, account,
                            self.MAX_LINES,
                            load_more_remaining,
                            cumulated_balance,
                        ))

                '''
                Account Total Line
                '''
                lines.append(self._get_account_total_line(
                    options, account,
                    account_sum.get('amount_currency', 0.0),
                    account_sum.get('debit', 0.0),
                    account_sum.get('credit', 0.0),
                    account_sum.get('balance', 0.0),
                ))

        if not line_id:
            # Report total line.
            total_line = self._get_total_line(
                options,
                total_debit,
                total_credit,
                company_currency.round(total_balance),
            )
            total_line['level'] = 3
            lines.append(total_line)

            # Tax Declaration lines.
            journal_options = self._get_options_journals(options)
            if len(journal_options) == 1 and journal_options[0]['type'] in ('sale', 'purchase'):
                lines += self._get_tax_declaration_lines(
                    options, journal_options[0]['type'], taxes_results
                )
        if self.env.context.get('aml_only'):
            return aml_lines
        return lines


    def get_xlsx(self, options, response=None):
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        sheet = workbook.add_worksheet(self._get_report_name()[:31])
        date_default_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2, 'num_format': 'yyyy-mm-dd'})
        date_default_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': 'yyyy-mm-dd'})
        default_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2})
        default_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'})
        title_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'bottom': 2})
        super_col_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'align': 'center'})
        level_0_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 13, 'bottom': 6, 'font_color': '#666666'})
        level_1_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 13, 'bottom': 1, 'font_color': '#666666'})
        level_2_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 1})
        level_2_col1_total_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666'})
        level_2_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666'})
        level_3_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2})
        level_3_col1_total_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 1})
        level_3_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'})
        currect_date_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'align': 'right'})
        currect_date_style.set_border(0)
        super_col_style.set_border(0)
        # Set the first column width to 50
        sheet.set_column(0, 0, 20)
        sheet.set_column(1, 1, 17)
        sheet.set_column(2, 2, 20)
        sheet.set_column(3, 3, 15)
        sheet.set_column(4, 4, 10)
        sheet.set_column(5, 5, 15)
        sheet.set_column(6, 6, 12)
        super_columns = self._get_super_columns(options)
        y_offset = 0
        col = 0
        sheet.merge_range(y_offset, col, 6, col, '', super_col_style)
        if self.env.user and self.env.user.company_id and self.env.user.company_id.header_logo:
            filename = 'logo.png'
            image_data = io.BytesIO(base64.standard_b64decode(
                self.env.user.company_id.header_logo))
            sheet.insert_image(0, 0, filename, {
                               'image_data': image_data, 'x_offset': 8, 'y_offset': 3, 'x_scale': 0.6, 'y_scale': 0.6})
        col += 1
        start = datetime.strptime(str(options['date'].get('date_from')), '%Y-%m-%d').date()
        end = datetime.strptime(str(options['date'].get('date_to')), '%Y-%m-%d').date()
        start_date = start.strftime('%B %d')
        s_year = start.strftime('%Y')
        end_date = end.strftime('%B %d')
        e_year = end.strftime('%Y')
        start_month_name = start.strftime("%B")
        end_month_name = end.strftime("%B")
        
        if self.env.user.lang == 'es_MX':
            start_month_name = self.get_month_name(start.month)
            end_month_name = self.get_month_name(end.month)

        header_date = str(start.day).zfill(2) + " " + start_month_name+" DE "+str(start.year)
        header_date += " AL "+str(end.day).zfill(2) + " " + end_month_name +" DE "+str(end.year)
        

        header_title = '''UNIVERSIDAD NACIONAL AUTÓNOMA DE MÉXICO\nDIRECCIÓN GENERAL DE CONTROL PRESUPUESTAL-CONTADURÍA GENERAL
CONTABILIDAD GENERAL DEL %s \nCifras en Pesos''' % (header_date)
        sheet.merge_range(y_offset, col, 5, col + 6,
                          header_title, super_col_style)
        y_offset += 6
        col = 1
        currect_time_msg = "Fecha y hora de impresión: "
        currect_time_msg += datetime.today().strftime('%d/%m/%Y %H:%M')
        sheet.merge_range(y_offset, col, y_offset, col + 6,
                          currect_time_msg, currect_date_style)
        y_offset += 1
        for row in self.get_header(options):
            x = 0
            for column in row:
                colspan = column.get('colspan', 1)
                header_label = column.get('name', '').replace(
                    '<br/>', ' ').replace('&nbsp;', ' ')
                if colspan == 1:
                    sheet.write(y_offset, x, header_label, title_style)
                else:
                    sheet.merge_range(y_offset, x, y_offset,
                                      x + colspan - 1, header_label, title_style)
                x += colspan
            y_offset += 1
        ctx = self._set_context(options)
        ctx.update({'no_format': True, 'print_mode': True,
                    'prefetch_fields': False})
        # deactivating the prefetching saves ~35% on get_lines running time
        lines = self.with_context(ctx)._get_lines(options)
        if options.get('hierarchy'):
            lines = self._create_hierarchy(lines, options)
        if options.get('selected_column'):
            lines = self._sort_lines(lines, options)
        # write all data rows
        for y in range(0, len(lines)):
            level = lines[y].get('level', 1)
            if lines[y].get('caret_options'):
                style = level_3_style
                col1_style = level_3_col1_style
            elif level == 0:
                y_offset += 1
                style = level_0_style
                col1_style = style
            elif level == 1:
                style = level_1_style
                col1_style = style
            elif level == 2:
                style = level_2_style
                col1_style = 'total' in lines[y].get('class', '').split(
                    ' ') and level_2_col1_total_style or level_2_col1_style
            elif level == 3:
                style = level_3_style
                col1_style = 'total' in lines[y].get('class', '').split(
                    ' ') and level_3_col1_total_style or level_3_col1_style
            else:
                style = default_style
                col1_style = default_col1_style
            # write the first column, with a specific style to manage the
            # indentation
            cell_type, cell_value = self._get_cell_type_value(lines[y])

            '''
            Prepend the account name with a certain number of spaces 
            depending on it's level attribute
            '''
            row_spacing = '  ' * level

            if cell_type == 'date':
                sheet.write_datetime(
                    y + y_offset, 0, row_spacing + str(cell_value), date_default_col1_style)
            else:
                sheet.write(y + y_offset, 0, row_spacing + str(cell_value), col1_style)
            # write all the remaining cells
            for x in range(1, len(lines[y]['columns']) + 1):
                cell_type, cell_value = self._get_cell_type_value(
                    lines[y]['columns'][x - 1])
                if cell_type == 'date':
                    sheet.write_datetime(
                        y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, date_default_style)
                else:
                    sheet.write(
                        y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, style)
        workbook.close()
        output.seek(0)
        generated_file = output.read()
        output.close()
        return generated_file


    @api.model
    def _get_columns_name(self, options):
        '''
        (Overridden) Copied over to be able to apply translation names as
        requested by the client
        '''
        return [
            {'name': ''},
            {'name': _('Date'), 'class': 'date'},
            {'name': _('Communication')},
            {'name': _('Partner')},
            {'name': _('Currency'), 'class': 'number'},
            {'name': _('Debit'), 'class': 'number'},
            {'name': _('Credit'), 'class': 'number'},
            {'name': _('Balance'), 'class': 'number'}
        ]

    @api.model
    def _get_query_amls(self, options, expanded_account, offset=None, limit=None):
        '''
        (Overridden) Overridden to get the dependency and subdependency fields
        from the account move lines
        '''
        ''' Construct a query retrieving the account.move.lines when expanding a report line with or without the load
        more.
        :param options:             The report options.
        :param expanded_account:    The account.account record corresponding to the expanded line.
        :param offset:              The offset of the query (used by the load more).
        :param limit:               The limit of the query (used by the load more).
        :return:                    (query, params)
        '''

        unfold_all = options.get('unfold_all') or (self._context.get('print_mode') and not options['unfolded_lines'])

        # Get sums for the account move lines.
        # period: [('date' <= options['date_to']), ('date', '>=', options['date_from'])]
        if expanded_account:
            domain = [('account_id', '=', expanded_account.id)]
        elif unfold_all:
            domain = []
        elif options['unfolded_lines']:
            domain = [('account_id', 'in', [int(line[8:]) for line in options['unfolded_lines']])]

        new_options = self._force_strict_range(options)
        tables, where_clause, where_params = self._query_get(new_options, domain=domain)
        ct_query = self._get_query_currency_table(options)
        query = '''
            SELECT
                account_move_line.id,
                account_move_line.date,
                account_move_line.date_maturity,
                account_move_line.name,
                account_move_line.dependency_id,
                account_move_line.sub_dependency_id,
                account_move_line.ref,
                account_move_line.company_id,
                account_move_line.account_id,
                account_move_line.payment_id,
                account_move_line.partner_id,
                account_move_line.currency_id,
                account_move_line.amount_currency,
                ROUND(account_move_line.debit * currency_table.rate, currency_table.precision)   AS debit,
                ROUND(account_move_line.credit * currency_table.rate, currency_table.precision)  AS credit,
                ROUND(account_move_line.balance * currency_table.rate, currency_table.precision) AS balance,
                account_move_line__move_id.name         AS move_name,
                company.currency_id                     AS company_currency_id,
                partner.name                            AS partner_name,
                account_move_line__move_id.type         AS move_type,
                account.code                            AS account_code,
                account.name                            AS account_name,
                journal.code                            AS journal_code,
                journal.name                            AS journal_name,
                full_rec.name                           AS full_rec_name
            FROM account_move_line
            LEFT JOIN account_move account_move_line__move_id ON account_move_line__move_id.id = account_move_line.move_id
            LEFT JOIN %s ON currency_table.company_id = account_move_line.company_id
            LEFT JOIN res_company company               ON company.id = account_move_line.company_id
            LEFT JOIN res_partner partner               ON partner.id = account_move_line.partner_id
            LEFT JOIN account_account account           ON account.id = account_move_line.account_id
            LEFT JOIN account_journal journal           ON journal.id = account_move_line.journal_id
            LEFT JOIN account_full_reconcile full_rec   ON full_rec.id = account_move_line.full_reconcile_id
            WHERE %s
            ORDER BY account_move_line.date, account_move_line.id
        ''' % (ct_query, where_clause)

        if offset:
            query += ' OFFSET %s '
            where_params.append(offset)
        if limit:
            query += ' LIMIT %s '
            where_params.append(limit)

        return query, where_params