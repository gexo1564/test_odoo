# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields, api
from lxml import etree

class StatementOfCash(models.Model):
    _name = 'cash.statement'
    _description = 'Statement of Cash'
    _rec_name = 'concept'

    concept = fields.Char(string='Concepto')
    major = fields.Selection([('operating_activity','Cash Flows from Operating Activities'),('invest_activity','Cash Flows from Investing Activities'),('finance_activity','Cash Flow from Financing Activities')],string='Major')
    types = fields.Selection([('option','Options'),('origin','Origin')],string='Type')
    parent_id = fields.Many2one('cash.statement', string='Parent')
    coa_conac_ids = fields.Many2many('coa.conac', string="CODE CONAC")

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super().fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])
        is_admin_contabilidad = self.env.user.has_group('jt_account_base.group_account_admin_user')
        is_training_contabilidad = self.env.user.has_group('jt_account_base.group_account_training_user')
        if not (is_admin_contabilidad or is_training_contabilidad):
            for node in doc.xpath("//" + view_type):
                # Ocultar opcion de exportar de las vistas Odoo
                node.set('export', 'false')
        res['arch'] = etree.tostring(doc)
        return res