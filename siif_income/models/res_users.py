# -*- coding: utf-8 -*-
import logging
from odoo import models, fields, api, _

class ResUsers(models.Model):
    _inherit = 'res.users'

    dependency_income_ids = fields.Many2many(
        'dependency',
        'allowed_users_dep_income_rel',
        string = "User Permissions for Dependencies"
    )

    sub_dependency_income_ids = fields.Many2many(
        'sub.dependency',
        'allowed_users_sd_income_rel',
        string = "User Permissions for Sub Dependencies"
    )

    all_dep_subdep_income = fields.Boolean(string='All Dependencies selected')
    len_perm_assigned_income = fields.Integer(compute='_get_size_permissions_income',string="Permissions Assigned",store=False)

    def _get_size_permissions_income(self):
        for info in self:
            len_dep=len(info.dependency_income_ids)
            len_subdep=len(info.sub_dependency_income_ids)
            info.len_perm_assigned_income=len_dep+len_subdep

    #Asignación de Todos las Dependencias
    @api.onchange('all_dep_subdep_income')
    def _all_deps_selected_income(self):

        if (self.all_dep_subdep_income==True):
            #logging.critical("Boton True")  
            dependency_ids = self.env['dependency'].search([])
            #logging.critical(type(dependency_ids))
            for user in self:
                user.dependency_income_ids=dependency_ids
        else:
            for user in self:
                if(len(user.dependency_income_ids)>=1):
                    user.dependency_income_ids=[(6, 0, [])]
    
    ### Sobrescritura de este método ###
    @api.model
    def _is_system(self):
        self.ensure_one()
        return self.has_group('base.group_system') or self._is_donation_user()

    # Usuarios de Ingresos/donaciones
    def _is_donation_user(self):
        donation_groups = ['jt_income.group_income_admin_user', 
                        'jt_income.group_income_finance_user']
    
        return any(self.env.user.has_group(group) for group in donation_groups)
                  