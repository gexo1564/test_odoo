import logging
from odoo.tests import common
from odoo.exceptions import UserError, ValidationError
from odoo.tests import common
from odoo.exceptions import ValidationError
import base64
from datetime import date

"""Modelos"""
ACC_JOURNAL='account.journal'
class TestMoneyFundsRequest(common.TransactionCase):
    def setUp(self):
        super().setUp()
        self.account_account = self.env['account.account'].search([('code','=','110.002.001')])
        self.journal = self.env[ACC_JOURNAL].search([('name','=','BBVA BANCOMER-8141 (BBV01)')])
        self.currency           = self.env['res.currency'].search([('name','=','MXN')])
        self.user               = self.env['res.partner'].search([('password_beneficiary','=','N1420924')])
        self.dep                = self.env['dependency'].create({
            'dependency':'999',
            'description':"Dependencia Pruebas",
            'sort_description':"Test",
        })
        self.sub_dep            = self.env['sub.dependency'].create({
            'dependency_id':self.dep.id,
            'sub_dependency':'01',
            'description':'Subdependencia Prueba'
        })
        self.name="Usuario"
        self.emails = "a@mail.com"
    
        self.diario = self.env[ACC_JOURNAL].create({
            'name':'Diario Fondo',
            'type':'cash',
            'company_id':self.env['res.company'].search([('name','=','Universidad Nacional Autónoma de México')],limit=1).id,
            'code':'CCṔ01',
        })

        self.cuenta_min = self.env['account.account'].search([('name','=','CUENTAS POR COBRAR CFDIS')])



    def test_create_money_fund_request(self):
        ministration=self.env['money.funds.request.ministration']
        file_aux = open('siif_currency_purchase_req/static/tests/file_pdf.pdf',"rb")
        out_aux = file_aux.read()
        file_aux.close()
        files = base64.b64encode(out_aux)
        qr = open('siif_currency_purchase_req/static/tests/QR.png',"rb")
        out_aux = qr.read()
        qr.close()
        qr_img = base64.b64encode(out_aux)
        #Titulares
        vals={
            'name':"Usuario 1",
            'email':'a@mail.com',
        }
        titular=self.env['titulars'].create(vals)

        #Realizar solicitud
        vals={
            'dependency_id':self.dep.id,
            'sub_dependency_id':self.sub_dep.id,
            'is_new_fund':True,
            'debtor_account': self.account_account.id,
            'currency_id': self.currency.id,
            'new_fund': 20000,
            'name_proxy':self.name,
            'email_proxy':self.emails,
            'name_owner':self.name,
            'email_owner':self.emails,
            'name_secretary':self.name,
            'email_secretary':self.emails,
            'name_third':self.name,
            'email_third':self.emails,

            'ine_doc_proxy':files,
            'address_doc_proxy':files,
            'nombramiento_doc_proxy':files,
            'ine_doc_owner':files,
            'address_doc_owner':files,
            'nombramiento_doc_owner':files,
            'ine_doc_secretary':files,
            'address_doc_secretary':files,
            'nombramiento_doc_secretary':files,
            'ine_doc_third':files,
            'address_doc_third':files,

            'dependency_mail':'dep@mail.com',
            'dependency_address':'CU',
            'titulars_id':titular,
            'no_titulars':len(titular),
            
        }
        
        money_fund=self.env['money.funds'].create(vals)
        #Se revisa si se asigna el nombre con la moneda asignada
        self.assertEqual(money_fund.name,'Fondo Fijo-999-01-1')

        #Se revisa si se crea una solicitud de compra
        request = self.env['money.funds.request.increment'].search([('dependency_id','=',self.dep.id),('sub_dependency_id','=',self.sub_dep.id)])
        logging.critical(request)
        self.assertEqual(request.actual_fund,0)
        self.assertEqual(request.amount_request,20000)
        self.assertEqual(request.move_type,'adding')

        #====================================================================
        #--------------Asignaciónes
        #====================================================================
        with self.assertRaises(ValidationError): 
            request.action_for_publish()
        
        #Publicar
        archivo=self.env['ir.attachment'].create({
				'name':"Archivo prueba",
				'res_model':"money.funds.request.increment",
				'company_id':1,
                'store_fname':files,
				})
        request.update({'justify_files':archivo})
        request.action_for_publish()
        self.assertEqual(request.state,'published')

        #Revisar observaciones
        request.action_for_observations()
        self.assertEqual(request.state,'published')
        self.assertEqual(request.is_published,True)
        self.assertNotEqual(request.observations,'')

        #Mandar a revisión
        request.action_for_revised()
        self.assertEqual(request.state,'revised')

        #Aprobar
        request.action_for_approve()
        self.assertEqual(request.state,'approved')
        self.assertEqual(request.name,'FF-INC-'+str(date.today().year)+'-999-01-1')
        self.assertEqual(request.money_fund.bank_account_journal.id,False)
        self.assertEqual(request.money_fund.account_journal.id,False)
        #Si es aprobado por la diferencia
        with self.assertRaises(ValidationError): 
            request.action_for_accept_dep()
        
        self.assertIsNotNone(request.money_fund.request_account.id)

        request.money_fund.request_account.generate_modification_request()

        #Asignación del diario y la cuenta contable asociada a la cuenta
        request.money_fund.update({
            'account_journal':self.diario.id,
            'bank_account_journal':self.journal.id})
        with self.assertRaises(ValidationError):
            request.action_for_accept_dep()

        #Asignación de la firma electrónica dependencia
        request.update({'qr_signature_dep':qr_img})
        request.action_for_accept_dep()

        with self.assertRaises(ValidationError):
            request.action_for_autho_dfg()
        #Asignación de la firma electrónica DGF
        request.update({'qr_signature_dgf':qr_img})
        request.action_for_autho_dfg()
        self.assertIsNotNone(request.transfer_request.id)

        #Solicitud de transferencia
        
        with self.assertRaises(ValidationError):
            request.transfer_request.request_finance()
        
        request.transfer_request.update({'bank_account_id':request.transfer_request.desti_bank_account_id.id})
        request.transfer_request.request_finance()
        request.transfer_request.approve_finance()
        request.transfer_request.action_schedule_transfers()
        request.transfer_request.payment_ids.post()

        self.assertEqual(request.state,'with_funds')
        self.assertEqual(request.money_fund.import_fund,20000)
        self.assertEqual(request.money_fund.state,'on_use')

        #=========================================================================
        #--------------Ministraciones
        #=========================================================================
        #Se revisa si se crea una ministración
        vals_min={
            'dependency_id':self.dep.id,
            'sub_dependency_id':self.sub_dep.id,
            'has_funds':True,
            'money_fund':money_fund.id,
            'move_type':'ministration',
            'date_request':date.today(),
            'min_account_id':self.cuenta_min.id,
        }
        with self.assertRaises(ValidationError): 
            ministration.create(vals_min)
        vals_min={
            'dependency_id':self.dep.id,
            'sub_dependency_id':self.sub_dep.id,
            'has_funds':True,
            'money_fund':money_fund.id,
            'move_type':'ministration',
            'date_request':date.today(),
            'trade_doc':files,
            'recived_doc':files,
            'justify_min_doc':files,
            'cover_doc':files,
            'min_account_id':self.cuenta_min.id,
        }
        with self.assertRaises(ValidationError): 
            ministration.create(vals_min)

        vals_min={
            'dependency_id':self.dep.id,
            'sub_dependency_id':self.sub_dep.id,
            'has_funds':True,
            'money_fund':money_fund.id,
            'move_type':'ministration',
            'amount_request':-1000,
            'date_request':date.today(),
            'trade_doc':files,
            'recived_doc':files,
            'justify_min_doc':files,
            'cover_doc':files,
            'min_account_id':self.cuenta_min.id,
        }
        with self.assertRaises(ValidationError): 
            ministration.create(vals_min)

        vals_min={
            'dependency_id':self.dep.id,
            'sub_dependency_id':self.sub_dep.id,
            'has_funds':True,
            'money_fund':money_fund.id,
            'move_type':'ministration',
            'amount_request':30000,
            'date_request':date.today(),
            'trade_doc':files,
            'recived_doc':files,
            'justify_min_doc':files,
            'cover_doc':files,
            'min_account_id':self.cuenta_min.id,
        }
        with self.assertRaises(ValidationError): 
            ministration.create(vals_min)
        vals_min={
            'dependency_id':self.dep.id,
            'sub_dependency_id':self.sub_dep.id,
            'has_funds':True,
            'money_fund':money_fund.id,
            'move_type':'ministration',
            'amount_request':1500,
            'date_request':date.today(),
            'trade_doc':files,
            'recived_doc':files,
            'justify_min_doc':files,
            'cover_doc':files,
            'min_account_id':self.cuenta_min.id,
        }
        request = ministration.create(vals_min)

        #====================================================================
        #--------------Ministraciones
        #====================================================================

        with self.assertRaises(ValidationError):
            request.action_for_publish()
        
        request.update({'qr_signature_dep':qr_img})
        request.action_for_publish()
        self.assertEqual(request.state,'published')

        #Revisar observaciones
        request.action_for_observations()
        self.assertEqual(request.state,'published')
        self.assertEqual(request.is_published,True)
        self.assertNotEqual(request.observations,'')

        #Mandar a revisión
        request.action_for_revised()
        self.assertEqual(request.state,'revised')

        #Aprobar
        request.action_for_approve()
        self.assertEqual(request.state,'approved')

            
        self.assertIsNotNone(request.transfer_request.id)

        #Solicitud de transferencia
        
        with self.assertRaises(ValidationError):
            request.transfer_request.request_finance()
        
        request.transfer_request.update({'bank_account_id':request.transfer_request.desti_bank_account_id.id})
        request.transfer_request.request_finance()
        request.transfer_request.approve_finance()
        request.transfer_request.action_schedule_transfers()
        request.transfer_request.payment_ids.post()

        self.assertEqual(request.state,'deposited')
        self.assertEqual(request.money_fund.import_fund,18500)
        self.assertIsNotNone(request.account_topay_id)

        #=========================================================================
        #--------------Cancelación
        #=========================================================================
        vals_cancel={
            'dependency_id':self.dep.id,
            'sub_dependency_id':self.sub_dep.id,
            'has_funds':True,
            'money_fund':money_fund.id,
            
        } 
        request = self.env['money.funds.cancel'].create(vals_cancel)
        with self.assertRaises(UserError):
            request.action_for_publish()

        archivo=self.env['ir.attachment'].create({
            'name':'file_pdf.pdf',
            'res_model':'money.funds.cancel',
            'company_id':1,
            'store_fname':files,
        })
        request.update({'cancel_files':archivo})
        request.action_for_publish()
        request.action_for_observations()

        res_bank = self.env['bank.reference'].calculate_bank_ref("A999A10294",request.money_fund.import_fund,date.today())

        referencia=self.env['bank.reference'].create({
            'reference':res_bank,
            'short_ref':"A999A10294",
            'date':date.today(),
            'currency_id':self.currency.id,
            'amount':request.money_fund.import_fund,
        })
        
        request.update({'bank_ref':referencia.id})


        bank_move = self.env['bank.movements'].create({
            'income_bank_journal_id':self.diario.id,
            'amount_interest':request.money_fund.import_fund,
            'ref':referencia,
            'customer_ref':referencia,
            'reference_plugin_1':referencia,
        })
        bank_move.recognize()