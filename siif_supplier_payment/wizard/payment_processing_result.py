# -*- coding: utf-8 -*-
from distutils import log
from email.policy import default
import json
import logging
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError

PAYMENT_PROCESSING_RESULT = 'payment.processing.result'
ACCOUNT_JOURNAL = 'account.journal'

ACCOUNT_BANK_SRC = 'Account Bank Source'
TOTAL_AMOUNT = 'Total Amount'

class WizardPaymentProcessingResult(models.TransientModel):
    _name = PAYMENT_PROCESSING_RESULT
    _description = 'Wizard to show the result of automatic payment processing'

    message = fields.Text('Message')

    payment_processing_result_line_ids = fields.One2many('payment.processing.result.line','payment_processing_result_id')
    payment_processing_result_error_line_ids = fields.One2many('payment.processing.result.error.line','payment_processing_result_id')

    payment_processing_cash_result_line_ids = fields.One2many('payment.processing.cash.result.line','payment_processing_result_id')
    payment_processing_cash_result_error_line_ids = fields.One2many('payment.processing.cash.result.error.line','payment_processing_result_id')

    payment_processing_check_result_line_ids = fields.One2many('payment.processing.check.result.line','payment_processing_result_id')
    payment_processing_check_result_error_line_ids = fields.One2many('payment.processing.check.result.error.line','payment_processing_result_id')

class WizardPaymentProcessingResultLine(models.TransientModel):
    _name = 'payment.processing.result.line'

    payment_processing_result_id = fields.Many2one(PAYMENT_PROCESSING_RESULT)

    journal_id = fields.Many2one(ACCOUNT_JOURNAL, string=ACCOUNT_BANK_SRC)
    target_bank = fields.Selection([('same', 'Same'), ('other', 'Other')], string='Target Bank')
    payment_issuance_method = fields.Selection(
        [('manual', 'Manual'),('automatic', 'Automatic'),('check', 'Check')],
        string='Record Type',
    )
    folio = fields.Integer('Folio')

class WizardPaymentProcessingResultErrorLine(models.TransientModel):
    _name = 'payment.processing.result.error.line'

    payment_processing_result_id = fields.Many2one(PAYMENT_PROCESSING_RESULT)

    currency_id = fields.Many2one('res.currency','Currency')
    journal_id = fields.Many2one(ACCOUNT_JOURNAL, string=ACCOUNT_BANK_SRC)
    total_amount = fields.Float(TOTAL_AMOUNT)
    balance_available = fields.Float('Balance Available')
    minimum_balance = fields.Float('Minimum Balance')

class WizardPaymentCashProcessingResultLine(models.TransientModel):
    _name = 'payment.processing.cash.result.line'

    payment_processing_result_id = fields.Many2one(PAYMENT_PROCESSING_RESULT)

    currency_funds_id = fields.Many2one('currency.funds', 'Currency Funds')
    folio = fields.Integer('Folio')

class WizardPaymentCashProcessingResultErrorLine(models.TransientModel):
    _name = 'payment.processing.cash.result.error.line'

    payment_processing_result_id = fields.Many2one(PAYMENT_PROCESSING_RESULT)

    currency_id = fields.Many2one('res.currency','Currency')
    currency_funds_id = fields.Many2one('currency.funds', 'Currency Funds')
    total_amount = fields.Float(TOTAL_AMOUNT)
    currency_application_request_id = fields.Many2one('application.request', 'Currency Application Request')
    requested_amount = fields.Float(string='Request Amount')

class WizardPaymentcheckProcessingResultLine(models.TransientModel):
    _name = 'payment.processing.check.result.line'

    payment_processing_result_id = fields.Many2one(PAYMENT_PROCESSING_RESULT)

    checkbook_request_id = fields.Many2one('checkbook.request', 'Checkbook')
    journal_id = fields.Many2one(ACCOUNT_JOURNAL, string=ACCOUNT_BANK_SRC)
    payment_type = fields.Char('Payment Type')
    folio = fields.Integer('Folio')
    number_payments = fields.Integer('Number of payments')
    total_amount = fields.Float(TOTAL_AMOUNT)

class WizardPaymentcheckProcessingResultErrorLine(models.TransientModel):
    _name = 'payment.processing.check.result.error.line'

    payment_processing_result_id = fields.Many2one(PAYMENT_PROCESSING_RESULT)

    checkbook_request_id = fields.Many2one('checkbook.request', 'Checkbook')
    journal_id = fields.Many2one(ACCOUNT_JOURNAL, string=ACCOUNT_BANK_SRC)
    total_amount = fields.Float(TOTAL_AMOUNT)
    balance_available = fields.Float('Balance Available')
    number_payments = fields.Integer('Number of payments')
    minimum_balance = fields.Float('Minimum Balance')