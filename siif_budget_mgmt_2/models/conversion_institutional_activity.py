# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from lxml import etree
from odoo.exceptions import ValidationError, UserError

class ConversionInstitutionalActivity(models.Model):

    _name = 'conversion.institutional.activity'

    program_id = fields.Many2one('program','Program Key')
    institutional_activity_id = fields.Many2one('institutional.activity','Institutional Activity')


    _sql_constraints = [('conversion_institutional_activity_uk', 'unique(program_id, start_date, end_date)', 'The Program Key Activity must be unique')]

    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')

    @api.constrains('start_date', 'end_date')
    def check_end_date(self):
        if not self.start_date:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("Configure la fecha inicial"))
            else:
                raise ValidationError(_("The start date of validity is required"))
        if self.end_date and self.end_date <= self.start_date:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("La fecha de fin de vigencia no puede ser menor o igual a la fecha de inicio de vigencia"))
            else:
                raise ValidationError(_("The effective end date cannot be less than or equal to the effective date"))


    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super().fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])
   
        is_group_budget_approval_user = self.env.user.has_group('jt_budget_mgmt.group_budget_approval_user')
        is_group_budget_guest_user = self.env.user.has_group('jt_budget_mgmt.group_budget_guest_user')
        is_group_budget_confirmation_user = self.env.user.has_group('jt_budget_mgmt.group_budget_confirmation_user')
        is_group_budget_register_user = self.env.user.has_group('jt_budget_mgmt.group_budget_register_user')
        
        if (is_group_budget_approval_user or is_group_budget_guest_user or is_group_budget_confirmation_user or is_group_budget_register_user): 
            for node in doc.xpath("//" + view_type):
                node.set('export', '0')
                node.set('create', '0') 
                node.set('edit', '0')
                node.set('delete', '0')
                
            for node in doc.xpath("//field"):
                node.set('options','{"no_open":true}')
                           
                
        res['arch'] = etree.tostring(doc) 
        return res

    def name_get(self):
        return [(rec.id, rec.program_id.key_unam) for rec in self]