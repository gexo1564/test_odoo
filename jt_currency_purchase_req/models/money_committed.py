from odoo import models, fields, api, _

class MoneyCommited(models.Model):

    _name = 'money.commited'
    _description = 'Money committed for application requests'

    folio = fields.Integer('Folio')
    currency_fund = fields.Many2one('currency.funds',string="Currency Funds")
    app_request = fields.Many2one('application.request',string='Application Request ID')
    currency_id = fields.Many2one('res.currency', compute='_get_currency', string="Currency")
    money = fields.Monetary('Money Committed',currency_field='currency_id')


    #Asignación de la moneda por el fondo fijo
    @api.depends('app_request')
    def _get_currency(self):
        self.currency_id=self.app_request.currency_id