# -*- coding: utf-8 -*-
from datetime import datetime
from odoo import api, fields, models, _


class ReportAccountFinancialReport(models.Model):
    _inherit = 'jt_account_module_design.financial.report'


    def _get_lines(self, options, line_id=None):
        lines = []
        domain = []
        dep_domain = []
        is_add_fiter = False
        is_add_dep_fiter = False
        if len(options['selected_programs']) > 0:
            domain.append(('program_id.key_unam', 'in', options['selected_programs']))
            is_add_fiter = True
        if len(options['selected_sub_programs']) > 0:
            domain.append(('sub_program_id.sub_program', 'in', options['selected_sub_programs']))
            is_add_fiter = True
        if len(options['selected_dependency']) > 0:
            dep_domain.append(('move_id.dependancy_id.dependency', 'in', options['selected_dependency']))
            is_add_dep_fiter = True
            is_add_fiter = True
        if len(options['selected_sub_dependency']) > 0:
            dep_domain.append(('move_id.sub_dependancy_id.sub_dependency', 'in', options['selected_sub_dependency']))
            is_add_dep_fiter = True
            is_add_fiter = True
        if len(options['selected_items']) > 0:
            domain.append(('item_id.item', 'in', options['selected_items']))
            is_add_fiter = True
        if len(options['selected_or']) > 0:
            domain.append(('resource_origin_id.key_origin', 'in', options['selected_or']))
            is_add_fiter = True
            
        program_code_obj = self.env['program.code']
        program_codes = program_code_obj.search(domain)
        
        program_codes_account_ids = program_codes.mapped('item_id.unam_account_id')
        program_codes_account_ids = program_codes_account_ids.ids
         
        report_id = self.env.ref('account_reports.account_financial_report_balancesheet0')
        
        if is_add_dep_fiter:
            move_lines_ids = self.env['account.move.line'].search(dep_domain)
            program_codes_account_ids += move_lines_ids.mapped('account_id').ids
        
        if  report_id:
            #======= Side Data ======#
            line_obj = report_id.line_ids
            if line_id:
                line_obj = self.env['account.financial.html.report.line'].search([('id', '=', line_id)])
            if options.get('comparison') and options.get('comparison').get('periods'):
                line_obj = line_obj.with_context(periods=options['comparison']['periods'])
            if options.get('ir_filters'):
                line_obj = line_obj.with_context(periods=options.get('ir_filters'))
    
            currency_table = report_id._get_currency_table()
            domain, group_by = report_id._get_filter_info(options)
    
            if group_by:
                options['groups'] = {}
                options['groups']['fields'] = group_by
                options['groups']['ids'] = report_id._get_groups(domain, group_by)
    
            amount_of_periods = len((options.get('comparison') or {}).get('periods') or []) + 1
            amount_of_group_ids = len(options.get('groups', {}).get('ids') or []) or 1
            linesDicts = [[{} for _ in range(0, amount_of_group_ids)] for _ in range(0, amount_of_periods)]
            
            left_line_obj = self.env['account.financial.html.report.line']
            right_line_obj = line_obj
            left_line_id = self.env.ref('account_reports.account_financial_report_total_assets0')
            if left_line_id and left_line_id.id in line_obj.ids:
                left_line_obj = left_line_id
                right_line_obj = right_line_obj - left_line_id
            
            if is_add_fiter:
                if is_add_dep_fiter:
                    domain = dep_domain

                lines = left_line_obj.with_context(
                        filter_domain=domain,custom_account_ids=[('account_id','in',program_codes_account_ids)]
                    )._get_lines(self, currency_table, options, linesDicts)
                if self.env.context and self.env.context.get('side_lines',False) and self.env.context.get('side_lines',False)=='right':
                    lines = []
                      
                for l in lines:

                    new_col = []
                    for data in l.get('columns'):
                        if data.get('no_format_name',0):
                            balance_pesos = data.get('no_format_name',0)/1000 
                            new_col.append(self._format({'name': balance_pesos},figure_type='float'))
                        elif data.get('name',0) and isinstance(data.get('name',0), float):
                            balance_pesos = data.get('name',0)/1000 
                            new_col.append(self._format({'name': balance_pesos},figure_type='float'))
                        else:
                            new_col.append(data)
                    
                    l.update({'side':'left','columns':new_col})

                right_lines = right_line_obj.with_context(
                        filter_domain=domain,custom_account_ids=[('account_id','in',program_codes_account_ids)]
                    )._get_lines(self, currency_table, options, linesDicts)
                
                if self.env.context and self.env.context.get('side_lines',False) and self.env.context.get('side_lines',False)=='left':
                    right_lines = []
                    
                for l in right_lines:
                    
                    new_col = []
                    for data in l.get('columns'):
                        if data.get('no_format_name',0):
                            balance_pesos = data.get('no_format_name',0)/1000 
                            new_col.append(self._format({'name': balance_pesos},figure_type='float'))
                        elif data.get('name',0) and isinstance(data.get('name',0), float):
                            balance_pesos = data.get('name',0)/1000 
                            new_col.append(self._format({'name': balance_pesos},figure_type='float'))                            
                        else:
                            new_col.append(data)
                    
                    l.update({'side':'right','columns':new_col})
                    
                lines += right_lines
                #lines = report_id.with_context(custom_account_ids=[('account_id','in',program_codes_account_ids)])._get_lines(options, line_id=line_id)
            else:
                lines = left_line_obj.with_context(
                        filter_domain=domain
                    )._get_lines(self, currency_table, options, linesDicts)

                if self.env.context and self.env.context.get('side_lines',False) and self.env.context.get('side_lines',False)=='right':
                    lines = []
                    





                for l in lines:
                    new_col = []
                    for data in l.get('columns'):
                        if data.get('no_format_name',0):
                            balance_pesos = data.get('no_format_name',0)/1000 
                            new_col.append(self._format({'name': balance_pesos},figure_type='float'))
                        elif data.get('name',0) and isinstance(data.get('name',0), float):
                            balance_pesos = data.get('name',0)/1000 
                            new_col.append(self._format({'name': balance_pesos},figure_type='float'))
                        else:
                            new_col.append(data)
                            
                    l.update({'side':'left','columns':new_col})
                    
                right_lines = right_line_obj.with_context(
                        filter_domain=domain
                    )._get_lines(self, currency_table, options, linesDicts)
                if self.env.context and self.env.context.get('side_lines',False) and self.env.context.get('side_lines',False)=='left':
                    right_lines = []
                    
                for l in right_lines:
                    new_col = []
                    for data in l.get('columns'):
                        if data.get('no_format_name',0):
                            balance_pesos = data.get('no_format_name',0)/1000 
                            new_col.append(self._format({'name': balance_pesos},figure_type='float'))
                        elif data.get('name',0) and isinstance(data.get('name',0), float):
                            balance_pesos = data.get('name',0)/1000 
                            new_col.append(self._format({'name': balance_pesos},figure_type='float'))
                        else:
                            new_col.append(data)
                    
                    l.update({'side':'right','columns':new_col})
                
                lines+=right_lines


        periods = options.get('comparison', {}).get('periods')

        for i, line in enumerate(lines):
            if line.get('caret_options', None) == 'account.account':
                new_lines = []
                acc = self.env['account.account'].browse([int(line.get('id', '').split('_')[-1])])

                deps = self.env['dependency'].get_dependencies_for_account(acc, [
                    ('date', '<=', datetime.strptime(options['date'].get('date_to'), '%Y-%m-%d').date()),
                    ('parent_state', '=', 'posted') if  not options.get('all_entries') else ('parent_state', '!=', 'cancel')
                ])
                if deps:
                    line.update({
                        'unfoldable': True,
                        'folded': True

                    })
                for dep in deps:
                    balance_vals = []
                    ''' Append balance for current period '''
                    balance_vals.append(self.calculate_balance(acc=acc, dep=dep, 
                        posted_only=not options.get('all_entries'),
                        date_to=datetime.strptime(options['date'].get('date_to'), '%Y-%m-%d').date()))

                    ''' Append balances for all other comparison periods '''
                    for period in periods:
                        balance_vals.append(self.calculate_balance(acc=acc, dep=dep, 
                            posted_only=not options.get('all_entries'),
                            date_to=datetime.strptime(period.get('date_to'), '%Y-%m-%d').date()))


                    dep_cols = [self._format({'name': balance, 'class': 'number'}, figure_type='float') for balance in balance_vals]

                    subdeps = self.env['sub.dependency'].get_subdependencies_for_account(acc, dep, [
                        ('date', '<=', datetime.strptime(options['date'].get('date_to'), '%Y-%m-%d').date()),
                        ('parent_state', '=', 'posted') if  not options.get('all_entries') else ('parent_state', '!=', 'cancel')
                    ])

                    dep_line = self.env['account.report'].format_line(acc=acc, dep=dep, children=subdeps, cols=dep_cols,
                        extra_info={
                            'parent_id': line.get('id', ''),
                            'financial_group_line_id': line.get('financial_group_line_id'),
                            'side': line.get('side'),
                            'folded': True
                    })
                    new_lines.append(dep_line)

                    for subdep in subdeps:
                        subdep_balance_vals = []

                        subdep_balance_vals.append(self.calculate_balance(acc=acc, dep=dep, subdep=subdep,
                            posted_only=not options.get('all_entries'),
                            date_to=datetime.strptime(options['date'].get('date_to'), '%Y-%m-%d').date()))


                        for period in periods:
                            subdep_balance_vals.append(self.calculate_balance(acc=acc, dep=dep, subdep=subdep,
                                posted_only=not options.get('all_entries'),
                                date_to=datetime.strptime(period.get('date_to'), '%Y-%m-%d').date()))

                        subdep_cols = [self._format({'name': balance, 'class': 'number'}, figure_type='float') for balance in subdep_balance_vals]


                    
                        subdep_line = self.env['account.report'].format_line(acc=acc, dep=dep, subdep=subdep, 
                        cols=subdep_cols, extra_info={
                            'financial_group_line_id': line.get('financial_group_line_id'),
                            'side': line.get('side'),
                            'folded': True
                        })
                        new_lines.append(subdep_line)
                lines[i + 1:i + 1] = new_lines


        return lines

    def calculate_balance(self, acc, dep, posted_only, date_to, subdep=None):
        '''
        (New) Calculates the balance of an account and dependency, filtered
        optionally by a subdependency. If the account is not an asset account,
        it will give the sum of credits - debits, rather than debits - credits
        :param account_id acc: account id
        :param dependency_id: dependency id
        :posted_only boolean: If true will only count journal items that have
        been posted
        :date_to date: This function will only sum up journal items until this 
        date
        '''

        balance_lines = [l.debit - l.credit for l in self.env['account.move.line'].search([
                ('account_id', '=', acc.id),
                ('dependency_id', '=', dep.id),
                ('sub_dependency_id', '=', subdep.id) if subdep else ('id', '!=', None),
                ('parent_state', '=', 'posted') if posted_only else ('parent_state', '!=', 'cancel'),
                ('date', '<', date_to)
            ])]

        if acc.internal_group != 'asset':
            balance_lines = [line * -1 for line in balance_lines]

        return sum(balance_lines) / 1000