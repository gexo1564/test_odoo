from odoo.exceptions import ValidationError,UserError
from odoo import models,fields,api,_
from datetime import date
from xmlrpc import client
from odoo.addons.sd_utils.tools import utils
import os
import logging
_logger = logging.getLogger(__name__)

modelos_repetidos=[
    'invoice.services.education',               #0
    'account.journal',                          #1   
    'association.distribution.ie.accounts',     #2
    'invoice.services.education.line',          #3
    'account.move'                              #4
    ]
mensajes_repetidos = [
    'Nueva Factura',                                                            #0
    '*********************No se encontraron registros del dia de hoy'           #1
    ]


class InvoiceServicesEducation(models.Model):
    _name = modelos_repetidos[0]
    _description = "CFDI por Servicios de Educación"

    name = fields.Char(string="Number Invoice", required=True, copy=False, readonly=True, index=True, default=mensajes_repetidos[0])
    account_bank = fields.Many2one(modelos_repetidos[1],string="Bank")
    amount = fields.Float(string="Amount")
    date_emision = fields.Date(string='Date Emission')
    currency_id = fields.Many2one('res.currency',string='Currency')
    type_of_collection = fields.Many2one(modelos_repetidos[2],string='Type of Revenue Collection')
    percentage = fields.Char(string="percentage")
    invoice_serie = fields.Char(string='Serie')
    invoice_folio = fields.Char(string='Folio')
    invoice_uuid = fields.Char(string='UUID')
    invoice_status = fields.Char(string='Status')
    invoice_pdf = fields.Binary(string='CFDI PDF')
    invoice_pdf_name = fields.Char()
    invoice_xml = fields.Binary(string='CFDI XML')
    invoice_xml_name = fields.Char()
    account_move_ids = fields.One2many(modelos_repetidos[3],'invoice_services_education_id', readonly=True)


    @api.model
    def create(self, vals):
        """Método que sobrescribe el create del objeto."""
        today = date.today()
        name = ''
        serv_ed_id = self.env[modelos_repetidos[2]].search([('desc','=','Servicios de Educación')]).id
        ex_adm_id = self.env[modelos_repetidos[2]].search([('desc','=','Examen de Admisión')]).id
        asp_id = self.env[modelos_repetidos[2]].search([('desc','=','Aspirantes')]).id
        if vals.get('name',mensajes_repetidos[0]) == mensajes_repetidos[0]:
            name_seq = self.env['ir.sequence'].next_by_code('invoice_service_education.sequence') or mensajes_repetidos[0]
            if vals.get('type_of_collection') == serv_ed_id:
                name = 'SEDU/'+ str(today.year) +'/' + str(name_seq)
            elif vals.get('type_of_collection') == ex_adm_id:
                name = 'EXAD/'+ str(today.year) +'/' + str(name_seq)
            elif vals.get('type_of_collection') == asp_id:
                name = 'ASPI/'+ str(today.year) +'/' + str(name_seq)
            _logger.info(f"***** name: {name}")
            vals['name'] = name 
        return super(InvoiceServicesEducation, self).create(vals)

    def order_of_generate(self):
        ids_banks = []
        ids_ser_ed = self.env[modelos_repetidos[1]].search([('name','in',('BBVA BANCOMER-7081 (BBV01)','SANTANDER-2119 (BSA01)','SCOTIABANK-8000 (BSC01)'))]).ids
        for i in ids_ser_ed:
            ids_banks.append(i)
        id_prep_ex = self.env[modelos_repetidos[1]].search([('name','=','SANTANDER-9804 (BSA01)')]).id
        ids_banks.append(id_prep_ex)
        id_asp = self.env[modelos_repetidos[1]].search([('name','=','SANTANDER-3306 (BSA01)')]).id
        ids_banks.append(id_asp)
        for id_bank in ids_banks:
            if ids_banks.index(id_bank) < 3:
                #Servicios de educación
                self.generate_ser_ed(id_bank)
            elif ids_banks.index(id_bank) == 3:
                #Preparación de Examenes
                self.generate_prep_ex(id_bank)
            else:
                #Aspirantes
                self.generate_asp(id_bank)
    
    def generate_ser_ed(self,id_bank):
        percentages = []
        suma = 0
        porc1 = 0.0
        porc2 = 0.0
        acc_moves_ids = []
        vals_lines = []
        afectation_id = self.env[modelos_repetidos[2]].search([('desc','=','Servicios de Educación')]).id
        acc_moves = self.env[modelos_repetidos[4]].search([('income_bank_journal_id','=',id_bank),('afectation_account_id','=',722),('date','=',date.today()),('state','=','posted')])
        ie_accounts = self.env['association.distribution.ie.accounts.line'].search([('ie_account_id','=',afectation_id)])

        if acc_moves:
            for ie in ie_accounts:
                percentages.append(ie.percentage)
            for acc_move in acc_moves:
                suma += acc_move.amount_interest
                acc_moves_ids.append(acc_move.id)
            if len(percentages) == 2:
                porc1 = suma * (percentages[0]/100)
                porc2 = suma * (percentages[1]/100)
            
            vals = {
                'account_bank':id_bank,
                'amount':suma,
                'percentage':str(percentages[0])+'% = '+str("{0:.2f}".format(porc1))+'$ |--| '+str(percentages[1])+'% = '+str("{0:.2f}".format(porc2))+'$',
                'date_emision':date.today(),
                'currency_id':acc_moves[0].currency_id.id,
                'type_of_collection': afectation_id,
                
            }
            new_invoice = self.env[modelos_repetidos[0]].create(vals)
            
            if new_invoice:
                for moves in acc_moves_ids:
                    line = {
                        'invoice_services_education_id':new_invoice.id,
                        'account_move_id':moves
                    }
                    vals_lines.append(line)
                new_acco_move_ids = self.env[modelos_repetidos[3]].create(vals_lines)
            new_invoice.write({'account_move_ids':new_acco_move_ids})

            new_invoice.generate_invoice(suma)
            
        else:
            _logger.info(mensajes_repetidos[1])
    
    def generate_prep_ex(self,id_bank):
        suma = 0
        acc_moves_ids = []
        vals_lines = []
        afectation_id = self.env[modelos_repetidos[2]].search([('desc','=','Examen de Admisión')]).id
        acc_moves = self.env[modelos_repetidos[4]].search([('income_bank_journal_id','=',id_bank),('afectation_account_id','=',afectation_id),('date','=',date.today()),('state','=','posted')])
        if acc_moves:
            for acc_move in acc_moves:
                suma += acc_move.amount_interest
                acc_moves_ids.append(acc_move.id)
            vals = {
                'account_bank':id_bank,
                'amount':suma,
                'date_emision':date.today(),
                'currency_id':acc_moves[0].currency_id.id,
                'type_of_collection': afectation_id
            }
            new_invoice = self.env[modelos_repetidos[0]].create(vals)
            if new_invoice:
                for moves in acc_moves_ids:
                    line = {
                        'invoice_services_education_id':new_invoice.id,
                        'account_move_id':moves
                    }
                    vals_lines.append(line)
                account_move_ids = self.env[modelos_repetidos[3]].create(vals_lines)
                new_invoice.write({'account_move_ids':account_move_ids})
                new_invoice.generate_invoice(suma)

        else:
            _logger.info(mensajes_repetidos[1])
    
    def generate_asp(self,id_bank):
        suma = 0
        acc_moves_ids = []
        vals_lines = []
        afectation_id = self.env[modelos_repetidos[2]].search([('desc','=','Aspirantes')]).id
        acc_moves = self.env[modelos_repetidos[4]].search([('income_bank_journal_id','=',id_bank),('afectation_account_id','=',afectation_id),('date','=',date.today()),('state','=','posted')])
        if acc_moves:    
            for acc_move in acc_moves:
                suma += acc_move.amount_interest
                acc_moves_ids.append(acc_move.id)
            vals = {
                'account_bank':id_bank,
                'amount':suma,
                'date_emision':date.today(),
                'currency_id':acc_moves[0].currency_id.id,
                'type_of_collection': afectation_id
            }
            new_invoice = self.env[modelos_repetidos[0]].create(vals)
            if new_invoice:
                for moves in acc_moves_ids:
                    line = {
                        'invoice_services_education_id':new_invoice.id,
                        'account_move_id':moves
                    }
                    vals_lines.append(line)
                account_move_ids = self.env[modelos_repetidos[3]].create(vals_lines)
            new_invoice.write({'account_move_ids':account_move_ids})
            new_invoice.generate_invoice(suma)
        else:
            _logger.info(mensajes_repetidos[1])
    
    def generate_invoice(self,total):
        url_facturacion = os.environ['HOST_FACTURACION']
        proxy = client.ServerProxy(url_facturacion)
        
        products = []


        #TIPO DE SERVICIO=====================================================
        service_type ={
                'Cvepago': 'SE', # SG - Servicios Generales
                'Tpocfd':'FD' # FD - Factura Digital
        }

        # PRODUCTOS=====================================================
        products =[
                        {
                         'Cta_ie': 202,
                         'Cve_producto': 86121700,
                         'Cantidad':1,
                         'Unidad': 'PIEZA', 
                         'Cve_unidad': 'H87',
                         'Unidad_local':'PIEZA',  #OPCIONAL
                         'Descripcion':'Titulo',
                         'Precio_unit':total,
                         'Iva':'X',
                         'Iva_impte':0,
                         'Descto':0,
                         'c_tasacuota_tralix':'0.000000',
                         'c_impuesto_tralix':'002',
                         'c_tipofactor_tralix':'Tasa',
                         'Descto_impte':0.000000,
                         'Importe_total':total,
                         'Importe_mil':0.000000,
                        }
                 ]

        #MÉTODOS DE PAGO=====================================================
        payment =[
                        {
                        'Cve_pago':'EF',
                        'Importe':total,
                        'Cve_banco':'',
                        'Num_cheque':'',
                        'Cta_tarjeta':'',
                        'Referencia_banco':'',
                        'Cve_tipo_TDC':'',
                        'Fecha_transaccion':'',
                        'Folio_aprobacion':''
                        },
                ]

        # DATOS FISCALES=====================================================
        fiscal = {
                    'Procedencia':'OT',
                    'Pais':'MEX',
                    'Rfc':'MOLL8602162S2',
                    'Nombre':'CARLOS',
                    'A_paterno':'FUENTES',
                    'A_materno':'MACIAS',
                    'Estado':'', #no son obligatorios
                    'DelOMunicipio':'', #no son obligatorios
                    'CP':'80290',
                    'Colonia':'', #no son obligatorios
                    'Calle':'', #no son obligatorios
                    'No_ext':'', #no son obligatorios
                    'No_int':'', #no son obligatorios
                    'Email':'correo1@dominio.com',
                    'Email2': 'correo1@dominio.com', #no son obligatorios
                    'UsoCFDI': 'G03',
                }



        # DEP=====================================================
        dependency = {
                    'Dependencia':'553',  #323
                    'Subdep':'01',
                    'Origen':'AE',
                    'Status_pago': 'I',   # cuando es P es posterior, esto genera una referencia
                    'Importe_factura':total,
                    'Cve_moneda':'MXN',
                    'Forma_liq': 'E',# 'E',
                    'Carta_certif':'0',
                    'Tipo_moneda': '1', # '1',
                    'Cve_actividad':'1',
                    'Observaciones':''
                }
        #INE--------------------------------------------------
        ine = {}

        cfdi_request = proxy.genera_cfdi.v3(service_type, products, payment, fiscal, dependency, ine)

        if cfdi_request:
            if cfdi_request[0]:
                self.invoice_serie = cfdi_request[1]
                self.invoice_folio = cfdi_request[2]
                self.invoice_status = cfdi_request[3]
                self.invoice_uuid = cfdi_request[4]
                try:
                    self.get_cfdi_files()
                except:
                    return cfdi_request[0]

            else:
                raise UserError(cfdi_request[1])
        else:
            raise UserError("No fue posible comunicarse con el servicio de facturación.\nPor favor, intente más tarde.")
        
        return cfdi_request[0]

    def get_cfdi_files(self):
        """Método que descarga y almacena los archivos XML y PDF una vez generada una factura"""
        self.ensure_one()

        response_pdf = utils.get_cfdi_pdf_xml_file(self.invoice_serie,self.invoice_folio,'PDF')
        self.handle_error(response_pdf,"invoice_pdf",self.name,'.pdf')
        response_xml = utils.get_cfdi_pdf_xml_file(self.invoice_serie,self.invoice_folio,'XML')
        self.handle_error(response_xml,"invoice_xml",self.name,'.xml')
        
    def handle_error(self,response, attachment_name, file_name, format):
        if isinstance(response, bytes):
            ir_attachment_rec = utils.store_binary_file(self, attachment_name, response)
            if ir_attachment_rec:
                self.cfdi_of_donation_pdf_name = file_name + ".pdf" if self.benefactor_rfc else "CFDI.pdf"
            else:
                raise ValidationError(f"No fue posible guardar el archivo CFDI en {format}.")
        else:
            raise ValidationError(response)
        

class InvoiceServicesEducationLine(models.Model):

    _name = modelos_repetidos[3]
    _description = 'Lineas de Servicios de Educación'

    invoice_services_education_id = fields.Many2one(modelos_repetidos[0], string='Invoice Services Education')
    account_move_id = fields.Many2one(modelos_repetidos[4],string="Account Move")
    amount = fields.Float(related='account_move_id.amount_interest')
