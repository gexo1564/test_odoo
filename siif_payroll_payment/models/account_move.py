# -*- coding: utf-8 -*-

import logging
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
from datetime import datetime, date

from openpyxl import Workbook
from openpyxl.styles import Font

import xlwt
import io
import base64
import math

from odoo.addons.siif_supplier_payment.tools.utils import Dictionary
from odoo.addons.siif_payroll_payment.tools.layouts import Context
from odoo.addons.siif_payroll_payment.tools.layouts.bancos import LayoutBanorte, LayoutBBVASIT, LayoutBBVA, LayoutHSBC, LayoutSantanderH2H, LayoutSantander, LayoutBanamex

ACCOUNT_MOVE = 'account.move'
ACCOUNT_MOVE_LINE = 'account.move.line'
ACCOUNT_PAYMENT = 'account.payment'
IR_SEQUENCE = 'ir.sequence'
RES_PARTNER = 'res.partner'
ACCOUNT_JOURNAL = 'account.journal'
PAYMENT_BATCH_SUPPLIER = 'payment.batch.supplier'
IR_ACTION_WINDOW = 'ir.actions.act_window'

NUMBER_FORMAT = u'#,##0.00'

STR_CODIGO_PROGRAMATICO = "Código Programático"
STR_DESC_DEPENDENCIA = "Descripción de la Dependencia"
STR_SUB_DEPENDENCIA = "Sub Dependencia"
STR_DESC_SUB_DEPENDENCIA = "Descripción de la Sub Dependencia"

# CONSTANT FIELDS
L10N_MX_EDI_PAYMENT_METHOD_TRANSF = 'l10n_mx_edi.payment_method_transferencia'
L10N_MX_EDI_PAYMENT_METHOD_CHEQUE = 'l10n_mx_edi.payment_method_cheque'
class AccountMove(models.Model):
    _inherit = ACCOUNT_MOVE

    number_of_payment_receipts = fields.Integer('Total Number of Payment Receipts')
    number_of_validated_receipts = fields.Integer('Number of Validated Receipts')

    is_payroll_payment_reissue = fields.Boolean()
    adjustment_case_id = fields.Many2one(comodel_name='adjustment.cases', string='Adjustment Case')

    def create_payroll_payment_request(self, vals, lines):
        # Validación del status de los cheques
        self.validate_payrroll_checks_available_for_printing(vals['payroll_processing_id'])
        # TODO Agregar descripción de los apuntes contables
        # logging.info("Calculando total ...")
        total = sum([l['price_unit']for l in lines])
        # logging.info("Ok")
        # Los valores de los montos solo aplican a solicitudes de nómina sin impuestos
        vals.update({
            'payment_state': 'registered',
        })
        # Creando la solicitud de pago
        # logging.info("Creando solicitud de pago ...")
        move_id = self.insert_sql([vals])[0]
        # logging.info("Ok")
        # Referencia del asiento contable
        name = "Pago de nómina quincena %s %s" % (
            vals.get('fornight', ''),
            vals.get('period_start').strftime('%Y')
        )
        # Se crea la líne de apunte contable con el pago a la cuenta contable del
        # proovedor de la nómina.
        partner_id = self.env[RES_PARTNER].browse([vals.get('partner_id')])
        val = {
            'account_id': partner_id.property_account_payable_id.id,
            'quantity' : 1,
            'price_unit' : -total,
            'exclude_from_invoice_tab': True,
        }
        lines.insert(0, val)
        # Se crean las líneas de factura
        # logging.info("Creando las líneas de factura ...")
        self.env[ACCOUNT_MOVE_LINE].insert_sql(move_id, lines, name)
        # logging.info("Ok")
        # Se obtienen los códigos programáticos y el monto total afectado
        query_program_code_lines = """
            select program_code_id,
                price_total amount,
                p.dependency_id dependency_id,
                p.sub_dependency_id sub_dependency_id,
                i.unam_account_id account_id
            from account_move_line l, program_code p, expenditure_item i
            where l.program_code_id is not null
                and l.program_code_id = p.id
                and p.item_id = i.id
                and l.exclude_from_invoice_tab = False
                and l.move_id = %s
        """
        self.env.cr.execute(query_program_code_lines, (move_id,))
        program_code_lines = self.env.cr.dictfetchall()
        # logging.info("Ejecutando el gasto sobregirando el presupuesto ...")
        self.env['expenditure.budget.line'].update_available_with_overdraft(program_code_lines)
        # logging.info("Ok")
        # Creación de los apuntes contables presupuestales
        # logging.info("Creando apuntes contables presupuestales ...")
        budget_lines = self.get_accounting_budget_lines(move_id, program_code_lines)
        self.env[ACCOUNT_MOVE_LINE].insert_sql(move_id, budget_lines, name)
        # logging.info("Ok")
        # Se actualizan el status y fecha de aprobación de la solicitud de pago
        # logging.info("Actualizando fecha y estado del pago ...")
        query = """
            update account_move set payment_state = 'approved_payment',
                date_approval_request = now()
            where id = %s
        """
        self.env.cr.execute(query, (move_id,))
        # move_id.write({
        #     'payment_state': 'approved_payment',
        #     'date_approval_request': datetime.today().date(),
        # })
        # logging.info("Ok")
        # Se obtiene el objeto account.move
        # logging.info("Obteniendo account.move ...")
        move_id = self.env[ACCOUNT_MOVE].browse([move_id])
        # logging.info("Ok")
        # Se realiza la generación del folio de la solicitud
        # logging.info("Generando folio de la solicitud de pago ...")
        move_id.generate_folio()
        # logging.info("Ok")
        # Se capitaliza la solicitud de pago
        # logging.info("Realizando capitalización de la solicitud ...")
        move_id.capitalizacion()
        # logging.info("Ok")
        # Se publican los asientos contables
        # logging.info("Publicando los asientos contables ...")
        move_id.action_post()
        # logging.info("Ok")
        # Actualiza el status del cheque a impreso
        # logging.info("Actualizando status de los cheques a impresos ...")
        self.update_payrroll_checks_available_for_printing_to_printed(vals['payroll_processing_id'])
        return move_id

    def validate_payrroll_checks_available_for_printing(self, payroll_id):
        query_total_checks = """
            select count(1) from check_log
            where id in (
                select check_folio_id from employee_payroll_file
                where payroll_processing_id = %s
                    and check_folio_id is not null
                    and check_final_folio_id is null
                union all
                select check_final_folio_id from employee_payroll_file
                where payroll_processing_id = %s and check_final_folio_id is not null
            )
        """
        self.env.cr.execute(query_total_checks, (payroll_id, payroll_id))
        total_checks = (self.env.cr.fetchone() or [0])[0]
        query_available_for_printing = """
            select count(1) from check_log
            where id in (
                select check_folio_id from employee_payroll_file
                where payroll_processing_id = %s
                    and check_folio_id is not null
                    and check_final_folio_id is null
                union all
                select check_final_folio_id from employee_payroll_file
                where payroll_processing_id = %s and check_final_folio_id is not null
            )
            and status = 'Available for printing'
        """
        self.env.cr.execute(query_available_for_printing, (payroll_id, payroll_id))
        available_for_printing = (self.env.cr.fetchone() or [0])[0]
        if available_for_printing != total_checks:
            raise UserError("No es posible procesar la nómina porque hay cheques con estatus diferente a Disponible para Impresión.")

    def update_payrroll_checks_available_for_printing_to_printed(self, payroll_id):
        query_update_checks = """
            update check_log as c
            set status = 'Printed',
                date_printing = current_date,
                check_amount = e.net_salary
            from employee_payroll_file e
            where c.id = coalesce(e.check_final_folio_id, e.check_folio_id)
                and c.status = 'Available for printing'
                and e.payroll_processing_id = %s
        """
        self.env.cr.execute(query_update_checks, (payroll_id, ))

    def get_accounting_budget_lines(self, move_id, lines):
        budget_lines = []
        # Se obtienen los datos iniciales necesarios para generar los asientos contables
        # presupuestales
        query_move = """
            select journal_id, partner_id from account_move where id = %s
        """
        self.env.cr.execute(query_move, (move_id,))
        res = self.env.cr.dictfetchone()
        journal_id = self.env[ACCOUNT_JOURNAL].browse([res['journal_id']])
        partner_id = res['partner_id']
        for line in lines:
            program_code = line.get('program_code_id')
            balance = line.get('amount')
            dependency_id = line.get('dependency_id')
            sub_dependency_id = line.get('sub_dependency_id')
            budget_lines.append({
                'account_id': journal_id.default_credit_account_id.id,
                'coa_conac_id': journal_id.conac_credit_account_id.id,
                'price_unit': -balance,
                'exclude_from_invoice_tab': True,
                'conac_move': True,
                'partner_id': partner_id,
                'program_code_id': program_code,
                'dependency_id': dependency_id,
                'sub_dependency_id': sub_dependency_id,
                'is_for_approved_payment': True,
                'move_id': self.id,
            })
            budget_lines.append({
                'account_id': journal_id.default_debit_account_id.id,
                'coa_conac_id': journal_id.conac_debit_account_id.id,
                'price_unit': balance,
                'exclude_from_invoice_tab': True,
                'conac_move': True,
                'partner_id': partner_id,
                'program_code_id': program_code,
                'dependency_id': dependency_id,
                'sub_dependency_id': sub_dependency_id,
                'is_for_approved_payment': True,
                'move_id': self.id,
            })
            budget_lines.append({
                'account_id': journal_id.accured_credit_account_id.id,
                'coa_conac_id': journal_id.conac_accured_credit_account_id.id,
                'price_unit': -balance,
                'exclude_from_invoice_tab': True,
                'conac_move': True,
                'partner_id': partner_id,
                'program_code_id': program_code,
                'dependency_id': dependency_id,
                'sub_dependency_id': sub_dependency_id,
                'is_for_approved_payment': True,
                'move_id': self.id,
            })
            budget_lines.append({
                'account_id': journal_id.accured_debit_account_id.id,
                'coa_conac_id': journal_id.conac_accured_debit_account_id.id,
                'price_unit': balance,
                'exclude_from_invoice_tab': True,
                'conac_move': True,
                'partner_id': partner_id,
                'program_code_id': program_code,
                'dependency_id': dependency_id,
                'sub_dependency_id': sub_dependency_id,
                'is_for_approved_payment': True,
                'move_id': self.id,
            })
            budget_lines.append({
                'account_id': journal_id.execercise_credit_account_id.id,
                'coa_conac_id': journal_id.conac_exe_credit_account_id.id,
                'price_unit': -balance,
                'exclude_from_invoice_tab': True,
                'conac_move': True,
                'partner_id': partner_id,
                'program_code_id': program_code,
                'dependency_id': dependency_id,
                'sub_dependency_id': sub_dependency_id,
                'is_for_approved_payment': True,
                'move_id': self.id,
            })
            budget_lines.append({
                'account_id': journal_id.execercise_debit_account_id.id,
                'coa_conac_id': journal_id.conac_exe_debit_account_id.id,
                'price_unit': balance,
                'exclude_from_invoice_tab': True,
                'conac_move': True,
                'partner_id': partner_id,
                'program_code_id': program_code,
                'dependency_id': dependency_id,
                'sub_dependency_id': sub_dependency_id,
                'is_for_approved_payment': True,
                'move_id': self.id,
            })
        return budget_lines

    def insert_sql(self, lines):

        def _get_column_stored_name():
            query = """
                select column_name from information_schema.columns
                where table_schema = 'public'
                and table_name   = 'account_move' and column_name <> 'id'
            """
            self.env.cr.execute(query, ())
            return [f'"{res[0]}"' for res in self.env.cr.fetchall()]

        def _get_insert_sql():
            columns = _get_column_stored_name()
            column = ", ".join(columns)
            values = ", ".join(["%s"] * len(columns))
            return f"insert into account_move ({column}) values({values}) returning id", columns


        def _get_insert_params(columns, vals):
            def update_vals(updates):
                for k, v in updates:
                    if k not in vals: vals.update({k: v})

            res = []
            # Pone los valores por defecto a los campos default o calculados de odoo
            updates = [
                ('name', '/'),
                ('date', date.today()),
                ('state', 'draft'),
                ('type', 'in_invoice'),
                ('to_check', False),
                ('company_id', self.env.user.company_id.id),
                ('currency_id', self.env.company.currency_id.id),
                ('auto_post', False),
                ('invoice_user_id', self.env.user.id),
                ('invoice_payment_state', 'not_paid'),
                ('invoice_date', date.today()),
                ('invoice_date_due', date.today()),
                ('invoice_sent', False),
                ('extract_state', 'no_extract_state'),
                ('extract_remote_id', -1),
                ('is_tax_closing', False),
                ('tax_report_control_error', False),
                ('l10n_mx_edi_sat_status', 'undefined'),
                ('is_from_reschedule_payment', False),
                ('document_type', 'national'),
                ('commitment_date', date.today()),
                ('is_hide_budget_refund', True),
                ('create_date', 'now()'),
                ('write_date', 'now()'),
                ('create_uid', self.env.user.id),
                ('write_uid', self.env.user.id),
            ]
            update_vals(updates)
            partner_id = vals.get('partner_id')
            if partner_id:
                partner_id = self.env[RES_PARTNER].browse([partner_id])
                vals.update({
                    'invoice_partner_display_name': partner_id.display_name,
                    'baneficiary_key': partner_id.password_beneficiary,
                    'rfc': partner_id.vat,
                })
            for column in columns:
                res.append(vals.get(column[1:-1], None))
            return tuple(res)

        query, columns = _get_insert_sql()
        ids = []
        for line in lines:
            params = _get_insert_params(columns, line)
            self.env.cr.execute(query, params)
            res = (self.env.cr.fetchone() or [None])[0]
            if not res:
                raise ValidationError("Error al insertar el registro account.move")
            ids.append(res)
        return ids

    def _get_salary_payment_data(self, payroll_processing_id, bank, l10n_mx_edi_payment_method_id):
        query_salary = """
            select
                u.partner_id partner_id,
                p.password_beneficiary baneficiary_key,
                coalesce(r.net_salary, 0)::numeric amount,
                receiving_bank_acc_pay_id partner_bank_account_id,
                l10n_mx_edi_payment_method_id,
                e.dependancy_id,
                e.sub_dependancy_id,
                bank_receiving_payment_id payment_bank_id,
                r.id employee_payroll_id,
                r.check_folio_id,
                r.check_final_folio_id
            from employee_payroll_file r, hr_employee e, res_users u, res_partner p
            where r.employee_id = e.id
            and e.user_id = u.id
            and u.partner_id = p.id
            and r.casualties_and_cancellations is null
            and r.payroll_processing_id = %s
            and r.bank_key_name = %s
            and r.l10n_mx_edi_payment_method_id = %s
        """
        self.env.cr.execute(query_salary, (payroll_processing_id.id, bank, l10n_mx_edi_payment_method_id.id))
        return self.env.cr.dictfetchall()

    def _get_benefits_payment_data(self, payroll_processing_id, bank, l10n_mx_edi_payment_method_id):
        query_salary = """
            select r.id,
                u.partner_id partner_id,
                p.password_beneficiary baneficiary_key,
                coalesce(r.total_additional_lending, 0)::numeric amount,
                receiving_bank_acc_pay_id partner_bank_account_id,
                l10n_mx_edi_payment_method_id,
                e.dependancy_id,
                e.sub_dependancy_id,
                bank_receiving_payment_id payment_bank_id,
                r.id employee_payroll_id,
                r.check_folio_id,
                r.check_final_folio_id
            from employee_payroll_file r, hr_employee e, res_users u, res_partner p
            where r.employee_id = e.id
            and e.user_id = u.id
            and u.partner_id = p.id
            and r.casualties_and_cancellations is null
            and r.payroll_processing_id = %s
			and r.bank_key_name = %s
            and r.l10n_mx_edi_payment_method_id = %s
			and exists (
				select 1 from additional_payments_line l where l.payroll_id = r.id
			)
        """
        self.env.cr.execute(query_salary, (payroll_processing_id.id, bank, l10n_mx_edi_payment_method_id.id))
        return self.env.cr.dictfetchall()

    def _get_alimony_payment_data(self, payroll_processing_id, bank, l10n_mx_edi_payment_method_id):
        query_salary = """
            select p.id partner_id,
                p.password_beneficiary baneficiary_key,
                pe.bank_acc_number partner_bank_account_id,
                coalesce(pe.total_pension, 0)::numeric amount,
                bank_acc_number partner_bank_account_id,
                pe.l10n_mx_edi_payment_method_id,
                pe.bank_id payment_bank_id,
                r.id employee_payroll_id
            from res_partner p, pension_payment_line pe, employee_payroll_file r, res_bank b
            where p.id = pe.partner_id
            and pe.payroll_id = r.id
            and pe.bank_id = b.id
            and r.casualties_and_cancellations is null
            and r.payroll_processing_id = %s
            and b.name = %s
            and pe.l10n_mx_edi_payment_method_id = %s
        """
        self.env.cr.execute(query_salary, (payroll_processing_id.id, bank, l10n_mx_edi_payment_method_id.id))
        return self.env.cr.dictfetchall()


    def _get_benefits_payment_check_data(self, payroll_processing_id, banks, l10n_mx_edi_payment_method_id):
        return []

    def _get_salary_payment_check_data(self, payroll_processing_id, criteria, l10n_mx_edi_payment_method_id):
        query_salary = """
            select
                u.partner_id partner_id,
                p.password_beneficiary baneficiary_key,
                coalesce(r.net_salary, 0)::numeric amount,
                l10n_mx_edi_payment_method_id,
                e.dependancy_id,
                e.sub_dependancy_id,
                bank_receiving_payment_id payment_bank_id,
                r.id employee_payroll_id,
                coalesce(r.check_final_folio_id, r.check_folio_id) check_folio_id
            from employee_payroll_file r, hr_employee e, res_users u, res_partner p, check_log cl, checklist c
            where r.employee_id = e.id
            and e.user_id = u.id
            and u.partner_id = p.id
            and coalesce(r.check_final_folio_id, r.check_folio_id) = cl.id
            and r.casualties_and_cancellations is null
            and cl.checklist_id = c.id
            and r.payroll_processing_id = %s
            and c.checkbook_req_id = %s
            and r.l10n_mx_edi_payment_method_id = %s
        """
        self.env.cr.execute(query_salary, (payroll_processing_id.id, criteria, l10n_mx_edi_payment_method_id.id))
        return self.env.cr.dictfetchall()

    def _get_alimony_payment_check_data(self, payroll_processing_id, criteria, l10n_mx_edi_payment_method_id):
        query_salary = """
            select p.id partner_id,
                p.password_beneficiary baneficiary_key,
                pe.check_folio_id,
                coalesce(pe.total_pension, 0)::numeric amount,
                pe.l10n_mx_edi_payment_method_id,
                pe.bank_id payment_bank_id,
                r.id employee_payroll_id
            from res_partner p, pension_payment_line pe, employee_payroll_file r, check_log cl, checklist c
            where p.id = pe.partner_id
            and pe.payroll_id = r.id
            and pe.check_folio_id = cl.id
            and cl.checklist_id = c.id
            and r.casualties_and_cancellations is null
            and r.payroll_processing_id = %s
            and c.checkbook_req_id = %s
            and pe.l10n_mx_edi_payment_method_id = %s
            """
        self.env.cr.execute(query_salary, (payroll_processing_id.id, criteria , l10n_mx_edi_payment_method_id.id))
        return self.env.cr.dictfetchall()

    def _get_payment_data(self, payment_method_id, journal_id, move_id, payment_type, bank, folio, payment_schedule_date):
        payroll_processing_id = move_id.payroll_processing_id
        # Retrieve payment data
        if payment_method_id == self.env.ref(L10N_MX_EDI_PAYMENT_METHOD_TRANSF):
            if payment_type == 'payroll_payment':
                payments = self._get_salary_payment_data(payroll_processing_id, bank, payment_method_id)
            elif payment_type == 'additional_benefits':
                payments = self._get_benefits_payment_data(payroll_processing_id, bank, payment_method_id)
            elif payment_type == 'alimony':
                payments = self._get_alimony_payment_data(payroll_processing_id, bank, payment_method_id)
        elif payment_method_id == self.env.ref(L10N_MX_EDI_PAYMENT_METHOD_CHEQUE):
            if payment_type == 'payroll_payment':
                payments = self._get_salary_payment_check_data(payroll_processing_id, bank, payment_method_id)
            elif payment_type == 'additional_benefits':
                payments = self._get_benefits_payment_check_data(payroll_processing_id, bank, payment_method_id)
            elif payment_type == 'alimony':
                payments = self._get_alimony_payment_check_data(payroll_processing_id, bank, payment_method_id)
        # Update values
        total_amount = 0
        for payment in payments:
            payment.update({
                'state': 'draft',
                'journal_id': journal_id.id,
                'payment_method_id': 1, # revisar
                'payment_date': payment_schedule_date,
                'communication': move_id.name,
                'partner_type': 'supplier',
                'l10n_mx_edi_sat_status': 'undefined',
                'payment_bank_account_id': payment.get('partner_bank_account_id'),
                'payment_issuing_bank_acc_id': None, # revisar
                'batch_folio': folio,
                'folio': move_id.folio,
                'bank_reference': move_id.folio,
                'payment_state': 'draft',
                'payment_request_id': move_id.id,
                'payment_request_type': 'payroll_payment',
                'payroll_request_type': payment_type,
                'fornight': payroll_processing_id.fornight,
                'payment_type': 'outbound',
                'employee_partner_type': 'alimony' if payment_type == 'alimony' else 'employee'
            })
            total_amount += payment.get('amount')
        return payments, total_amount

    def _create_check_protection(self, move_id, journal_bank_id, folio, payment_type, payments, checkbook_request_id):
        """
        Programación de cheques de nómina
        """
        bank_id = journal_bank_id.id
        bank_acc_id = journal_bank_id.bank_account_id.id if journal_bank_id.bank_account_id else False
        dict_batch = {
            'batch_folio': folio,
            'type_of_payment_method': 'checks',
            'type_of_batch': 'pension' if payment_type == 'alimony' else 'nominal',
            'checkbook_req_id': checkbook_request_id,
            'payment_issuing_bank_id': bank_id,
            'payment_issuing_bank_acc_id': bank_acc_id,
            'fornight': move_id.payroll_processing_id.fornight
        }
        batch_id = self.env[PAYMENT_BATCH_SUPPLIER].create(dict_batch)
        for payment_id in self.env[ACCOUNT_PAYMENT].browse(payments):
            check_req = {
                'payment_batch_id': batch_id.id,
                'payment_req_id': move_id.id,
                'amount_to_pay': payment_id.amount,
                'payment_id': payment_id.id,
                'check_folio_id': payment_id.check_folio_id.id,
                'check_status': 'Printed',
                'payment_partner_id': payment_id.partner_id.id
            }
            self.env['check.payment.req'].insert_sql([check_req])

    def create_payroll_payment(self, payment_method_id, journal_id, move_id, payment_type, bank, payment_schedule_date):
        # Generate batch folio
        folio = self.env[IR_SEQUENCE].next_by_code('batch.sheet.folio')
        # Generate payment data
        payments, total_amount = self._get_payment_data(
            payment_method_id,
            journal_id,
            move_id,
            payment_type,
            bank,
            folio,
            payment_schedule_date
        )
        if not payments:
            return False
        # Create payments
        payments = self.env[ACCOUNT_PAYMENT].insert_sql(payments)
        concepts = {
            'payroll_payment': 'Sueldos',
            'additional_benefits': 'Prestaciones Adicionales',
            'alimony': 'Pensiones Alimenticias',
        }
        type_batch = None
        if payment_method_id == self.env.ref(L10N_MX_EDI_PAYMENT_METHOD_TRANSF):
            type_batch = 'payroll'
        elif payment_method_id == self.env.ref(L10N_MX_EDI_PAYMENT_METHOD_CHEQUE):
            self._create_check_protection(move_id, journal_id, folio, payment_type, payments, bank)
            type_batch = 'payroll_checks'
        # Create batch managment
        self.env['batch.management'].create({
            'type': type_batch,
            'journal_id': journal_id.id,
            'folio': folio,
            'payment_schedule_date': payment_schedule_date,
            'amount_total': total_amount,
            'number_of_payments': len(payments),
            'concept': concepts.get(payment_type),
            'payroll_processing_id': move_id.payroll_processing_id.id,
            'target_bank': bank,
            'fortnight': move_id.fornight,
        })
        return folio, len(payments), total_amount

    def payroll_payment_processing_action(self, move_ids):
        # Se obtiene las solicitudes de pago de nómina que se encuentran aprobadas
        # logging.info("Inciando creación de cheques ...")
        lines_ok, lines_err = None, None
        for move_id in move_ids:
            query = """
                select payroll_processing_id from account_move
                where type = 'in_invoice'
                    and is_payroll_payment_request = True
                    and id = %s
            """
            # and payment_state = 'approved_payment'
            self.env.cr.execute(query, (move_id,))
            payrroll_procesing_id = (self.env.cr.fetchone() or [None])[0]
            if payrroll_procesing_id:
                move_id = self.env[ACCOUNT_MOVE].browse([move_id])
                # Procesando cheques
                lines_ok, lines_err = self.payroll_payments_with_checks(move_id, payrroll_procesing_id)
        # logging.info("END")
        check_ok = True if lines_ok else False
        check_err = True if lines_err else False
        message = None
        if not check_ok and not check_err:
            message = '• No se encontraron Solicitudes de Pago automáticas para procesar.'
        elif not check_ok and check_err:
            message = '• No fue posible generar los lotes de pago automáticos por insuficiencia de saldo.'
        elif check_ok and not check_err:
            message = '• Los lotes de pago automáticos fueron generados exitosamente.'
        elif check_ok and check_err:
            message = '• Algunos lotes de pago automáticos no pudieron ser generados y/o validados, vea la sección de cuentas bancarias/fondos sin saldo disponible para ver el detalle.'
        return {
            'name': _('Procesamiento Automático de Pagos'),
            'type': IR_ACTION_WINDOW,
            'res_model': 'payment.processing.result',
            'view_mode': 'form',
            'view_type': 'form',
            'context': {
                'default_message': message,
                'default_payment_processing_check_result_line_ids': lines_ok,
                'default_payment_processing_check_result_error_line_ids': lines_err,
            },
            'views': [(False, 'form')],
            'target': 'new'
        }

    def payroll_payment_processing_reissue_action(self, move_ids):
        # Se obtiene las solicitudes de pago de nómina que se encuentran aprobadas
        # logging.info("Inciando creación de cheques ...")
        lines_ok, lines_err = self.payroll_payments_reissue_with_checks(move_ids)
        check_ok = True if lines_ok else False
        check_err = True if lines_err else False
        message = None
        if not check_ok and not check_err:
            message = '• No se encontraron Solicitudes de Pago automáticas para procesar.'
        elif not check_ok and check_err:
            message = '• No fue posible generar los lotes de pago automáticos por insuficiencia de saldo.'
        elif check_ok and not check_err:
            message = '• Los lotes de pago automáticos fueron generados exitosamente.'
        elif check_ok and check_err:
            message = '• Algunos lotes de pago automáticos no pudieron ser generados y/o validados, vea la sección de cuentas bancarias/fondos sin saldo disponible para ver el detalle.'
        return {
            'name': _('Procesamiento Automático de Pagos'),
            'type': IR_ACTION_WINDOW,
            'res_model': 'payment.processing.result',
            'view_mode': 'form',
            'view_type': 'form',
            'context': {
                'default_message': message,
                'default_payment_processing_check_result_line_ids': lines_ok,
                'default_payment_processing_check_result_error_line_ids': lines_err,
            },
            'views': [(False, 'form')],
            'target': 'new'
        }

    def payroll_payments_with_electronic_transfers_from_layouts(self, payroll_request_id, journal_bank_id, layout_file):
        # Identificación de la nómina
        payroll_processing_id = payroll_request_id.payroll_processing_id.id

        # Mapeo de formatos bancarios a clases de diseño
        layout_mapping = {
            "banorte": LayoutBanorte(),
            "bbva_232": LayoutBBVASIT(),
            "bbva_nomina": LayoutBBVA(),
            "hsbc": LayoutHSBC(),
            "santander_h2h": LayoutSantanderH2H(),
            "santander": LayoutSantander(),
            "banamex": LayoutBanamex(),
        }
        layout_type = layout_mapping.get(journal_bank_id.payroll_load_bank_format)

        if not layout_type:
            return

        # Lectura y obtención de los ids de los recibos de nómina
        lines_ok, lines_err = Context(self.env, layout_type).read_layout_from_payroll_file(payroll_processing_id, layout_file)

        if lines_ok:
            # Generación del folio por lotes
            folio = self.env[IR_SEQUENCE].next_by_code('payroll.payment.batch.sheet.folio')
            # Creación y Validación de los pagos
            for line in lines_ok:
                if line.payment_type == 'pension':
                    ok = self.schedule_pension_payment_with_bank_account(
                        move_id = payroll_request_id,
                        journal_bank_id = journal_bank_id,
                        folio = folio,
                        partner_id = line.res_id,
                        payment_status = line.payment_status,
                        reasons_for_rejection = line.error_message,
                        amount = line.columns.get('amount')
                    )
                    if not ok:
                        line.error_message = \
                            f"Error al generar el pago el beneficiario " \
                            f"{line.columns.get('beneficiary', '-')} " \
                            f"con cuenta {line.columns.get('account_bank', '-')}"
                        lines_err.append(line)
                else:
                    self.schedule_payroll_payment_with_bank_account(
                        move_id = payroll_request_id,
                        journal_bank_id = journal_bank_id,
                        folio = folio,
                        payroll_id = line.res_id,
                        payment_status = line.payment_status,
                        reasons_for_rejection = line.error_message,
                        payroll_type = line.payment_type,
                        amount = line.columns.get('amount')
                    )
                self.env.cr.commit()

        return lines_err

    def schedule_payroll_payment_check(self, move_id, journal_bank_id, folio, payroll_ids, checkbook_request_id):
        """
        Programación de cheques de nómina
        """
        bank_id = journal_bank_id.id
        bank_acc_id = journal_bank_id.bank_account_id.id if journal_bank_id.bank_account_id else False
        dict_batch = {
            'batch_folio': folio,
            'type_of_payment_method': 'checks',
            'type_of_batch': 'nominal',
            'checkbook_req_id' : checkbook_request_id,
            'payment_issuing_bank_id': bank_id,
            'payment_issuing_bank_acc_id': bank_acc_id,
            'fornight': move_id.payroll_processing_id.fornight
        }
        batch_id = self.env[PAYMENT_BATCH_SUPPLIER].create(dict_batch)
        for payroll_id in payroll_ids:
            self.schedule_payroll_payment_with_bank_account(
                move_id=move_id,
                journal_bank_id=journal_bank_id,
                folio=folio,
                payroll_id=payroll_id,
                is_check=True,
                batch_id=batch_id
            )

    def schedule_payroll_payment_reissue_check(self, journal_bank_id, fortnight, folio, checkbook_request_id, move_ids):
        """
        Programación de reexpedición de cheques de nómina
        """
        bank_id = journal_bank_id.id
        bank_acc_id = journal_bank_id.bank_account_id.id if journal_bank_id.bank_account_id else False
        dict_batch = {
            'batch_folio': folio,
            'type_of_payment_method': 'checks',
            'type_of_batch': 'reissue_payroll',
            'checkbook_req_id' : checkbook_request_id,
            'payment_issuing_bank_id': bank_id,
            'payment_issuing_bank_acc_id': bank_acc_id,
            'is_payroll_payment_reissue': True,
            'fornight': fortnight,
        }
        batch_id = self.env[PAYMENT_BATCH_SUPPLIER].create(dict_batch)
        for move_id in self.env[ACCOUNT_MOVE].browse(move_ids):
            self.schedule_payroll_payment_with_bank_account(
                move_id=move_id,
                journal_bank_id=journal_bank_id,
                folio=folio,
                payroll_id=None,
                is_check=True,
                batch_id=batch_id
            )

    def schedule_payroll_payment_with_bank_account(self,
        move_id,
        journal_bank_id,
        folio,
        payroll_id,
        payment_status = None,
        reasons_for_rejection = None,
        payroll_type = 'sueldo',
        amount = None,
        is_check = False,
        batch_id = None
    ):
        """
        Crea un objeto account.payment para los pagos de nómina

        Params:
            move_id (account.move()):
                registro de la solicitud de pago asociada.

            journal_bank_id (account.journal()):
                registro del diario de la cuenta bancaria asociado al pago.

            folio (str):
                valor del folio por lotes asociado al pago

            payroll_id (int):
                valor del id del registro de employee.payroll.file asociado al
                beneficiario del pago

            payment_status (boolean):
                bandera para saber si el pago se debe validar o rechazar. Si su
                valor es None, se ignora

            reasons_for_rejection (str):
                mensaje de rechazo del pago, solo se utiliza si la bandera
                payment_status es True

            payroll_type (str):
                valor del tipo de pago (sueldo, prestacion, pension)

            amount (float):
                importe del pago

            is_check (boolean):
                bandera para la creación de un pago por cheques

            batch_id (int):
                valor del id de la solicitud de chequera asociada al cheque

        Returns:
            int (): id del objeto account.payment creado
        """
        # Si payroll_id existe, se toman los datos del recibo de nómina
        # (FLUJO NORMAL)
        payment = None
        if payroll_id:
            query_recibo_nomina = """
                select
                    u.partner_id partner_id,
                    p.password_beneficiary baneficiary_key,
                    coalesce(r.net_salary, 0)::numeric amount,
                    receiving_bank_acc_pay_id partner_bank_account_id,
                    l10n_mx_edi_payment_method_id,
                    e.dependancy_id,
                    e.sub_dependancy_id,
                    bank_receiving_payment_id payment_bank_id,
                    r.id employee_payroll_id,
                    r.check_folio_id,
                    r.check_final_folio_id
                from employee_payroll_file r, hr_employee e, res_users u, res_partner p
                where r.employee_id = e.id
                and e.user_id = u.id
                and u.partner_id = p.id
                and r.id = %s
            """
            # Obtención de los datos a crear
            self.env.cr.execute(query_recibo_nomina, (payroll_id,))
            payment = self.env.cr.dictfetchone()
        # Si payroll_id existe, se toman los datos de la solicitud de pago
        # (FLUJO DE REEXPEDICIÓN)
        else:
            query_solicitud_pago = """
                select partner_id,
                    p.password_beneficiary baneficiary_key,
                    coalesce(m.amount_total, 0)::numeric amount,
                    payment_bank_account_id partner_bank_account_id,
                    l10n_mx_edi_payment_method_id,
                    m.dependancy_id,
                    m.sub_dependancy_id,
                    payment_bank_id,
                    check_folio_id,
                    adjustment_case_id
                from account_move m, res_partner p
                where m.partner_id = p.id
                    and m.id = %s
            """
            # Obtención de los datos a crear
            self.env.cr.execute(query_solicitud_pago, (move_id.id,))
            payment = self.env.cr.dictfetchone()
            # Se actualiza la bandera para saber que el pago esta asociado a una
            # reexpedición
            payment.update({'is_payroll_payment_reissue': True})

        payroll_request_type = {
            'sueldo': 'payroll_payment',
            'prestacion': 'additional_benefits',
            'pension': 'alimony'
        }
        # Si el tipo de pago es distinto a sueldo, se coloca el monto que viene
        # del layout
        if payroll_type != "sueldo":
            payment.update({'amount': amount})
        check_final_folio = payment.get('check_final_folio_id')
        check_folio = payment.get('check_folio_id') if not check_final_folio else check_final_folio
        payment.update({
            'state': 'draft',
            'journal_id': journal_bank_id.id,
            'payment_method_id': 1, # revisar
            'payment_date': datetime.today(),
            'communication': move_id.name,
            'partner_type': 'supplier',
            # 'l10n_mx_edi_partner_bank_id': False,
            'l10n_mx_edi_sat_status': 'undefined',
            'payment_bank_account_id': payment.get('partner_bank_account_id'),
            'payment_issuing_bank_acc_id': None, # revisar
            'batch_folio': folio,
            'folio': move_id.folio,
            'bank_reference': move_id.folio,
            'payment_state': 'for_payment_procedure',
            'payment_request_id': move_id.id,
            'payment_request_type': 'payroll_payment',
            'fornight': move_id.payroll_processing_id.fornight,
            'payroll_request_type': payroll_request_type.get(payroll_type),
            'payment_type': 'outbound',
            'check_folio_id': check_folio,
            'employee_partner_type': 'employee' # if payroll_type == 'payroll_payment' else 'alimony'
        })
        # Creación del pago
        payment_id = self.env[ACCOUNT_PAYMENT].insert_sql([payment])[0]

        # Flujo alterno para cheques
        if is_check:
            # Creación de la solicitud de cheques
            check_req = {
                'payment_batch_id': batch_id.id,
                'payment_req_id': move_id.id,
                'amount_to_pay': payment.get('amount'),
                'payment_id': payment_id,
                'check_folio_id': check_folio,
                'check_status': 'Printed',
                'payment_partner_id': payment.get('partner_id')
            }
            if not payroll_id:
                check_req.update({'is_payroll_payment_reissue': True})
            self.env['check.payment.req'].insert_sql([check_req])

        # Flujo de Validación/Rechazo del pago
        if payment_status != None:
            # Si el status del pago es verdadero, se debe validar
            if payment_status:
                self.env[ACCOUNT_PAYMENT].payroll_paymet_post([payment_id], True)
            # Caso contrario se rechaza y se notifica el motivo de rechazo
            else:
                self.env[ACCOUNT_PAYMENT].paymet_reject(payment_id, reasons_for_rejection)

        query_insert_inovice_ids = """
            insert into account_invoice_payment_rel(payment_id, invoice_id) values (%s, %s)
        """
        self.env.cr.execute(query_insert_inovice_ids, (payment_id, move_id.id))
        # Registra el monto comprometido de la cuenta
        journal_bank_id.bank_account_id.add_expense(payment.get('amount'))
        return payment_id

    def schedule_pension_payment_with_bank_account(self,
        move_id,
        journal_bank_id,
        folio,
        partner_id,
        payment_status = None,
        reasons_for_rejection = None,
        amount = None
    ):
        """
        Crea un objeto account.payment para los pagos de nómina

        Params:
            move_id (account.move()):
                registro de la solicitud de pago asociada.

            journal_bank_id (account.journal()):
                registro del diario de la cuenta bancaria asociado al pago.

            folio (str):
                valor del folio por lotes asociado al pago

            payroll_id (int):
                valor del id del registro de employee.payroll.file asociado al
                beneficiario del pago

            payment_status (boolean):
                bandera para saber si el pago se debe validar o rechazar. Si su
                valor es None, se ignora

            reasons_for_rejection (str):
                mensaje de rechazo del pago, solo se utiliza si la bandera
                payment_status es True

            payroll_type (str):
                valor del tipo de pago (sueldo, prestacion, pension)

            amount (float):
                importe del pago

        Returns:
            int (): id del objeto account.payment creado
        """
        payment_method_id = self.env.ref(L10N_MX_EDI_PAYMENT_METHOD_TRANSF).id
        query_recibo_nomina = """
            select p.id partner_id,
                p.password_beneficiary baneficiary_key,
                pe.bank_acc_number partner_bank_account_id,
                pe.l10n_mx_edi_payment_method_id,
                pe.bank_id payment_bank_id,
                r.id employee_payroll_id
            from res_partner p, pension_payment_line pe, employee_payroll_file r
            where p.id = partner_id
            and pe.payroll_id = r.id
            and p.id = %s
            and r.payroll_processing_id = %s
            and pe.l10n_mx_edi_payment_method_id = %s
        """
        # Obtención de los datos a crear
        self.env.cr.execute(query_recibo_nomina, (partner_id, move_id.payroll_processing_id.id, payment_method_id))
        payment = self.env.cr.dictfetchone()
        if payment:
            payment.update({
                'amount': amount,
                'state': 'draft',
                'journal_id': journal_bank_id.id,
                'payment_method_id': 1, # revisar
                'payment_date': datetime.today(),
                'communication': move_id.name,
                'partner_type': 'supplier',
                # 'l10n_mx_edi_partner_bank_id': False,
                'l10n_mx_edi_sat_status': 'undefined',
                'payment_bank_account_id': payment.get('partner_bank_account_id'),
                'payment_issuing_bank_acc_id': None, # revisar
                'batch_folio': folio,
                'folio': move_id.folio,
                'bank_reference': move_id.folio,
                'payment_state': 'for_payment_procedure',
                'payment_request_id': move_id.id,
                'payment_request_type': 'payroll_payment',
                'fornight': move_id.payroll_processing_id.fornight,
                'payroll_request_type': 'alimony',
                'payment_type': 'outbound',
                'employee_partner_type': 'employee' # if payroll_type == 'payroll_payment' else 'alimony'
            })
            # Creación del pago
            payment_id = self.env[ACCOUNT_PAYMENT].insert_sql([payment])[0]

            # Flujo de Validación/Rechazo del pago
            if payment_status != None:
                # Si el status del pago es verdadero, se debe validar
                if payment_status:
                    self.env[ACCOUNT_PAYMENT].payroll_paymet_post([payment_id], True)
                # Caso contrario se rechaza y se notifica el motivo de rechazo
                else:
                    self.env[ACCOUNT_PAYMENT].paymet_reject(payment_id, reasons_for_rejection)

            query_insert_inovice_ids = """
                insert into account_invoice_payment_rel(payment_id, invoice_id) values (%s, %s)
            """
            self.env.cr.execute(query_insert_inovice_ids, (payment_id, move_id.id))
            return payment_id
        return False

    def get_total_payroll_payment_request(self, ids):
        """
        Obtiene el monto total a pagar de los recibos de nómina
        """
        query = """
            select sum(net_salary)::numeric from employee_payroll_file where id in %s
        """
        self.env.cr.execute(query, (tuple(ids),))
        return (self.env.cr.fetchone() or [None])[0]

    def _get_salary_payments_with_checks(self, payroll_processing_id, payment_method_id):
        query = """
            select distinct cl.bank_id journal_id, c.checkbook_req_id
            from employee_payroll_file r, check_log cl, checklist c
            where coalesce(r.check_final_folio_id, r.check_folio_id) = cl.id
            and cl.checklist_id = c.id
            and payroll_processing_id = %s
            and l10n_mx_edi_payment_method_id = %s
            --and cl.status = 'Printed'
        """
        self.env.cr.execute(query, (payroll_processing_id, payment_method_id))
        return [('payroll_payment', *r) for r in self.env.cr.fetchall()]

    def _get_benefits_payments_with_checks(self, payroll_processing_id, payment_method_id):
        return []

    def _get_alimony_payments_with_checks(self, payroll_processing_id, payment_method_id):
        query = """
            select distinct cl.bank_id journal_id, c.checkbook_req_id
            from employee_payroll_file r, pension_payment_line pe, check_log cl, checklist c
            where r.id = pe.payroll_id
            and pe.check_folio_id = cl.id
            and cl.checklist_id = c.id
            and payroll_processing_id = %s
            and pe.l10n_mx_edi_payment_method_id = %s
            --and cl.status = 'Printed'
        """
        self.env.cr.execute(query, (payroll_processing_id, payment_method_id))
        return [('alimony', *r) for r in self.env.cr.fetchall()]


    def _get_payroll_payments_with_checks(self, payroll_processing_id):
        payment_method_id = self.env.ref(L10N_MX_EDI_PAYMENT_METHOD_CHEQUE).id
        payments = self._get_salary_payments_with_checks(payroll_processing_id, payment_method_id)
        payments += self._get_benefits_payments_with_checks(payroll_processing_id, payment_method_id)
        payments += self._get_alimony_payments_with_checks(payroll_processing_id, payment_method_id)
        return payments

    def payroll_payments_with_checks(self, move_id, payroll_processing_id):
        payment_method_id = self.env.ref(L10N_MX_EDI_PAYMENT_METHOD_CHEQUE)
        groups = self._get_payroll_payments_with_checks(payroll_processing_id)
        lines_ok = []
        lines_err = []
        # Generar los grupos de pagos
        for payment_type, journal_id, checkbook_request_id in groups:
            journal_id = self.env[ACCOUNT_JOURNAL].browse(journal_id)
            folio, number, amount = self.create_payroll_payment(
                payment_method_id,
                journal_id,
                move_id,
                payment_type,
                checkbook_request_id,
                fields.Date().today(),
            )
            concepts = {
                'payroll_payment': 'Sueldos',
                'additional_benefits': 'Prestaciones Adicionales',
                'alimony': 'Pensiones Alimenticias',
            }
            lines_ok.append((0, 0, {
                    'checkbook_request_id': checkbook_request_id,
                    'journal_id': journal_id.id,
                    'folio': folio,
                    'number_payments': number,
                    'total_amount': amount,
                    'payment_type': concepts.get(payment_type)
                }))
        return lines_ok, lines_err

    def payroll_payments_reissue_with_checks(self, move_ids):
        """
        Método para la automatización de procesamiento de pagos de reexpedición
        de cheques de nómina
        """
        # Diccionario con los valores actualizados del disponible de la cuenta bancaria
        journal_bank_id_balance = {}

        def generate_groups():
            # Se obtiene las chequeras registradas por las que se agruparan los lotes
            payment_method_id = self.env.ref(L10N_MX_EDI_PAYMENT_METHOD_CHEQUE).id
            groups = []
            query = """
                select cl.checklist_id, cl.bank_id, cr.id checkbook_req_id, m.fornight, m.id
                from account_move m, check_log cl, checklist c, checkbook_request cr
                where m.check_folio_id = cl.id
					and cl.checklist_id = c.id
                    and c.checkbook_req_id = cr.id
                    and m.id in %s
                    and m.payment_state = 'approved_payment'
                    and (m.batch_folio = 0 or m.batch_folio is null)
                    and l10n_mx_edi_payment_method_id = %s
                order by cl.checklist_id, cl.bank_id, checkbook_req_id
            """
            self.env.cr.execute(query, (tuple(move_ids), payment_method_id))
            # Por cada chequera, se obtiene los recibos de nomina que salen por esa chequera
            groups = Dictionary()
            for check_list_id, bank_id, checkbook_req_id, fortnight, id in self.env.cr.fetchall():
                groups.add_list((bank_id, checkbook_req_id, fortnight), id)
            return [(g[0], g[1], g[2], ids)for g, ids in groups.items()]

        def check_balances(journal_bank_id, ids):
            # Se obtiene la información del caché en memoria del balance de la cuenta
            # bancaria
            if not journal_bank_id_balance.get(journal_bank_id):
                __, res = journal_bank_id.check_balances(0)
                journal_bank_id_balance[journal_bank_id] = res
            res = journal_bank_id_balance.get(journal_bank_id)
            balance_available, minimum_balance, total_amount_journal = res
            # Se obtiene el monto total del grupo de cheques
            total_amount = self.get_total_payroll_payment_request(ids)
            # Se valida que tenga disponible la cuenta
            if (balance_available - total_amount) < minimum_balance:
                # Se actualiza el monto requerido
                journal_bank_id_balance[journal_bank_id] = (
                    balance_available,
                    minimum_balance,
                    round(total_amount_journal + total_amount, 2),
                )
                return False, journal_bank_id_balance.get(journal_bank_id)
            # Si la cuenta tiene disponible se hace la actualización de este
            journal_bank_id_balance[journal_bank_id] = (
                round(balance_available - total_amount, 2),
                minimum_balance,
                round(total_amount_journal + total_amount, 2),
                )
            return True, journal_bank_id_balance.get(journal_bank_id)


        def register_payment(checkbook_request_id, journal_bank_id, fortnight, folio, payment_ids):
            # Creación de los objetos de pago
            self.schedule_payroll_payment_reissue_check(
                journal_bank_id,
                fortnight,
                folio,
                checkbook_request_id,
                payment_ids
            )
            query = """
                update account_move set
                    payment_state = 'for_payment_procedure',
                    date_for_payment_procedure = current_date
                where id in %s
            """
            self.env.cr.execute(query, (tuple(move_ids),))
            # # Actualiza el status del cheque a impreso
            # query_update_checks = """
            #     update check_log set status = 'Printed' where id in (
            #         select check_folio_id from employee_payroll_file
            #         where id in %s and check_final_folio_id is null
            #         union all
            #         select check_final_folio_id from employee_payroll_file
            #         where id in %s and check_final_folio_id is not null
            #     )
            # """
            # self.env.cr.execute(query_update_checks, (tuple(payroll_ids), tuple(payroll_ids)))

        lines_ok = []
        lines_err = []
        # Generar los grupos de pagos
        for bank_id, checkbook_request_id, fortnight, payment_ids in generate_groups():
            journal_bank_id = self.env[ACCOUNT_JOURNAL].browse(bank_id)
            # Verificar el saldo de la cuenta
            # ok, res = check_balances(journal_bank_id, receipt_ids)
            ok, res = True, None # Se omite validación de disponibilidad de la cuenta bancaria
            if ok:
                # Se generan los lotes y obtiene el folio
                folio = self.env[ACCOUNT_MOVE].generate_payment_batch('payroll', payment_ids)
                # Se registrar el pago
                register_payment(checkbook_request_id, journal_bank_id, fortnight, folio, payment_ids)
                # Se agregan a la lista de lineas exitosas
                lines_ok.append((0, 0, {
                        'checkbook_request_id': checkbook_request_id,
                        'journal_id': journal_bank_id.id,
                        'folio': folio
                    }))
            # Si no hay saldo suficiente
            else:
                # Se agregan a las lineas de error
                balance_available, minimum_balance, total_amount = res
                lines_err.append((0, 0, {
                        'checkbook_request_id': checkbook_request_id,
                        'journal_id': journal_bank_id.id,
                        'balance_available': balance_available,
                        'minimum_balance': minimum_balance,
                        'total_amount': total_amount,
                    }))
        return lines_ok, lines_err


    def inc_number_of_validated_receipts(self):
        query = """
            update account_move set number_of_validated_receipts = number_of_validated_receipts + 1
            where id = %s
            returning number_of_payment_receipts, number_of_validated_receipts
        """
        self.env.cr.execute(query, (self.id,))
        res = self.env.cr.fetchall()
        number_of_payment_receipts, number_of_validated_receipts = res[0]
        if number_of_payment_receipts == number_of_validated_receipts:
            query = """
                update account_move set payment_state = 'paid'
                where move_id = %s
            """
            self.env.cr.execute(query, (self.id,))

    def get_payment_lines_file(self):
        filename = "SolicitudDePagoDeNomina_%s_%s.xlsx" % (
            self.payroll_processing_id.fornight,
            self.period_start.strftime("%Y"),
        )
        return {
            'type' : 'ir.actions.act_url',
            'url': '/web/binary/download_payroll_request_lines?id=%s&filename=%s'%(self.id, filename),
            'target': 'self',
        }

    @api.model
    def get_payroll_payment_lines(self):
        workbook = Workbook()
        # Líneas de Factura
        ws1 = workbook.active
        ws1.title = "Líneas de Factura"
        # ws1.title("Líneas de Factura")
        self.get_payroll_register_lines_files(ws1)
        # Apuntes Contables Gasto
        ws2 = workbook.create_sheet("Apuntes Contables Gasto")
        # ws2.title("Apuntes Contables Gasto")
        self.get_payroll_move_lines_files(ws2)
        # Apuntes Contables Pago
        ws3 = workbook.create_sheet("Apuntes Contables Pago")
        self.get_payroll_payment_lines_files(ws3)
        # ws3.title("Apuntes Contables Pago")

        payment_file = io.BytesIO()
        workbook.save(payment_file)
        return base64.encodestring(payment_file.getvalue())

    @api.model
    def get_payroll_register_lines_files(self, worksheet):
        query_len = """
            select count(1) from account_move_line
            where move_id = %s and exclude_from_invoice_tab is False
        """
        query = """
            select coalesce(p.program_code, '') program_code,
                coalesce(a.code, '') account,
                coalesce(d.dependency, '') dependency,
                coalesce(d.description, '') dependency_desc,
                coalesce(sd.sub_dependency, '') sub_dependency,
                coalesce(sd.description, '') sub_dependency_desc,
                l.quantity cantidad,
                l.price_total::numeric total
            from account_move_line l
                left join account_account a on l.account_id = a.id
                left join dependency d on l.dependency_id = d.id
                left join sub_dependency sd on l.sub_dependency_id = sd.id
                left join program_code p on l.program_code_id = p.id
            where move_id = %s
            and exclude_from_invoice_tab is False
            order by l.id
            limit %s offset %s
        """
        self.env.cr.execute(query_len, (self.id,))
        size = (self.env.cr.fetchone() or [0])[0]

        worksheet.column_dimensions['A'].width = (60) * 1.2
        worksheet.column_dimensions['B'].width = (15) * 1.2
        worksheet.column_dimensions['C'].width = (12) * 1.2
        worksheet.column_dimensions['D'].width = (70) * 1.2
        worksheet.column_dimensions['E'].width = (16) * 1.2
        worksheet.column_dimensions['F'].width = (70) * 1.2
        worksheet.column_dimensions['G'].width = (10) * 1.2
        worksheet.column_dimensions['H'].width = (13) * 1.2

        worksheet.column_dimensions['H'].number_format = NUMBER_FORMAT

        worksheet.append((
            STR_CODIGO_PROGRAMATICO,
            "Cuenta",
            "Dependencia",
            STR_DESC_DEPENDENCIA,
            STR_SUB_DEPENDENCIA,
            STR_DESC_SUB_DEPENDENCIA,
            "Cantidad",
            "Total",
        ))

        worksheet['A1'].font = Font(bold=True, name= 'Arial')
        worksheet['B1'].font = Font(bold=True, name= 'Arial')
        worksheet['C1'].font = Font(bold=True, name= 'Arial')
        worksheet['D1'].font = Font(bold=True, name= 'Arial')
        worksheet['E1'].font = Font(bold=True, name= 'Arial')
        worksheet['F1'].font = Font(bold=True, name= 'Arial')
        worksheet['G1'].font = Font(bold=True, name= 'Arial')
        worksheet['H1'].font = Font(bold=True, name= 'Arial')

        total_total = 0

        limit = 20000
        for page in range(math.floor(size / limit) + 1):
            self.env.cr.execute(query, (self.id, limit, page*limit))
            for res in self.env.cr.fetchall():
                worksheet.append(res)
                total_total += float(res[7])

        # Totales
        worksheet.append(())
        worksheet.append(('', '', '', '', '', '', 'TOTAL', total_total))
        col = len(worksheet['F'])
        worksheet['G%s' % col].font = Font(bold=True, name= 'Arial')

    @api.model
    def get_payroll_move_lines_files(self, worksheet):
        query_len = """
            select count(1) from account_move_line where move_id = %s
        """
        query = """
            select coalesce(p.program_code, '') program_code,
                coalesce(a.code, '') account,
                coalesce(a.name, '') account_desc,
                coalesce(d.dependency, '') dependency,
                coalesce(d.description, '') dependency_desc,
                coalesce(sd.sub_dependency, '') sub_dependency,
                coalesce(sd.description, '') sub_dependency_desc,
                l.name line_desc,
                l.debit::numeric debit,
                l.credit::numeric credit
            from account_move_line l
                left join account_account a on l.account_id = a.id
                left join dependency d on l.dependency_id = d.id
                left join sub_dependency sd on l.sub_dependency_id = sd.id
                left join program_code p on l.program_code_id = p.id
            where move_id = %s
            order by l.id
            limit %s offset %s
        """
        self.env.cr.execute(query_len, (self.id,))
        size = (self.env.cr.fetchone() or [0])[0]

        worksheet.column_dimensions['A'].width = (60) * 1.2
        worksheet.column_dimensions['B'].width = (15) * 1.2
        worksheet.column_dimensions['C'].width = (70) * 1.2
        worksheet.column_dimensions['D'].width = (12) * 1.2
        worksheet.column_dimensions['E'].width = (70) * 1.2
        worksheet.column_dimensions['F'].width = (16) * 1.2
        worksheet.column_dimensions['G'].width = (70) * 1.2
        worksheet.column_dimensions['H'].width = (50) * 1.2
        worksheet.column_dimensions['I'].width = (16) * 1.2
        worksheet.column_dimensions['J'].width = (16) * 1.2

        worksheet.column_dimensions['I'].number_format = NUMBER_FORMAT
        worksheet.column_dimensions['J'].number_format = NUMBER_FORMAT

        worksheet.append((
            STR_CODIGO_PROGRAMATICO,
            "Cuenta",
            "Nombre",
            "Dependencia",
            STR_DESC_DEPENDENCIA,
            STR_SUB_DEPENDENCIA,
            STR_DESC_SUB_DEPENDENCIA,
            "Descripción",
            "Debe",
            "Haber",
        ))

        worksheet['A1'].font = Font(bold=True, name= 'Arial')
        worksheet['B1'].font = Font(bold=True, name= 'Arial')
        worksheet['C1'].font = Font(bold=True, name= 'Arial')
        worksheet['D1'].font = Font(bold=True, name= 'Arial')
        worksheet['E1'].font = Font(bold=True, name= 'Arial')
        worksheet['F1'].font = Font(bold=True, name= 'Arial')
        worksheet['G1'].font = Font(bold=True, name= 'Arial')
        worksheet['H1'].font = Font(bold=True, name= 'Arial')
        worksheet['I1'].font = Font(bold=True, name= 'Arial')
        worksheet['J1'].font = Font(bold=True, name= 'Arial')

        total_debit = 0
        total_credit = 0

        limit = 5000
        for page in range(math.floor(size / limit) + 1):
            self.env.cr.execute(query, (self.id, limit, page*limit))
            for res in self.env.cr.fetchall():
                worksheet.append(res)
                total_debit += float(res[8])
                total_credit += float(res[9])

        # Totales
        worksheet.append(())
        worksheet.append(('', '', '', '', '','', '', 'TOTAL', total_debit, total_credit))
        col = len(worksheet['H'])
        worksheet['H%s' % col].font = Font(bold=True, name= 'Arial')

    @api.model
    def get_payroll_payment_lines_files(self, worksheet):
        query_len = """
            select count(1) from account_move m, account_move_line l
            where m.id = l.move_id and m.ref = %s
        """
        self.env.cr.execute(query_len, (self.name,))
        size = (self.env.cr.fetchone() or [0])[0]
        query = """
            select coalesce(p.program_code, '') program_code,
                coalesce(a.code, '') account,
                coalesce(a.name, '') account_desc,
                coalesce(d.dependency, '') dependency,
                coalesce(d.description, '') dependency_desc,
                coalesce(sd.sub_dependency, '') sub_dependency,
                coalesce(sd.description, '') sub_dependency_desc,
                l.name line_desc,
                l.debit::numeric debit,
                l.credit::numeric credit
            from account_move_line l
                join account_move m on l.move_id = m.id
                left join account_account a on l.account_id = a.id
                left join dependency d on l.dependency_id = d.id
                left join sub_dependency sd on l.sub_dependency_id = sd.id
                left join program_code p on l.program_code_id = p.id
            where m.ref = %s
            order by l.id
            limit %s offset %s
        """

        worksheet.column_dimensions['A'].width = (60) * 1.2
        worksheet.column_dimensions['B'].width = (15) * 1.2
        worksheet.column_dimensions['C'].width = (70) * 1.2
        worksheet.column_dimensions['D'].width = (12) * 1.2
        worksheet.column_dimensions['E'].width = (70) * 1.2
        worksheet.column_dimensions['F'].width = (16) * 1.2
        worksheet.column_dimensions['G'].width = (70) * 1.2
        worksheet.column_dimensions['H'].width = (50) * 1.2
        worksheet.column_dimensions['I'].width = (16) * 1.2
        worksheet.column_dimensions['J'].width = (16) * 1.2

        worksheet.column_dimensions['I'].number_format = NUMBER_FORMAT
        worksheet.column_dimensions['J'].number_format = NUMBER_FORMAT

        worksheet.append((
            STR_CODIGO_PROGRAMATICO,
            "Cuenta",
            "Nombre",
            "Dependencia",
            STR_DESC_DEPENDENCIA,
            STR_SUB_DEPENDENCIA,
            STR_DESC_SUB_DEPENDENCIA,
            "Descripción",
            "Debe",
            "Haber",
        ))

        worksheet['A1'].font = Font(bold=True, name= 'Arial')
        worksheet['B1'].font = Font(bold=True, name= 'Arial')
        worksheet['C1'].font = Font(bold=True, name= 'Arial')
        worksheet['D1'].font = Font(bold=True, name= 'Arial')
        worksheet['E1'].font = Font(bold=True, name= 'Arial')
        worksheet['F1'].font = Font(bold=True, name= 'Arial')
        worksheet['G1'].font = Font(bold=True, name= 'Arial')
        worksheet['H1'].font = Font(bold=True, name= 'Arial')
        worksheet['I1'].font = Font(bold=True, name= 'Arial')
        worksheet['J1'].font = Font(bold=True, name= 'Arial')

        total_debit = 0
        total_credit = 0

        limit = 20000
        for page in range(math.floor(size / limit) + 1):
            self.env.cr.execute(query, (self.name, limit, page*limit))
            for res in self.env.cr.fetchall():
                worksheet.append(res)
                total_debit += float(res[8])
                total_credit += float(res[9])

        # Totales
        worksheet.append(())
        worksheet.append(('', '', '', '', '','', '', 'TOTAL', total_debit, total_credit))
        col = len(worksheet['H'])
        worksheet['H%s' % col].font = Font(bold=True, name= 'Arial')

    def upload_payroll_layouts_action(self):
        return {
            'name': _('Load Bank Layout'),
            'res_model': 'load.bank.layout.supplier.payment',
            'view_mode': 'form',
            'view_id': self.env.ref('jt_supplier_payment.view_load_bank_layout_payroll_payment_form').id,
            'context': {
                'default_payment_request_id': self.id,
            },
            'target': 'new',
            'type': IR_ACTION_WINDOW,
        }

    def _post(self, move_id, journal_id, date=datetime.today()):
        journal_id = self.env['account.journal'].browse([journal_id])
        move_name = ''
        sequence = journal_id.sequence_id
        seq = journal_id.sequence_id.prefix.split('/')
        # Generación del nombre del asiento contable
        if(seq[0] != str(journal_id.code)):
            aux = sequence.with_context(ir_sequence_date=date).next_by_id()
            name_aux = aux.split('/')
            move_name = str(journal_id.code)
            for i in range (1,len(name_aux)):
                move_name += "/" + name_aux[i]
        else:
            move_name = sequence.with_context(ir_sequence_date=date).next_by_id()
        # Actualización de de la poliza contable
        query_move = """
            update account_move set state = 'posted',
                name = %s,
                invoice_payment_ref = %s
            where id = %s
        """
        # Actualización de los apuntes contables
        self.env.cr.execute(query_move, (move_name, move_name, move_id))
        query_lines = """
            update account_move_line set
                parent_state = 'posted',
                move_name = %s
            where move_id = %s
        """
        self.env.cr.execute(query_lines, (move_name, move_id,))

    def create_payroll_payment_reissue_request(self,
            employee_id,
            payroll_processing_id,
            account_id,
            check_old_folio_id,
            check_new_folio_id,
            total_amount,
            adjustment_case_id,
            **kwargs):
        # Obtención de parámetros para la creación de la solicitud
        parameters = self.env['payroll.payment.parameters'].search([])
        if not (parameters and parameters.partner_id):
            raise ValidationError("Configure el beneficiario de la Solicitud de Pago en el Ménu de Parámetros de Pago de Nómina.")
        # Diario de la solicitud de pago
        journal_id = parameters.journal_id
        if not journal_id:
            raise ValidationError("Configure el diario de la Solicitud de Pago en el Ménu de Parámetros de Pago de Nómina.")
        # Obtención del partner a partir del empleado
        query = """
            select u.partner_id from hr_employee e, res_users u
            where e.user_id = u.id and e.id = %s
        """
        self.env.cr.execute(query, (employee_id.id,))
        partner_id = (self.env.cr.fetchone() or [None])[0]
        partner_id = self.env[RES_PARTNER].browse([partner_id])
        # Clave de egreso para cuenta contable
        egress_key_id = self.env['egress.keys'].search([('key', '=', 'CC')])
        # Valores de la solicitud de pago
        vals = {
            'payroll_processing_id': payroll_processing_id.id,
            'partner_id': partner_id.id,
            'invoice_date': fields.Date.today(),
            'journal_id': journal_id.id if journal_id else None,
            'fornight' : payroll_processing_id.fornight,
            'payroll_register_user_id': self.env.user.id,
            'payroll_send_user_id': self.env.user.id,
            'type' : 'in_invoice',
            'dependancy_id': employee_id.dependancy_id.id,
            'sub_dependancy_id': employee_id.sub_dependancy_id.id,
            'period_start' : payroll_processing_id.period_start or None,
            'period_end' : payroll_processing_id.period_end or None,
            'payment_state': 'registered',
            'is_payroll_payment_reissue': True,
            'user_registering_id': self.env.user.id,
            'check_folio_id': check_new_folio_id.id,
            'payment_bank_id': check_new_folio_id.bank_account_id.bank_id.id,
            'l10n_mx_edi_payment_method_id': self.env.ref(L10N_MX_EDI_PAYMENT_METHOD_CHEQUE).id,
            'related_check_history': check_old_folio_id.folio_ch,
            'adjustment_case_id': adjustment_case_id.id,
            'invoice_line_ids': [(0, 0, {
                'egress_key_id': egress_key_id and egress_key_id.id or None,
                'account_id': account_id,
                'dependency_id': employee_id.dependancy_id.id,
                'sub_dependency_id': employee_id.sub_dependancy_id.id,
                'price_unit': total_amount,
                'quantity': 1,
                'exclude_from_invoice_tab': False,
            })],
        }
        # Actualización de los valores pasados por los kwargs
        for k, v in kwargs.items():
            vals.update({k: v})
        # Creación de la solicitud de pago
        move_id = self.create(vals).with_context(check_move_validity=False)
        # Cambia al status aprobada para pago la solicitud
        self.approve_for_payment([move_id.id])
        # Actualiza el status del cheque a Impreso
        # check_new_folio_id.status = 'Printed'
        return move_id