# -*- coding: utf-8 -*-

import base64
from collections import Counter
import io
import logging
from odoo import models, fields, api, _
from datetime import datetime
from odoo.exceptions import UserError, ValidationError

from odoo.addons.sd_utils.models import message_wizard
from odoo.addons.siif_supplier_payment.tools.utils import Dictionary
from odoo.addons.siif_supplier_payment.tools import cancel_wizard

from odoo.addons.siif_payroll_payment.tools.layouts import Context
from odoo.addons.siif_payroll_payment.tools.layouts.bancos import LayoutBanorte, LayoutBBVASIT, LayoutBBVA, LayoutHSBC, LayoutSantanderH2H, LayoutSantander, LayoutBanamex


FORTNIGHT = [(str(i).zfill(2),) * 2 for i in range(1, 25)]

class BatchManagement(models.Model):

    _name = 'batch.management'
    _description = 'Batch Management'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'folio'

    state = fields.Selection(
        [
            ('without_resources', 'Without resources'),
            ('requested_resources', 'Requested Resources'),
            ('with_resources', 'With resources'),
            ('for_payment_procedure', 'For Payment Procedure'),
            ('processed', 'Processed'),
            ('cancelled', 'Cancelled'),
            ('rejected_resources', 'Rejected resources')
        ], default='without_resources', string='State', tracking=True
    )
    type = fields.Selection(
        [
            ('manual', 'Manual'),
            ('automatic', 'Automatic'),
            ('general_checks', 'General Checks'),
            ('payroll', 'Payroll'),
            ('payroll_checks','Payroll Checks'),
        ], string="Type"
    )
    fortnight = fields.Selection(FORTNIGHT, string="Fortnight")

    target_bank = fields.Char(string="Target Bank")

    folio = fields.Char('Folio')
    journal_id = fields.Many2one('account.journal', string='Account Journal')
    concept = fields.Char('Concept')
    # Moneda
    currency_id = fields.Many2one('res.currency', default=lambda self: self.env.user.company_id.currency_id)
    amount_total = fields.Monetary('Amount total')
    payment_schedule_date = fields.Date('Payment Schedule Date')
    # Solicitud de recursos
    resource_request_id = fields.Many2one('resource.request')
    # Número de pagos
    number_of_payments = fields.Integer(string="Number of payments")
    # Número de pagos confirmados
    number_of_payments_validated = fields.Integer(computstring="Number of payments Validated")
    amount_total_validated = fields.Monetary('Amount total Validated')
    # Número de pagos confirmados
    number_of_payments_rejected = fields.Integer(computstring="Number of payments Rejected")
    amount_total_rejected = fields.Monetary('Amount total Rejected')

    file = fields.Binary(string='Layout', tracking=True)
    file_name = fields.Char(string='Layout File name')

    response_file = fields.Binary(string='Response Layout', tracking=True)
    response_file_name = fields.Char(string='Response Layout File name')

    reason_for_rejection = fields.Text('Reason For Rejection')
    reason_for_cancellation = fields.Text("reason_for_cancellation")

    is_layout_generated = fields.Boolean('Is layout generated')

    payroll_processing_id = fields.Many2one('custom.payroll.processing','Payroll Processing')

    @api.constrains('response_layouts')
    def _check_unique_attachments(self):
        for record in self:
            # Check for duplicate
            contador = Counter([l.name for l in record.response_layouts])
            if any(count > 1 for count in contador.values()):
                raise ValidationError("Duplicate attachments are not allowed.")

    def _create_resource_request(self, records):
        bank_journal_id = Dictionary()
        lines = []
        amount_total = 0
        # Se agrupan los lotes por cuenta de pago
        for record in records:
            bank_journal_id.add_list(record.journal_id.id, record)
        # Para cada cuenta bancaria de pago se calcula el total y se obtiene como
        # lista el número de lotes.
        for journal_id, record_list in bank_journal_id.items():
            required_amount = 0
            folios = []
            # Se calcula el monto total
            for record in record_list:
                required_amount += record.amount_total
                folios.append((4, record.id))
            # Se generan las líneas de la solicitud de recursos
            lines.append((0, 0, {
                'dst_bank_account_journal_id': journal_id,
                'folio': folios,
                'required_amount': required_amount,
                'amount': 0,
            }))
            amount_total += required_amount
        if lines:
            # Crea la solicitud de recursos
            resource_request_id = self.env['resource.request'].create({
                'amount': amount_total,
                'state': 'draft',
                'transfer_request': 'finances',
                'resource_request_line_ids': lines,
                # 'type': type,
            })
            # Actualiza el estatus del lote
            records.write({
                'state': 'requested_resources',
                'resource_request_id': resource_request_id,
            })
            return True
        return False

    def request_resources(self, res_ids):
        records = self.browse(res_ids)
        # Se valida que los lotes tengan su concepto y fecha de programación de pago
        for record in records:
            if not record.concept or not record.payment_schedule_date:
                raise ValidationError("Es necesario indicar el concepto y la fecha de programación de pago en todos los lotes.")
            if record.state != 'without_resources':
                raise ValidationError("Solo se pueden crear Solicitudes de Recursos de folios con el estatus sin recursos.")
        if self._create_resource_request(records):
            return message_wizard.display_modal(self,
                'Solicitud de Recursos',
                '¡Solicitud de Recursos creada exitosamente!',
                refresh_page=True
            )
        return {}

    def reject(self, folio):
        self.search([('folio', '=', folio)]).write({'state': 'rejected_resources'})

    def schedule_payment(self, res_ids):
        records = self.browse(res_ids)
        folios = []
        # Validar el estatus del lote
        for record in records:
            if record.state != 'with_resources':
                raise UserError("Solo se pueden programar lotes de pagos con recursos.")
        for record in records:
            query_update_payments = """
                update account_payment
                    set payment_state = 'for_payment_procedure',
                    write_date = now(),
                    write_uid = %s
                where batch_folio = %s
            """
            self.env.cr.execute(query_update_payments, (self.env.user.id, record.folio))
            record.state = 'for_payment_procedure'
            folios.append(record.folio)
        return message_wizard.display_modal(self,
            'Programación de Pagos',
            '¡Se han realizado la programación de pago con folio %s!' % (','.join(folios)),
            refresh_page=True
        )

    def unlink(self):
        raise UserError(_("Deleting a payment batch should not be allowed"))

    def _cancel(self, reason_for_cancellation):
        query = """
            update account_payment set state = 'cancelled',
                payment_state = 'cancelled',
                reason_for_cancel = %s
            where batch_folio = %s and payment_state <> 'cancelled'
        """
        for record in self:
            if record.state not in ('without_resources', 'with_resources', 'for_payment_procedure'):
                raise ValidationError(f"Error al cancelar el lote de pago {record.folio}.\r\nSolo se permite cancelar lotes de pago en estatus Sin Recursos, Con Recursos o Para Trámite de Pago")

            self.env.cr.execute(query, (reason_for_cancellation, record.folio))
            record.write({'state': 'cancelled', 'reason_for_cancellation': reason_for_cancellation})

    def cancel(self, reason_for_cancellation):
        batch = self
        if self.type == 'payroll':
            payment_type = 'alimony' if self.concept == 'Pensiones Alimenticias' else 'salary_and_benefits'
            if payment_type == 'salary_and_benefits':
                # Obtiene los lotes que son de salarios y prestaciones adicionales
                batch = self.search([
                    ('target_bank', '=', self.target_bank),
                    ('type', '=', 'payroll'),
                    ('payroll_processing_id', '=', self.payroll_processing_id.id),
                    ('concept', '!=', 'Pensiones Alimenticias'),
                    ('state', '!=', 'cancelled'),
                ])
                # Actualiza la relación de bancos generados por nómina
            query_delete = """
                delete from payroll_payment_batch_bank
                where payment_type = %s
                and bank = %s
                and payroll_processing_id = %s
            """
            self.env.cr.execute(query_delete, (
                payment_type,
                self.target_bank,
                self.payroll_processing_id.id
            ))

        batch._cancel(reason_for_cancellation)

    def action_cancel(self):
        self = self.browse([self.env.context.get('active_id')])

        if self.state not in ('without_resources', 'with_resources', 'for_payment_procedure'):
            raise ValidationError(_("Solo se permite cancelar el pago de un lote con el estatus Sin Recursos, Con Recursos o Para Trámite de Pago"))

        return cancel_wizard.display_modal(self, 'Cancelar Lote de Pago')

    def generate_layout(self, res_ids):
        """
        Genera el layout de banco para el primer lote que se encuentre aprobado para pago
        """
        for record in self.browse(res_ids):
            if record.state == 'for_payment_procedure':
                return {
                    'type' : 'ir.actions.act_url',
                    'url': '/web/binary/download_bank_layout?id=%s'%(record.id),
                    'target': 'self',
                }
        raise UserError("Solo se puede generar el layout de un lote con estatus para Trámite de Pago.")

    def download_layout_file(self):
        """
        Genera el archivo del layout utilizando la funcionalidad del modelo generate.bank.layout
        """
        # Busca todos los pagos asociados al folio
        payments = self.env['account.payment'].search([
            ('batch_folio', '=', self.folio)
        ])
        if not payments:
            return False, False

        # Crea el objeto para poder generar el layout
        layout = self.env['generate.bank.layout'].create({
            'journal_id': self.journal_id.id,
            'payment_ids': [(6, 0, payments.ids)]
        })
        # Genera el nombre de archivo
        layout.onchange_journal_id()
        # Genera el archivo del banco
        if self.type in ('manual', 'automatic'):
            layout.generate_bank_layout()
        elif self.type in ('payroll'):
            layout.generate_payroll_payment_bank_layout()
        self.file = layout.file_data
        self.file_name = layout.file_name
        self.is_layout_generated = True

    def upload_layout_file(self):
        if self.state != 'for_payment_procedure':
            raise ValidationError("Solo se puede cargar el layout de lotes para Trámite de Pago")
        # Get payment tyṕe
        payment_type = 'payroll' if 'payroll' in self.type else 'general'
        # Display wizard to load layout
        return self.env['load.bank.transfer.layout'].open_wizard(
            default_batch_management_id=self.id,
            default_payment_type=payment_type
        )

    def process_payment(self, file_name, file, payments):
        # Update batch managment
        self.response_file_name = file_name
        self.response_file = file
        self.state = 'processed'
        # Update payments
        account_payment = self.env['account.payment']
        for payment in payments:
            if payment.payment_status:
                account_payment.payment_post([payment.res_id])
            else:
                account_payment.paymet_reject(payment.res_id, payment.error_message)

    def update_number_of_payments_validated(self, folio, amount):
        query = """
            update batch_management
            set number_of_payments_validated = coalesce(number_of_payments_validated, 0) + 1,
                amount_total_validated = coalesce(amount_total_validated, 0) + %s
            where folio = %s
        """
        self.env.cr.execute(query, (amount, folio))

    def update_number_of_payments_rejected(self, folio, amount):
        query = """
            update batch_management
            set number_of_payments_rejected = coalesce(number_of_payments_rejected, 0) + 1,
                amount_total_rejected = coalesce(amount_total_rejected, 0) + %s
            where folio = %s
        """
        self.env.cr.execute(query, (amount, folio))
