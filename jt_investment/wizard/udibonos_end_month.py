import json
import logging
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
import datetime
import requests
from dateutil.relativedelta import relativedelta


class UDIEndMonthWizard(models.TransientModel):
    _name = 'udi.end.month.wizard'
    _description = 'Wizard para calcular los rendimientos de los udibonos al final del mes'

    id_udibono = fields.Integer()

    date_consult = fields.Date(
        default=datetime.date.today())

    udi_value = fields.Float("Valor de la UDI", digits=(4, 9))


    @api.constrains('udi_value')
    def check_udi_value(self):
        if self.udi_value <= 0:
            raise UserError(
                _('Negative values or values equal to zero are not allowed'))

    @api.onchange('date_consult')
    def get_udi_value_on_date(self):
        today = self.date_consult
        token = self.env['ir.config_parameter'].get_param('banxico_token')
        url_banxico = self.env['ir.config_parameter'].get_param('banxico_api_url')
        url = f"{url_banxico}SP68257/datos/{today}/{today}?token={token}"
        response = requests.get(url,timeout=10)

        if response.status_code == 200:
            data = json.loads(response.content)
            if "datos" not in data["bmx"]["series"][0]:
                self.udi_value = 0
            else:
                udibono = data["bmx"]["series"][0]["datos"][0]["dato"]
                if udibono is not None:
                    self.udi_value = udibono
                else:
                    self.udi_value = 0
        else:
            self.udi_value = 0


    def end_month(self):

        self.id_udibono = self.env.context.get('active_id')

        # objeto de tipo udibono para consultar el id del udibono actual
        udibono = self.env['investment.udibonos'].search(
            [('id', '=', self.id_udibono)])

        # Verificar que el registro inicie en una fecha posterior a cuando se activa la inversión
        if udibono.date_days_period_elapsed > self.date_consult:
            raise UserError("Lo siento, no es posible realizar acciones antes de la fecha de inicio de la inversión. Por favor, asegúrate de que la fecha de la acción sea posterior a la fecha de inicio de la inversión.")
        
        

        # objeto de tipo accrued interest para consultar la fecha
        date_duplicate = self.env['accrued.interest'].search(
            [('udibonos_id', '=', udibono.id), ('description', '=', 'Movimiento')])
        

        if len(date_duplicate)== 0:
            # Verificar que el registro inicie en una fecha no mayor a un mes que la fecha se se activa la inversión
            if udibono.date_days_period_elapsed.month != self.date_consult.month and udibono.date_days_period_elapsed.year != self.date_consult.year:
                raise UserError(
                    'Tienes movimientos pendientes que aún no han sido procesados. Por favor, revisa las fechas y asegúrate de que estén dentro del período correspondiente.')

        # Verificar que no se haya creado un registro para la fecha actual
        for rec in date_duplicate:
            if rec.date == self.date_consult and rec.description == 'Movimiento':
                raise UserError(_("Ya se ha creado registro para esta fecha"))

        # Verificar que no se haya espacio de más de un mes entre dos movimientos haciendo resta entre meses
        if date_duplicate:
            last_date = date_duplicate[-1].date
            # Se restan los años y se multiplica por 12, luego se le suma la diferencia de meses para obtener el total de meses
            diff_months = (self.date_consult.year - last_date.year) * 12 + (self.date_consult.month - last_date.month)
            if diff_months != 1:
                raise UserError('No puede haber más de un mes de diferencia entre dos movimientos.')


        # Verificar que el movimiento no coincida con el mes y año de udibono.expiry_date
        if self.date_consult.month == udibono.expiry_date.month and self.date_consult.year == udibono.expiry_date.year:
            raise UserError(
                'No puede haber movimientos en el mes de vencimiento.')
        


        # Asigna valores para realizar el calculo 
        udibono.movement_date = self.date_consult
        udibono.udi_value_end_month = self.udi_value
        # Se llama a la funcion para realizar el calculo de fin de mes
        udibono.end_month()

        return True

