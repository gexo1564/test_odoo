from odoo.tests import TransactionCase
from odoo.tests import tagged

@tagged('jt_payroll_payment')
class AdjustedPayrollWizard(TransactionCase):
    ## Nuevos Unit test ##
    def test_generate_query1(self):        
        lite_original_bank_key = ("('15', '14') GROUP BY br.l10n_mx_edi_code; SELECT id FROM productive_account where id = 1--",)
 
        current_query = """
            SELECT br.l10n_mx_edi_code as bank_key, STRING_AGG(lc.folio::VARCHAR, ',') as folio 
            FROM import_payroll_check_issue icpi
            INNER JOIN res_bank br ON icpi.original_bank_id = br.id
            INNER JOIN check_log lc ON icpi.original_check_id = lc.id
            WHERE br.l10n_mx_edi_code IN %(lite_original_bank_key)s
            GROUP BY br.l10n_mx_edi_code;
        """

        params = {'lite_original_bank_key': lite_original_bank_key}
        self.env.cr.execute(current_query, params)
        resultados = self.env.cr.fetchall()
        self.assertEqual(len(resultados), 0)
        print(F"PRUEBA DE QUERY SANITIZADO\nRESULTADOS:{len(resultados)}")


    def test_generate_query2(self):
        postgres_employee_bank_id_list = ["(1, 2) GROUP BY br.l10n_mx_edi_code; SELECT id FROM productive_account where id = 1--",]

        current_query = """SELECT rb.l10n_mx_edi_code, STRING_AGG(concat(cl.folio, ',', cl.id), ';') 
                            FROM check_log cl INNER JOIN res_partner_bank rpb on cl.bank_account_id = rpb.id 
                            INNER JOIN res_bank rb on rpb.bank_id = rb.id where cl.status IN 
                            ('Printed') 
                            AND cl.general_status = 'assigned' AND rb.id IN %(postgres_employee_bank_id_list_element)s 
                            group by rb.l10n_mx_edi_code"""
                        
                        
        params = {'postgres_employee_bank_id_list_element': tuple(postgres_employee_bank_id_list)}
        self.env.cr.execute(current_query, params)
        resultados = self.env.cr.fetchall()
        self.assertEqual(len(resultados), 0)
        print(F"PRUEBA DE QUERY SANITIZADO 2\nRESULTADOS:{len(resultados)}")


    def test_generate_query3(self):
        lite_original_bank_key = ("('15', '14') GROUP BY br.l10n_mx_edi_code; SELECT id FROM productive_account where id = 1--",)

        current_query = """
                    SELECT br.l10n_mx_edi_code as bank_key, STRING_AGG(lc.folio::VARCHAR, ',') as folio 
                    FROM import_payroll_check_issue icpi
                    INNER JOIN res_bank br ON icpi.new_bank_id = br.id
                    INNER JOIN check_log lc ON icpi.new_check_id = lc.id
                    WHERE br.l10n_mx_edi_code IN %(lite_original_bank_key)s
                    GROUP BY br.l10n_mx_edi_code;
                """

        params =  {'lite_original_bank_key': lite_original_bank_key}
        self.env.cr.execute(current_query, params)
        resultados = self.env.cr.fetchall()
        self.assertEqual(len(resultados), 0)
        print(F"PRUEBA DE QUERY SANITIZADO 3\nRESULTADOS:{len(resultados)}")


    def test_generate_query4(self):
        postgres_employee_new_bank_id_list = ["(1, 2) GROUP BY br.l10n_mx_edi_code; SELECT id FROM productive_account where id = 1--",]
        current_query = """SELECT rb.l10n_mx_edi_code, STRING_AGG(concat(cl.folio, ',', cl.id), ';') 
            FROM check_log cl INNER JOIN res_partner_bank rpb on cl.bank_account_id = rpb.id 
            INNER JOIN res_bank rb on rpb.bank_id = rb.id where cl.status IN 
            ('Available for printing') 
            AND cl.general_status = 'available' AND rb.id IN %(postgres_employee_new_bank_id_list_element)s group by rb.l10n_mx_edi_code"""
        
        params =  {'postgres_employee_new_bank_id_list_element': tuple(postgres_employee_new_bank_id_list)}
        self.env.cr.execute(current_query, params)

        resultados = self.env.cr.fetchall()
        self.assertEqual(len(resultados), 0)
        print(F"PRUEBA DE QUERY SANITIZADO 4\nRESULTADOS:{len(resultados)}")


    def test_generate_query5(self):
        postgres_bank_id_list = ["(1, 2) GROUP BY rb.l10n_mx_edi_code; SELECT id FROM productive_account where id = 1--",]
        current_query = """SELECT rb.l10n_mx_edi_code, STRING_AGG(concat(cl.folio, ',', cl.id), ';') 
        FROM check_log cl INNER JOIN res_partner_bank rpb on cl.bank_account_id = rpb.id 
        INNER JOIN res_bank rb on rpb.bank_id = rb.id where cl.status IN 
        ('Available for printing') 
        AND cl.general_status = 'available' AND rb.id IN %(postgres_bank_id_list)s group by rb.l10n_mx_edi_code"""
        
        params = {'postgres_bank_id_list' : tuple(postgres_bank_id_list)}
        self.env.cr.execute(current_query, params)

        resultados = self.env.cr.fetchall()
        self.assertEqual(len(resultados), 0)
        print(F"PRUEBA DE QUERY SANITIZADO 5\nRESULTADOS:{len(resultados)}")


    def test_generate_query6(self):
        current_query = """
                SELECT lpp.id FROM pension_payment_line lpp
                INNER JOIN employee_payroll_file fpe ON lpp.payroll_id = fpe.id
                INNER JOIN custom_payroll_processing ppc ON fpe.payroll_processing_id = ppc.id
                WHERE ppc.id = %(payroll_process_id)s;
            """

        params = {'payroll_process_id': 1}
        self.env.cr.execute(current_query, params)

        resultados = self.env.cr.fetchall()
        self.assertEqual(len(resultados), 0)
        print(F"PRUEBA DE QUERY SANITIZADO 6\nRESULTADOS:{len(resultados)}")
