# -*- coding: utf-8 -*-
from posixpath import split
from odoo import api, fields, models, _
from odoo.exceptions import UserError


class AccountReconcileModel(models.Model):
    _inherit = 'account.reconcile.model'

    match_reference = fields.Boolean(string='Match by reference number')

    def _get_select_communication_flag(self):
        '''
        (Overridden) This function sets the communication flag to either true or
        false. If match_reference is set to True, set the communication flag to
        true as well
        '''
        communication_flag_query = super(AccountReconcileModel, self)._get_select_communication_flag()

        if self.match_reference:
            communication_flag_query = r'''
            (
                move.ref IS NOT NULL
                AND
                move.ref = st_line.ref
            )
            OR
            ''' + communication_flag_query

        return communication_flag_query

    def _get_invoice_matching_query(self, st_lines, excluded_ids=None, partner_map=None):
        '''
        (Overridden) Add additional option to match move lines with bank 
        statement lines that have the same reference numbers
        '''
        invoice_matching_query, invoice_matching_params = \
            super(AccountReconcileModel, self)._get_invoice_matching_query(st_lines, excluded_ids, partner_map)

        invoice_matching_query_arr = invoice_matching_query.split(' UNION ALL ')
        for index, rule in enumerate(self):
            if rule.match_reference:
                split_query = invoice_matching_query_arr[index].split('''
                    (
                        line_partner.partner_id != 0
                        AND
                        aml.partner_id = line_partner.partner_id
                    )
                ''')
                reference_match_query = '''
                        (
                            line_partner.partner_id != 0
                            AND
                            aml.partner_id = line_partner.partner_id
                        )
                        OR
                        (
                            move.ref IS NOT NULL
                            AND
                            move.ref = st_line.ref
                        )
                '''
                invoice_matching_query_arr[index] = ('').join(split_query[0] + reference_match_query + split_query[1])

            full_query = ' UNION ALL '.join(invoice_matching_query_arr)

        return full_query, invoice_matching_params