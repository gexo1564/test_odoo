# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import ValidationError
# python
import logging
from lxml import etree
import json

class IncomeRecognitionActions(models.Model):
    _name = 'income.recognition.actions'
    _description = 'Acciones de ingresos no reconocidos'


    name = fields.Char("Name",required=True,)

    type_action = fields.Selection([('reclassify', 'Reclasificación'),
                                    ('account_accountant','Cuenta contable')])
    nature_movement = fields.Selection([
            ('none', 'None'),
            ('add', 'Add'),
            ('subtract', 'Subtract'),
        ], string='Nature of movement', default='none')

    type_of_revenue_collection = fields.Selection([('education_servs', 'Education Services'),
                                                    ('aspirants','Aspirants'),
                                                    ('exam_prep', 'Exam Preparation'),
                                                    ('bank_interest','Bank Interest')])
                                                        
    accounting_account = fields.Many2one("account.account", "Accounting account")    
    visible = fields.Boolean('Visible', default = True)   
        
    dependency_id = fields.Many2one('dependency', string="Dependency")
    sub_dependency_id = fields.Many2one('sub.dependency', "Subdependency")

    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        res = super(IncomeRecognitionActions, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)

        if view_type == 'form':
            dep_subdep_flag = self.env.context.get('default_dep_subdep_flag') # Contexto desde formulario padre
            doc = etree.XML(res['arch']) # Se obtiene la arquitectura de la vista     
            cont = 0
            for node in doc.xpath("//field"):
                modifiers = json.loads(node.get("modifiers"))
                cont+=1
                # modificadores: 2 dependency_id , 3 sub_dependency_id 
                if cont > 1: 
                    # Si es True puede editar la dependencia y subdependencia
                    if dep_subdep_flag:
                        modifiers['readonly'] = False
                        node.set('modifiers', json.dumps(modifiers)) # Se agrega el modificador a los ya existentes 
            res['arch'] = etree.tostring(doc) # Se actualiza la arquitectura de la vista            
       
        return res




