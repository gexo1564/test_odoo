# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import logging
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from datetime import datetime

from odoo.addons.siif_security.tools.utils import check_authorization, check_only_allowed_fields
class SubProgram(models.Model):

    _name = 'sub.program'
    _description = 'Sub-Program'
    _rec_name = 'sub_program'

    unam_key_id = fields.Many2one('program', string='UNAM key')
    sub_program = fields.Char(string='Sub program', size=2)
    desc = fields.Text(string='Sub program description')
    dependency_id = fields.Many2one('dependency', string='Dependency')
    sub_dependency_id = fields.Many2one('sub.dependency', string='Sub Dependency')
    
    _sql_constraints = [('sub_program_unam_key_id', 'unique(sub_program,unam_key_id,dependency_id,sub_dependency_id, start_date, end_date)',
                         'The sub program must be unique per UNAM key,Dependency and Sub Dependency')]

    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')

    @api.constrains('start_date', 'end_date')
    def check_end_date(self):
        if not self.start_date:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("Configure la fecha inicial"))
            else:
                raise ValidationError(_("The start date of validity is required"))
        if self.end_date and self.end_date <= self.start_date:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("La fecha de fin de vigencia no puede ser menor o igual a la fecha de inicio de vigencia"))
            else:
                raise ValidationError(_("The effective end date cannot be less than or equal to the effective date"))

    @api.constrains('sub_program')
    def _check_sub_program(self):
        if not str(self.sub_program).isnumeric():
            raise ValidationError(_('The Sub Program value must be numeric value'))

    def fill_zero(self, code):
        return str(code).zfill(2)

    @api.onchange('dependency_id')
    def onchange_dependency_id(self):
        if self.env.context and not self.env.context.get('default_sub_dependency_id',False):
            self.sub_dependency_id = False
        
    @api.model
    def create(self, vals):
        if vals.get('sub_program') and len(vals.get('sub_program')) != 2:
            vals['sub_program'] = self.fill_zero(vals.get('sub_program'))
        if vals.get('sub_dependency_id',False) and vals.get('dependency_id',False):
            sub_dep_master = self.env['sub.dependency'].browse(vals.get('sub_dependency_id',[]))
            if sub_dep_master:
                link_sub_dep = self.env['sub.dependency'].search([('dependency_id','=',vals.get('dependency_id',False)),('sub_dependency','=',sub_dep_master.sub_dependency)],limit=1)
                if link_sub_dep:
                    vals.update({'sub_dependency_id':link_sub_dep.id})
                else:
                    vals.update({'sub_dependency_id':False})
        res = super(SubProgram, self).create(vals)
        base_sp = self.env['base.sub.program'].sudo()
        already_exists = base_sp.search([('sub_program', '=', res.sub_program)], limit=1)
        if not already_exists:
            base_sp.create({'sub_program' : res.sub_program})
        return res

    @check_authorization(
        'jt_budget_mgmt.group_budget_catalogs_admin_user',
        'jt_budget_mgmt.group_budget_admin_user'
    )
    @check_only_allowed_fields('desc', 'start_date', 'end_date', 'geographic_location_id')
    def write(self, vals):
        return super(SubProgram, self).write(vals)

    @check_authorization(
        'jt_budget_mgmt.group_budget_catalogs_admin_user',
        'jt_budget_mgmt.group_budget_admin_user'
    )
    def unlink(self):
        for subprogram in self:
            program_code = self.env['program.code'].search([('sub_program_id', '=', subprogram.id)], limit=1)
            if program_code:
                raise ValidationError('No puedes eliminar un Subprograma que este ligado a un Código Programático!')
        return super(SubProgram, self).unlink()

    def validate_subprogram(self, subprogram_string, program,dependency,subdependency):
        if len(str(subprogram_string)) > 1:
            subprogram_str = str(subprogram_string).zfill(2)
            if subprogram_str.isnumeric():
                subprogram = self.search(
                    [('unam_key_id', '=', program.id),('dependency_id','=',dependency.id),('sub_dependency_id','=',subdependency.id),('sub_program', '=', subprogram_str)], limit=1)
                if subprogram:
                    return subprogram
        return False

    # if not return value is not valid #
    def get_validity_sub_program(self, program, sub_program, dependency, sub_dependency, today = datetime.now()):
        query = """
                SELECT sp.id
                FROM sub_program sp
                JOIN program p on sp.unam_key_id = p.id
                JOIN dependency d on sp.dependency_id = d.id
                JOIN sub_dependency sd on sp.sub_dependency_id = sd.id
                WHERE 
                    (
                        (sp.start_date IS NULL AND sp.end_date IS NULL AND p.key_unam = %(program)s AND sp.sub_program = %(sub_program)s AND d.dependency  = %(dependency)s  AND sd.sub_dependency  = %(sub_dependency)s) OR
                        (sp.start_date IS NOT NULL AND sp.end_date IS NULL AND p.key_unam = %(program)s AND sp.sub_program = %(sub_program)s AND d.dependency  = %(dependency)s  AND sd.sub_dependency  = %(sub_dependency)s AND sp.start_date <= %(today)s) OR
                        (sp.start_date IS NOT NULL AND sp.end_date IS NOT NULL AND p.key_unam = %(program)s AND sp.sub_program = %(sub_program)s AND d.dependency  = %(dependency)s  AND sd.sub_dependency  = %(sub_dependency)s AND sp.start_date <= %(today)s AND sp.end_date >= %(today)s)
                    )
                """
        params = {'program' : str(program),
                  'sub_program' : str(sub_program),
                  'dependency': str(dependency),
                  'sub_dependency': str(sub_dependency),
                  'today': today}
        
        self.env.cr.execute(query, params)
        sub_program_validity = self.env.cr.fetchall()

        return sub_program_validity