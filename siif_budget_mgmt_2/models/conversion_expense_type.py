# -*- coding: utf-8 -*-
from datetime import datetime
import logging
from tokenize import group
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from lxml import etree
class ConversionExpenseType(models.Model):

    _name = 'conversion.expense.type'

    expense_type = fields.Many2one("expense.type", string='Expense Type')
    expense_type_name = fields.Text(related='expense_type.description_expenditure_type')

    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')

    expense_group = fields.Char(string='Expense Group', size=3)

    selector_item = fields.Selection(
        [
            ('all', 'All'),
            ('except', 'Except'),
            ('only', 'Only')
        ],
        default='all', required=True, string='Selector Items'
    )
    item_ids = fields.Many2many('expenditure.item', 'conversion_expense_type_item_rel', string='Items')


    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super().fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])
   
        is_group_budget_approval_user = self.env.user.has_group('jt_budget_mgmt.group_budget_approval_user')
        is_group_budget_guest_user = self.env.user.has_group('jt_budget_mgmt.group_budget_guest_user')
        is_group_budget_confirmation_user = self.env.user.has_group('jt_budget_mgmt.group_budget_confirmation_user')
        is_group_budget_register_user = self.env.user.has_group('jt_budget_mgmt.group_budget_register_user')
        
        if (is_group_budget_approval_user or is_group_budget_guest_user or is_group_budget_confirmation_user or is_group_budget_register_user): 
            for node in doc.xpath("//" + view_type):
                node.set('export', '0')
                node.set('create', '0') 
                node.set('edit', '0')
                node.set('delete', '0')
                
            for node in doc.xpath("//field"):
                node.set('options','{"no_open":true}')
                
                    
        res['arch'] = etree.tostring(doc) 
        return res

    @api.constrains('start_date', 'end_date')
    def check_end_date(self):
        if not self.start_date:
            raise ValidationError(_("The start date of validity is required."))
        if self.end_date and self.end_date <= self.start_date:
            raise ValidationError(_("The effective end date cannot be less than or equal to the effective date."))

    @api.model
    def create(self, vals):
        domain = []
        if vals.get('expense_type',None):
            domain.append(('expense_type', '=', vals.get('expense_type',None)))
        if vals.get('start_date',None):
            domain.append(('start_date', '=', vals.get('start_date',None)))
        if vals.get('end_date',None):
            domain.append(('end_date', '=', vals.get('end_date',None)))
        if vals.get('expense_group',None):
            domain.append(('expense_group', '=', vals.get('expense_group',None)))
        if vals.get('selector_item',None):
            domain.append(('selector_item', '=', vals.get('selector_item',None)))
        item_ids = vals.get('item_ids',None)
        if item_ids and item_ids[0][2]:
            domain.append(('item_ids', 'in', item_ids[0][2]))
        if  self.env['conversion.expense.type'].search(domain):
            raise ValidationError("La Regla del Tipo de gasto ya existe.")
        return super().create(vals)

    @api.onchange("selector_item")
    def _onchange_selector_item(self):
        if self.selector_item == 'all':
            self.write({'item_ids': [(5,)]})

    def name_get(self):
        return [(rec.id, rec.expense_type.key_expenditure_type) for rec in self]

    @api.model
    def get_expense_type(self, item, date=datetime.now()):
        query = f"""
        select et.id, et.key_expenditure_type expense_type from conversion_expense_type c, expense_type et
        where expense_type = et.id and (selector_item = 'all' or (
                selector_item = 'only' and exists (
                select 1 from conversion_expense_type_item_rel r, expenditure_item i where conversion_expense_type_id = c.id and expenditure_item_id = i.id and i.item = '{item}'
            )) or (selector_item = 'except' and not exists (
                select 1 from conversion_expense_type_item_rel r, expenditure_item i where conversion_expense_type_id = c.id and expenditure_item_id = i.id and i.item = '{item}'
            ))
        ) and substring(expense_group,1,1) = substring('{item}',1,1)
        and c.start_date <= '{date}' and (c.end_date is null or c.end_date >= '{date}')
        """
        self._cr.execute(query, ())
        res = self.env.cr.fetchall()
        return res[0] if len(res) == 1 else (None, None)