from odoo import models, fields,api,_
from odoo.exceptions import ValidationError

class DepositCertificateType(models.Model):

    _name = 'deposit.certificate.type'
    _description = "Type of Deposit Certificate"

    name = fields.Char("Name")
    description = fields.Text("General Description")


    @api.model
    def fields_get(self, fields=None, attributes=None):
        no_selectable_fields = ['id','current_account_id','previous_account_id']       #lista de filtrar por
        no_sortable_field = ['current_account_id','previous_account_id']          #lista de agrupar por

        res = super(DepositCertificateType, self).fields_get()
        for key, value in res.items():
            if key in no_selectable_fields:
                value.update({'selectable': False})

        for key, value in res.items():
            if key in no_sortable_field:
                value.update({'sortable': False})
        return res

    def unlink(self):
        for this_type_cert in self:
            is_exist_asign_cert = self.env['account.move'].search([('operation_key_id','=',this_type_cert.id)],limit=1)
            if is_exist_asign_cert:
                raise ValidationError(_('No puedes eliminar un tipo de certificado cuando ya esta siendo utilizado en los registros de los certificados.'))
        return super(DepositCertificateType,self).unlink()
    
    @api.constrains('name')
    def _check_code(self):
        for rec in self:
            if rec.name:
                deposit_id = self.env['deposit.certificate.type'].search([('name','=',rec.name),('id','!=',rec.id)],limit=1)
                if deposit_id:
                    raise ValidationError(_("There is already a record with the same Certificate of Deposit Name"))    