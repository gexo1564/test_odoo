import io
from ..layouts import Layout, ResultLine

class LayoutSantanderH2H(Layout):

    def load_file(self, file_data):
        # io.StringIO(file_data.decode("utf-8"))
        self.file = io.StringIO(file_data.decode("ISO-8859-1")) #open(file, encoding="ISO-8859-1")

    def load_body(self):
        return self.file.readlines()[1:-1]

    def close_file(self):
        self.file.close()

    def read_line(self, line):
        rfc = line[210:228].strip()
        amount = float("%s.%s" % (line[27:40], line[40:42]))
        status = line[400:402].strip()
        bank_acount = line[150:170]
        ok = status == "00"
        return ResultLine(ok, "unknown", {'rfc': rfc, 'amount': amount, 'bank_account': bank_acount}, status)

    def get_ids_search_employee_payroll_file(self, payroll_processing_id: int, lines: list):
        rfc = tuple([l.columns.get('rfc') for l in lines])
        # Se obtienen los ids que hacen match con el criterio de búsqueda
        query = """
            select e.rfc, r.id, r.net_salary sueldo, coalesce(p.amount, 0) prestacion
            from employee_payroll_file r
            join hr_employee e on r.employee_id = e.id
            left join (
                select payroll_id, sum(amount) amount
                from additional_payments_line l, employee_payroll_file r
                where l.payroll_id = r.id
                and r.payroll_processing_id = %s
                group by payroll_id
            ) p on r.id = p.payroll_id
            where  e.rfc in %s
            and r.payroll_processing_id = %s
        """
        self.env.cr.execute(query, (payroll_processing_id, rfc, self.payroll_processing_id))
        result_set = {r[0]: r[1:] for r in self.env.cr.fetchall()}
        lines_ok, lines_err = [], []
        for line in lines:
            res = result_set.get(line.columns.get('rfc'), None)
            if res:
                line.res_id = res[0]
                # Se busca si es sueldo o prestacion segun coincida con los montos
                amount = line.columns.get('amount')
                if amount == res[1]:
                    line.payment_type = "sueldo"
                elif amount == res[2]:
                    line.payment_type = "prestacion"
                lines_ok.append(line)
            else:
                line.res_id = None
                line.error_message = "No se encontró un registro para el empleado con rfc %s" % line.columns.get('rfc')
                lines_err.append(line)
        return lines_ok, lines_err

    def get_ids_search_account_payment(self, batch_folio: str, lines: list):
        # Se obtienen los ids que hacen match con el criterio de búsqueda
        query = """
            select r.vat rfc, p.id, p.amount
            from account_payment p, res_partner_bank a, res_partner r
            where p.payment_bank_account_id = a.id
            and p.partner_id = r.id
            and batch_folio = %s
        """
        self.env.cr.execute(query, (batch_folio,))
        result_set = {r[0]: r[1:] for r in self.env.cr.fetchall()}
        lines_ok, lines_err = [], []
        for line in lines:
            rfc = line.columns.get('rfc')
            res = result_set.get(rfc, None)
            if res:
                if res[1] == line.columns.get('amount'):
                    line.res_id = res[0]
                    lines_ok.append(line)
                else:
                    line.res_id = None
                    line.error_message = f"El monto del pago ({line.columns.get('amount'):,.2f}) del beneficiario {rfc} no coincide con el registrado ({res[1]:,.2f})."
                    lines_err.append(line)
            else:
                line.res_id = None
                line.error_message = f"No se encontró un registro para el beneficiario {rfc}"
                lines_err.append(line)
        return lines_ok, lines_err