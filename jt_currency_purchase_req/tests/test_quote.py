from odoo.tests import common
from odoo.exceptions import ValidationError
from datetime import date
import base64

class TestQuote(common.TransactionCase):
	def setUp(self):
		super().setUp()
	
		self.account_account = self.env['account.account'].search([('name','=','FF EFECTIVO EN DÓLARES')])
		self.journal = self.env['account.journal'].search([('name','=','BBVA BANCOMER-8141 (BBV01)')])
		self.journal2 = self.env['account.journal'].search([('name','=','AFIRME-8266 (BAF04)')])
		self.currency_prueba_01 = self.env['res.currency'].search([('name','=','AED')])
		self.bank = self.env['res.bank'].search([('name','=','REFORMA')])
		self.currency_fund = self.env['currency.funds'].create({
			'name':'Fondo Fijo',
			'associated_account':self.account_account.id,
			'journal_id':self.journal.id,
			'currency_id':self.currency_prueba_01.id,
			'fund_limit':10000.00,
			'currency_reorder_point':1000.00})
	

	#Revisión de la creación de cotizaciones
	def test_create(self):
		quote= self.env['quote']
		den_test={
			'count':6,
			'denominations':'den_500'
		}
		den_01={
			'count':30,
			'denominations':'den_100'
		}
		den_02={
			'count':1,
			'denominations':'den_50'
		}
		den_test_01 = self.env['total.denominations'].create(den_test)
		den_quote_01 = self.env['total.denominations.quote'].create(den_01)
		den_quote_02 = self.env['total.denominations.quote'].create(den_02)
		vals = {
			'status':'draft',
			'currency_fund_id':self.currency_fund.id,
			'required_date':date.today(),
			'amount_funds':3000,
			'account_journal':self.journal.id,
			'total_denomination':den_test_01,
		}
		test01 = self.env['application.request'].create(vals)
		test01.action_approve_published()

		self.assertEqual(test01.has_quotes,False)
		with self.assertRaises(ValidationError):
			test01.action_quotes()

		#Abrir archivo para asignarle información

		filetest = open('jt_currency_purchase_req/static/tests/test.txt',"rb")
		outtest = filetest.read()
		filetest.close()
		files_base = base64.b64encode(outtest)

		#Creación de cotizaciones
		"""
			Caso 01: Cotización con tipo de cambio de divisa en 0
		"""
		info_quote={
			'status':'draft',
			'application_request':test01.id,
			'amount_app_request':3000,
			'currency_fund_id':self.currency_fund.id,
			'denominations_quote_ids':den_quote_01,
			'bank_id':self.bank.id,
			'exchange_rate_number':0,
			'equal_denominations':False,
		}
		with self.assertRaises(ValidationError):
			quote.create(info_quote)
		
		"""
			Caso 02: Cotización con tipo de cambio de divisa en negativo
		"""
		info_quote={
			'status':'draft',
			'application_request':test01.id,
			'amount_app_request':3000,
			'currency_fund_id':self.currency_fund.id,
			'denominations_quote_ids':den_quote_01,
			'bank_id':self.bank.id,
			'exchange_rate_number':-20.81,
			'equal_denominations':False,
		}
		with self.assertRaises(ValidationError):
			quote.create(info_quote)

		"""
			Caso 03: Cotización sin documento
		"""
		info_quote={
			'status':'draft',
			'application_request':test01.id,
			'amount_app_request':3000,
			'currency_fund_id':self.currency_fund.id,
			'denominations_quote_ids':den_quote_01,
			'bank_id':self.bank.id,
			'exchange_rate_number':20.80,
			'equal_denominations':False,
		}
		quote_01 = quote.create(info_quote)
		

		"""
			Caso 04: Cotización Publicada
		"""
		quote_01.update({'doc_quote':files_base})
		quote_01.action_published()


		"""
			Caso 05: Cotización con suma de denominaciones diferentes
		"""
		info_quote_02={
			'status':'draft',
			'application_request':test01.id,
			'amount_app_request':3000,
			'currency_fund_id':self.currency_fund.id,
			'denominations_quote_ids':den_quote_02,
			'bank_id':self.bank.id,
			'exchange_rate_number':19.00,
			'equal_denominations':False,
			'doc_quote':files_base,
		}
		quote_02 = self.env['quote'].create(info_quote_02)
		
		with self.assertRaises(ValidationError):
			quote_02.action_published()
		
		den_quote_02.update({'count':60})
		quote_02.action_published()
		

		"""
			Caso 06: Cotización con iguales denominaciones
		"""
		info_quote_03={
			'status':'draft',
			'application_request':test01.id,
			'amount_app_request':3000,
			'currency_fund_id':self.currency_fund.id,
			'bank_id':self.bank.id,
			'exchange_rate_number':18.50,
			'equal_denominations':True,
			'doc_quote':files_base,
		}
		quote_03 = self.env['quote'].create(info_quote_03)
		quote_03.action_published()
		
		#Revisar si la denominacion de la cotización 3 se generaron
		self.assertEqual(quote_03.denominations_quote_ids[0].denominations,'den_500')
		self.assertEqual(quote_03.denominations_quote_ids[0].count,6)

		#Revisar estado cotizaciones
		self.assertEqual(quote_01.status,'published')
		self.assertEqual(quote_02.status,'published')
		self.assertEqual(quote_03.status,'published')

		#Revisar que la cotización 03 es la más barata
		self.assertEqual(quote_01.selected_quote,False)
		self.assertEqual(quote_02.selected_quote,False)
		self.assertEqual(quote_03.selected_quote,True)

		#Revisar que se cambie el estado de la solicitud de compra
		test01.action_quotes()

		#No se pueden cambiar a borrador si ya no están publicados
		with self.assertRaises(ValidationError):
			quote_01.action_draft()

		with self.assertRaises(ValidationError):
			quote_02.action_draft()

		with self.assertRaises(ValidationError):
			quote_03.action_draft()


