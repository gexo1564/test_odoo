import io
import csv
from ..layouts import Layout, ResultLine

class LayoutHSBC(Layout):

    def load_file(self, file_data):
        # io.StringIO(file_data.decode("utf-8"))
        self.file = io.StringIO(file_data.decode("ISO-8859-1")) #open(file, encoding="ISO-8859-1")

    def load_body(self):
        return [l for l in csv.reader(self.file)][1:]

    def close_file(self):
        self.file.close()

    def read_line(self, line):
        concepts = {
            "SUELDO QN": "sueldo",
            "PRESTA QN": "prestacion",
            "PENSION Q": "pension",
        }
        account_bank = line[1]
        amount = float(line[4])
        beneficiary = line[2].strip()
        status = line[6]
        ok = status == "Processed"
        return ResultLine(ok, "pension", {'beneficiary': beneficiary, 'account_bank': account_bank, 'amount': amount}, status)

    def get_ids_search_employee_payroll_file(self, lines: list):
        query = """
            select substring(replace(name, 'Ñ', ' '), 1, 35) as name,
                lpad(b.acc_number, 10, '0') account,
                p.id partner_id
            from res_partner_bank b, res_partner p, pension_payment_line l, employee_payroll_file r
            where p.id = b.partner_id
            and l.partner_id = p.id
            and l.payroll_id = r.id
            and r.payroll_processing_id = %s
            and beneficiary_type = 'alimony'
        """
        self.env.cr.execute(query, (self.payroll_processing_id,))
        result_set = {(r[0], r[1]): r[2:] for r in self.env.cr.fetchall()}
        lines_ok, lines_err = [], []
        for line in lines:
            if line.payment_type == "pension":
                # Busqueda para PENSION
                beneficiary = line.columns.get('beneficiary')
                account_bank = line.columns.get('account_bank')
                res = result_set.get((beneficiary, account_bank), None)
                if res:
                    line.payroll_id = res[0]
                    lines_ok.append(line)
                else:
                    line.payroll_id = None
                    line.error_message = "No se encontró un registro para el beneficiario %s con cuenta %s" % (
                        beneficiary,
                        account_bank
                    )
                    lines_err.append(line)
            else:
                line.error_message = "No es pago de pensión."
                lines_err.append(line)
        return lines_ok, lines_err