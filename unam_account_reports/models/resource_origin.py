from odoo import fields, models


class ResourceOrigin(models.Model):
    _name = 'resource.origin'
    _inherit = ['resource.origin', 'excel.detailed.information']
    
    def _compute_excel_display_name(self):
        for record in self:
            record.excel_display_name = '%s' % (record.desc or record.key_origin)
