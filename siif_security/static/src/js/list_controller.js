odoo.define("siif.ListController", function (require) {
  "use strict";

  let ListController = require("web.ListController")
  let core = require('web.core')
  let Dialog = require('web.Dialog')
  let Sidebar = require('web.Sidebar')

  let _t = core._t

  ListController.include({
    renderSidebar: function ($node) {
      let self = this
      if (this.hasSidebar) {
        let other = []
        if (this.activeActions.export){
          other.push({
              label: _t("Export"),
              callback: this._onExportData.bind(this),
          });
        }
        if (this.archiveEnabled) {
          other.push({
            label: _t("Archive"),
            callback: function () {
              Dialog.confirm(
                self,
                _t("Are you sure that you want to archive all the selected records?"),
                {confirm_callback: self._toggleArchiveState.bind(self, true),}
              );
            },
          });
          other.push({
            label: _t("Unarchive"),
            callback: this._toggleArchiveState.bind(this, false),
          });
        }
        if (this.is_action_enabled("delete")) {
          other.push({
            label: _t("Delete"),
            callback: this._onDeleteSelectedRecords.bind(this),
          });
        }
        this.sidebar = new Sidebar(this, {
          editable: this.is_action_enabled("edit"),
          env: {
            context: this.model.get(this.handle, { raw: true }).getContext(),
            activeIds: this.getSelectedIds(),
            model: this.modelName,
          },
          actions: _.extend(this.toolbarActions, { other: other }),
        });
        return this.sidebar.appendTo($node).then(function () {
          self._toggleSidebar();
        });
      }
      return Promise.resolve();
    },
  });
});
