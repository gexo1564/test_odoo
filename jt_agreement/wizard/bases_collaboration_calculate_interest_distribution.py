import logging
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from dateutil.relativedelta import relativedelta


class CalculateInterestDistributionWizard(models.TransientModel):
    _name = 'calculate.interest.distribution.wizard'
    _description = 'Wizard para calcular la distribución de intereses'

    date = fields.Date()
    
    def calculate_interest_distribution_monthly(self):
        self.env['bases.collaboration'].calculate_all_interest_distribution_monthly(self.date)

    def action_interest_distribution_monthly(self):
        self.env['bases.collaboration'].action_interest_distribution(self.date)

    
