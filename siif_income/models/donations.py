from email.policy import default
from pyexpat import model
from string import digits
from datetime import datetime, timedelta
from tokenize import String
from urllib import response
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError,UserError
from odoo import models,fields,api,_
from xmlrpc import client
from lxml import etree
from odoo.addons.sd_utils.tools import utils
from odoo.addons.sd_utils.models import message_wizard
import mimetypes
from odoo.tools.mimetypes import guess_mimetype
import os
import json
import urllib.request
import logging
_logger = logging.getLogger(__name__)
ACCOUNT_MOVE_MODEL = 'account.move'
DEPOSIT_SLIP_LINE_MODEL = 'sd_base.deposit_slip.line'
TAX_REGIME_MODEL = 'sd_base.tax_regime'
PAYMENT_OF_INCOME_MODEL = 'payment.of.income'

UNIT_KEY = [
    ('M4','M4 - Valor Monetario'),
    ('C62','C62 - Uno'),
    ('ZZ','ZZ - Mutuamente definido'),
]

CFDI_USE = [
    ('G03','G03 - Gastos en general'),
    ('D04','D04 - Donativos'),
    ('P01','P01 - Por definir'),
    ('S01','S01 - Sin efectos fiscales'),   # Queda en desuso debido a CFDI v4?
]

WAY_PAY = [
    ('EF','01 - Efectivo'),
    ('CH','02 - Cheque nominativo'),
    ('TB','03 - Transferencia electrónica de fondos'),
    ('NA','99 - Por definir'),
]
nd = 'Nuevo Donativo'
type_dona = ['Donativo en Numerario',              #0
             'Donativos por descuento de nómina',  #1
             'Donativos por cheque']               #2
modelos_repetidos = ['res.partner',               # 0
                  'ir.actions.act_window',        # 1
                  'body.email.donations',         # 2
                  'ir.config_parameter',          # 3
                  'mail.template',                # 4        
                  'res.users',                    # 5
                  'mail.mail',                    # 6
                  'sd_base.deposit_slip']         # 7
parametros_repetidos = ['web.base.url',
                        'Admin correo sidia']
mensajes_repetidos = ['No se encuentra al usuario de correo admin',  #0
            'Favor de no responder este mensaje.',         #1
            'No se creó el mensaje',                       #2
            'Las Fichas de Deposito aun no han caducado, ¿Desea generar nuevas fichas?\n', #3
            'Antes de aceptar, verifique sus datos',  #4
            'Alerta Fichas de Deposito',            #5
            'Folio de Solicitud: ', #6
            '<p>Fichas ya pagadas: ',  #7
            'Las Fichas de Deposito ya expiraron, ¿Desea generar nuevas fichas?\n']   #8
wizzards = ['preview.invoice.donation.wizard',       #0
            'siif_income.alert_new_deposit_slip',    #1
            'alert.donation.wizard']                 #2


PAYMENT_METHOD = [
    ('E','PUE - Pago en una sola exhibición'),
    ('D','PPD - Pago en parcialidades o diferido'),

]
class Donations(models.Model):  
    _name = 'donations'
    _description = "Donaciones"
    _inherit = ['mail.thread','mail.activity.mixin']

    name = fields.Char(string="Donativo", required=True, copy=False, readonly=True, index=True, default=nd)

    depen_trans = fields.Text(string="Dependency Transmitter", default='744 Dirección General de Finanzas')
    sub_depen_trans = fields.Text(string="Sub Dependency Transmitter", default='01 Dirección General de Finanzas')

    def get_domain_dependency(self):
        dep = self.env.user.dependency_income_ids.ids
        dep += [d.id for d in self.env.user.sub_dependency_income_ids.dependency_id]
        return json.dumps([('id', 'in', dep)])

    dependancy_id = fields.Many2one('dependency', string='Dependency Receptor', domain=get_domain_dependency)

    @api.depends('dependancy_id')
    def _compute_domain_sub_dependency(self):
        for record in self:
            domain = [('dependency_id', '=', self.dependancy_id.id)]
            if self.dependancy_id.id not in self.env.user.dependency_income_ids.ids:
                domain.append(('id', 'in', self.env.user.sub_dependency_income_ids.ids))
            domain.append(('sub_dependency','!=','00'))
            record.domain_sub_dependency = json.dumps(domain)
    domain_sub_dependency = fields.Char(compute="_compute_domain_sub_dependency", store=False)

    sub_dependancy_id = fields.Many2one('sub.dependency', 'Sub Dependency Receptor')

    type_donation_id = fields.Many2one('type.donations', string='Type Donation')
    name_type = fields.Char(related='type_donation_id.name_type',store=False)
    is_later_payment = fields.Boolean(string='Later Payment', default=False)
    limit_value_uma = fields.Boolean(default=False)

    benefactor = fields.Many2one(modelos_repetidos[0],"Benefactor")
    benefactor_rfc = fields.Char(related="benefactor.vat",string="RFC")
    benefactor_email = fields.Char('Benefactor Email')
    ommision_email = fields.Char('Omission Email', default='dgfdonativos.unam@patronato.unam.mx')
    
    emission_date = fields.Datetime(string="Emission Date", default=datetime.today())
    expiration_date = fields.Datetime(string="Expiration Date", default=lambda self: self._default_expiration_date())

    cfdi_of_donation_pdf = fields.Binary(string="CFDI PDF")
    cfdi_of_donation_pdf_name = fields.Char(string="Nombre CFDI")
    cfdi_of_donation_xml = fields.Binary(string="CFDI XML")
    cfdi_of_donation_xml_name = fields.Char(string="Nombre XML")
    series_don = fields.Char(string="Serie")
    uuid_don = fields.Char(string="# Factura")
    folio_don = fields.Char(string="Folio")
    cfdi_status_don = fields.Char(string="Estatus")
    cfdi_date_don = fields.Date(string="Fecha de Factura")
    cfdi_deposit_token = fields.Char(string='cfdi deposit token')
    invoice_id = fields.Many2one(ACCOUNT_MOVE_MODEL, string='Related Invoice')

    description_donation = fields.Char('Description Donation')
    number_files_referred = fields.Integer('Number Files Referred',default=1)
    donation_amout_p_token = fields.Float('Donation Amount per Token')
    total_donation_amount = fields.Float('Total Donation Amount', digits=(15,2))
    currency_id = fields.Many2one(comodel_name='res.currency', string='Currency', default=lambda self: self.env.company.currency_id)
    donation_status = fields.Selection([('draft','Draft'),('in_review','In Review'),('accepted','Accepted'),('refused','Refused'),('with_reference','With Reference'),
                                        ('received','Received'),('invoiced','Invoiced'),('charged','Charged')],
                                        string="Donation Status")

    deduction_id = fields.Many2one(comodel_name='deduction', string='accounting scholarship')
    agreement = fields.Char('Agreement Donator')
    request_letter = fields.Char('Request Letter')
    bank_check = fields.Many2one('res.bank', string='Bank')
    check = fields.Char('Check', default='1')
    donation_concept = fields.Char('Donation Concept')

    fortnight = fields.Selection([('01','01'),('02','02'),('03','03'),('04','04'),('05','05'),('06','06'),('07','07'),('08','08'),('09','09'),('10','10'),
                                  ('11','11'),('12','12'),('13','13'),('14','14'),('15','15'),('16','16'),('17','17'),('18','18'),('19','19'),('20','20'),
                                  ('21','21'),('22','22'),('23','23'),('24','24')])
    excercise = fields.Char('Excercise',default=str(datetime.today().year))
    deposit_tokens_line_ids = fields.One2many(DEPOSIT_SLIP_LINE_MODEL,'donations_id',string="")
    deposit_token = fields.Many2one(related='deposit_tokens_line_ids.deposit_token', string="Deposit token")

    '''files'''
    tax_id_of_the_donor = fields.Boolean('tax ID of the donor')
    rdi = fields.Boolean('Request for donation of independence')
    agreement_file = fields.Boolean('Agreement')
    letter_N_Considerations = fields.Boolean('Letter of No Considerations')
    documents_sopport = fields.Many2many('ir.attachment',string='Support documents', attachment=True)
    documents_sopport_name = fields.Char('File name documents sopport')

    observations = fields.Text('Observations')
    cfdi_use = fields.Selection(CFDI_USE, string="Uso de CFDI")
    cve_unidad = fields.Selection(UNIT_KEY, string="CVE Unidad")
    form_pago = fields.Selection(WAY_PAY, string="Forma de Pago")
    met_pag = fields.Selection(PAYMENT_METHOD, string="Método de Pago")
    is_group_income_admin_user = fields.Boolean(compute="_compute_is_group_income_admin_user")
    payroll_payment_request_id = fields.Many2one(ACCOUNT_MOVE_MODEL, "Related payroll request")
    tax_regime_id = fields.Many2one(TAX_REGIME_MODEL, string="Tax regime", default=lambda self: self.env[TAX_REGIME_MODEL].search([('code', '=', '605')], limit=1))
    payment_service_key_id = fields.Many2one(PAYMENT_OF_INCOME_MODEL, string="Payment service key", default=lambda self: self.env[PAYMENT_OF_INCOME_MODEL].search([('name', '=', 'DE')], limit=1))

    @api.depends('is_group_income_admin_user')
    def _compute_is_group_income_admin_user(self):
        self.is_group_income_admin_user = self.env.user.has_group('jt_income.group_income_admin_user')

    def get_domain_dependency_list(self):
        dep = self.env.user.dependency_income_ids.ids
        dep += [d.id for d in self.env.user.sub_dependency_income_ids.dependency_id]
        return dep

    def doman_view(self):
        domain_dependency = []

        domain_dependency = self.get_domain_dependency_list()
        return {
            "name":"Donaciones",
            "type": "ir.actions.act_window",
            "res_model":"donations",
            "view_mode":"tree,form",
            "domain":[('dependancy_id','in', domain_dependency)],
        }

    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(Donations,self).fields_view_get(
            view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])

        is_group_income_guest_user = self.env.user.has_group('jt_income.group_income_guest_user')
        is_group_income_dependence_user = self.env.user.has_group('jt_income.group_income_dependence_user')
        if is_group_income_dependence_user or is_group_income_guest_user:
            for node in doc.xpath("//" + view_type):
                node.set('export_xlsx', '0')

        res['arch'] = etree.tostring(doc)
        return res

    @api.model
    def create(self, vals):
        """Método que sobrescribe el create del objeto."""
        today = datetime.today()
        if vals.get('name',nd) == nd:
            name_seq = self.env['ir.sequence'].next_by_code('donations.sequence') or nd
            name = 'DONATIVO/'+ str(today.year) +'/' + str(name_seq)
            _logger.info(f"***** name: {name}")
            vals['name'] = name 
        return super(Donations, self).create(vals)

    def set_donation_in_payroll(self):
        i_found_deduction = False
        i_found_the_price = False
        year_now = datetime(int(self.excercise),1,1)

        if self.benefactor.beneficiary_type == 'employee':
            employee_id = self.env['hr.employee'].search([('name','=',self.benefactor.name)]).id
            if employee_id:
                employee_payroll_file = self.env['employee.payroll.file'].search([('employee_id','=',employee_id),('fornight','=',self.fortnight),('period_start','>=',year_now)])
                if len(employee_payroll_file) >= 2:
                    employee_payroll_file = employee_payroll_file[-1]
                elif len(employee_payroll_file) == 0:
                    raise ValidationError(_('No se encontro la nomina del empleado'))
                if employee_payroll_file.state == 'done':
                    payment_request = self.env[ACCOUNT_MOVE_MODEL].search([('partner_id','=',self.benefactor.id),('fornight','=',self.fortnight),('period_start','>=',year_now)]) or \
                        self.payroll_payment_request_id

                    if len(payment_request) >= 2:
                        payment_request = payment_request[-1]
                    elif len(payment_request) == 0:
                        raise ValidationError(_('No se encontro la Solicitud de Pago de Nómina '))
                    if payment_request.payment_state == 'approved_payment':
                        deduction_lines = employee_payroll_file.deduction_line_ids
                        for deduction_line in deduction_lines:
                            if deduction_line.deduction_id.id == self.deduction_id.id:
                                i_found_deduction = True
                                if deduction_line.amount == self.total_donation_amount:
                                    i_found_the_price = True
                                    new_donation_in_line = self.env['donation.line'].create({
                                        'payroll_id':employee_payroll_file.id,
                                        'donation_id':self.id,
                                        'move_id':payment_request.id
                                    })
                                    if new_donation_in_line:
                                        self.donation_status = 'accepted'

                        if not i_found_deduction:
                            raise ValidationError(_('La Clave de la dedución no se encontro en la nomina, verifique sus registros'))
                        elif not i_found_the_price:
                            raise ValidationError(_('El monto del donativo no concide con la cantidad deducida en la nomina'))
                    else:
                        raise ValidationError(_('La solicitud de Pago de Nómina no esta en estado Aprobada para Pago'))
                else:
                    raise ValidationError(_('La nomina no ha sido completamente procesado'))
            else:
                raise ValidationError(_('No se encontro el empleado; puede que el nombre del donador registrado en Contactos no coincida con el registrado en Empleados'))
        else:
            raise ValidationError(_('El donador no es un empleado'))


    @api.constrains('request_letter')
    def _check_request_letter(self):
        if not self.request_letter:
            raise UserError(_('Insert Request Folio.'))
        

    @api.constrains('documents_sopport')
    def _check_name_multi_files(self):
        mimetype = None
        files = 0
        is_pdf = 0
        _logger.info(f"***** documents_sopport_ids:{self.documents_sopport}")
        if self.documents_sopport:
            for i in self.documents_sopport:
                mimetype = mimetypes.guess_type(i.name)
                _logger.info(f"***** documents_sopport type:{mimetype[0]}")
                if mimetype[0] != 'application/pdf':
                    files += 1
                else:
                    is_pdf += 1
                    files += 1
            if files != is_pdf:
                raise ValidationError("Solo se admiten documentos en .pdf")

    def clean_description(self):
        self.observations = ''
        self.donation_status = 'in_review'

    @api.constrains('description_donation')
    def _check_description_donation(self):
        if not self.description_donation:
            raise ValidationError(_('Inserta la descripcion para el CFDI'))
    
    def _check_documents_support(self):
        if self.type_donation_id.name_type == type_dona[0] or self.type_donation_id.name_type == type_dona[2]:
            if not self.tax_id_of_the_donor and not self.rdi or not self.agreement_file or not self.letter_N_Considerations:
                raise ValidationError(_('Verifique si los documentos estan listos y marque los en las casillas, si no es asi, rechaze el donativo'))
        else:
            if not self.tax_id_of_the_donor or not self.rdi:
                raise ValidationError(_('Verifique si los documentos estan listos y marque los en las casillas, si no es asi, rechaze el donativo'))

    @api.constrains('donation_concept')
    def _check_donation_concept(self):
        if self.type_donation_id.name_type != type_dona[1]:
            if not self.donation_concept:
                raise ValidationError(_('Inserte la descripcion en Ficha Referenciada'))

    @api.constrains('dependancy_id','sub_dependancy_id')
    def _check_dependencys(self):
        if not self.dependancy_id:
            raise ValidationError(_('Selecciona dependencia'))
        else:
            if not self.sub_dependancy_id:
                raise ValidationError(_('Selecciona Sub-dependencia'))

    @api.constrains('donation_amout_p_token', 'number_files_referred')
    def _calculate_total_donation_amount(self):
        if self.type_donation_id.name_type == type_dona[0]:
            if self.number_files_referred >0:
                if self.donation_amout_p_token>0.00:
                    self.total_donation_amount = float(self.number_files_referred) * self.donation_amout_p_token
                    self.check_value_uma()

    @api.constrains('total_donation_amount')
    def _check_total_mount(self):
        if self.total_donation_amount > 0:
            if self.type_donation_id.name_type == type_dona[1] or self.type_donation_id.name_type == type_dona[2]:
                    self.check_value_uma()
        else:
            raise UserError(_('El Monto Total del Donativo debe ser mayor a 0'))

    @api.constrains('expiration_date')
    def constrains_date(self):
        if self.expiration_date < self.emission_date:
            raise ValidationError(_('La fecha de caducidad no puede ser inferior a la fecha actual'))
        elif self.expiration_date.year != datetime.today().year:
            raise ValidationError(_('La fecha de caducidad no puede ser fuera del ejercicio actual'))

    @api.onchange('expiration_date')
    def _onchangue_expiration_date(self):
        previous_expiration_date = fields.Datetime.from_string(self._origin.expiration_date)
        expiration_date = fields.Datetime.from_string(self.expiration_date)
        
        if previous_expiration_date:
            today = previous_expiration_date
            one_month_later = today + relativedelta(months=1)

            if expiration_date.year > previous_expiration_date.year:
                raise ValidationError(_('The expiration date cannot be outside of the current fiscal year.'))        
            elif expiration_date > one_month_later:
                raise ValidationError(_('It cannot be extended more than 1 month.'))
            elif expiration_date < self.emission_date:
                raise ValidationError(_('The expiration date cannot be less than the emission date.')) 

    def check_value_uma(self):
        uma_value = self.env['sd_base.configuration_uma'].search([('is_active','=',True)],limit=1).price_uma
        limit_uma = self.env['limits.umas.configurations'].search([],limit=1)


        if self.total_donation_amount >= (uma_value*float(limit_uma.limit)):
            self.limit_value_uma = True

        if self.total_donation_amount < (uma_value*float(limit_uma.limit)):
            self.limit_value_uma = False

    @api.onchange('documents_sopport')
    def _check_all_files_ready(self):
        if self.documents_sopport:
            self.donation_status = 'in_review'
        else:
            self.donation_status = 'draft'

    @api.constrains('donation_amout_p_token')
    def _check_donation_amout_p_token(self):
        if self.type_donation_id.name_type == type_dona[0] and not self.is_later_payment:
            if self.donation_amout_p_token<=0:
                raise ValidationError(_('El monto de la donación por ficha no puede ser negativo o cero'))

    @api.onchange('check')
    def _check_check(self):
        if self.type_donation_id.name_type == type_dona[2]:
            if not self.check.isdigit():
                raise ValidationError(_('El valor insertado en cheque, no es numérico'))

    @api.constrains('bank_check')
    def _check_bank(self):
        if self.type_donation_id.name_type == type_dona[2]:
            if not self.bank_check:
                raise ValidationError(_('Seleccione el banco del que pertenece el cheque'))
    
    @api.constrains('excercise')
    def _check_excercise(self):
        if not self.excercise.isdigit():
            raise ValidationError(_('El valor insertado en ejercicio, no es numérico'))
        if len(self.excercise)!=4:
            raise ValidationError(_('El valor en ejercicio, no es de 4 posiciones'))
        if int(self.excercise)<0 or int(self.excercise)>=10000:
            raise ValidationError(_('El ejercicio no puede ser menor que 0 ni mayor que 9999'))

    @api.constrains('benefactor_email')
    def _check_benefactor_email(self):
        if self.benefactor_email:
            arr_emails = self.benefactor_email.split(';')
            _logger.info(f'************************arr_emails: {arr_emails}')
            for email in arr_emails:
                if not utils.is_valid_email_address(email):
                    raise UserError(f'El correo {email} es invalido')
        else:
            raise UserError('Inserte correo(s) de donador(es)')

    @api.constrains('ommision_email')
    def _check_ommision_email(self):
        if self.ommision_email:
            if not utils.is_valid_email_address(self.ommision_email):
                raise UserError("Este no es un correo electrónico valido")


    def changeAccepted(self):
        if self.type_donation_id.name_type == type_dona[1]:
            #self._check_documents_support()
            self.set_donation_in_payroll()
            self.observations = ''
            self.send_email_accepted()
        elif self.type_donation_id.name_type == type_dona[2] or self.type_donation_id.name_type == type_dona[0]:
            #self._check_documents_support()
            self.donation_status = 'accepted'
            self.observations = ''
            self.send_email_accepted()

    def add_observation(self, observations, condition, message):
        if condition:
            return observations + message
        return observations

    def validate_and_send_refusal(self, message):
        if not self.observations:
            raise ValidationError(_('Escriba el motivo de rechazo en la pestaña de observaciones, en la parte inferior'))
        self.donation_status = 'refused'
        self.send_email_refused()

    def changeRefused(self):
        ob1 = ''
        ob2 = ''
        ob3 = ''
        ob4 = ''

        if not self.tax_id_of_the_donor:
            ob1 = '\n- Falta Cédula fiscal del donante'
        if not self.rdi:
            ob2 = '\n- Falta Oficio de solicitud del donativo de la dependencia'

        if self.type_donation_id.name_type == type_dona[0] or self.type_donation_id.name_type == type_dona[2]:
            ob3 = self.add_observation(ob3, not self.agreement_file, '\n- Falta Convenio')
            ob4 = self.add_observation(ob4, not self.letter_N_Considerations, '\n- Falta Carta de no contraprestación')
        elif self.type_donation_id.name_type == type_dona[1]:
            self.agreement_file = None
            self.letter_N_Considerations = None

        self.observations = ob1 + ob2 + ob3 + ob4

        if self.observations:
            self.validate_and_send_refusal('Motivo de rechazo')

    def preview_invoice_donation(self):
        view = self.env.ref('siif_income.preview_invoice_donation_form')
        dep = self.env['dependency'].search([('dependency','=','743')])
        subdep = self.env['sub.dependency'].search([('dependency_id','=',dep.id),('sub_dependency','=','01')])
        name = ''+dep.dependency+' '+subdep.sub_dependency+' '+dep.description
        
        new_wizard = {
                        'id_dona':self._origin.id,
                        'dep_emisora':name,
                        'name':'MARIA OLIVIA MARTINEZ',
                        'rfc':'MASO451221PM4',
                        'tax_regime_id': self.tax_regime_id.id,
                        'dom_fis':'-',
                        'cfdi_use':self.cfdi_use,
                        'met_pag': self.met_pag,
                        'clvProvSer':'84101600',
                        'cant':'1',
                        'cveUnidad': self.cve_unidad,#'C62',
                        'descripcion':self.description_donation,
                        'p_Unitario':self.total_donation_amount,
                        'importe':self.total_donation_amount,
                        'subtotal':self.total_donation_amount,
                        'total':self.total_donation_amount,
                        'currency':'MXN',
                        'form_pago': self.form_pago,
                        'payment_service_key_id': self.payment_service_key_id.id,
                    }
        create_wizard = self.env[wizzards[0]].create(new_wizard)
        return {
                'name':_("Previsualizacion de datos en CFDI"),
                'res_model': wizzards[0],
                'views': [(view.id, 'form')],
                'type': modelos_repetidos[1],
                'target': 'new',
                'res_id': create_wizard.id
            }
    
    # Cve_banco segun catalogo UTICT
    def get_cve_banco(self, bank_name):
        bank_mapping = {
            'BBVA BANCOMER': 'BBVA',
            'BANAMEX': 'BNMX',
            'BANORTE': 'BNRT',
            'HSBC': 'HSBC',
            'IXE': 'IXE',
            'SCOTIABANK': 'SCOT',
            'SANTANDER': 'SNTN',
        }
        return bank_mapping.get(bank_name, 'OTRO')
    

    def get_folio_aprobacion(self, bank_movement):
        folio = bank_movement.folio
        if bank_movement.income_bank_journal_id.bank_id.name == 'JP MORGAN':
            date = datetime.strptime("2023-02-13", "%Y-%m-%d")
            folio = date.strftime("%d%m%y")

        return folio
    

    def get_referencia_banco(self):
        referencia_banco = ''
        if self.deposit_tokens_line_ids:
            referencia_banco = self.deposit_tokens_line_ids[-1].deposit_token.payment_reference
        
        return referencia_banco

    #Metodo que genera la factura del donativo si el estatus esta en recibido 
    def generate_invoice_donation(self):
        url_facturacion = os.environ['HOST_FACTURACION']
        proxy = client.ServerProxy(url_facturacion)
        
        products = []

        postal_code = self.benefactor.zip
        description = self.description_donation
        total = self.total_donation_amount
        cve_unidad = self.cve_unidad
        cfdi_use = self.cfdi_use
        met_pag = self.met_pag
        form_pago = self.form_pago
        cve_banco = ''
        num_cheque = ''
        status_pago = 'I'
        folio_aprobacion = ''
        referencia_banco = ''

        if form_pago == 'NA':
            status_pago = 'P'
        elif form_pago == 'TB':
            bank_movement = self.env['bank.movements'].search([('donations_id', '=', self.id)])
            cve_banco = self.get_cve_banco(bank_movement.income_bank_journal_id.bank_id.name)
            folio_aprobacion = self.get_folio_aprobacion(bank_movement)
            referencia_banco = self.get_referencia_banco()

        elif form_pago == 'CH':
            if self.type_donation_id.name_type != type_dona[2]:
                raise UserError("Solo en Donativo por cheque puede seleccionar esta Forma de pago.")
            num_cheque = self.check
            cve_banco = self.get_cve_banco(self.bank_check.name)
           
        #TIPO DE SERVICIO=====================================================
        service_type ={
                'Cvepago': self.payment_service_key_id.name or 'DE', # SG - Servicios Generales # DE - Donativos efectivo
                'Tpocfd':'FD' # FD - Factura Digital
        }

        # PRODUCTOS=====================================================
        products =[
                         {
                         'Cta_ie': '232',
                         'Cve_producto': '84101600',
                         'Cantidad':1,
                         'Unidad': 'UNO', 
                         'Cve_unidad': cve_unidad, 
                         'Unidad_local':'DONATIVO',  #OPCIONAL
                         'Descripcion':description,
                         'Precio_unit':total,
                         'Iva':'N',
                         'Iva_impte':0,
                         'Descto':0,
                         'c_tasacuota_tralix':'0.000000',
                         'c_impuesto_tralix':'002',
                         'c_tipofactor_tralix':'Tasa',
                         'Descto_impte':0.000000,
                         'Importe_total':total,
                         'Importe_mil':0.000000,
                         }
                 ]

        #MÉTODOS DE PAGO=====================================================
        payment =[
                        {
                        'Cve_pago':form_pago,
                        'Importe':total,
                        'Cve_banco': cve_banco,
                        'Num_cheque': num_cheque,
                        'Cta_tarjeta':'',
                        'Referencia_banco':referencia_banco,
                        'Cve_tipo_TDC':'',
                        'Fecha_transaccion':'',
                        'Folio_aprobacion':folio_aprobacion
                        },
                ]

        # DATOS FISCALES=====================================================
        fiscal = {
                    'Procedencia':'OT',
                    'Pais':'MEX',
                    'Rfc':'MASO451221PM4',
                    'Nombre':'MARIA OLIVIA',
                    'A_paterno':'MARTINEZ',
                    'A_materno':'SAGAZ',
                    'Estado':'', #no son obligatorios
                    'DelOMunicipio':'', #no son obligatorios
                    'Cp': postal_code or '80290',
                    'Colonia':'', #no son obligatorios
                    'Calle':'', #no son obligatorios
                    'No_ext':'', #no son obligatorios
                    'No_int':'', #no son obligatorios
                    'Email':'correo1@dominio.com',
                    'Email2': '', #no son obligatorios
                    'UsoCFDI': cfdi_use,
                    'c_Regimenfiscal': self.tax_regime_id.code or '605',
                }

        if self.benefactor.person_type == 'moral':
            fiscal['Razon_soc'] = self.benefactor.name   #omitir datos nombre, ap-pt y ap-mt al ser moral

        # DEP=====================================================
        dependency = {
                    'Dependencia':'743',  #323
                    'Subdep':'01',
                    'Origen':'SI',
                    'Status_pago': status_pago,   # cuando es P es posterior, esto genera una referencia
                    'Importe_factura':total,
                    'Cve_moneda':'MXN',
                    'Forma_liq': met_pag,# 'E',
                    'Carta_certif':'0',
                    'Tipo_moneda': '1', # '1',
                    'Cve_actividad':'4',
                    'Observaciones':''
                }

        if self.is_later_payment:
            dependency['Status_pago'] = 'P'
        #INE--------------------------------------------------
        ine = {}
        
        cfdi_request = proxy.genera_cfdi.v4(service_type, products, payment, fiscal, dependency, ine)

        if not self.is_later_payment:
            if cfdi_request:
                if cfdi_request[0]:
                    self.series_don = cfdi_request[1]
                    self.folio_don = cfdi_request[2]
                    self.cfdi_status_don = cfdi_request[3]
                    self.uuid_don = cfdi_request[4]
                    self.cfdi_date_don = datetime.today().date()
                    self.get_cfdi_files()
                else:
                    raise UserError(cfdi_request[1])
            else:
                raise UserError("No fue posible comunicarse con el servicio de facturación.\nPor favor, intente más tarde.")
        else:
            if cfdi_request:
                if cfdi_request[0]:
                    self.series_don = cfdi_request[1]
                    self.folio_don = cfdi_request[2]
                    self.cfdi_status_don = cfdi_request[3]
                    self.uuid_don = cfdi_request[4]
                    self.cfdi_deposit_token = cfdi_request[5]
                    self.cfdi_date_don = datetime.today().date()
                    self.get_cfdi_files()
                else:
                    raise UserError(cfdi_request[1])
            else:
                raise UserError("No fue posible comunicarse con el servicio de facturación.\nPor favor, intente más tarde.")

        
        return cfdi_request[0]
    
    def get_cfdi_files(self):
        """Método que descarga y almacena los archivos XML y PDF una vez generada una factura"""
        self.ensure_one()

        name_of_files = '' 
        name_of_files = 'UNA2907227Y5_'+self.series_don+'_'+self.folio_don+'_'+self.benefactor_rfc

        response_pdf = utils.get_cfdi_pdf_xml_file(self.series_don,self.folio_don,'PDF')
        self.handle_error(response_pdf,"cfdi_of_donation_pdf",name_of_files,'.pdf')
        response_xml = utils.get_cfdi_pdf_xml_file(self.series_don,self.folio_don,'XML')
        self.handle_error(response_xml,"cfdi_of_donation_xml",name_of_files,'.xml')

        
    def handle_error(self,response, attachment_name, file_name, format):
        if isinstance(response, bytes):
            ir_attachment_rec = utils.store_binary_file(self, attachment_name, response)
            if ir_attachment_rec:
                self.cfdi_of_donation_pdf_name = file_name + ".pdf" if self.benefactor_rfc else "CFDI.pdf"
            else:
                raise ValidationError(f"No fue posible guardar el archivo CFDI en {format}.")
        else:
            raise ValidationError(response)

    def send_email_accepted(self):
        if not self.benefactor_email:
            raise UserError('Inserte correo(s) de donador(es)')
        conf_email = self.env[modelos_repetidos[2]].search([('state','=','Aprobado')])
        arr_emails = self.benefactor_email.split(';')
        if self.ommision_email:
            arr_emails.append(self.ommision_email)
        self.send_email_specific_mail(arr_emails,self.benefactor.name,conf_email.subject,conf_email.msj)

    def send_email_refused(self):
        conf_email = self.env[modelos_repetidos[2]].search([('state','=','Rechazado')])
        arr_emails = self.benefactor_email.split(';')
        if self.ommision_email:
            arr_emails.append(self.ommision_email)
        self.send_email_specific_mail(arr_emails,self.benefactor.name,conf_email.subject,conf_email.msj)

    def send_email_specific_mail(self,email_delivery,name_delivery,subject,body):
        if email_delivery:
            for email in email_delivery:
                self.env.context = dict(self.env.context)
                self.env.context.update({
                        'is_email' : True
                    })

                base_url = self.env[modelos_repetidos[3]].sudo().get_param(parametros_repetidos[0])
                template_obj = self.env[modelos_repetidos[4]].sudo().search([('name','=','mail_template_bac')], limit=1)
                auxbody = template_obj.body_html
                auxbody = auxbody.replace('--subject--',subject)
                auxbody = auxbody.replace('--summary--',body)
                auxbody = auxbody.replace('--base_url--',base_url)
                name_to = ""

                root_email = self.env[modelos_repetidos[5]].search([('name','=',parametros_repetidos[1])])
                if not root_email:
                    raise UserError(mensajes_repetidos[0])

                author_id = self.env[modelos_repetidos[0]].search([('id','=',root_email.partner_id.id)])


                mail_pool = self.env[modelos_repetidos[6]]
                values = {
                                'subject' : subject,
                                'email_to' : "",
                                'email_from' : author_id.email,
                                'author_id':author_id.id,
                                'reply_to' : mensajes_repetidos[1],
                                'body_html' : "",
                                'body' : body,
                    }

                values['email_to'] = str(email)
                if name_delivery:
                    name_to = name_delivery

                auxbody = auxbody.replace('--name_to--',name_to)
                values['body_html'] = auxbody

                msg_id = mail_pool.create(values)

                if msg_id:
                    mail_pool.sudo().send([msg_id])
                    _logger.info(f"***** Correo electrónico enviado a <{values['email_to']}>")

                else:
                    raise UserError(mensajes_repetidos[2])

    def button_wizard_2(self):
        view_wizard = self.env.ref(wizzards[1])
        msj = mensajes_repetidos[3]
        msj2 = mensajes_repetidos[4]
        alert = self.env[wizzards[2]].create({'msj_on_wizard':msj+msj2, 'id_dona':self._origin.id})
        return {
                'name':_(mensajes_repetidos[5]),
                'res_model': wizzards[2],
                'views': [(view_wizard.id, 'form')],
                'type': modelos_repetidos[1],
                'target': 'new',
                'res_id': alert.id
            }

    def button_wizard_1(self):
        view_wizard = self.env.ref(wizzards[1])
        msj = mensajes_repetidos[8]
        msj2 = mensajes_repetidos[4]
        alert = self.env[wizzards[2]].create({'msj_on_wizard':msj+msj2, 'id_dona':self._origin.id})
        return {
                'name':_(mensajes_repetidos[5]),
                'res_model': wizzards[2],
                'views': [(view_wizard.id, 'form')],
                'type': modelos_repetidos[1],
                'target': 'new',
                'res_id': alert.id
            }

    def send_email_regenered_reference(self,msj,ids_slips):
        conf_email = self.env[modelos_repetidos[2]].search([('state','=','Cancelación de Fichas')])
        model = modelos_repetidos[7]
        arr_emails = self.benefactor_email.split(';')
        if self.ommision_email:
            arr_emails.append(self.ommision_email)
        if not conf_email.msj:
            raise UserError(_('no hay mensaje configurado en el cuerpo del correo, configure el cuerpo del correo'))
        else:
            self._message_post_attachments_invoice(arr_emails,self.benefactor.name,conf_email.subject,(conf_email.msj+msj),model,ids_slips)

    def send_email_no_regenered_reference(self,msj):
        conf_email = self.env[modelos_repetidos[2]].search([('state','=','Cancelación de Fichas')])
        arr_emails = self.benefactor_email.split(';')
        if self.ommision_email:
            arr_emails.append(self.ommision_email)
        if not conf_email.msj:
            raise UserError(_('no hay mensaje configurado en el cuerpo del correo, configure el cuerpo del correo'))
        else:
            self.send_email_specific_mail(arr_emails,self.benefactor.name,conf_email.subject,(conf_email.msj+msj))
    
    def new_deposit_tokens(self):
        ind = 0
        slips_pag = 0
        lines_deps_slips = []
        slips = []
        slips_gen = []
        suma = 0.0
        leng_of_lines = len(self.deposit_tokens_line_ids)
        deposit_token = self.env[modelos_repetidos[7]]
        vals = {
                'dependency_id': 270,         
                'sub_dependency_id': 1201,                            
                'res_model': self._name,                        
                'expiration_date': self.expiration_date,        
                'concepts': " - ".join([str(self.dependancy_id.dependency),self.sub_dependancy_id.sub_dependency,self.sub_dependancy_id.description, self.donation_concept,'('+mensajes_repetidos[6]+self.request_letter+')']).upper(), 
                'origin': 'SI',     
                }
        line = {
                'donations_id':self._origin.id,
                'deposit_token':deposit_token.id,
                'emission_date':datetime.today()
                }
        
        for id in self.deposit_tokens_line_ids:
            if not id.recognized:
                id_dep_slip = id.deposit_token.id
                self.env[DEPOSIT_SLIP_LINE_MODEL].search([('id','=',id.id)]).unlink()
                self.env[modelos_repetidos[7]].search([('id','=',id_dep_slip)]).unlink()
                ind += 1
            else:
                lines_deps_slips.append(id.id)
        if not lines_deps_slips:  #si no hay ninguna ficha pagada, o en el caso de cheque, la ficha del cheque no esta pagada
            if self.type_donation_id.name_type == type_dona[0]:     # para el caso de donativos por numerario
                self.button_generate_deposit_slips_of_donation()
            elif self.type_donation_id.name_type == type_dona[2]:    #para el caso de donativos por cheque
                vals['total_price'] = self.total_donation_amount
                vals['currency_id'] = self.currency_id.id
                vals['payment_type'] = "CH"                          
                deposit_token.create(vals)
                desposit_slip_response = deposit_token.generate_deposit_slip()
                if desposit_slip_response:  
                    self.deposit_tokens_line_ids = self.env[DEPOSIT_SLIP_LINE_MODEL].create(line)
                    self.check_value_uma()
                    id_token = []
                    for deposit_token in self.deposit_tokens_line_ids:
                        id_token.append(deposit_token.id)
                    self.send_email_regenered_reference('',id_token)
        else:
            self.manage_paid_tokens(vals, deposit_token, slips,leng_of_lines, line,slips_gen, lines_deps_slips,slips_pag,suma)

    def manage_paid_tokens(self, vals, deposit_token, slips,leng_of_lines, line,slips_gen, lines_deps_slips,slips_pag,suma):
        self.deposit_tokens_line_ids = None
        vals['total_price'] = self.donation_amout_p_token
        vals['currency_id'] = self.currency_id.id
        vals['payment_type'] = "EF"
        if self.number_files_referred >= leng_of_lines :
            ind = self.number_files_referred - len(lines_deps_slips)
            ##Crea los nuevos registros de las fichas 
            for _ in range(ind):
                deposit_token.create(vals)
                desposit_slip_response = deposit_token.generate_deposit_slip()
                if desposit_slip_response[0]:
                    slips.append(line)
            self.deposit_tokens_line_ids = lines_deps_slips
            self.deposit_tokens_line_ids += self.env[DEPOSIT_SLIP_LINE_MODEL].create(slips)
            for lines in self.deposit_tokens_line_ids:
                suma += lines.amout_p_token
            self.total_donation_amount = suma
            self.check_value_uma()
            for slip in self.deposit_tokens_line_ids:
                if not slip.recognized:
                    slips_gen.append(slip.id)
                else:
                    slips_pag += 1
            self.donation_status = 'with_reference'
            msj = mensajes_repetidos[7]+str(slips_pag)+', Fichas Generadas: '+str(len(slips_gen))+'</p>'
            self.send_email_regenered_reference(msj,slips_gen)
        elif self.number_files_referred < leng_of_lines:
            ind = self.number_files_referred
            ind = ind - len(lines_deps_slips)               #resta las que ya estan reconocidas
            if ind > 0:                                                     #cuando se resten fichas
                self.token_subtraction(lines_deps_slips,slips,line,slips_gen, vals,ind)             
            else:                                                        
                self.paid_chips(lines_deps_slips)

    #si no hay fichas por regenerar, solo adihere las ya pagadas
    def paid_chips(self,lines_deps_slips):
        self.deposit_tokens_line_ids = lines_deps_slips
        for lines in self.deposit_tokens_line_ids:
            suma += lines.amout_p_token
        self.total_donation_amount = suma
        self.check_value_uma()
        self.donation_status = 'with_reference'
        for slip in self.deposit_tokens_line_ids:
            if slip.recognized:
                slips_pag += 1
        self.donation_status = 'with_reference'
        msj = mensajes_repetidos[7]+str(slips_pag)+'</p>'
        self.send_email_no_regenered_reference(msj)

    #si hay fichas por regenerar
    def token_subtraction(self, lines_deps_slips,slips,line,slips_gen, vals,ind):                                              
        ##Crea los nuevos registros de las fichas 
        for _ in range(ind):
            deposit_token = self.env[modelos_repetidos[7]].create(vals)
            desposit_slip_response = deposit_token.generate_deposit_slip()
            if desposit_slip_response[0]:
                slips.append(line)
        self.deposit_tokens_line_ids = lines_deps_slips
        self.deposit_tokens_line_ids += self.env[DEPOSIT_SLIP_LINE_MODEL].create(slips)
        for lines in self.deposit_tokens_line_ids:
            suma += lines.amout_p_token
        self.total_donation_amount = suma
        self.check_value_uma()
        for slip in self.deposit_tokens_line_ids:
            if not slip.recognized:
                slips_gen.append(slip.id)
            else:
                slips_pag += 1
        self.donation_status = 'with_reference'
        msj = mensajes_repetidos[7]+str(slips_pag)+', Fichas Generadas: '+str(len(slips_gen))+'</p>'
        self.send_email_regenered_reference(msj,slips_gen)
    
    def button_generate_deposit_slips_of_donation(self):
        if self.deposit_tokens_line_ids and self.deposit_tokens_line_ids[-1].recognized:
            raise ValidationError(_("You can't generate another deposit slip if the current ones are already paid."))
        
        slips = []
        if not self.deposit_tokens_line_ids:
            vals = {
                'dependency_id': 270,         
                'sub_dependency_id': 1201,                            
                'res_model': self._name,                        
                'total_price': self.donation_amout_p_token,               
                'expiration_date': self.expiration_date,        
                'concepts': " - ".join([str(self.dependancy_id.dependency),self.sub_dependancy_id.sub_dependency,self.sub_dependancy_id.description, self.donation_concept,'('+mensajes_repetidos[6]+self.request_letter+')']).upper(), 
                'origin': 'SI',                                 
                'payment_type': 'EF'     
            }
            ##Crea los registros de las fichas con su convenio y referencia
            for i in range(self.number_files_referred):
                deposit_token = self.env[modelos_repetidos[7]].create(vals)
                desposit_slip_response = deposit_token.generate_deposit_slip()
                if desposit_slip_response[0]:
                    lines = {
                            'donations_id':self._origin.id,
                            'deposit_token':deposit_token.id,
                            'emission_date':datetime.today()
                        }
                    slips.append(lines)
            self.deposit_tokens_line_ids = self.env[DEPOSIT_SLIP_LINE_MODEL].create(slips)
        elif self.deposit_tokens_line_ids:
            if datetime.today() >= self.expiration_date:   #si ya expiraron las fichas
                view_wizard = self.env.ref(wizzards[1])
                msj = mensajes_repetidos[8]
                msj2 = mensajes_repetidos[4]
                alert = self.env[wizzards[2]].create({'msj_on_wizard':msj+msj2, 'id_dona':self._origin.id})
                return {
                        'name':_(mensajes_repetidos[5]),
                        'res_model': wizzards[2],
                        'views': [(view_wizard.id, 'form')],
                        'type': modelos_repetidos[1],
                        'target': 'new',
                        'res_id': alert.id
                    }
            else:                                          #si aun no expiran
                view_wizard = self.env.ref(wizzards[1])
                msj = mensajes_repetidos[3]
                msj2 = mensajes_repetidos[4]
                alert = self.env[wizzards[2]].create({'msj_on_wizard':msj+msj2, 'id_dona':self._origin.id})
                return {
                        'name':_(mensajes_repetidos[5]),
                        'res_model': wizzards[2],
                        'views': [(view_wizard.id, 'form')],
                        'type': modelos_repetidos[1],
                        'target': 'new',
                        'res_id': alert.id
                    }
        self.donation_status = 'with_reference'
        self.send_email_with_reference()
        
    def button_generate_deposit_slips_of_check(self):
        if not self.deposit_tokens_line_ids:
            vals = {
                'dependency_id': 270,         
                'sub_dependency_id': 1201,                            
                'res_model': self._name,                        
                'total_price': self.total_donation_amount,               
                'expiration_date': self.expiration_date,        
                'concepts': " - ".join([str(self.dependancy_id.dependency),self.sub_dependancy_id.sub_dependency,self.sub_dependancy_id.description, self.donation_concept,'('+mensajes_repetidos[6]+self.request_letter+')']).upper(), 
                'origin': 'SI',     
                'currency_id': self.currency_id.id,                            
                'payment_type': 'CH'
                
                
            }
            deposit_token = self.env[modelos_repetidos[7]].create(vals)
            desposit_slip_response = deposit_token.generate_deposit_slip()
            if desposit_slip_response:
                line = {
                        'donations_id':self._origin.id,
                        'deposit_token':deposit_token.id,
                        'emission_date':datetime.today()
                    }
            self.deposit_tokens_line_ids = self.env[DEPOSIT_SLIP_LINE_MODEL].create(line)
        elif self.deposit_tokens_line_ids:
            if datetime.today() >= self.expiration_date:   #si ya expiraron las fichas
                view_wizard = self.env.ref(wizzards[1])
                msj = mensajes_repetidos[8]
                msj2 = mensajes_repetidos[4]
                alert = self.env[wizzards[2]].create({'msj_on_wizard':msj+msj2, 'id_dona':self._origin.id})
                return {
                        'name':_(mensajes_repetidos[5]),
                        'res_model': wizzards[2],
                        'views': [(view_wizard.id, 'form')],
                        'type': modelos_repetidos[1],
                        'target': 'new',
                        'res_id': alert.id
                    }
            else:                                          #si aun no expiran
                view_wizard = self.env.ref(wizzards[1])
                msj = mensajes_repetidos[3]
                msj2 = mensajes_repetidos[4]
                alert = self.env[wizzards[2]].create({'msj_on_wizard':msj+msj2, 'id_dona':self._origin.id})
                return {
                        'name':_(mensajes_repetidos[5]),
                        'res_model': wizzards[2],
                        'views': [(view_wizard.id, 'form')],
                        'type': modelos_repetidos[1],
                        'target': 'new',
                        'res_id': alert.id
                    }
        self.donation_status = 'with_reference'
        self.send_email_with_reference()

    def send_email_with_reference(self):
        res_ids = []
        rec_deposit_slip = self.env[DEPOSIT_SLIP_LINE_MODEL].search([('donations_id','=',self._origin.id)])
        for deposit in rec_deposit_slip:
            res_ids.append(deposit.deposit_token.id)
        _logger.info(f'deposit slip lines ids: {res_ids}')
       
        conf_email = self.env[modelos_repetidos[2]].search([('state','=','Con Referencia')])
        model = modelos_repetidos[7]
        arr_emails = self.benefactor_email.split(';')
        if self.ommision_email:
            arr_emails.append(self.ommision_email)
        self._message_post_attachments_invoice(arr_emails,self.benefactor.name,conf_email.subject,conf_email.msj,model,res_ids)

    def asig_extencion_in_files(self):
        attch_obj = self.env['ir.attachment']
        file_pdf = attch_obj.sudo().search([('res_model','=','donations'),('res_id','=',self.id),('res_field','=','cfdi_of_donation_pdf')])
        _logger.info(f'file_pdf: {file_pdf.name}')
        file_xml = attch_obj.sudo().search([('res_model','=','donations'),('res_id','=',self.id),('res_field','=','cfdi_of_donation_xml')])
        _logger.info(f'file_xml: {file_xml.name}')

        name_of_files = '' 
        name_of_files = 'UNA2907227Y5_'+self.series_don+'_'+self.folio_don+'_'+self.benefactor_rfc

        edit_name_pdf = file_pdf.sudo().write({'name':name_of_files+'.pdf'})
        _logger.info(f'edit_name_pdf: {edit_name_pdf}')
        edit_name_xml = file_xml.sudo().write({'name':name_of_files+'.xml'})
        _logger.info(f'edit_name_pdf: {edit_name_xml}')
    
    def send_email_charger(self):
        if self.benefactor_email:
            arr_emails = self.benefactor_email.split(';')
            if self.ommision_email:
                arr_emails.append(self.ommision_email)
            for email in arr_emails:
                conf_email = self.env[modelos_repetidos[2]].search([('state','=','Cobrado')])
                self.env.context = dict(self.env.context)
                self.env.context.update({
                        'is_email' : True
                    })
                    
                base_url = self.env[modelos_repetidos[3]].sudo().get_param(parametros_repetidos[0])
                template_obj = self.env[modelos_repetidos[4]].sudo().search([('name','=','mail_template_bac')], limit=1)
                auxbody = template_obj.body_html
                auxbody = auxbody.replace('--subject--',conf_email.subject)
                auxbody = auxbody.replace('--summary--',conf_email.msj)
                auxbody = auxbody.replace('--base_url--',base_url)
                name_to = ""

                att_ids = []
                
                
                self.env.cr.execute(
                    f"""
                    select id
                    from ir_attachment
                    where res_model = 'donations' and
                        res_id = {self.id} and res_field = 'cfdi_of_donation_pdf'
                    """
                )
                record_pdf = self.env.cr.fetchall()
                pdf = record_pdf[0]
                cfdi_pdf = pdf[0]
                att_ids.append(cfdi_pdf)
                _logger.info(f'cfdi_pdf: {cfdi_pdf}')


                self.env.cr.execute(
                    f"""
                    select id
                    from ir_attachment
                    where res_model = 'donations' and
                        res_id = {self.id} and res_field = 'cfdi_of_donation_xml'
                    """
                )
                record_xml = self.env.cr.fetchall()
                xml = record_xml[0]
                cfdi_xml = xml[0]
                att_ids.append(cfdi_xml)
                _logger.info(f'cfdi_xml: {cfdi_xml}')


                _logger.info(f'att_ids: {att_ids}')

                root_email = self.env[modelos_repetidos[5]].search([('name','=',parametros_repetidos[1])])
                if not root_email:
                    raise UserError(mensajes_repetidos[0])

                author_id = self.env[modelos_repetidos[0]].search([('id','=',root_email.partner_id.id)])
                
                mail_pool = self.env[modelos_repetidos[6]]
                values = {
                                'subject' : conf_email.subject,
                                'email_to' : "",
                                'email_from' : author_id.email,
                                'author_id':author_id.id,
                                'reply_to' : mensajes_repetidos[1],
                                'body_html' : "",
                                'body' : conf_email.msj,
                                'attachment_ids': [(6, 0, att_ids)],
                                'auto_delete': True,
                    }

                values['email_to'] = str(email)
                if self.benefactor.name:
                    name_to = self.benefactor.name

                auxbody = auxbody.replace('--name_to--',name_to)
                values['body_html'] = auxbody

                msg_id = mail_pool.create(values)

                if msg_id:
                    mail_pool.sudo().send([msg_id])
                    _logger.info(f"***** Correo electrónico enviado a <{values['email_to']}>")

                else:
                    raise UserError(mensajes_repetidos[2])

    def _get_attachments(self, models_ids, model):
        records_pdfs = []
        for model_id in models_ids:
            self.env.cr.execute(f"""select id from ir_attachment where res_model = '{model}' and res_id = {model_id}""")
            record_pdf = self.env.cr.fetchall()
            records_pdfs.append(record_pdf[0])
        
        return records_pdfs
    
    def _iter_records(self, record_pdf):
        att_ids = []
        for records in record_pdf:
            r = records[0]
            att_ids.append(r)
        
        return att_ids
    
    def _get_email_template(self, subject, body):
        self.env.context = dict(self.env.context)
        self.env.context.update({'is_email' : True})
        base_url = self.env[modelos_repetidos[3]].sudo().get_param(parametros_repetidos[0])
        # se obtiene la plantilla del cuerpo de correo a enviar
        template_obj = self.env[modelos_repetidos[4]].sudo().search([('name','=','mail_template_bac')], limit=1)
        auxbody = template_obj.body_html.replace('--subject--', subject).replace('--summary--', body).replace('--base_url--', base_url)

        return auxbody

    def _message_post_attachments_invoice(self,email_delivery,name_delivery,subject,body, model, models_ids):
        # valida si existen direcciones email destinatarios
        if email_delivery:
            # itera la cantidad de direcciones email para enviar correos a 1 o más destinatarios
            for email in email_delivery:
                auxbody = self._get_email_template(subject,body)
                name_to = ""
                records_pdfs = self._get_attachments(models_ids,model)
                # se obtienen las fichas o documentos a enviar en el correo electronico
                att_ids = self._iter_records(records_pdfs)
                root_email = self.env[modelos_repetidos[5]].search([('name','=',parametros_repetidos[1])])
                if not root_email:
                    raise UserError(mensajes_repetidos[0])
                author_id = self.env[modelos_repetidos[0]].search([('id','=',root_email.partner_id.id)])
                mail_pool = self.env[modelos_repetidos[6]]
                values = {
                                'subject' : subject,
                                'email_to' : "",
                                'email_from' : author_id.email,
                                'author_id':author_id.id,
                                'reply_to' : mensajes_repetidos[1],
                                'body_html' : "",
                                'body' : body,
                                'attachment_ids': [(6, 0, att_ids)],
                                'auto_delete': True,
                    }
                values['email_to'] = str(email)
                if name_delivery:
                    name_to = name_delivery
                auxbody = auxbody.replace('--name_to--',name_to)
                values['body_html'] = auxbody
                msg_id = mail_pool.create(values)
                if msg_id:
                    mail_pool.sudo().send([msg_id])
                else:
                    raise UserError(mensajes_repetidos[2])
    
    def button_generate_and_send_invoice(self):
        self.generate_invoice_donation()
        self.asig_extencion_in_files()
        self.send_email_charger()
        self.donation_status = 'charged'
    
    def button_gns_invoice_n_reference(self):
        self.generate_invoice_donation()
        self.asig_extencion_in_files()
        self.send_email_charger()
        self.asig_deposit_slip(self.cfdi_deposit_token)
        self.donation_status = 'invoiced'

    def asig_deposit_slip(self,reference):
        self.ensure_one()

        local_agreement = '1407279'

        vals = {
                'dependency_id': 269 ,#'743',         
                'sub_dependency_id': 1200, #'01',                            
                'res_model': self._name,                        
                'total_price': self.total_donation_amount,               
                'expiration_date': self.expiration_date,        
                'concepts': " - ".join([str(self.dependancy_id.dependency),self.sub_dependancy_id.sub_dependency,self.sub_dependancy_id.description, self.donation_concept,'('+mensajes_repetidos[6]+self.request_letter+')']).upper(), 
                'origin': 'SI',                                 
                'payment_type': 'EF'
            }
        token = self.env[modelos_repetidos[7]].create(vals)
        _logger.info(f"*** token: {token}")


        new_token = token.write({
                    'payment_reference': reference,
                    'agreement_number': local_agreement,
                    'date_of_issue':self.emission_date
                })
        _logger.info(f"*** new_token: {new_token}")

        desposit_slip_response = token._generate_report_file()
        _logger.info(f"*** new_token: {desposit_slip_response}")
        if desposit_slip_response:
            line = {
                        'donations_id':self._origin.id,
                        'deposit_token':token.id,
                    }
        self.deposit_tokens_line_ids = self.env[DEPOSIT_SLIP_LINE_MODEL].create(line)
        self.send_email_with_reference()
    

    def change_received(self):
        deposit_tokens_ids = self.env[DEPOSIT_SLIP_LINE_MODEL].search([('donations_id', '=', self._origin.id), ('is_canceled', '=', False)])
        len_tokens_ids = len(deposit_tokens_ids)
        recognizeds = deposit_tokens_ids.filtered(lambda dt: dt.recognized)
        len_recognizeds = len(recognizeds)
        if len_recognizeds == len_tokens_ids:
            self.donation_status = 'charged' if self.is_later_payment else 'received'
                

    def _default_expiration_date(self):
        today = datetime.today()
       
        if today.month == 12:
            new_date = datetime(today.year, 12, 31)
        else:
            new_month = (today.month % 12) + 1
            new_year = today.year + 1 if today.month == 12 else today.year
            new_day = 31 if new_month == 12 else today.day
            new_date = today.replace(year=new_year, month=new_month, day=new_day)
      
        return new_date

              

class sdBaseDepositSlipLine (models.Model):
    _name = "sd_base.deposit_slip.line"
    _description = "Lineas de Fichas de Deposito"

    donations_id = fields.Many2one('donations', string="Donation")
    deposit_token = fields.Many2one(modelos_repetidos[7])
    emission_date = fields.Datetime(string="Fecha de Emision")
    expiration_date = fields.Datetime(related='deposit_token.expiration_date', string="Fecha de Expiracion")
    amout_p_token = fields.Float(related='deposit_token.total_price', string="Monto", digits=(15,2))
    reference = fields.Char(related='deposit_token.payment_reference',string="Referencia")
    agreement_num = fields.Char(related='deposit_token.agreement_number',string="Convenio")
    deposit_slip_file = fields.Binary(related='deposit_token.deposit_slip_file',string="Archivo de ficha de depósito")
    deposit_slip_file_name = fields.Char(related='deposit_token.deposit_slip_file_name',string="Nombre del archivo de ficha de depósito")
    recognized = fields.Boolean(string='Pago Reconocido', default=False)
    is_canceled = fields.Boolean('Is canceled')
    canceled_amount = fields.Float(string="Canceled amount", digits=(15,2))
    original_donation_request_id = fields.Many2one('donations', "Original donation request")
    is_omited = fields.Boolean('Is omited')

    #relacion de las referencias
    def relatedReferences(self):
        #busca su referencia en el movimiento. se asigna True si el movimiento es de tipo recognized y si el monto es el mismo generado en la ficha
        bank_movement = self.env['bank.movements'].search([('ref','=',self.reference)])
        if bank_movement.type_movement == 'recognized':
            if bank_movement.charge_amount == self.amout_p_token:
                self.recognized = True
                

    def cancel_deposit_slip(self):
        actual_donation = self.donations_id
        deposit_tokens_line_id = self
        deposit_token = deposit_tokens_line_id.deposit_token
        
        deposit_tokens_line_id.is_canceled = True
        deposit_tokens_line_id.canceled_amount = deposit_tokens_line_id.amout_p_token
        actual_donation.total_donation_amount -= deposit_tokens_line_id.canceled_amount
        actual_donation.number_files_referred -= 1
        deposit_token.total_price = 0


    def omit_deposit_slip(self):
        actual_donation = self.donations_id
        deposit_tokens_line_id = self
        deposit_token_amount = deposit_tokens_line_id.deposit_token.total_price
         # Quitar la relación en la donación actual y disminuir el monto
        remove_deposit_tokens_line = [(3, self.id, False)]
        actual_donation.write({'deposit_tokens_line_ids': remove_deposit_tokens_line,
                               'total_donation_amount': deposit_token_amount,
                               'number_files_referred':  actual_donation.number_files_referred - 1})
        # Establecer que fue omitido y la solicitud original
        deposit_tokens_line_id.write({'original_donation_request_id': actual_donation.id,
                                      'is_omited': True})
         # Crear una nueva donación sin el registro omitido
        new_donation = actual_donation.copy()
        new_deposit_tokens_line_ids = [(4, deposit_tokens_line_id.id)]
        new_donation.write({'deposit_tokens_line_ids': new_deposit_tokens_line_ids,
                            'total_donation_amount': deposit_token_amount,
                            'number_files_referred': 1})

        return {
            "res_model": "donations",
            "type": "ir.actions.act_window",
            "view_mode": "form",
            "view_type": "form",
            "views": [(False, "form")],
            "view_id" : self.env.ref('siif_income.donations_view_form').id,
            "res_id": new_donation.id,
            "target": "new",
        }

    
class TypeDonations(models.Model):
    
    _name = "type.donations"
    _description = "Tipos de Donativo"

    name_type = fields.Char(string='Name')

    _sql_constraints = [('name_type', 'unique(name_type)', 'The type of donation must be unique.')]


    def name_get(self):
        result = []
        for record in self:
            name = record.name_type or ''
            if self.env.context.get('show_for_type_donation',False):
                name = record.name_type
            result.append((record.id,name))
        return result

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
        args = args or []
        if name:
            records = self.search([('name_type',operator,name)])
            return records.name_get()
        return self.search([('name_type',operator,name)]+args, limit=limit).name_get()


class LimitsUmasConfigurations(models.Model):

    _name = "limits.umas.configurations"
    _description = "Configuacion de limite de UMAS"

    limit = fields.Char(string='Limit UMAS')

    _sql_constraints = [('limit', 'unique(limit)', 'The limit of UMAS must be unique.')]


class BodyEmailDonations(models.Model):

    _name = modelos_repetidos[2]
    _description = 'Cuerpo de Correo Donativos'

    state = fields.Char(string='State')
    subject = fields.Char(string='Subject')
    msj = fields.Html(string='Messange in email')
    _sql_constraints = [('state', 'unique(state)', 'Donation state name must be unique.')]
    _sql_constraints = [('subject', 'unique(subject)', 'The email title must be unique.')]


    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(BodyEmailDonations,self).fields_view_get(
            view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])

        is_group_income_guest_user = self.env.user.has_group('jt_income.group_income_guest_user')
        is_group_income_dependence_user = self.env.user.has_group('jt_income.group_income_dependence_user')
        if is_group_income_dependence_user or is_group_income_guest_user:
            for node in doc.xpath("//" + view_type):
                node.set('create', '0')
                node.set('delete', '0')
                node.set('edit', '0')
                node.set('export', '0')
                node.set('export_xlsx', '0')

        res['arch'] = etree.tostring(doc)
        return res

    def name_get(self):
        result = []
        for record in self:
            name = record.state or ''
            if self.env.context.get('show_for_state',False):
                name = record.state
            result.append((record.id,name))
        return result

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
        args = args or []
        if name:
            records = self.search([('state',operator,name)])
            return records.name_get()
        return self.search([('state',operator,name)]+args, limit=limit).name_get()



UNIDADES = (
            'cero',
            'uno',
            'dos',
            'tres',
            'cuatro',
            'cinco',
            'seis',
            'siete',
            'ocho',
            'nueve'
        )

DECENAS = (
            'diez',
            'once',
            'doce',
            'trece',
            'catorce',
            'quince',
            'dieciseis',
            'diecisiete',
            'dieciocho',
            'diecinueve'
        )

DIEZ_DIEZ = (
            'cero',
            'diez',
            'veinte',
            'treinta',
            'cuarenta',
            'cincuenta',
            'sesenta',
            'setenta',
            'ochenta',
            'noventa'
        )

CIENTOS = (
            '_',
            'ciento',
            'doscientos',
            'trescientos',
            'cuatroscientos',
            'quinientos',
            'seiscientos',
            'setecientos',
            'ochocientos',
            'novecientos'
        )
MAX_NUMERO = 999999999999
import math


class PreviewInvoiceDonation(models.TransientModel):
    _name = wizzards[0]
    _description = 'Preview Invoice Donation'

    id_dona = fields.Integer()

    dep_emisora = fields.Char(string="Dependencia emisora", readonly=True)
    name = fields.Char(string="Donador", readonly=True)
    rfc = fields.Char(string="RFC", readonly=True)
    tax_regime_id = fields.Many2one(TAX_REGIME_MODEL, string="Tax regime", default=lambda self: self.env[TAX_REGIME_MODEL].search([('code', '=', '605')], limit=1))
    dom_fis = fields.Char(string="Domicilio Fiscal", readonly=True)
    cfdi_use = fields.Selection(CFDI_USE, string="Uso de CFDI")
    met_pag = fields.Selection(PAYMENT_METHOD, string="Método de Pago")
    clvProvSer = fields.Char(string="Clave Prod. Serv.",readonly=True)
    cant = fields.Char(string="Cantidad", readonly=True)
    cveUnidad = fields.Selection(UNIT_KEY, string="CVE Unidad")
    descripcion = fields.Char(string="Descripcion *", readonly=True)

    p_Unitario = fields.Float(string="P. Unitario *", readonly=True, digits=(15,2))
    importe = fields.Float(string="Importe *", readonly=True)
    subtotal = fields.Float(string="Subtotal *", readonly=True)
    total = fields.Float(string="Total *", readonly=True)
    can_let = fields.Char(string="Cantidad con letra *", compute='numero_a_letras', readonly=True)
    currency = fields.Char(string='Moneda', readonly=True)
    form_pago = fields.Selection(WAY_PAY, string="Forma de Pago")
    payment_service_key_id = fields.Many2one(PAYMENT_OF_INCOME_MODEL, string="Payment service key", default=lambda self: self.env[PAYMENT_OF_INCOME_MODEL].search([('name', '=', 'DE')], limit=1))


    def Button_save(self):
        donation = self.env['donations'].search([('id','=',self.id_dona)])
        donation.write({'cfdi_use':self.cfdi_use,
                        'cve_unidad':self.cveUnidad,
                        'met_pag':self.met_pag,
                        'form_pago':self.form_pago})

    def numero_a_letras(self):
        numero = self.p_Unitario
        parte_decimal,numero_entero = math.modf(numero)
        parte_decimal = float("{:.2f}".format(parte_decimal))
        numero_entero = math.floor(numero_entero)
        parte_decimal = math.floor(parte_decimal*100)
        if numero_entero > MAX_NUMERO:
            raise OverflowError('Número demasiado alto')
        _logger.info(f'*****************Su parte entera es {numero_entero} y su parte decimal es {parte_decimal}')
        _logger.info(f'*****************el tipo de dato de la parte entera es{type(numero_entero)}')
        if (numero_entero <= 99):
            resultado = self.leer_decenas(numero_entero)
        elif (numero_entero <= 999):
            resultado = self.leer_centenas(numero_entero)
        elif (numero_entero <= 999999):
            resultado = self.leer_miles(numero_entero)
        elif (numero_entero <= 999999999):
            resultado = self.leer_millones(numero_entero)
        else:
            resultado = self.leer_millardos(numero_entero)
        resultado = resultado + ' pesos'
        resultado = resultado.replace('uno mil', 'un mil')
        resultado = resultado.strip()
        resultado = resultado.replace(' _ ', ' ')
        resultado = resultado.replace('  ', ' ')
        if parte_decimal == 0:
            parte_decimal_str = str(parte_decimal)+'0/100 M.N.'
        else:
            parte_decimal_str = str(parte_decimal)+'/100 M.N.'
        if parte_decimal_str:
            resultado = '%s %s' % (resultado,parte_decimal_str)
        self.can_let = resultado.upper()

    def leer_decenas(self,numero):
        if numero < 10:
            return UNIDADES[numero]
        decena, unidad = divmod(numero, 10)
        if numero <= 19:
            resultado = DECENAS[unidad]
        elif 21 <= numero <= 29:
            resultado = 'veinti%s' % UNIDADES[unidad]
        else:
            resultado = DIEZ_DIEZ[decena]
            if unidad > 0:
                resultado = '%s y %s' % (resultado, UNIDADES[unidad])
        return resultado

    def leer_centenas(self,numero):
        centena, decena = divmod(numero, 100)
        if numero == 0:
            resultado = 'cien'
        else:
            resultado = CIENTOS[centena]
            if decena > 0:
                resultado = '%s %s' % (resultado, self.leer_decenas(decena))
        return resultado

    def leer_miles(self,numero):
        millar, centena = divmod(numero, 1000)
        resultado = ''
        if (millar == 1):
            resultado = ''
        if (millar >= 2) and (millar <= 9):
            resultado = UNIDADES[millar]
        elif (millar >= 10) and (millar <= 99):
            resultado = self.leer_decenas(millar)
        elif (millar >= 100) and (millar <= 999):
            resultado = self.leer_centenas(millar)
        resultado = '%s mil' % resultado
        if centena > 0:
            resultado = '%s %s' % (resultado, self.leer_centenas(centena))
        return resultado

    def leer_millones(self,numero):
        millon, millar = divmod(numero, 1000000)
        resultado = ''
        if (millon == 1):
            resultado = ' un millon '
        if (millon >= 2) and (millon <= 9):
            resultado = UNIDADES[millon]
        elif (millon >= 10) and (millon <= 99):
            resultado = self.leer_decenas(millon)
        elif (millon >= 100) and (millon <= 999):
            resultado = self.leer_centenas(millon)
        if millon > 1:
            resultado = '%s millones' % resultado
        if (millar > 0) and (millar <= 999):
            resultado = '%s %s' % (resultado, self.leer_centenas(millar))
        elif (millar >= 1000) and (millar <= 999999):
            resultado = '%s %s' % (resultado, self.leer_miles(millar))
        return resultado

    def leer_millardos(self,numero):
        millardo, millon = divmod(numero, 1000000)
        return '%s millones %s' % (self.leer_miles(millardo), self.leer_millones(millon))

class AlertDepositSlip(models.TransientModel):
    _name = wizzards[2]
    _description = mensajes_repetidos[5]

    msj_on_wizard = fields.Char(string="Alerta", readonly=True)
    id_dona = fields.Integer()


    def Button_in_alert(self):
        donation = self.env['donations'].search([('id','=',self.id_dona)])
        donation.new_deposit_tokens()
        
        

