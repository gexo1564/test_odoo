# -*- coding: utf-8 -*-
import logging
from odoo import models, fields, api, _

class ResUsers(models.Model):
    _inherit = 'res.users'

    dependency_ids = fields.Many2many(
        'dependency',
        'allowed_users_dep_cxp_rel',
        string = "User Permissions for Dependencies"
    )

    sub_dependency_ids = fields.Many2many(
        'sub.dependency',
        'allowed_users_sd_cxp_rel',
        string = "User Permissions for Sub Dependencies"
    )

    all_dep_subdep = fields.Boolean(string='All Dependencies selected')
    len_perm_assigned = fields.Integer(compute='_get_size_permissions',string="Permissions Assigned",store=False)

    def _get_size_permissions(self):
        for info in self:
            len_dep=len(info.dependency_ids)
            len_subdep=len(info.sub_dependency_ids)
            info.len_perm_assigned=len_dep+len_subdep

    #Asignación de Todos las Dependencias
    @api.onchange('all_dep_subdep')
    def _all_deps_selected(self):
        if (self.all_dep_subdep==True):
            dependency_ids = self.env['dependency'].search([])
            for user in self:
                user.dependency_ids=user.dependency_ids=[(6, 0, dependency_ids.ids)]
            logging.critical(user.dependency_ids)
        else:
            for user in self:
                if(len(user.dependency_ids)>=1):
                    user.dependency_ids=[(6, 0, [])]