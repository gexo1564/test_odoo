from odoo.tests import common
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from psycopg2.errors import NotNullViolation
import base64

class TestTotalDenominations(common.TransactionCase):
	def test_create_total_denominations(self):
		total_den=self.env['total.denominations']
		"""
			Caso 01: Denominación con número de billetes en 0
		"""
		den={
			'count':0,
			'denominations':'den_500'
		}
		with self.assertRaises(ValidationError):
			total_den.create(den)

		"""
			Caso 02: Denominación con número de billetes en negativo
		"""
		den2={
			'count':-5,
			'denominations':'den_500'
		}
		with self.assertRaises(ValidationError):
			total_den.create(den2)

		"""
			Caso 03: Denominación sin denominación
		"""
		den3={
			'count':5,
		}
		with self.assertRaises(NotNullViolation):
			total_den.create(den3)
		"""
			Caso 04: Denominación correcta
		"""
		den4={
			'count':20,
			'denominations':'den_500',
		}
		
		total_den.create(den4)
	
	def test_create_total_denominations_quote(self):
		total_den_quote=self.env['total.denominations.quote']
		"""
			Caso 01: Denominación con número de billetes en 0
		"""
		den={
			'count':0,
			'denominations':'den_500'
		}
		with self.assertRaises(ValidationError):
			total_den_quote.create(den)

		"""
			Caso 02: Denominación con número de billetes en negativo
		"""
		den2={
			'count':-5,
			'denominations':'den_500'
		}
		with self.assertRaises(ValidationError):
			total_den_quote.create(den2)

		"""
			Caso 03: Denominación sin denominación
		"""
		den3={
			'count':5,
		}
		with self.assertRaises(NotNullViolation):
			total_den_quote.create(den3)
		"""
			Caso 04: Denominación correcta
		"""
		den4={
			'count':20,
			'denominations':'den_500',
		}
		
		total_den_quote.create(den4)