from odoo import models, fields

class MyModuleMessageWizard(models.TransientModel):
    _name = 'payroll.payment.message.wizard'
    _description = "General Message"

    message = fields.Text('General Message', required=True)

    def action_close(self):
        return {'type': 'ir.actions.act_window_close'}