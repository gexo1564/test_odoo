import json
import logging

from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError

from odoo import models, fields, api, tools, _

_logger = logging.getLogger(__name__)
try:
    from num2words import num2words
except ImportError:
    _logger.warning("The num2words python library is not installed, amount-to-text features won't be fully available.")
    num2words = None

from lxml import etree

ACCOUNT_MOVE = 'account.move'
CHECK_LOG = 'check.log'
PAYMENT_BATCH_SUPPLIER_MODEL = 'payment.batch.supplier'
CHECK_PAYMENT_REQ_MODEL = 'check.payment.req'
ACCOUNT_MOVE_LINE_MODEL = 'account.move.line'
CHECKBOOK_REGISTRATION_TEXT = 'Checkbook registration'
ASSIGNED_FOR_SHIPPING_TEXT = 'Assigned for shipping'
AVAILABLE_FOR_PRINTING_TEXT = 'Available for printing'
IN_TRANSIT_TEXT = 'In transit'
PROTECTED_AND_TRANSIT_TEXT = 'Protected and in transit'
WITHDRAWN_FROM_CIRCULATION_TEXT = 'Withdrawn from circulation'
CANCELED_IN_CUSTODY = 'Canceled in custody of Finance'
ON_FILE_TEXT = 'On file'

class ResCurrency(models.Model):
    _inherit = "res.currency"

    def amount_to_text(self, amount):
        self.ensure_one()

        def _num2words(number, lang):
            try:
                return num2words(number, lang=lang).title()
            except NotImplementedError:
                return num2words(number, lang='en').title()

        if num2words is None:
            logging.getLogger(__name__).warning("The library 'num2words' is missing, cannot render textual amounts.")
            return ""

        formatted = "%.{0}f".format(self.decimal_places) % amount
        parts = formatted.partition('.')
        integer_value = int(parts[0])
        fractional_value = int(parts[2] or 0)

        lang_code = self.env.context.get('lang') or self.env.user.lang
        lang = self.env['res.lang'].with_context(active_test=False).search([('code', '=', lang_code)])
        amount_words = tools.ustr('{amt_value} {amt_word}').format(
            amt_value=_num2words(integer_value, lang=lang.iso_code),
            amt_word=self.currency_unit_label,
        )

        if fractional_value == 0:
            amount_words += ' 00/100 M.N.'

        if not self.is_zero(amount - integer_value):
            context = self._context
            if 'from_supplier_payment_batch_report' in context and \
                    'lang' in context and (context.get('lang') == 'es_MX' or context.get('lang') == 'es_MX'):
                amount_in_word = amount_words + ' ' + str(fractional_value) + '/100' + ' M.N.'
                amount_words = amount_in_word
            else:
                amount_words += ' ' + _('and') + tools.ustr(' {amt_value} {amt_word}').format(
                    amt_value=_num2words(fractional_value, lang=lang.iso_code),
                    amt_word=self.currency_subunit_label,
                )

        return amount_words


class PaymentBatchSupplier(models.Model):
    _name = PAYMENT_BATCH_SUPPLIER_MODEL
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = "Payment Batch Supplier"
    _rec_name = 'batch_folio'

    batch_folio = fields.Integer("Batch Folio")
    payment_issuing_bank_id = fields.Many2one("account.journal", "Payment Issuing Bank")
    payment_issuing_bank_acc_id = fields.Many2one("res.partner.bank", "Payment Issuing Bank Account")
    checkbook_req_id = fields.Many2one("checkbook.request", "Checkbook Number")
    type_of_payment_method = fields.Selection([('handbook', 'Handbook'),
                                               ('checks', 'Checks')], "Type of Payment Method")
    payment_date = fields.Date(string="Payment Date",default=datetime.today())
    amount_of_checkes = fields.Integer("Amount of Checkes", compute='_get_check_data')
    intial_check_folio = fields.Many2one("check.log", compute='_get_check_data')
    final_check_folio = fields.Many2one("check.log", compute='_get_check_data')


    @api.depends('check_status_filter')
    def _compute_domain_payment_req_ids(self):
        return []

    payment_req_ids = fields.One2many(CHECK_PAYMENT_REQ_MODEL, "payment_batch_id", "Check Payment Requests", domain=_compute_domain_payment_req_ids)
    printed_checks = fields.Boolean("Printed checks")
    description_layout = fields.Text("Description Layout")
    selected = fields.Boolean("Select All")
    selected_available = fields.Boolean("Select Available All")

    type_of_batch = fields.Selection(
        [
            ('supplier','Supplier'),
            ('project','Project'),
            ('nominal','Nominal'),
            ('reissue_payroll','Reissue Payroll'),
            ('pension','Pension'),
        ],string="Type Of Batch"
    )
    move_line_ids = fields.One2many(
        ACCOUNT_MOVE_LINE_MODEL, 'payment_batch_check_id', string="Journal Items")

    check_status_filter = fields.Selection([(CHECKBOOK_REGISTRATION_TEXT, CHECKBOOK_REGISTRATION_TEXT),
                        (ASSIGNED_FOR_SHIPPING_TEXT, ASSIGNED_FOR_SHIPPING_TEXT),
                        (AVAILABLE_FOR_PRINTING_TEXT, AVAILABLE_FOR_PRINTING_TEXT),
                        ('Printed', 'Printed'), ('Delivered', 'Delivered'),
                        (IN_TRANSIT_TEXT, IN_TRANSIT_TEXT), ('Sent to protection','Sent to protection'),
                        (PROTECTED_AND_TRANSIT_TEXT,PROTECTED_AND_TRANSIT_TEXT),
                        ('Protected', 'Protected'), ('Detained','Detained'),
                        (WITHDRAWN_FROM_CIRCULATION_TEXT,WITHDRAWN_FROM_CIRCULATION_TEXT),
                        ('Cancelled', 'Cancelled'),
                        (CANCELED_IN_CUSTODY, CANCELED_IN_CUSTODY),
                        (ON_FILE_TEXT,ON_FILE_TEXT),('Destroyed','Destroyed'),
                        ('Reissued', 'Reissued'),('Charged','Charged')], string="Check Status Filter")

    is_payroll_payment_reissue = fields.Boolean()


    def show_payment_line_ids(self):
        payment_req_ids = self.mapped('payment_req_ids.payment_req_id').ids
        
        if not payment_req_ids:
            self.payment_move_line_ids = False
            return
        
        query = """
            SELECT line.id
            FROM account_payment payment
            JOIN account_move_line line ON line.payment_id = payment.id
            WHERE payment.payment_request_id IN %s
                AND payment.state NOT IN ('draft', 'cancelled')
        """
        
        self.env.cr.execute(query, (tuple(payment_req_ids),))
        result = self.env.cr.fetchall()
        
        line_ids = [row[0] for row in result]
        
        self.payment_move_line_ids = [(6, 0, line_ids)]
            

    payment_move_line_ids = fields.Many2many(ACCOUNT_MOVE_LINE_MODEL, 'rel_account_move_payment_line_check', 'payment_req_id', 'line_id', string='Payment Journal Items',
                                             compute="show_payment_line_ids")

    @api.onchange('payment_issuing_bank_id')
    def onchange_payment_issuing_bank_id(self):
        if self.payment_issuing_bank_id:
            self.payment_issuing_bank_acc_id = self.payment_issuing_bank_id and self.payment_issuing_bank_id.bank_account_id and self.payment_issuing_bank_id.bank_account_id.id or False
        else:
            self.payment_issuing_bank_acc_id = False


    @api.onchange('select_all')
    def select_lines(self):
        self.payment_req_ids.write({'selected': True})
        self.selected = True
    
    
    @api.onchange('deselect_all')
    def deselect_lines(self):
        self.payment_req_ids.write({'selected': False})
        self.selected = False


    def select_available_lines(self):
        for line in self.payment_req_ids.filtered(lambda x:x.check_status==AVAILABLE_FOR_PRINTING_TEXT):
            line.selected = True
        self.selected_available = True

    def deselect_available_lines(self):
        for line in self.payment_req_ids.filtered(lambda x:x.check_status==AVAILABLE_FOR_PRINTING_TEXT):
            line.selected = False
        self.selected_available = False


    def get_date(self):
        date = self.payment_date and self.payment_date or False
        if date:
            day = date.day
            month = date.month
            month_name = ''
            if month == 1:
                month_name = 'Enero'
            elif month == 2:
                month_name = 'Febrero'
            elif month == 3:
                month_name = 'Marzo'
            elif month == 4:
                month_name = 'Abril'
            elif month == 5:
                month_name = 'Mayo'
            elif month == 6:
                month_name = 'Junio'
            elif month == 7:
                month_name = 'Julio'
            elif month == 8:
                month_name = 'Agosto'
            elif month == 9:
                month_name = 'Septiembre'
            elif month == 10:
                month_name = 'Octubre'
            elif month == 11:
                month_name = 'Noviembre'
            elif month == 12:
                month_name = 'Diciembre'
            year = date.year
            return str(day) + ' de ' + month_name + ' de ' + str(year)


    def _get_check_data(self):
        for rec in self:
            req_list = [req for req in rec.payment_req_ids if req.check_folio_id]
            rec.amount_of_checkes = len(req_list)

            if req_list:
                reqs = sorted(req_list, key=lambda req: req.check_folio_id.id)
                rec.intial_check_folio = reqs[0].check_folio_id.id
                rec.final_check_folio = reqs[-1].check_folio_id.id
            else:
                rec.intial_check_folio = False
                rec.final_check_folio = False
            

    def get_check_protection_journal(self,batch_type):
        if batch_type=='supplier':
            journal = self.env.ref('jt_check_controls.supplier_checks_protection_journal')
        elif batch_type=='project':
            journal = self.env.ref('jt_check_controls.project_checks_protection_journal')
        elif batch_type=='pension':
            journal = self.env.ref('jt_check_controls.pension_checks_protection_journal')
        else:
            journal = self.env.ref('jt_check_controls.payroll_checks_protection_journal')
        return journal
    
    #! Apuntes ya no se usan
    def create_check_protection_entry(self,batch,lines):
        journal = self.get_check_protection_journal(batch.type_of_batch)
        if not journal.paid_credit_account_id or not journal.conac_paid_credit_account_id \
            or not journal.paid_debit_account_id or not journal.conac_paid_debit_account_id :
            raise ValidationError(_("Please configure UNAM and CONAC Paid account in %s journal!" %
                                  journal.name))

        today = datetime.today().date()
        user = self.env.user
        partner_id = user.partner_id.id
        name = "Payment Batch "+str(batch.type_of_batch)+" "+str(batch.batch_folio)
        move_lines = []
        for line in lines:
            amount = line.amount_to_pay
            line_name = line.check_folio_id and line.check_folio_id.folio_ch or ''
            move_lines.append((0,0,{
                             'account_id': journal.paid_credit_account_id.id,
                             'coa_conac_id': journal.conac_paid_credit_account_id.id,
                             'credit': amount,
                             'partner_id': partner_id,
                             'payment_batch_check_id' : batch.id,
                             'check_payment_req_id' : line.id,
                             'name' : line_name,
                             
                }))
            move_lines.append((0,0,{
                             'account_id': journal.paid_debit_account_id.id,
                             'coa_conac_id': journal.conac_paid_debit_account_id.id,
                             'debit': amount,
                             'partner_id': partner_id,
                             'payment_batch_check_id' : batch.id,
                             'check_payment_req_id' : line.id,
                             'name' : line_name,
                }))
            
        unam_move_val = { 'ref': name,  'conac_move': True,
                         'date': today, 'journal_id': journal.id, 'company_id': self.env.user.company_id.id,
                         'payment_batch_check_id' : batch.id,
                         'line_ids': move_lines}
        move_obj = self.env[ACCOUNT_MOVE]
        unam_move = move_obj.create(unam_move_val)
        unam_move.action_post()

    def action_protected_checks(self):
        # Check if the selected bank has established check protection validity
        selected_bank = self.payment_issuing_bank_id and self.payment_issuing_bank_id.bank_id
        if selected_bank and selected_bank.check_protection_term == 0:
            raise ValidationError(_("The selected bank has not established the validity of check protection, please configure it."))

        # Filter selected payment lines and retrieve related check folio IDs
        selected_lines = self.payment_req_ids.filtered(lambda x: x.selected)

        # Check if any checks are selected
        if not selected_lines:
            raise ValidationError(_("Select some check."))

        if self.type_of_batch in ('nominal', 'reissue_payroll'):
            # Check if any selected checks are in 'Sent to protection' status
            if selected_lines.filtered(lambda x: x.check_folio_id.status == 'Sent to protection'):
                # Update accounting records for nominal checks
                self.accounting_action_nominal(selected_lines)

                if self.is_payroll_payment_reissue:
                    # Update payment state to 'paid' for related account moves
                    query_update_payment = """
                        update account_move set payment_state = 'paid'
                        where id in (select payment_req_id from check_payment_req where id in %s)
                    """
                    self.env.cr.execute(query_update_payment, (tuple(selected_lines.ids), ))

        elif self.type_of_batch in ('supplier', 'project'):
            # Find account payments related to the selected checks
            account_payments = self.env[ACCOUNT_MOVE].search([('check_folio_id', 'in', selected_lines.mapped('check_folio_id').ids)])
            for_payment_procedure_state = account_payments.filtered(lambda payment: payment.payment_state == 'for_payment_procedure')

            if not for_payment_procedure_state:
                raise ValidationError(_("It can only be protected in for payment procedure."))

            # Update payment state to 'paid' for payments in 'for_payment_procedure' state
            for_payment_procedure_state.write({'payment_state': 'paid'})

            # Perform accounting actions for supplier and project batches
            self.accounting_action_supplier(selected_lines)
            self.accounting_action_payment_request_supplier(selected_lines)

        # Update status, date_protection, date_expiration checks
        query = """
            UPDATE check_log
            SET status = CASE
                WHEN status = 'Sent to protection' AND %(type_of_batch)s IN ('supplier', 'project') THEN 'Protected and in transit'
                WHEN status = 'Sent to protection' THEN 'Protected'
                ELSE COALESCE(status, 'Sent to protection')
            END,
            date_protection = %(today)s,
            date_expiration = CASE
                WHEN status = 'Sent to protection' THEN %(today)s + interval '%(check_protection_term)s days'
                ELSE date_expiration
            END
            WHERE id IN %(checks_ids)s
                AND (status = 'Sent to protection')
        """

        params = {
            'type_of_batch': self.type_of_batch,
            'today': datetime.today().date(),
            'check_protection_term': int(self.payment_issuing_bank_id.bank_id.check_protection_term),
            'checks_ids': tuple(selected_lines.mapped('check_folio_id').ids),
        }

        self.env.cr.execute(query, params)
        self.env.cr.commit()

        # Reset 'selected' flag for selected lines
        selected_lines.write({'selected': False})

    def accounting_action_payment_request_supplier(self, selected_lines):
        account_creditor = self.env['account.catalog.type'].search([('catalog_type', '=', 'supplier')])
 
        if not account_creditor:
            raise ValidationError(_("There isn't supplier account in the Account Catalog Type."))
                 
        for payment in selected_lines: 
            payment.payment_req_id.line_ids = [
                    (0, 0, {
                    'name':	 'ACREEDORES proveedor',
                    'account_id': account_creditor[-1].account_id.id, 
                    'debit': payment.amount_to_pay,
                    'partner_id': payment.payment_req_id.partner_id.id or False,
                    'move_id': payment.payment_req_id.id,
                    'dependency_id':payment.payment_req_id.dependancy_id.id,
                    'sub_dependency_id':payment.payment_req_id.sub_dependancy_id.id,
                    'exclude_from_invoice_tab': True,
                    }),
                    (0, 0, {
                    'name':	 payment.payment_req_id.partner_id.property_account_payable_id.name,    
                    'account_id': payment.payment_req_id.partner_id.property_account_payable_id.id,
                    'credit': payment.amount_to_pay,
                    'partner_id': payment.payment_req_id.partner_id.id or False,
                    'move_id': payment.payment_req_id.id,
                    'dependency_id':payment.payment_req_id.dependancy_id.id,
                    'sub_dependency_id':payment.payment_req_id.sub_dependancy_id.id,
                    'exclude_from_invoice_tab': True,
                    }),
                ]
            
            payment.payment_req_id.action_post()          
          

    def accounting_action_supplier(self, selected_lines):
        today = datetime.today().date()
        company_id = self.env.user.company_id.id

        journal_id = self.payment_issuing_bank_id
        account_topay_supplier = selected_lines[-1].payment_partner_id.property_account_payable_id
        account_creditor = self.env['account.catalog.type'].search([('catalog_type', '=', 'supplier')])

        journal_protection = self.env['check.journal.catalog'].search([('account_type', '=', 'supplier'), ('journal_type', '=', 'protection')])
        account_paid_debit = journal_protection.journal_id.paid_debit_account_id.id
        account_paid_credit = journal_protection.journal_id.paid_credit_account_id.id

        if not account_paid_debit or not account_paid_credit:
            raise ValidationError(_("There isn't paid account in the supplier check protection journal."))
         
        move_vals = []
        for payment in selected_lines:
            move_vals.append({'ref': '', 'conac_move': False, 'date': today, 'journal_id': journal_id.id, 'company_id': company_id,
                'line_ids': [
                    (0, 0, {
                    'name':	 account_topay_supplier.name,#'CUENTAS POR PAGAR EN PROCESO DE PAGO MXN',    
                    'account_id': account_topay_supplier.id,
                    'debit': payment.amount_to_pay,
                    'partner_id': payment.payment_req_id.partner_id.id or False,
                    'payment_batch_check_id': self.id,
                    'exclude_from_invoice_tab': True,
                    }),
                    (0, 0, {
                    'name':	 'ACREEDORES proveedor',
                    'account_id': account_creditor[-1].account_id.id, 
                    'credit': payment.amount_to_pay,
                    'partner_id': payment.payment_req_id.partner_id.id or False,
                    'payment_batch_check_id': self.id,
                    'exclude_from_invoice_tab': True,
                    }),
                    (0, 0, {
                    'name':	 'P.E Pagado',
                    'account_id': account_paid_credit,
                    'credit': payment.amount_to_pay,
                    'partner_id': payment.payment_req_id.partner_id.id or False,
                    'payment_batch_check_id': self.id,
                    'exclude_from_invoice_tab': True,
                    }),
                    (0, 0, {
                    'name':	 'P.E Pagado',    
                    'account_id': account_paid_debit,
                    'debit': payment.amount_to_pay,
                    'partner_id': payment.payment_req_id.partner_id.id or False,
                    'payment_batch_check_id': self.id,
                    'exclude_from_invoice_tab': True,
                    }),
                ]}
            )
                   
        move_obj = self.env[ACCOUNT_MOVE]
        move_id = move_obj.create(move_vals)
        move_id.action_post()


    def accounting_action_nominal(self, selected_lines):
        journal_id = self.payment_issuing_bank_id
        account_topay_supplier = selected_lines[-1].payment_partner_id.property_account_payable_id
        account_paid = journal_id.paid_debit_account_id.id
        account_exercised = journal_id.execercise_debit_account_id.id
        account_creditor = self._get_account_creditor_protection_nominal()
        journal_protection = self._get_journal_protection_nominal()

        if not account_creditor:
            raise ValidationError(_("There isn't nominal account in the Account Catalog Type."))

        if not journal_protection:
            raise ValidationError(_("There isn't journal for protection in the Check Journal Catalog."))

        if not account_paid:
            raise ValidationError(_("The journal doesn't have paid account."))

        if not account_exercised:
            raise ValidationError(_("The journal doesn't exercised account."))

        payment_ids = selected_lines.mapped('payment_id.id')
        if payment_ids:
            self._payroll_paymet_post(payment_ids, account_topay_supplier, journal_protection)

    def _get_account_creditor_protection_nominal(self):
        account_creditor = self.env['account.catalog.type'].search([('catalog_type', '=', 'nomina'),('type_for_nomina', '=', 'normal')])

        return account_creditor[-1].account_id

    def _get_journal_protection_nominal(self):
        journal = self.env['check.journal.catalog'].search([('account_type', '=', 'nominal'), ('journal_type', '=', 'protection')])

        return journal[-1]

    def _payroll_paymet_post(self, ids, account_topay_supplier, journal_protection):
        """
            Método optimizado para realizar el post de los pagos de nómina
        """
        debit_protect_account = account_topay_supplier.id
        credit_protect_account = self.env['account.catalog.type']._get_nominal_param('normal').id
        debit_reissue_account = self.env['account.catalog.type']._get_nominal_param('check_cancel').id
        credit_reissue_account = self.env['account.catalog.type']._get_nominal_param('check_forwarded').id
        # Se obtienen los datos de los pagos para la póliza de account.move
        query_move = """
            select
                p.id,
                p.partner_id,
                p.communication as ref,
                p.payment_date,
                p.amount price_unit,
                p.employee_payroll_id,
                p.payment_method_id,
                p.dependancy_id,
                p.sub_dependancy_id,
                p.fornight,
                p.folio,
                p.payroll_request_type,
                p.check_folio_id,
                p.l10n_mx_edi_payment_method_id,
                p.is_payroll_payment_reissue
            from account_payment p
            where p.id in %s
        """
        self.env.cr.execute(query_move, (tuple(ids), ))

        for payment in self.env.cr.dictfetchall():
            # Asignación de la cuentas de debito/credito, por defecto se asignan
            # las cuentas de protección de cheques
            debit_account = debit_protect_account
            credit_account = credit_protect_account
            # Si el pago esta asociado a una reexpedición se usan las cuentas de proteccíon
            # por reexpedición
            if payment.get('is_payroll_payment_reissue'):
                debit_account = debit_reissue_account
                credit_account = credit_reissue_account
            # Cuenta de la solicitud de pago/cheque
            payment.update({'type': 'entry', 'journal_id': journal_protection.journal_id.id})

            # Creación de de la Póliza contable (account.move)
            # # logging.info("Creación de la poliza de pago ...")
            move_id = self.env[ACCOUNT_MOVE].insert_sql([payment])[0]
            # # logging.info("Ok")
            # Creación de los apuntes contables de pago: Cuenta de nomina vs Banco
            # # logging.info("Generación de las líneas de los apuntes contables ...")
            lines = []
            # Referencia del asiento contable
            check_folio = self.env[CHECK_LOG].browse([payment.get('check_folio_id')])
            name = "%s Protección de cheque QNA: %s" % (check_folio.folio_ch or '', self.fornight or '')
            # Débito: Cuenta de por pagar de nómina
            lines.append({
                'price_unit': payment.get('price_unit'),
                'amount_residual': payment.get('price_unit'),
                'date_maturity': payment.get('payment_date'),
                'account_id': debit_account,
                'dependency_id': payment.get('dependancy_id'),
                'sub_dependency_id': payment.get('sub_dependancy_id'),
                'payment_batch_check_id': self.id,
                'partner_id': payment.get('partner_id'),
                'ref': payment.get('ref'),
            })
            # Crédito: Cuenta de acreedores
            lines.append({
                'price_unit': -payment.get('price_unit'),
                'amount_residual': -payment.get('price_unit'),
                'date_maturity': payment.get('payment_date'),
                'account_id':  credit_account,
                'payment_batch_check_id': self.id,
                'partner_id': payment.get('partner_id'),
                'exclude_from_invoice_tab': True,
                'ref': payment.get('ref'),
            })
            # Generación de las PRESUSPUESTALES DE PAGO
            if not payment.get('is_payroll_payment_reissue'):
                query_program_code = None
                if payment.get('payroll_request_type', '') == 'payroll_payment':
                    query_program_code = """
                        select l.program_code_id program_code_id,
                            pc.dependency_id,
                            pc.sub_dependency_id,
                            sum(amount)::numeric price_unit
                        from preception_line l, preception p, program_code pc
                        where l.preception_id = p.id
                            and l.program_code_id = pc.id
                            and p.is_net_pay is True
                            and l.payroll_id = %s
                        group by l.program_code_id, pc.dependency_id, pc.sub_dependency_id
                    """
                elif payment.get('payroll_request_type', '') == 'additional_benefits':
                    query_program_code = """
                        select l.program_code_id program_code_id,
                            pc.dependency_id,
                            pc.sub_dependency_id,
                            sum(amount)::numeric price_unit
                        from additional_payments_line l, program_code pc
                        where l.program_code_id = pc.id
                            and l.payroll_id = %s
                        group by l.program_code_id, pc.dependency_id, pc.sub_dependency_id
                    """
                if query_program_code:
                    self.env.cr.execute(query_program_code, (payment.get('employee_payroll_id'),))
                    # Para cada código programático
                    for vals in self.env.cr.dictfetchall():
                        # Inicialización de valores
                        vals.update({
                            'exclude_from_invoice_tab': True,
                            'conac_move': True,
                            'payment_batch_check_id': self.id,
                            'partner_id': payment.get('partner_id'),
                            'ref': payment.get('ref'),
                        })
                        # Crédito: Cuenta 825.001.001.001
                        line_credit = vals.copy()
                        line_credit.update({
                            'account_id': journal_protection.journal_id.paid_credit_account_id.id ,
                            'coa_conac_id': journal_protection.journal_id.conac_paid_credit_account_id.id,
                            'price_unit': -vals.get('price_unit'),
                        })
                        lines.append(line_credit)
                        # Debito: Cuenta 826.001.001.001
                        line_debit = vals.copy()
                        line_debit.update({
                            'account_id': journal_protection.journal_id.paid_debit_account_id.id,
                            'coa_conac_id': journal_protection.journal_id.conac_paid_debit_account_id.id,
                            'price_unit': vals.get('price_unit'),
                        })
                        lines.append(line_debit)

            # Creación de los apuntes contables
            self.env[ACCOUNT_MOVE_LINE_MODEL].insert_sql(move_id, lines, name=name)
            # logging.info("Obteniendo account.move ...")
            # move_id = self.env[ACCOUNT_MOVE].browse([move_id])
            # logging.info("Ok")
            # Se publican los asientos contables
            # logging.info("Publicando los asientos contables ...")
            self.env[ACCOUNT_MOVE]._post(move_id, journal_protection.journal_id.id)


    def accounting_action_payment_request_nominal(self, selected_lines):
        account_creditor = self.env['account.catalog.type'].search([('catalog_type', '=', 'nomina'),('type_for_nomina', '=', 'normal')])
 
        if not account_creditor:
            raise ValidationError(_("There isn't nominal account in the Account Catalog Type."))

        for payment in selected_lines:
            payment.payment_req_id.line_ids = [
                    (0, 0, {
                    'name':	 account_creditor[-1].name,
                    'account_id': account_creditor[-1].account_id.id, 
                    'debit': payment.amount_to_pay,
                    'partner_id': payment.payment_req_id.partner_id.id or False,
                    'move_id': payment.payment_req_id.id,
                    'dependency_id':payment.payment_req_id.dependancy_id.id,
                    'sub_dependency_id':payment.payment_req_id.sub_dependancy_id.id,
                    'exclude_from_invoice_tab': True,
                    }),
                    (0, 0, {
                    'name':	 payment.payment_req_id.partner_id.property_account_payable_id.name,    
                    'account_id': payment.payment_req_id.partner_id.property_account_payable_id.id,
                    'credit': payment.amount_to_pay,
                    'partner_id': payment.payment_req_id.partner_id.id or False,
                    'move_id': payment.payment_req_id.id,
                    'dependency_id':payment.payment_req_id.dependancy_id.id,
                    'sub_dependency_id':payment.payment_req_id.sub_dependancy_id.id,
                    'exclude_from_invoice_tab': True,
                    }),
                ]
            
            payment.payment_req_id.action_post()        
    

    def action_send_file_to_protection(self):
        for rec in self:
            payment_req_lines = rec.payment_req_ids.filtered(lambda x: x.selected == True)
            if payment_req_lines:
                if rec.type_of_batch in ('supplier', 'project'):
                    status_condition = 'Delivered'
                else:
                    status_condition = 'Printed'
                
                check_folio_ids = payment_req_lines.mapped('check_folio_id')
                
                # Update the status using SQL query
                query = """
                    UPDATE check_log
                    SET status = 'Sent to protection'
                    WHERE id IN %s AND status = %s
                """
                
                self.env.cr.execute(query, (tuple(check_folio_ids.ids), status_condition))
                self.env.cr.commit()
                
                # Reset selected fields
                payment_req_lines.write({'selected': False})
                rec.write({'selected': False})
            else:
                raise ValidationError(_('No seleccionó ningún registro. Asegúrese de seleccionar los registros que se incluyeron dentro del layout de protección que será enviado a la Institución bancaria.'))

    
    def print_checks_and_set_status(self):

        for rec in self:
            rec.printed_checks = True
            self.env.cr.commit()
                    
        return self.env.ref('jt_check_controls.payslip_batch_supplier').report_action(self)


    def get_toolbar_print_checks_and_set_status(self):
        toolbars = [
            ('action', 'jt_check_controls.action_assign_check_folio'),
            ('action', 'jt_check_controls.action_mass_confirm_check_folio'),
            ('action', 'jt_check_controls.action_deliver_checks'),
            ('action', 'jt_check_controls.action_layout_check_protection'),
            ('action', 'jt_check_controls.action_send_file_to_protection'),
            ('action', 'jt_check_controls.action_protected_checks'),
            ('action', 'jt_check_controls.action_request_payment_batch_document'),
            ('print', 'jt_check_controls.action_confirm_n_print'),
        ]
        show_list = []
        for t in toolbars:
            toolbar_id = self.env.ref(t[1])
            if toolbar_id:
                show_list.append((t[0], toolbar_id.id))
        return show_list

        

    def action_deliver_checks(self):
        selected_lines = self.payment_req_ids.filtered(lambda x: x.selected)
        
        if self.type_of_batch in ('supplier', 'project'):
            printed_check_ids = selected_lines.filtered(lambda x: x.check_folio_id.status == 'Printed').mapped('check_folio_id').ids
            if printed_check_ids:
                query = """
                    UPDATE check_log
                    SET status = 'Delivered'
                    WHERE id IN %s AND status = 'Printed'
                """
                self.env.cr.execute(query, (tuple(printed_check_ids),))
                self.env.cr.commit()

                activity_type = self.env.ref('mail.mail_activity_data_todo').id
                summary = "Payment Request checks are in delivered status"
                model_id = self.env['ir.model'].sudo().search([('model', '=', ACCOUNT_MOVE)]).id

                activity_values = [{
                    'activity_type_id': activity_type,
                    'res_model': ACCOUNT_MOVE,
                    'res_id': line.payment_req_id.id,
                    'res_model_id': model_id,
                    'summary': summary,
                    'user_id': self.env.user.id
                } for line in selected_lines if line.check_folio_id.status == 'Delivered' and line.payment_req_id]

                self.env['mail.activity'].create(activity_values)
        else:
            protected_check_ids = selected_lines.filtered(lambda x: x.check_folio_id.status == 'Protected').mapped('check_folio_id').ids
            if protected_check_ids:
                query = """
                    UPDATE check_log
                    SET status = IN_TRANSIT_TEXT
                    WHERE id IN %s AND status = 'Protected'
                """
                self.env.cr.execute(query, (tuple(protected_check_ids),))
                self.env.cr.commit()
        
        selected_lines.write({'selected': False})
        self.write({'selected': False})


    def action_layout_check_protection(self):
        rec_id = False
        bank_layout = False
        if self:
            bank_ids = self.mapped('payment_issuing_bank_id')
            if len(bank_ids) > 1:
                raise ValidationError(_("Not Allowed to generate layout of different bank"))
            
            rec_id = self[0].id
            if self[0].payment_issuing_bank_id and self[0].payment_issuing_bank_id.bank_id and self[0].payment_issuing_bank_id.bank_id.name:
                if self[0].payment_issuing_bank_id.bank_id.name.upper()== 'Banamex'.upper():
                    bank_layout = 'Banamex'
                elif self[0].payment_issuing_bank_id.bank_id.name.upper()== 'BBVA Bancomer'.upper():
                    bank_layout = 'BBVA Bancomer'
                elif self[0].payment_issuing_bank_id.bank_id.name.upper()== 'Inbursa'.upper():
                    bank_layout = 'Inbursa'
                elif self[0].payment_issuing_bank_id.bank_id.name.upper()== 'Santander'.upper():
                    bank_layout = 'Santander'
                elif self[0].payment_issuing_bank_id.bank_id.name.upper()== 'Scotiabank'.upper():
                    bank_layout = 'Scotiabank'
        return {
            'name': _('Generate Check Layout'),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': False,
            'res_model': 'generate.supp.check.layout',
            'domain': [],
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': {'active_ids':self.ids,'default_batch_id': rec_id,'default_layout':bank_layout}
        }
    

    def set_related_check_log(self, line):
        if line.check_folio_id:
            previous_data = ''
            
            if line.payment_req_id:
                for check in line.payment_req_id.related_check_folio_ids:
                    if check.id != line.check_folio_id.id:
                        previous_data = ','.join(str(c.folio) for c in line.payment_req_id.related_check_folio_ids if c.id != check.id)
                        
                        current_check_data = str(line.check_folio_id.folio)
                        current_check_data += ','.join(str(c.folio) for c in line.payment_req_id.related_check_folio_ids if c.id != check.id)
                        
                        check.related_checks = current_check_data
            
            line.check_folio_id.related_checks = previous_data
        
  
    def action_assign_check_folio(self):
        check_log_obj = self.env[CHECK_LOG]
        check_payment_req_obj = self.env[CHECK_PAYMENT_REQ_MODEL]
        
        for rec in self:
            if rec.checkbook_req_id:
                payment_record_assign = rec.payment_req_ids.filtered(lambda x: x.check_status and x.check_status != 'Cancelled' and x.selected)
                payment_record_cancel = rec.payment_req_ids.filtered(lambda x: x.check_status and x.check_folio_id and x.check_folio_id.reason_cancellation != 'Error de Impresión' and x.check_status == 'Cancelled' and x.selected)
                
                if payment_record_assign or payment_record_cancel:
                    raise ValidationError(_('Cannot assign a new check folio because they already have one'))
                
                payment_record = rec.payment_req_ids.filtered(lambda x: x.check_status == 'Printed' and x.selected)
                
                if payment_record:
                    raise ValidationError(_('A new check folio cannot be assigned to a check that has been confirmed as Printed.'))
                
                count = rec.payment_req_ids.filtered(lambda x: x.selected)
                logs = check_log_obj.search([
                    ('checklist_id.checkbook_req_id', '=', rec.checkbook_req_id.id),
                    ('status', '=', AVAILABLE_FOR_PRINTING_TEXT)
                ], order='folio').ids
                
                exit_logs = check_payment_req_obj.search([('check_folio_id', 'in', logs)]).mapped('check_folio_id').ids
                logs = list(set(logs) ^ set(exit_logs))
                
                if len(logs) < len(count):
                    raise ValidationError(_('Not enough checks available to assign printing!'))
                
                if logs:
                    logs = check_log_obj.search([('id', 'in', logs)], order='folio')
                    
                    for line, log in zip(rec.payment_req_ids.filtered(lambda x: x.selected), logs):
                        line.check_folio_id = log
                        line.check_folio_id.check_amount = line.amount_to_pay
                        
                        if line.payment_req_id and line.payment_req_id.check_folio_id:
                            previous_data = line.payment_req_id.related_check_history
                            
                            if previous_data:
                                previous_data += "," + str(line.payment_req_id.check_folio_id.folio)
                            else:
                                previous_data = str(line.payment_req_id.check_folio_id.folio)
                            
                            line.payment_req_id.related_check_history = previous_data
                            line.payment_req_id.related_check_folio_ids = [(4, line.payment_req_id.check_folio_id.id)]
                            line.payment_req_id.check_folio_id.related_check_folio_id = line.check_folio_id.id
                            line.check_folio_id.related_check_folio_id = line.payment_req_id.check_folio_id.id
                            
                        line.payment_req_id.check_folio_id = line.check_folio_id.id
                        self.set_related_check_log(line)
                        
                        line.selected = False
                        
                        if line.payment_req_id.check_folio_id:
                            line.payment_req_id.payment_state = 'assigned_payment_method'
                    
                    rec.printed_checks = True
                
                if not logs:
                    raise ValidationError(_('No checks available to assign!'))
            
            rec.selected = False


    def confirm_printed_checks(self):
        selected_lines = self.payment_req_ids.filtered(lambda x: x.selected and x.check_status == AVAILABLE_FOR_PRINTING_TEXT and x.check_folio_id)

        if not selected_lines:
            raise ValidationError(_("Select Check Payment Requests to confirm!"))

        line_vals = [{
            'check_folio_id': line.check_folio_id.id,
            'payment_id': line.payment_id.id,
            'payment_req_id': line.payment_req_id.id,
            'currency_id': line.currency_id.id,
            'amount_to_pay': line.amount_to_pay,
            'check_status': line.check_status
        } for line in selected_lines]

        return {
            'name': _('Check Print'),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': False,
            'res_model': 'confirm.printed.check',
            'domain': [],
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': {
                'default_payment_req_ids': [(0, 0, val) for val in line_vals],
                'default_batch_ids': [(6, 0, self.ids)]
            }
        }


    #================== Accounting Entry =========================#

    def get_check_printing_journal(self,batch_type):
        if batch_type=='supplier':
            journal = self.env.ref('jt_check_controls.supplier_check_printing_journal')
        elif batch_type=='project':
            journal = self.env.ref('jt_check_controls.project_check_printing_journal')
        elif batch_type=='pension':
            journal = self.env.ref('jt_check_controls.pension_check_printing_journal')
        else:
            journal = self.env.ref('jt_check_controls.payroll_check_printing_journal')
        return journal
    
    def create_check_printing_entry(self,batch,lines):
        journal = self.get_check_printing_journal(batch.type_of_batch)
        if not journal.execercise_credit_account_id or not journal.conac_exe_credit_account_id \
            or not journal.execercise_debit_account_id or not journal.conac_exe_debit_account_id :
            raise ValidationError(_("Please configure UNAM and CONAC Exercised account in %s journal!" %
                                  journal.name))

        today = datetime.today().date()
        user = self.env.user
        name = "Payment Batch "+str(batch.type_of_batch)+" "+str(batch.batch_folio)
        move_lines = []
        for line in lines:
            partner_id = line.payment_partner_id and line.payment_partner_id.id or user.partner_id.id     
            amount = line.amount_to_pay
            line_name = line.check_folio_id and line.check_folio_id.folio_ch or ''  
            move_lines.append((0,0,{
                             'account_id': journal.execercise_credit_account_id.id,
                             'coa_conac_id': journal.conac_exe_credit_account_id.id,
                             'credit': amount,
                             'partner_id': partner_id,
                             'payment_batch_check_id' : batch.id,
                             'check_payment_req_id' : line.id,
                             'name': line_name,
                             
                }))
            move_lines.append((0,0,{
                             'account_id': journal.execercise_debit_account_id.id,
                             'coa_conac_id': journal.conac_exe_debit_account_id.id,
                             'debit': amount,
                             'partner_id': partner_id,
                             'payment_batch_check_id' : batch.id,
                             'check_payment_req_id' : line.id,
                             'name': line_name,
                             
                }))
            
        unam_move_val = { 'ref': name,  'conac_move': True,
                         'date': today, 'journal_id': journal.id, 'company_id': self.env.user.company_id.id,
                         'payment_batch_check_id' : batch.id,
                         'line_ids': move_lines}
        move_obj = self.env[ACCOUNT_MOVE]
        unam_move = move_obj.create(unam_move_val)
        unam_move.action_post()

    def action_request_attach_file(self):
        return {
            'name': _('Attach File'),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': False,
            'res_model': 'payment.lot.attach.file',
            'domain': [],
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context':self.env.context,
        }

    #Función de seguridad en Módulo de Finanzas
    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super().fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])
        is_admin= self.env.user.has_group('jt_payroll_payment.group_on_finance_admin')
        """
        Sección de Seguridad aplicado para:
            Módulo de Finanzas
                -Protección de Cheques
                    -Proveedores
                    -Nómina
                    -Proyectos
                    -Pago de pensión            
        """
        if 'payment.batch.supplier.' in res['name'] :
            if is_admin:
                for node in doc.xpath("//" + view_type):
                    node.set('edit', '1')
                    node.set('create', '1')
                    node.set('delete', '1')
                    node.set('import', '1')
                    node.set('export_xlsx', '1')


        res['arch'] = etree.tostring(doc)
        return res


class CheckPaymentRequests(models.Model):
    _name = CHECK_PAYMENT_REQ_MODEL
    _description = "Check Payment Request"

    payment_batch_id = fields.Many2one(PAYMENT_BATCH_SUPPLIER_MODEL)
    check_folio_id = fields.Many2one(CHECK_LOG, "Check Folio")
    payment_id = fields.Many2one('account.payment')
    payment_req_id = fields.Many2one(ACCOUNT_MOVE)
    payment_partner_id = fields.Many2one('res.partner', string="Beneficiary of the payment")
    currency_id = fields.Many2one(
        'res.currency', default=lambda self: self.env.user.company_id.currency_id, string="Currency")
    amount_to_pay = fields.Monetary("Amount to Pay", currency_field='currency_id')
    selected = fields.Boolean("Select")
    zone = fields.Integer('Zone',compute="get_zone_data")
    is_withdrawn_circulation = fields.Boolean(default=False, copy=False)
    layout_generated = fields.Boolean(default=False, copy=False,string="Layout Generated")
    protected_cheque_file = fields.Binary(default=False, copy=False,string="Protected Cheque File")
    protected_cheque_file_name = fields.Char(default=False, copy=False,string="Protected Cheque File")
    
    check_status = fields.Selection([(CHECKBOOK_REGISTRATION_TEXT, CHECKBOOK_REGISTRATION_TEXT),
                                     (ASSIGNED_FOR_SHIPPING_TEXT, ASSIGNED_FOR_SHIPPING_TEXT),
                                     (AVAILABLE_FOR_PRINTING_TEXT, AVAILABLE_FOR_PRINTING_TEXT),
                                     ('Printed', 'Printed'), ('Delivered', 'Delivered'),
                                     (IN_TRANSIT_TEXT, IN_TRANSIT_TEXT), ('Sent to protection', 'Sent to protection'),
                                     (PROTECTED_AND_TRANSIT_TEXT, PROTECTED_AND_TRANSIT_TEXT),
                                     ('Protected', 'Protected'), ('Detained', 'Detained'),
                                     (WITHDRAWN_FROM_CIRCULATION_TEXT, WITHDRAWN_FROM_CIRCULATION_TEXT),
                                     ('Cancelled', 'Cancelled'),
                                     (CANCELED_IN_CUSTODY, CANCELED_IN_CUSTODY),
                                     (ON_FILE_TEXT, ON_FILE_TEXT), ('Destroyed', 'Destroyed'),
                                     ('Reissued', 'Reissued'), ('Charged', 'Charged')], related='check_folio_id.status',
                                    store=False)


    @api.depends('zone')
    def get_zone_data(self):

        U = "369369369703603603658148148143692692692036936936981471471476925925925369269269214704704709258258258"
        count = 0
        importe = ''
        NumChq = ''
        vl_impaux = ""
        va = ""
        total = ""
        DIG1 = ""
        dig2 = ""
        numchqs = ""
        zone = {}
        for record in self:
            
            importe = str(record.check_folio_id.folio)
            NumChq = str(record.amount_to_pay)
            IJ = str.find(importe, ".")
                # Elimina el . del importe
            if IJ == 0:
                pass
            else:
                vl_txtaux = importe[0:IJ]
                vl_txtaux = vl_txtaux + importe[IJ + 1:11]
                vl_impaux = vl_txtaux

            # Otra vez elimina cualquier punto del importe
            vl_txtaux = ""
            for IJ in range(0, len(vl_impaux)):
                if vl_impaux[IJ] != ".":
                    vl_txtaux = vl_txtaux + vl_impaux[IJ]
          
            # Rellena con ceros a la izquierda hasta que sean 12 dígitos
            for IJ in range(0, 12 - len(vl_txtaux)):
                vl_txtaux = "0" + vl_txtaux

            #justifica con zeros a la izquierda el numchq hasta 8 dígitos
            numchqs = ""
            for IJ in range(1, 8 - len(NumChq) + 1):
                numchqs = "0" + numchqs
          
            # une el número de cheque y el importe
            numchqs = numchqs + NumChq

            # Agrega otro cero a la izquierda. Deben ser 21 caracteres
            va = "0" + numchqs + vl_txtaux 

            # Calcula el primer dígito
            total = 0

            # Revisa con isdigit porque hay comas en la cadena del importe
            # Si hay coma no se suma nada
            if str.isdigit(va[2]):
                total = total + int(va[2])
            if str.isdigit(va[5]):
                total = total + int(va[5])
            if str.isdigit(va[8]):
                total = total + int(va[8])
            if str.isdigit(va[11]):
                total = total + int(va[11])
            if str.isdigit(va[14]):
                total = total + int(va[14])
            if str.isdigit(va[17]):
                total = total + int(va[17])
            if str.isdigit(va[20]):
                total = total + int(va[20])

            #total = int(va[2]) + int(va[5]) + int(va[8]) + int(va[11]) + \
            #        int(va[14]) + int(va[17]) + int(va[20])   
            
            #inicia la suma de pares 1ra parte
            if str.isdigit(va[0:2]) and int(va[0:2]) > 0:
                total = total + int(U[int(va[0:2])-1])

            if str.isdigit(va[3:5]) and int(va[3:5]) > 0:
                total = total + int(U[int(va[3:5])-1])

            if str.isdigit(va[6:8]) and int(va[6:8]) > 0:
                total = total + int(U[int(va[6:8])-1])

            if str.isdigit(va[9:11]) and int(va[9:11]) > 0:
                total = total + int(U[int(va[9:11])-1])

            if str.isdigit(va[12:14]) and int(va[12:14]) > 0:
                total = total + int(U[int(va[12:14])-1])

            if str.isdigit(va[15:17]) and int(va[15:17]) > 0:
                total = total + int(U[int(va[15:17])-1])

            if str.isdigit(va[18:20]) and int(va[18:20]) > 0:
                total = total + int(U[int(va[18:20])-1])

            DIG1 = 10 - int(str(total)[-1])

            if DIG1 == 10:
                DIG1 = 0

            # Calcula el segundo dígito
            va = ""
            total = 0
            va = numchqs + vl_txtaux + str(DIG1)

            if str.isdigit(va[2]):
                total = total + int(va[2])
            if str.isdigit(va[5]):
                total = total + int(va[5])
            if str.isdigit(va[8]):
                total = total + int(va[8])
            if str.isdigit(va[11]):
                total = total + int(va[11])
            if str.isdigit(va[14]):
                total = total + int(va[14])
            if str.isdigit(va[17]):
                total = total + int(va[17])
            if str.isdigit(va[20]):
                total = total + int(va[20])

            #total = int(va[2]) + int(va[5]) + int(va[8]) + int(va[11]) + \
            #        int(va[14]) + int(va[17]) + int(va[20])

            if str.isdigit(va[0:2]) and int(va[0:2]) > 0:
                total = total + int(U[int(va[0:2])-1])

            if str.isdigit(va[3:5]) and int(va[3:5]) > 0:
                total = total + int(U[int(va[3:5])-1])

            if str.isdigit(va[6:8]) and int(va[6:8]) > 0:
                total = total + int(U[int(va[6:8])-1])

            if str.isdigit(va[9:11]) and int(va[9:11]) > 0:
                total = total + int(U[int(va[9:11])-1])

            if str.isdigit(va[12:14]) and int(va[12:14]) > 0:
                total = total + int(U[int(va[12:14])-1])

            if str.isdigit(va[15:17]) and int(va[15:17]) > 0:
                total = total + int(U[int(va[15:17])-1])

            if str.isdigit(va[18:20]) and int(va[18:20]) > 0:
                total = total + int(U[int(va[18:20])-1])

            dig2 = 10 - int(str(total)[-1])
            
            if dig2 == 10:
                dig2 = 0


            # Une los dos dígitos
            digitos = str(DIG1) + str(dig2)
            record.zone = digitos
        return zone


    def insert_sql(self, lines):
        def _get_column_stored_name():
            query = """
                select column_name from information_schema.columns
                where table_schema = 'public'
                and table_name   = 'check_payment_req' and column_name <> 'id'
            """
            self.env.cr.execute(query, ())
            return [f'"{res[0]}"' for res in self.env.cr.fetchall()]

        def _get_insert_sql():
            columns = _get_column_stored_name() 
            column = ", ".join(columns)
            values = ", ".join(["%s"] * len(columns))
            return f"insert into check_payment_req ({column}) values({values}) returning 1", columns

        def _get_insert_params(columns, vals):
            def update_vals(updates):
                for k, v in updates:
                    if k not in vals: vals.update({k: v})

            res = []
            updates = [
                ('currency_id', self.env.user.company_id.currency_id.id),
                ('zone', ""),
                ('is_withdrawn_circulation', False),
                ('layout_generated', False),
            ]
            update_vals(updates)
            for column in columns:
                res.append(vals.get(column[1:-1], None))
            return tuple(res)

        ids = []
        query, columns = _get_insert_sql()
        params = []
        for line in lines:
            params.append(_get_insert_params(columns, line))
        for p in params:
            self.env.cr.execute(query, p)
            res = (self.env.cr.fetchone() or [None])[0]
            if not res:
                raise ValidationError("Error al insertar el registro check.payment.req")
            ids.append(res)
        return ids

    @api.constrains('check_folio_id')
    def _check_number(self):
        for rec in self:
            if rec.check_folio_id:
                exit_lines = self.env[CHECK_PAYMENT_REQ_MODEL].search([('check_folio_id','=',rec.check_folio_id.id),('id','!=',rec.id)])
    #             if self.env.user.lang == 'es_MX':
    #                 raise ValidationError(_('El número de Actividad Institucional debe ser un valor numérico'))
    #             else:
                if exit_lines:
                    raise ValidationError(_('This check folio already assing to other payment request'))

    def select_lines(self):
        for line in self:
            if line.selected:
                line.selected = False
            else:
                line.selected = True

class PaymentLotAttachFile(models.TransientModel):
    _name = 'payment.lot.attach.file'

    file_data= fields.Binary(string='Attachment For Protection')
    file_name = fields.Char('Attachment For Protection')

    def action_attach(self):
        if self.env.context and self.env.context.get('active_ids'):
            active_ids = self.env.context.get('active_ids', [])
            payment_reqs = self.env[PAYMENT_BATCH_SUPPLIER_MODEL].browse(active_ids).payment_req_ids.filtered(lambda r: r.selected)
            if payment_reqs:
                payment_reqs.write({
                    'protected_cheque_file_name': self.file_name,
                    'protected_cheque_file': self.file_data,
                })
            else:
                return {
                    'name': _('Hey Warning!'),
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'alert.pbs.wizard',
                    'type': 'ir.actions.act_window',
                    'target': 'new',
                    'context':self.env.context,
                    'position': 'center',
                }

class AlertWizzardPaymentBatchSupp(models.TransientModel):
    _name = 'alert.pbs.wizard'
    _description = 'Alerta de Cheques'

    msj_on_wizard = fields.Char(string="¡!", readonly=True, default="Por favor seleccione los registros correspondientes al archivo de layout de respuesta bancaria.")
