// Crea una instancia de observer
let mutationObserver = new MutationObserver(function (mutations) {
  mutations.forEach(function (mutation) {
    if (mutation.target.className === 'o_loading') {
      //Busca por title dentro el th de la tabla
      let partnerId = $("th[data-name='partner_id']");
      //Valor del ultimo span de la barra de busqueda
      let inputValue = $(".o_facet_values span").last();

      // Existe el titulo en la tabla
      if (partnerId.text() === 'Partner' || partnerId.text() === 'Cte/Prov/Dep' || partnerId.text() === 'Empleado') {

        if (inputValue.text().toLowerCase().startsWith('fcd01')) {
          // fcd01
          partnerId.text('Dependencia / Subdependencia')
        } else if (inputValue.text().toLowerCase().startsWith('fdi02')) {
          // fdi02
          partnerId.text('Cliente');
        } else if (inputValue.text().toLowerCase().startsWith('dgg02')) {
          // dgg02
          partnerId.text('Proveedor');
        }

      }

    }


  });
});

//Configura el observer:
let config = {
  attributes: true, subtree: true, //childList: true, 
  //attributeOldValue: true, characterDataOldValue: true, characterData: true,
};
//selecciona el nodo target
let target = document.documentElement
//Pasa al observer el nodo y la configuracion
mutationObserver.observe(target, config);

