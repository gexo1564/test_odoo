from odoo import models, fields, api,_
from odoo.exceptions import UserError, ValidationError

class ResPartnerBank(models.Model):

    _inherit = 'res.partner.bank'

    current_balance = fields.Monetary("Actual Amount", currency_field='currency_id')
    committed_balance = fields.Monetary('Committed Balance', currency_field='currency_id')
    available_balance = fields.Monetary('Available Balance', currency_field='currency_id')

    def set_current_balance(self, current_balance):
        self.current_balance = current_balance
        self.available_balance = round(self.current_balance - self.committed_balance, 2)

    def add_expense(self, expense):
        self.committed_balance = round(self.committed_balance + expense, 2)
        self.available_balance = round(self.available_balance - expense, 2)

    def confirm_expense(self, expense):
        self.committed_balance = round(self.committed_balance - expense, 2)

    def cancel_expense(self, expense):
        self.committed_balance = round(self.committed_balance - expense, 2)
        self.available_balance = round(self.available_balance + expense, 2)


    #Boton de la vista de Fondo Fijo para solicitudes de devoluciones
    def historical_button(self):
        return {
            'name': 'Historial de Montos',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'res.partner.bank.historical',
            'domain': [('res_partner_bank_id', '=', self.id)],
            'type': 'ir.actions.act_window',
        }