# -*- coding: utf-8 -*-

from odoo.tools.safe_eval import safe_eval
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
from datetime import datetime
import time
import json
import logging
import pytz

# Define Constant
NAME_MODULE = 'siif.budget.mgmt.income.budgets'
NAME_LINES = 'siif.budget.mgmt.income.budgets.lines'
NAME_MIRROR = 'siif.budget.mgmt.income.budget.mirror'
ACCOUNT_FISCAL_YEAR = "account.fiscal.year"
FISCAL_YEAR = "Año Fiscal"
INITIAL_DATE = 'Fecha Inicial'
END_DATE = 'Fecha Final'
ACCOUNT_JOURNAL = 'account.journal'
IR_SEQUENCE = 'ir.sequence'
CALENDAR_ASSIGNED_AMOUNTS = 'calendar.assigned.amounts'
CLASSIFIER_INCOME_ITEM = "classifier.income.item"
LINEAS_MODEL = 'Lineas Model'
TECHNICAL_FIELD_FOR_UX_PURPOSE = "Technical field for UX purpose."

class IncomeBudget(models.Model):
    _name = NAME_MODULE
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Income Budgets'
    _rec_name = 'name'

    # Lineas Presupuesto
    line_budgets = fields.One2many(NAME_LINES, 'my_id', 'Lines')
    sequence_tree = fields.Integer(string = 'Sequence', store = False)

    # Lineas Asientos Contables
    line_accounting_entries = fields.One2many('account.move.line', 'income_butgets_id', 'Asientos Contables')
    sequence_tree = fields.Integer(string = 'Sequence', store = False)

    budget_mirror = fields.One2many(NAME_MIRROR, 'my_id', 'Lines')
    sequence__tree = fields.Integer(string = 'Sequence', store = False)


    name = fields.Text(string='Nombre del Presupuesto', tracking=True) # , states={'validate': [('readonly', True)]})
    fiscal_year = fields.Many2one(comodel_name = ACCOUNT_FISCAL_YEAR, string = FISCAL_YEAR)
    user_id = fields.Many2one(comodel_name='res.users', string='Responsable', default=lambda self: self.env.user, tracking=True) # , states={'validate': [('readonly', True)]})
    total_budget = fields.Float(string='Presupuesto Total', tracking=True, store=True) # , compute="_compute_total_budget"
    import_record_number = fields.Integer(string='Número de Registros Importados', compute = '_compute_record_number', store = True) # , compute='_get_count', readonly=True 
    from_date = fields.Date(string=INITIAL_DATE, related='fiscal_year.date_from', tracking=True, store=True) # , states={'validate': [('readonly', True)]}
    to_date = fields.Date(string=END_DATE, related='fiscal_year.date_to', tracking=True, store=True) # , states={'validate': [('readonly', True)]}
    journal_id = fields.Many2one(ACCOUNT_JOURNAL, string = 'Diario')
    state = fields.Selection([('draft', 'Borrador'),
                              ('previous', 'Previo'),
                              ('confirmed', 'Confirmado'),
                              ('validated', 'Validado'),
                             ], string="Estado", default='draft', track_visibility="always")
    policy_description = fields.Char(string='Poliza Descripción', tracking=True)
    documentary_support = fields.Many2many("ir.attachment", relation="income_budgets_documentary_rel", column1="document_id", column2="attachment_id", string="Agregar Soporte Documental")
    registration_date = fields.Datetime(string = "Fecha Registro", readonly=True, default=datetime.today())

    lines_added_later = fields.Integer(string="Líneas Agregadas Porteriormente")
    
    @api.model
    def default_get(self, fields):
        res = super(IncomeBudget, self).default_get(fields)
        v_journal = 'Presupuestos/D00001'
        budget_app_jou = self.env['siif.budget.mgmt.relation.journal.process'].search([('name', '=', v_journal)])
        if not budget_app_jou:
            res.update({'journal_id': False})
            raise ValidationError(_(f" No se encontró el diario {v_journal} en la Relación Diario-Proceso, solicite al administrador que lo modifique"))
        elif budget_app_jou:
            res.update({'journal_id': budget_app_jou.journal_name.id})
        return res
        
    @api.model
    def create(self, vals):
        res = super(IncomeBudget, self).create(vals)
        year_fiscal = self.env['account.fiscal.year'].search([('id', '=', vals['fiscal_year'])])
        year = year_fiscal.name
        fiscal_year = ''
        year_sequence = self.env[IR_SEQUENCE]
        if year:
            existing_sequence = self.env[IR_SEQUENCE].search([('name', 'like', f'Secuencia Presupuesto de Ingresos {year}')])
            if existing_sequence:
                fiscal_year = existing_sequence[0].code
            else:
                new_sequence = year_sequence.create({
                    'name': f'Secuencia Presupuesto de Ingresos {year}',
                    'code': f'seq.siif.budget.mgmt.income.budgets.{year}',
                    'padding': 3,
                    'implementation': 'standard',
                })
                fiscal_year = new_sequence.code
        seq = self.env[IR_SEQUENCE].next_by_code(fiscal_year)
        res.name = "Presupuesto Ingresos" + "/" + str(year) + "/" + str(seq)
        return res

        
    def action_draft(self):
        self.ensure_one()
        self.state = 'draft'

    def action_previous(self):
        self.ensure_one()
        self.state = 'previous'
        self.correct_line()
    
    def action_confirmed(self):
        self.ensure_one()
        self.state = 'confirmed'
    
    def action_validated(self):
        self.ensure_one()
        self.fiscal_year_validated()
        self.state = 'validated'
        self.validated()
        self.validated_calendar_assigned_amount()

        # copiar al por recaudar
        budget_estimated = self.env[NAME_LINES].search([('my_id', '=', self.id)])
        if budget_estimated.ids:
            for lines in budget_estimated:
                lines.to_collect_january_field = lines.estimated_january_field
                lines.to_collect_february_field = lines.estimated_february_field
                lines.to_collect_march_field = lines.estimated_march_field
                lines.to_collect_april_field = lines.estimated_april_field
                lines.to_collect_may_field = lines.estimated_may_field
                lines.to_collect_june_field = lines.estimated_june_field
                lines.to_collect_july_field = lines.estimated_july_field
                lines.to_collect_august_field = lines.estimated_august_field
                lines.to_collect_september_field = lines.estimated_september_field
                lines.to_collect_october_field = lines.estimated_october_field
                lines.to_collect_november_field = lines.estimated_november_field
                lines.to_collect_december_field = lines.estimated_december_field
                lines.to_collect_total = lines.estimated_total
        
        # copiar al modificado
        budget_modified = self.env[NAME_LINES].search([('my_id', '=', self.id)])
        if budget_modified.ids:
            for lines in budget_modified:
                lines.modified_january_field = lines.estimated_january_field
                lines.modified_february_field = lines.estimated_february_field
                lines.modified_march_field = lines.estimated_march_field
                lines.modified_april_field = lines.estimated_april_field
                lines.modified_may_field = lines.estimated_may_field
                lines.modified_june_field = lines.estimated_june_field
                lines.modified_july_field = lines.estimated_july_field
                lines.modified_august_field = lines.estimated_august_field
                lines.modified_september_field = lines.estimated_september_field
                lines.modified_october_field = lines.estimated_october_field
                lines.modified_november_field = lines.estimated_november_field
                lines.modified_december_field = lines.estimated_december_field
                lines.modified_total = lines.estimated_total
            
    
    def import_lines_button(self):
        return {
            'name': _('Importar Líneas'),
            'type': 'ir.actions.act_window',
            'res_model': 'import.budgets.line',
            'view_mode': 'form',
            'view_type': 'form',
            'views': [(False, 'form')],
            'context': {
                'default_father_budget': self.id,
            },
            'target': 'new',
            }
    
    def _compute_success_rows(self):
        for record in self:
            record.success_rows = 0
            try:
                data = safe_eval(record.success_row_ids)
                record.success_rows = len(data)
            except Exception as e:
                raise ValidationError(e)
    
    
    def fiscal_year_validated(self):
        # if self.fiscal_year:
            id_butget = self.env[NAME_MODULE].search([('fiscal_year', '=', self.fiscal_year.id), ('state', '=', 'validated')])
            for ids in id_butget:
                if ids.state == 'validated' and ids.fiscal_year.name == self.fiscal_year.name:
                    raise ValidationError(_('El Presupuesto con nombre %s ya está validado y tiene el mismo año fiscal que el que deseas validar.' % (ids.name)))

    def correct_line(self):
        if self.success_rows != self.total_rows:
            raise ValidationError(_('No puede seguir con el proceso de validación del presupuesto ya que del total de líneas del excel son %s y %s fueron exitósas' % (self.total_rows, self.success_rows)))
           
    # Import Status Lines
    validation_status = fields.Boolean(string = "Estado de Validación")
    total_rows = fields.Integer(string="Total Líneas", readonly = True, store = True) # , compute="_compute_total_rows"
    success_rows = fields.Integer(string='Total Líneas Exitosas', readonly = True, store=True, compute="_compute_success_rows")
    failed_rows = fields.Integer(string='Total Líneas Erróneas', readonly = True, store = True)
    budget_file = fields.Binary(string='Archivo Importado')
    budget_file_filename = fields.Char(string='File name')
    failed_row_file = fields.Binary(string = "Archivo de Líneas Erróneas")
    failed_row_filename = fields.Char(string='File name', compute='_compute_failed_row_filename')
    
    # sobreescritura de la vista en la parte de los asientos contables
    date = fields.Date(string = "Fecha", store=False)
    company_id = fields.Many2one(comodel_name = "res.company", string = "Compañia", store=False)
    move_id = fields.Many2one(comodel_name = "account.move", string = "Asiento Contable", store=False)
    account_id = fields.Many2one(comodel_name = "account.account", string = "Cuenta", store=False)
    cri = fields.Char(string='CRI', store=False)
    debit = fields.Float(string = "Debe", store=False)
    credit = fields.Float(string = "Haber", store=False)


    def validated(self):
        if self.journal_id:
            move_obj = self.env['account.move']
            journal = self.journal_id
            today = self.fiscal_year.date_from
            income_butgets_id = self.id
            user = self.env.user
            partner_id = user.partner_id.id
            for lines in self.line_budgets:
                amount = lines.estimated_total
                company_id = user.company_id.id
                if not journal.estimate_income_debit_account_id or not journal.estimate_income_credit_account_id \
                        or not journal.conac_estimate_income_debit_account_id or not journal.conac_estimate_income_credit_account_id:
                    if self.env.user.lang == 'es_MX':
                        raise ValidationError(_("Por favor configure la cuenta UNAM y CONAC en diario!"))
                    else:
                        raise ValidationError(_("Please configure UNAM and CONAC account in budget journal!"))
                unam_move_val = {'ref': self.name, 'income_butgets_id': income_butgets_id, 'conac_move': True, 'type': 'entry',
                                 'date': today, 'journal_id': journal.id, 'company_id': company_id,
                                 'line_ids': [(0, 0, {
                                     'account_id': journal.estimate_income_credit_account_id.id,
                                     'coa_conac_id': journal.conac_estimate_income_credit_account_id.id,
                                     'credit': amount,
                                     'partner_id': partner_id,
                                     'income_butgets_id': self.id,
                                     'cri': lines.estimated_cri_id.name,
                                     'name': self.policy_description,
                                 }), (0, 0, {
                                     'account_id': journal.estimate_income_debit_account_id.id,
                                     'coa_conac_id': journal.conac_estimate_income_debit_account_id.id,
                                     'debit': amount,
                                     'partner_id': partner_id,
                                     'income_butgets_id': self.id,
                                     'cri': lines.estimated_cri_id.name,
                                     'name': self.policy_description,
                                 })]}
                unam_move = move_obj.create(unam_move_val)
                unam_move.action_post()
    
            

    def unlink(self):
        if self.state == 'validated':
            raise ValidationError(_("El %s está VALIDADO, No se puede eliminar!!!" % (self.name)))
        res = super(IncomeBudget, self).unlink()
        return res
    
    # Definir nombre para el archivo de errores
    def _compute_failed_row_filename(self):
        for record in self:
            tz = pytz.timezone('America/Mexico_City')
            filename = f'Registros_erroneos-{record.name}-{datetime.now(tz).strftime("%Y-%m-%d_%H:%M:%S")}.txt'
            record.failed_row_filename = filename
    
    #- Método que modifica el css de la vista en Presupuesto de Ingresos
    x_css = fields.Html(string='CSS', sanitize=False, compute='_compute_css', store=False)
    @api.depends('state')
    def _compute_css(self):
        is_admin_user = self.env.user.has_group('jt_budget_mgmt.group_budget_admin_user')
        is_budget_control_user = self.env.user.has_group('siif_budget_mgmt.group_budget_budget_control_user')
        

        for record in self:
            record.x_css = ""

            if is_admin_user:
                #Ocultar boton de editar en Presupuesto de Ingresos unas vez que llega a estado de Validado
                if record.state in ['confirmed', 'validated']:
                    record.x_css += """
                        <style>
                            .o_form_view .o_form_statusbar > .o_statusbar_status {
                                max-width:85%;
                            }
                            .o_form_button_edit {
                                display: none !important;
                            }
                        </style>
                    """

            if is_budget_control_user:
                #Ocultar boton de editar en Presupuesto de Ingresos unas vez que llega a estado de Validado
                if record.state in ['confirmed', 'validated']:
                    record.x_css += """
                        <style>
                            .o_form_view .o_form_statusbar > .o_statusbar_status {
                                max-width:85%;
                            }
                            .o_form_button_edit {
                                display: none !important;
                            }
                        </style>
                    """
                else:
                    record.x_css += """
                        <style>
                            .o_form_view .o_form_statusbar > .o_statusbar_status{
                                max-width:85%;
                            }
                        </style>
                    """
    ######################################################################################################################
    # Función para agregar el subsidio a las líneas del presupuesto si hay subsidiodado de alta y en estatus de validado,#
    # de lo contrarió no agrega la línea                                                                                 #
    ######################################################################################################################
    def validated_calendar_assigned_amount(self):
        budget_estimated = self.env[NAME_LINES].search([('my_id', '=', self.id)])
        calendar_assigned_lines = self.env['calendar.assigned.amounts.lines'].search([('year', '=', budget_estimated.estimated_fiscal_year.name)])
        calendar_assigned = self.env[CALENDAR_ASSIGNED_AMOUNTS].search([('id', '=', calendar_assigned_lines.calendar_assigned_amount_id.id)])
        journal_id = self.env[ACCOUNT_JOURNAL].search([('id', '=', calendar_assigned.journal_id.id)])
        # logging.info(f"****************** Diario: {journal_id.name}")
        account_id = self.env['account.account'].search([('id', '=', journal_id.default_credit_account_id.id)])
        # logging.info(f"****************** Cuenta: {account_id.code}")
        account_id_cri = self.env['relation.account.cri'].search([('cuentas_id', '=', account_id.id)])
        # logging.info(f"****************** CRI: {account_id_cri.cri.name}")
        cri_id = self.env['classifier.income.item'].search([('id', '=', account_id_cri.cri.id)])
        # logging.info(f"****************** CRI: {cri_id.name}")
        if calendar_assigned_lines.calendar_assigned_amount_id.state == 'validate':

            sum_january = 0
            sum_february = 0
            sum_march = 0
            sum_april = 0
            sum_may = 0
            sum_june = 0
            sum_july = 0
            sum_august = 0
            sum_september = 0
            sum_october = 0
            sum_november = 0
            sum_december = 0
            sum_total = 0


            for rec in calendar_assigned_lines:
                sum_january += rec.january
                sum_february += rec.february
                sum_march += rec.march
                sum_april += rec.april
                sum_may += rec.may
                sum_june += rec.june
                sum_july += rec.july
                sum_august += rec.august
                sum_september += rec.september
                sum_october += rec.october
                sum_november += rec.november
                sum_december += rec.december
                sum_total += rec.annual_amount
        
            lines_budget = []

            lines_budget.append((0,0,{
                'estimated_fiscal_year': budget_estimated.estimated_fiscal_year.id,
                'estimated_from_date': budget_estimated.estimated_fiscal_year.date_from,
                'estimated_to_date': budget_estimated.estimated_fiscal_year.date_to,
                'estimated_cri_id': cri_id.id,
                'estimated_january_field': sum_january,
                'estimated_february_field': sum_february,
                'estimated_march_field': sum_march,
                'estimated_april_field': sum_april,
                'estimated_may_field': sum_may,
                'estimated_june_field': sum_june,
                'estimated_july_field': sum_july,
                'estimated_august_field': sum_august,
                'estimated_september_field': sum_september,
                'estimated_october_field': sum_october,
                'estimated_november_field': sum_november,
                'estimated_december_field': sum_december,
                'estimated_total': sum_total,
            
            }))

            self.write({'line_budgets': lines_budget})
    
    def delete_records(self):
        # for data in self:
        budget_id = self.env[NAME_MODULE].browse(self._context.get('active_id'))
        domain = [
                  ('my_id', '=', budget_id.id)
                #   ('to_collect_january_field', '!=', 0.0)
                 ]
        records = self.env[NAME_LINES].search(domain)
        records.unlink()

        estimated_mirror = self.env[NAME_MIRROR].search([('my_id', '=', budget_id.id)])
        calendar_assigned_lines = self.env['calendar.assigned.amounts.lines'].search([('year', '=', estimated_mirror.mirror_fiscal_year.name)])
        calendar_assigned = self.env[CALENDAR_ASSIGNED_AMOUNTS].search([('id', '=', calendar_assigned_lines.calendar_assigned_amount_id.id)])
        journal_id = self.env[ACCOUNT_JOURNAL].search([('id', '=', calendar_assigned.journal_id.id)])
        account_id = self.env['account.account'].search([('id', '=', journal_id.default_credit_account_id.id)])
        account_id_cri = self.env['relation.account.cri'].search([('cuentas_id', '=', account_id.id)])
        cri_id = self.env['classifier.income.item'].search([('id', '=', account_id_cri.cri.id)])
        for data in estimated_mirror:

        # budget_estimated = self.env['siif.budget.mgmt.income.budgets.lines'].search([('my_id', '=', budget_id.id)])
        # if budget_estimated.ids:
            lines = []
            lines.append((0,0,{
                'my_id': budget_id.id,
                'estimated_fiscal_year': data.mirror_fiscal_year.id,
                'estimated_cri_id': data.mirror_cri_id.id,
                'estimated_january_field': data.mirror_january_field,
                'estimated_february_field': data.mirror_february_field,
                'estimated_march_field': data.mirror_march_field,
                'estimated_april_field': data.mirror_april_field,
                'estimated_may_field': data.mirror_may_field,
                'estimated_june_field': data.mirror_june_field,
                'estimated_july_field': data.mirror_july_field,
                'estimated_august_field': data.mirror_august_field,
                'estimated_september_field': data.mirror_september_field,
                'estimated_october_field': data.mirror_october_field,
                'estimated_november_field': data.mirror_november_field,
                'estimated_december_field': data.mirror_december_field,
                'estimated_total': data.mirror_total,

                'to_collect_january_field': data.mirror_january_field,
                'to_collect_february_field': data.mirror_february_field,
                'to_collect_march_field': data.mirror_march_field,
                'to_collect_april_field': data.mirror_april_field,
                'to_collect_may_field': data.mirror_may_field,
                'to_collect_june_field': data.mirror_june_field,
                'to_collect_july_field': data.mirror_july_field,
                'to_collect_august_field': data.mirror_august_field,
                'to_collect_september_field': data.mirror_september_field,
                'to_collect_october_field': data.mirror_october_field,
                'to_collect_november_field': data.mirror_november_field,
                'to_collect_december_field': data.mirror_december_field,
                'to_collect_total': data.mirror_total,

                'modified_january_field': data.mirror_january_field,
                'modified_february_field': data.mirror_february_field,
                'modified_march_field': data.mirror_march_field,
                'modified_april_field': data.mirror_april_field,
                'modified_may_field': data.mirror_may_field,
                'modified_june_field': data.mirror_june_field,
                'modified_july_field': data.mirror_july_field,
                'modified_august_field': data.mirror_august_field,
                'modified_september_field': data.mirror_september_field,
                'modified_october_field': data.mirror_october_field,
                'modified_november_field': data.mirror_november_field,
                'modified_december_field': data.mirror_december_field,
                'modified_total': data.mirror_total,
            }))
            self.write({'line_budgets': lines})
        
        if calendar_assigned_lines.calendar_assigned_amount_id.state == 'validate':

            sum_january = 0
            sum_february = 0
            sum_march = 0
            sum_april = 0
            sum_may = 0
            sum_june = 0
            sum_july = 0
            sum_august = 0
            sum_september = 0
            sum_october = 0
            sum_november = 0
            sum_december = 0
            sum_total = 0

            lines = []
            for rec in calendar_assigned_lines:
                sum_january += rec.january
                sum_february += rec.february
                sum_march += rec.march
                sum_april += rec.april
                sum_may += rec.may
                sum_june += rec.june
                sum_july += rec.july
                sum_august += rec.august
                sum_september += rec.september
                sum_october += rec.october
                sum_november += rec.november
                sum_december += rec.december
                sum_total += rec.annual_amount
        
            lines_budget = []

            lines_budget.append((0,0,{
                'estimated_fiscal_year': data.mirror_fiscal_year.id,
                # 'estimated_from_date': budget_estimated.estimated_fiscal_year.date_from,
                # 'estimated_to_date': budget_estimated.estimated_fiscal_year.date_to,
                'estimated_cri_id': cri_id.id,
                'estimated_january_field': sum_january,
                'estimated_february_field': sum_february,
                'estimated_march_field': sum_march,
                'estimated_april_field': sum_april,
                'estimated_may_field': sum_may,
                'estimated_june_field': sum_june,
                'estimated_july_field': sum_july,
                'estimated_august_field': sum_august,
                'estimated_september_field': sum_september,
                'estimated_october_field': sum_october,
                'estimated_november_field': sum_november,
                'estimated_december_field': sum_december,
                'estimated_total': sum_total,

                'to_collect_january_field': sum_january,
                'to_collect_february_field': sum_february,
                'to_collect_march_field': sum_march,
                'to_collect_april_field': sum_april,
                'to_collect_may_field': sum_may,
                'to_collect_june_field': sum_june,
                'to_collect_july_field': sum_july,
                'to_collect_august_field': sum_august,
                'to_collect_september_field': sum_september,
                'to_collect_october_field': sum_october,
                'to_collect_november_field': sum_november,
                'to_collect_december_field': sum_december,
                'to_collect_total': sum_total,

                'modified_january_field': sum_january,
                'modified_february_field': sum_february,
                'modified_march_field': sum_march,
                'modified_april_field': sum_april,
                'modified_may_field': sum_may,
                'modified_june_field': sum_june,
                'modified_july_field': sum_july,
                'modified_august_field': sum_august,
                'modified_september_field': sum_september,
                'modified_october_field': sum_october,
                'modified_november_field': sum_november,
                'modified_december_field': sum_december,
                'modified_total': sum_total,
            
            }))

            self.write({'line_budgets': lines_budget})

                       

class IncomeBudgetLines(models.Model):
    _name = NAME_LINES
    _description = 'Income Budgets Lines'

    my_id = fields.Many2one(NAME_MODULE, LINEAS_MODEL, ondelete="cascade")
    display_type = fields.Selection([('line_section', "Section"), ('line_note', "Note")], default=False, help=TECHNICAL_FIELD_FOR_UX_PURPOSE, store=False)
    sequence = fields.Integer(string = 'Sequence', store = False)

    my_id_subsidy = fields.Many2one(CALENDAR_ASSIGNED_AMOUNTS, LINEAS_MODEL)
    display_type = fields.Selection([('line_section', "Section"), ('line_note', "Note")], default=False, help=TECHNICAL_FIELD_FOR_UX_PURPOSE, store=False)
    sequence = fields.Integer(string = 'Sequence', store = False)

    # campos del estimado
    estimated_fiscal_year = fields.Many2one(comodel_name = ACCOUNT_FISCAL_YEAR, string = FISCAL_YEAR, default=0.0)
    estimated_from_date = fields.Date(string=INITIAL_DATE, related='estimated_fiscal_year.date_from', store=True, default=0.0)
    estimated_to_date = fields.Date(string=END_DATE, related='estimated_fiscal_year.date_to', store=True, default=0.0)
    estimated_cri_id = fields.Many2one(comodel_name = CLASSIFIER_INCOME_ITEM, string = "CRI", default=0.0)
    estimated_january_field = fields.Float(string = "Enero", default=0.0)
    estimated_february_field = fields.Float(string = "Febrero", default=0.0)
    estimated_march_field = fields.Float(string = "Marzo", default=0.0)
    estimated_april_field = fields.Float(string = "Abril", default=0.0)
    estimated_may_field = fields.Float(string = "Mayo", default=0.0)
    estimated_june_field = fields.Float(string = "Junio", default=0.0)
    estimated_july_field = fields.Float(string = "Julio", default=0.0)
    estimated_august_field = fields.Float(string = "Agosto", default=0.0)
    estimated_september_field = fields.Float(string = "Septiembre", default=0.0)
    estimated_october_field = fields.Float(string = "Octubre", default=0.0)
    estimated_november_field = fields.Float(string = "Noviembre", default=0.0)
    estimated_december_field = fields.Float(string = "Diciembre", default=0.0)
    estimated_total = fields.Float(string = "Total", default=0.0)

    # campos del por recaudar
    to_collect_january_field = fields.Float(string = "Enero (Por Recaudar)", default=0.0)
    to_collect_february_field = fields.Float(string = "Febrero (Por Recaudar)", default=0.0)
    to_collect_march_field = fields.Float(string = "Marzo (Por Recaudar)", default=0.0)
    to_collect_april_field = fields.Float(string = "Abril (Por Recaudar)", default=0.0)
    to_collect_may_field = fields.Float(string = "Mayo (Por Recaudar)", default=0.0)
    to_collect_june_field = fields.Float(string = "Junio (Por Recaudar)", default=0.0)
    to_collect_july_field = fields.Float(string = "Julio (Por Recaudar)", default=0.0)
    to_collect_august_field = fields.Float(string = "Agosto (Por Recaudar)", default=0.0)
    to_collect_september_field = fields.Float(string = "Septiembre (Por Recaudar)", default=0.0)
    to_collect_october_field = fields.Float(string = "Octubre (Por Recaudar)", default=0.0)
    to_collect_november_field = fields.Float(string = "Noviembre (Por Recaudar)", default=0.0)
    to_collect_december_field = fields.Float(string = "Diciembre (Por Recaudar)", default=0.0)
    to_collect_total = fields.Float(string = "Total (Por Recaudar)", default=0.0)

    # campos del modificado
    modified_january_field = fields.Float(string = "Enero (Modificado)", default=0.0)
    modified_february_field = fields.Float(string = "Febrero (Modificado)", default=0.0)
    modified_march_field = fields.Float(string = "Marzo (Modificado)", default=0.0)
    modified_april_field = fields.Float(string = "Abril (Modificado)", default=0.0)
    modified_may_field = fields.Float(string = "Mayo (Modificado)", default=0.0)
    modified_june_field = fields.Float(string = "Junio (Modificado)", default=0.0)
    modified_july_field = fields.Float(string = "Julio (Modificado)", default=0.0)
    modified_august_field = fields.Float(string = "Agosto (Modificado)", default=0.0)
    modified_september_field = fields.Float(string = "Septiembre (Modificado)", default=0.0)
    modified_october_field = fields.Float(string = "Octubre (Modificado)", default=0.0)
    modified_november_field = fields.Float(string = "Noviembre (Modificado)", default=0.0)
    modified_december_field = fields.Float(string = "Diciembre (Modificado)", default=0.0)
    modified_total = fields.Float(string = "Total (Modificado)", default=0.0)

    # campos de los aumentos
    increases_january_field = fields.Float(string = "Enero (Aumentos)", default=0.0)
    increases_february_field = fields.Float(string = "Febrero (Aumentos)", default=0.0)
    increases_march_field = fields.Float(string = "Marzo (Aumentos)", default=0.0)
    increases_april_field = fields.Float(string = "Abril (Aumentos)", default=0.0)
    increases_may_field = fields.Float(string = "Mayo (Aumentos)", default=0.0)
    increases_june_field = fields.Float(string = "Junio (Aumentos)", default=0.0)
    increases_july_field = fields.Float(string = "Julio (Aumentos)", default=0.0)
    increases_august_field = fields.Float(string = "Agosto (Aumentos)", default=0.0)
    increases_september_field = fields.Float(string = "Septiembre (Aumentos)", default=0.0)
    increases_october_field = fields.Float(string = "Octubre (Aumentos)", default=0.0)
    increases_november_field = fields.Float(string = "Noviembre (Aumentos)", default=0.0)
    increases_december_field = fields.Float(string = "Diciembre (Aumentos)", default=0.0)
    increases_total = fields.Float(string = "Total (Aumentos)", default=0.0)

    # campos de las disminuciones
    decreases_january_field = fields.Float(string = "Enero (Disminuciones)", default=0.0)
    decreases_february_field = fields.Float(string = "Febrero (Disminuciones)", default=0.0)
    decreases_march_field = fields.Float(string = "Marzo (Disminuciones)", default=0.0)
    decreases_april_field = fields.Float(string = "Abril (Disminuciones)", default=0.0)
    decreases_may_field = fields.Float(string = "Mayo (Disminuciones)", default=0.0)
    decreases_june_field = fields.Float(string = "Junio (Disminuciones)", default=0.0)
    decreases_july_field = fields.Float(string = "Julio (Disminuciones)", default=0.0)
    decreases_august_field = fields.Float(string = "Agosto (Disminuciones)", default=0.0)
    decreases_september_field = fields.Float(string = "Septiembre (Disminuciones)", default=0.0)
    decreases_october_field = fields.Float(string = "Octubre (Disminuciones)", default=0.0)
    decreases_november_field = fields.Float(string = "Noviembre (Disminuciones)", default=0.0)
    decreases_december_field = fields.Float(string = "Diciembre (Disminuciones)", default=0.0)
    decreases_total = fields.Float(string = "Total (Disminuciones)", default=0.0)

    # campos del devengado
    accrued_january_field = fields.Float(string = "Enero (Devengado)", default=0.0)
    accrued_february_field = fields.Float(string = "Febrero (Devengado)", default=0.0)
    accrued_march_field = fields.Float(string = "Marzo (Devengado)", default=0.0)
    accrued_april_field = fields.Float(string = "Abril (Devengado)", default=0.0)
    accrued_may_field = fields.Float(string = "Mayo (Devengado)", default=0.0)
    accrued_june_field = fields.Float(string = "Junio (Devengado)", default=0.0)
    accrued_july_field = fields.Float(string = "Julio (Devengado)", default=0.0)
    accrued_august_field = fields.Float(string = "Agosto (Devengado)", default=0.0)
    accrued_september_field = fields.Float(string = "Septiembre (Devengado)", default=0.0)
    accrued_october_field = fields.Float(string = "Octubre (Devengado)", default=0.0)
    accrued_november_field = fields.Float(string = "Noviembre (Devengado)", default=0.0)
    accrued_december_field = fields.Float(string = "Diciembre (Devengado)", default=0.0)
    accrued_total = fields.Float(string = "Total (Devengado)", default=0.0)

    # campos del recaudado
    collected_january_field = fields.Float(string = "Enero (Recaudado)", default=0.0)
    collected_february_field = fields.Float(string = "Febrero (Recaudado)", default=0.0)
    collected_march_field = fields.Float(string = "Marzo (Recaudado)", default=0.0)
    collected_april_field = fields.Float(string = "Abril (Recaudado)", default=0.0)
    collected_may_field = fields.Float(string = "Mayo (Recaudado)", default=0.0)
    collected_june_field = fields.Float(string = "Junio (Recaudado))", default=0.0)
    collected_july_field = fields.Float(string = "Julio (Recaudado)", default=0.0)
    collected_august_field = fields.Float(string = "Agosto (Recaudado)", default=0.0)
    collected_september_field = fields.Float(string = "Septiembre (Recaudado)", default=0.0)
    collected_october_field = fields.Float(string = "Octubre (Recaudado)", default=0.0)
    collected_november_field = fields.Float(string = "Noviembre (Recaudado)", default=0.0)
    collected_december_field = fields.Float(string = "Diciembre (Recaudado)", default=0.0)
    collected_total = fields.Float(string = "Total (Recaudado)", default=0.0)

    state_related = fields.Selection(related='my_id.state', string='State Related', store=True)

    #####################################################
    # function to hide the line belonging to CRI 910000 #
    #####################################################
    @api.model
    def search(self, args, offset=0, limit=None, order=None, count=False):
        if args is None:
            args = []

        # Añade la condición para ocultar las líneas donde estimated_cri_id sea igual a 910000
        args += [('estimated_cri_id.name', '!=', 910000)]

        return super(IncomeBudgetLines, self).search(args, offset, limit, order, count)


class AccountingMovementsLogbook(models.Model):
    _name = 'siif.budget.mgmt.accounting.movements.logbook'
    _description = 'Accounting Movements Logbook' # Bitacora de afectaciones al presupuesto de ingresos 
    _order = "create_date desc"

    fiscal_year = fields.Many2one(comodel_name = ACCOUNT_FISCAL_YEAR, string = FISCAL_YEAR)
    cri_id = fields.Many2one(comodel_name = CLASSIFIER_INCOME_ITEM, string = "CRI")
    proposed_date = fields.Date(string = "Fecha Propuesta")
    movement_type = fields.Selection([('adequacy', 'Adecuación'),
                                      ('rescheduling', 'Recalendarización'),
                                      ('income', 'Ingresos'),
                                      ('adequacy-subsidy', 'Adequacy - Federal Subsidy'),
                                      ('recalendarization-subsidy', 'Recalendarization - Federal Subsidy')
                                     ], string = "Tipo de Movimiento")
    movement = fields.Selection([('increase', 'Aumento'),
                                 ('decrease', 'Disminución'),
                                 ('increase_and_decrease', 'Aumento y Disminución'),
                                 ('for_exercised_accrued', 'eje-dev'),
                                 ('accrued_collected', 'dev-rec'),
                                 ('for_exercising_collected', 'eje-rec'),
                                ], string = "Movimiento")
    source_type = fields.Selection([('adequacy', 'Adecuación'),
                                    ('re-adequacy', 'Re Adecuación'),
                                    ('accounting_seat', 'Asiento Contable'),
                                    ('income', 'Ingresos'),
                                    ('income-sf', 'Ingresos - Subsidio Federal'),
                                    ('income-cd', 'Ingresos - Certificados de depósito'),
                                    ('income-se', 'Ingresos - Servicios de educación'),
                                    ('income-cob', 'Ingresos - Cobros / Facturación'),
                                    ('income-com', 'Ingresos - Comisiones'),
                                    ('income-cpp', 'Ingresos - Cuentas por pagar'),
                                    ('income-sp', 'Ingresos - Solicitudes de pagos'),
                                    ('income-di', 'Ingresos - Distribución de intereses'),
                                    ('income-dev-int', 'Ingresos - Devoluciones por Intereses'),
                                    ('income-dev-exc', 'Ingresos - Devoluciones por Excedentes'),
                                    ('income-dev-int-ant', 'Ingresos - Devoluciones por Intereses Ejercicio Anterior'),
                                    ('income-dev-exc-ant', 'Ingresos - Devoluciones por Excedentes Ejercicio Anterior'),
                                    ('income-nc', 'Ingresos - Notas de Credito'),
                                    ('income_donation', 'Ingresos - Donación'),
                                    ('adequacy-subsidy', 'Adequacy - Federal Subsidy'),
                                    ('recalendarization-subsidy', 'Recalendarization - Subsidy')
                                   ], string = "Tipo de Origen")
    cve = fields.Char(string = "Clave")
    journal_applied = fields.Many2one(comodel_name = "account.journal", string = "Diario Aplicado")
    date_update = fields.Datetime(string = "Fecha Actualización del Registro")
    policy_amount = fields.Float(string = "Monto Póliza")
    budget_update_date = fields.Datetime(string = "Fecha de Actualización al Presupuesto")


class IncomeBudgetLinesMirror(models.Model):
    _name = NAME_MIRROR
    _description = "Income Busgets Lines Mirror"

    my_id = fields.Many2one(NAME_MODULE, LINEAS_MODEL)
    display_type = fields.Selection([('line_section', "Section"), ('line_note', "Note")], default=False, help=TECHNICAL_FIELD_FOR_UX_PURPOSE, store=False)
    sequence = fields.Integer(string = 'Sequence', store = False)

    # # campos del Presupuesto de Ingresos Espejo
    mirror_fiscal_year = fields.Many2one(comodel_name = ACCOUNT_FISCAL_YEAR, string = FISCAL_YEAR)
    mirror_from_date = fields.Date(string=INITIAL_DATE, related='mirror_fiscal_year.date_from', store=True)
    mirror_to_date = fields.Date(string=END_DATE, related='mirror_fiscal_year.date_to', store=True)
    mirror_cri_id = fields.Many2one(comodel_name = CLASSIFIER_INCOME_ITEM, string = "CRI")
    mirror_january_field = fields.Float(string = "Enero")
    mirror_february_field = fields.Float(string = "Febrero")
    mirror_march_field = fields.Float(string = "Marzo")
    mirror_april_field = fields.Float(string = "Abril")
    mirror_may_field = fields.Float(string = "Mayo")
    mirror_june_field = fields.Float(string = "Junio")
    mirror_july_field = fields.Float(string = "Julio")
    mirror_august_field = fields.Float(string = "Agosto")
    mirror_september_field = fields.Float(string = "Septiembre")
    mirror_october_field = fields.Float(string = "Octubre")
    mirror_november_field = fields.Float(string = "Noviembre")
    mirror_december_field = fields.Float(string = "Diciembre")
    mirror_total = fields.Float(string = "Total")