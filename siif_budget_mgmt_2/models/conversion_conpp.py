# -*- coding: utf-8 -*-
from datetime import datetime
import logging
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from lxml import etree

class ConversionCONPP(models.Model):

    _name = 'conversion.conpp'

    conpp = fields.Many2one("shcp.code", string='Conversion of CONPP')
    conpp_name = fields.Text(related='conpp.desc')

    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')

    selector_pr = fields.Selection(
        [
            ('all', 'All'),
            ('except', 'Except'),
            ('only', 'Only')
        ],
        default='all', required=True, string='Selector Programs'
    )
    program_ids = fields.Many2many('program', 'conversion_conpp_program_rel', string='Programs')

    selector_sp = fields.Selection(
        [
            ('all', 'All'),
            ('except', 'Except'),
            ('only', 'Only')
        ],
        default='all', required=True, string='Selector Sub Programs'
    )
    subprogram_ids = fields.Many2many('base.sub.program', 'conversion_conpp_sub_program_rel', string='Sub Programs')

    selector_item = fields.Selection(
        [
            ('all', 'All'),
            ('except', 'Except'),
            ('only', 'Only')
        ],
        default='all', required=True, string='Selector Items'
    )
    item_ids = fields.Many2many('expenditure.item', string='Items')

    selector_dependency = fields.Selection(
        [
            ('all', 'All'),
            ('except', 'Except'),
            ('only', 'Only')
        ],
        default='all', required=True, string='Selector Dependencys'
    )
    dependency_ids = fields.Many2many('dependency', 'conversion_conpp_dependency_rel', string='Dependency')

    selector_subdependency = fields.Selection(
        [
            ('all', 'All'),
            ('except', 'Except'),
            ('only', 'Only')
        ],
        default='all', required=True, string='Selector Sub Dependencys'
    )
    subdependency_ids = fields.Many2many('base.sub.dependency', 'conversion_conpp_sub_dependency_rel', string='Sub Dependency')

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super().fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])
   
        is_group_budget_approval_user = self.env.user.has_group('jt_budget_mgmt.group_budget_approval_user')
        is_group_budget_guest_user = self.env.user.has_group('jt_budget_mgmt.group_budget_guest_user')
        is_group_budget_confirmation_user = self.env.user.has_group('jt_budget_mgmt.group_budget_confirmation_user')
        is_group_budget_register_user = self.env.user.has_group('jt_budget_mgmt.group_budget_register_user')
        
        if (is_group_budget_approval_user or is_group_budget_guest_user or is_group_budget_confirmation_user or is_group_budget_register_user): 
            
            
            for node in doc.xpath("//" + view_type):
                node.set('export', '0')
                node.set('create', '0') 
                node.set('edit', '0')
                node.set('delete', '0')

            for node in doc.xpath("//field"):
                node.set('options','{"no_open":true}')
                    
                
        res['arch'] = etree.tostring(doc) 
        return res

    @api.constrains('start_date', 'end_date')
    def check_end_date(self):
        if not self.start_date:
            raise ValidationError(_("The start date of validity is required."))
        if self.end_date and self.end_date <= self.start_date:
            raise ValidationError(_("The effective end date cannot be less than or equal to the effective date."))

    @api.model
    def create(self, vals):
        domain = []
        if vals.get('conpp',None):
            domain.append(('conpp', '=', vals.get('conpp',None)))
        if vals.get('start_date',None):
            domain.append(('start_date', '=', vals.get('start_date',None)))
        if vals.get('end_date',None):
            domain.append(('end_date', '=', vals.get('end_date',None)))
        if vals.get('selector_pr',None):
            domain.append(('selector_pr', '=', vals.get('selector_pr',None)))
        program_ids = vals.get('program_ids',None)
        if program_ids and program_ids[0][2]:
            domain.append(('program_ids', 'in', program_ids[0][2]))
        if vals.get('selector_sp',None):
            domain.append(('selector_sp', '=', vals.get('selector_sp',None)))
        subprogram_ids = vals.get('subprogram_ids',None)
        if subprogram_ids and subprogram_ids[0][2]:
            domain.append(('subprogram_ids', 'in', subprogram_ids[0][2]))
        if vals.get('selector_item',None):
            domain.append(('selector_item', '=', vals.get('selector_item',None)))
        item_ids = vals.get('item_ids',None)
        if item_ids and item_ids[0][2]:
            domain.append(('item_ids', 'in', item_ids[0][2]))
        if vals.get('selector_dependency',None):
            domain.append(('selector_dependency', '=', vals.get('selector_dependency',None)))
        dependency_ids = vals.get('dependency_ids',None)
        if dependency_ids and dependency_ids[0][2]:
            domain.append(('dependency_ids', 'in', dependency_ids[0][2]))
        if vals.get('selector_subdependency',None):
            domain.append(('selector_subdependency', '=', vals.get('selector_subdependency',None)))
        subdependency_ids = vals.get('subdependency_ids',None)
        if subdependency_ids and subdependency_ids[0][2]:
            domain.append(('subdependency_ids', 'in', subdependency_ids[0][2]))
        if  self.env['conversion.conpp'].search(domain):
            raise ValidationError("La Regla del CONPP ya existe.")
        return super().create(vals)

    @api.onchange("selector_pr")
    def _onchange_selector_pr(self):
        if self.selector_pr == 'all':
            self.write({'program_ids': [(5,)]})

    @api.onchange("selector_sp")
    def _onchange_selector_sp(self):
        if self.selector_sp == 'all':
            self.write({'subprogram_ids': [(5,)]})

    @api.onchange("selector_item")
    def _onchange_selector_item(self):
        if self.selector_item == 'all':
            self.write({'item_ids': [(5,)]})

    @api.onchange("selector_dependency")
    def _onchange_selector_dependency(self):
        if self.selector_dependency == 'all':
            self.write({'dependency_ids': [(5,)]})

    @api.onchange("selector_subdependency")
    def _onchange_selector_subdependency(self):
        if self.selector_subdependency == 'all':
            self.write({'subdependency_ids': [(5,)]})

    def name_get(self):
        return [(rec.id, rec.conpp.name) for rec in self]

    @api.model
    def get_conpp(self, program, subprogram, dependency, subdependency, item, date=datetime.now()):
        query = f"""
        select s.id, s.name conpp from conversion_conpp c, shcp_code s
        where conpp = s.id and (selector_pr = 'all' or (
            selector_pr = 'only' and exists (
            select 1 from conversion_conpp_program_rel r, program p where conversion_conpp_id = c.id and program_id = p.id and p.key_unam = '{program}'
            )) or (selector_pr = 'except' and not exists (
                select 1 from conversion_conpp_program_rel r, program p where conversion_conpp_id = c.id and program_id = p.id and p.key_unam = '{program}'
            ))
        ) and (selector_sp = 'all' or (
                selector_sp = 'only' and exists (
                select 1 from conversion_conpp_sub_program_rel r, base_sub_program p where conversion_conpp_id = c.id and base_sub_program_id = p.id and p.sub_program = '{subprogram}'
            )) or (selector_sp = 'except' and not exists (
                select 1 from conversion_conpp_sub_program_rel r, base_sub_program p where conversion_conpp_id = c.id and base_sub_program_id = p.id and p.sub_program = '{subprogram}'
            ))
        ) and (selector_item = 'all' or (
                selector_item = 'only' and exists (
                select 1 from conversion_conpp_expenditure_item_rel r, expenditure_item i where conversion_conpp_id = c.id and expenditure_item_id = i.id and i.item = '{item}'
            )) or (selector_item = 'except' and not exists (
                select 1 from conversion_conpp_expenditure_item_rel r, expenditure_item i where conversion_conpp_id = c.id and expenditure_item_id = i.id and i.item = '{item}'
            ))
        ) and (selector_dependency = 'all' or (
                selector_dependency = 'only' and exists (
                select 1 from conversion_conpp_dependency_rel r, dependency d where conversion_conpp_id = c.id and dependency_id = d.id and d.dependency = '{dependency}'
            )) or (selector_dependency = 'except' and not exists (
                select 1 from conversion_conpp_dependency_rel r, dependency d where conversion_conpp_id = c.id and dependency_id = d.id and d.dependency = '{dependency}'
            ))
        ) and (selector_subdependency = 'all' or (
                selector_subdependency = 'only' and exists (
                select 1 from conversion_conpp_sub_dependency_rel r, base_sub_dependency s where conversion_conpp_id = c.id and base_sub_dependency_id = s.id and s.sub_dependency = '{subdependency}'
            )) or (selector_subdependency = 'except' and not exists (
                select 1 from conversion_conpp_sub_dependency_rel r, base_sub_dependency s where conversion_conpp_id = c.id and base_sub_dependency_id = s.id and s.sub_dependency = '{subdependency}'
            ))
        ) and c.start_date <= '{date}' and (c.end_date is null or c.end_date >= '{date}')
        """
        self._cr.execute(query, ())
        res = self.env.cr.fetchall()
        return res[0] if len(res) == 1 else (None, None)