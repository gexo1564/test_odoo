# -*- coding: utf-8 -*-
{
    'name': 'UNAM: Aggregate Reports by Departments',
    'summary': '''
      
    ''',
    'description': '''
        MPV - TASK ID: 2694490
    ''',
    'license': 'OPL-1',
    'author': 'Odoo Inc',
    'website': 'https://www.odoo.com',
    'category': 'Development Services/Custom Development',
    'version': '1.0',
    'depends': [
        'account_reports',
        'jt_account_module_design',
        'jt_budget_mgmt',
        'siif_account_design'
    ],
    'data': [
        'data/assets.xml',
        'views/general_accounting.xml',
        'views/report_financial.xml',
        'views/trial_balance.xml'
    ],
    'installable': True,
    'application': False,
    'auto_install': False
}