# -*- coding: utf-8 -*-
import logging
import unittest
from odoo.exceptions import UserError, ValidationError
from datetime import datetime
from odoo.tests import common, tagged
_logger = logging.getLogger(__name__)

@tagged('-at_install','post_install','income_adequacies','siif_budget_mgmt')
class IncomeAdequaciesTest(common.TransactionCase):
	
    # ! Prepara entorno de pruebas
    def setUp(self):
        super(IncomeAdequaciesTest,self).setUp()
        self.model = self.env['income.adequacies']
        self.budget_affectation = self.env['affectation.income.budget']
        self.budget_id = self.env['siif.budget.mgmt.income.budgets']
        self.journal_id = self.env['account.journal']
        self.fiscal_year = self.env['account.fiscal.year']

    # ! Test año fiscal no existente
    def test_fiscal_year_not_exist(self):
        id_year = "0"
        fiscal_year = self.fiscal_year.search([('id', '=', id_year)])
        
        # ? Verifica que fiscal_year esté vacío, lo que indica que no se encontró el año fiscal
        self.assertEqual(len(fiscal_year), 0)

        # ? Intenta obtener el año fiscal predeterminado en el modelo
        res = self.model.default_get([])

        # ? Verifica que el campo fiscal_year en res no coincida con el año fiscal no existente
        self.assertNotEqual(res.get('fiscal_year'), str(fiscal_year))

        print('------------------------ AÑO FISCAL NO EXISTE ----------------------------- OK')

    # ! Test diario no existente
    def test_journal_id_not_exist(self):
        id_journal = "0"
        journal_id = self.journal_id.search([('id', '=', id_journal)])

        # ? Verifica que journal_id esté vacío, lo que indica que no se encontró el diario
        self.assertEqual(len(journal_id), 0)

        # ? Intenta obtener el año fiscal predeterminado en el modelo
        res = self.model.default_get([])

        # ? Verifica que el campo journal_id en res no coincida con el diario no existente
        self.assertNotEqual(res.get('journal_id'), str(journal_id))

        print('------------------------ DIARIO NO EXISTE ----------------------------- OK')

    # ! Test presupuesto de ingresos no existente
    def test_budget_id_not_exist(self):
        id_budget = "0"
        budget_id = self.budget_id.search([('id', '=', id_budget)])

         # ? Verifica que budget_id esté vacío, lo que indica que no se encontró el presupuesto
        self.assertEqual(len(budget_id), 0 )

        # ? Intenta obtener el año fiscal predeterminado en el modelo
        res = self.model.default_get([])

        self.assertNotEqual(res.get('budget_id'), str(budget_id))
        print('------------------------ PRESUPUESTO NO EXISTE ----------------------------- OK')

    # ! Test funcion de afetaación al presupuesto
    def test_affectation_budget_function(self):
        income_adequacy_id = False
        income_budget_id = False
        type_adequacy = False
        origin_type = False

         # Llamar a la función a probar y verificar si se lanza una excepción
        with self.assertRaises(Exception):
            self.env['affectation.income.budget'].affectation_income_budget(income_adequacy_id, income_budget_id, type_adequacy, origin_type)
            print('------------------------ NO SE REALIZO LA AFECTACION AL PRESUPUESTO ----------------------------- OK')
