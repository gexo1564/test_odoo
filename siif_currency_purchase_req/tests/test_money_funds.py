import logging
from odoo.tests import common
from odoo.exceptions import UserError, ValidationError
from odoo.tests import common
from odoo.exceptions import ValidationError
import base64

MONEY_FUND='money.funds'

USER_AUX='Usuario 1'
EMAIL_AUX='a@mail.com'
EMAIL_DEP='dep@mail.com'
class TestMoneyFunds(common.TransactionCase):
    def setUp(self):
        super().setUp()
        self.account_account = self.env['account.account'].search([('code','=','110.002.001')])
        self.journal = self.env['account.journal'].search([('name','=','BBVA BANCOMER-8141 (BBV01)')])
        self.currency           = self.env['res.currency'].search([('name','=','MXN')])
        self.user               = self.env['res.partner'].search([('password_beneficiary','=','N1420924')])
        self.dep                = self.env['dependency'].create({
            'dependency':'999',
            'description':"Dependencia Pruebas",
            'sort_description':"Test",
        })
        self.sub_dep            = self.env['sub.dependency'].create({
            'dependency_id':self.dep.id,
            'sub_dependency':'01',
            'description':'Subdependencia Prueba'
        })
        self.name="Usuario"
        self.emails = "a@mail.com"
    
    def test_create_titulars(self):
        #Revisar si tiene correo valido
        vals={
            'name':USER_AUX,
            'email':'amailcom',
        }
        with self.assertRaises(UserError):
            self.env['titulars'].create(vals)
        #Tiene los elementos
        vals={
            'name':USER_AUX,
            'email':"a@mail.com",
        }
        self.env['titulars'].create(vals)



    def test_create_money_fund(self):
        file_aux = open('siif_currency_purchase_req/static/tests/file_pdf.pdf',"rb")
        out_aux = file_aux.read()
        file_aux.close()
        files = base64.b64encode(out_aux)
        #Titulares
        vals={
            'name':"Usuario 1",
            'email':EMAIL_AUX,
        }
        titular=self.env['titulars'].create(vals)


        #Revisar si tiene dependencia y subdependencia
        vals={
            'is_new_fund':True,
            'debtor_account': self.account_account.id,
            'currency_id': self.currency.id,
            'new_fund':10000,
            'name_proxy':self.name,
            'email_proxy':self.emails,
            'name_owner':self.name,
            'email_owner':self.emails,
            'name_secretary':self.name,
            'email_secretary':self.emails,
            'name_third':self.name,
            'email_third':self.emails,

            'ine_doc_proxy':files,
            'address_doc_proxy':files,
            'nombramiento_doc_proxy':files,
            'ine_doc_owner':files,
            'address_doc_owner':files,
            'nombramiento_doc_owner':files,
            'ine_doc_secretary':files,
            'address_doc_secretary':files,
            'nombramiento_doc_secretary':files,
            'ine_doc_third':files,
            'address_doc_third':files,
            'titulars_id':titular,

            'dependency_mail':EMAIL_DEP,
            'dependency_address':'CU',
            
        }
        with self.assertRaises(KeyError):
            info=self.env[MONEY_FUND].create(vals)
            info.unlink()
        #Checar si tiene monto mayor a 0
        vals={
            'dependency_id':self.dep.id,
            'sub_dependency_id':self.sub_dep.id,
            'is_new_fund':True,
            'debtor_account': self.account_account.id,
            'currency_id': self.currency.id,
            'new_fund':-5000,
            'name_proxy':self.name,
            'email_proxy':self.emails,
            'name_owner':self.name,
            'email_owner':self.emails,
            'name_secretary':self.name,
            'email_secretary':self.emails,
            'name_third':self.name,
            'email_third':self.emails,

            'ine_doc_proxy':files,
            'address_doc_proxy':files,
            'nombramiento_doc_proxy':files,
            'ine_doc_owner':files,
            'address_doc_owner':files,
            'nombramiento_doc_owner':files,
            'ine_doc_secretary':files,
            'address_doc_secretary':files,
            'nombramiento_doc_secretary':files,
            'ine_doc_third':files,
            'address_doc_third':files,
            'titulars_id':titular,

            'dependency_mail':EMAIL_DEP,
            'dependency_address':'CU',
            
        }
        with self.assertRaises(ValidationError):
            info=self.env[MONEY_FUND].create(vals)
            info.unlink()
            

        #Checar si el correo esta mal
        vals={
            'dependency_id':self.dep.id,
            'sub_dependency_id':self.sub_dep.id,
            'is_new_fund':True,
            'debtor_account': self.account_account.id,
            'currency_id': self.currency.id,
            'new_fund': 10000,
            'name_proxy':self.name,
            'email_proxy':self.emails,
            'name_owner':self.name,
            'email_owner':self.emails,
            'name_secretary':self.name,
            'email_secretary':self.emails,
            'name_third':self.name,
            'email_third':self.emails,

            'ine_doc_proxy':files,
            'address_doc_proxy':files,
            'nombramiento_doc_proxy':files,
            'ine_doc_owner':files,
            'address_doc_owner':files,
            'nombramiento_doc_owner':files,
            'ine_doc_secretary':files,
            'address_doc_secretary':files,
            'nombramiento_doc_secretary':files,
            'ine_doc_third':files,
            'address_doc_third':files,
            'titulars_id':titular,

            'dependency_mail':'depmail.com',
            'dependency_address':'CU',
            
        }
        with self.assertRaises(UserError):
            info=self.env[MONEY_FUND].create(vals)
            info.unlink()

        
        #Realizar solicitud
        vals={
            'dependency_id':self.dep.id,
            'sub_dependency_id':self.sub_dep.id,
            'is_new_fund':True,
            'debtor_account': self.account_account.id,
            'currency_id': self.currency.id,
            'new_fund': 10000,
            'name_proxy':self.name,
            'email_proxy':self.emails,
            'name_owner':self.name,
            'email_owner':self.emails,
            'name_secretary':self.name,
            'email_secretary':self.emails,
            'name_third':self.name,
            'email_third':self.emails,

            'ine_doc_proxy':files,
            'address_doc_proxy':files,
            'nombramiento_doc_proxy':files,
            'ine_doc_owner':files,
            'address_doc_owner':files,
            'nombramiento_doc_owner':files,
            'ine_doc_secretary':files,
            'address_doc_secretary':files,
            'nombramiento_doc_secretary':files,
            'ine_doc_third':files,
            'address_doc_third':files,

            'dependency_mail':EMAIL_DEP,
            'dependency_address':'CU',
            'titulars_id':titular,
            'no_titulars':len(titular),
            
        }
        
        money_fund=self.env[MONEY_FUND].create(vals)
        #Se revisa si se asigna el nombre con la moneda asignada
        self.assertEqual(money_fund.name,'Fondo Fijo-999-01-3')

        #Se revisa si se crea una solicitud de compra
        request = self.env['money.funds.request.increment'].search([('dependency_id','=',self.dep.id),('sub_dependency_id','=',self.sub_dep.id)])
        logging.critical(request)
        self.assertEqual(request.actual_fund,0)
        self.assertEqual(request.amount_request,10000)
        self.assertEqual(request.move_type,'adding')

    def test_money_fund_dup(self):
        file_aux = open('siif_currency_purchase_req/static/tests/file_pdf.pdf',"rb")
        out_file = file_aux.read()
        file_aux.close()
        files = base64.b64encode(out_file)
        #Titulares
        vals={
            'name':"Usuario 1",
            'email':EMAIL_AUX,
        }
        titular=self.env['titulars'].create(vals)
        
        #Realizar solicitud
        vals={
            'dependency_id':self.dep.id,
            'sub_dependency_id':self.sub_dep.id,
            'is_new_fund':True,
            'debtor_account': self.account_account.id,
            'currency_id': self.currency.id,
            'new_fund': 10000,
            'name_proxy':self.name,
            'email_proxy':self.emails,
            'name_owner':self.name,
            'email_owner':self.emails,
            'name_secretary':self.name,
            'email_secretary':self.emails,
            'name_third':self.name,
            'email_third':self.emails,

            'ine_doc_proxy':files,
            'address_doc_proxy':files,
            'nombramiento_doc_proxy':files,
            'ine_doc_owner':files,
            'address_doc_owner':files,
            'nombramiento_doc_owner':files,
            'ine_doc_secretary':files,
            'address_doc_secretary':files,
            'nombramiento_doc_secretary':files,
            'ine_doc_third':files,
            'address_doc_third':files,

            'dependency_mail':EMAIL_DEP,
            'dependency_address':'CU',
            'titulars_id':titular,
            'no_titulars':len(titular),
            'state':'on_use'
            
        }
        
        self.env[MONEY_FUND].create(vals)
        
        vals_01={
            'dependency_id':self.dep.id,
            'sub_dependency_id':self.sub_dep.id,
            'is_new_fund':True,
            'debtor_account': self.account_account.id,
            'currency_id': self.currency.id,
            'new_fund': 40000,
            'name_proxy':self.name,
            'email_proxy':self.emails,
            'name_owner':self.name,
            'email_owner':self.emails,
            'name_secretary':self.name,
            'email_secretary':self.emails,
            'name_third':self.name,
            'email_third':self.emails,

            'ine_doc_proxy':files,
            'address_doc_proxy':files,
            'nombramiento_doc_proxy':files,
            'ine_doc_owner':files,
            'address_doc_owner':files,
            'nombramiento_doc_owner':files,
            'ine_doc_secretary':files,
            'address_doc_secretary':files,
            'nombramiento_doc_secretary':files,
            'ine_doc_third':files,
            'address_doc_third':files,

            'dependency_mail':EMAIL_DEP,
            'dependency_address':'CU',
            'titulars_id':titular,
            'no_titulars':len(titular),
            'state':'on_use'
            
        }
        with self.assertRaises(UserError): 
            self.env[MONEY_FUND].create(vals_01)
