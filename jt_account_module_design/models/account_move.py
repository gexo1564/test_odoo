from odoo import models, fields, api,_
from odoo.exceptions import UserError, ValidationError


# CONSTANTS FOR MODELS
ACCOUNT_MOVE_MANUAL_PERMISSION_MODEL = 'account.move.manual.permission'

class AccountMove(models.Model):

    # This is the model that will be inherited by the account.move model
    # The purpose of this model is to add a new field to the account.move model to indicate if the account.move
    # is a manual accounting record. 

    _inherit = 'account.move'

    allowed_journal_to_manual_record = fields.Many2one(
        'account.journal',
        string='Allowed Journals to Manual Record',
        domain=lambda self: self._get_journals_allowed_to_actual_user()
    )

    allowed_dependency_to_manual_record = fields.Many2one(
        'dependency',
        string='Allowed Dependencies to Manual Record',
        domain=lambda self: self._get_dependencies_allowed_to_actual_user()
    )

    allowed_subdependency_to_manual_record = fields.Many2one(
        'sub.dependency',
        string='Allowed Subdependencies to Manual Record',
        domain=lambda self: self._get_subdependencies_allowed_to_actual_user()
    )


    def _get_journals_allowed_to_actual_user(self):
        user = self.env.user

        user_permission = self.env[ACCOUNT_MOVE_MANUAL_PERMISSION_MODEL].search([('res_users_id','=',user.id)])
        journals_to_actual_user = user_permission.allowed_journals

        return [('id', 'in', journals_to_actual_user.ids)]

    def _get_dependencies_allowed_to_actual_user(self):
        user = self.env.user

        user_permission = self.env[ACCOUNT_MOVE_MANUAL_PERMISSION_MODEL].search([('res_users_id','=',user.id)])
        dependencies_to_actual_user = user_permission.allowed_dependencies
        
        return [('id', 'in', dependencies_to_actual_user.ids)]
    
    def _get_subdependencies_allowed_to_actual_user(self):
        user = self.env.user

        user_permission = self.env[ACCOUNT_MOVE_MANUAL_PERMISSION_MODEL].search([('res_users_id','=',user.id)])
        subdependencies_to_actual_user = user_permission.allowed_subdependencies
        
        return [('id', 'in', subdependencies_to_actual_user.ids)]

    @api.onchange('allowed_dependency_to_manual_record')
    def _onchange_allowed_dependency_to_manual_record(self):
        self.dependancy_id = self.allowed_dependency_to_manual_record
        self.allowed_subdependency_to_manual_record = False
        values = self._update_allowed_subdependency_domain()
        return values

    def _update_allowed_subdependency_domain(self):
        user = self.env.user
        user_permission = self.env[ACCOUNT_MOVE_MANUAL_PERMISSION_MODEL].search([('res_users_id','=',user.id)])
        subdependencies_to_actual_user = user_permission.allowed_subdependencies
        
        sub_dep_list = []
        values = {}
        for rec in subdependencies_to_actual_user:
            if rec.dependency_id.id == self.allowed_dependency_to_manual_record.id:
                sub_dep_list.append(rec.id)
        values.update({'domain': {'allowed_subdependency_to_manual_record': [('id', 'in', sub_dep_list)]}})
        return values
    
    @api.onchange('allowed_journal_to_manual_record')
    def _onchange_allowed_journal_to_manual_record(self):
        self.journal_id = self.allowed_journal_to_manual_record

    @api.onchange('allowed_subdependency_to_manual_record')
    def _onchange_allowed_subdependency(self):
        self.sub_dependancy_id = self.allowed_subdependency_to_manual_record
        values = self._update_allowed_subdependency_domain()

        if self.allowed_dependency_to_manual_record.id != self.allowed_subdependency_to_manual_record.dependency_id.id:
                self.allowed_subdependency_to_manual_record = False
                
        return values
    
    def _delete_mail_tracking_value_by_field(self):
        

        # list of fields to delete from mail_tracking_value table 
        list_of_fields = [
            'allowed_journal_to_manual_record',
            'allowed_dependency_to_manual_record',
            'allowed_subdependency_to_manual_record',
            'domain_sub_dependency',
            ]
        
        for list_element in list_of_fields:

            query = """
                DELETE FROM mail_tracking_value WHERE field = %(list_element)s
            """
            
            params = {
                'list_element': list_element
            }
                
            self.env.cr.execute(query, params)
        