# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

from odoo.addons.siif_security.tools.utils import check_authorization, check_only_allowed_fields

class ConversionGeographicLocation(models.Model):

    _inherit = 'sub.program'

    geographic_location_id = fields.Many2one('geographic.location','Geographic Location')

    geographic_location_name = fields.Text(related='geographic_location_id.state_name')


    @check_only_allowed_fields('desc','geographic_location_id', 'start_date', 'end_date')
    def write(self, vals):
        return super(ConversionGeographicLocation, self).write(vals)