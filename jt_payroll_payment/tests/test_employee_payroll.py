from odoo.tests import TransactionCase
from odoo.tests import tagged
from unittest.mock import patch, MagicMock

@tagged('jt_payroll_payement')
class IncomeAnnualReport(TransactionCase):
    def test_action_create_portal_users(self):
        # Intento de inyección el cual no se realiza porque esta parametrizado  
        query = "update hr_employee set user_id = %s, emp_partner_id = %s where id = %s"
        
        exist_id = 1000
        exist_user_partner = 1001
        emp_id = 8707
        params = (exist_id, exist_user_partner, emp_id,)

        # Simular la ejecución de la consulta
        with patch.object(self.env.cr, 'execute') as mock_execute:           
            # Ejecutar la prueba
            self.env.cr.execute(query, params)
            # Asegurar que la consulta se haya ejecutado al menos una vez
            self.assertTrue(mock_execute.called, "La consulta query no se ejecutó.")
            print(F"PRUEBA DE QUERY SANITIZADO 1 test__action_create_portal_users\n{query}")
