# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.tools import float_compare, float_is_zero
from odoo.exceptions import UserError

class AccountBankStatementLine(models.Model):
    
    _inherit = "account.bank.statement.line"

    customer_ref = fields.Char('Client reference')
    reference_plugin = fields.Char('Plugin reference')
    description_of_the_movement = fields.Char('Description of movement')
    movement_date = fields.Date('Movement date')
    settlement_date = fields.Date('Settlement date')
       
