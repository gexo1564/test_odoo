from odoo import models, fields,api,_
from odoo.exceptions import ValidationError
from lxml import etree

class ActivityCatalog(models.Model):

    _name = 'activity.catalog'
    _description = "Activity Catalog"
    _rec_name = 'activity_id'

    activity_id = fields.Char("ID",required=True)
    description = fields.Text("Description")
    
    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        res = super(ActivityCatalog, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])
        
        is_group_income_fac_administrator_user = self.env.user.has_group('jt_income.group_income_fac_administrator_user')

        if not is_group_income_fac_administrator_user:
            for node in doc.xpath("//" + view_type):
                node.set('export', '0')
                node.set('duplicate', '0')
                node.set('delete', '0')

        res['arch'] = etree.tostring(doc)
        return res

    @api.constrains('activity_id')
    def _check_activity_id(self):
        if self.activity_id == ' ':
            raise ValidationError(_('Please add the Activity ID'))

    @api.constrains('activity_id')
    def _check_code(self):
        for rec in self:
            if rec.activity_id:
                activity_id = self.env['activity.catalog'].search([('activity_id','=',rec.activity_id),('id','!=',rec.id)],limit=1)
                if activity_id:
                    raise ValidationError(_("There is already a record with the same Activity ID"))
        if self.activity_id == None or self.activity_id == '' or not self.activity_id:
            raise ValidationError(_('Please add the Activity ID'))

    def unlink(self):
        for this_act in self:
            is_exist_asign_act = self.env['account.move'].search([('activity_key_id','=',this_act.id)],limit=1)
            if is_exist_asign_act:
                raise ValidationError(_('You cannot delete an Activity when it is already being used in the Billing records'))
        return super(ActivityCatalog,self).unlink()
            
    def name_get(self):
        result = []
        for rec in self:
            name = rec.activity_id or ''
            if rec.description: 
                name += ' ' + rec.description
            result.append((rec.id, name))
        return result