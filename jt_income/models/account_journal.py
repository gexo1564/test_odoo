from odoo import models, fields, api, _

#Modelo
ACCOUNT_ACCOUNT = 'account.account'
DEF_CREDIT_ACCOUNT = "Default Credit Account"
DEF_DEBIT_ACCOUNT = "Default Debit Account"
CONAC_CREDIT_ACCOUNT = "CONAC Credit Account"
CONAC_DEBIT_ACCOUNT = "CONAC Debit Account"
class AccountJournal(models.Model):
    
    _inherit = "account.journal"

    estimate_income_credit_account_id = fields.Many2one(ACCOUNT_ACCOUNT, DEF_CREDIT_ACCOUNT)
    conac_estimate_income_credit_account_id = fields.Many2one(related='estimate_income_credit_account_id.coa_conac_id', string=CONAC_CREDIT_ACCOUNT)
    estimate_income_debit_account_id = fields.Many2one(ACCOUNT_ACCOUNT, DEF_DEBIT_ACCOUNT)
    conac_estimate_income_debit_account_id = fields.Many2one(related='estimate_income_debit_account_id.coa_conac_id', string=CONAC_DEBIT_ACCOUNT)

    income_run_credit_account_id = fields.Many2one(ACCOUNT_ACCOUNT, DEF_CREDIT_ACCOUNT)
    conac_income_run_credit_account_id = fields.Many2one(related='income_run_credit_account_id.coa_conac_id', string=CONAC_CREDIT_ACCOUNT)
    income_run_debit_account_id = fields.Many2one(ACCOUNT_ACCOUNT, DEF_DEBIT_ACCOUNT)
    conac_income_run_debit_account_id = fields.Many2one(related='income_run_debit_account_id.coa_conac_id', string=CONAC_DEBIT_ACCOUNT)

    accrued_income_credit_account_id = fields.Many2one(ACCOUNT_ACCOUNT, DEF_CREDIT_ACCOUNT)
    conac_accrued_income_credit_account_id = fields.Many2one(related='accrued_income_credit_account_id.coa_conac_id', string=CONAC_CREDIT_ACCOUNT)
    accrued_income_debit_account_id = fields.Many2one(ACCOUNT_ACCOUNT, DEF_DEBIT_ACCOUNT)
    conac_accrued_income_debit_account_id = fields.Many2one(related='accrued_income_debit_account_id.coa_conac_id', string=CONAC_DEBIT_ACCOUNT)

    recover_income_credit_account_id = fields.Many2one(ACCOUNT_ACCOUNT, DEF_CREDIT_ACCOUNT)
    conac_recover_income_credit_account_id = fields.Many2one(related='recover_income_credit_account_id.coa_conac_id', string=CONAC_CREDIT_ACCOUNT)
    recover_income_debit_account_id = fields.Many2one(ACCOUNT_ACCOUNT, DEF_DEBIT_ACCOUNT)
    conac_recover_income_debit_account_id = fields.Many2one(related='recover_income_debit_account_id.coa_conac_id', string=CONAC_DEBIT_ACCOUNT)

    for_income = fields.Boolean(related="bank_account_id.for_income",string="Income")
    