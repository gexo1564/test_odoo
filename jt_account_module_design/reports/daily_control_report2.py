# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import copy
import ast
from lxml import etree
from lxml.objectify import fromstring
from odoo import models, fields, api, _
from odoo.tools.safe_eval import safe_eval
from odoo.tools.misc import formatLang, format_date
from odoo.tools import float_is_zero, ustr
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError, ValidationError
from odoo.osv import expression
from odoo.tools.misc import xlsxwriter
from odoo.tools import config, date_utils, get_lang
import io
import base64
from datetime import datetime
from datetime import timedelta
import lxml.html
import logging
from psycopg2 import sql

class DailyControlReport(models.Model):
    _name = "jt_account_module_design.daily.control.report.two"
    _description = "Daily Control Report"
    _inherit = "account.coa.report"

    filter_date = {'mode': 'range', 'filter': 'this_month'}
    filter_comparison = None
    filter_all_entries = None
    filter_journals = None
    filter_analytic = None
    filter_unfold_all = None
    filter_cash_basis = None
    filter_hierarchy = None
    filter_unposted_in_period = None


    filter_date_sel = True
    filter_bank = True
    filter_request_open_balance_finance_folio = True
    filter_account_bank = True
    filter_transfer_request =True
    MAX_LINES = None

    #Filtro fechas selección
    @api.model
    def _get_filter_date_sel(self):
        data = [('0','date_required',_('Required Date')),('1','create_date',_('Created Date')),('2','date_payment',_('Payment Date'))]
        return data
    @api.model
    def _init_filter_date_sel(self, options, previous_options=None):
        if self.filter_date_sel is None:
            return
        if previous_options and previous_options.get('date_sel'):
            journal_map = dict((opt['id'], opt['selected']) for opt in previous_options['date_sel'] if opt['id'] != 'divider' and 'selected' in opt)
        else:
            journal_map = {}
        options['date_sel'] = []

        default_group_ids = []
        
        for j in self._get_filter_date_sel():
            options['date_sel'].append({
                'id': j[0],
                'name': str(j[2]),
                'selected': journal_map.get(j[0], j[1] in default_group_ids),
            })

    #Filtro para el banco
    @api.model
    def _get_filter_bank(self):
        cr = self._cr
        cr.execute("""
            SELECT DISTINCT rb.id, rb.name FROM res_bank rb, account_journal aj, res_partner_bank rpb
                WHERE aj.bank_account_id = rpb.id
                AND rpb.bank_id = rb.id
                ORDER BY rb.name ASC;
        """)
        banks = []
        for f in cr.fetchall():
            banks.append({'id':f[0],'name':f[1]})
        return banks
    
    @api.model
    def _init_filter_bank(self, options, previous_options=None):
        if self.filter_bank is None:
            return
        if previous_options and previous_options.get('res_bank'):
            journal_map = dict((opt['id'], opt['selected']) for opt in previous_options['res_bank'] if opt['id'] != 'divider' and 'selected' in opt)
        else:
            journal_map = {}
        options['res_bank'] = []

        default_group_ids = []

        for j in self._get_filter_bank():
            options['res_bank'].append({
                'id': j.get('id'),
                'name': j.get('name'),
                'selected': journal_map.get(j.get('id'), j.get('id') in default_group_ids),
            })
    #Filtro para los folios
    @api.model
    def _get_filter_request_open_balance_finance_folio(self):
        return self.env['request.open.balance.finance'].search([('state','=','confirmed')])
    
    @api.model
    def _init_filter_request_open_balance_finance_folio(self, options, previous_options=None):
        if self.filter_request_open_balance_finance_folio is None:
            return
        if previous_options and previous_options.get('request_open_balance_finance_folio'):
            journal_map = dict((opt['id'], opt['selected']) for opt in previous_options['request_open_balance_finance_folio'] if opt['id'] != 'divider' and 'selected' in opt)
        else:
            journal_map = {}
        options['request_open_balance_finance_folio'] = []

        default_group_ids = []

        for j in self._get_filter_request_open_balance_finance_folio():
            options['request_open_balance_finance_folio'].append({
                'id': j.id,
                'name': j.operation_number,
                'selected': journal_map.get(j.id, j.id in default_group_ids),
            })
    
    #Filtro para las cuentas bancarias
    @api.model
    def _get_filter_account_bank(self):
        cr = self._cr
        cr.execute("""
            SELECT aj.id, aj.name, rb.name, rpb.acc_number FROM account_journal aj, res_company rc, res_partner_bank rpb, res_bank rb 
                WHERE aj.company_id = rc.id
                AND aj.bank_account_id = rpb.id
                AND rpb.bank_id = rb.id
                AND rc.name like 'Universidad Nacional Autónoma de México'
                ORDER BY rb.name ASC;
        """)
        data = []
        for f in cr.fetchall():
            data.append(f)
        return data
    
    @api.model
    def _init_filter_account_bank(self, options, previous_options=None):
        if self.filter_account_bank is None:
            return
        if previous_options and previous_options.get('account_bank_info'):
            journal_map = dict((opt['id'], opt['selected']) for opt in previous_options['account_bank_info'] if opt['id'] != 'divider' and 'selected' in opt)
        else:
            journal_map = {}
        options['account_bank_info'] = []

        default_group_ids = []

        for j in self._get_filter_account_bank():
            
            options['account_bank_info'].append({
                'id': j[0],
                'name': str(j[1])+'('+str(j[3])+')',
                'selected': journal_map.get(j[0],j[0] in default_group_ids),
            })

        #Filtro para las solicitudes de transferencia
    @api.model
    def _get_filter_transfer_request(self):
        data = [('0','investments',_('Investments')),('1','projects',_('Projects')),('2','finances',_('Finances'))]
        return data
    
    
    @api.model
    def _init_filter_transfer_request(self, options, previous_options=None):
        if self.filter_transfer_request is None:
            return
        if previous_options and previous_options.get('transfer_request_select'):
            journal_map = dict((opt['id'], opt['selected']) for opt in previous_options['transfer_request_select'] if opt['id'] != 'divider' and 'selected' in opt)
        else:
            journal_map = {}
        options['transfer_request_select'] = []

        default_group_ids = []
        for j in self._get_filter_transfer_request():
            options['transfer_request_select'].append({
                'id': j[0],
                'name': str(j[2]),
                'selected': journal_map.get(j[0],j[1] in default_group_ids),
            })
    



    def _get_reports_buttons(self):
        return [
            {'name': _('Export to PDF'), 'sequence': 1, 'action': 'print_pdf', 'file_export_type': _('PDF')},
            {'name': _('Export (XLSX)'), 'sequence': 2, 'action': 'print_xlsx', 'file_export_type': _('XLSX')},
        ]

    def _get_templates(self):
        templates = super(
            DailyControlReport, self)._get_templates()
        templates[
            'main_table_header_template'] = 'account_reports.main_table_header'
        templates['main_template'] = 'account_reports.main_template'
        return templates

    def _get_columns_name_control_report(self, options,accounts):
        col = [{'name': _('NO DE OF: ENVIADO A')}]

        for i in options['date_sel']:
            if i.get('selected')==True:
                col.append({'name': i.get('name')})
        col.append({'name': _('Folio')})
        col.append({'name': _('Concepto')})

        for i in range(0, len(accounts)):
            col.append({'name': _('Cargo')})
            col.append({'name': _('Abono')})

        options['columns_control']=col

    def _get_columns_name(self, options):
        return options['columns_control']

    def _format(self, value, figure_type):
        if self.env.context.get('no_format'):
            return value
        value['no_format_name'] = value['name']

        if figure_type == 'float':
            currency_id = self.env.company.currency_id
            if currency_id.is_zero(value['name']):
                # don't print -0.0 in reports
                value['name'] = abs(value['name'])
                value['class'] = 'number text-muted'
            value['name'] = formatLang(
                self.env, value['name'], currency_obj=currency_id)
            value['class'] = 'number'
            return value
        if figure_type == 'percents':
            value['name'] = str(round(value['name'] * 100, 1)) + '%'
            value['class'] = 'number'
            return value
        value['name'] = round(value['name'], 1)
        return value

    #Obtención de los bancos de las cuentas utilizadas en las solicitudes de transferencia
    def _get_account_journals(self,journal):
        header_journal =[]
        cuentas = []
        #Cuentas por cargos
        for j in journal:
            if j.bank_account_id not in header_journal:
                header_journal.append(j.bank_account_id)
        #Cuentas por abonos
        for j in journal:
            if j.desti_bank_account_id not in header_journal:
                header_journal.append(j.desti_bank_account_id)

        #Obtención de la información a utilizar como encabezado
        for head in header_journal:
            cuentas.append({'id':head.id,'name':head.name,'bank':head.bank_id.name,'account':head.bank_account_id.acc_number})

        #Orden de los bancos y cuentas
        if len(cuentas)>0:
            cuentas.sort(key=lambda header: (str(header['bank']),  str(header['account'])))

        return cuentas

    #Obtención de los ID's de los filtros
    def _get_list_options(self,options_list):
        list_info=[]
        for opt in options_list:
            if opt['selected'] == True:
                list_info.append(opt['id'])
        return list_info
    #Obtención de las listas en str
    def _get_list_str(self, list_info,aux):
        if aux is False:
            if len(list_info)==1:
                str_info='('+str(*list_info,)+')'
            else:
                str_info=str(tuple(list_info))
        else:
            if len(list_info)==1:
                str_info="('"+str(*list_info,)+"')"
            else:
                str_info=str(tuple(list_info))
        return str_info
    
    #===============================================================================
    #           Funciones para queries
    #===============================================================================
    #Función para obtener fechas para el query
    def _get_query_dates_sql(self,query,list_date,tables,params,start,end,count_or):
        if len(list_date)!=0:
            query+=sql.SQL("""(""")
            for date in list_date:
                if '0' in date:
                    query+= sql.SQL("""(robf.date_required >= %(start)s AND robf.date_required <= %(end)s)""")
        
                elif '1' in date:
                    query+= sql.SQL("""(robf.date >= %(start)s AND robf.date <= %(end)s)""")

                elif '2' in date:
                    tables.append('account_payment ap')
                    tables.append('rel_req_payment rrp')
                    query+= sql.SQL("""robf.id=rrp.payment_id AND rrp.payment_request_rel_id = ap.id AND( ap.payment_date >= %(start)s AND ap.payment_date <= %(end)s )""")

                params.update({'start': start,
                                'end': end})
                if count_or!=(len(list_date)):
                    count_or+=1
                    query+= sql.SQL(""" OR """)
            query+= sql.SQL(""") AND """)
        return query
    #Funcion para obtener los bancos en el sql
    def _get_banks_sql(self,query,list_bank,tables):
        list_bank_table=['account_journal aj','res_partner_bank rpb','res_bank rb']
            
        if len(list_bank)!=0:
            
            for i in list_bank_table:
                if i not in tables: 
                    tables.append(i)
            str_bank=tuple(list_bank)
            
            query+=sql.SQL(""" ((robf.bank_account_id = aj.id AND \
                aj.bank_account_id = rpb.id AND \
                rpb.bank_id = rb.id AND \
                rb.id in {})""").format(
                sql.Literal(str_bank)
                ) 
            query+= sql.SQL("""OR(robf.desti_bank_account_id = aj.id AND \
                aj.bank_account_id = rpb.id AND \
                rpb.bank_id = rb.id AND \
                rb.id in {})) AND """).format(
                sql.Literal(str_bank)
                )
        return query
    #Función para obtener los folios en sql
    def _get_folios_sql(self,query,list_folio):
        if len(list_folio)!=0:
            query+=sql.SQL("""(robf.id in {}) AND """).format(
                sql.Literal(tuple(list_folio))  
            )
        return query
    #Función para obtener cuentas bancarias en sql
    def _get_bank_account_sql(self,query,list_acc_info,tables):
        list_bank_acc = ['account_journal aj']
        if len(list_acc_info)!=0:
            for i in list_bank_acc:
                if i not in tables: 
                    tables.append(i)
            str_acc=tuple(list_acc_info)
            query+=sql.SQL("""((robf.bank_account_id = aj.id \
                        AND aj.id in {})""").format(
                            sql.Literal(str_acc)
                        )
            
            query+=sql.SQL("""OR(robf.desti_bank_account_id = aj.id \
                    AND aj.id in {})) AND """).format(
                        sql.Literal(str_acc)
                    )
        return query
    #Función para buscar transferencias bancarias en sql
    def _get_transfer_sql(self,query,list_transfer):
        #[('0','investments',_('Investments')),('1','projects',_('Projects')),('2','finances',_('Finances'))]     
        transfer_info=[]
        if len(list_transfer)!=0:
            if '0' in list_transfer and "investments" not in transfer_info: #Módulo de inversiones
                transfer_info.append(str("investments"))
            if '1' in list_transfer and "projects" not in transfer_info: #Módulo de Proyectos
                transfer_info.append(str("projects"))
            if '2' in list_transfer and "finances" not in transfer_info: #Módulo de Finanzas
                transfer_info.append(str("finances"))
            query+=sql.SQL("""(robf.trasnfer_request in {}) AND """).format(
                sql.Literal(tuple(transfer_info))
            )
        return query
    #Función de busqueda de información por consultas
    def _search_info(self,options):

        start = datetime.strptime(str(options['date'].get('date_from')), '%Y-%m-%d').date()
        end = datetime.strptime(str(options['date'].get('date_to')), '%Y-%m-%d').date()
        cr = self._cr
        records=[]
        head_query= sql.SQL("""SELECT DISTINCT robf.id, robf.operation_number FROM """)
        tables =[]
        tables.append('request_open_balance_finance robf')
        query= sql.SQL("""WHERE """)
    
        list_date       =  self._get_list_options(options['date_sel'])
        list_bank       =  self._get_list_options(options['res_bank'])
        list_folio      =  self._get_list_options(options['request_open_balance_finance_folio'])
        list_acc_info   =  self._get_list_options(options['account_bank_info'])
        list_transfer   =  self._get_list_options(options['transfer_request_select'])
        count_segments = len(list_date)+len(list_bank)+len(list_folio)+len(list_acc_info)+len(list_transfer) 

        params = {}
        #Query de las fecha
        if count_segments>0:
            count_or=1
            #Quety de fechas
            query=self._get_query_dates_sql(query,list_date,tables,params,start,end,count_or)
            #Query del banco
            query=self._get_banks_sql(query,list_bank,tables)
            #Query de folios
            query=self._get_folios_sql(query,list_folio)
            #Query de cuentas bancarias
            query=self._get_bank_account_sql(query,list_acc_info,tables)
            #Query de solicitud de transferencia
            query=self._get_transfer_sql(query,list_transfer)
            #Generación del query
            total_query=head_query
            for i in range (0, len(tables)):
                total_query+=sql.SQL(f"""{tables[i]}""")
                if i!=len(tables)-1:
                    total_query+=sql.SQL(''', ''')
                else:
                    total_query+=sql.SQL(''' ''')

            query += sql.SQL("""robf.state = 'confirmed' ORDER BY robf.operation_number ASC;""")
            total_query+=query

        else: 
            table=sql.SQL("""request_open_balance_finance robf """) # se coloca el valor directo de table[0]
            final_query = sql.SQL("""robf.state = 'united';""")
            total_query=head_query+table+query+final_query
        #Ejecución del query
        cr.execute(total_query, params)
        ids = []
        for f in cr.fetchall():
            ids.append(f[0])
        

        #Obtención de la infromación de los registros de las solicitudes de transferencia
        records = self.env['request.open.balance.finance'].search([('id','in',ids)])
        return records
    
    #===========================================================================
    #           Funciones para obtener las lineas de la tabla
    #===========================================================================

    #Función para obtener abonos y creditos
    def _get_cargos_abonos_lines(self,accounts,columns,register):
        id_cargos,id_abonos=0,0
        for c in range(0,len(accounts)):
            if accounts[c].get('id')==register.bank_account_id.id:
                #logging.critical("CARGO => INDICE: "+str(c)+" CUENTA: "+str(accounts[c].get('account')))
                id_cargos=(c*2)
            if accounts[c].get('id')==register.desti_bank_account_id.id:
                #logging.critical("ABONO => INDICE: "+str(c)+" CUENTA: "+str(accounts[c].get('account')))
                id_abonos=(c*2)+1
        return id_cargos,id_abonos
    #Función para obtener moneda
    def _get_currency_lines(self,register):
        totalmxn,totalusd=0,0
        if register.currency_id.name == 'MXN':
            totalmxn = totalmxn + float(register.amount)
        elif register.currency_id.name == 'USD':
            totalusd = totalusd + float(register.amount)
        return totalmxn,totalusd
    #Función para obtener fechas en lineas
    def _get_date_lines(self,register,options,columns):
        for i in options['date_sel']:
            if i.get('selected')==True:
                if i.get('name')==_('Required Date'):
                    columns.append({'name': str(register.date_required)})
                elif i.get('name')==_('Created Date'):
                    columns.append({'name': str(register.date)})
                elif i.get('name')==_('Payment Date'):
                    columns.append({'name': str(register.payment_ids.payment_date)})
    #Función para obtener nombre y concepto
    def _get_name_concept_lines(self,register,columns):
        columns.append({'name': str(register.operation_number)})
        if register.concept is False:
            columns.append({'name': "'SIN CONCEPTO'"})
        else:
            columns.append({'name': str(register.concept)})
    #Función para obtención de cargos y abonos de cuentas
    def _get_total_cargos_abonos_lines(self,register,accounts,id_abonos,id_cargos,columns,totales):
        total_abonos = 0.0
        total_cargos = 0.0
        #Obtención de cargos, abonos de las cuentas
        for columna in range(0,2*len(accounts)):
            if columna == id_cargos or columna == id_abonos:
                columns.append(self._format({'name': register.amount},figure_type='float'))
                totales[columna]+=register.amount
                if columna == id_cargos:
                    total_cargos+=register.amount
                else:
                    total_abonos+=register.amount
            else:
                columns.append(self._format({'name': 0.0}, figure_type='float'))
        return total_cargos,total_abonos
    def _assign_format_table(self,totales,columnas_totales,accounts,options,lines,name_totales):
        for v in range(0,len(totales)):
            columnas_totales.append({'name': 'gg'})
            for i in options['date_sel']:
                if i.get('selected'):
                    columnas_totales.append({'name': 'ff'})
            columnas_totales.append({'name': 'ff'})
            
            for i in range(0,2*len(accounts)):
                if i == (2*len(accounts))-1:
                    columnas_totales.append(self._format({'name': float(totales[v])}, figure_type='float'))
                else:
                    columnas_totales.append({'name': 'ff'})
                
            lines.append({
                'id': "currency_",
                'name': name_totales[v],
                'columns': columnas_totales.copy(),
                'level': 2,
                'unfoldable': False,
                'unfolded': True,
            })
            columnas_totales.clear()
    #Obtención de Lineas con los datos de los movimientos
    def _get_lines(self, options, line_id=None,xlsx=False):
        lines = []
        columns =[]
        records = self._search_info(options)
        total_abonos = 0.0
        total_cargos = 0.0
        totalmxn = 0.0
        totalusd = 0.0
        
        accounts=self._get_account_journals(records)
        totales=[0]*(2*len(accounts))
        id_cargos=0
        id_abonos=0

        for register in records:
            #IDS de cargos y Abonos para las columnas
            columns.clear()
            id_cargos,id_abonos=self._get_cargos_abonos_lines(accounts,columns,register)
            totalmxn,totalusd=self._get_currency_lines(register)
            self._get_date_lines(register,options,columns)
            self._get_name_concept_lines(register,columns)
            total_cargos,total_abonos=self._get_total_cargos_abonos_lines(register,accounts,id_abonos,id_cargos,columns,totales)

            lines.append({
                    'id': "currency_",
                    'name': '',
                    'columns': columns.copy(),
                    'level': 3,
                    'unfoldable': False,
                    'unfolded': True,
                })

        #TOTALES DE LAS COLUMNAS
        col_total=[]
        for i in options['date_sel']:
            if i.get('selected'):
                col_total.append({'name': ''})
        col_total.append({'name': ''})
        col_total.append({'name': ''})
        for i in range(0,2*len(accounts)):
            col_total.append(self._format({'name': float(totales[i])}, figure_type='float')),

        lines.append({
                'id': "currency_",
                'name': 'TOTALES',
                'columns': col_total.copy(),
                'level': 2,
                'unfoldable': False,
                'unfolded': True,
            })  

        columnas_totales = []
        totales=[]
        name_totales =[
            'Total Abonos',
            'Total Cargos',
            'Total MXN',
            'Total USD'
        ]
        totales.append(total_abonos)
        totales.append(total_cargos)
        totales.append(totalmxn)
        totales.append(totalusd)
        
        self._assign_format_table(totales,columnas_totales,accounts,options,lines,name_totales)
        if not xlsx:
            return lines,accounts
        return lines
    def get_pdf(self, options, minimal_layout=False):
        # As the assets are generated during the same transaction as the rendering of the
        # templates calling them, there is a scenario where the assets are unreachable: when
        # you make a request to read the assets while the transaction creating them is not done.
        # Indeed, when you make an asset request, the controller has to read the `ir.attachment`
        # table.
        # This scenario happens when you want to print a PDF report for the first time, as the
        # assets are not in cache and must be generated. To workaround this issue, we manually
        # commit the writes in the `ir.attachment` table. It is done thanks to a key in the context.
        
        ir_report=self.env['ir.actions.report']
        web_min_layout="web.minimal_layout"
        if not config['test_enable']:
            self = self.with_context(commit_assetsbundle=True)

        base_url = self.env['ir.config_parameter'].sudo().get_param('report.url') or self.env[
            'ir.config_parameter'].sudo().get_param('web.base.url')
        rcontext = {
            'mode': 'print',
            'base_url': base_url,
            'company': self.env.company,
        }

        body = self.env['ir.ui.view'].render_template(
            "account_reports.print_template",
            values=dict(rcontext),
        )
        body_html = self.with_context(print_mode=True).get_html(options)
        body_html = body_html.replace(b'<div class="o_account_reports_header">', b'<div style="display:none;">')

        body = body.replace(b'<body class="o_account_reports_body_print">',
                            b'<body class="o_account_reports_body_print">' + body_html)
        if minimal_layout:
            header = ''
            footer = ir_report.render_template("web.internal_layout", values=rcontext)
            spec_paperformat_args = {'data-report-margin-top': 10, 'data-report-header-spacing': 10}
            footer = ir_report.render_template(web_min_layout,
                                                                   values=dict(rcontext, subst=True, body=footer))
        else:
            rcontext.update({
                'css': '',
                'o': self.env.user,
                'res_company': self.env.company,
            })
            header = ir_report.render_template(
                "jt_account_module_design.external_layout_daily_count_id", values=rcontext)
            header = header.decode('utf-8')  # Ensure that headers and footer are correctly encoded
            spec_paperformat_args = {}
            # Default header and footer in case the user customized web.external_layout and removed the header/footer
            headers = header.encode()
            footer = b''
            # parse header as new header contains header, body and footer
            try:
                root = lxml.html.fromstring(header)
                match_klass = "//div[contains(concat(' ', normalize-space(@class), ' '), ' {} ')]"

                for node in root.xpath(match_klass.format('header')):
                    headers = lxml.html.tostring(node)
                    headers = ir_report.render_template(web_min_layout,
                                                                            values=dict(rcontext, subst=True,
                                                                                        body=headers))

                for node in root.xpath(match_klass.format('footer')):
                    footer = lxml.html.tostring(node)
                    footer = ir_report.render_template(web_min_layout,
                                                                           values=dict(rcontext, subst=True,
                                                                                       body=footer))

            except lxml.etree.XMLSyntaxError:
                headers = header.encode()
                footer = b''
            header = headers

        landscape = False
        if len(self.with_context(print_mode=True).get_header(options)[-1]) > 5:
            landscape = True

        fec = datetime.now().strftime('%d/%m/%Y %H:%M')
        conc = '<div class="o_account_reports_summary"></div> <div class="float-right"> <span style="font-size:16px;">Fecha y hora de impresión &#160;' + fec + '</span></div>'
        body = body.replace(b'<div class="o_account_reports_summary"></div>', str.encode(conc))
        body = body.replace(b'<tr class="o_account_reports_level2  o_js_account_report_parent_row_unfolded" style="outline: thin solid; outline-width: 2px;">', b'<tr class="o_account_reports_level2  o_js_account_report_parent_row_unfolded" style="border-top:2px solid black">')

        return ir_report._run_wkhtmltopdf(
            [body],
            header=header, footer=footer,
            landscape=landscape,
            specific_paperformat_args=spec_paperformat_args
        )
    #Función para la construcción del encabezado del control, dependiendo de los bancos en la busqueda de las solicitudes de transacción
    #Sección con xlsxwriter
    def _header_constructor_xlsx(self,options,sheet,style):
        records = self._search_info(options)
        accounts=self._get_account_journals(records)
        
        if len(accounts)>0:         
            
            count=2
            for i in options['date_sel']:
                if i.get('selected')==True:
                   count+=1
            sheet.set_column(4,count+(2*len(accounts)),20)
            #Asignación de Bancos en celdas combinadas
            for row in range(0,2*len(accounts),2):
                sheet.merge_range(8,count+row+1,8,count+row+2,str(accounts[int(row/2)].get('bank')),style)
            
            sheet.merge_range(9,0,9,count,"CUENTAS BANCARIAS",style)
            
            #Asignación de Cuentas Bancarias en celdas combinadas
            for row in range(0,2*len(accounts),2):
                sheet.merge_range(9,count+row+1,9,count+row+2,str(accounts[int(row/2)].get('account')),style)
    
    #CREACIÓN DE CELDAS con información
    def _write_remain_info(self,sheet,lines,y,y_offset,style,date_default_style):
        # write all the remaining cells
        for x in range(1, len(lines[y]['columns']) + 1):
            cell_type, cell_value = self._get_cell_type_value(lines[y]['columns'][x - 1])
            if cell_type == 'date':
                sheet.write_datetime(y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value,
                                        date_default_style)
            else:
                if cell_value not in ['gg','ff']:
                    sheet.write(y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, style)
                else:
                    sheet.write(y + y_offset, x + lines[y].get('colspan', 1) - 1, "", style)
    def _write_all_data_columns(self,lines,workbook,sheet,y_offset):
        date_default_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2, 'num_format': 'yyyy-mm-dd'})
        date_default_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': 'yyyy-mm-dd'})
        default_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2})
        default_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'})
        level_0_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 13, 'bottom': 6, 'font_color': '#666666'})
        level_1_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 13, 'bottom': 1, 'font_color': '#666666'})
        level_2_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 1})
        level_2_col1_total_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666'})
        level_2_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666'})
        level_3_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2})
        level_3_col1_total_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 1})
        level_3_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'})
        for y in range(0, len(lines)):
            level = lines[y].get('level')
            if lines[y].get('caret_options'):
                style = level_3_style
                col1_style = level_3_col1_style
            elif level == 0:
                y_offset += 1
                style = level_0_style
                col1_style = style
            elif level == 1:
                style = level_1_style
                col1_style = style
            elif level == 2:
                style = level_2_style
                col1_style = 'total' in lines[y].get('class', '').split(
                    ' ') and level_2_col1_total_style or level_2_col1_style
            elif level == 3:
                style = level_3_style
                col1_style = 'total' in lines[y].get('class', '').split(
                    ' ') and level_3_col1_total_style or level_3_col1_style
            else:
                style = default_style
                col1_style = default_col1_style

            # write the first column, with a specific style to manage the indentation
            cell_type, cell_value = self._get_cell_type_value(lines[y])
            if cell_type == 'date':
                sheet.write_datetime(y + y_offset, 0, cell_value, date_default_col1_style)
            else:
                sheet.write(y + y_offset, 0, cell_value, col1_style)

            self._write_remain_info(sheet,lines,y,y_offset,style,date_default_style)
            

    def get_xlsx(self, options, response=None):
        output = io.BytesIO()
        
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        sheet = workbook.add_worksheet(self._get_report_name()[:31])

        
        title_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'bottom': 2})
        super_col_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'align': 'center'})
        
        currect_date_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'align': 'right'})
        currect_date_style.set_border(0)
        super_col_style.set_border(0)
        

        sheet.set_column(0, 0, 20)
        sheet.set_column(1, 2, 15)
        sheet.set_column(3, 3, 20)

        y_offset = 0
        col = 0

        sheet.merge_range(y_offset, col, 6, col, '', super_col_style)
        if self.env.user and self.env.user.company_id and self.env.user.company_id.header_logo:
            filename = 'logo.png'
            image_data = io.BytesIO(base64.standard_b64decode(self.env.user.company_id.header_logo))
            sheet.insert_image(0, 0, filename,
                               {'image_data': image_data, 'x_offset': 8, 'y_offset': 3, 'x_scale': 0.6, 'y_scale': 0.6})

        col += 1
        header_title = '''UNIVERSIDAD NACIONAL AUTÓNOMA DE MÉXICO\nUniversity Board\nGeneral Directorate of Finance\n%s''' % self._get_report_name()
        sheet.merge_range(y_offset, col, 5, col + 3, header_title, super_col_style)
        y_offset += 6
        col = 1
        currect_time_msg = "Fecha y hora de impresión: "
        currect_time_msg += datetime.today().strftime('%d/%m/%Y %H:%M')
        sheet.merge_range(y_offset, col, y_offset, col + 3, currect_time_msg, currect_date_style)
        y_offset += 1
        sheet.merge_range(y_offset, 1, y_offset, 2, "PERIODO", super_col_style)
        y_offset += 1

        self._header_constructor_xlsx(options,sheet,super_col_style)

        y_offset += 2
        
        for row in self.get_header(options):
            x = 0
            for column in row:
                colspan = column.get('colspan', 1)
                header_label = column.get('name', '').replace('<br/>', ' ').replace('&nbsp;', ' ')
                if colspan == 1:
                    sheet.write(y_offset, x, header_label, title_style)
                else:
                    sheet.merge_range(y_offset, x, y_offset, x + colspan - 1, header_label, title_style)
                x += colspan
            y_offset += 1
        
        ctx = self._set_context(options)
        ctx.update({'no_format': True, 'print_mode': True, 'prefetch_fields': False})
        # deactivating the prefetching saves ~35% on get_lines running time
        lines= self.with_context(ctx)._get_lines(options,xlsx=True)
        
        if options.get('hierarchy'):
            lines = self._create_hierarchy(lines, options)
        if options.get('selected_column'):
            lines = self._sort_lines(lines, options)

        # write all data rows
        self._write_all_data_columns(lines,workbook,sheet,y_offset)
        workbook.close()
        output.seek(0)
        generated_file = output.read()
        output.close()
        return generated_file

    #Función para la construcción del encabezado del control, dependiendo de los bancos en la busqueda de las solicitudes de transacción
    def _header_constructor(self,accounts,options):
        header=''
        if len(accounts)>0:
            count=3
            for i in options['date_sel']:
                if i.get('selected')==True:
                   count+=1

            header = '<tr class="o_account_report_column_header"> <th colspan="{count}" class="o_account_report_column_header " style="text-align:center !important"></th>'.format(count=count)
            #Bancos
            for acc in accounts:
                header+='<th colspan="2" class="o_account_report_column_header " style="text-align:center !important">{bank}</th>'.format(bank=acc['bank'])
            header+='</tr>'
            #Cuentas
            

            header+= '<tr class="o_account_report_column_header"> <th colspan="{count}" class="o_account_report_column_header " style="text-align:center !important">Cuentas Bancarias</th>'.format(count=count)
            for acc in accounts:
                header+='<th colspan="2" class="o_account_report_column_header " style="text-align:center !important">{account}</th>'.format(account=acc['account'])
            header+='</tr>'
        header=bytes(header, 'utf-8')
        return header

    def _get_hierarchy(self,options,lines):
        if options.get('hierarchy'):
            lines = self._create_hierarchy(lines, options)
        if options.get('selected_column'):
            lines = self._sort_lines(lines, options)
        return lines

    def _get_print_mode(self,lines,report_manager,footnotes_to_render):
        if self.env.context.get('print_mode', False):
            # we are in print mode, so compute footnote number and include them in lines values, otherwise, let the js compute the number correctly as
            # we don't know all the visible lines.
            footnotes = dict([(str(f.line), f) for f in report_manager.footnotes_ids])
            number = 0
            for line in lines:
                f = footnotes.get(str(line.get('id')))
                if f:
                    number += 1
                    line['footnote'] = str(number)
                    footnotes_to_render.append({'id': f.id, 'number': number, 'text': f.text})
    def get_html(self, options, line_id=None, additional_context=None):
        '''
        return the html value of report, or html value of unfolded line
        * if line_id is set, the template used will be the line_template
        otherwise it uses the main_template. Reason is for efficiency, when unfolding a line in the report
        we don't want to reload all lines, just get the one we unfolded.
        '''
        # Check the security before updating the context to make sure the options are safe.
        self._check_report_security(options)
        
        # Prevent inconsistency between options and context.
        self = self.with_context(self._set_context(options))

        templates = self._get_templates()
        
        report_manager = self._get_report_manager(options)

        #Obtener las lineas, las columnas de las cuentas
        lines,accounts = self._get_lines(options, line_id=line_id)
        self._get_columns_name_control_report(options,accounts)

        lines=self._get_hierarchy(options,lines)
        
        footnotes_to_render = []
        self._get_print_mode(lines,report_manager,footnotes_to_render)
        report = {'name': self._get_report_name(),
                'summary': report_manager.summary,
                'company_name': self.env.company.name,}
        rcontext = {'report': report,
                    'lines': {'columns_header': self.get_header(options), 'lines': lines},
                    'options': {},
                    'context': self.env.context,
                    'model': self,
                    }
        
        
        if additional_context and type(additional_context) == dict:
            rcontext.update(additional_context)
        if self.env.context.get('analytic_account_ids'):
            rcontext['options']['analytic_account_ids'] = [
                {'id': acc.id, 'name': acc.name} for acc in self.env.context['analytic_account_ids']
            ]

        render_template = templates.get('main_template', 'jt_account_module_design.financial_statement_main_template')
        if line_id is not None:
            render_template = templates.get('line_template', 'account_reports.line_template')
        html = self.env['ir.ui.view'].render_template(
            render_template,
            values=dict(rcontext),
        )
        if self.env.context.get('print_mode', False):
            for k, v in self._replace_class().items():
                html = html.replace(k, v)
            # append footnote as well
            html = html.replace(b'<div class="js_account_report_footnotes"></div>',
                                self.get_html_footnotes(footnotes_to_render))
        #Ajustes de la tabla por las fechas seleccionadas
        count=0
        for i in options['date_sel']:
            if i.get('selected'):
                count+=1

        spaces='<td colspan='+str(2*len(accounts)+count+1)+' style="text-align:center !important"></td>'
        spaces=bytes(spaces, 'utf-8')
        

        #COnfiguración del Formato de la tabla en vista del reporte
        html = html.replace(b'<th class="o_account_report_column_header " style="">',b'<th class="o_account_report_column_header " style="text-align:center !important">')
        ##Cambio para el encabezado con respecto a 
        html = html.replace(b'<thead>',self._header_constructor(accounts,options))
        html = html.replace(b'</thead>',b' ')
        html = html.replace(b'<tbody>', b' ')
        html = html.replace(b'</tbody>', b' ')
        html = html.replace(b'<td class="o_account_report_line  o_account_report_line_indent" style="">', b'<td class="o_account_report_line  o_account_report_line_indent" style="text-align:left !important">')
        html = html.replace(b'<table class="o_account_reports_table table-hover">', b'<table class="o_account_reports_table table-hover" bordercolor="blue" border = "1"> ')
        html = html.replace(b'<td class="o_account_report_line  o_account_report_line_indent" style="text-align:left !important">',b'<td class="o_account_report_line" style="text-align:left !important">')
        html = html.replace(b'<td class="o_account_report_line number o_account_report_line_indent" style="">', b'<td class="o_account_report_line number o_account_report_line_indent" style="text-align:right !important">')
        html = html.replace(b'<tr class="o_account_reports_level2  o_js_account_report_parent_row_unfolded" style="">', b'<tr class="o_account_reports_level2  o_js_account_report_parent_row_unfolded" style="outline: thin solid; outline-width: 2px;">')
        html = html.replace(b'<td class="o_account_report_line" style="text-align:left !important">\n                    <span class="o_account_report_column_value">\n                        ff\n                    </span>\n                </td>', b'')
        html = html.replace(b'<td class="o_account_report_line" style="text-align:left !important">\n                    <span class="o_account_report_column_value">\n                        gg\n                    </span>\n                </td>',spaces)

        return html


    def _get_report_name(self):
        return _("Daily Control Report")
