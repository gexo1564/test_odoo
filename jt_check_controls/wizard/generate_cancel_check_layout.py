from odoo import models, fields,_
from odoo.exceptions import UserError, ValidationError
import base64
from datetime import datetime, timedelta
from odoo.tools.misc import formatLang, format_date, get_lang
from babel.dates import format_datetime, format_date

import logging
_logger = logging.getLogger(__name__)

class GenerateCancelCheckLayout(models.TransientModel):
    _name = 'generate.cancel.check.layout'
    _description = 'Generate Cancel Check Layout'

    format_layout = fields.Selection([('Banamex', 'Banamex'), ('BBVA Bancomer', 'BBVA Bancomer'),
                                ('Scotiabank', 'Scotiabank'),('Inbursa', 'Inbursa'),('Banamex_2', 'Banamex 2'),
                                ('Santander','Santander')], string="Layout")
    file_name = fields.Char('Filename')
    file_data = fields.Binary('Download')
    reissue_ids = fields.Many2many('reissue.checks','reissue_checks_bank_layout_rel','check_layout_id','reissue_id','Reissue')

    def banamex_cancel_check_file_format(self):
        file_data = ''
        file_name = 'Banamex.txt'
        
        #===Transaction Type ====#
        file_data += '01'
        branch_no = '0000'
        if self.reissue_ids:
            check = self.reissue_ids[0]
            
        if check.bank_id and check.bank_id.branch_number:
            branch_no = check.bank_id.branch_number.zfill(4)
        file_data += branch_no
        #===Destination Account ====#
        bank_account = '00000000000000000000'
        if check.bank_account_id and check.bank_account_id.acc_number:
            bank_account = check.bank_account_id.acc_number.zfill(20)
             
        file_data += bank_account
        #===Client ====#
        file_data += '000008505585'
        #===File Format ====#
        file_data += '0001'
        #===Sequential ====#
        file_data += '000001'
        #===Future use ====#
        file_data += '000000000000'
        file_data +="\r\n"
        
        total_rec = len(self.reissue_ids)
        total_amount = 0
        
        for check in self.reissue_ids:
            #===Transaction Type ====#
            file_data += '02'
            #===Branch Account Number ====#
            branch_no = '0000'
            if check.bank_id and check.bank_id.branch_number:
                branch_no = check.bank_id.branch_number.zfill(4)
            file_data += branch_no
            #===Destination Account ====#
            bank_account = '00000000000000000000'
            if check.bank_account_id and check.bank_account_id.acc_number:
                bank_account = check.bank_account_id.acc_number.zfill(20)
                 
            file_data += bank_account
            
            temp_log = ''
            if check.check_log_id and check.check_log_id.folio:
                temp_log =  check.check_log_id.folio
                file_data += str(temp_log).zfill(7)
            else:
                file_data += temp_log.zfill(7)

            #=== Check Status=======#
            file_data += '08'

            #====== Amount Data =========
            amount = round(check.check_amount, 2)
            amount = "%.2f" % check.check_amount            
            amount = str(amount).split('.')
            file_data +=str(amount[0]).zfill(12)
            file_data +=str(amount[1])
            
            total_amount += check.check_amount
            #=====Future use=========#
            file_data += '00000000000'
            file_data +="\r\n"
        
        #=======TRAILER =======#
        
        #=======Transaction Type =======#
        file_data += '03'
        
        #====== Total Movement ======#
        file_data +=str(total_rec).zfill(6)

        #======Total Amount Data =========
        amount = round(total_amount, 2)
        amount = "%.2f" % total_amount            
        amount = str(amount).split('.')
        file_data +=str(amount[0]).zfill(14)
        file_data +=str(amount[1])

        gentextfile = base64.b64encode(bytes(file_data, 'utf-8'))
        self.file_data = gentextfile
        self.file_name = file_name

    def BBVA_cancel_check_file_format(self):
        file_data = ''
        file_name = 'BBVA Bancomer.txt'
        
        #====== Type of register ==#
        file_data += 'T'

        #====== Total Movement ======#
        bank_account = ''
        if self.reissue_ids and self.reissue_ids[0].bank_account_id:
            bank_account = self.reissue_ids[0].bank_account_id.acc_number
            
        total_rec = len(self.reissue_ids)
        total_amount = sum(x.check_amount for x in self.reissue_ids) 
        file_data +=str(total_rec).zfill(5)

        #======Total Amount Data =========
        amount = round(total_amount, 2)
        amount = "%.2f" % total_amount            
        amount = str(amount).split('.')
        file_data +=str(amount[0]).zfill(16)
        file_data +=str(amount[1])
        
        file_data += '\r\n'  

 
        #====== Register detail =========
        for check in self.reissue_ids:
            
            #====== Type of suspension ==#
            file_data += 'N'

            # ==== bank account ===#
            bank_account = ''
            if self.reissue_ids and self.reissue_ids[0].bank_account_id:
                bank_account = self.reissue_ids[0].bank_account_id.acc_number
                
            file_data += bank_account.zfill(18)
            total_rec = len(self.reissue_ids)
            total_amount = sum(x.check_amount for x in self.reissue_ids) 
            
            #====== Check Folio Number =========
            temp_log = ''
            if check.check_log_id and check.check_log_id.folio:
                temp_log =  check.check_log_id.folio
                file_data += str(temp_log).zfill(15)
            else:
                file_data += temp_log.zfill(15)

            
            #====== Current Dat time=========
            currect_time = datetime.today()
            file_data +=str(currect_time.day).zfill(2) + "/"
            file_data +=str(currect_time.month).zfill(2) + "/"
            file_data +=str(currect_time.year) 

            #====== Amount Data =========
            amount = round(check.check_amount, 2)
            amount = "%.2f" % check.check_amount            
            amount = str(amount).split('.')
            file_data +=str(amount[0]).zfill(14)
            file_data +=str(amount[1])

            
            #====== Parnet ID =========
            partner_name = ''
            partner_name = check.partner_id.name
            partner_name = partner_name.translate(str.maketrans('áéíóúäëïöüñÁÉÍÓÚÄËÏÖÜÑ', 'aeiouaeiou&AEIOUAEIOU&')) if partner_name else ''
            partner_name = ''.join(i for i in partner_name if not i in ['!','“', '#', '/', '(', ')', ')', '=', '=', '?', '¡', '*', '}', '{', ']', '[', '.', ',', ';', ':', '¨', "'",'"', '´', '¨' ])
            partner_name = partner_name.replace('No','&')
            partner_name = partner_name[:30].ljust(30)
            file_data += str(partner_name)
            file_data += '\r\n'

        gentextfile = base64.b64encode(bytes(file_data, 'utf-8'))
        self.file_data = gentextfile
        self.file_name = file_name
            

    def scotiabank_cancel_check_file_format(self):
        file_data = ''
        file_name = 'Scotiabank.txt'

        #===== Type of Registration =====#
        file_data += 'H'

        #====== Current Dat time=========
        currect_time = datetime.today()
        
        file_data +=str(currect_time.year) 
        file_data +=str(currect_time.month).zfill(2)
        file_data +=str(currect_time.day).zfill(2)
        
        #===== Filler ======#
        filler = ''
        file_data += filler.ljust(140, " ")

        #===== Key movement =====#
        file_data += '1'

        file_data += '\r\n'
        
        total_amount = 0
        total_check_folio = 0
        for check in self.reissue_ids:
            #===== Registration =====#
            file_data += 'B'
            
            branch_number = '001'

#             if check.bank_id and check.bank_id.branch_number and len(check.bank_id.branch_number) > 3:
#                 branch_number = check.bank_id.branch_number[-3:]
#             elif check.bank_id and check.bank_id.branch_number:
#                 branch_number = check.bank_id.branch_number if check.bank_id.branch_number else ''
            
            file_data += branch_number

            #===== Account Currency=====#
            file_data += '1'
            
            #===== Bank Account ====#
            bank_account = ''
            if check.bank_account_id and check.bank_account_id.acc_number:
                bank_account = check.bank_account_id.acc_number[-10:]
            
            file_data +=bank_account.zfill(-10)
            
            #====== Check Folio ======#
            temp_log = ''
            if check.check_log_id and check.check_log_id.folio:
                total_check_folio += check.check_log_id.folio
                temp_log =  check.check_log_id.folio
                file_data += str(temp_log).zfill(10)
            else:
                file_data += temp_log.zfill(10)
            
            total_amount += check.check_amount
            
            #====== Amount Data =========#
            amount = round(check.check_amount, 2)
            amount = "%.2f" % check.check_amount            
            amount = str(amount).split('.')
            file_data +=str(amount[0]).zfill(13)
            file_data +=str(amount[1])
            
            #===== Beneficiary's =====#
            partner_name = ''
            if check.partner_id:
                partner_name = check.partner_id.name
            partner_name = partner_name.translate(str.maketrans('áéíóúäëïöüñÁÉÍÓÚÄËÏÖÜÑ', 'aeiouaeiou&AEIOUAEIOU&')) if partner_name else ''
            partner_name = ''.join(i for i in partner_name if not i in ['!','“', '#', '/', '(', ')', ')', '=', '=', '?', '¡', '*', '}', '{', ']', '[', '.', ',', ';', ':', '¨', "'",'"', '´', '¨' ])
            partner_name = partner_name.replace('No','&')    
            file_data += partner_name.ljust(60)
            
            #======= Fillrer=====
            file_data += ''.ljust(49)

            #======= Key Movement=====
            file_data += '3'
            
            file_data += '\r\n'
        
        #====Trailer========#
        
        #=========== Type Of Registration ====#
        file_data += 'P'

        #======= Total highs =====
        file_data += ''.zfill(9)

        #======= Checksum of Check Numbers =====
        file_data += ''.zfill(15)

        #======= Sum Of amounts =====
        file_data += ''.zfill(18)
        
        #======= Total Records =====
        total_rec= len(self.reissue_ids)
        file_data +=str(total_rec).zfill(9)
        
        #===== Checksum (Low) ===#
        amount = "%.2f" % total_check_folio            
        amount = str(amount).split('.')
        file_data +=str(amount[0]).zfill(15)

        #===== Sum of amounts (Low) ===#    
        amount = round(total_amount, 2)
        amount = "%.2f" % total_amount       
        amount = str(amount).split('.')
        file_data +=str(amount[0]).zfill(16)
        file_data +=str(amount[1])
        
        #===== Total high and lows===#
        file_data +=str(total_rec).zfill(10)

        #===== Checksum (Low) ===#
        amount = "%.2f" % total_check_folio            
        amount = str(amount).split('.')
        file_data +=str(amount[0]).zfill(15)

        #===== Sum of amounts (Low) ===#    
        amount = round(total_amount, 2)
        amount = "%.2f" % total_amount       
        amount = str(amount).split('.')
        file_data +=str(amount[0]).zfill(16)
        file_data +=str(amount[1])
        
        #======= Fillrer=====
        file_data += ''.ljust(21)

        #======= Key Movement=====
        file_data += '5'

                
        gentextfile = base64.b64encode(bytes(file_data, 'utf-8'))
        self.file_data = gentextfile
        self.file_name = file_name
            
    def inbursa_cancel_check_file_format(self):
        file_data = ''
        file_name = 'Inbursa.txt'

        for check in self.reissue_ids:
            check_data = check.check_log_id
            num_acc = str(check_data.bank_account_id.acc_number)
            folio = str(check_data.folio_ch)

            if num_acc.isnumeric():
                file_data += num_acc.zfill(11)
            file_data += ''.ljust(3)
            if folio.isnumeric():
                file_data += folio.zfill(13)

        gentextfile = base64.b64encode(bytes(file_data, 'utf-8'))
        self.file_data = gentextfile
        self.file_name = file_name

    def banamex2_cancel_check_file_format(self):
        file_data = ''
        file_name = 'Banamex_2.txt'

        for check in self.reissue_ids:
            payment_id = self.env['account.payment'].search([('check_folio_id', '=', check.check_log_id.id)])
            # Tipo de Transacción
            file_data += '08'
            # Tipo de Cuenta Origen
            file_data += '01'
            # Sucursal Cuenta Origen
            file_data += '0650'
            # Cuenta Origen
            file_data += str(check.bank_account_id.acc_number).zfill(20)
            # Tipo de Cuenta Destino 
            file_data += '00'
            # Sucursal Cuenta Destino
            file_data += '0000'
            # Cuenta Destino
            file_data += '0'.zfill(20)
            # Importe 
            file_data += str(int(payment_id.amount * 100)).zfill(14)
            # Tipo de Moneda
            if payment_id.currency_id.name == 'MXN':
                file_data += '001'
            elif payment_id.currency_id.name == 'USD':
                file_data += '005'
            # Estatus Cheques
            file_data += '08'
            # Cheque Inicial
            file_data += str(check.check_log_id.folio_ch).zfill(8)
            # Cheque Final
            file_data += str(check.check_log_id.folio_ch).zfill(8)

            # No. de Autorizacion
            file_data += '0'.zfill(6)
            # Autorizacion 2
            file_data += '0'.zfill(6)
            # Fecha de Aplicacion
            file_data += '0'.zfill(6)
            # Hora de Aplicacion
            file_data += '0'.zfill(4)

        gentextfile = base64.b64encode(bytes(file_data, 'utf-8'))
        self.file_data = gentextfile
        self.file_name = file_name

    def santander_cancel_check_file_format(self):
        file_data = ''
        file_name = 'Santander.txt'

        for check in self.reissue_ids:
            # Codigo de Layout
            file_data += 'LC002'
            # Tipo de Acción
            file_data += 'C'
            # Cuenta de Cheques
            file_data += str(check.bank_account_id.acc_number).zfill(16)
            # Numero de Cheque
            file_data += str(check.check_log_id.folio_ch).zfill(7)

            pay_bat_sup_id = self.env['check.payment.req'].search([('check_folio_id', '=', check.check_log_id.id)],limit=1)
            if pay_bat_sup_id:
                pay_req_name = str(pay_bat_sup_id.payment_req_id.name)
                acc_move_id = self.env['account.move'].search([('name', '=', pay_req_name)],limit=1)
                if acc_move_id:
                    payroll_name = str(acc_move_id.payroll_processing_id.name)
                    cust_pay_process_id = self.env['custom.payroll.processing'].search([('name', '=', payroll_name)],limit=1).id
                    _logger.info(f'****cust_pay_process_id: {cust_pay_process_id}')
                    if cust_pay_process_id:
                        emp_pay_file_id = self.env['employee.payroll.file'].search([('payroll_processing_id', '=', cust_pay_process_id),('check_number', '=', check.check_log_id.folio_ch)],limit=1)
                        if emp_pay_file_id:
                            # Nombre del Beneficiario
                            benef_name =str(emp_pay_file_id.employee_id.name).translate(str.maketrans('áéíóúäëïöüÁÉÍÓÚÄËÏÖÜ', 'aeiouaeiouAEIOUAEIOU')) if emp_pay_file_id.employee_id.name else ''.ljust(60)
                            file_data += benef_name.ljust(60) 
                            # Importe
                            file_data += str(int(emp_pay_file_id.net_salary * 100)).zfill(16)
                        else:
                            raise UserError(_('No payment receipts were found related to the requested check number.'))
                    else:
                        raise UserError(_(f'Does not exist {payroll_name} in Payroll Processing'))
                else:
                    raise UserError(_(f'No payroll payment request was found with the number: {pay_req_name}'))
            else:
                raise UserError(_('There is no request for payment with this check'))
                
            # Divisa
            file_data += 'MXN'  

            # Fecha de Libramiento 
            if check.check_log_id.date_protection:
                p_d = (check.check_log_id.date_protection).strftime("%d/%m/%Y")
                prot_dat = p_d.replace("-", "/")
                file_data += prot_dat.ljust(10)
            else:
                raise UserError(_('The Protection date field is not assigned'))
            # Fecha Limite de Pago
            if check.check_log_id.date_expiration:
                d_e = (check.check_log_id.date_expiration).strftime("%d/%m/%Y")
                date_exp = d_e.replace("-", "/")
                file_data += date_exp.ljust(10)
            else:
                raise UserError(_('The Expiration date field is not assigned'))
            # Estatus
            file_data += 'CA'

            
        gentextfile = base64.b64encode(bytes(file_data, 'utf-8'))
        self.file_data = gentextfile
        self.file_name = file_name
    
    def action_generate(self):
        for check in self.reissue_ids:            
            if check.state != 'approved':
                raise ValidationError(_('Confirm that the status of all selected checks is "Approved", therefore no '
                                        'layout can be generated until the status is not Approved'))

            if check.type_of_batch in ('supplier', 'project'):
                if check.type_of_request != 'check_cancellation':
                    raise ValidationError(_('Confirm that the all selected checks Type of Request is "Check Cancellation", therefore no '
                                        'layout can be generated until the Type of Request is not Check Cancellation'))
            else:
                if check.type_of_request_payroll != 'check_cancellation':
                    raise ValidationError(_('Confirm that the all selected checks Type of Request is "Check Cancellation", therefore no '
                                        'layout can be generated until the Type of Request is not Check Cancellation'))
            if check.status != 'Cancelled':
                raise ValidationError(_('Confirm that all selected checks Status is "Cancelled", therefore no '
                                        'layout can be generated until the Check Status is not Cancelled'))

            if not check.bank_id:
                raise ValidationError(_('Please select Payment Issuing Bank into records'))

            if check.bank_id and not check.bank_id.bank_id:
                raise ValidationError(_('Please select Bank into Payment Issuing Bank'))

            # if check.bank_id.bank_id.name.upper() != self.layout.upper():
            #     raise ValidationError(_('The selected layout does NOT match the bank of the selected records" and no '
            #                             'layout can be generated until the correct bank is selected.'))
            check.layout_generated = True
            
        if self.format_layout == 'Banamex':
            self.banamex_cancel_check_file_format()
        elif self.format_layout == 'BBVA Bancomer':
            self.BBVA_cancel_check_file_format()
        elif self.format_layout == 'Scotiabank':
            self.scotiabank_cancel_check_file_format()
        elif self.format_layout == 'Inbursa':
            self.inbursa_cancel_check_file_format()
        elif self.format_layout == 'Banamex_2':
            self.banamex2_cancel_check_file_format()
        elif self.format_layout == 'Santander':
            self.santander_cancel_check_file_format()
                    
        return {
            'name': _('Generate Bank Layout'),
            'res_model': 'generate.cancel.check.layout',
            'res_id' : self.id,
            'view_mode': 'form',
            'target': 'new',
            #'view_id': self.env.ref('jt_supplier_payment.view_generate_payroll_payment_bank_layout').id,
            'type': 'ir.actions.act_window',
        }