# -*- coding: utf-8 -*-
from odoo import models, fields


class Dependency(models.Model):
    _inherit = 'dependency'

    allowed_users_payment = fields.Many2many('res.users',  'allowed_users_dep_payment_rel', string ='Allowed users')

class SubDependency(models.Model):
    _inherit = 'sub.dependency'

    allowed_users_payment = fields.Many2many('res.users',  'allowed_users_sd_payment_rel', string ='Allowed users')