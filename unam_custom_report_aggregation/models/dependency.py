# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.osv import expression
from odoo.tools.misc import groupby
from functools import reduce
from datetime import date, datetime, timedelta

class Dependency(models.Model):
    _inherit = 'dependency'

    _order = 'dependency asc'


    def get_account_balances(self, account, dates, options):
        '''
        (New) Return the balances of an account grouped by dependency for a 
        certain period

        :param account: account.account model
        :param dates: List of tuple pair  of date fields in the form of 
        (start, end)
        :param options: Dictionary with various options:
            - posted_only: Will only calculate balances using posted amls
            - dep_domain: Extra domain for the aml's to consider
            - all_deps: If false, will only calculate for balances for the
            depencency on self
            - subdep_balance: Will also calculate the balances for 
            subdependencies
        '''

        posted_only = options.get('posted_only', True)
        dep_domain = options.get('dep_domain', [])
        sd_domain = options.get('sd_domain', [])
        all_deps = options.get('all_deps', True)
        init_balance = options.get('init_balance', False)
        subdep_balance = options.get('subdep_balance', False)

        if not account.dep_subdep_flag:
            return []
        if init_balance:
            dates = [(date.min, dates[0][0] - timedelta(days=1))] + dates

        fields = [
            'debit',
            'credit',
            'balance',
            'dependency_id.dependency',
            'dependency_id.description',
            'sub_dependency_id.sub_dependency',
            'sub_dependency_id.description'
        ]

        domain = [
            ('account_id', '=', account.id),
            ('parent_state', '=', 'posted') if posted_only else ('parent_state', '!=', 'cancel')
        ]
        if sd_domain:
            domain.append(('sub_dependency_id', 'in', sd_domain))
        if dep_domain:
            domain.append(('dependency_id', 'in', dep_domain))

        def aml_dep_filter(aml, dep_id):
            if aml.get('dependency_id') and aml.get('dependency_id')[0]:
                return aml.get('dependency_id')[0] == dep_id
            if not aml.get('dependency_id') and not dep_id:
                return True
            return False

    
        res_dict = {}
        for i, (start_date, end_date) in enumerate(dates):
            period_domain = domain + [
                ('date', '>=', start_date),
                ('date', '<=', end_date),
            ]
            if all_deps and self.id:
                period_domain += ('dependency_id', '=', self.id)

            subdep_groups = []
            if subdep_balance:
                subdep_groups = self.env['account.move.line'].read_group(period_domain, fields, 
                    ['dependency_id', 'sub_dependency_id'], lazy=False, orderby='dependency_id asc,sub_dependency_id asc')

            period_amls = []

            dep_groups = self.env['account.move.line'].read_group(period_domain, fields, 'dependency_id', orderby='dependency_id asc')
            for group in dep_groups:
                dep_id = None
                if group.get('dependency_id') and group.get('dependency_id')[0]:
                    dep_id = group.get('dependency_id')[0]

                if dep_id not in res_dict:
                    res_dict[dep_id] = {}
                    res_dict[dep_id]['dep'] = {}
                
                res_dict[dep_id]['dep'][i] = {
                    'account': account,
                    'dep': dep_id,
                    'debit': group.get('debit'),
                    'credit': group.get('credit'),
                    'balance': group.get('balance'),
                }


                if subdep_balance:
                    grouped_subdep_amls = list(filter(lambda aml: aml_dep_filter(aml, dep_id), subdep_groups))
                    for grouped_aml in grouped_subdep_amls:
                        subdep_id = None
                        if grouped_aml.get('sub_dependency_id') and grouped_aml.get('sub_dependency_id')[0]:
                            subdep_id = grouped_aml.get('sub_dependency_id')[0]
                        subdep = self.env['sub.dependency'].browse(subdep_id)

                        if subdep_id not in res_dict[dep_id]:
                            res_dict[dep_id][subdep_id] = {}

                        res_dict[dep_id][subdep_id][i] = {
                            'account': account,
                            'dep': dep_id,
                            'subdep': subdep,
                            'debit': grouped_aml.get('debit'),
                            'credit': grouped_aml.get('credit'),
                            'balance': grouped_aml.get('balance'),
                        }

        res = []
        for dep_id, subdep_dict in res_dict.items():
            dep = self.env['dependency'].browse(dep_id)
            dep_vals = {
                'deps': [],
                'subdeps': [],
            }
            for subdep_id, period_dict in subdep_dict.items():
                subdep = None
                subdep_res = []
                if subdep_id != 'dep':
                    subdep = self.env['sub.dependency'].browse(subdep_id)
                
                for i, period in enumerate(dates):
                    val = period_dict.get(i, {
                        'account': account,
                        'dep': dep_id,
                        'subdep': subdep,
                        'debit': 0,
                        'credit': 0,
                        'balance': 0,
                    })
                    if subdep_id == 'dep':
                        dep_vals['deps'].append(val)
                    else:
                        subdep_res.append(val)
                if subdep_id != 'dep':
                    dep_vals['subdeps'].append(subdep_res)
            res.append((dep, dep_vals))

        '''
        Even though we have already ordered the sub dependencies in read_group,
        because we are looping through period, if a sub dependency doesn't have
        any values in the initial balance, the ordering will be incorrect.
        '''
        # Se ordena el resultado por dependencia
        res.sort(key= lambda d: d[0].dependency or '999')
        for dep_dict in res:
            if dep_dict[1].get('subdeps'):
                if any(list(map(lambda s: isinstance(s, (bool)), dep_dict[1].get('subdeps')))):
                    print('here')
                dep_dict[1].get('subdeps').sort(key=lambda subdep: int(subdep[0].get('subdep').sub_dependency) if subdep[0].get('subdep').sub_dependency else 99999999)

        return res

    def get_move_lines(self, account, period, posted_only):
        return self.env['account.move.line'].search([
            ('account_id', '=', account.id),
            ('dependency_id', '=', self.id),
            ('date', '>=', period[0]),
            ('date', '<=', period[1]),
            ('parent_state', '=', 'posted') if posted_only else ('parent_state', '!=', 'cancel')
        ])



    @api.model
    def get_dependencies_for_account(self, account, move_line_domain):
        '''
        (New) Returns list of all of the dependency accounts that have had 
        account.move.line entries. Will return empty list if the account should
        not be grouped by dependencies
        :param move_line_domain An extra domain that can be added to the search
        for account.move.lines. Typically this param can restrict the move lines
        for certain periods
        '''
        
        if not account.dep_subdep_flag:
            return []
        domain = [('account_id', '=', account.id)] + move_line_domain

        deps = self.env['account.move.line'].search(domain).sorted(lambda aml: aml.dependency_id.description if aml.dependency_id else '0')\
            .mapped('dependency_id')
        
        empty_dep = self.env['account.move.line'].search(domain + [('dependency_id', '=', False)])
        if empty_dep:
            deps += self.env['dependency'].browse([None])
        return deps