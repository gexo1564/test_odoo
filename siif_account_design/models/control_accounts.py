# -*- coding: utf-8 -*-

import logging
from odoo import models, fields, api, _
from datetime import datetime
from odoo.exceptions import UserError, ValidationError

class PayrollPaymentParameters(models.Model):

    _name = 'control.accounts'

    account_group_id = fields.Many2one('account.group', 'Account Group')

    _sql_constraints = [('account_group_id_unique', 'unique(account_group_id)', _('Account Group: Must be unique.'))]

    @api.constrains('account_group_id')
    def check_account_group_id(self):
        if self.account_group_id and self.account_group_id.parent_id:
            raise UserError(_('You can only select one parent account group.'))

    def get_control_acounts(self):
        return [a.account_group_id.code_prefix for a in self.search([])]

    def name_get(self):
        return [(rec.id, "%s %s" % (rec.account_group_id.code_prefix,  rec.account_group_id.name)) for rec in self]