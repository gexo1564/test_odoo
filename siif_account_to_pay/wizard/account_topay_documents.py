from odoo import models, fields, api, _
from odoo.exceptions import RedirectWarning, UserError, ValidationError, AccessError
from datetime import datetime, timedelta, date
import time
import logging

class AccountTopayDocuments(models.TransientModel):
  _name = 'account.topay.documents'
  _description = 'Account Topay Rejection'

  #Función para aceptar el estado de la cuenta por pagar a Revisa
  def accept_documentation(self):
    info = self.env['account.topay'].search([('id','=',self.env.context['current_account_topay_id'])])
    info.sudo().write({'state':'revised'})


  def reject_documentation(self):
    return {
      'name': _('Reject Motivos'),
      'type': 'ir.actions.act_window',
      'res_model': 'account.topay.rejection',
      'view_mode': 'form',
      'view_type': 'form',
      'context': {
        'account_topay_id': self.env.context['current_account_topay_id'],
        'next_state': "rejected_by_documentation"
      },
      'target': 'new',
    }
    #Enviar correo sección
