{
    'name': 'SIIF Supplier Payment',
    'summary': "SIIF Supplier Payment is used to improve functional requirements to the source code through inherit",
    'version': '13.0.0.1.0',
    'category': 'Invoicing',
    'author': 'SIIF UNAM',
    'maintainer': 'SIIF UNAM',
    'website': '',
    'license': 'AGPL-3',
    'depends': ['jt_supplier_payment', 'siif_payroll_payment', 'siif_account_to_pay','siif_currency_purchase_req', 'jt_agreement'],
    'data': [
        'security/security.xml',
        
        "views/assets_backend.xml",
        "views/account_payment_cash.xml",
        "views/account_payment_view.xml",
        'views/invoice_view.xml',
        'views/res_users_dep_sd.xml',
        'views/upa_permissions_views.xml',
        'views/payment_requests_general.xml',
        'views/payment_requests_refunds.xml',
        'views/payment_requests_projects.xml',
        'views/account_payment_transfer.xml',
        'views/payment_parameters.xml',
        'views/account_returns_views.xml',
        'views/batch_management.xml',
        'views/resource_request.xml',
        'views/deposit_certificate_type_catalog.xml',
        'views/res_partner_bank_view.xml',
        'views/res_partner_bank_historical_view.xml',
        'wizard/payment_processing_result.xml',
        'wizard/reject_payment_request.xml',
        'wizard/select_return_lines.xml',
        'wizard/reject_account_return_view.xml',
        'wizard/bank_balance_manual.xml',
        'wizard/reject_wizard.xml',
        'wizard/cancel_wizard.xml',
        'wizard/load_bank_transfer_layout.xml',
        'report/travel_receipt.xml',
        'report/bank_reference_print.xml',
        'report/certificados.xml',
        'data/sequence.xml',
        'security/ir.model.access.csv'
    ],
    'qweb': [
        "static/src/xml/payment_processing_result.xml",
        "static/src/xml/layout_upload.xml",
    ],
    'demo': [

    ],
    'application': False,
    'installable': True,
    'auto_install': False,
}