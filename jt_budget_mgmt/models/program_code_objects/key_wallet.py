# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import datetime
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

from odoo.addons.siif_security.tools.utils import check_authorization, check_only_allowed_fields

class KeyWallet(models.Model):

    _name = 'key.wallet'
    _description = 'Key Wallet'
    _rec_name = 'wallet_password'

    wallet_password = fields.Char(string='Wallet password', size=4)
    wallet_password_name = fields.Text(string='Wallet password name')
    wallet_password_desc = fields.Text(string='Wallet password description')

    _sql_constraints = [('wallet_password', 'unique(wallet_password, start_date, end_date)',
                         'The wallet password must be unique.')]

    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')

    @api.constrains('start_date', 'end_date')
    def check_end_date(self):
        if not self.start_date:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("Configure la fecha inicial"))
            else:
                raise ValidationError(_("The start date of validity is required"))
        if self.end_date and self.end_date <= self.start_date:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("La fecha de fin de vigencia no puede ser menor o igual a la fecha de inicio de vigencia"))
            else:
                raise ValidationError(_("The effective end date cannot be less than or equal to the effective date"))

    @api.constrains('wallet_password')
    def _check_wallet_password(self):
        if not str(self.wallet_password).isnumeric():
            raise ValidationError(_('The wallet password must be numeric value'))

    def fill_zero(self, code):
        return str(code).zfill(4)

    @api.model
    def create(self, vals):
        if vals.get('wallet_password') and len(vals.get('wallet_password')) != 4:
            vals['wallet_password'] = self.fill_zero(vals.get('wallet_password'))
        return super(KeyWallet, self).create(vals)

    @check_authorization(
        'jt_budget_mgmt.group_budget_catalogs_admin_user',
        'jt_budget_mgmt.group_budget_admin_user'
    )
    @check_only_allowed_fields('wallet_password_name', 'wallet_password_desc', 'start_date', 'end_date')
    def write(self, vals):
        if vals.get('wallet_password') and len(vals.get('wallet_password')) != 4:
            vals['wallet_password'] = self.fill_zero(vals.get('wallet_password'))
        return super(KeyWallet, self).write(vals)

    @check_authorization(
        'jt_budget_mgmt.group_budget_catalogs_admin_user',
        'jt_budget_mgmt.group_budget_admin_user'
    )
    def unlink(self):
        for key in self:
            program_code = self.env['program.code'].search([('portfolio_id', '=', key.id)], limit=1)
            if program_code:
                raise ValidationError(_('You can not delete key portfolio item which are mapped with program code!'))
        return super(KeyWallet, self).unlink()

    def validate_wallet_key(self, wallet_key_string):
        if len(str(wallet_key_string)) > 3:
            wallet_key_str = str(wallet_key_string).zfill(4)
            if wallet_key_str.isnumeric():
                wallet_key = self.search(
                    [('wallet_password', '=', wallet_key_str)], limit=1)
                if wallet_key:
                    return wallet_key
        return False
    
    # if not return value is not valid #
    def get_validity_key_wallet(self, wallet_password, today = datetime.now()):
        query = """
                SELECT id
                FROM key_wallet
                WHERE 
                    (
                        (start_date IS NULL AND end_date IS NULL AND wallet_password = %(wallet_password)s) OR
                        (start_date IS NOT NULL AND end_date IS NULL AND wallet_password = %(wallet_password)s AND start_date <= %(today)s) OR
                        (start_date IS NOT NULL AND end_date IS NOT NULL AND wallet_password = %(wallet_password)s AND start_date <= %(today)s AND end_date >= %(today)s)
                    )
                """
        params = {'wallet_password' : str(wallet_password),
                  'today': today}
        self.env.cr.execute(query, params)
        key_wallet_validity = self.env.cr.fetchall()

        return key_wallet_validity