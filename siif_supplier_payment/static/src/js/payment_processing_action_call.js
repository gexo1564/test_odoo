odoo.define("siif_supplier_payment.ListController", function (require) {
  "use strict";

  let ListController = require("web.ListController")
  let rpc = require('web.rpc');
  let session = require('web.session')
  let core = require('web.core')

  ListController.include({
    action_def: function () {
      const self = this
      const user = session.uid;
      rpc.query({
        model: 'account.payment',
        method: 'payment_processing_action',
        args: [[user]],
      }).then(function (e) {
        if (e){
          self.do_action(e);
          window.location
        }
      });
    },
    action_load_layout: function () {
      const self = this
      const user = session.uid;
      rpc.query({
        model: 'account.payment',
        method: 'action_upload_layout',
        args: [[user]],
      }).then(function (e) {
        if (e){
          self.do_action(e);
          window.location
        }
      });
    },
    action_request_resources: function () {
      const self = this
      const user = session.uid;

      rpc.query({
        model: 'batch.management',
        method: 'request_resources',
        args: [[user], self.getSelectedIds()],
      }).then(function (e) {
        if (e){
          self.do_action(e);
          window.location
        }
      });
    },
    action_schedule_payment: function () {
      const self = this
      const user = session.uid;
      rpc.query({
        model: 'batch.management',
        method: 'schedule_payment',
        args: [[user], self.getSelectedIds()],
      }).then(function (e) {
        if (e){
          self.do_action(e);
          window.location
        }
      });
    },
    renderButtons: function($node) {
      this._super.apply(this, arguments)
      if (this.$buttons) {
        this.$buttons.find('.oe_action_payment_processing').click(this.proxy('action_def'))
        this.$buttons.find('.oe_action_upload_layout').click(this.proxy('action_load_layout'))
        this.$buttons.find('.oe_action_request_resources').click(this.proxy('action_request_resources'))
        this.$buttons.find('.oe_action_schedule_payment').click(this.proxy('action_schedule_payment'))
      }
    },
  });
});
