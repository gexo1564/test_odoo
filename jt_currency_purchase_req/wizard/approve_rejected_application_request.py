from odoo import models, fields, api, _
from odoo.exceptions import UserError
from datetime import date
import logging

class AccountTopayDocuments(models.TransientModel):
	_name = 'application.request.approval'
	_description = 'Approval for Application Request'

	#Función para aceptar el estado de la cuenta por pagar a aprobar
	def approve_app_request(self):
		info = 	self.env['application.request'].search([('id','=',self.env.context['current_app_request_id'])])
		quote_sel = info.published_quotes.filtered(lambda quote: quote.selected_quote)
		if len(quote_sel)==1:
			new_transfer_req=self.set_transfer_request(info,quote_sel)
			info.write({'status':'approved','transfer_request':new_transfer_req})


		elif len(info.published_quotes.filtered(lambda quote: quote.selected_quote))==0:
			raise UserError(_('The request doesnt have a selected quote, check!'))
		else:
			raise UserError(_('The request have more than one quote. check!'))

	#Creación de solicitud de transferencia
	def set_transfer_request(self,app_request,quote_sel):
		request={
			'state':'draft',
			'desti_bank_account_id':app_request.account_journal.id,
			'desti_account':app_request.account_journal.bank_account_id.id,
			'currency_id':quote_sel.currency_id.id,
			'amount':quote_sel.withdrawal_amount,
			'prepared_by_user_id':self.env.user.id,
			'user_id':self.env.user.id,
			'trasnfer_request':'finances',
			'date_required':date.today(),
			'from_app_request':True,
        }
		return self.env['request.open.balance.finance'].create(request)




	#Motivos de rechazo
	def reject_app_request(self):	
		return {
			'name': _('Motivos de Rechazo'),
			'type': 'ir.actions.act_window',
			'res_model': 'application.request.rejection',
			'view_mode': 'form',
			'view_type': 'form',
			'views': [(False, 'form')],
			'context': {'app_request_info': self.env.context['current_app_request_id'],'next_state':'rejected'},
			'target': 'new',
		}

