# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import base64
from cmath import log
import re
from time import time
import encodings
import io
import logging
import math
from datetime import datetime, timedelta
from xlrd import open_workbook
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.safe_eval import safe_eval
import json

ADEQUACIES = 'adequacies'
ADEQUACIES_LINES = 'adequacies.lines'
CONTROL_ASSIGNED_AMOUNTS = 'control.assigned.amounts'
CONTROL_ASSIGNED_AMOUNTS_LINES = 'control.assigned.amounts.lines'
DATE_FORMAT = '%Y-%m-%d'
EXPENDITURE_BUDGET = 'expenditure.budget'
EXPENDITURE_BUDGET_LINE = 'expenditure.budget.line'
MODEL_IR_SEQUENCE = 'ir.sequence'
PROGRAM_CODE = 'program.code'

MSG_ERROR_TO_CREATE_PROGRAM_CODE = 'Error, al crear el código programatico %s'

class Adequacies(models.Model):
    _name = 'adequacies'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Adequacies'
    _rec_name = 'folio'

    # Function calculate total imported and manufal rows
    def _get_count(self):
        for record in self:
            record.record_number = len(record.adequacies_lines_ids)
            record.imported_record_number = len(
                record.adequacies_lines_ids.filtered(lambda l: l.imported == True))

    # Function to calculate total increase amount and total decrease amount
    def _compute_total_amounts(self):
        for adequacies in self:
            total_decreased = 0
            total_increased = 0
            for line in adequacies.adequacies_lines_ids:
                if line.line_type == 'decrease':
                    total_decreased += line.amount
                if line.line_type == 'increase':
                    total_increased += line.amount
            adequacies.total_decreased = float(total_decreased)
            adequacies.total_decreased = float(total_increased)
            
    @api.model
    def fields_get(self, fields=None, attributes=None):
        
        no_selectable_fields = ['__id', '_id', 'adequacies_lines_ids', 'is_send_request', 'pointer_row',  'is_from_project', 'message_main_attachment_id', 'cron_running', 'total_rows', 
                                'invoice_move_id',  'failed_row_ids', 'reason', 'filename', 'journal_id' , 'import_status',  'allow_upload', 'message_needaction', 'activity_ids', 
                                'activity_exception_decoration', 'failed_row_file', 'budget_file', 'move_line_ids', 'message_is_follower', 'adequacies_lines_ids', 'message_has_error',
                                'message_has_sms_error', 'activity_date_deadline', 'activity_summary', 'activity_type_id', 'message_channel_ids', 'message_follower_ids', 'message_partner_ids', 
                                'activity_user_id', 'success_row_ids', 'date_of_liquid_adu', 'create_date' ]
        
        no_sortable_field = [ 'cron_running', 'is_from_project', 'is_send_request', 'message_main_attachment_id', 'total_rows', 'pointer_row',
                             'invoice_move_id',  'allow_upload', 'fialed_row_filename', 'date_of_liquid_adu', 'create_date']

        res = super(Adequacies, self).fields_get(fields, attributes=attributes)
        
        for key, value in res.items():
            if key in no_selectable_fields:
                value.update({'selectable': False})
        
        for key, value in res.items():
            if key in no_sortable_field:
                value.update({'sortable': False})
       
                
        return res
        

    # Total increased and decreased fields
    total_decreased = fields.Float(
        string='Total Decreased Amount', compute="_compute_total_amounts")
    total_increased = fields.Float(
        string='Total Decreased Amount', compute="_compute_total_amounts")
    adequacy_name = fields.Text(string='Adequacy Name', readonly=True, tracking=True)
    fiscal_year = fields.Many2one("account.fiscal.year", string = "Fiscal Year", required=True)
    folio = fields.Char(string='Folio', states={
        'accepted': [('readonly', True)], 'rejected': [('readonly', True)]}, tracking=True)
    budget_id = fields.Many2one(EXPENDITURE_BUDGET, string='Budget', states={
        'accepted': [('readonly', True)], 'rejected': [('readonly', True)]}, tracking=True)
    reason = fields.Text(string='Reason for rejection')
    cron_running = fields.Boolean(string='Running CRON?')

    # Total imported or manual rows
    record_number = fields.Integer(
        string='Number of records', compute='_get_count')
    imported_record_number = fields.Integer(
        string='Number of records imported.', compute='_get_count')

    state = fields.Selection(
        [
            ('draft', 'Draft'),
            ('confirmed', 'Confirmed'),
            ('accepted', 'Accepted'),
            ('rejected', 'Rejected')
        ],
        default='draft', required=True, string='State', tracking=True
    )

    observation = fields.Text(string='Observation', tracking=True)
    adaptation_type = fields.Selection(
        [('compensated', 'Compensated Adjustments'), ('liquid', 'Liquid Adjustments')], default='compensated', states={
            'accepted': [('readonly', True)], 'rejected': [('readonly', True)]}, tracking=True)
    adequacies_lines_ids = fields.One2many(
        ADEQUACIES_LINES, 'adequacies_id', string='Adequacies Lines',
        states={'accepted': [('readonly', True)], 'rejected': [('readonly', True)]})
    move_line_ids = fields.One2many("account.move.line", 'adequacy_id', string="Journal Items")
    journal_id = fields.Many2one('account.journal', string="Daily")
    date_of_budget_affected = fields.Date("Date Of Budget Affected")
    date_of_liquid_adu = fields.Date("Date of Liquid Adjustments")
    is_dgpo_authorization = fields.Boolean("DGPo authorization of games",copy=False,default=False)

    origin_of_the_adequacy = fields.Selection(
        [('manual', 'Manual'), ('automatic', 'Automatic'),
        ], default='manual'
    )

    _sql_constraints = [
        ('folio_uniq', 'unique(folio)', 'The folio must be unique.')]


    @api.onchange('date_of_budget_affected')
    def _onchangue_date_of_budget_affected(self):
        last_fiscalyear_lock_date = self.get_last_fiscalyear_lock_date()
     
        if last_fiscalyear_lock_date:
            if self.date_of_budget_affected and (self.date_of_budget_affected <= last_fiscalyear_lock_date):
                raise ValidationError(_("Date of budget affected is less or equal to the last configured lock date %s.") % last_fiscalyear_lock_date)
        

    def get_last_fiscalyear_lock_date(self):
        last_fiscalyear_lock_date = None
        acld = self.env['account.change.lock.date'].search([])
        
        if acld:
            last_fiscalyear_lock_date = acld[-1].fiscalyear_lock_date
            
        return last_fiscalyear_lock_date
    
    # Insert in new fields increase_type, decrease_type
    @api.onchange('adequacies_lines_ids')
    def _onchangue_adequacies_lines_ids(self):
        data = ''
        line_vals = {}
        for data in self.adequacies_lines_ids:
            if data.line_type == 'increase':
                data.increase_type = data.amount
                data.decrease_type = 0
            elif data.line_type == 'decrease':
                data.decrease_type = data.amount
                data.increase_type = 0

            line_vals = {
                            'program': data.program.id,
                            'line_type': data.line_type,
                            'amount': data.amount,
                            'increase_type' : data.increase_type,
                            'decrease_type' : data.decrease_type,
                            'creation_type': data.creation_type,
                            'imported': data.imported,
                        }

        self.write({'adequacies_lines_ids': [(3, 0, line_vals)]})

    def write(self, vals):
        if vals.get('date_of_liquid_adu', None):
            vals['date_of_budget_affected'] = vals['date_of_liquid_adu']

        previous_program_code_ids = self.adequacies_lines_ids.program.ids
        result = super(Adequacies, self).write(vals)
        current_program_code_ids = self.adequacies_lines_ids.program.ids
        result_program_code_ids = list(set(previous_program_code_ids) - set(current_program_code_ids))

        if result_program_code_ids:
            maybe_to_delete = self.env[PROGRAM_CODE].search([('id', 'in', result_program_code_ids)])
            program_code_deleted_counter = 0
            for to_delete in maybe_to_delete:
                if to_delete.state == 'draft':
                    control_assigned_amount_to_delete = self.env[CONTROL_ASSIGNED_AMOUNTS_LINES].search([('program_code_id', '=', to_delete.id), ('new_adequacies_id', '=', self.id)])
                    to_delete.unlink()
                    self.env.cr.execute('DELETE FROM control_assigned_amounts_lines WHERE id IN %s', [control_assigned_amount_to_delete._ids])
                    program_code_deleted_counter += 1
            if program_code_deleted_counter > 0:
                control_assigned_amounts_decrement = self.env[CONTROL_ASSIGNED_AMOUNTS].search([('budget_id', '=', self.budget_id.id)])
                for control_assigned_amount in control_assigned_amounts_decrement:
                    control_assigned_amount.record_number -= program_code_deleted_counter
                    control_assigned_amount.total_rows -= program_code_deleted_counter
                    control_assigned_amount.success_rows -= program_code_deleted_counter
        return result

    @api.constrains('folio')
    def _check_folio(self):
        if not str(self.folio).isnumeric():
            if self.env.user.lang == 'es_MX':
                raise ValidationError('Folio Debe ser un valor numérico!')
            else:
                raise ValidationError('Folio Must be numeric value!')

    @api.constrains('date_of_budget_affected', 'date_of_liquid_adu', 'adaptation_type')
    def _check_date_of_budget_affected(self):
        """
            _check_date_of_budget_affected Valida que las fechas de las adecuaciones
            no sean mayores que la fecha actual. En caso de serlo lanza un ValidationError
        """
        if self.adaptation_type == 'compensated':
            adu_date = self.date_of_budget_affected.strftime(DATE_FORMAT)
            now_date = datetime.today().strftime(DATE_FORMAT)
            if adu_date > now_date:
                raise ValidationError('La fecha de la adecuación no puede ser mayor que la fecha actual')
        else:
            adu_date = self.date_of_liquid_adu.strftime(DATE_FORMAT)
            now_date = datetime.today().strftime(DATE_FORMAT)
            if adu_date > now_date:
                raise ValidationError('La fecha de la adecuación no puede ser mayor que la fecha actual')

    def _compute_failed_rows(self):
        for record in self:
            record.failed_rows = 0
            try:
                if re.match('.*exec.*',record.failed_row_ids) is not None:
                    raise ValueError('Exec inyection')
                data = safe_eval(record.failed_row_ids)
                record.failed_rows = len(data)

            except Exception as e:
                raise ValidationError(e)

    def _compute_success_rows(self):
        for record in self:
            record.success_rows = record.total_rows - record.failed_rows

    # Import process related fields
    allow_upload = fields.Boolean(string='Allow Update XLS File?')
    budget_file = fields.Binary(string='Uploaded File', states={'accepted': [
        ('readonly', True)], 'rejected': [('readonly', True)]})
    filename = fields.Char(string='File name')
    import_status = fields.Selection([
        ('draft', 'Draft'),
        ('in_progress', 'In Progress'),
        ('done', 'Completed')], default='draft', copy=False)
    failed_row_file = fields.Binary(string='Failed Rows File')
    fialed_row_filename = fields.Char(
        string='File name', default=lambda self: _("Failed_Rows.txt"))
    failed_rows = fields.Integer(
        string='Failed Rows', compute="_compute_failed_rows")
    success_rows = fields.Integer(
        string='Success Rows', compute="_compute_success_rows")
    success_row_ids = fields.Text(
        string='Success Row Ids', default="[]", copy=False)
    failed_row_ids = fields.Text(
        string='Failed Row Ids', default="[]", copy=False)
    pointer_row = fields.Integer(
        string='Current Pointer Row', default=1, copy=False)
    total_rows = fields.Integer(string="Total Rows", copy=False)

    @api.model
    def create(self,vals):
        if vals.get('date_of_liquid_adu', None):
            vals['date_of_budget_affected'] = vals['date_of_liquid_adu']
        name = self.env[MODEL_IR_SEQUENCE].next_by_code('adequacies.folio')
        vals.update({'folio':name})
        res = super(Adequacies,self).create(vals)

        if not vals.get('fiscal_year'):
            year = fields.Datetime.now().year
            fiscal_year_obj = self.env[EXPENDITURE_BUDGET].search([('id', '=', vals['budget_id'])])
            fiscal_year = fiscal_year_obj.fiscal_year
            res.fiscal_year = fiscal_year
        else:
            year_fiscal = self.env['account.fiscal.year'].search([('id', '=', vals['fiscal_year'])])
            year = year_fiscal.name
        fiscal_year = ''
        year_sequence = self.env[MODEL_IR_SEQUENCE]
        if year:
            existing_sequence_name = self.env[MODEL_IR_SEQUENCE].search([('name', 'like', f'Secuencia Adecuaciones de Egresos {year}')])            
            if existing_sequence_name:
                fiscal_year = existing_sequence_name[0].code
            else:
                new_sequence_name = year_sequence.create({
                    'name': f'Secuencia Adecuaciones de Egresos {year}',
                    'code': f'seq.adequacy.name.{year}',
                    'padding': 5,
                    'implementation': 'standard',
                })
                fiscal_year = new_sequence_name.code            
        
        seq = self.env[MODEL_IR_SEQUENCE].next_by_code(fiscal_year)
        res.adequacy_name = "ADEG" + "/" + str(year) + "/" + str(seq)

        # Validate last_fiscalyear_lock_date
        last_fiscalyear_lock_date = self.get_last_fiscalyear_lock_date()
        if last_fiscalyear_lock_date:
            date_of_budget_affected = datetime.strptime(vals['date_of_budget_affected'], '%Y-%m-%d').date()
            if date_of_budget_affected and (date_of_budget_affected <= last_fiscalyear_lock_date):
                raise ValidationError(_("Date of budget affected is less or equal to the last configured lock date %s.") % last_fiscalyear_lock_date)
                    
        return res

    def import_lines(self):
        return {
            'name': _('Import Adequacies'),
            'type': 'ir.actions.act_window',
            'res_model': 'import.adequacies.line',
            'view_mode': 'form',
            'view_type': 'form',
            'views': [(False, 'form')],
            'target': 'new',
        }

    def _month_to_quarter(self, month):
        return [1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4][month-1]

    def create_new_budget_line(self, program_code, amount):
        # Se obtiene el ID de la adecuación
        new_adequacies_id = self.id

        # Se obtiene las cadenas del codigo programatico para insertarlas en las
        # tablas expenditure.budget.line y control.assigned.amounts.lines
        codigo = program_code.program_code
        vals_program_code ={
            'year': codigo[0:4],
            'program': codigo[4:6],
            'subprogram': codigo[6:8],
            'dependency': codigo[8:11],
            'subdependency': codigo[11:13],
            'item': codigo[13:16],
            'dv': codigo[16:18],
            'origin_resource': codigo[18:20],
            'ai': codigo[20:25],
            'conversion_program': codigo[25:29],
            'departure_conversion': codigo[29:34],
            'expense_type': codigo[34:36],
            'location': codigo[36:38],
            'portfolio': codigo[38:42],
            'project_type': codigo[42:44],
            'project_number': codigo[44:50],
            'stage': codigo[50:52],
            'agreement_type' : codigo[52:54],
            'agreement_number' : codigo[54:60],
        }
        # Se valida si existe la linea de presupuesto
        query = """
            select id from expenditure_budget_line
            where budget_line_yearly_id is null and program_code_id = %s
        """
        self._cr.execute(query, (program_code.id,))
        res = self.env.cr.dictfetchall()
        budget_line = res[0]['id'] if res else None

        # Se crea en caso de no existir la lina de presupuesto anual
        if not budget_line:
            budget_line = self.env[EXPENDITURE_BUDGET_LINE].sudo().create({
                'expenditure_budget_id': self.budget_id.id,
                'program_code_id': program_code.id,
                'start_date': self.budget_id.from_date,
                'end_date': self.budget_id.to_date,
                'state': 'success',
                'is_create_from_adequacies': True,
                'new_adequacies_id': new_adequacies_id,
                **vals_program_code,
            })
            budget_line = budget_line.id

        # Se obtienen las estacionalidades trimestralidades del presupuesto actual
        control_assign_ids = self.env[CONTROL_ASSIGNED_AMOUNTS].search([
            ('success_line_ids','!=',False),
            ('budget_id','=',self.budget_id.id),
            ('state','=','validated')
        ])
        # Para cada estacionalidad trimestral se debe crear una linea en
        # expenditure.budget.line
        for control_assign in control_assign_ids:
            # Se obtiene las fechas de las estacionalidad de alguna de las lineas
            # en este caso de la primer linea
            first_line = control_assign.success_line_ids[0]
            # Fecha de inicio de la estacionalidad
            start_date = first_line.start_date
            # Fecha de fin de la estacionalidad
            end_date = first_line.end_date

            # Se establece el monto asignado inicialmente como 0
            assigned = 0

            # ? Se busca si existe la linea correspondiente a la estacionalidad
            budget_line_exist = self.env[EXPENDITURE_BUDGET_LINE].sudo().search(
                [
                    ('program_code_id', '=', program_code.id),
                    ('expenditure_budget_id', '=', self.budget_id.id),
                    ('start_date','=',start_date),
                    ('end_date','=',end_date),
                ], limit=1
            )
            # Si no existe se crea
            if not budget_line_exist:
                self.env[EXPENDITURE_BUDGET_LINE].sudo().create({
                    'expenditure_budget_id':        self.budget_id.id,
                    'program_code_id':              program_code.id,
                    'start_date':                   start_date,
                    'end_date':                     end_date,
                    'state':                        'success',
                    'is_create_from_adequacies':    True,
                    'new_adequacies_id':            new_adequacies_id,
                    'assigned':                    assigned,
                    'budget_line_yearly_id':        budget_line,
                    **vals_program_code,
                })

            # ? Se busca si existe la linea correspondiente a la estacionalidad dentro
            # ? del modelo control.assigned.amounts.lines
            control_line_exist = self.env[CONTROL_ASSIGNED_AMOUNTS_LINES].sudo().search(
                [
                    ('program_code_id', '=', program_code.id),
                    ('assigned_amount_id', '=', control_assign.id),
                ], limit=1
            )
            # ? En caso de no existir se debe crear
            if not control_line_exist:
                self.env[CONTROL_ASSIGNED_AMOUNTS_LINES].sudo().create({
                    'assigned_amount_id':           control_assign.id,
                    'program_code_id':              program_code.id,
                    'start_date':                   start_date,
                    'end_date':                     end_date,
                    'state':                        'success',
                    'budget_id' :                   self.budget_id.id,
                    'assigned':                    assigned,
                    #'available':                   assigned,
                    'is_create_from_adequacies':    True,
                    'new_adequacies_id' :           new_adequacies_id,
                    **vals_program_code,
                })
        # Se regresa la linea padre (registro de la anualidad)
        return budget_line

    def create_sql_program_code(self, program_vals):
        """
            create_sql_program_code crea la instruccion SQL INSERT para crear un
            código programático
        """
        # Creación de la search_key usando la combinación ids, la definición se
        # tomo del modelo program.code
        search_key = ("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s" % (
            program_vals['year'],
            program_vals['program_id'],
            program_vals['sub_program_id'],
            program_vals['dependency_id'],
            program_vals['sub_dependency_id'],
            program_vals['item_id'],
            program_vals['resource_origin_id'],
            program_vals['institutional_activity_id'],
            program_vals['budget_program_conversion_id'],
            program_vals['conversion_item_id'],
            program_vals['expense_type_id'],
            program_vals['location_id'],
            program_vals['portfolio_id'],
            program_vals['project_type_id'],
            program_vals['stage_id'],
            program_vals['agreement_type_id']
        ))
        program_code_query = """
            INSERT INTO program_code(
                budget_id, year, program_id, sub_program_id, dependency_id, sub_dependency_id, item_id,
                check_digit, resource_origin_id, institutional_activity_id, budget_program_conversion_id,
                conversion_item_id, expense_type_id, location_id, portfolio_id, project_type_id, stage_id,
                agreement_type_id, project_id,
                program_code, program_code_copy,
                search_key, state,
                conacyt_code,
                create_uid, write_uid, create_date, write_date)
            VALUES (%s, %s, %s, %s, %s, %s, %s,
                %s, %s, %s, %s,
                %s, %s, %s, %s, %s, %s,
                %s, %s,
                %s, %s,
                %s, %s,
                false,
                %s, %s, now(), now()
            )
            RETURNING id
        """
        return [program_code_query, (
            self.budget_id.id,
            program_vals['year'],
            program_vals['program_id'],
            program_vals['sub_program_id'],
            program_vals['dependency_id'],
            program_vals['sub_dependency_id'],
            program_vals['item_id'],
            program_vals['check_digit'],
            program_vals['resource_origin_id'],
            program_vals['institutional_activity_id'],
            program_vals['budget_program_conversion_id'],
            program_vals['conversion_item_id'],
            program_vals['expense_type_id'],
            program_vals['location_id'],
            program_vals['portfolio_id'],
            program_vals['project_type_id'],
            program_vals['stage_id'],
            program_vals['agreement_type_id'],
            program_vals['project_id'],
            program_vals['program_code'],
            program_vals['program_code'],
            search_key,
            program_vals['state'],
            self.env.user.id,
            self.env.user.id,
        )]

    def create_sql_budget_line(self, program_vals, start_date, end_date):
        """
            create_sql_budget_line crea la instruccion SQL INSERT para crear un
            las lineas de presupuesto, recibe los valores del código programático
            y las fechas de inicio y fin del periodo
        """
        budget_line_query = """
            INSERT INTO expenditure_budget_line(
                expenditure_budget_id, start_date, end_date, authorized, assigned, available,
                currency_id, state,
                year, program, subprogram, dependency, subdependency, item, dv, origin_resource, ai,
                conversion_program, departure_conversion, expense_type, location, portfolio, project_type,
                project_number, stage, agreement_type, agreement_number,
                exercise_type, is_create_from_adequacies, new_adequacies_id,
                create_uid, write_uid, create_date, write_date,
                program_code_id, budget_line_yearly_id
            ) VALUES (
                %s, %s, %s, 0.0, 0.0, 0.0,
                %s, 'success',
                %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,
                %s, %s, %s,
                %s, %s, now(), now(),
                %s, %s
            ) RETURNING id;
        """
        return [budget_line_query, (
            self.budget_id.id, start_date, end_date,
            self.env.company.currency_id.id,
            program_vals['year_str'],
            program_vals['program_str'],
            program_vals['subprogram_str'],
            program_vals['dependency_str'],
            program_vals['subdependency_str'],
            program_vals['item_str'],
            program_vals['dv_str'],
            program_vals['origin_resource_str'],
            program_vals['ai_str'],
            program_vals['conversion_program_str'],
            program_vals['departure_conversion_str'],
            program_vals['expense_type_str'],
            program_vals['location_str'],
            program_vals['portfolio_str'],
            program_vals['project_type_str'],
            program_vals['project_number_str'],
            program_vals['stage_str'],
            program_vals['agreement_type_str'],
            program_vals['agreement_number_str'],
            program_vals['exercise_type_str'], True, self.id,
            self.env.user.id,
            self.env.user.id,
        )]

    def create_sql_control_assigned(self, program_vals, assigned_amount_id, start_date, end_date):
        """
            create_sql_control_assigned crea la instruccion SQL INSERT para crear un
            las lineas de control, recibe los valores del código programático,
            las fechas de inicio y fin del periodo, y el id de la trimestralidad
        """
        control_assigned_query = """
            INSERT INTO control_assigned_amounts_lines(
                start_date, end_date, assigned, available, currency_id, assigned_amount_id,
                budget_id, state,
                year, program, subprogram, dependency, subdependency, item, dv, origin_resource, ai, conversion_program,
                departure_conversion, expense_type, location, portfolio, project_type, project_number,
                stage, agreement_type, agreement_number, exercise_type,
                is_create_from_adequacies, new_adequacies_id,
                create_uid, write_uid, create_date, write_date,
                program_code_id
            ) VALUES (
                %s, %s, 0.0, 0.0, %s, %s,
                %s, 'success',
                %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,
                %s, %s, now(), now(),
                %s
            ) RETURNING 1
        """
        return [control_assigned_query, (
            start_date, end_date, self.env.company.currency_id.id, assigned_amount_id,
            self.budget_id.id,
            program_vals['year_str'],
            program_vals['program_str'],
            program_vals['subprogram_str'],
            program_vals['dependency_str'],
            program_vals['subdependency_str'],
            program_vals['item_str'],
            program_vals['dv_str'],
            program_vals['origin_resource_str'],
            program_vals['ai_str'],
            program_vals['conversion_program_str'],
            program_vals['departure_conversion_str'],
            program_vals['expense_type_str'],
            program_vals['location_str'],
            program_vals['portfolio_str'],
            program_vals['project_type_str'],
            program_vals['project_number_str'],
            program_vals['stage_str'],
            program_vals['agreement_type_str'],
            program_vals['agreement_number_str'],
            program_vals['exercise_type_str'],
            True,
            self.id,
            self.env.user.id,
            self.env.user.id,
        )]

    def create_new_program_code(self, program_vals):
        """
            create_new_program_code crea las instrucciones SQL INSERT necesarias
            para la creación de un código programático:
            - program_code (1 registro)
            - lineas de presupuesto en expenditure_budget_line (5 registros)
            - lineas de las trimestralidades (4 registros)
            Y devuelve una lista con los 10 SQL necesarios
        """
        # Array para almacena la tupla de las consultas insert
        querys = []
        # Creación de Query para crear el nuevo código programático
        querys.append(self.create_sql_program_code(program_vals))
        # Creación de las lineas de presupuesto
        querys.append(self.create_sql_budget_line(program_vals,self.budget_id.from_date, self.budget_id.to_date))

        # Para cada estacionalidad trimestral se debe crear una linea en
        # expenditure.budget.line
        for control_assign in program_vals['control_assign_ids']:
            # Id de la estacionalidad
            assigned_amount_id = control_assign[0]
            # Fecha de inicio de la estacionalidad
            start_date = control_assign[1]
            # Fecha de fin de la estacionalidad
            end_date = control_assign[2]

            # Query para crear las lineas trimestrales
            querys.append(self.create_sql_budget_line(program_vals, start_date, end_date))
            # Query para crear las estacionalidades
            querys.append(self.create_sql_control_assigned(program_vals, assigned_amount_id, start_date, end_date))

        return querys

    def _validate_adecuation_type(self, par, type):
        # Definicion de partidas validas
        decrease = [
            '211', '212', '216', '218', '231', '235', '242', '243', '245', '248', '253', '411', '412',
            '213', '214', '215',
            '222', '223',
            '721',
            '232',
            '722',
            '251', '252', '258',
            '725',
            '431', '511', '512', '515', '516',
            '521', '523',
        ]
        increase = [
            '211','212','213','214','215','218','222','223','227','231',
                '232','233','335','241','242','243','244','245','248','249',
                '251','252','253','255','257','258','411','412','414','431',
                '511','512','515','516','521','523','531','622','623',
            '213', '214', '215',
            '222', '223',
            '721',
            '414',
            '722',
            '251', '252', '258',
            '725',
            '431', '511', '512', '515', '516',
            '521', '523',
        ]
        if type == 'increase':
            return par in increase
        return par in decrease

    def check_program_item_games(self, adequacies_lines_ids):
        # Si la adecuación tiene Autorización DGPo de Partida o se trata de una
        # adecuación liquida, no se realiza ninguna validación.
        if self.is_dgpo_authorization or self.adaptation_type != 'compensated':
            return
        # De lo contrario se realiza la validación:
        def get_type(line):
            # Función para ordenar las partidas por el tipo
            return line.get('type')
        # Se mapean las adecuaciones a una lista de diccionarios que contiene
        # la partida y el tipo de adecuación
        adequacies = []
        for line in adequacies_lines_ids:
            item_name = line.program and line.program.item_id and line.program.item_id.item or False
            if not self._validate_adecuation_type(item_name, line.line_type):
                type_error = "incremento" if line.line_type == "increase" else "decremento"
                raise ValidationError(_("No está permitido usar la partida %s como %s." % (item_name, type_error)))
            adequacies.append({'par': item_name, 'type': line.line_type})
        # Se realiza el ordenamiento de acuerdo al tipo de partida, primero
        # decremento y luego incremento.
        adequacies.sort(key=get_type)
        par_grupo = None
        par_error = None
        # Se evalua la primer adecuación y se ve en que grupo esta incluida.
        # Posteriormente de acuerdo a las reglas de negocio se valida que el resto
        # de lineas de la adecuación esten incluidas dentro de los grupos permitidos
        # caso contrario se registra la partida que no paso y se rompe la validación
        # En caso de que el primer partida no entre en ningún grupo significa que
        # no esta permitida.
        line = adequacies[0]
        if line['par'] in ('211', '212', '216', '218', '231', '235', '242', '243', '245', '248', '253', '411', '412'):
            for i in range (1, len(adequacies)):
                line = adequacies[i]
                if line['type'] == 'decrease':
                    if line['par'] not in ('211', '212', '216', '218', '231', '235', '242', '243', '245', '248', '253', '411', '412'):
                        par_grupo = adequacies[0]['par']
                        par_error = line['par']
                        break
                elif line['type'] == 'increase':
                    if line['par'] not in ('211','212','213','214','215','218','222','223','227','231',
                                        '232','233','335','241','242','243','244','245','248','249',
                                        '251','252','253','255','257','258','411','412','414','431',
                                        '511','512','515','516','521','523','531','622','623'):
                        par_grupo = adequacies[0]['par']
                        par_error = line['par']
                        break
        elif line['par'] in ('213', '214', '215'):
            for i in range (1, len(adequacies)):
                line = adequacies[i]
                if line['par'] not in ('213', '214', '215'):
                    par_grupo = adequacies[0]['par']
                    par_error = line['par']
                    break
        elif line['par'] in ('222', '223'):
            for i in range (1, len(adequacies)):
                line = adequacies[i]
                if line['par'] not in ('222', '223'):
                    par_grupo = adequacies[0]['par']
                    par_error = line['par']
                    break
        elif line['par'] == '721':
            for i in range (1, len(adequacies)):
                line = adequacies[i]
                if line['par'] != '721':
                    par_grupo = adequacies[0]['par']
                    par_error = line['par']
                    break
        elif line['par'] == '232':
            for i in range (1, len(adequacies)):
                line = adequacies[i]
                if line['par'] != '414':
                    par_grupo = adequacies[0]['par']
                    par_error = line['par']
                    break
        elif line['par'] == '414':
            for i in range (1, len(adequacies)):
                line = adequacies[i]
                if line['par'] != '232':
                    par_grupo = adequacies[0]['par']
                    par_error = line['par']
                    break
        elif line['par'] == '722':
            for i in range (1, len(adequacies)):
                line = adequacies[i]
                if line['par'] != '722':
                    par_grupo = adequacies[0]['par']
                    par_error = line['par']
                    break
        elif line['par'] in ('251', '252', '258'):
            for i in range (1, len(adequacies)):
                line = adequacies[i]
                if line['par'] not in ('251', '252', '258'):
                    par_grupo = adequacies[0]['par']
                    par_error = line['par']
                    break
        elif line['par'] == '725':
            for i in range (1, len(adequacies)):
                line = adequacies[i]
                if line['par'] != '725':
                    par_grupo = adequacies[0]['par']
                    par_error = line['par']
                    break
        elif line['par'] in ('431', '511', '512', '515', '516'):
            for i in range (1, len(adequacies)):
                line = adequacies[i]
                if line['par'] not in ('431', '511', '512', '515', '516'):
                    par_grupo = adequacies[0]['par']
                    par_error = line['par']
                    break
        elif line['par'] in ('521', '523'):
            for i in range (1, len(adequacies)):
                line = adequacies[i]
                if line['par'] not in ('521', '523'):
                    par_grupo = adequacies[0]['par']
                    par_error = line['par']
                    break
        if par_grupo and par_error:
            raise ValidationError(_("La combinación de partidas es incorrecta.\nNo se puede utilizar la partida %s con la partida %s.") % (par_grupo, par_error))


#     def check_program_item_games(self,item_name,line_type,from_import):
#         message = False
#         if not self.is_dgpo_authorization and item_name and line_type:
#             if self.adaptation_type=='liquid' and line_type=='decrease' and item_name in ('211','212','216','218','231','235','242','243','245','248','253','411','412'):
#                 if from_import:
#                     message = " Please check DGPo authorization of games to confirm item %s to %s" %(item_name,line_type)
#                 else:
#                     raise ValidationError(_("Please check DGPo authorization of games to confirm item %s to %s" %(item_name,line_type)))
# 
#             elif self.adaptation_type=='liquid' and line_type=='increase' and item_name in ('211','212','213','214','215','218','222','223','227','231','232',
#                                                                                             '233','235','241','242','243','244','245','248','249','251','252',
#                                                                                             '253','255','257','258','411','412','414','431',
#                                                                                             '511','512','515','516','521','523','531','622','623'
#                                                                                             ):
# 
#                 if from_import:
#                     message = " Please check DGPo authorization of games to confirm item %s to %s" %(item_name,line_type)
#                 else:
#                     raise ValidationError(_("Please check DGPo authorization of games to confirm item %s to %s" %(item_name,line_type)))
#             
#             elif self.adaptation_type=='compensated' and line_type=='increase' and item_name in ('211','212','213','214','215','218','222','223','227','231','232',
#                                                                                             '233','235','241','242','243','244','245','248','249','251','252',
#                                                                                             '253','255','257','258','411','412','414','431',
#                                                                                             '511','512','515','516','531','622','623'
#                                                                                             ):
#                 if from_import:
#                     message = " Please check DGPo authorization of games to confirm item %s to %s" %(item_name,line_type)
#                 else:
#                     raise ValidationError(_("Please check DGPo authorization of games to confirm item %s to %s" %(item_name,line_type)))
# 
#             elif self.adaptation_type=='compensated' and line_type=='decrease' and item_name in ('211','212','216','218','231','235','242','243','245','248','253','411','412'):
#                 if from_import:
#                     message = " Please check DGPo authorization of games to confirm item %s to %s" %(item_name,line_type)
#                 else:
#                     raise ValidationError(_("Please check DGPo authorization of games to confirm item %s to %s" %(item_name,line_type)))
#                 
#         return message

    def check_digit_from_codes(self, program, sub_program, dependency, sub_dependency, item):
        if program and sub_program and dependency and sub_dependency and item:
            combined_digits = '' + program + sub_program + dependency + sub_dependency + item

            odd = 0
            even = 0
            counter = 0
            for digit in combined_digits:
                counter += 1
                if counter % 2 == 0:
                    even += int(digit)
                else:
                    odd += int(digit)
            result_odd = odd * 7
            result_even = even * 3
            result_odd_even = result_odd + result_even
            vd = '0' + str(result_odd_even)[-1:]
            return vd
        else:
            return '00'


    def validate_file_lines(self, record_id=False, cron_id=False):
        if self.budget_file:
            data = base64.decodestring(self.budget_file)
            info = data.decode('utf-8')
            #logging.critical("####Archivo"+str(info))
            lines = info.splitlines()
            extension = self.filename.split(".")[-1]
            self.pointer_row = 1
            pointer = self.pointer_row

            # * Para optimizar el tiempo de las adecuaciones se mapean todos los
            # * catalogos de los elementos que integran los códigos programáticos
            # * a memoria en una estructura de diccionario con el fin de obtener
            # * una complejidad de acceso a datos O(1)

            # Catlogo que contiene las porciones de código programático que se van
            # a ignorar dado que se utilizan como comodín para la carga de adecuaciones
            exclude_program_code = self.env['exclude.program.code.adequacies'].search([])

            # Solo considera solo el año del presupuesto que se va afectar
            year_obj = self.env['year.configuration'].search_read(
                [('name', '=',self.budget_id.from_date.year)],
                fields=['id', 'name'],
                limit=1
            )
            date_of_budget_affected = self.date_of_budget_affected
            program_obj = self.env['program'].search_read(
                [
                '&',('program_key_id','!=',False),
                    '&', ('start_date', '<=', date_of_budget_affected),
                        '|', ('end_date', '=', False), ('end_date', '>=', date_of_budget_affected)

                ], 
                fields=['id', 'key_unam']
            )
            program_dict = {p['key_unam']: p['id'] for p in program_obj}

            item_obj = self.env['expenditure.item'].search_read(
                [
                '&', ('start_date', '<=', date_of_budget_affected),
                        '|', ('end_date', '=', False), ('end_date', '>=', date_of_budget_affected) 
                ], 
                fields=['id', 'item', 'exercise_type'])
            item_dict = {i['item']: i['id'] for i in item_obj}
            dv_obj = self.env['verifying.digit']

            dependency_obj = self.env['dependency'].search_read([
                '&', ('start_date', '<=', date_of_budget_affected),
                        '|', ('end_date', '=', False), ('end_date', '>=', date_of_budget_affected) 
                ], 
                fields=['id', 'dependency'])
            dependency_dict = {d['dependency']: d['id'] for d in dependency_obj}

            sub_dependency_obj = self.env['sub.dependency'].search_read(
                [
                '&', ('start_date', '<=', date_of_budget_affected),
                        '|', ('end_date', '=', False), ('end_date', '>=', date_of_budget_affected) 
                ],
                fields=['id', 'dependency_id', 'sub_dependency']
            )
            sub_dependency_dict = {(d['dependency_id'][0], d['sub_dependency']): d['id'] for d in sub_dependency_obj}

            sub_program_obj = self.env['sub.program'].search_read(
                [
                ('dependency_id', '!=', False),
                ('sub_dependency_id', '!=', False),
                ('unam_key_id', '!=', False),
                '&',
                ('start_date', '<=', date_of_budget_affected),
                '|',
                ('end_date', '=', False),
                ('end_date', '>=', date_of_budget_affected)
                ],
                fields=['id', 'dependency_id', 'sub_dependency_id', 'unam_key_id', 'sub_program']
            )
            sub_program_dict = {(s['unam_key_id'][0], s['sub_program'], s['dependency_id'][0], s['sub_dependency_id'][0]) : s['id'] for s in sub_program_obj}

            origin_obj = self.env['resource.origin'].search_read(
                [
                '&', ('start_date', '<=', date_of_budget_affected),
                        '|', ('end_date', '=', False), ('end_date', '>=', date_of_budget_affected) 
                ], fields=['id', 'key_origin'])
            origin_dict = {o['key_origin']: o['id'] for o in origin_obj}

            activity_obj = self.env['institutional.activity'].search_read(
                [
                '&', ('start_date', '<=', date_of_budget_affected),
                        '|', ('end_date', '=', False), ('end_date', '>=', date_of_budget_affected) 
                ], 
                fields=['id', 'number'])
            activity_dict = {a['number']: a['id'] for a in activity_obj}

            conpa_obj = self.env['departure.conversion'].search_read(
                [
                    '&', ('item_id', '!=', False),
                        '&', ('start_date', '<=', self.date_of_budget_affected),
                            '|', ('end_date', '=', False), ('end_date', '>=', self.date_of_budget_affected)
                ],
                fields=['id', 'item_id', 'federal_part']
            )
            conpa_dict = {(c['item_id'][0], c['federal_part']): c['id'] for c in conpa_obj}

            conpp_obj = self.env['budget.program.conversion'].search_read(
                [
                '&', ('start_date', '<=', date_of_budget_affected),
                        '|', ('end_date', '=', False), ('end_date', '>=', date_of_budget_affected) 
                ],
                fields=['id', 'program_key_id', 'conversion_key_id', 'shcp']
            )
            conpp_dict = {(c['program_key_id'][1], c['conversion_key_id'][1], c['shcp'][1]): c['id'] for c in conpp_obj}

            expense_type_obj = self.env['expense.type'].search_read(
                [
                '&', ('start_date', '<=', date_of_budget_affected),
                        '|', ('end_date', '=', False), ('end_date', '>=', date_of_budget_affected) 
                ], 
                fields=['id', 'key_expenditure_type'])
            expense_type_dict = {e['key_expenditure_type']: e['id'] for e in expense_type_obj}

            geo_location_obj = self.env['geographic.location'].search_read(
                [
                '&', ('start_date', '<=', date_of_budget_affected),
                        '|', ('end_date', '=', False), ('end_date', '>=', date_of_budget_affected) 
                ], 
                fields=['id', 'state_key'])
            geo_location_dict = {l['state_key']: l['id'] for l in geo_location_obj}

            wallet_obj = self.env['key.wallet'].search_read(
                [
                '&', ('start_date', '<=', date_of_budget_affected),
                        '|', ('end_date', '=', False), ('end_date', '>=', date_of_budget_affected) 
                ], 
                fields=['id', 'wallet_password'])
            wallet_dict = {w['wallet_password']: w['id'] for w in wallet_obj}

            project_obj = self.env['project.project'].search_read(
                [('number', '!=', False)],
                fields=['id', 'number', 'custom_project_type_id', 'custom_stage_id']
            )
            project_dict = {(p['number'], p['custom_project_type_id'][1], p['custom_stage_id'][1]): p['id'] for p in project_obj}

            project_type_obj = self.env['project.type'].search_read([], fields=['id', 'project_id'])
            project_type_dict = {p['project_id'][0]: p['id'] for p in project_type_obj}

            stage_obj = self.env['stage'].search_read([], fields=['id', 'project_id'])
            stage_dict = {s['project_id'][0]: s['id'] for s in stage_obj}

            agreement_obj = self.env['agreement.type'].search_read(
                [],
                fields=['id', 'project_id', 'agreement_type', 'number_agreement']
            )
            agreement_dict = {(a['project_id'][0], a['agreement_type'], a['number_agreement']): a['id'] for a in agreement_obj}

            control_assign_obj = self.env[CONTROL_ASSIGNED_AMOUNTS].search([
                ('success_line_ids','!=',False),
                ('budget_id','=',self.budget_id.id),
                ('state','=','validated')
            ])
            control_assign = [
                (c.id, c.success_line_ids[0].start_date, c.success_line_ids[0].end_date)
                for c in control_assign_obj
            ]
            program_code_obj = self.env[PROGRAM_CODE]
            # Lista que almacenará las instrucciones SQL que se ejecutaran al final
            # de la validación
            querys = []
            # Se agregan en un arreglo los códigos que serán creados mediante
            # una adecuación de incremento para no volver a crear sus instrucciones
            # sql si es que estos se utilizan más de una vez
            codes_for_create = []
            # Se realiza primero una validación en la que se crean todos los códigos
            # programaticos que no existen y cumplen con las reglas del negocio
            failed_row = ""
            pointer = self.pointer_row
            success_row_ids = []
            failed_row_ids = []
            list_line = []

            if self.adequacies_lines_ids:
                self.adequacies_lines_ids.unlink()
            if extension=='txt' or extension=='pap':
                for line_aux in lines:
                    if len(line_aux)>27:
                        folio=line_aux[0:19]

                        list_line.append(line_aux[0:80]+line_aux[141:153]+'decrease')
                        list_line.append(folio+line_aux[80:141]+line_aux[141:153]+'increase')
                lines=list_line

            for line in lines:
                list_result = []
                if len(line)>27:
                    list_result.append(line[19:23])
                    list_result.append(line[23:25])
                    list_result.append(line[25:27])
                    list_result.append(line[27:30])
                    list_result.append(line[30:32])
                    list_result.append(line[32:35])
                    list_result.append(line[35:37])
                    list_result.append(line[37:39])
                    list_result.append(line[39:44])
                    list_result.append(line[44:48])
                    list_result.append(line[48:53])
                    list_result.append(line[53:55])
                    list_result.append(line[55:57])
                    list_result.append(line[57:61])
                    list_result.append(line[61:63])
                    list_result.append(line[63:69])
                    list_result.append(line[69:71])
                    list_result.append(line[71:73])
                    list_result.append(line[73:79])
                    list_result.append(line[79:80])
                    list_result.append(float(line[80:90]+"."+line[90:92]))

                    if extension=='txt' or extension=='pap':
                        list_result.append(line[92:100])
                        typee=line[92:100]

                    elif extension=='aum':
                        typee='increase'
                        list_result.append(typee)
                    elif extension=='dis':
                        typee='decrease'
                        list_result.append(typee)
                    else:
                        failed_row += str(list_result) + \
                                _("------>> Invalid Type Format\n")
                        failed_row_ids.append(pointer)
                        continue
                    ####Empiezan las validaciones
                    program_code_str = "".join(list_result[0:19])
                    # Se agrega validación para descartar códigos programáticos que se
                    # como comodines para la creacion de codigos programaticos con saldo
                    # cero
                    exclude = False
                    for e in exclude_program_code:
                        init = e.initial_position - 1
                        end = init + len(e.code_slice)
                        if program_code_str[init:end] == e.code_slice:
                            exclude = True
                    if exclude:
                        success_row_ids.append(pointer)
                        continue
                    if program_code_str in codes_for_create:
                        success_row_ids.append(pointer)
                        continue
                    # Si el código no se encuentra dentro de los códigos a excluir,
                    # se realiza su busqueda en la base de datos
                    program_code = self.env[PROGRAM_CODE].sudo().search(
                        [('program_code', '=', program_code_str)],
                        limit=1
                    )
                    # Si la cadena del código programatico no existe en la BD
                    # y la adecuación es de tipo incremento, se
                    # procede a hacer la validación de cada uno de los elementos
                    # contra los valores de los catálogos mapeados a memoria
                    # para posteriormente crear el código programatico
                    if program_code and program_code.state == 'draft':
                        query = """
                            select a.folio
                            from expenditure_budget_line l, adequacies a
                            where a.id = l. new_adequacies_id
                                and l.program_code_id = %s
                                and a.id <> %s
                            limit 1
                        """
                        self.env.cr.execute(query, (program_code.id, self.id))
                        res = (self.env.cr.fetchone() or [None])[0]
                        if res:
                            failed_row += str(list_result) + \
                                        (_("------>> The programmatic code was created in the adequacie %s, but has not yet been validated\n") % res)
                            failed_row_ids.append(pointer)
                            continue
                    if not program_code and typee == 'increase':
                        # Validate year format
                        year_str = year_obj[0]['name']
                        year = year_obj[0]['id']
                        if year_obj[0]['name'] != list_result[0]:
                            failed_row += str(list_result) + \
                                        _("------>> Invalid Year Format\n")
                            failed_row_ids.append(pointer)
                            continue

                        # Validate Program(PR)
                        program_str = str(list_result[1]).zfill(2)
                        program = program_dict.get(program_str, None)
                        if not program:
                            failed_row += str(list_result) + \
                                        _("------>> Invalid Program(PR) Format, check the validity\n")
                            failed_row_ids.append(pointer)
                            continue

                        # Validate Dependency
                        dependency_str = str(list_result[3]).zfill(3)
                        dependency = dependency_dict.get(dependency_str, None)
                        if not dependency:
                            failed_row += str(list_result) + \
                                        _("------>> Invalid Dependency(DEP) Format, check the validity\n")
                            failed_row_ids.append(pointer)
                            continue

                        # Validate Sub-Dependency
                        sub_dependency_str = str(list_result[4]).zfill(2)
                        sub_dependency = sub_dependency_dict.get((dependency, sub_dependency_str), None)
                        if not sub_dependency:
                            failed_row += str(list_result) + \
                                        _("------>> Invalid Sub Dependency(SUB-DEP) Format, check the validity\n")
                            failed_row_ids.append(pointer)
                            continue

                        # Validate Sub-Program
                        sub_program_str = str(list_result[2]).zfill(2)
                        sub_program = sub_program_dict.get((program, sub_program_str, dependency, sub_dependency), None)
                        if not sub_program:
                            failed_row += str(list_result) + \
                                        _("------>> Invalid SubProgram(SP) Format, check the validity\n")
                            failed_row_ids.append(pointer)
                            continue

                        # Validate Item
                        item_str = str(list_result[5]).zfill(3)
                        item = item_dict.get(item_str, None)
                        if not item:
                            failed_row += str(list_result) + \
                                        _("------>> Invalid Expense Item(PAR) Format, check the validity\n")
                            failed_row_ids.append(pointer)
                            continue

                        # Validate DV
                        dv = list_result[6]
                        if not dv:
                            failed_row += str(list_result) + \
                                        _("------>> Digito Verificador is not added!\n")
                            failed_row_ids.append(pointer)
                            continue
                        dv_check = dv_obj.check_digit_from_values(
                            program_str,
                            sub_program_str,
                            dependency_str,
                            sub_dependency_str,
                            item_str
                        )
                        if dv != dv_check:
                            failed_row += str(list_result) + \
                                    _("------>> Digito Verificador is not matched! \n")
                            failed_row_ids.append(pointer)
                            continue

                        # Validate Origin Of Resource
                        origin_resource_str = str(list_result[7]).zfill(2)
                        origin_resource = origin_dict.get(origin_resource_str, None)
                        if not origin_resource:
                            failed_row += str(list_result) + \
                                        _("------>> Invalid Origin Of Resource(OR) Format, check the validity\n")
                            failed_row_ids.append(pointer)
                            continue

                        # Validation Institutional Activity Number
                        institutional_activity_str = str(list_result[8]).zfill(5)
                        institutional_activity = activity_dict.get(institutional_activity_str, None)
                        if not institutional_activity:
                            failed_row += str(list_result) + \
                                        _("------>> Invalid Institutional Activity Number(AI) Format, check the validity\n")
                            failed_row_ids.append(pointer)
                            continue

                        # Validation CONPA
                        conpa_str = str(list_result[10]).zfill(5)
                        conpa = conpa_dict.get((item, conpa_str), None)
                        if not conpa:
                            failed_row += str(list_result) + \
                                        _("------>> Invalid SHCP Games(CONPA) Format, check the validity\n")
                            failed_row_ids.append(pointer)
                            continue

                        # Validation CONPP
                        conpp_str = str(list_result[9]).zfill(4)
                        conpp = conpp_dict.get((list_result[1][0], conpa_str, conpp_str), None)
                        if not conpp:
                            failed_row += str(list_result) + \
                                        _("------>> Invalid Conversion Program SHCP(CONPP) Format, check the validity\n")
                            failed_row_ids.append(pointer)
                            continue

                        # Validation Expense Type
                        expense_type_str = str(list_result[11]).zfill(2)
                        expense_type = expense_type_dict.get(expense_type_str, None)
                        if not expense_type:
                            failed_row += str(list_result) + \
                                        _("------>> Invalid Expense Type(TG) Format, check the validity\n")
                            failed_row_ids.append(pointer)
                            continue

                        # Validation Geographic Location
                        geo_location_str = str(list_result[12]).zfill(2)
                        geo_location = geo_location_dict.get(geo_location_str, None)
                        if not geo_location:
                            failed_row += str(list_result) + \
                                        _("------>> Invalid Geographic Location (UG) Format, check the validity\n")
                            failed_row_ids.append(pointer)
                            continue

                        # Validation Wallet Key
                        wallet_key_str = str(list_result[13]).zfill(4)
                        wallet_key = wallet_dict.get(wallet_key_str, None)
                        if not wallet_key:
                            failed_row += str(list_result) + \
                                        _("------>> Invalid Wallet Key(CC) Format, check the validity\n")
                            failed_row_ids.append(pointer)
                            continue

                        # * Nuevas consultas
                        # Validate Project
                        project_type_custom_str = str(list_result[14]).zfill(2)
                        project_number_custom_str = str(list_result[15]).zfill(6)
                        custom_stage_str = str(list_result[16]).zfill(2)
                        project = project_dict.get((project_number_custom_str, project_type_custom_str, custom_stage_str), None)
                        if not project:
                            failed_row += str(list_result) + \
                                        _("------>> Invalid Project\n")
                            failed_row_ids.append(pointer)
                            continue

                        # Validate Project Type
                        project_type = project_type_dict.get(project, None)
                        if not project_type:
                            failed_row += str(list_result) + \
                                        _("------>> Invalid Project Type(TP) Format, check the validity\n")
                            failed_row_ids.append(pointer)
                            continue

                        # Validate Stage
                        stage = stage_dict.get(project, None)
                        if not stage:
                            failed_row += str(list_result) + \
                                        _("------>> Invalid Stage(E) Format, check the validity\n")
                            failed_row_ids.append(pointer)
                            continue

                        # Validate Agreement Type
                        agreement_type_str = str(list_result[17]).zfill(2)
                        agreement_number_str = str(list_result[18]).zfill(6)
                        agreement_type = agreement_dict.get((project, agreement_type_str, agreement_number_str), None)
                        if not agreement_type:
                            failed_row += str(list_result) + \
                                        _("------>> Invalid Agreement Type(TC) Format, check the validity\n")
                            failed_row_ids.append(pointer)
                            continue

                        # Excercise Type
                        exercise_type = str(list_result[19])

                        # Validaciones de catálogos compuestos #
                        failed_row, failed_row_ids = self.check_composite_catalogs_program_code(program_code_obj, list_result, failed_row, failed_row_ids, pointer)
       
                        # Si se llega hasta este punto significa que el código prográmatico
                        # es valido, por lo que se crean las instrucciones SQL INSERT
                        program_vals = {
                            'year': year,
                            'program_id': program,
                            'sub_program_id': sub_program,
                            'dependency_id': dependency,
                            'sub_dependency_id': sub_dependency,
                            'item_id': item,
                            'check_digit': dv,
                            'resource_origin_id': origin_resource,
                            'institutional_activity_id': institutional_activity,
                            'budget_program_conversion_id': conpp,
                            'conversion_item_id': conpa,
                            'expense_type_id': expense_type,
                            'location_id': geo_location,
                            'portfolio_id': wallet_key,
                            'project_type_id': project_type,
                            'stage_id': stage,
                            'agreement_type_id': agreement_type,
                            'state': 'draft',
                            'project_id' : project,
                            'program_code': program_code_str,
                            'year_str': year_str,
                            'program_str': program_str,
                            'subprogram_str': sub_program_str,
                            'dependency_str': dependency_str,
                            'subdependency_str': sub_dependency_str,
                            'item_str': item_str,
                            'dv_str': dv,
                            'origin_resource_str': origin_resource_str,
                            'ai_str': institutional_activity_str,
                            'conversion_program_str': conpp_str,
                            'departure_conversion_str': conpa_str,
                            'expense_type_str': expense_type_str,
                            'location_str': geo_location_str,
                            'portfolio_str': wallet_key_str,
                            'project_type_str': project_type_custom_str,
                            'project_number_str': project_number_custom_str,
                            'stage_str': custom_stage_str,
                            'agreement_type_str': agreement_type_str,
                            'agreement_number_str': agreement_number_str,
                            'exercise_type_str': exercise_type,
                            'control_assign_ids': control_assign
                        }
                        querys.extend(self.create_new_program_code(program_vals))
                        success_row_ids.append(pointer)
                        codes_for_create.append(program_code_str)
                    # En caso de que el código programático no existe y se trate de una
                    # adecuación de decremento, se notifica al usuario que no se
                    # puede crear el codigo programático
                    elif not program_code and typee == 'decrease':
                        failed_row += str(list_result) + \
                                    _("------>> Programmatic code does not exist and cannot be created in a decrement adequacie\n")
                        failed_row_ids.append(pointer)
                    # Caso contrario, el código programático existe y se valida el monto
                    else:
                        # NUEVAS VALIDACIONES DE CATALOGOS COMPUESTOS #
                        program_code_dicts = {'year': year_obj,
                                              'check_digit': dv_obj,
                                              'program_dict': program_dict,
                                              'sub_program_dict': sub_program_dict,
                                              'dependency_dict': dependency_dict,
                                              'sub_dependency_dict': sub_dependency_dict,
                                              'expenditure_item_dict': item_dict,
                                              'resource_origin_dict': origin_dict,
                                              'institutional_activity_dict': activity_dict,
                                              'conpp_dict': conpp_dict,
                                              'conpa_dict': conpa_dict,
                                              'expense_type_dict': expense_type_dict,
                                              'geographic_location_dict': geo_location_dict,
                                              'key_wallet_dict': wallet_dict,
                                              'project_dict': project_dict,
                                              'project_type_dict': project_type_dict,
                                              'stage_dict': stage_dict,
                                              'agreement_dict': agreement_dict
                                              }
                        # Validaciones de vigencia de cada modelo que conforma el codigo #
                        failed_row, failed_row_ids = self.check_validity_program_code_sections(list_result, failed_row, failed_row_ids, pointer, program_code_dicts)
                        # Validaciones de catálogos compuestos #
                        failed_row, failed_row_ids = self.check_composite_catalogs_program_code(program_code_obj, list_result, failed_row, failed_row_ids, pointer)
            
                        success_row_ids.append(pointer)
                        # Validation Amount
                        amount = 0
                        try:
                            amount = float(list_result[20])
                            if amount < 0:
                                failed_row += str(list_result) + \
                                            _("------>> Amount should be 0 or greater than 0\n")
                                failed_row_ids.append(pointer)
                                continue
                        except ValueError:
                            failed_row += str(list_result) + \
                                        _("------>> Invalid Amount Format")
                            failed_row_ids.append(pointer)
                            continue
                    pointer+=1
                 
            # Una vez que se recorren, validan y obtienen las instrucciones SQL
            # se lanzan las consultas a la base de datos, respetando el orden de creación
            # Y la asociación con los ids creados en pasos previos
            num_codigos_creados = 0
            q = 0
            while q < len(querys):
                # Creacion del codigo programatico
                self.env.cr.execute(querys[q][0], querys[q][1])
                program_code_id = self.env.cr.fetchone()[0]
                if not program_code_id:
                    raise ValidationError(MSG_ERROR_TO_CREATE_PROGRAM_CODE %(program_code_str))
                q += 1
                num_codigos_creados += 1
                # Creación de la linea padre
                self.env.cr.execute(querys[q][0], querys[q][1] + (program_code_id, None))
                budget_yearly_id = self.env.cr.fetchone()[0]
                if not program_code_id:
                    raise ValidationError(MSG_ERROR_TO_CREATE_PROGRAM_CODE %(program_code_str))
                q += 1
                for __ in control_assign:
                    # Creación de las linea de presupuesto
                    self.env.cr.execute(querys[q][0], querys[q][1] + (program_code_id, budget_yearly_id))
                    if not self.env.cr.fetchone()[0]:
                        raise ValidationError(MSG_ERROR_TO_CREATE_PROGRAM_CODE %(program_code_str))
                    q += 1
                    # Creación de las trimestralidades
                    self.env.cr.execute(querys[q][0], querys[q][1] + (program_code_id,))
                    if not self.env.cr.fetchone()[0]:
                        raise ValidationError(MSG_ERROR_TO_CREATE_PROGRAM_CODE %(program_code_str))
                    q += 1
            if num_codigos_creados > 0:
                # Se actualizan los valores del número de registros
                query = """
                    UPDATE expenditure_budget set success_lines_count = success_lines_count + %s where id = %s RETURNING 1
                """
                self.env.cr.execute(query, (num_codigos_creados*5, self.budget_id.id))
                if not self.env.cr.fetchone()[0]:
                    raise ValidationError("Error, al crear los códigos programáticos.")
                query = """
                    UPDATE control_assigned_amounts set
                    record_number = record_number + %s,
                    success_rows = success_rows + %s,
                    total_rows = total_rows + %s
                    where id = %s RETURNING 1
                """
                for c in control_assign:
                    # Id de la estacionalidad
                    self.env.cr.execute(query, (num_codigos_creados, num_codigos_creados, num_codigos_creados, c[0]))
                    if not self.env.cr.fetchone()[0]:
                        raise ValidationError("Error, al crear los códigos programáticos.")
            # Una vez creados los códigos programáticos se vuelven a recorrer las
            # lineas de la adecuación, esta vez para persistir dichas lineas en
            # la base de datos
            # querys ahora almacenará una lista con los INSERT para adequacies_lines,
            # por lo que se vuelve a establecer como una lista vacía
            querys = []
            execute = True
            for line in lines:
                list_result = []
                if len(line)>27:
                    list_result.append(line[19:23])
                    list_result.append(line[23:25])
                    list_result.append(line[25:27])
                    list_result.append(line[27:30])
                    list_result.append(line[30:32])
                    list_result.append(line[32:35])
                    list_result.append(line[35:37])
                    list_result.append(line[37:39])
                    list_result.append(line[39:44])
                    list_result.append(line[44:48])
                    list_result.append(line[48:53])
                    list_result.append(line[53:55])
                    list_result.append(line[55:57])
                    list_result.append(line[57:61])
                    list_result.append(line[61:63])
                    list_result.append(line[63:69])
                    list_result.append(line[69:71])
                    list_result.append(line[71:73])
                    list_result.append(line[73:79])
                    list_result.append(line[79:80])
                    list_result.append(float(line[80:90]+"."+line[90:92]))
                    
                    if extension=='txt' or extension=='pap':
                        list_result.append(line[92:100])
                        typee=line[92:100]
                    elif extension=='aum':
                        typee='increase'
                        list_result.append(typee)
                    elif extension=='dis':
                        typee='decrease'
                        list_result.append(typee)

                    program_code_str = "".join(list_result[0:19])
                    # Se repite la validación para ignorar los códigos programaticos
                    # que actuan como comodines
                    exclude = False
                    for e in exclude_program_code:
                        init = e.initial_position - 1
                        end = init + len(e.code_slice)
                        if program_code_str[init:end] == e.code_slice:
                            exclude = True
                    if exclude:
                        continue
                    # Se busca de nuevo el código programático, esta vez para obtener su
                    # id y poder asociarlo la linea de la adecuación
                    program_code = self.env[PROGRAM_CODE].sudo().search(
                        [('program_code', '=', program_code_str)],
                        limit=1
                    )
                    try:
                        # Si el código programatico sigue sin existir hasta este caso, se ignora
                        if not program_code:
                            continue
                        # Si el código programático existe y esta en borrador
                        # valida que se haya creado por la adecuación actual
                        # de lo contrario, lo ignora para que se genere el mensaje de error
                        if program_code.state == 'draft':
                            query = """
                                select new_adequacies_id from expenditure_budget_line l
                                where l.program_code_id = %s
                                limit 1
                            """
                            self.env.cr.execute(query, (program_code.id,))
                            res = (self.env.cr.fetchone() or [None])[0]
                            if res != self.id:
                                continue
                        amount = float(list_result[20])
                        amount = round(amount, 2)

                        if self._context.get('re_scan_failed'):
                            failed_row_ids_eval_refill = safe_eval(self.failed_row_ids)
                            failed_row_ids_eval_refill.remove(pointer)
                            self.write({'failed_row_ids': str(list(set(failed_row_ids_eval_refill)))})

                        increase_type = decrease_type = 0
                        if typee == 'increase':
                            increase_type = amount
                        elif typee == 'decrease':
                            decrease_type = amount
                        # Query para crear una linea de adecuación
                        adequacie_query = """
                            INSERT INTO adequacies_lines(
                                line_type, amount, creation_type, adequacies_id, imported,
                                program, increase_type, decrease_type,
                                create_uid, write_uid, create_date, write_date
                            ) VALUES (
                                %s, %s, %s, %s, %s,
                                %s, %s, %s,
                                %s, %s, now(), now()
                            ) RETURNING 1;
                        """
                        querys.append([adequacie_query, (
                            typee, amount, 'imported', self.id, True,
                            program_code.id, increase_type, decrease_type,
                            self.env.user.id, self.env.user.id,
                        )])
                    except:
                        execute = False
                        failed_row += str(list_result) + \
                                    "------>> Row Data Are Not Corrected or Validated Program Code Not Found!\r\n"
                        failed_row_ids.append(pointer)
            # Si no ocurrio ningún error, se lanzan los INSERT para crear las adecuaciones
            if execute:
                for query in querys:
                    self.env.cr.execute(query[0], query[1])
                    if not self.env.cr.fetchone()[0]:
                        raise ValidationError("Error al crear la adecuación")

            failed_row_ids_eval = safe_eval(self.failed_row_ids)
            success_row_ids_eval = safe_eval(self.success_row_ids)
            if len(success_row_ids) > 0:
                success_row_ids_eval.extend(success_row_ids)
            if len(failed_row_ids) > 0:
                failed_row_ids_eval = failed_row_ids
            vals = {
                'failed_row_ids': str(list(set(failed_row_ids_eval))),
                'success_row_ids': str(list(set(success_row_ids_eval))),
                'pointer_row': pointer,
            }

            failed_data = False
            if failed_row != "":
                # Crear un conjunto para eliminar duplicados
                failed_row_set = set(failed_row.split("\n"))
                # Convertir el conjunto nuevamente en una cadena
                cleaned_failed_row = "\n".join(failed_row_set)
                content = "\n"
                content += _("...................Failed Rows ") + \
                        str(datetime.today()) + "...............\r\n"
                content += str(cleaned_failed_row)
                failed_data = base64.b64encode(content.encode('utf-8'))
                vals['failed_row_file'] = failed_data
            if self.failed_rows==0:
                vals['import_status'] = 'done'

            if pointer == len(lines) and len(failed_row_ids_eval) > 0:
                self.write({'allow_upload': True})

            if self.failed_row_ids == 0 or len(failed_row_ids_eval) == 0:
                self.write({'allow_upload': False})

            if vals:
                self.write(vals)
            if self.failed_row_ids == 0 or len(failed_row_ids_eval) == 0:
                adequacies_lines_ids = self.env[ADEQUACIES_LINES].search([('adequacies_id', '=', self.id)])
                self.check_program_item_games(adequacies_lines_ids)
            # if len(failed_row_ids) == 0:
            #     return{
            #         'effect': {
            #             'fadeout': 'slow',
            #             'message': 'All rows are imported successfully!',
            #             'type': 'rainbow_man',
            #         }
            #     }
        # Volver a validar adecuaciones manuales #    
        else:
            self.validate_catalog_validity()

    def validate_and_add_budget_line(self, record_id=False, cron_id=False):
        if record_id:
            self = self.env['adequacies'].browse(int(record_id))
        extension = self.filename.split(".")[-1]  
        
        if self.budget_file and extension in ('aum', 'dis', 'txt', 'pap'):
            self.validate_file_lines()

        # Volver a validar adecuaciones manuales #    
        else:
            self.validate_catalog_validity()  

    def send_notification_msg(self, user, failed, successed):
        ch_obj = self.env['mail.channel']
        base_user = self.env.ref('base.user_root')
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        url = base_url + '/web#id=%s&view_type=form&model=adequacies' % (self.id)
        body = (_("Adequacies Validation Process is Completed for \
                    <a href='%s' target='new'>%s</a>") % (url, self.folio))
        body += (_("<ul><li>Total Successed Lines: %s </li>\
            <li>Total Failed Lines: %s </li></ul>") % (str(successed), str(failed)))
        if user:
            ch = ch_obj.sudo().search([('name', '=', str(base_user.name + ', ' + user.name)),
                                       ('channel_type', '=', 'chat')], limit=1)
            if not ch:
                ch = ch_obj.create({
                    'name': 'OdooBot, ' + user.name,
                    'public': 'private',
                    'channel_type': 'chat',
                    'channel_last_seen_partner_ids': [(0, 0, {'partner_id': user.partner_id.id,
                                                              'partner_email': user.partner_id.email}),
                                                      (0, 0, {'partner_id': base_user.partner_id.id,
                                                              'partner_email': base_user.partner_id.email})
                                                      ]
                })
            ch.message_post(attachment_ids=[], body=body, content_subtype='html',
                            message_type='comment', partner_ids=[], subtype='mail.mt_comment',
                            email_from=base_user.partner_id.email, author_id=base_user.partner_id.id)
        return True

    def remove_cron_records(self):
        crons = self.env['ir.cron'].sudo().search(
            [('model_id', '=', self.env.ref('jt_budget_mgmt.model_adequacies').id)])
        for cron in crons:
            if cron.adequacies_id and not cron.adequacies_id.cron_running:
                try:
                    cron.sudo().unlink()
                except Exception as e:
                    raise ValidationError(e)

    def validate_draft_lines(self):
        if self.budget_file:
            total_cron = 1
            msg = (_("Adequacies Validation Process Started at %s") % datetime.strftime(
                datetime.now(), DEFAULT_SERVER_DATETIME_FORMAT))
            self.env['mail.message'].create({'model': 'adequacies', 'res_id': self.id,
                                             'body': msg})
            if total_cron == 1:
                extension = self.filename.split(".")[-1]
                if extension == 'xls' or extension =='xlsx':
                    self.validate_and_add_budget_line()
                else:
                    self.validate_file_lines()
                msg = (_("Adequacies Validation Process Ended at %s") % datetime.strftime(
                    datetime.now(), DEFAULT_SERVER_DATETIME_FORMAT))
                self.env['mail.message'].create({'model': 'adequacies', 'res_id': self.id,
                                                 'body': msg})
            else:
                self.write({'cron_running': True})
                prev_cron_id = False
                for seq in range(1, total_cron + 1):
                    # Create CRON JOB
                    cron_name = str(self.folio).replace(' ', '') + "_" + str(datetime.now()).replace(' ', '')
                    nextcall = datetime.now()
                    nextcall = nextcall + timedelta(seconds=10)

                    cron_vals = {
                        'name': cron_name,
                        'state': 'code',
                        'nextcall': nextcall,
                        'nextcall_copy': nextcall,
                        'numbercall': -1,
                        'code': "model.validate_and_add_budget_line()",
                        'model_id': self.env.ref('jt_budget_mgmt.model_adequacies').id,
                        'user_id': self.env.user.id,
                        'adequacies_id': self.id,
                        'doall':True,
                    }

                    # Final process
                    cron = self.env['ir.cron'].sudo().create(cron_vals)
                    cron.write(
                        {'code': "model.validate_and_add_budget_line(" + str(self.id) + "," + str(cron.id) + ")"})
                    if prev_cron_id:
                        cron.write({'prev_cron_id': prev_cron_id, 'active': False})
                    prev_cron_id = cron.id

    def validate_data(self):
        def generate_message(program_code, available, requerido, ampliado):
            msg = ''
            if self.env.user.lang == 'es_MX':
                msg = "El código programático: %s tiene una insuficiencia presupuestal de $%s.\r\nEl monto total de las reducciones es de $%s" % (
                    program_code.program_code,
                    format(requerido - (available + ampliado), ",.2f"),
                    format(requerido, ",.2f"),
                )
                if ampliado > 0:
                    msg += ", el monto total de las ampliaciones es de $%s" % format(ampliado, ",.2f")
                msg += " y el monto disponible es de $%s" % format(available, ",.2f")
            else:
                msg = "Program Code: %s has a budget insufficiency of $%s.\r\nThe amount of the total reductions is $%s" % (
                    program_code.program_code,
                    format(requerido - (available + ampliado), ",.2f"),
                    format(requerido, ",.2f"),
                )
                if ampliado > 0:
                    msg += ", the total expanded amount is $%s" % format(ampliado, ",.2f")
                msg += " and the amount available is $%s" % format(available, ",.2f")
            return msg + ".\r\n"

        for adequacies in self:
            total_decreased = 0
            total_increased = 0
            if len(adequacies.adequacies_lines_ids.ids) == 0:
                if self.env.user.lang == 'es_MX':
                    raise ValidationError("Seleccione o importe líneas!")
                else:
                    raise ValidationError("Please select or import lines!")
            if adequacies.adaptation_type != 'compensated':
                line_types = adequacies.adequacies_lines_ids.mapped('line_type')
                line_types = list(set(line_types))
                if len(line_types) > 1:
                    raise ValidationError(_(
                        "In liquid adjustment, you can only increase or decrease amount of budget!"))
            code_list_decrese_msg = ''
            self.check_program_item_games(adequacies.adequacies_lines_ids)
            program_code_amounts = {}
            for line in adequacies.adequacies_lines_ids:
                #===================================================#
                #====== Added changes for OS-ODOO-010 ===============#
                # if adequacies.is_send_request:
                #     self.create_new_budget_line(line.program,line.amount)
                #===================================================#
                budget_lines_check = self.env[EXPENDITURE_BUDGET_LINE].sudo().search([
                    ('program_code_id', '=', line.program.id),
                    ('expenditure_budget_id', '=', self.budget_id.id)
                ])
                #====== Added changes for OS-ODOO-010 ===============#
                if not budget_lines_check:
                    self.create_new_budget_line(line.program,line.amount)
                    budget_lines_check = self.env[EXPENDITURE_BUDGET_LINE].sudo().search([
                        ('program_code_id', '=', line.program.id),
                        ('expenditure_budget_id', '=', self.budget_id.id)
                    ])

                #=========End ==================
                found = False
                for b_check in budget_lines_check:
                    if adequacies.adaptation_type != 'compensated' and adequacies.date_of_liquid_adu and \
                            b_check.start_date and b_check.end_date:
                        if adequacies.date_of_liquid_adu >= b_check.start_date and adequacies.date_of_liquid_adu <= b_check.end_date:
                            found = True
                    elif adequacies.adaptation_type == 'compensated' and adequacies.date_of_budget_affected \
                            and b_check.start_date and b_check.end_date:
                        if adequacies.date_of_budget_affected >= b_check.start_date and \
                                adequacies.date_of_budget_affected <= b_check.end_date:
                            found = True
                if not found:
                    raise ValidationError(_("%s Program code is not available in %s with selected quarter!") % \
                                            (line.program.program_code, adequacies.budget_id.name))

                if line.line_type == 'decrease':
                    (red, amp) = program_code_amounts.get(line.program, (0, 0))
                    program_code_amounts[line.program] = (red + line.amount, amp)
                    total_decreased += line.amount
                elif line.line_type == 'increase':
                    (red, amp) = program_code_amounts.get(line.program, (0, 0))
                    program_code_amounts[line.program] = (red, amp + line.amount)
                    total_increased += line.amount

            # Obtención del trimestre de afectación de acuerdo al tipo de adecuación
            if self.date_of_budget_affected and self.adaptation_type == 'compensated':
                b_month = self.date_of_budget_affected.month
            elif self.date_of_liquid_adu and self.adaptation_type == 'liquid':
                b_month = self.date_of_liquid_adu.month
            b_quarter = self._month_to_quarter(b_month)
            err = False
            # Validación del disponible de cada código programático
            for program_code, value in program_code_amounts.items():
                available = self.env[EXPENDITURE_BUDGET_LINE].get_available_by_program_code(program_code.id, b_quarter)
                reducciones, ampliaciones = value
                if available - reducciones + ampliaciones < 0:
                    err = True
                    code_list_decrese_msg += generate_message(program_code, available, reducciones, ampliaciones)
            if err:
                if self.env.user.lang == 'es_MX':
                    raise ValidationError(_("No es posible ejecutar la adecuación \r\n%s" % (code_list_decrese_msg)))
                else:
                    raise ValidationError(_("The amount is greater than the one assigned in the budget. \n Budget: %s \n%s" % (adequacies.budget_id.name,code_list_decrese_msg)))

            if adequacies.adaptation_type == 'compensated' and round(total_decreased,2) != round(total_increased,2):
                raise ValidationError(_(
                    "The total amount of the increases and the total amount of the decreases must be equal for compensated adjustments!"))

    def confirm(self):
        self.validate_catalog_validity()
        # Sino hay errores ya sea manual o importado
        if self.failed_rows == 0:
            self.validate_data()
            self.state = 'confirmed'

    def validate_catalog_validity(self):
        program_code_obj = self.env[PROGRAM_CODE]
        # Solo considera solo el año del presupuesto que se va afectar
        year_obj = self.env['year.configuration'].search_read(
            [('name', '=',self.budget_id.from_date.year)],
            fields=['id', 'name'],
            limit=1
        )
        date_of_budget_affected = self.date_of_budget_affected
        program_obj = self.env['program'].search_read(
            [
            '&',('program_key_id','!=',False),
                '&', ('start_date', '<=', date_of_budget_affected),
                    '|', ('end_date', '=', False), ('end_date', '>=', date_of_budget_affected)

            ], 
            fields=['id', 'key_unam']
        )
        program_dict = {p['key_unam']: p['id'] for p in program_obj}

        item_obj = self.env['expenditure.item'].search_read(
            [
            '&', ('start_date', '<=', date_of_budget_affected),
                    '|', ('end_date', '=', False), ('end_date', '>=', date_of_budget_affected) 
            ], 
            fields=['id', 'item', 'exercise_type'])
        item_dict = {i['item']: i['id'] for i in item_obj}
        dv_obj = self.env['verifying.digit']

        dependency_obj = self.env['dependency'].search_read([
            '&', ('start_date', '<=', date_of_budget_affected),
                    '|', ('end_date', '=', False), ('end_date', '>=', date_of_budget_affected) 
            ], 
            fields=['id', 'dependency'])
        dependency_dict = {d['dependency']: d['id'] for d in dependency_obj}

        sub_dependency_obj = self.env['sub.dependency'].search_read(
            [
            '&', ('start_date', '<=', date_of_budget_affected),
                    '|', ('end_date', '=', False), ('end_date', '>=', date_of_budget_affected) 
            ],
            fields=['id', 'dependency_id', 'sub_dependency']
        )
        sub_dependency_dict = {(d['dependency_id'][0], d['sub_dependency']): d['id'] for d in sub_dependency_obj}

        sub_program_obj = self.env['sub.program'].search_read(
            [
            ('dependency_id', '!=', False),
            ('sub_dependency_id', '!=', False),
            ('unam_key_id', '!=', False),
            '&',
            ('start_date', '<=', date_of_budget_affected),
            '|',
            ('end_date', '=', False),
            ('end_date', '>=', date_of_budget_affected)
            ],
            fields=['id', 'dependency_id', 'sub_dependency_id', 'unam_key_id', 'sub_program']
        )
        sub_program_dict = {(s['unam_key_id'][0], s['sub_program'], s['dependency_id'][0], s['sub_dependency_id'][0]) : s['id'] for s in sub_program_obj}

        origin_obj = self.env['resource.origin'].search_read(
            [
            '&', ('start_date', '<=', date_of_budget_affected),
                    '|', ('end_date', '=', False), ('end_date', '>=', date_of_budget_affected) 
            ], fields=['id', 'key_origin'])
        origin_dict = {o['key_origin']: o['id'] for o in origin_obj}

        activity_obj = self.env['institutional.activity'].search_read(
            [
            '&', ('start_date', '<=', date_of_budget_affected),
                    '|', ('end_date', '=', False), ('end_date', '>=', date_of_budget_affected) 
            ], 
            fields=['id', 'number'])
        activity_dict = {a['number']: a['id'] for a in activity_obj}

        conpa_obj = self.env['departure.conversion'].search_read(
            [
                '&', ('item_id', '!=', False),
                    '&', ('start_date', '<=', self.date_of_budget_affected),
                        '|', ('end_date', '=', False), ('end_date', '>=', self.date_of_budget_affected)
            ],
            fields=['id', 'item_id', 'federal_part']
        )
        conpa_dict = {(c['item_id'][0], c['federal_part']): c['id'] for c in conpa_obj}

        conpp_obj = self.env['budget.program.conversion'].search_read(
            [
            '&', ('start_date', '<=', date_of_budget_affected),
                    '|', ('end_date', '=', False), ('end_date', '>=', date_of_budget_affected) 
            ],
            fields=['id', 'program_key_id', 'conversion_key_id', 'shcp']
        )
        conpp_dict = {(c['program_key_id'][1], c['conversion_key_id'][1], c['shcp'][1]): c['id'] for c in conpp_obj}

        expense_type_obj = self.env['expense.type'].search_read(
            [
            '&', ('start_date', '<=', date_of_budget_affected),
                    '|', ('end_date', '=', False), ('end_date', '>=', date_of_budget_affected) 
            ], 
            fields=['id', 'key_expenditure_type'])
        expense_type_dict = {e['key_expenditure_type']: e['id'] for e in expense_type_obj}

        geo_location_obj = self.env['geographic.location'].search_read(
            [
            '&', ('start_date', '<=', date_of_budget_affected),
                    '|', ('end_date', '=', False), ('end_date', '>=', date_of_budget_affected) 
            ], 
            fields=['id', 'state_key'])
        geo_location_dict = {l['state_key']: l['id'] for l in geo_location_obj}

        wallet_obj = self.env['key.wallet'].search_read(
            [
            '&', ('start_date', '<=', date_of_budget_affected),
                    '|', ('end_date', '=', False), ('end_date', '>=', date_of_budget_affected) 
            ], 
            fields=['id', 'wallet_password'])
        wallet_dict = {w['wallet_password']: w['id'] for w in wallet_obj}
        project_obj = self.env['project.project'].search_read(
            [('number', '!=', False)],
            fields=['id', 'number', 'custom_project_type_id', 'custom_stage_id']
        )
        project_dict = {(p['number'], p['custom_project_type_id'][1], p['custom_stage_id'][1]): p['id'] for p in project_obj}

        project_type_obj = self.env['project.type'].search_read([], fields=['id', 'project_id'])
        project_type_dict = {p['project_id'][0]: p['id'] for p in project_type_obj}

        stage_obj = self.env['stage'].search_read([], fields=['id', 'project_id'])
        stage_dict = {s['project_id'][0]: s['id'] for s in stage_obj}

        agreement_obj = self.env['agreement.type'].search_read(
            [],
            fields=['id', 'project_id', 'agreement_type', 'number_agreement']
        )
        agreement_dict = {(a['project_id'][0], a['agreement_type'], a['number_agreement']): a['id'] for a in agreement_obj}

        program_code_dicts = {  'year': year_obj,
                                'check_digit': dv_obj,
                                'program_dict': program_dict,
                                'sub_program_dict': sub_program_dict,
                                'dependency_dict': dependency_dict,
                                'sub_dependency_dict': sub_dependency_dict,
                                'expenditure_item_dict': item_dict,
                                'resource_origin_dict': origin_dict,
                                'institutional_activity_dict': activity_dict,
                                'conpp_dict': conpp_dict,
                                'conpa_dict': conpa_dict,
                                'expense_type_dict': expense_type_dict,
                                'geographic_location_dict': geo_location_dict,
                                'key_wallet_dict': wallet_dict,
                                'project_dict': project_dict,
                                'project_type_dict': project_type_dict,
                                'stage_dict': stage_dict,
                                'agreement_dict': agreement_dict
                                }
        
        list_result = []
        failed_row_ids = []
        failed_row = ""
        exercise_key = ''
        pointer = 0 
        # SI hay lineas de adecuacion pero no por archivo importado
        if self.adequacies_lines_ids and not self.budget_file:
            for adequacie_line in self.adequacies_lines_ids:
                pointer = adequacie_line.id
                list_result = [
                                adequacie_line.year,
                                adequacie_line.program_id,
                                adequacie_line.subprogram,
                                adequacie_line.dependency,
                                adequacie_line.subdependency,
                                adequacie_line.item,
                                adequacie_line.dv,
                                adequacie_line.origin_resource,
                                adequacie_line.ai,
                                adequacie_line.conversion_program,
                                adequacie_line.departure_conversion,
                                adequacie_line.expense_type,
                                adequacie_line.location,
                                adequacie_line.portfolio,
                                adequacie_line.project_type,
                                adequacie_line.project_number,
                                adequacie_line.stage,
                                adequacie_line.agreement_type,
                                adequacie_line.agreement_number,
                                exercise_key,
                                adequacie_line.amount,
                                adequacie_line.line_type
                                ]
                # Validaciones de vigencia de cada modelo que conforma el codigo #
                failed_row, failed_row_ids = self.check_validity_program_code_sections(list_result, failed_row, failed_row_ids, pointer, program_code_dicts)
                # Validaciones de catálogos compuestos #
                failed_row, failed_row_ids = self.check_composite_catalogs_program_code(program_code_obj, list_result, failed_row, failed_row_ids, pointer)

                self.total_rows = len(self.adequacies_lines_ids)
                self.failed_row_ids = str(list(set(failed_row_ids)))
                if len(failed_row_ids) > 0:
                    # Si hay errores se crea el archivo de texto
                    self.create_failed_row_file(failed_row)
                
        
    def create_failed_row_file(self, failed_row):
        if failed_row != "":
            # Crear un conjunto para eliminar duplicados
            failed_row_set = set(failed_row.split("\n"))
            # Convertir el conjunto nuevamente en una cadena
            cleaned_failed_row = "\n".join(failed_row_set)
            content = "\n"
            content += _("...................Failed Rows ") + \
                    str(datetime.today()) + "...............\r\n"
            content += str(cleaned_failed_row)
            failed_data = base64.b64encode(content.encode('utf-8'))
            self.failed_row_file = failed_data

    
    @api.onchange('adaptation_type')
    def onchange_type(self):
        if self.adaptation_type == 'compensated':
            comp_adequacy_jour = self.env.ref('jt_conac.comp_adequacy_jour')
            if comp_adequacy_jour:
                self.journal_id = comp_adequacy_jour.id
        else:
            liq_adequacy_jour = self.env.ref('jt_conac.liq_adequacy_jour')
            if liq_adequacy_jour:
                self.journal_id = liq_adequacy_jour.id

    def set_new_program_state(self):
        for line in self.adequacies_lines_ids.filtered(lambda x:x.program and x.program.state=='draft'):
            line.program.state = 'validated'
        for line in self.adequacies_lines_ids.filtered(lambda x:x.program and not x.program.budget_id):
            line.program.budget_id = self.budget_id.id

    def accept(self, descripcion=None):
        # Se valida que el se cuente con presupuesto suficiente para la operación
        self.validate_data()

        # Se crean los diccionarios para acumular los incrementos y decrementos
        # totales de códigos
        program_code_inc_amounts = {}
        program_code_dec_amounts = {}

        # Se obtiene la fecha de afectación de la adecuación
        b_date = None
        if self.date_of_budget_affected and self.adaptation_type == 'compensated':
            b_date = self.date_of_budget_affected
        elif self.date_of_liquid_adu and self.adaptation_type == 'liquid':
            b_date = self.date_of_liquid_adu

        # Se iteran sobre la lista de adecuaciones y se aculmula el valor del
        # incremento o decremento en el arreglo
        for line in self.adequacies_lines_ids:
            if line.program:
                if line.line_type == 'decrease':
                    tmp = program_code_dec_amounts.get(line.program.id, 0)
                    program_code_dec_amounts[line.program.id] = tmp + line.amount
                elif line.line_type == 'increase':
                    tmp = program_code_inc_amounts.get(line.program.id, 0)
                    program_code_inc_amounts[line.program.id] = tmp + line.amount

        # Se ejecuta la modificación de disponibles, primero se hace las
        # adecuaciones de incremento, y luego las de decremento
        for program_code, value in program_code_inc_amounts.items():
            self.env[EXPENDITURE_BUDGET_LINE].update_adequacie('increase', program_code, b_date, value, self.id)
        for program_code, value in program_code_dec_amounts.items():
            self.env[EXPENDITURE_BUDGET_LINE].update_adequacie('decrease', program_code, b_date, value, self.id)

        # Creación de Asientos Contables
        amount = sum(self.adequacies_lines_ids.mapped('amount'))
        if self.journal_id and amount > 0:
            move_obj = self.env['account.move']
            journal = self.journal_id
            date = self.date_of_budget_affected
            user = self.env.user
            partner_id = user.partner_id.id
            company_id = user.company_id.id
            if not journal.default_debit_account_id or not journal.default_credit_account_id \
                    or not journal.conac_debit_account_id or not journal.conac_credit_account_id:
                if self.env.user.lang == 'es_MX':
                    raise ValidationError(_("Por favor configure la cuenta UNAM y CONAC en diario!"))
                else:
                    raise ValidationError(_("Please configure UNAM and CONAC account in journal!"))
            unam_move_val = {
                'ref': self.folio, 'adequacy_id': self.id, 'conac_move': True,
                'date': date, 'journal_id': journal.id, 'company_id': company_id
            }
            line_ids = []
            for line in self.adequacies_lines_ids:
                amount = line.amount
                if line.line_type == 'increase':
                    line_ids.append((0, 0, {
                        'account_id': journal.default_credit_account_id.id,
                        'coa_conac_id': journal.conac_credit_account_id.id,
                        'credit': amount, 'adequacy_id': self.id,
                        'partner_id': partner_id,
                        'program_code_id': line.program.id,
                        'dependency_id':line.program.dependency_id.id if journal.default_credit_account_id.dep_subdep_flag else False,
                        'sub_dependency_id': line.program.sub_dependency_id.id if journal.default_credit_account_id.dep_subdep_flag else False,
                        'name': descripcion,
                    }))
                    line_ids.append((0, 0, {
                        'account_id': journal.default_debit_account_id.id,
                        'coa_conac_id': journal.conac_debit_account_id.id,
                        'debit': amount, 'adequacy_id': self.id,
                        'partner_id': partner_id,
                        'program_code_id': line.program.id,
                        'dependency_id':line.program.dependency_id.id if journal.default_debit_account_id.dep_subdep_flag else False,
                        'sub_dependency_id': line.program.sub_dependency_id.id if journal.default_debit_account_id.dep_subdep_flag else False,
                        'name': descripcion,
                    }))
                else:
                    line_ids.append((0, 0, {
                        'account_id': journal.default_credit_account_id.id,
                        'coa_conac_id': journal.conac_credit_account_id.id,
                        'debit': amount, 'adequacy_id': self.id,
                        'partner_id': partner_id,
                        'program_code_id': line.program.id,
                        'dependency_id':line.program.dependency_id.id if journal.default_credit_account_id.dep_subdep_flag else False,
                        'sub_dependency_id': line.program.sub_dependency_id.id if journal.default_credit_account_id.dep_subdep_flag else False,
                        'name': descripcion,
                    }))
                    line_ids.append((0, 0, {
                        'account_id': journal.default_debit_account_id.id,
                        'coa_conac_id': journal.conac_debit_account_id.id,
                        'credit': amount, 'adequacy_id': self.id,
                        'partner_id': partner_id,
                        'program_code_id': line.program.id,
                        'dependency_id':line.program.dependency_id.id if journal.default_debit_account_id.dep_subdep_flag else False,
                        'sub_dependency_id': line.program.sub_dependency_id.id if journal.default_debit_account_id.dep_subdep_flag else False,
                        'name': descripcion,
                    }))
            unam_move_val['line_ids'] = line_ids

            if unam_move_val:
                unam_move = move_obj.create(unam_move_val)
                unam_move.action_post()

        self.state = 'accepted'
        self.set_new_program_state()

    def delete_new_budget_line(self):
        # Se obtienen los códigos en borrador, que se usaron en la adecuación
        # y que se crearon en la adecuación para ser borrados
        query = """
            select distinct program_code_id from expenditure_budget_line
            where program_code_id in (
                select distinct pc.id from adequacies_lines l, program_code pc
                where l.program = pc.id and pc.state = 'draft' and l.adequacies_id = %s
            ) and is_create_from_adequacies is True and new_adequacies_id = %s
        """
        self.env.cr.execute(query, (self.id, self.id))
        program_code_ids = tuple([res[0] for res in self.env.cr.fetchall()])
        if program_code_ids:
            # Borrado de las líneas de presupuesto
            query = """
                delete from expenditure_budget_line where program_code_id in %s
            """
            self.env.cr.execute(query, (program_code_ids,))
            # Borrado de las líneas de trimestralidades
            query = """
                delete from control_assigned_amounts_lines where program_code_id in %s
            """
            self.env.cr.execute(query, (program_code_ids,))
            # Borrado de los códigos programáticos
            query = "delete from program_code where id in %s"
            self.env.cr.execute(query, (program_code_ids,))

    def reject(self):
        for record in self:
            record.state = 'rejected'
            record.delete_new_budget_line()

    def unlink(self):
        for record in self:
            if record.state != 'draft':
                if self.env.user.lang == 'es_MX':
                    raise ValidationError(
                        'No se puede eliminar una Adecuación validada!')
                else:
                    raise ValidationError(
                        'You can not delete confirmed/rejected adjustments!')
            record.delete_new_budget_line()


    def check_validity_program_code_sections(self, program_codes_list, failed_row, failed_row_ids, pointer, program_code_dicts):       
        program_section = program_codes_list[1]
        sub_program_section = program_codes_list[2]
        dependency_section = program_codes_list[3]
        sub_dependency_section = program_codes_list[4]
        expenditure_item_section = program_codes_list[5]
        resource_origin_section = program_codes_list[7]
        institutional_activity_section = program_codes_list[8]
        conpp_section = program_codes_list[9]
        conpa_section = program_codes_list[10]
        expense_type_section = program_codes_list[11]
        geographic_location_section = program_codes_list[12]
        key_wallet_section = program_codes_list[13]

        # Validate year format
        if program_code_dicts.get('year')[0]['name'] != program_codes_list[0]:
            failed_row += str(program_codes_list) + \
                        _("------>> Invalid Year Format\n")
            failed_row_ids.append(pointer)

        # Validate Program
        program_str = str(program_section).zfill(2)
        program = program_code_dicts.get('program_dict').get(program_str, None)
        if not program:
            failed_row += str(program_codes_list) + \
                        _("------>> Invalid Program(PR) Format, check the validity\n")
            failed_row_ids.append(pointer)
            
        # Validate Dependency
        dependency_str = str(dependency_section).zfill(3)
        dependency = program_code_dicts.get('dependency_dict').get(dependency_str, None)
        if not dependency:
            failed_row += str(program_codes_list) + \
                        _("------>> Invalid Dependency(DEP) Format, check the validity\n")
            failed_row_ids.append(pointer)

        # Validate Sub-Dependency
        sub_dependency_str = str(sub_dependency_section).zfill(2)
        sub_dependency = program_code_dicts.get('sub_dependency_dict').get((dependency, sub_dependency_str), None)
        if not sub_dependency:
            failed_row += str(program_codes_list) + \
                        _("------>> Invalid Sub Dependency(SUB-DEP) Format, check the validity\n")
            failed_row_ids.append(pointer)

        # Validate Sub-Program
        sub_program_str = str(sub_program_section).zfill(2)
        sub_program = program_code_dicts.get('sub_program_dict').get((program, sub_program_str, dependency, sub_dependency), None)
        if not sub_program:
            failed_row += str(program_codes_list) + \
                        _("------>> Invalid SubProgram(SP) Format, check the validity\n")
            failed_row_ids.append(pointer)        

        # Validate Item
        item_str = str(expenditure_item_section).zfill(3)
        item = program_code_dicts.get('expenditure_item_dict').get(item_str, None)
        if not item:
            failed_row += str(program_codes_list) + \
                        _("------>> Invalid Expense Item(PAR) Format, check the validity\n")
            failed_row_ids.append(pointer)

        # Validate DV
        dv = program_codes_list[6]
        if not dv:
            failed_row += str(program_codes_list) + \
                        _("------>> Digito Verificador is not added!\n")
            failed_row_ids.append(pointer)
      
        dv_check = program_code_dicts.get('check_digit').check_digit_from_values(
            program_str,
            sub_program_str,
            dependency_str,
            sub_dependency_str,
            item_str
        )
        if dv != dv_check:
            failed_row += str(program_codes_list) + \
                    _("------>> Digito Verificador is not matched! \n")
            failed_row_ids.append(pointer)

        # Validate Origin Of Resource
        origin_resource_str = str(resource_origin_section).zfill(2)
        origin_resource = program_code_dicts.get('resource_origin_dict').get(origin_resource_str, None)
        if not origin_resource:
            failed_row += str(program_codes_list) + \
                        _("------>> Invalid Origin Of Resource(OR) Format, check the validity\n")
            failed_row_ids.append(pointer)

        # Validation Institutional Activity Number
        institutional_activity_str = str(institutional_activity_section).zfill(5)
        institutional_activity = program_code_dicts.get('institutional_activity_dict').get(institutional_activity_str, None)
        if not institutional_activity:
            failed_row += str(program_codes_list) + \
                        _("------>> Invalid Institutional Activity Number(AI) Format, check the validity\n")
            failed_row_ids.append(pointer)

        # Validation CONPA
        conpa_str = str(conpa_section).zfill(5)
        conpa = program_code_dicts.get('conpa_dict').get((item, conpa_str), None)
        if not conpa:
            failed_row += str(program_codes_list) + \
                        _("------>> Invalid SHCP Games(CONPA) Format, check the validity\n")
            failed_row_ids.append(pointer)

        # Validation CONPP
        conpp_str = str(conpp_section).zfill(4)
        conpp = program_code_dicts.get('conpp_dict').get((program_codes_list[1][0], conpa_str, conpp_str), None)
        if not conpp:
            failed_row += str(program_codes_list) + \
                        _("------>> Invalid Conversion Program SHCP(CONPP) Format, check the validity\n")
            failed_row_ids.append(pointer)

        # Validation Expense Type
        expense_type_str = str(expense_type_section).zfill(2)
        expense_type = program_code_dicts.get('expense_type_dict').get(expense_type_str, None)
        if not expense_type:
            failed_row += str(program_codes_list) + \
                        _("------>> Invalid Expense Type(TG) Format, check the validity\n")
            failed_row_ids.append(pointer)

        # Validation Geographic Location
        geo_location_str = str(geographic_location_section).zfill(2)
        geo_location = program_code_dicts.get('geographic_location_dict').get(geo_location_str, None)
        if not geo_location:
            failed_row += str(program_codes_list) + \
                        _("------>> Invalid Geographic Location (UG) Format, check the validity\n")
            failed_row_ids.append(pointer)

        # Validation Wallet Key
        wallet_key_str = str(key_wallet_section).zfill(4)
        wallet_key = program_code_dicts.get('key_wallet_dict').get(wallet_key_str, None)
        if not wallet_key:
            failed_row += str(program_codes_list) + \
                        _("------>> Invalid Wallet Key(CC) Format, check the validity\n")
            failed_row_ids.append(pointer)

        # Validate Project
        project_type_custom_str = str(program_codes_list[14]).zfill(2)
        project_number_custom_str = str(program_codes_list[15]).zfill(6)
        custom_stage_str = str(program_codes_list[16]).zfill(2)
        project = program_code_dicts.get('project_dict').get((project_number_custom_str, project_type_custom_str, custom_stage_str), None)
        if not project:
            failed_row += str(program_codes_list) + \
                        _("------>> Invalid Project\n")
            failed_row_ids.append(pointer)

        # Validate Project Type
        project_type = program_code_dicts.get('project_type_dict').get(project, None)
        if not project_type:
            failed_row += str(program_codes_list) + \
                        _("------>> Invalid Project Type(TP) Format, check the validity\n")
            failed_row_ids.append(pointer)

        # Validate Stage
        stage = program_code_dicts.get('stage_dict').get(project, None)
        if not stage:
            failed_row += str(program_codes_list) + \
                        _("------>> Invalid Stage(E) Format, check the validity\n")
            failed_row_ids.append(pointer)

        # Validate Agreement Type
        agreement_type_str = str(program_codes_list[17]).zfill(2)
        agreement_number_str = str(program_codes_list[18]).zfill(6)
        agreement_type = program_code_dicts.get('agreement_dict').get((project, agreement_type_str, agreement_number_str), None)
        if not agreement_type:
            failed_row += str(program_codes_list) + \
                        _("------>> Invalid Agreement Type(TC) Format, check the validity\n")
            failed_row_ids.append(pointer)    
       
       
        return failed_row, failed_row_ids
    

    def check_composite_catalogs_program_code(self, program_code_obj, program_codes_list, failed_row, failed_row_ids, pointer):
        
        resource_origin_validation = program_code_obj.validate_conversion_origin_resource(program_codes_list[5], program_codes_list[2], program_codes_list[7])
        if resource_origin_validation == False:
            failed_row += str(program_codes_list) + \
                                _("------>> The Resource origin invalid or not valid according to the OR-SP-PAR rule\n")
            failed_row_ids.append(pointer)

        institutional_activity_validation = program_code_obj.validate_conversion_activity_institutional(program_codes_list[1])

        if institutional_activity_validation == False:
            failed_row += str(program_codes_list) + \
                                _("------>> The Institutional activity invalid or not valid according to the AI-PR\n")
            failed_row_ids.append(pointer)
           
        conpp_validation = program_code_obj.validate_conpp_conversion(program_codes_list[1], program_codes_list[2], program_codes_list[3], program_codes_list[4], program_codes_list[5])
        if conpp_validation == False:
            failed_row += str(program_codes_list) + \
                                _("------>> CONPP invalid or not valid according to the CONPP-PR-SP-PAR\n")
            failed_row_ids.append(pointer)
        
        expense_type_validation = program_code_obj.validate_type_expense_conversion(program_codes_list[5])
        if expense_type_validation == False:
            failed_row += str(program_codes_list) + \
                                _("------>> The expense type invalid or not valid according to the TG-PAR\n")
            failed_row_ids.append(pointer)

        return failed_row, failed_row_ids


    @api.model
    def create_budget_adequacy(self, program_code_list, date=None, observation=None):
        lines = []
        errors = []

        if not date:
            date = datetime.now()
        else:
            date = datetime.strptime(date, "%Y-%m-%d")

        for program_code in program_code_list:
            # search for program code
            exercise = program_code[0:4]
            resource_origin = program_code[18:20]
            if str(exercise) != str(date.year):
                errors.append(str(program_code + _('(Adequacy year and program code year are not equal.)')))
                errors.append('\n')

            if resource_origin != '00':
                errors.append(str(program_code + _('(Budget Adequacy does not allow resource origin different to 00.)')))
                errors.append('\n')

            program_code_id = self.env[PROGRAM_CODE].search([('program_code', '=', program_code), ('state', '=', 'validated')])
            if program_code_id:
                errors.append(str(program_code + _('(Program code already exists.)')))
                errors.append('\n')

            try:
                # Program code rules
                vpcs_ok, vpcs_program_code = self.env[PROGRAM_CODE].validate_program_code_structure(program_code, date)
                if not vpcs_ok:
                    errors.append(str(program_code + _(f'({vpcs_program_code})')))
                    errors.append('\n')

                gvpc_ok, gvpc_vals = self.env['program.code'].get_vals_program_code(program_code, date, exercise)
                if not gvpc_ok:
                    errors.append(str(program_code + _(f'({gvpc_vals})')))
                    errors.append('\n')

            except Exception as e:
                errors.append(str(program_code + str(e)))
                errors.append('\n')

        if errors:
            return errors

        if not errors:
            journal_id = self.env.ref('jt_conac.comp_adequacy_jour')
            for program_code in program_code_list:
                program_code_id, err = self.env[PROGRAM_CODE].get_or_create_program_code_id(program_code, str(date.date()))
                lines.append((0, 0, {
                    'program': program_code_id,
                    'line_type': 'increase',
                    'amount': 0,
                    'increase_type': 0,
                    'creation_type': 'automatic',
                }))

        budget_id = self.env[EXPENDITURE_BUDGET].search([('fiscal_year.name', '=', date.year)], limit=1)
        # Adequacy creation
        adequacy = {
            'budget_id': budget_id.id,
            'adaptation_type': 'liquid',
            'state': 'confirmed',
            'journal_id': journal_id and journal_id.id or False,
            'date_of_liquid_adu': date,
            'adequacies_lines_ids': lines,
            'is_dgpo_authorization': True,
            'observation': observation,
            'origin_of_the_adequacy': 'automatic',
        }

        adequacy = self.env[ADEQUACIES].create(adequacy)
        adequacy.accept()

        return [adequacy.id]


class AdequaciesLines(models.Model):
    _name = ADEQUACIES_LINES
    _description = 'Adequacies Lines'
    _rec_name = ''

    line_type = fields.Selection(
        [('increase', 'Increase'), ('decrease', 'Decrease')], string='Type', default='increase')
    amount = fields.Float(string='Amount')
    # New fields required
    increase_type = fields.Float(string='Increase')
    decrease_type = fields.Float(string='Decrease')
    creation_type = fields.Selection([
        ('manual', 'Manual'),
        ('imported', 'Imported'),
        ('automatic', 'Automatic'),
        ],string='Creation type', default='manual'
    )
    adequacies_id = fields.Many2one('adequacies', string='Adequacies', ondelete="cascade", index=True)
    imported = fields.Boolean(default=False)
    program = fields.Many2one(PROGRAM_CODE, string='Program', domain="[('state', '=', 'validated'), ('budget_id', '=', parent.budget_id)]", index=True)

    _sql_constraints = [('uniq_program_per_adequacies_id', 'unique(program,id)',
                         'The program code must be unique per Adequacies')]
    state_related = fields.Selection(string="Estado", related="adequacies_id.state", store=True)
    dependency_id = fields.Many2one('dependency', string='Dependency', related="program.dependency_id")
    sub_dependency_id = fields.Many2one('sub.dependency', string='Sub-Dependency', related="program.sub_dependency_id")
    item_id = fields.Many2one('expenditure.item', string='Item', related="program.item_id", store=True)

    year = fields.Char(string='Year', related="program.year.name", store=True)
    program_id = fields.Char(string='Program', related="program.program_id.key_unam", store=True)
    subprogram = fields.Char(string='Sub-Program', related="program.sub_program_id.sub_program", store=True)
    dependency = fields.Char(string='Dependency', related="program.dependency_id.dependency", store=True)
    subdependency = fields.Char(string='Sub-Dependency', related="program.sub_dependency_id.sub_dependency", store=True)
    item = fields.Char(string='Expense Item', related="program.item_id.item", store=True)
    dv = fields.Char(string='Digit Verification', related="program.check_digit", store=True)
    origin_resource = fields.Char(string='Origin Resource', related="program.resource_origin_id.key_origin", store=True)
    ai = fields.Char(string='Institutional Activity', related="program.institutional_activity_id.number", store=True)
    conversion_program = fields.Char(string='Conversion Program', related="program.budget_program_conversion_id.shcp.name", store=True)
    departure_conversion = fields.Char(string='Federal Item', related="program.conversion_item_id.federal_part", store=True)
    expense_type = fields.Char(string='Expense Type', related="program.expense_type_id.key_expenditure_type", store=True)
    location = fields.Char(string='State Code', related="program.location_id.state_key", store=True)
    portfolio = fields.Char(string='Key portfolio', related="program.portfolio_id.wallet_password", store=True)
    project_type = fields.Char(string='Type of Project', related="program.project_type_id.project_type_identifier", store=True)
    project_number = fields.Char(string='Project Number', related="program.project_type_id.project_id.number", store=True)
    stage = fields.Char(string='Stage Identifier', related="program.stage_id.stage_identifier", store=True)
    agreement_type = fields.Char(string='Type of Agreement', related="program.agreement_type_id.agreement_type", store=True)
    agreement_number = fields.Char(string="Agreement number", related="program.agreement_type_id.number_agreement", store=True)
    departure_group_id = fields.Many2one('departure.group', string = 'Departure group', related="item_id.departure_group_id", store=True)

    def write(self,vals):
        result = super(AdequaciesLines,self).write(vals)
        if 'amount' in vals:
            for line in self:
                b_month = False
                if line.adequacies_id.date_of_budget_affected and line.adequacies_id.adaptation_type == 'compensated':
                    b_month = line.adequacies_id.date_of_budget_affected.month
                if line.adequacies_id.date_of_liquid_adu and line.adequacies_id.adaptation_type == 'liquid':
                    b_month = line.adequacies_id.date_of_liquid_adu.month
                
                control_assign_ids = self.env[CONTROL_ASSIGNED_AMOUNTS_LINES].search([('program_code_id','=',line.program.id),('assigned_amount_id.budget_id','=',line.adequacies_id.budget_id.id),('assigned_amount_id.state','=','validated')])
                for c_line in control_assign_ids:
                    start_date = c_line.start_date
                    b_s_month = start_date.month
                    
                    if b_month and b_month in (1, 2, 3) and b_s_month in (1, 2, 3):
                        c_line.assigned = line.amount
                        c_line.available = line.amount
                    elif b_month and b_month in (4, 5, 6) and b_s_month in (4, 5, 6):
                        c_line.assigned = line.amount
                        c_line.available = line.amount
                    elif b_month and b_month in (7, 8, 9) and b_s_month in (7, 8, 8):
                        c_line.assigned = line.amount
                        c_line.available = line.amount
                    elif b_month and b_month in (10, 11, 12) and b_s_month in (10, 11, 12):
                        c_line.assigned = line.amount
                        c_line.available = line.amount
                     
        return result
    
    def logged_user_domain(self):
        domain =  self.env['res.users'].get_user_dependencies_subdependencies_domain_budget() 
        
        return {
            'name': _('Expenditure Adequacy Lines'),
            "res_model": "adequacies.lines",
            "domain": domain,
            "type": "ir.actions.act_window",
            "view_mode": "tree,search",
            "views": [(self.env.ref('jt_budget_mgmt.adequacies_lines_tree_view').id, 'tree'),
                      (self.env.ref('jt_budget_mgmt.adequacies_lines_search_view').id, 'search')
                     ],
        }
    
    

class AdequaciesLinesReport(models.Model):
    _name = 'adequacies.lines.report'
    _description = 'Adequacies Lines Report'

    adequacies_id = fields.Many2one('adequacies', string='Adequacies', ondelete="cascade", index=True)
    program_code_id = fields.Many2one(PROGRAM_CODE, string='Program code', index=True)
    quarter = fields.Integer(string='Quarter')
    expansion_amount = fields.Monetary(string='Expansion Amount', help='Expansion Amount')
    reduction_amount = fields.Monetary(string='Reduction Amount', help='Reduction Amount')

    currency_id = fields.Many2one('res.currency', default=lambda self: self.env.company.currency_id)

    @api.model
    def init(self):
        self._cr.execute("""
            create index if not exists adequacies_lines_report_write_date_idx
            on adequacies_lines_report(write_date);
        """)
