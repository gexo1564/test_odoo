# -*- coding: utf-8 -*-
import logging
from odoo import models, fields, api, _
from datetime import datetime, timedelta
from odoo.exceptions import ValidationError
from lxml import etree

ACCOUNT_MOVE_MODEL = 'account.move'
class BankMovements(models.Model):
    _name = 'bank.movements'
    _description = 'Bank Movements'
    _rec_name = 'ref'

    # Banco
    income_bank_journal_id = fields.Many2one('account.journal', "Bank")
    # Cuenta
    income_bank_account = fields.Many2one(related="income_bank_journal_id.bank_account_id", string="Bank Account")
    # Fecha del movimiento
    movement_date = fields.Date('Movement date')
    # Monto
    amount_interest = fields.Float('Amount Interest')
    # Referencia Bancaria
    ref = fields.Char("Bank Reference")
    # Referencia Cliente
    customer_ref = fields.Char("Customer Reference")
    # Referencias complementarias
    reference_plugin_1 = fields.Char('Reference Plugin 1')
    reference_plugin_2 = fields.Char("Reference plugin 2")
    reference_plugin_3 = fields.Char("Reference plugin 3")
    # Descripción del movimiento
    description_of_the_movement = fields.Char('Description of movement')
    # Tipo de movimiento Bancario
    type_movement = fields.Selection([
        ('draft', 'Draft'),
        ('inprocess', 'Inprocess Movement'),
        ('unrecognized', 'Unrecognized Movement'),
        ('recognized', 'Recognized Movement'),
        ('partial_recognized', 'Partial recognized'),
        ('reclassified', 'Reclassified')
    ], "Movement state", default="draft")
    id_movto_bancario = fields.Char("ID bank movement")
    id_movto_ban = fields.Char("Key bank movement")
    charge_amount = fields.Float("Charge amount")
    folio =  fields.Char("Folio")
    bank_branch =  fields.Char("Bank branch")
    settlement_date =  fields.Date("Settlement date")
    invoice_id = fields.Many2one(ACCOUNT_MOVE_MODEL, "Related invoice/payment")
    comission_id = fields.Many2one('comission.profit', "Related comision")
    #move_line_ids = fields.One2many('account.move.line', 'bank_movement_id', string="Accounting Notes")
    donations_id = fields.Many2one('donations', "Related donation")
    amount_recognized = fields.Float('Amount recognized')
    amount_unrecognized = fields.Float('Amount unrecognized')
    recognition_request_id = fields.Many2one('request.income.recognition', "Related recognition request")
    currency_related = fields.Char(related="income_bank_journal_id.currency_id.name", string="Currency")
    me_amount = fields.Float("ME amount")
    type_of_changes = fields.Float("Type of changes")
    reclassified_to = fields.Many2one(ACCOUNT_MOVE_MODEL,"Reclassified to")
    payment_id = fields.Many2one('account.payment', "Related payment")
    charge_id = fields.Char("Charge id")
    conciliation_state = fields.Selection([('unreconciled', 'Unreconciled'), 
        ('reconciled', 'reconciled')], "Conciliation state", default="unreconciled")


    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        res = super(BankMovements, self).fields_view_get(
            view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])

        is_group_income_admin_user = self.env.user.has_group('jt_income.group_income_admin_user')
        if not is_group_income_admin_user:
            for node in doc.xpath("//" + view_type):
                node.set('export', '0')
                node.set('duplicate', '0')
                node.set('create', '0')

        res['arch'] = etree.tostring(doc)
        return res
                    

    def recognize(self):
        for record in self:
            # Si es un movimiento gasto (pagos)
            if record.charge_amount > 0:
                # Se busca si hay alguna solicitud que pago que haga match para validar
                # el pago
                ok = self.env['expense.recognition'].recognize_payment(self)
                # Si el pago fue validado se cambia el estado del movimiento bancario
                if ok:
                    record.type_movement = 'recognized'
                    continue
            invoice_list  = []
            ref_aux = ''

            if not record.reference_plugin_1 and not record.reference_plugin_2 and not record.reference_plugin_3 and not record.ref:
                if self.env.user.lang == 'es_MX':
                    raise ValidationError(_("El registro no tiene referencias para validar el movimiento"))
                else:
                    raise ValidationError(_("The registry has not references"))

            if record.reference_plugin_1:
                string_len = len(record.reference_plugin_1)

                if string_len>20:
                    if record.reference_plugin_1[0:2]=='CE' or record.reference_plugin_1[0:2]=='CC' or string_len == 28:
                        ref_aux = record.reference_plugin_1[8:string_len]
                    elif record.reference_plugin_1[0:2]=='CI':
                        ref_aux = record.reference_plugin_1[2:22]
                    else:
                        ref_aux = record.reference_plugin_1
                else:
                    ref_aux = record.reference_plugin_1
            else:
               ref_aux = record.reference_plugin_1 

            record.type_movement = 'unrecognized'

            move_id = self.env[ACCOUNT_MOVE_MODEL].search([('type_of_revenue_collection', '=', 'billing'),('ref', 'in', (ref_aux, record.ref, record.reference_plugin_2, record.reference_plugin_3)),('amount_total', '=', record.amount_interest), ('ref', 'not in', ('', False)), ('state','!=', 'cancel') ])

            donations_id = self.env['sd_base.deposit_slip.line'].search([('reference', 'in', (ref_aux, record.ref, record.reference_plugin_2, record.reference_plugin_3)),('amout_p_token', '=', record.amount_interest), ('reference', 'not in', ('', False))])
            payment_id = self.env['account.payment'].search([('bank_reference', 'in', (ref_aux, record.ref, record.reference_plugin_2, record.reference_plugin_3)), ('bank_reference', 'not in', ('', False)), ('amount', '=', record.amount_interest)])
            bank_ref_id =  self.env['bank.reference'].search([('reference', 'in', (ref_aux, record.ref, record.reference_plugin_2, record.reference_plugin_3)), ('reference', 'not in', ('', False)), ('amount', '=', record.amount_interest)])
            logging.critical(donations_id)
            logging.critical(payment_id)
            logging.critical(bank_ref_id)
            today = datetime.today().date()
            user = self.env.user
            partner_id = user.partner_id.id
            journal = record.income_bank_journal_id
            if move_id:
                if move_id.invoice_payment_state == 'not_paid':
                    if not move_id.income_bank_journal_id:
                        move_id.income_bank_journal_id = record.income_bank_journal_id.id
                    if not move_id.invoice_date_conciliation:
                        move_id.invoice_date_conciliation=record.movement_date
                    if move_id.state == 'posted':
                        invoice_list.append((4,move_id.id))
                        payment_register_id = self.env['account.payment.register'].create({'payment_method_id':1,'journal_id':move_id.income_bank_journal_id.id,'invoice_ids':invoice_list})
                        payment_register_id.create_payments()
                        record.type_movement = 'recognized'
                        record.invoice_id = move_id.id
                    elif move_id.state == 'draft':
                        move_id.action_post()
                        payment_register_id = self.env['account.payment.register'].create({'payment_method_id':1,'journal_id':move_id.income_bank_journal_id.id,'invoice_ids':invoice_list})
                        payment_register_id.create_payments()
                        record.type_movement = 'recognized'
                        record.invoice_id = move_id.id
                else:
                    record.type_movement = 'recognized'
                    record.invoice_id = move_id.id
            if donations_id:
                donations_id.recognized = True
                record.type_movement = 'recognized'
                record.donations_id = donations_id.donations_id.id
            if payment_id:
                record.type_movement = 'recognized'
                record.payment_id = payment_id.id
            elif bank_ref_id:
                #MOvimientos bancarios relacionados con
                #   Devoluciones de Fondo Fijo
                #   Cancelación de Fondo Fijo
                #   Devoluciones de Pago
                money_fund_req_id =  self.env['money.funds.request.returns'].search([('bank_ref', '=', bank_ref_id.id)])
                money_fund_cancel_id =  self.env['money.funds.cancel'].search([('bank_ref', '=', bank_ref_id.id)])
                account_return_id = self.env['account.returns'].search([('bank_ref', '=', bank_ref_id.id)])
                if money_fund_req_id:
                    
                    record.type_movement = 'recognized'
                    money_fund_req_id.state = 'recived_deposit'
                    #Asignar movimiento bancario a la solicitud de devoluciones
                    money_fund_req_id.bank_move_id  =  record
                    self.env['money.funds.request.returns']._generate_returns_account_moves(money_fund_req_id)
                elif money_fund_cancel_id:
                    if record.income_bank_journal_id==self.env['bank.reference.conf'].get_param('journal'):
                        record.type_movement = 'recognized'
                        money_fund_cancel_id.state = 'recived_deposit'
                        money_fund_cancel_id.money_fund.state='cancelled'
                        #Asignar movimiento bancario a la solicitud de cancelación
                        money_fund_cancel_id.bank_move_id  =  record
                        self.env['money.funds.cancel']._gen_account_moves(money_fund_cancel_id)
                    else:
                        record.type_movement = 'unrecognized'
                elif account_return_id:
                    record.type_movement = 'recognized'
                    account_return_id.return_paid(record)
                else:
                    record.type_movement = 'unrecognized'

            else:
                record.type_movement = 'unrecognized'
                '''account_id =  self.env['account.movements'].search([('account_id','!=', None)])
                unam_move_val = {'ref': record.ref,  'conac_move': False,
                                 'date': today, 'journal_id': journal.id, 'company_id': self.env.user.company_id.id,
                                 'line_ids': [(0, 0, {
                                     'account_id': journal.default_credit_account_id.id,
                                     'credit': record.amount_interest if record.charge_amount==0.0 else record.charge_amount,
                                     'partner_id': partner_id,
                                     'bank_movement_id': self.id,
                                     }),
                                     (0, 0, {
                                     'account_id': account_id['account_id'].id,
                                     'debit': record.amount_interest if record.charge_amount==0.0 else record.charge_amount,
                                     'partner_id': partner_id,
                                     'bank_movement_id': self.id,
                                     }),
                                 ]}
                move_obj = self.env['account.move']
                unam_move = move_obj.create(unam_move_val)
                unam_move.action_post()'''
        return {}
            
    def logged_user(self):
        is_group_income_admin_user = self.env.user.has_group('jt_income.group_income_admin_user')
        is_group_income_finance_user = self.env.user.has_group('jt_income.group_income_finance_user')
        is_group_income_project_coordinator_user = self.env.user.has_group('jt_income.group_income_project_coordinator_user')
        is_group_income_dependence_user = self.env.user.has_group('jt_income.group_income_dependence_user')       
        views = [(self.env.ref('jt_finance.bank_movements_tree').id, 'tree'), (self.env.ref('jt_finance.bank_movements_form').id, 'form')]
        domain = []

        if is_group_income_dependence_user:
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'deposit.slip.wizard',
                'name': 'Ficha de depósito',
                'view_mode': 'form',
                'view_type': 'form',
                'views': [(self.env.ref("jt_finance.deposit_slip_wizard_view_form").id, 'form')],
                'domain': [],
                'context': {},
                'target': 'new',
            }
        elif is_group_income_project_coordinator_user:
            account_journal_configuration = self.env["configuration.journals"].search([("is_active", "=", True),("coordinator_user", "=", True)])
            income_bank_journal_ids = account_journal_configuration.journal_id.ids
            domain = [('income_bank_journal_id.id', 'in', income_bank_journal_ids)]

        elif is_group_income_finance_user or is_group_income_admin_user:
            domain = []

        return {
            'name': 'Ingresos no Reconocidos',
            "res_model": "bank.movements",
            "domain": domain,
            "type": "ir.actions.act_window",
            "view_mode": "tree,form",
            "views": views,
        } 

  
                 
'''class AccountMoveLine(models.Model):

    _inherit = 'account.move.line'

    bank_movement_id = fields.Many2one('bank.movements','Bank movements')'''

class DependenceDeposit(models.TransientModel):
    _name = 'deposit.slip.wizard'
    _inherit = 'bank.movements'
 
    def action_income_recognition_window(self):
        # Ficha de deposito en wizard cuando es usuario dependencia
        views = [(self.env.ref('jt_finance.bank_movements_tree').id, 'tree'), (self.env.ref('jt_finance.bank_movements_form').id, 'form')]
        return {
            'name': 'Ingresos no Reconocidos',
            "res_model": "bank.movements",
            "domain": [('income_bank_journal_id.id', '=', self.income_bank_journal_id.id),
                       ('movement_date', '=', self.movement_date),
                       ('amount_interest', '=', self.amount_interest)
                       ],
            "type": "ir.actions.act_window",
            "view_mode": "tree,form",
            "views": views,
        }         



