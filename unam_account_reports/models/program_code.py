from odoo import fields, models


class ProgramCode(models.Model):
    _name = 'program.code'
    _inherit = ['program.code', 'excel.detailed.information']

    def _compute_excel_display_name(self):
        for record in self:
            record.excel_display_name = '%s - %s' % ('1', record.desc_item)
