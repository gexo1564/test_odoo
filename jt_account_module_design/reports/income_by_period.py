# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2023 UNAM.
#    Author: SIIF UNAM
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


import io
import base64
import json
from datetime import datetime
import pytz

from odoo import models, api, _
from odoo.tools.misc import xlsxwriter
from odoo.tools import config
from odoo.exceptions import ValidationError

DATE_FORMAT = '%Y-%m-%d'
CURRENCY_FORMAT = '#,##0.00'

class IncomeByPeriod(models.AbstractModel):
    _name = "income.by.period.report"
    _inherit = "account.report"
    _description = "Income by date range report"

    filter_date = {'mode': 'range', 'filter': 'this_month'}
    filter_all_entries = None
    filter_journals = None
    filter_unfold_all = None
    filter_cash_basis = None
    filter_hierarchy = None
    MAX_LINES = None
    filter_levels = None

    filter_thousands = True

    def _get_reports_buttons(self):
        return [
            {'name': _('Export (XLSX)'),
             'sequence': 1,
             'action': 'print_xlsx',
             'file_export_type': _('XLSX')
             },
        ]

    def print_xlsx(self, options, extra_data):
        options['is_xlsx_report'] = True
        return {
                'type': 'ir_actions_account_report_download',
                'data': {'model': self.env.context.get('model'),
                         'options': json.dumps(options),
                         'output_format': 'xlsx',
                         'financial_id': self.env.context.get('id'),
                         }
                }

    @api.model
    def _get_columns_name(self, options):        
        columns = [
            {'name': '', 'style': 'width:60%'},
            {'name': _('Remainder')},
            {'name': _('Adjustments')},
            {'name': _('Adjusted')},
        ]
        
        return columns

    @api.model
    def _get_lines(self, options, line_id=None):
        # Groups that have access to the report
        list_groups = [
            'jt_account_base.group_account_admin_user',
            'jt_account_base.group_account_super_admin_user',
            'jt_account_base.group_account_guest_user',
            'jt_account_base.group_account_approval_user']
        
        self.env['security.tools'].check_user_permissions(list_groups)     

        lines = []
        ext_inc = own_income = group_sum = 0

        group_obj = self.env['detailed.report.configuration']
        groups = group_obj.search([('conf_type', '=', 'income')], order = 'order')
        ln_obj = self.env['account.move.line']
        domain = [
                    (),
                    ('parent_state', '=', 'posted'),
                    ('date', '>=', options['date'].get('date_from')),
                    ('date', '<=', options['date'].get('date_to')),
                ]
        for group in groups:
            group_sum = 0
            lines.append({
                'id': 'conf_' + str(group.id),
                'name': group.name,
                'level': 1,
                })
            for account in group.accounts_ids:
                domain[0] = ('account_id', '=', account.id)
                balance = -sum(ln_obj.search(domain).mapped('balance'))
                group_sum += balance
                lines.append({
                'id': 'acc_' + str(account.id),
                'name': account.name,
                'columns': [
                    {'name': self.format_value(balance), 'class': 'number'},
                    {'name': self.format_value(0), 'class': 'number'},
                    {'name': self.format_value(balance), 'class': 'number'},
                ],
                'level': 2,
                })
            lines.append({
                'id': 'sum_' + str(group.id),
                'name': 'SUMA',
                'columns': [
                    {'name': self.format_value(group_sum), 'class': 'number'},
                    {'name': self.format_value(0), 'class': 'number'},
                    {'name': self.format_value(group_sum), 'class': 'number'},
                ],
                'level': 0,
                'class': 'text-right'
                })
            own_income += group_sum
            if group.name.casefold() == 'Ingresos Extraordinarios'.casefold():
                ext_inc = group_sum
            # Ingresos Extraordinarios and Fondos Patrimoniales sum line
            if group.name.casefold() == 'Fondos Patrimoniales'.casefold():
                group_sum += ext_inc
                lines.append({
                    'id': 'sum_ing_ext_fondos',
                    'name': _('EXT. INC. AND ENDOWMENT FUNDS SUM'), #SUMA ING. EXT. Y FONDOS PATRIMONIALES
                    'columns': [
                        {'name': self.format_value(group_sum), 'class': 'number'},
                        {'name': self.format_value(0), 'class': 'number'},
                        {'name': self.format_value(group_sum), 'class': 'number'},
                    ],
                    'level': 0,
                    'class': 'text-center'
                })

        lines.append({
            'id': 'sum_ing_ext_fondos',
            'name': _('OWN INCOME TOTAL'),
            'columns': [
                {'name': self.format_value(own_income), 'class': 'number'},
                {'name': self.format_value(0), 'class': 'number'},
                {'name': self.format_value(own_income), 'class': 'number'},
            ],
            'level': 0,
            'class': 'text-center'
        })
        
        try:
            group = group_obj.search([('conf_type', '=', 'subsidy')])[0]
        except:
            raise ValidationError(_('Add Federal Subsidy in Configuration'))
        
        group_sum = 0
        for account in group.accounts_ids:
            domain = [
                ('account_id', '=', account.id),
                ('parent_state', '=', 'posted'),
                ('date', '>=', options['date'].get('date_from')),
                ('date', '<=', options['date'].get('date_to')),
            ]
            balance = -sum(ln_obj.search(domain).mapped('balance'))
            group_sum += balance
            lines.append({
                'id': 'acc_' + str(account.id),
                'name': account.name,
                'columns': [
                    {'name': self.format_value(balance), 'class': 'number'},
                    {'name': self.format_value(0), 'class': 'number'},
                    {'name': self.format_value(balance), 'class': 'number'},
                ],
                'level': 2,
            })
        lines.append({
            'id': 'sum_' + str(group.id),
            'name': 'SUMA',
            'columns': [
                {'name': self.format_value(group_sum), 'class': 'number'},
                {'name': self.format_value(0), 'class': 'number'},
                {'name': self.format_value(group_sum), 'class': 'number'},
            ],
            'level': 0,
            'class': 'text-right'
        })

        total_income = group_sum + own_income
        lines.append({
            'id': 'sum_ing',
            'name': _('TOTAL INCOME'),
            'columns': [
                {'name': self.format_value(total_income), 'class': 'number'},
                {'name': self.format_value(0), 'class': 'number'},
                {'name': self.format_value(total_income), 'class': 'number'},
            ],
            'level': 0,
            'class': 'text-center'
        })

        lines.append({
            'id': 'sum_ing_alt',
            'name': _('Total Income'),
            'columns': [
                {'name': self.format_value(total_income), 'class': 'number'},
                {'name': self.format_value(0), 'class': 'number'},
                {'name': self.format_value(total_income), 'class': 'number'},
            ],
            'level': 2,
            'class': 'text-center'
        })
        
        exp_inv_ids = group_obj.search([('conf_type', 'in', ['expenses', 'investments'])]).accounts_ids.ids
        domain[0] = ('account_id', 'in', exp_inv_ids)
        exp_inv = sum(ln_obj.search(domain).mapped('balance'))
        lines.append({
            'id': 'sum_egr_inv',
            'name': _('Total Expenses and Investments'),
            'columns': [
                {'name': self.format_value(exp_inv), 'class': 'number'},
                {'name': self.format_value(0), 'class': 'number'},
                {'name': self.format_value(exp_inv), 'class': 'number'},
            ],
            'level': 2,
            'class': 'text-center'
        })

        residuary = total_income - exp_inv
        lines.append({
            'id': 'sum_rem',
            'name': _('Residuary'),
            'columns': [
                {'name': self.format_value(residuary), 'class': 'number'},
                {'name': self.format_value(0), 'class': 'number'},
                {'name': self.format_value(residuary), 'class': 'number'},
            ],
            'level': 2,
            'class': 'text-center'
        })
        
        return lines
        
    @api.model
    def _get_report_name(self):
        return _("Income by Period Report")

    def get_month_name(self, month):
        month_name = ''
        if month == 1:
            month_name = 'Enero'
        elif month == 2:
            month_name = 'Febrero'
        elif month == 3:
            month_name = 'Marzo'
        elif month == 4:
            month_name = 'Abril'
        elif month == 5:
            month_name = 'Mayo'
        elif month == 6:
            month_name = 'Junio'
        elif month == 7:
            month_name = 'Julio'
        elif month == 8:
            month_name = 'Agosto'
        elif month == 9:
            month_name = 'Septiembre'
        elif month == 10:
            month_name = 'Octubre'
        elif month == 11:
            month_name = 'Noviembre'
        elif month == 12:
            month_name = 'Diciembre'

        return month_name.upper()

    def get_xlsx(self, options, response=None):
        # Groups that have access to the report
        list_groups = [
            'jt_account_base.group_account_admin_user',
            'jt_account_base.group_account_super_admin_user',
            'jt_account_base.group_account_guest_user',
            'jt_account_base.group_account_approval_user',
        ]
        
        self.env['security.tools'].check_user_permissions(list_groups)     

        options['xlsx'] = True
        options['thousands'] = False

        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        sheet = workbook.add_worksheet(self._get_report_name())
        date_default_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2, 'num_format': 'yyyy-mm-dd'})
        date_default_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': 'yyyy-mm-dd'})
        default_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2})
        default_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'})
        title_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'bottom': 2, 'align': 'center'})
        super_col_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'align': 'center'})
        level_0_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 13, 'bottom': 6, 'font_color': '#666666', 'num_format': CURRENCY_FORMAT, 'align': 'right'})
        level_1_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 13, 'bottom': 1, 'font_color': '#666666', 'num_format': CURRENCY_FORMAT, 'align': 'center'})
        level_2_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 0})
        level_2_col1_total_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666'})
        level_2_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'num_format': CURRENCY_FORMAT})
        level_3_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 0})
        level_3_col1_total_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 0})
        level_3_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': CURRENCY_FORMAT})
        level_4_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 0})
        level_4_col1_total_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 0})
        level_4_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': CURRENCY_FORMAT})
        level_5_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 0})
        level_5_col1_total_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 0})
        level_5_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': CURRENCY_FORMAT})
        currect_date_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'align': 'right'})
        currect_date_style.set_border(0)
        super_col_style.set_border(0)
        # Set columns width
        sheet.set_column(0, 0, 25)
        sheet.set_column(1, 1, 20)
        sheet.set_column(2, 2, 20)
        sheet.set_column(3, 3, 20)
        
        y_offset = 0
        col = 0
        
        sheet.merge_range(y_offset, col, 7, col, '', super_col_style)
        if self.env.user and self.env.user.company_id and self.env.user.company_id.header_logo:
            filename = 'logo.png'
            image_data = io.BytesIO(base64.standard_b64decode(self.env.user.company_id.header_logo))
            sheet.insert_image(0,0, filename, {'image_data': image_data,'x_offset':60,'y_offset':15,'x_scale':0.6,'y_scale':0.6})
        
        col += 1
        x_offset = 1

        tz = pytz.timezone('America/Mexico_City')
        actual_date = datetime.now(tz)
        currect_time_msg = "Fecha y hora de impresión: "
        month_name = self.get_month_name(actual_date.month)
        currect_time_msg += str(actual_date.day) + " de " + month_name + " del " + str(actual_date.year) + str(actual_date.strftime(" %I:%M:%S %p"))
        user_msg = " Reporte generado por: " + self.env.user.name
        date_range = "DEL " + options['date'].get('date_from') + " AL " + options['date'].get('date_to')
        sheet.merge_range(y_offset, x_offset, y_offset, x_offset+2, '', super_col_style)
        sheet.merge_range(y_offset+1, x_offset, y_offset+1, x_offset+2, 'UNIVERSIDAD NACIONAL AUTÓNOMA DE MÉXICO', super_col_style)
        sheet.merge_range(y_offset+2, x_offset, y_offset+2, x_offset+2, 'PATRONATO UNIVERSITARIO', super_col_style)
        sheet.merge_range(y_offset+3, x_offset, y_offset+3, x_offset+2, 'TESORERÍA', super_col_style)
        sheet.merge_range(y_offset+4, x_offset, y_offset+4, x_offset+2, self._get_report_name().upper(), super_col_style)
        sheet.merge_range(y_offset+5, x_offset, y_offset+5, x_offset+2, date_range, super_col_style)
        sheet.merge_range(y_offset+6, x_offset, y_offset+6, x_offset+2, currect_time_msg, super_col_style)
        sheet.merge_range(y_offset+7, x_offset, y_offset+7, x_offset+2, user_msg, super_col_style)
        y_offset += 9
        total_columns = 0
        #import pdb; pdb.set_trace()
        if options['thousands']:
            thousands_msg = 'CIFRAS EN MILES'
        else:
            thousands_msg = 'CIFRAS EN PESOS'
        sheet.merge_range(y_offset, x_offset, y_offset, x_offset+2, thousands_msg, super_col_style)
        y_offset += 1
        for row in self.get_header(options):
            x = 0
            for column in row:
                colspan = column.get('colspan', 1)
                header_label = column.get('name', '')
                sheet.write(y_offset, x, header_label, title_style)
                x += colspan
            total_columns = x
            y_offset += 1

        
        ctx = self._set_context(options)
        ctx.update({'no_format':True, 'print_mode':True, 'prefetch_fields': False})

        # deactivating the prefetching saves ~35% on get_lines running time
        lines = self.with_context(ctx)._get_lines(options)
        if options.get('hierarchy'):
            lines = self._create_hierarchy(lines, options)
        if options.get('selected_column'):
            lines = self._sort_lines(lines, options)            
        
        # write all data rows
        for y in range(0, len(lines)):
            level = lines[y].get('level')
            if lines[y].get('caret_options'):
                style = level_3_style
                col1_style = level_3_col1_style
            elif level == 0:
                style = level_0_style
                col1_style = style
            elif level == 1:
                style = level_1_style
                col1_style = style
            elif level == 2:
                style = level_2_style
                col1_style = 'total' in lines[y].get('class', '').split(
                    ' ') and level_2_col1_total_style or level_2_col1_style
            elif level == 3:
                style = level_3_style
                col1_style = 'total' in lines[y].get('class', '').split(
                    ' ') and level_3_col1_total_style or level_3_col1_style
            elif level == 4:
                style = level_4_style
                col1_style = 'total' in lines[y].get('class', '').split(
                    ' ') and level_4_col1_total_style or level_4_col1_style
            elif level == 5:
                style = level_5_style
                col1_style = 'total' in lines[y].get('class', '').split(
                    ' ') and level_5_col1_total_style or level_5_col1_style
            else:
                style = default_style
                col1_style = default_col1_style
            # write the first column, with a specific style to manage the
            # indentation
            cell_type, cell_value = self._get_cell_type_value(lines[y])
            for x in range(0, total_columns):
                if x == 0:
                    sheet.write(y + y_offset, x, cell_value, col1_style)
                else:
                    sheet.write(y + y_offset, x, '', col1_style)
            # write all the remaining cells
            for x in range(1, len(lines[y].get('columns', [])) + 1):
                cell_type, cell_value = self._get_cell_type_value(
                    lines[y]['columns'][x - 1])
                if cell_type == 'date':
                    sheet.write_datetime(
                        y + y_offset, x, cell_value, date_default_style)
                else:
                    sheet.write(
                        y + y_offset, x, cell_value, style)
        workbook.close()
        output.seek(0)
        generated_file = output.read()
        output.close()
        return generated_file
    
    def _get_date_formated(self, date_number, options):
        date = options[date_number]
        date_obj = datetime.strptime(date, '%Y-%m-%d')
        date_result = "al "+date_obj.strftime('%d-%m-%Y')
        return date_result
