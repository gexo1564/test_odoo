{
    'name': 'UNAM: Financial Situation Report',
    'summary': '''
        Financial Situation Report
    ''',
    'description': '''
        Financial Situation Report
        - global html
        - excel export with annexes
        - wizard + settings
    ''',
    'license': 'OPL-1',
    'author': 'Odoo Ps',
    'website': 'https://www.odoo.com',
    'category': 'Development Services/Custom Development',
    'version': '1.0',
    'depends': [
        'account_reports',
        'account_accountant',
        'jt_budget_mgmt',
    ],
    'data': [
        # Security.
        'security/ir.model.access.csv',

        # Data.
        'data/annex.table.csv',
        'data/account_financial_report_data.xml',
        'data/account_financial_report_data_admin.xml',

        # Asssets
        'views/assets.xml',

        # Wizard.
        'wizard/account_configurator.xml',

        # Views.
        'views/account_annex_view.xml',
        'views/annex_table_views.xml',
        'views/unam_account_group_views.xml',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
    # 'pre_init_hook': 'pre_init_unam_account_reports',
    # 'post_init_hook': 'post_init_unam_account_reports',
}
