import logging
import itertools

from odoo import api, SUPERUSER_ID

_logger = logging.getLogger(__name__)

from . import models
from . import wizard


def pre_init_unam_account_reports(cr):
    env = api.Environment(cr, SUPERUSER_ID, {})
    aml_model = env['ir.model'].sudo().search([('model', '=', 'account.move.line')]).id
    account_model = env['ir.model'].sudo().search([('model', '=', 'account.account')]).id
    account_group_model = env['ir.model'].sudo().search([('model', '=', 'account.group')]).id 
    res_partner_model = env['ir.model'].sudo().search([('model', '=', 'res.partner')]).id

    _logger.info("Creating columns (PRE)")

    cr.execute("alter table account_move_line add column if not exists unam_account_id int")
    cr.execute("alter table account_move_line add column if not exists provider_id int")
    cr.execute("alter table account_account add column if not exists excel_display_name varchar")
    cr.execute("alter table account_group add column if not exists excel_display_name varchar")
    cr.execute("alter table res_partner add column if not exists excel_display_name varchar")

    _logger.info("Creating display names in ir_model_fields (PRE)")

    cr.execute("select id from ir_model_fields where name='excel_display_name' and model='account.account'")
    if not cr.fetchall():
        cr.execute("""insert into ir_model_fields 
            (model, model_id, name, field_description, ttype, store, state)
            values ('account.account', %s, 'excel_display_name', 'Excel Display Name', 'char', 't', 'manual')""" % (account_model))
    
    cr.execute("select id from ir_model_fields where name='excel_display_name' and model='account.group'")
    if not cr.fetchall():
        cr.execute("""insert into ir_model_fields 
            (model, model_id, name, field_description, ttype, store, state)
            values ('account.group', %s, 'excel_display_name', 'Excel Display Name', 'char', 't', 'manual')""" % (account_group_model))

    cr.execute("select id from ir_model_fields where name='excel_display_name' and model='res.partner'")
    if not cr.fetchall():
        cr.execute("""insert into ir_model_fields 
            (model, model_id, name, field_description, ttype, store, state)
            values ('res.partner', %s, 'excel_display_name', 'Excel Display Name', 'char', 't', 'manual')""" % (res_partner_model))
    

    _logger.info("Started setting display names for models (PRE)")

    cr.execute("update account_account set excel_display_name=code || '-' || name where name is not null")
    cr.execute("update account_group set excel_display_name=code_prefix || '-' || name where name is not null")
    cr.execute("update res_partner set excel_display_name=name where name is not null")

    _logger.info("Finished setting display names for models (PRE)")

    # _logger.info("Creating foreign keys (PRE)")

    cr.execute("select id from ir_model_fields where name='provider_id' and model='account.move.line'")
    if not cr.fetchall():
        cr.execute("""insert into ir_model_fields 
            (model, model_id, name, field_description, ttype, relation, store, related, state)
            values ('account.move.line', %s, 'provider_id', 'Provider', 'many2one', 'res.partner', 't', 'partner_id', 'manual')""" % (aml_model))
    
    cr.execute("select id from ir_model_fields where name='unam_account_id' and model='account.move.line'")
    if not cr.fetchall():
        cr.execute("""insert into ir_model_fields
            (model, model_id, name, field_description, ttype, relation, store, related, state)
            values ('account.move.line', %s, 'unam_account_id', 'UNAM Account', 'many2one', 'account.account', 't', 'account_id', 'manual')""" % (aml_model))

    _logger.info("Started updating related fields for account move lines (PRE)")

    cr.execute("update account_move_line as aml set unam_account_id=aml.account_id, provider_id=aml.partner_id where account_id is not null or partner_id is not null")

    _logger.info("Finished updating related fields for account move lines (PRE)")

def post_init_unam_account_reports(cr, registry):
    _logger.info("Started updating related fields for account move lines (POST)")

    cr.execute(query = """
            UPDATE account_move_line
            SET resource_origin_id = pc_resource_origin_id,
                expenditure_item_id = pc_item_id,
                program_code_dependency_id = pc_dependency_id,
                provider_id = aml_partner_id,
                unam_account_id = aml_account_id
            FROM (
                SELECT aml.id as aml_id, aml.partner_id as aml_partner_id, aml.account_id as aml_account_id, 
                        pc.resource_origin_id as pc_resource_origin_id, pc.item_id as pc_item_id, pc.dependency_id as pc_dependency_id
                FROM account_move_line aml
                INNER JOIN program_code pc ON aml.program_code_id = pc.id
                WHERE aml.program_code_id is not null
            ) AS aml_id
            WHERE account_move_line.id = aml_id;
        """)

    _logger.info("Finished updating display names for fields (POST)")
