from odoo import models, fields, api, _
import logging
logger = logging.getLogger(__name__)


class GenerateBankReference(models.TransientModel):
	"""
		Wizard para generar la referencia bancaria a partir de una ventana emergente.
			Utilizado en:
				-Devoluciones de Fondo Fijo
				-Devoluciones de Solicitudes de Pago
	"""
	_name = 'wiz.bank.reference'
	_description = 'Wizard for Generate Bank Reference'
	reference = fields.Char(string='Reference')
	short_ref = fields.Char(string='Short Reference')
	date = fields.Date(string="Date")
	currency_id = fields.Many2one('res.currency',string='Currency',required=True,default=lambda self: self.env.company.currency_id)
	amount = fields.Monetary(string="Monetary Reference",currency_field='currency_id',default=0)
	
	@api.model
	def create(self,vals):
		res = super(GenerateBankReference, self).create(vals)
		return res

	def gen_bank_ref(self):
		ref = self.reference.upper()
		long_ref = self.env['bank.reference'].calculate_bank_ref(ref,self.amount,self.date)
		vals={
			'reference':long_ref,
			'short_ref':ref,
			'date':self.date,
			'amount':self.amount,
		}
		info = self.env['bank.reference'].create(vals)

		#Modelo del Fondo fijo (Cancelación y Devoluciones)
		if 'returns'==self.env.context['model']:
			req = self.env['money.funds.request.returns'].search([('id','=',self.env.context['id_request'])])
			req.write({'bank_ref':info})
		elif 'cancel'==self.env.context['model']:
			req = self.env['money.funds.cancel'].search([('id','=',self.env.context['id_request'])])
			req.write({'bank_ref':info})
		#Modelo de Devoluciones Generales
		elif 'account_return'==self.env.context['model']:
			req = self.env['account.returns'].search([('id','=',self.env.context['id_request'])])
			req.write({'bank_ref':info})
	

