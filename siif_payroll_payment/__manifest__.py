{
    'name': 'SIIF Payroll Payment',
    'summary': "SIIF Payroll Payment is used to improve the source code for payroll's module through inherit",
    'version': '13.0.0.1.0',
    'category': 'Payroll',
    'author': 'SIIF UNAM',
    'maintainer': 'SIIF UNAM',
    'website': '',
    'license': 'AGPL-3',
    'depends': ['jt_payroll_payment', 'jt_budget_mgmt', 'siif_account_design'],
    'data': [
        'views/assets_backend.xml',
        'views/payroll_payment_parameters.xml',
        'views/payment_requests_payroll.xml',
        'views/payment_requests_payroll_reissue.xml',
        'views/account_payment_view.xml',
        'views/operation_type.xml',
        'views/pay_handbooks.xml',
        'views/update_payroll_file_view.xml',
        'wizard/payroll_payment_batch_wizard.xml',
        #Security
        'security/security.xml',
        'security/ir.model.access.csv'
    ],
    'qweb': [
        "static/src/xml/payroll_payment_request.xml",
    ],
    'application': False,
    'installable': True,
    'auto_install': False,
}