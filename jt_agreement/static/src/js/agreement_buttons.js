odoo.define("jt_agreement.ListController", function (require) {
    "use strict";
  
    let ListController = require("web.ListController")
    let rpc = require('web.rpc');
    let session = require('web.session')
  
    ListController.include({
      action_interest_distribution: function () {
        let self = this
        let user = session.uid;
  
        rpc.query({
          model: 'bases.collaboration',
          method: 'call_bases_collaboration_calculate_interest_distribution_view',
          args: [[user]],
        }).then(function (e) {
            if(e){
                self.do_action(e);
             
            }
        });
      },
      renderButtons: function($node) {
        this._super.apply(this, arguments)
        if (this.$buttons) {
          // Botón para Generar Distribución de rendimientos
          this.$buttons.find('.oe_action_interest_distribution')
          .click(this.proxy('action_interest_distribution'))
        }
      },
    });
  });