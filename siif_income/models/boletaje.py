# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
from datetime import datetime
import time
import json
import logging
#Modelos
ACCOUNT_FISCAL_YEAR = 'account.fiscal.year'
ACCOUNT_MOVE = 'account.move'
HR_EMPLOYEE = 'hr.employee'
IR_ATTACHMENT = 'ir.attachment'
SIIF_INCOME_SERIES_DEP = 'siif.income.series.dependencia.boletaje'
MODEL_CORTE_CAJA = 'siif.income.corte.caja'
MODEL_CORTE_CAJA_LINES = 'siif.income.corte.caja.lines'
MODEL_IR_ACT_WINDOW = "ir.actions.act_window"
MODEL_IR_SEQUENCE = 'ir.sequence'
MODEL_RES_USERS = 'res.users'
MODEL_SERIE_DOTACIONES_LINES = 'siif.income.series.dotaciones.lines'
MODEL_SIDIA_UNIVERSITY_SIGNATURE = 'sidia.university_signature'
MODEL_SOLICITUD_DOTACIONES_LINES = 'siif.income.solicitud.dotaciones.lines'
SUB_DEPENDENCY = 'sub.dependency'


# Strings
MAIL_THREAD = 'mail.thread'
MAIL_ACTIVITY_MIXIN = 'mail.activity.mixin'
STR_APRECIABLE_DEP = 'Apreciable Dependencia'
STR_CANTIDAD_BOLETOS = 'La cantidad de boletos que desea comprobar es mayor al Total de Boletos por Comprobar!!!'
STR_CVE_DOTACION = 'Clave Dotación'
STR_FECHA_FIRMA = 'Fecha firma'
STR_FEU_SECRETARIO_ADMINISTRATIVO = 'Firma Secretario Administrativo o Jefe de Unidad Administrativa (Dependencia cedente)'
STR_FISCAL_YEAR = 'Año Fiscal'
STR_FIRMA_DGF = 'Firma DGF'
STR_FOLIO_INICIAL = 'Folio Inicial'
STR_FOLIO_FINAL = 'Folio Final'
STR_HELP = 'Technical field for UX purpose.'
STR_LINES_MODEL = 'Lineas Model'
STR_ROB_EXTRAV = 'Robados y/o Extraviados'
STR_TYPE_CONTROL = 'Tipo de Control'
STR_VIEW_MODE = 'tree,form'


class SiifBoletaje(models.Model):
    _name = "siif.income.boletaje"
    _description = "Menú Boletaje"


class SiifSolicitudBoletaje(models.Model):
    _name = "siif.income.solicitud.boletaje"
    _description = "Solicitud de Folios"
    _rec_name = "no_solicitud"
    _inherit = [MAIL_THREAD, MAIL_ACTIVITY_MIXIN]

    line_ids_dotaciones = fields.One2many(MODEL_SOLICITUD_DOTACIONES_LINES, 'my_id', 'Lines')
    sequence_tree = fields.Integer(string = 'Sequence')

    no_solicitud = fields.Char(string = 'Número de Solicitud', tracking= True)

    serie = fields.Many2one(comodel_name=SIIF_INCOME_SERIES_DEP, string = 'Serie', track_visibility=True)
    fiscal_year =  fields.Many2one(comodel_name = ACCOUNT_FISCAL_YEAR, string = STR_FISCAL_YEAR)
    tipo_control = fields.Char(string = STR_TYPE_CONTROL)
    tipo_boleto = fields.Selection([('printed', 'Impresos'),
                                    ('electronics', 'Electrónicos'),
                                   ], string="Tipos de Boletos")

    dependency_id = fields.Many2one('dependency', string='Dependencia', related="serie.dependency_id", store=True)
    sub_dependency_id = fields.Many2one(SUB_DEPENDENCY, string = 'Subdependencia', related="serie.sub_dependency_id", store=True)
    domain_sub_dependency = fields.Char(compute="_compute_domain_sub_dependency", store=False)
    fecha_registro = fields.Datetime(string='Fecha y hora de Registro', default=datetime.today(), tracking=True)
    folio_inicial = fields.Integer(string = 'Boleto Inicial')
    folios_solicitados = fields.Integer(comodel_name=MODEL_SOLICITUD_DOTACIONES_LINES ,string="Boletos Solicitados", compute='_compute_cantidad_dotaciones')
    total_boletos_asig = fields.Integer(string = 'Total de Boletos Asignados')
    concepto_solicitud = fields.Char(string = 'Concepto de la Solicitud')
    nombre = fields.Many2one(comodel_name=HR_EMPLOYEE,
                             string = 'Nombre(s)',
                             default=lambda self: self._default_current_user_id(),)
    curp = fields.Char(comodel_name=HR_EMPLOYEE, string = 'CURP', default=lambda self: self._default_current_curp_id(),)
    state = fields.Selection([('draft', 'Borrador'),
                              ('registered', 'Registrada'),
                              ('requested', 'Solicitada'),
                              ('validated', 'Validada'),
                              ('rejected', 'Rechazada'),
                              ('authorized', 'Autorizada'),
                              ('not_authorized', 'No Autorizada')
                             ], string="Estado", default='draft', track_visibility = "always")


    documentary_support = fields.Many2many(IR_ATTACHMENT, string="Agregar Soporte Documental")

    feu_signature_1 = fields.Many2one(MODEL_SIDIA_UNIVERSITY_SIGNATURE, string=STR_FEU_SECRETARIO_ADMINISTRATIVO, store=True, tracking= True)
    qr_signature_1 = fields.Binary(related="feu_signature_1.qr_image", string=STR_FEU_SECRETARIO_ADMINISTRATIVO, readonly=True)
    signature_name_1 = fields.Text(related="feu_signature_1.signature_name", string="Nombre", readonly=True, store=True)
    curp_feu_1 = fields.Char(related="feu_signature_1.curp", string="CURP", readonly=True , store=True)
    date_feu_1 = fields.Datetime(related="feu_signature_1.create_date", string=STR_FECHA_FIRMA, readonly=True)

    feu_signature_2 = fields.Many2one(MODEL_SIDIA_UNIVERSITY_SIGNATURE, string=STR_FIRMA_DGF, store=True, tracking= True)
    qr_signature_2 = fields.Binary(related="feu_signature_2.qr_image", string=STR_FIRMA_DGF, readonly=True)
    signature_name_2 = fields.Text(related="feu_signature_2.signature_name", string="Nombre", readonly=True, store=True)
    curp_feu_2 = fields.Char(related="feu_signature_2.curp", string="CURP", readonly=True, store=True)
    date_feu_2 = fields.Datetime(related="feu_signature_2.create_date", string=STR_FECHA_FIRMA, readonly=True)

    fecha_authorized = fields.Datetime(string='Fecha y hora de Autorización', default=datetime.today(), tracking= True)
    reason_rejection = fields.Text(string='Motivo de Rechazo')
    emails = fields.Char()

    @api.model
    def _default_current_user_id(self):
        user = self.env.user
        employee = self.env[HR_EMPLOYEE].search([('user_id', '=', user.id)])
        return employee

    def _default_current_serie_id(self):
        if self.serie:
            serie = self.env[SIIF_INCOME_SERIES_DEP].search([('serie', '=', serie)])

            return serie

    # domain para filtrar la serie conforme a dep, subdep y año fiscal
    @api.onchange('fiscal_year')
    def onchange_fiscal_year(self):
        if self.fiscal_year:
            domain = [
                ('dependency_id', 'in', self.env.user.dependency_income_ids.ids),
                ('sub_dependency_id', 'in', self.env.user.sub_dependency_income_ids.ids),
                ('state', '=', 'active'),
                ('fiscal_year', '=', self.fiscal_year.id)
            ]
        else:
            domain = [('id', '=', False)]
        return {'domain': {'serie': domain}}


    # crea la secuencia para numero de solicitud con prefijo del año en curso
    @api.model
    def create(self, vals):
        folios_solicitados = self.folios_solicitados #vals['folios_solicitados']
        folios_solicitados_guardado = self.folios_solicitados

        total_cantidad = 0

        if 'line_ids_dotaciones' in vals:
            for data in vals['line_ids_dotaciones']:
                cantidad = data[2]
                total_cantidad += cantidad.get('cantidad')
                folios_solicitados = total_cantidad


        if folios_solicitados == 0:
            raise ValidationError(_("No podrá GUARDAR la solicitud hasta que seleccione y asigne FOLIOS a una DOTACIÓN!!!"))
        if folios_solicitados_guardado:
            self.folios_solicitados

        res = super(SiifSolicitudBoletaje, self).create(vals)
        dependencia = self.env[SIIF_INCOME_SERIES_DEP].search([('id', '=', vals['serie'])])
        dep = dependencia.dependency_id.dependency
        subdep = dependencia.sub_dependency_id.sub_dependency
        year_fiscal = self.env[ACCOUNT_FISCAL_YEAR].search([('id', '=', vals['fiscal_year'])])
        year = year_fiscal.name
        fiscal_year = ''
        year_sequence = self.env[MODEL_IR_SEQUENCE]
        if year:
            existing_sequence = self.env[MODEL_IR_SEQUENCE].search([('name', 'like', f'Secuencia Número Solicitud {year}')])
            if existing_sequence:
                fiscal_year = existing_sequence[0].code
            else:
                new_sequence = year_sequence.create({
                    'name': f'Secuencia Número Solicitud {year}',
                    'code': f'seq.numero.solicitud.boletaje.{year}',
                    'padding': 6,
                    'implementation': 'standard',
                })
                fiscal_year = new_sequence.code
        seq = self.env[MODEL_IR_SEQUENCE].next_by_code(fiscal_year)
        res.no_solicitud = "SF/" + str(dep) + str(subdep) + "/" + str(year) + '/' + str(seq)
        return res

    def write(self, vals):
        serie_guardada = self.serie
        boletos_guardados = self.total_boletos_asig



        if 'state' in vals:
            if vals['state'] == 'authorized':
                serie_insertar = self.env[SIIF_INCOME_SERIES_DEP].search([('id','=', serie_guardada.id), ('state', '=', 'active')])
                if serie_insertar.state == 'active':
                    serie_insertar.total_boletos_asig = boletos_guardados
                    dotacion_copiar = self.env[MODEL_SOLICITUD_DOTACIONES_LINES].search([('my_id', '=', self.id)])

                    for record in dotacion_copiar:
                        clv_dotacion = record.clv_dotacion
                        serie_dotacion_insert = self.env[MODEL_SERIE_DOTACIONES_LINES].search([('id', '=', clv_dotacion.id)])
                        serie_dotacion_insert.cantidad += record.cantidad

                else:
                    raise ValidationError(_('Serie Inactiva o Cancelada, favor de verificar que la Serie esté ACTIVA para poder AUTORIZAR la solicitud!!!'))

        if 'state' in vals:
            if vals['state'] == 'registered':
                self.fecha_registro = self.write_date

        if 'state' in vals:
            if vals['state'] == 'authorized':
                self.fecha_authorized = self.write_date


        res = super(SiifSolicitudBoletaje, self).write(vals)
        return res


    # Accede a la CURP del empleado logueado al odoo
    @api.model
    def _default_current_curp_id(self):
        user = self.env.user
        employee = self.env[HR_EMPLOYEE].search([('user_id', '=', user.id)])

        return employee.curp

    #Al buscar la serie lo relaciona al último folio registrado en series dependencias
    @api.onchange('serie')
    def onchange_folio_inicial(self):
        if self.serie:
            serie = self.env[SIIF_INCOME_SERIES_DEP].search([('id', '=', self.serie.id), ('state', '=', 'active')], limit=1)
            if self.serie.id != serie.id:
                raise ValidationError(_('Para que pueda realizar una solicitud de boletos de la serie seleccionada, la serie debe estar ACTIVA!!!'))
            elif self.serie.id == serie.id:
                folio_inicial = serie.total_boletos_asig
                if folio_inicial == 0:
                    self.folio_inicial = 1
                else:
                    folio_inicial = serie.total_boletos_asig
                    folio_inicial += 1
                    self.folio_inicial = folio_inicial

    @api.onchange('serie')
    def onchange_serie_borrar_dep(self):
        if not self.serie:
            self.tipo_control = False
            self.dependency_id = False
            self.sub_dependency_id = False
            self.folio_inicial = False

    # Al ingresar los folios a solicitar hace el calculo para ingresar en automático el total de folios asignados
    @api.onchange('folios_solicitados')
    def onchange_folios_solicitados(self):
        if self.folios_solicitados:
            total_boletos_asignados = ((self.folios_solicitados + self.folio_inicial) - 1)
            self.total_boletos_asig = total_boletos_asignados

    @api.onchange('serie')
    def onchange_tipo_control(self):
        if self.serie:
            serie = self.env[SIIF_INCOME_SERIES_DEP].search([('id', '=', self.serie.id)], limit=1)
            if self.serie.id == serie.id:
                tipo_ctrl = serie.tipo_control
                self.tipo_control = tipo_ctrl

    @api.onchange('serie')
    def onchange_avisos(self):
        if self.serie:
            serie_restric = self.env[SIIF_INCOME_SERIES_DEP].search([('id', '=', self.serie.id)])
            
            if serie_restric.boletos_restriccion != 0:
                if serie_restric.total_boletos_rest > serie_restric.boletos_restriccion:
                    raise ValidationError(_('No puede realizar otra solicitud de boletos para la serie %s, ya que la restricción dice que le deben quedar %s boletos por comprobar para que pueda realizar otra solicitud y la serie tiene %s boletos aún por comprobar!!!')% (serie_restric.serie,serie_restric.boletos_restriccion, serie_restric.total_boletos_rest))


    def action_draft(self):
        self.ensure_one()
        self.state = 'draft'

    def action_registered(self):
        self.ensure_one()
        self.state = 'registered'

    def action_requested(self):
        self.ensure_one()
        self.set_solicite()

    def action_validated(self):
        self.ensure_one()
        self.state = 'validated'

    def action_rejected(self):
        self.ensure_one()
        self.state = 'rejected'

    def action_authorized(self):
        self.ensure_one()
        self.set_authorized()

    def action_not_authorized(self):
        self.ensure_one()
        self.set_not_authorized()


    def unlink(self):
        if self.state == 'authorized':
            raise ValidationError(_("Una vez que la solicitud está AUTORIZADA no se puede eliminar!!!"))
        res = super(SiifSolicitudBoletaje, self).unlink()
        return res

    # metodo se quien recibe el email
    def set_solicite(self): #, cuerpo):
        self.env.cr.execute('SELECT id FROM res_groups WHERE name = \'Boletaje User DGF\'')
        id_grupo_1 = self.env.cr.fetchall()
        self.env.cr.execute('SELECT id FROM res_groups WHERE name = \'Boletaje Director DGF\'')
        id_grupo_2 = self.env.cr.fetchall()
        group = self.env['res.groups'].search(['|', ('id', '=', id_grupo_1[0][0]), ('id', '=', id_grupo_2[0][0])])
        
        for user in group.users:
            email =  user.login #correo del que recibe
            name = "Dirección General de Finanzas" #Nombre del que recibe
            asunto = "SOLICITUD DE FOLIOS - DEPENDENCIA %s-%s SUBDEPENDENCIA %s-%s" % (self.dependency_id.dependency, self.dependency_id.description, self.sub_dependency_id.sub_dependency, self.sub_dependency_id.description)
            cuerpo = "<p> En el presente correo le hago sabedor de la solicitud %s. \nEl total de boletos requeridos es de %s. \nLa Subdependencia %s-%s de la Dependencia %s-%s queda a espera de su pronta respuesta.\n\n<p>" %(self.no_solicitud, self.folios_solicitados, self.sub_dependency_id.sub_dependency, self.sub_dependency_id.description, self. dependency_id.dependency, self.dependency_id.description)
            emisor = self.env.user.id # quien envía el correo
            self.send_email_specific_mail(email,name,asunto,cuerpo,emisor)
        self.state = "requested"

    # metodo se quien recibe el email
    def set_authorized(self):
        
        ##########################################################################################
        #                               IDENTIFICACIÓN DEL USURIO DEP                            #
        ##########################################################################################
        solicitante = self.nombre.id
        empleado_1 = self.env[HR_EMPLOYEE].search([('id', '=', solicitante)])
        # logging.info(f"**************************** EMPLEADO 1 ID: {empleado_1.user_id.id}")
        
        ##########################################################################################
        #                            IDENTIFICACIÓN DEL SECRETARIO DEP                           #
        ##########################################################################################
        name_feu = self.signature_name_1
        empleado_2 = self.env[HR_EMPLOYEE].search([('name', '=', name_feu)], limit=1)
        # logging.info(f"**************************** EMPLEADO 2 ID: {empleado_2.user_id.id}")
        
        usuario = self.env[MODEL_RES_USERS].search(['|', ('id', '=', empleado_1.user_id.id), ('id', '=', empleado_2.user_id.id)])      

        for user in usuario:
            email = user.login #correo del que recibe self.nombre.user_id.partner_id.email, self.emails
            name = STR_APRECIABLE_DEP #Nombre del que recibe
            asunto = "No. DE SOLICITUD %s AUTORIZADA PARA LA DEPENDENCIA %s-%s" % (self.no_solicitud, self.dependency_id.dependency, self.dependency_id.description)
            cuerpo = "<p> Este correo es enviado a manera de ejemplo para la DEP <p>"
            emisor = self.env.user.id # quien envía el correo
            self.send_email_specific_mail(email,name,asunto,cuerpo,emisor)
        self.state = "authorized"

    # metodo se quien recibe el email
    def set_not_authorized(self):
        ##########################################################################################
        #                               IDENTIFICACIÓN DEL USURIO DEP                            #
        ##########################################################################################
        solicitante = self.nombre.id
        empleado_1 = self.env[HR_EMPLOYEE].search([('id', '=', solicitante)])
        # logging.info(f"**************************** EMPLEADO 1 ID: {empleado_1.user_id.id}")
        
        ##########################################################################################
        #                            IDENTIFICACIÓN DEL SECRETARIO DEP                           #
        ##########################################################################################
        name_feu = self.signature_name_1
        empleado_2 = self.env[HR_EMPLOYEE].search([('name', '=', name_feu)], limit=1)
        
        usuario = self.env[MODEL_RES_USERS].search(['|', ('id', '=', empleado_1.user_id.id), ('id', '=', empleado_2.user_id.id)])

        for user in usuario:
            email = user.login #self.nombre.user_id.partner_id.email, self.emails #correo del que recibe
            name = STR_APRECIABLE_DEP #Nombre del que recibe
            asunto = "No. DE SOLICITUD %s NO AUTORIZADA PARA LA DEPENDENCIA %s-%s SUBDEPENDENCIA %s-%s" % (self.no_solicitud, self.dependency_id.dependency, self.dependency_id.description, self.sub_dependency_id.sub_dependency, self.sub_dependency_id.description)
            cuerpo = "<p> Este correo es enviado a manera de ejemplo para la DEP <p>"
            emisor = self.env.user.id # quien envía el correo
            self.send_email_specific_mail(email,name,asunto,cuerpo,emisor)
        self.state = "not_authorized"

    # metodo estructurar el correo electrónico by SALVADOR
    def send_email_specific_mail(self,email_delivery,name_delivery,subject,body,emisor):
            self.env.context = dict(self.env.context)
            self.env.context.update({
                    'is_email' : True
                })

            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            template_obj = self.env['mail.template'].sudo().search([('name','=','mail_template_bac')], limit=1)
            Auxbody = template_obj.body_html
            Auxbody = Auxbody.replace('--subject--',subject)
            Auxbody = Auxbody.replace('--summary--',body)
            Auxbody = Auxbody.replace('--base_url--',base_url)
            name_to = ""

            root_email = self.env[MODEL_RES_USERS].search([('id','=',emisor)])
            if not root_email:
                raise UserError('No se encuentra al usuario de correo')

            author_id = self.env['res.partner'].search([('id','=',root_email.partner_id.id)])


            mail_pool = self.env['mail.mail']
            values = {
                            'subject' : subject,
                            'email_to' : "",
                            'email_from' : author_id.email,
                            'author_id':author_id.id,
                            'reply_to' : 'Favor de no responder este mensaje.',
                            'body_html' : "",
                            'body' : body,
                }

            values['email_to'] = str(email_delivery)
            if name_delivery:
                name_to = name_delivery

            Auxbody = Auxbody.replace('--name_to--',name_to)
            values['body_html'] = Auxbody

            msg_id = mail_pool.create(values)

            if msg_id:
                mail_pool.sudo().send([msg_id])
                

            else:
                raise UserError('No se creó el mensaje')



     #- Método que modifica el css de la vista en Certificados de deposito
    x_css = fields.Html(string='CSS', sanitize=False, compute='_compute_css', store=False)
    @api.depends('state')
    def _compute_css(self):
        is_boletaje_administrator_dep = self.env.user.has_group('jt_income.group_income_boletaje_administrator_dep')
        is_boletaje_user_dep = self.env.user.has_group('jt_income.group_income_boletaje_user_dep')
        is_boletaje_user_dgf = self.env.user.has_group('jt_income.group_income_boletaje_user_dgf')
        is_boletaje_director_dgf = self.env.user.has_group('jt_income.group_income_boletaje_director_dgf')


        for record in self:
            record.x_css = ""

            if is_boletaje_administrator_dep:
                #Ocultar boton de editar para todos los grupos de certificados de deposito cuando se encuentra publicado
                if record.state in ['requested', 'authorized', 'not_authorized']:
                    record.x_css += """
                        <style>
                            .o_form_view .o_form_statusbar > .o_statusbar_status {
                                max-width:85%;
                            }
                            .o_form_button_edit {
                                display: none !important;
                            }
                        </style>
                    """
            if is_boletaje_user_dep:
                #Ocultar boton de editar para todos los grupos de certificados de deposito cuando se encuentra publicado
                if record.state in ['registered', 'requested', 'rejected','authorized']:
                    record.x_css += """
                        <style>
                            .o_form_view .o_form_statusbar > .o_statusbar_status {
                                max-width:85%;
                            }
                            .o_form_button_edit {
                                display: none !important;
                            }
                        </style>
                    """
            if is_boletaje_user_dgf:
                #Ocultar boton de editar para todos los grupos de boletaje
                if record.state in ['validated', 'authorized']:
                    record.x_css += """
                        <style>
                            .o_form_view .o_form_statusbar > .o_statusbar_status {
                                max-width:85%;
                            }
                            .o_form_button_edit {
                                display: none !important;
                            }
                        </style>
                    """

            if is_boletaje_director_dgf:
                #Ocultar boton de editar para todos los grupos de boletaje
                if record.state in ['authorized', 'not_authorized']:
                    record.x_css += """
                        <style>
                            .o_form_view .o_form_statusbar > .o_statusbar_status{
                            max-width:85%;
                            }
                            .o_form_button_edit {
                                display: none !important;
                            }
                        </style>
                    """
            else:
                record.x_css += """
                    <style>
                        .o_form_view .o_form_statusbar > .o_statusbar_status{
                            max-width:85%;
                        }
                    </style>
                """

    def logged_user(self):

        is_boletaje_user_dep = self.env.user.has_group('jt_income.group_income_boletaje_user_dep')
        is_boletaje_administrator_dep = self.env.user.has_group('jt_income.group_income_boletaje_administrator_dep')
        is_boletaje_user_dgf = self.env.user.has_group('jt_income.group_income_boletaje_user_dgf')
        is_boletaje_director_dgf = self.env.user.has_group('jt_income.group_income_boletaje_director_dgf')
        is_boletaje_admin_global = self.env.user.has_group('jt_income.group_income_admin_user')
        views = [(self.env.ref('siif_income.siif_income_solicitud_boletaje_tree').id, 'tree'), (self.env.ref('siif_income.siif_income_solicitud_boletaje_form').id, 'form')]
        domain = []

        if is_boletaje_user_dep:
            domain = [('state', 'in', ('draft','registered','requested','validated','rejected','authorized','not_authorized')), ('dependency_id', 'in', self.env.user.dependency_income_ids.ids), ('sub_dependency_id', 'in', self.env.user.sub_dependency_income_ids.ids)]

        if is_boletaje_administrator_dep:
            domain = [('state', 'in', ('draft','registered','requested','validated','rejected','authorized','not_authorized')), ('dependency_id', 'in', self.env.user.dependency_income_ids.ids), ('sub_dependency_id', 'in', self.env.user.sub_dependency_income_ids.ids)]

        if is_boletaje_user_dgf or is_boletaje_admin_global:
            domain = [('state', 'in', ('draft','registered','requested','validated','rejected','authorized','not_authorized')), ('dependency_id', 'in', self.env.user.dependency_income_ids.ids), ('sub_dependency_id', 'in', self.env.user.sub_dependency_income_ids.ids)]

        if is_boletaje_director_dgf:
            domain = [('state', 'in', ('draft','registered','requested','validated','rejected','authorized','not_authorized')), ('dependency_id', 'in', self.env.user.dependency_income_ids.ids), ('sub_dependency_id', 'in', self.env.user.sub_dependency_income_ids.ids)]

        return {
            'name': 'Solicitud de Folios',
            "res_model": "siif.income.solicitud.boletaje",
            "domain": domain,
            "type": MODEL_IR_ACT_WINDOW,
            "view_mode": STR_VIEW_MODE,
            "views": views,
        }


    @api.depends('line_ids_dotaciones.cantidad')
    def _compute_cantidad_dotaciones(self):
        for record in self:
            record.folios_solicitados = sum(line.cantidad for line in record.line_ids_dotaciones)

class SiifSolicitudDotacionesLines(models.Model):
    _name = "siif.income.solicitud.dotaciones.lines"
    _description = "Solicitud de Dotaciones Lines"
    _rec_name = "clv_dotacion"

    my_id = fields.Many2one('siif.income.solicitud.boletaje', STR_LINES_MODEL)
    display_type = fields.Selection([('line_section', "Section"), ('line_note', "Note")], default=False, help=STR_HELP)
    sequence = fields.Integer(string = 'Sequence')

    clv_dotacion = fields.Many2one(comodel_name=MODEL_SERIE_DOTACIONES_LINES, string = STR_CVE_DOTACION)
    concepto = fields.Char(string = 'Concepto')
    evento = fields.Char(string = 'Evento')
    cantidad = fields.Integer(string = 'Cantidad')
    folio_inicial = fields.Integer(string = STR_FOLIO_INICIAL)
    folio_final = fields.Integer(string = STR_FOLIO_FINAL)
    import_boleto = fields.Float(string = 'Importe por Boleto')

    serie_rel = fields.Many2one(related='my_id.serie', string='Serie Relacion')
    domain_clv_dotacion = fields.Char(compute="_compute_domain_clv_dotacion", store=False)

    @api.depends('serie_rel')
    def _compute_domain_clv_dotacion(self):
        serie = self.my_id.serie.id
        default_domain = [('my_id', '=', serie)]

        domain = default_domain

        self.domain_clv_dotacion = json.dumps(domain)


    @api.onchange('clv_dotacion')
    def onchange_clv_dotacion(self):
        if self.clv_dotacion:
            dotacion_serie = self.env[MODEL_SERIE_DOTACIONES_LINES].search([('id', '=', self.clv_dotacion.id)])
            concep = dotacion_serie.concepto
            evento = dotacion_serie.evento
            self.concepto = concep
            self.evento = evento

    @api.onchange('cantidad')
    def onchange_cantidad(self):
        if self.cantidad < 0:
            raise ValidationError(_("No puede utilizar números negativos!!!"))

    @api.onchange('import_boleto')
    def onchange_import_boleto(self):
        if self.import_boleto < 0:
            raise ValidationError(_("No puede ingresar el importe por boleto en negativo!!!"))

    @api.model
    def create(self, vals):
        res = super(SiifSolicitudDotacionesLines, self).create(vals)
        return res

    # Onchage para folio final calculado
    @api.onchange('folio_inicial', 'cantidad')
    def onchange_folio_final(self):
        if self.cantidad and self.folio_inicial:
            self.folio_final = ((self.cantidad + self.folio_inicial) -1)


class SiifCorteCaja(models.Model):
    _name = "siif.income.corte.caja"
    _description = "Corte de Caja"
    _inherit = [MAIL_THREAD, MAIL_ACTIVITY_MIXIN]

    # Lineas Conceptos
    line_ids = fields.One2many(MODEL_CORTE_CAJA_LINES, 'my_id', 'Lines')
    sequence_tree = fields.Integer(string = 'Sequence')

    domain_nombre_cfdi = fields.Char(compute='_compute_domain_nombre_cfdi', string="Domain Nombre CFDI", store=False)

    # Lineas CFDI's
    line_ids_cfdi = fields.One2many('siif.income.corte.caja.cfdi.lines', 'my_ids', 'Lines')

    # Lineas unitarios
    line_ids_unitary = fields.One2many('siif.income.corte.caja.unitary.lines', 'my_id', 'Lines')


    name = fields.Char(string = 'Número Corte de Caja', tracking=True)
    fiscal_year =  fields.Many2one(comodel_name = ACCOUNT_FISCAL_YEAR, string = STR_FISCAL_YEAR)
    serie = fields.Many2one(comodel_name=SIIF_INCOME_SERIES_DEP, string = 'Serie') # , default=lambda self: self._default_current_serie_id(), domain=get_registros_serie
    tipo_control = fields.Char(string = STR_TYPE_CONTROL)
    dependency_id = fields.Many2one('dependency', string='Dependencia', related="serie.dependency_id", store=True)
    sub_dependency_id = fields.Many2one(SUB_DEPENDENCY, string = 'Subdependencia', related="serie.sub_dependency_id", store=True)
    domain_sub_dependency = fields.Char(compute="_compute_domain_sub_dependency", store=False)
    fecha_registro = fields.Datetime(string="Fecha y hora de Registro", default=datetime.today(), tracking=True)
    fecha_authorized = fields.Datetime(string="Fecha y hora de Autorización", default=datetime.today(), tracking=True)
    fecha_not_authorized = fields.Datetime(string="Fecha y hora de No Autorización", default=datetime.today(), tracking=True)
    total_boletos_comp = fields.Integer(string = 'Total de Boletos Comprobados', compute='_compute_boletos_comp', store=True)
    total_boletos_rest = fields.Integer(string = 'Total de Boletos por Comprobar')
    importe_corte = fields.Integer(string = 'Importe')
    total_cost_for_boletos = fields.Float(comodel_name=MODEL_CORTE_CAJA_LINES ,string="Total", compute='_compute_total_cost_boletos', store=True)
    state = fields.Selection([('draft', 'Borrador'),
                              ('registered', 'Registrado'),
                              ('for_validation', 'Para Validación'),
                              ('validated', 'Validado'),
                              ('rejected', 'Rechazado'),
                              ('authorized', 'Autorizado'),
                              ('not_authorized', 'No Autorizado'),
                             ], string="Estado", default='draft', track_visibility="always")
    folio_inicial = fields.Integer(string = STR_FOLIO_INICIAL)
    folio_final = fields.Integer(string = STR_FOLIO_FINAL)

    total_importe_cfdi = fields.Float(comodel_name='siif.income.corte.caja.cfdi.lines', string='Total Monto CFDI', compute='_compute_total_monto_cfdi')

    documentary_support = fields.Many2many(IR_ATTACHMENT, string="Agregar Documentación", tracking=True)
    hide = fields.Boolean(string='Hide', default=False, compute='_compute_hide')
    rob_extrav_id = fields.Integer(string=STR_ROB_EXTRAV, store=False)

    documentary_support_2 = fields.Many2many(IR_ATTACHMENT, relation="siif_income_corte_caja_documents_rel", column1="document_id",
                                            column2="attachment_id", string="Agregar Acta Administrativa", track_visibility="onchange")

    def get_registros_corte_asociado(self):
        return json.dumps([('state', '=', 'not_authorized'), ('dependency_id', 'in', self.env.user.dependency_income_ids.ids), ('sub_dependency_id', 'in', self.env.user.sub_dependency_income_ids.ids), ('corte_rechazado_bandera', '!=', 'True')])
    corte_associated_rejected = fields.Many2one(MODEL_CORTE_CAJA, 'Corte de Caja Asociado (No Autorizado)', domain=get_registros_corte_asociado, store=True)
    corte_rechazado_bandera = fields.Boolean(string="Bandera Corte", default = False)

    feu_signature_1 = fields.Many2one(MODEL_SIDIA_UNIVERSITY_SIGNATURE, string=STR_FEU_SECRETARIO_ADMINISTRATIVO, store=True, tracking=True)
    qr_signature_1 = fields.Binary(related="feu_signature_1.qr_image", string=STR_FEU_SECRETARIO_ADMINISTRATIVO, readonly=True, store=True)
    signature_name_1 = fields.Text(related="feu_signature_1.signature_name", string="Nombre", readonly=True, store=True)
    curp_feu_1 = fields.Char(related="feu_signature_1.curp", string="CURP", readonly=True, store=True)
    date_feu_1 = fields.Datetime(related="feu_signature_1.create_date", string=STR_FECHA_FIRMA, readonly=True)
    domain_clv_dotacion = fields.Char(compute="_compute_domain_clv_dotacion", store=False)
    reason_rejection = fields.Text(string='Motivo de Rechazo', tracking=True)

    feu_signature_2 = fields.Many2one(MODEL_SIDIA_UNIVERSITY_SIGNATURE, string=STR_FIRMA_DGF, store=True, tracking=True)
    qr_signature_2 = fields.Binary(related="feu_signature_2.qr_image", string=STR_FIRMA_DGF, readonly=True, store=True)
    signature_name_2 = fields.Text(related="feu_signature_2.signature_name", string="Nombre", readonly=True, store=True)
    curp_feu_2 = fields.Char(related="feu_signature_2.curp", string="CURP", readonly=True, store=True)
    date_feu_2 = fields.Datetime(related="feu_signature_2.create_date", string=STR_FECHA_FIRMA, readonly=True)

    check_1 = fields.Boolean(string="Asociar Corte de Caja Rechazado?", default = False)
    check_2 = fields.Boolean(string="Desea copiar la información del corte de caja rechazado?", default = False)
    aviso = fields.Integer(strig="AVISO")
    nombre = fields.Many2one(comodel_name=HR_EMPLOYEE,
                             string = 'Nombre(s)',
                             default=lambda self: self._default_current_user_id(), store=True)


    @api.model
    def _default_current_user_id(self):
        user = self.env.user
        employee = self.env[HR_EMPLOYEE].search([('user_id', '=', user.id)])
        return employee

    # domain para filtrar la serie conforme a dep, subdep y año fiscal
    @api.onchange('fiscal_year')
    def onchange_fiscal_year(self):
        if self.fiscal_year:
            domain = [
                ('dependency_id', 'in', self.env.user.dependency_income_ids.ids),
                ('sub_dependency_id', 'in', self.env.user.sub_dependency_income_ids.ids),
                ('state', '=', 'active'),
                ('fiscal_year', '=', self.fiscal_year.id)
            ]
        else:
            domain = [('id', '=', False)]
        return {'domain': {'serie': domain}}

    @api.onchange('check_2')
    def onchange_check2_corte_asociado(self):
        if self.check_2 == True:
            cfdi_id_asociado = self.env['siif.income.corte.caja.cfdi.lines'].search([('my_ids', '=', self.corte_associated_rejected.id)])
            conceptos_id_asociado = self.env[MODEL_CORTE_CAJA_LINES].search([('my_id', '=', self.corte_associated_rejected.id)])

            line_cfdi = []
            for cfdis in cfdi_id_asociado:
                line_cfdi.append((0,0,{
                    'cfdi_id': cfdis.cfdi_id.id,
                    'poliza_contable': cfdis.poliza_contable,
                    'cliente': cfdis.cliente,
                    'description': cfdis.description,
                    'invoice_uuid': cfdis.invoice_uuid,
                    'importe_cfdi': cfdis.importe_cfdi,

                }))

            self.write({'line_ids_cfdi': line_cfdi})

            line_conceptos = []
            for conceptos in conceptos_id_asociado:
                line_conceptos.append((0,0,{
                    'clv_dotacion': conceptos.clv_dotacion.id,
                    'concepto_dot': conceptos.concepto_dot,
                    'concepto_boleto': conceptos.concepto_boleto,
                    'cfdi_id': conceptos.cfdi_id,
                    'poliza_contable': conceptos.poliza_contable,
                    'importe_cfdi_id': conceptos.importe_cfdi_id,
                    'total_boletos_comp_dotacion': conceptos.total_boletos_comp_dotacion,
                    'boleto_sold': conceptos.boleto_sold,
                    'boleto_cancelated': conceptos.boleto_cancelated,
                    'boleto_courtesy': conceptos.boleto_courtesy,
                    'boleto_reprinted': conceptos.boleto_reprinted,
                    'rob_extrav': conceptos.rob_extrav,
                    'boleto_other': conceptos.boleto_other,
                    'folio_inicial': conceptos.folio_inicial,
                    'folio_final': conceptos.folio_final,
                    'importe_boleto': conceptos.importe_boleto,
                    'total_cost_for_boletos': conceptos.total_cost_for_boletos,


                }))

            self.write({'line_ids': line_conceptos})


    @api.depends('serie')
    def _compute_domain_clv_dotacion(self):
        serie = self.serie.id
        default_domain = [('my_id', '=', serie)]

        domain = default_domain

        self.domain_clv_dotacion = json.dumps(domain)
    
    # Función computada para hacer invisible acta administrativa
    @api.depends('line_ids.rob_extrav')
    def _compute_hide(self):
        # simple logic, but you can do much more here
        for record in self:
            record.rob_extrav_id = sum(line.rob_extrav for line in record.line_ids)
            if record.rob_extrav_id > 0:
                self.hide = True
            else:
                self.hide = False

    @api.depends('line_ids_cfdi')
    def _compute_domain_nombre_cfdi(self):
        ids_cdfi = [l.cfdi_id.id for l in self.line_ids_cfdi if l.cfdi_id]
        if ids_cdfi:
            self.domain_nombre_cfdi = json.dumps([('id', 'in', ids_cdfi)])
        else:
            self.domain_nombre_cfdi = json.dumps([('id', '=', False)])

    # secuencia para corte de caja
    @api.model
    def create(self, vals):
        total_cantidad = 0
        total_cancelados = 0
        total_courtesy = 0
        total_other = 0
        total_rob_extrav = 0
        if 'line_ids' in vals:
            for datos in vals['line_ids']:
                total_cost = datos[2]
                total_cantidad += total_cost.get('boleto_sold')
                total_cancelados += total_cost.get('boleto_cancelated')
                total_courtesy += total_cost.get('boleto_courtesy')
                total_other += total_cost.get('boleto_other')
                total_rob_extrav += total_cost.get('rob_extrav')

                if (total_cantidad + total_cancelados + total_courtesy + total_other + total_rob_extrav) > vals['total_boletos_rest']:
                    raise ValidationError(_(STR_CANTIDAD_BOLETOS))


        total_cfdi = 0.0
        total_boletos = 0.0

        if 'line_ids_cfdi' in vals:
            for data in vals['line_ids_cfdi']:
                importe_cfdi= data[2]
                total_cfdi += importe_cfdi.get('importe_cfdi')

        if 'line_ids' in vals:
            for data in vals['line_ids']:
                total_cost_for_boletos = data[2]
                total_boletos += total_cost_for_boletos.get('total_cost_for_boletos')


        if total_cfdi != total_boletos:
            raise ValidationError(_("Verificar que los importes tanto de CONCEPTOS como de CFDI'S coincidan!!!"))

        res = super(SiifCorteCaja, self).create(vals)
        dependencia = self.env[SIIF_INCOME_SERIES_DEP].search([('id', '=', vals['serie'])])
        dep = dependencia.dependency_id.dependency
        subdep = dependencia.sub_dependency_id.sub_dependency
        year_fiscal = self.env[SIIF_INCOME_SERIES_DEP].search([('id', '=', vals['serie'])])
        year = year_fiscal.fiscal_year.name
        fiscal_year = ''
        year_sequence = self.env[MODEL_IR_SEQUENCE]
        if year:
            existing_sequence = self.env[MODEL_IR_SEQUENCE].search([('name', 'like', f'Secuencia Corte de Caja {dep} {subdep} {year}')])
            if existing_sequence:
                fiscal_year = existing_sequence[0].code
            else:
                new_sequence = year_sequence.create({
                    'name': f'Secuencia Corte de Caja {dep} {subdep} {year}',
                    'code': f'corte.caja.boletaje.{dep}.{subdep}.{year}',
                    'padding': 6,
                    'implementation': 'standard',
                })
                fiscal_year = new_sequence.code
        seq = self.env[MODEL_IR_SEQUENCE].next_by_code(fiscal_year)
        res.name = "CC/" + str(dep) + str(subdep) + "/" + str(year) + "/" + str(seq)
        return res

    def write(self, vals):
        folio_fin = self.folio_final #vals['folio_final']
        boletos_rest = self.total_boletos_rest
        serie_guardada = self.serie
        total_cost = self.total_cost_for_boletos
        total_cantidad = 0

        if 'line_ids' in vals:
            for datos in vals['line_ids']:
                total_cost = datos[2]

        if 'state' in vals:
            if vals['state'] == 'authorized':
                if serie_guardada:
                    serie_insertar = self.env[SIIF_INCOME_SERIES_DEP].search([('id','=', serie_guardada.id), ('state', '=', 'active')])
                    if serie_insertar.state == 'active':
                        serie_insertar.total_boletos_comp = folio_fin
                        serie_insertar.total_cost_for_boletos += total_cost

                        dotacion_copiar = self.env[MODEL_CORTE_CAJA_LINES].search([('my_id', '=', self.id)])
                        for record in dotacion_copiar:
                            clv_dotacion = record.clv_dotacion
                            serie_dotacion_insert = self.env[MODEL_SERIE_DOTACIONES_LINES].search([('id', '=', clv_dotacion.id)])
                            serie_dotacion_insert.boleto_sold += record.boleto_sold
                            serie_dotacion_insert.monto_comp += record.total_cost_for_boletos
                            serie_dotacion_insert.boleto_cancelated += record.boleto_cancelated
                            serie_dotacion_insert.boleto_courtesy += record.boleto_courtesy
                            serie_dotacion_insert.boleto_reprinted += record.boleto_reprinted
                            serie_dotacion_insert.boleto_other += record.boleto_other
                            serie_dotacion_insert.rob_extrav += record.rob_extrav
                    else:
                        raise ValidationError(_('Serie Inactiva o Cancelada, favor de verificar que la Serie esté ACTIVA para poder VALIDAR el corte de caja!!!'))

                if total_cantidad > boletos_rest:
                    raise ValidationError(_(STR_CANTIDAD_BOLETOS))
        if 'state' in vals:
            if vals['state'] == 'registered':
                self.fecha_registro = self.write_date

        if 'state' in vals:
            if vals['state'] == 'authorized':
                self.fecha_validated = self.write_date

        if 'state' in vals:
            if vals['state'] == 'not_authorized':
                self.fecha_not_authorized = self.write_date


        res = super(SiifCorteCaja, self).write(vals)
        return res

    @api.onchange('serie')
    def onchange_tipo_control_corte(self):
        if self.serie:
            serie = self.env[SIIF_INCOME_SERIES_DEP].search([('id', '=', self.serie.id)], limit=1)
            if self.serie.id == serie.id:
                tipo_ctrl = serie.tipo_control
                self.tipo_control = tipo_ctrl

    # Accede a la serie de la dependencia
    def _default_current_serie_id(self):
        if self.serie:
            serie = self.env[SIIF_INCOME_SERIES_DEP].search([('serie', '=', serie)])
            return serie.serie

    @api.onchange('serie')
    def onchange_serie_borrar(self):
        if not self.serie:
            self.total_boletos_rest = False
            self.dependency_id = False
            self.sub_dependency_id = False


    def action_draft(self):
        self.ensure_one()
        self.state = 'draft'

    def action_registered(self):
        self.ensure_one()
        self.state = 'registered'
       
        cfdi_folios = [cfdi.cfdi_id for cfdi in self.line_ids_cfdi]

        conceptos_folios = [conceptos.cfdi_id for conceptos in self.line_ids]

        for conceptos_folio in conceptos_folios:
            if conceptos_folio not in cfdi_folios:
                raise ValidationError(_("El CFDI con CFDI FOLIO %s no existe en la pestaña de CFDI's." % (conceptos_folio.cfdi_folio)))

        ##################################################################################################################################################
        #*** Validación: Si el Tipo de control es por Folio que verifique si antes de registar el corte de caja hay registros en la pestaña de folios ***#
        ##################################################################################################################################################
        if self.serie.tipo_control == "Folios":
            
            total_comp_conceptos = 0
            for datos in self.line_ids:
                total_comp_conceptos += (datos.boleto_sold + datos.boleto_cancelated + datos.boleto_courtesy + datos.boleto_other + datos.rob_extrav)
                
        
            total_comp_folios = 0
            for unitary in self.line_ids_unitary:
                total_comp_folios += (unitary.boleto_sold + unitary.boleto_cancelated + unitary.boleto_courtesy + unitary.boleto_other + unitary.rob_extrav)
                

            for rec in self:
                if not rec.line_ids_unitary:
                    raise ValidationError(_("No puede Registrar un corte de caja si el Tipo de Control de la serie %s es por Folios y dentro del corte de caja en la pestaña de Folios tiene %s boletos a comprobar, mientras que en la pestaña de Conceptos tiene %s boletos a comprobar!!!" % (self.serie.serie, total_comp_folios, total_comp_conceptos)))

        
        total_comp = 0
        for datos in self.line_ids:
            total_comp += (datos.boleto_sold + datos.boleto_cancelated + datos.boleto_courtesy + datos.boleto_other + datos.rob_extrav)
            if total_comp > self.total_boletos_rest:
                raise ValidationError(_(STR_CANTIDAD_BOLETOS))
        
        # if not self.documentary_support_2:
        for record in self:
            record.rob_extrav_id = sum(line.rob_extrav for line in record.line_ids)
            if record.rob_extrav_id > 0:
                if not self.documentary_support_2:
                    raise ValidationError(_("No se ha adjuntado el Acta Administrativa, tiene %s boletos robados y/o extraviados!!!" % (record.rob_extrav_id))) 

    def action_for_validation(self):
        self.ensure_one()
        self.set_for_solicite()

    def action_validated(self):
        self.ensure_one()
        self.state = 'validated'

    def action_rejected(self):
        self.ensure_one()
        self.state = 'rejected'

    def action_authorized(self):
        self.ensure_one()

        if self.corte_associated_rejected:
            self.corte_associated_rejected.corte_rechazado_bandera = True

        for cfdis in self.line_ids_cfdi.cfdi_id:
            cfdis.is_in_cash_closing = True
        self.set_authorized()



    def action_not_authorized(self):
        self.ensure_one()
        self.set_not_authorized()

    # metodo de quien recibe el email
    def set_for_solicite(self):
        self.env.cr.execute('SELECT id FROM res_groups WHERE name = \'Boletaje User DGF\'')
        id_grupo_1 = self.env.cr.fetchall()
        self.env.cr.execute('SELECT id FROM res_groups WHERE name = \'Boletaje Director DGF\'')
        id_grupo_2 = self.env.cr.fetchall()
        group = self.env['res.groups'].search(['|', ('id', '=', id_grupo_1[0][0]), ('id', '=', id_grupo_2[0][0])])

        for user in group.users:
            email = user.login #correo del que recibe
            name = "Apreciable Dirección General de Finanzas" #Nombre del que recibe
            asunto = "CORTE DE CAJA - DEPENDENCIA %s-%s SUBDEPENDENCIA %s-%s" % (self.dependency_id.dependency, self.dependency_id.description, self.sub_dependency_id.sub_dependency, self.sub_dependency_id.description)
            cuerpo = "<p>SE ENVÍA EL CORTE DE CAJA %s PARA SU VALIDACIÓN<p>" % (self.name)
            emisor = self.env.user.id # quien envía el correo
            self.send_email_specific_mail(email,name,asunto,cuerpo,emisor)
        self.state = "for_validation"

    # metodo de quien recibe el email
    def set_authorized(self):
        ##########################################################################################
        #                               IDENTIFICACIÓN DEL USURIO DEP                            #
        ##########################################################################################
        solicitante = self.nombre.id
        empleado_1 = self.env[HR_EMPLOYEE].search([('id', '=', solicitante)])
        # logging.info(f"**************************** EMPLEADO 1 ID: {empleado_1.user_id.id}")
        
        ##########################################################################################
        #                            IDENTIFICACIÓN DEL SECRETARIO DEP                           #
        ##########################################################################################
        name_feu = self.signature_name_1
        empleado_2 = self.env[HR_EMPLOYEE].search([('name', '=', name_feu)], limit=1)
        # logging.info(f"**************************** EMPLEADO 2 ID: {empleado_2.user_id.id}")
        
        usuario = self.env[MODEL_RES_USERS].search(['|', ('id', '=', empleado_1.user_id.id), ('id', '=', empleado_2.user_id.id)])

        for user in usuario:
            email = user.login #correo del que recibe
            name = STR_APRECIABLE_DEP #Nombre del que recibe
            asunto = "No. DE CORTE DE CAJA %s AUTORIZADO PARA LA DEPENDENCIA %s-%s. \nDE IGUAL MANERA SE LE INFORMA QUE LE QUEDAN %s BOLETOS POR COMPROBAR, SI REQUIERE MÁS FOLIOS PUEDE REALIZAR OTRA SOLICITUD" % (self.name, self.dependency_id.dependency, self.dependency_id.description, self.aviso)
            cuerpo = "<p>SE ENVÍA EL CORTE DE CAJA %s AUTORIZADA<p>" % (self.name)
            emisor = self.env.user.id # quien envía el correo
            self.send_email_specific_mail(email,name,asunto,cuerpo,emisor)
        self.state = "authorized"

    # metodo de quien recibe el email
    def set_not_authorized(self):
        ##########################################################################################
        #                               IDENTIFICACIÓN DEL USURIO DEP                            #
        ##########################################################################################
        solicitante = self.nombre.id
        empleado_1 = self.env[HR_EMPLOYEE].search([('id', '=', solicitante)])
        # logging.info(f"**************************** EMPLEADO 1 ID: {empleado_1.user_id.id}")
        
        ##########################################################################################
        #                            IDENTIFICACIÓN DEL SECRETARIO DEP                           #
        ##########################################################################################
        name_feu = self.signature_name_1
        empleado_2 = self.env[HR_EMPLOYEE].search([('name', '=', name_feu)], limit=1)
        
        usuario = self.env[MODEL_RES_USERS].search(['|', ('id', '=', empleado_1.user_id.id), ('id', '=', empleado_2.user_id.id)])

        for user in usuario:
            email = user.login #correo del que recibe
            name = STR_APRECIABLE_DEP #Nombre del que recibe
            asunto = "No. DE CORTE DE CAJA %s NO AUTORIZADO PARA LA DEPENDENCIA %s-%s SUBDEPENDENCIA %s-%s" % (self.name, self.dependency_id.dependency, self.dependency_id.description, self.sub_dependency_id.sub_dependency, self.sub_dependency_id.description)
            cuerpo = "<p>NO SE AUTORIZA EL CORTE DE CAJA %s, EL MOTIVO DE RECHAZO ES: %s<p>" % (self.name, self.reason_rejection)
            emisor = self.env.user.id # quien envía el correo
            self.send_email_specific_mail(email,name,asunto,cuerpo,emisor)
        self.state = "not_authorized"

    # metodo estructurar el correo electrónico by SALVADOR
    def send_email_specific_mail(self,email_delivery,name_delivery,subject,body,emisor):
        self.env.context = dict(self.env.context)
        self.env.context.update({
                'is_email' : True
            })

        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        template_obj = self.env['mail.template'].sudo().search([('name','=','mail_template_bac')], limit=1)
        Auxbody = template_obj.body_html
        Auxbody = Auxbody.replace('--subject--',subject)
        Auxbody = Auxbody.replace('--summary--',body)
        Auxbody = Auxbody.replace('--base_url--',base_url)
        name_to = ""

        root_email = self.env[MODEL_RES_USERS].search([('id','=',emisor)])
        if not root_email:
            raise UserError('No se encuentra al usuario de correo')

        author_id = self.env['res.partner'].search([('id','=',root_email.partner_id.id)])


        mail_pool = self.env['mail.mail']
        values = {
                        'subject' : subject,
                        'email_to' : "",
                        'email_from' : author_id.email,
                        'author_id':author_id.id,
                        'reply_to' : 'Favor de no responder este mensaje.',
                        'body_html' : "",
                        'body' : body,
            }

        values['email_to'] = str(email_delivery)
        if name_delivery:
            name_to = name_delivery

        Auxbody = Auxbody.replace('--name_to--',name_to)
        values['body_html'] = Auxbody

        msg_id = mail_pool.create(values)

        if msg_id:
            mail_pool.sudo().send([msg_id])
            

        else:
            raise UserError('No se creó el mensaje')




    @api.depends('line_ids.boleto_sold', 'line_ids.boleto_cancelated', 'line_ids.boleto_courtesy', 'line_ids.rob_extrav')
    def _compute_boletos_comp(self):
        for record in self:
            record.total_boletos_comp = sum(line.boleto_sold for line in record.line_ids) + sum(line.boleto_cancelated for line in record.line_ids) + sum(line.boleto_courtesy for line in record.line_ids) + sum(line.rob_extrav for line in record.line_ids) + sum(line.boleto_other for line in record.line_ids)

    # Busca la serie y asocia los boletos a comprobar
    @api.onchange('serie')
    def onchange_total_boletos_rest(self):
        if self.serie:
            serie = self.env[SIIF_INCOME_SERIES_DEP].search([('id', '=', self.serie.id)], limit=1)

            if self.serie.id == serie.id:

                total_boletos_rest = serie.total_boletos_rest
                self.total_boletos_rest = total_boletos_rest


    @api.depends('line_ids.total_cost_for_boletos')
    def _compute_total_cost_boletos(self):
        for record in self:
            record.total_cost_for_boletos = sum(line.total_cost_for_boletos for line in record.line_ids)


   #Al buscar la serie lo relaciona al último folio registrado en series dependencias
    @api.onchange('serie')
    def onchange_folio_inicial(self):
        if self.serie:
            serie = self.env[SIIF_INCOME_SERIES_DEP].search([('id', '=', self.serie.id)], limit=1)
            if serie.total_boletos_rest == 0:
                raise ValidationError(_("El total de Boletos Restantes por Comprobar en la serie seleccionada es 0!!!"))
            if self.serie.id == serie.id:
                serie = self.env[MODEL_CORTE_CAJA].search([('serie', '=', self.serie.id)], limit=1)
                folio_inicial = serie.folio_final
                if folio_inicial == 0:
                    self.folio_inicial = 1
                else:
                    folio = self.env[MODEL_CORTE_CAJA].search([('serie', '=', self.serie.id)])[-1]
                    folio_inicial = folio.folio_final
                    folio_inicial += 1
                    self.folio_inicial = folio_inicial


    # Al ingresar los folios a solicitar hace el calculo para ingresar en automático el total de folios asignados
    @api.onchange('total_boletos_comp')
    def onchange_total_boletos_comp(self):
        if self.total_boletos_comp:
            folio_final = ((self.total_boletos_comp + self.folio_inicial) - 1)
            self.folio_final = folio_final

    @api.depends('line_ids_cfdi.importe_cfdi')
    def _compute_total_monto_cfdi(self):
        for record in self:
            record.total_importe_cfdi = sum(line.importe_cfdi for line in record.line_ids_cfdi)

    def unlink(self):
        if self.state == 'authorized':
            raise ValidationError(_("Una vez que el corte de caja pasa a estado de AUTORIZADO no se puede eliminar!!!"))
        if self.state == 'not_authorized':
            raise ValidationError(_("Una vez que el corte de caja pasa a estado de NO AUTORIZADO no se puede eliminar!!!"))
        res = super(SiifCorteCaja, self).unlink()
        return res

    #- Método que modifica el css de la vista en Certificados de deposito
    x_css = fields.Html(string='CSS', sanitize=False, compute='_compute_css', store=False)
    @api.depends('state')
    def _compute_css(self):
        is_boletaje_administrator_dep = self.env.user.has_group('jt_income.group_income_boletaje_administrator_dep')
        is_boletaje_user_dep = self.env.user.has_group('jt_income.group_income_boletaje_user_dep')
        is_boletaje_user_dgf = self.env.user.has_group('jt_income.group_income_boletaje_user_dgf')
        is_boletaje_director_dgf = self.env.user.has_group('jt_income.group_income_boletaje_director_dgf')
        is_boletaje_admin_global = self.env.user.has_group('jt_income.group_income_admin_user')

        for record in self:
            record.x_css = ""

            if is_boletaje_administrator_dep:
                #Ocultar boton de editar para todos los grupos de certificados de deposito cuando se encuentra publicado
                if record.state in ['authorized', 'not_authorized']: 
                    record.x_css += """
                        <style>
                            .o_form_view .o_form_statusbar > .o_statusbar_status {
                                max-width:85%;
                            }
                            .o_form_button_edit {
                                display: none !important;
                            }
                        </style>
                    """

            if is_boletaje_user_dep:
            #Ocultar boton de editar para todos los grupos de certificados de deposito cuando se encuentra publicado
                if record.state in ['authorized', 'not_authorized']:
                    record.x_css += """
                        <style>
                            .o_form_view .o_form_statusbar > .o_statusbar_status {
                                max-width:85%;
                            }
                            .o_form_button_edit {
                                display: none !important;
                            }
                        </style>
                    """
            if is_boletaje_user_dgf:
            #Ocultar boton de editar para todos los grupos de certificados de deposito cuando se encuentra publicado
                if record.state in ['authorized', 'not_authorized']:
                    record.x_css += """
                        <style>
                            .o_form_view .o_form_statusbar > .o_statusbar_status {
                                max-width:85%;
                            }
                            .o_form_button_edit {
                                display: none !important;
                            }
                        </style>
                    """
            if is_boletaje_director_dgf:
            #Ocultar boton de editar para todos los grupos de certificados de deposito cuando se encuentra publicado
                if record.state in ['authorized', 'not_authorized']:
                    record.x_css += """
                        <style>
                            .o_form_view .o_form_statusbar > .o_statusbar_status {
                                max-width:85%;
                            }
                            .o_form_button_edit {
                                display: none !important;
                            }
                        </style>
                    """
            
            if is_boletaje_admin_global:
                #Ocultar boton de editar para todos los grupos de certificados de deposito cuando se encuentra publicado
                if record.state in ['authorized', 'not_authorized']: 
                    record.x_css += """
                        <style>
                            .o_form_view .o_form_statusbar > .o_statusbar_status {
                                max-width:85%;
                            }
                            .o_form_button_edit {
                                display: none !important;
                            }
                        </style>
                    """

            # En cualquiera de los estados, se limita el tamaño del statusbar para evitar tener
            # 'botones gordos'
            else:
                record.x_css += """
                    <style>
                        .o_form_view .o_form_statusbar > .o_statusbar_status{
                            max-width:85%;
                        }
                    </style>
                """

    def logged_user(self):

        is_boletaje_user_dep = self.env.user.has_group('jt_income.group_income_boletaje_user_dep')
        is_boletaje_administrator_dep = self.env.user.has_group('jt_income.group_income_boletaje_administrator_dep')
        is_boletaje_user_dgf = self.env.user.has_group('jt_income.group_income_boletaje_user_dgf')
        is_boletaje_director_dgf = self.env.user.has_group('jt_income.group_income_boletaje_director_dgf')
        is_boletaje_admin_global = self.env.user.has_group('jt_income.group_income_admin_user')
        views = [(self.env.ref('siif_income.siif_income_corte_caja_tree').id, 'tree'), (self.env.ref('siif_income.siif_income_corte_caja_form').id, 'form')]
        domain = []

        if is_boletaje_user_dep:
            domain = [('state', 'in', ('draft','registered','for_validation','validated','rejected','authorized','not_authorized')), ('dependency_id', 'in', self.env.user.dependency_income_ids.ids), ('sub_dependency_id', 'in', self.env.user.sub_dependency_income_ids.ids)]

        if is_boletaje_administrator_dep:
            domain = [('state', 'in', ('draft','registered','for_validation','validated','rejected','authorized','not_authorized')), ('dependency_id', 'in', self.env.user.dependency_income_ids.ids), ('sub_dependency_id', 'in', self.env.user.sub_dependency_income_ids.ids)]

        if is_boletaje_user_dgf:
            domain = [('state', 'in', ('draft','registered','for_validation','validated','rejected','authorized','not_authorized')), ('dependency_id', 'in', self.env.user.dependency_income_ids.ids), ('sub_dependency_id', 'in', self.env.user.sub_dependency_income_ids.ids)]

        if is_boletaje_director_dgf:
            domain = [('state', 'in', ('draft','registered','for_validation','validated','rejected','authorized','not_authorized')), ('dependency_id', 'in', self.env.user.dependency_income_ids.ids), ('sub_dependency_id', 'in', self.env.user.sub_dependency_income_ids.ids)]

        if is_boletaje_admin_global:
            domain = [('state', 'in', ('draft','registered','for_validation','validated','rejected','authorized','not_authorized')), ('dependency_id', 'in', self.env.user.dependency_income_ids.ids), ('sub_dependency_id', 'in', self.env.user.sub_dependency_income_ids.ids)]

        return {
            'name': 'Corte de Caja',
            "res_model": "siif.income.corte.caja",
            "domain": domain,
            "type": MODEL_IR_ACT_WINDOW,
            "view_mode": STR_VIEW_MODE,
            "views": views,
        }

    def import_lines(self):
        return {
            'name': _('Importar Conceptos'),
            'type': 'ir.actions.act_window',
            'res_model': 'import.concepts.line',
            'view_mode': 'form',
            'view_type': 'form',
            'views': [(False, 'form')],
            'context': {
                'default_domain_factura_cliente': self.domain_nombre_cfdi,
                'default_domain_clv_dotacion': self.domain_clv_dotacion,
            },
            'target': 'new',
        }

    def import_lines_folios(self):
        return {
            'name': _('Importar Folios Unitarios'),
            'type': 'ir.actions.act_window',
            'res_model': 'import.unitary.line',
            'view_mode': 'form',
            'view_type': 'form',
            'views': [(False, 'form')],
            'context': {
                'default_domain_factura_cliente': self.domain_nombre_cfdi,
                'default_domain_clv_dotacion': self.domain_clv_dotacion,
            },
            'target': 'new',
        }



class SiifCorteCajaLines(models.Model):
    _name = "siif.income.corte.caja.lines"
    _description = "Corte Caja lines"

    # Conceptos
    my_id = fields.Many2one(MODEL_CORTE_CAJA, STR_LINES_MODEL)
    display_type = fields.Selection([('line_section', "Section"), ('line_note', "Note")], default=False, help=STR_HELP, store=False)
    sequence = fields.Integer(string = 'Sequence')
    clv_dotacion = fields.Many2one(comodel_name=MODEL_SERIE_DOTACIONES_LINES, string = STR_CVE_DOTACION)
    concepto_dot = fields.Char(string = 'Concepto Dotación')
    concepto_boleto = fields.Char(string = 'Concepto Boleto')
    domain_cfdi = fields.Char(related='my_id.domain_nombre_cfdi', store=False)

    cfdi_id = fields.Many2one(comodel_name=ACCOUNT_MOVE, string='CFDI Folio') #, domain=get_registros_nombre_cfdi) #, compute='_compute_domain_nombre_cfdi_id', store=True)
    poliza_contable = fields.Char(string="Poliza de Registro Contable")
    importe_cfdi_id = fields.Integer(string = 'Importe CFDI')
    total_boletos_comp_dotacion = fields.Integer(string='Total Boletos por Comprobar', store=True)
    boleto_sold = fields.Integer(string='Boletos Vendidos')
    boleto_courtesy = fields.Integer(string='Boletos Cortesias')
    boleto_reprinted = fields.Integer(string='Boletos Reimpresos')
    boleto_other = fields.Integer(string='Boletos Otros')
    folio_inicial = fields.Integer(string=STR_FOLIO_INICIAL)
    folio_final = fields.Integer(string=STR_FOLIO_FINAL)
    boleto_cancelated = fields.Integer(string='Boletos Cancelados')
    rob_extrav = fields.Integer(string=STR_ROB_EXTRAV)
    importe_boleto = fields.Float(string = 'Importe por Boleto')
    total_cost_for_boletos = fields.Float(string="Total")

    serie_rel = fields.Many2one(related='my_id.serie', string='Serie Relacion')
    domain_clv_dotacion = fields.Char(compute="_compute_domain_clv_dotacion", store=False)


    @api.onchange('domain_cfdi')
    def _onchange_domain_cfdi(self):
        for record in self:
            record.domain_cfdi = record.my_id.domain_nombre_cfdi

    @api.depends('serie_rel')
    def _compute_domain_clv_dotacion(self):
        serie = self.my_id.serie.id
        default_domain = [('my_id', '=', serie)]

        domain = default_domain

        self.domain_clv_dotacion = json.dumps(domain)

    @api.onchange('clv_dotacion')
    def onchange_clv_dotacion_corte(self):
        if self.clv_dotacion:
            dotacion_serie = self.env[MODEL_SERIE_DOTACIONES_LINES].search([('id', '=', self.clv_dotacion.id)])
            concep = dotacion_serie.concepto
            self.concepto_dot = concep

            if dotacion_serie.folios_rest <= 0 :
                raise ValidationError(_('La cantidad de Boletos por Comprobar de la dotación seleccionada a llegado a 0, favor de ralizar un corte de caja sobre una dotación que si tenga Boletos por Comprobar!!!'))


    @api.onchange('clv_dotacion')
    def onchange_clv_dotacion_solicitud(self):
        if self.clv_dotacion:
            dotacion_serie = self.env[MODEL_SERIE_DOTACIONES_LINES].search([('id', '=', self.clv_dotacion.id)])
            if dotacion_serie.id:
                dotacion_solicitud = self.env[MODEL_SOLICITUD_DOTACIONES_LINES].search([('clv_dotacion', '=', dotacion_serie.id)])[-1]
                importe = dotacion_solicitud.import_boleto
                boletos_comprobar = dotacion_serie.folios_rest
                self.total_boletos_comp_dotacion = boletos_comprobar
                self.importe_boleto = importe
        if not self.clv_dotacion:
            self.concepto_dot = False
            self.total_boletos_comp_dotacion = False
            self.importe_boleto = False



    @api.onchange('boleto_sold', 'importe_boleto', 'total_cost_for_boletos', 'cancelados', 'rob_extrav')
    def _total_cost_boletos(self):
        vendidos = self.boleto_sold
        importe_boleto = self.importe_boleto
        if importe_boleto > 0:
            self.total_cost_for_boletos = vendidos * importe_boleto
        if self.boleto_sold < 0:
            raise ValidationError(_("La cantidad de boletos vendidos no puede ser un número negativo!!!"))
        if self.importe_boleto < 0:
            raise ValidationError(_("El importe por boleto no puede ser un número negativo!!!"))
        if self.total_cost_for_boletos < 0:
            raise ValidationError(_("El importe total en conceptos no puede ser un número negativo!!!"))
        if self.boleto_cancelated < 0:
            raise ValidationError(_("El número de boletos cancelados no puede ser un número negativo!!!"))
        if self.rob_extrav < 0:
            raise ValidationError(_("El número de boletos robados y/o extraviados no puede ser un número negativo!!!"))


    @api.onchange('boleto_sold','boleto_cancelated', 'boleto_courtesy', 'boleto_other','rob_extrav')
    def _total_folio_final_dot_corte(self):
        if (self.boleto_sold + self.boleto_cancelated + self.boleto_courtesy + self.boleto_other + self.rob_extrav) > self.total_boletos_comp_dotacion:
            raise ValidationError(_("La cantidad de boletos que desea comprobar en esta dotación no puede ser mayor al número Total de boletos por Comprobar de la dotación seleccionada!!!"))

    @api.onchange('cfdi_id')
    def onchange_nombre_cfdi_id(self):
        if self.cfdi_id:
            cliente_id = self.env[ACCOUNT_MOVE].search([('id', '=', self.cfdi_id.id)])
            self.importe_cfdi_id = cliente_id.amount_total
            self.poliza_contable = cliente_id.name + ' ' + '(' + cliente_id.ref + ')'

    # Onchage para folio final calculado
    @api.onchange('folio_inicial', 'boleto_sold', 'boleto_cancelated', 'boleto_courtesy', 'boleto_other', 'rob_extrav')
    def onchange_final_folio(self):
        if self.boleto_sold and self.folio_inicial:
            self.folio_final = ((self.boleto_sold + self.boleto_cancelated + self.boleto_courtesy + self.boleto_other + self.rob_extrav + self.folio_inicial) -1)

class SiifCorteCajaCfdiLines(models.Model):
    _name = "siif.income.corte.caja.cfdi.lines"
    _description = "Corte Caja CDFI Lines"

    #CFDI
    my_ids = fields.Many2one(MODEL_CORTE_CAJA, STR_LINES_MODEL)
    display_type = fields.Selection([('line_section', "Section"), ('line_note', "Note")], default=False, help=STR_HELP)
    sequence = fields.Integer(string = 'Sequence')
    
    cfdi_id = fields.Many2one(ACCOUNT_MOVE, string = 'CFDI Folio') # Antes nombre_cfdi
    cliente = fields.Char(string = 'Cliente')
    importe_cfdi = fields.Float(string = 'Importe')
    invoice_uuid = fields.Char(string = 'UUID')
    description = fields.Char(string = 'Descripción')
    poliza_contable = fields.Char(string = 'Poliza de Registro Contable')

    dep_id = fields.Many2one(related='my_ids.dependency_id', store=False)
    subdep_id = fields.Many2one(related='my_ids.sub_dependency_id', store=False)
    domain_cfdi_id = fields.Char(compute="_compute_domain_cfdi", store=False)

    @api.onchange('dep_id')
    def onchange_dep_id(self):
        self.dep_id = self.my_ids.dependency_id


    @api.onchange('subdep_id')
    def onchange_subdep_id(self):
        self.subdep_id = self.my_ids.sub_dependency_id
    
    @api.depends('dep_id', 'subdep_id')
    def _compute_domain_cfdi(self):
        for rec in self:
            domain = [('state', '=', 'posted'), ('type_of_revenue_collection', '=', 'billing'), ('invoice_payment_state', '=', 'paid'), ('payment_of_id', '=', 63), ('is_in_cash_closing', '!=', 'True')]
            if rec.dep_id and rec.subdep_id:
                domain.append(('dependancy_id', '=', rec.dep_id.id))
                domain.append(('sub_dependancy_id', '=', rec.subdep_id.id))
            rec.domain_cfdi_id = json.dumps(domain)
        
    @api.onchange('cfdi_id')
    def onchange_nombre_cfdi(self):
        if self.cfdi_id:
            cliente_id = self.env[ACCOUNT_MOVE].search([('id', '=', self.cfdi_id.id), ('state', '=', 'posted')])

            self.cliente = cliente_id.partner_id.name
            self.importe_cfdi = cliente_id.amount_total
            self.invoice_uuid = cliente_id.invoice_uuid

            self.poliza_contable = cliente_id.name + ' ' + "(" + cliente_id.ref + ")"
            descrip = self.env['income.invoice.move.line'].search([('move_id', '=', cliente_id.id)], limit = 1)
            self.description = descrip.name
        if not self.cfdi_id:
            self.poliza_contable = False
            self.cliente = False
            self.description = False
            self.invoice_uuid = False
            self.importe_cfdi = False


class SiifCorteCajaUnitaryLines(models.Model):
    _name = "siif.income.corte.caja.unitary.lines"
    _description = "Corte Caja Unitarios lines"

    #Unitarios
    my_id = fields.Many2one(MODEL_CORTE_CAJA, STR_LINES_MODEL)
    display_type = fields.Selection([('line_section', "Section"), ('line_note', "Note")], default=False, help=STR_HELP)
    sequence = fields.Integer(string = 'Sequence')

    clv_dotacion = fields.Many2one(comodel_name=MODEL_SERIE_DOTACIONES_LINES, string = STR_CVE_DOTACION)
    dep_subdep = fields.Char(string='Dependencia')
    id_boleto = fields.Integer(string='ID Boleto')
    folio_boleto = fields.Integer(string='Folio Boleto')
    boleto_sold = fields.Integer(string='Boleto Vendido')
    boleto_cancelated = fields.Integer(string='Boleto Cancelado')
    boleto_courtesy = fields.Integer(string='Boleto Cortesia')
    boleto_reprinted = fields.Integer(string='Boleto Reimpreso')
    rob_extrav = fields.Integer(string=STR_ROB_EXTRAV)
    boleto_other = fields.Integer(string='Boleto Otro')
    fecha_emision = fields.Char(string="Fecha Emisión") #, default=datetime.today(),)
    fecha_evento = fields.Char(string="Fecha del Evento") #, default=datetime.today(),)
    evento = fields.Char(string='Evento')
    precio_unitario = fields.Float(string='Precio Unitario')
    cfdi_id = fields.Char(string='CFDI Folio')
    concepto_boleto = fields.Char(string='Concepto del Boleto')
    nombre_recinto = fields.Char(string='Nombre del Recinto')
    status_compra = fields.Char(string='Estatus Compra')



class SiifSeriesDependenciaBoletaje(models.Model):
    _name = "siif.income.series.dependencia.boletaje"
    _description = "Serie Dependencia"
    _rec_name = "serie"
    _inherit = [MAIL_THREAD, MAIL_ACTIVITY_MIXIN]

    line_ids_dotaciones = fields.One2many(MODEL_SERIE_DOTACIONES_LINES, 'my_id', 'Lines')
    sequence_tree = fields.Integer(string = 'Sequence')

    serie = fields.Char(string = 'Serie', tracking=True)
    fiscal_year =  fields.Many2one(comodel_name = ACCOUNT_FISCAL_YEAR, string = STR_FISCAL_YEAR)
    def get_domain_dependency(self):
        dep = self.env.user.dependency_income_ids.ids
        dep += [d.id for d in self.env.user.sub_dependency_income_ids.dependency_id]

        return json.dumps([('id', 'in', dep)])
    dependency_id = fields.Many2one('dependency', string='Dependencia', domain=get_domain_dependency)
    sub_dependency_id = fields.Many2one(SUB_DEPENDENCY, string = 'Subdependencia')
    domain_sub_dependency = fields.Char(compute="_compute_domain_sub_dependency", store=False)

    description = fields.Text(string = 'Descripción')
    fecha = fields.Datetime(string='Fecha y hora de Registro', default=datetime.today(), tracking=True)
    total_boletos_asig = fields.Integer(string = 'Total Boletos Asignados') #, compute='_compute_total_boletos_asig')
    total_boletos_comp = fields.Integer(string = 'Total Boletos Comprobados', compute='_compute_boletos_comp', store=True)
    total_boletos_rest = fields.Integer(string = 'Total Boletos Restantes', compute='_compute_total_boletos_rest', store=True)
    porc_boletos_comp = fields.Integer(string = 'Porcentaje Boletos Comprobados', compute='_compute_porc_boletos_comprobados', store=True)
    boletos_aviso = fields.Integer(string = 'Boletos Aviso')
    boletos_restriccion = fields.Integer(string = 'Restantes para Próxima Solicitud')
    porc_boletos_restricción = fields.Integer(string = 'Porcentaje Restantes para Próxima Solicitud')
    total_cost_for_boletos = fields.Float(string="Importe Boletos Comprobados") #, compute='_compute_cost_boletos')
    state = fields.Selection([('draft', 'Borrador'),
                              ('active', 'Activa'),
                              ('inactive', 'Inactiva'),
                              ('cancelated', 'Cancelada'),
                              ('closed', 'Cerrada'),
                              ], string="Estado", default='draft', track_visibility="always")

    tipo_control = fields.Selection([('Rango', 'Rango'),
                                     ('Folios', 'Folios')
                                     ], string=STR_TYPE_CONTROL)
    
    documentary_support = fields.Many2many(IR_ATTACHMENT, string="Agregar Soporte Documental")
    reason_rejection = fields.Text(string='Motivo de Cancelación', tracking=True)

    # Función para hacer solo lectura si el usuario pertenece al grupo jt_income.group_income_boletaje_user_dep o si el total de boletos por comprobar es mayor a 0
    def set_access_user_for_control_type(self):
        self.readonly_user = self.env.user.has_group('jt_income.group_income_boletaje_user_dep') or self.env.user.has_group('jt_income.group_income_boletaje_administrator_dep') or self.total_boletos_comp > 0

    # Campo computado para hacer solo lectura si el usuario pertenece al grupo jt_income.group_income_boletaje_user_dep 
    readonly_user = fields.Boolean(compute=set_access_user_for_control_type, string=STR_TYPE_CONTROL, store=False)
    
    # Armar la serie (secuencia) S(DEP 3)(SUB 2)/(Consecutivo 6)
    @api.model
    def create(self, vals):
        res = super(SiifSeriesDependenciaBoletaje, self).create(vals)
        dependency = vals['dependency_id']
        sub_dependency = vals['sub_dependency_id']
        dependencia = self.env['dependency'].search([('id', '=', dependency)])
        sub_dependencia = self.env[SUB_DEPENDENCY].search([('id', '=', sub_dependency)])
        dep = dependencia.dependency
        sub = sub_dependencia.sub_dependency

        year_fiscal = self.env[ACCOUNT_FISCAL_YEAR].search([('id', '=', vals['fiscal_year'])])
        year = year_fiscal.name
        fiscal_year = ''
        year_sequence = self.env[MODEL_IR_SEQUENCE]
        if year:
            existing_sequence = self.env[MODEL_IR_SEQUENCE].search([('name', 'like', f'Secuencia Serie Dependencia {year}')])
            if existing_sequence:
                fiscal_year = existing_sequence[0].code
            else:
                new_sequence = year_sequence.create({
                    'name': f'Secuencia Serie Dependencia {year}',
                    'code': f'series.dependencias.boletaje.{year}',
                    'padding': 6,
                    'implementation': 'standard',
                })
                fiscal_year = new_sequence.code
        seq = self.env[MODEL_IR_SEQUENCE].next_by_code(fiscal_year)

        res.serie = "S/" + str(dep) + str(sub) + "/" + str(year) + "/" + str(seq)
        return res


    @api.depends('dependency_id')
    def _compute_domain_sub_dependency(self):
        for record in self:
            domain = [('dependency_id', '=', self.dependency_id.id)]
            if not self.dependency_id.id in self.env.user.dependency_income_ids.ids:
                domain.append(('id', 'in', self.env.user.sub_dependency_income_ids.ids))
            record.domain_sub_dependency = json.dumps(domain)

    @api.depends('line_ids_dotaciones.boleto_sold', 'line_ids_dotaciones.boleto_cancelated', 'line_ids_dotaciones.boleto_courtesy', 'line_ids_dotaciones.boleto_other','line_ids_dotaciones.rob_extrav')
    def _compute_boletos_comp(self):
        for record in self:
            record.total_boletos_comp = sum(line.boleto_sold for line in record.line_ids_dotaciones) + sum(line.boleto_cancelated for line in record.line_ids_dotaciones) + sum(line.boleto_courtesy for line in record.line_ids_dotaciones) + sum(line.boleto_other for line in record.line_ids_dotaciones) + sum(line.rob_extrav for line in record.line_ids_dotaciones)

    def action_draft(self):
        self.ensure_one()
        self.state = 'draft'

    def action_active(self):
        self.ensure_one()
        self.state = 'active'

        for rec in self:
            if not rec.line_ids_dotaciones:
                raise ValidationError(_("No puede activar una serie sin que tenga una dotación!!!"))
        
        for rec in self:
            for dot in rec.line_ids_dotaciones:
                if not dot.concepto:
                    raise ValidationError(_("La Clave Dotación %s de esta serie no contiene concepto. Favor de verificar su información!!!" % (dot.clv_dotacion)))

        for rec in self:
            for dot in rec.line_ids_dotaciones:
                if not dot.evento:
                    raise ValidationError(_("La Clave Dotación %s de esta serie no contiene evento. Favor de verificar su información!!!" % (dot.clv_dotacion)))

    def action_inactive(self):
        self.ensure_one()
        self.state = 'inactive'

    def action_cancelated(self):
        self.ensure_one()
        if self.total_boletos_comp > 0:
            raise ValidationError(_("Para poder cancelar una serie debe cumplir con la siguiente regla de negocio: \n\n - El campo Total de Boletos Comprobados debe ser igual a 0 y usted tiene %s boletos comprobados." % (self.total_boletos_comp)))

        if not self.reason_rejection:
            raise ValidationError(_("Para poder Cancelar una serie primero deberá ingresar un motivo de rechazo en el campo correspondiente."))

        if not self.documentary_support:
            raise ValidationError(_("Para poder Cancelar una serie primero deberá adjuntar un documento que soporte la cancelación!!!"))

        else:
            self.state = 'cancelated'

    def action_closed(self):
        self.ensure_one()
        
        if self.total_boletos_asig > 0 and self.total_boletos_comp > 0 and self.total_boletos_rest == 0:
            self.state = 'closed'
        else:
            raise ValidationError(_("Para poder cerrar la serie de cumplir con todas y cada una de las siguientes reglas de negocio: \n\n - El campo Total Boleto Asignados debe ser mayor a 0 y usted tiene %s.\n - El campo Total de Boletos Comprobados debe ser mayor a 0 y usted tiene %s.\n - El campo Total Boletos Restantes debe ser igual a 0 y usted tiene %s. \n\n Si al menos en una no cumple no le dejará cerra la serie." % (self.total_boletos_asig, self.total_boletos_comp, self.total_boletos_rest)))

    def unlink(self):
        if self.state == 'active':
            raise ValidationError(_("Una vez que la SERIE pasa a estado de ACTIVO no se puede eliminar!!!"))
        if self.state == 'inactive':
            raise ValidationError(_("La SERIE no se puede eliminar!!!"))
        res = super(SiifSeriesDependenciaBoletaje, self).unlink()
        return res


    # Función computada para calcular el total de boletos restantes
    @api.depends('total_boletos_asig','total_boletos_comp')
    def _compute_total_boletos_rest(self):
        for rec in self:
            rec.total_boletos_rest = rec.total_boletos_asig - rec.total_boletos_comp

    # Función computada para calcular el porcentaje de boletos utilizados
    @api.depends('total_boletos_asig', 'total_boletos_comp')
    def _compute_porc_boletos_comprobados(self):
        for rec in self:
            if rec.total_boletos_asig == 0:
                rec.porc_boletos_comp = 0
            else:
                rec.porc_boletos_comp = int((rec.total_boletos_comp * 100) / rec.total_boletos_asig)

    #- Método que modifica el css de la vista en Series Dependencia
    x_css = fields.Html(string='CSS', sanitize=False, compute='_compute_css', store=False)
    @api.depends('state')
    def _compute_css(self):
        is_boletaje_administrator_dep = self.env.user.has_group('jt_income.group_income_boletaje_administrator_dep')
        is_boletaje_user_dep = self.env.user.has_group('jt_income.group_income_boletaje_user_dep')
        is_boletaje_user_dgf = self.env.user.has_group('jt_income.group_income_boletaje_user_dgf')
        is_boletaje_director_dgf = self.env.user.has_group('jt_income.group_income_boletaje_director_dgf')
        is_boletaje_admin_global = self.env.user.has_group('jt_income.group_income_admin_user')

        for record in self:
            record.x_css = ""

            if is_boletaje_administrator_dep:
                #Ocultar boton de editar en boletaje cuando se encuentra en los siguientes estados
                if record.state in ['cancelated', 'closed']:
                    record.x_css += """
                        <style>
                            .o_form_view .o_form_statusbar > .o_statusbar_status {
                                max-width:85%;
                            }
                            .o_form_button_edit {
                                display: none !important;
                            }
                        </style>
                    """

            if is_boletaje_user_dep:
                #Ocultar boton de editar en boletaje cuando se encuentra en los siguientes estados
                if record.state in ['closed', 'cancelated']:
                    record.x_css += """
                        <style>
                            .o_form_view .o_form_statusbar > .o_statusbar_status {
                                max-width:85%;
                            }
                            .o_form_button_edit {
                                display: none !important;
                            }
                        </style>
                    """
            if is_boletaje_user_dgf:
                #Ocultar boton de editar en boletaje cuando se encuentra en los siguientes estados
                if record.state in ['cancelated', 'closed' ]:
                    record.x_css += """
                        <style>
                            .o_form_view .o_form_statusbar > .o_statusbar_status {
                                max-width:85%;
                            }
                            .o_form_button_edit {
                                display: none !important;
                            }
                        </style>
                    """

            if is_boletaje_director_dgf:
                #Ocultar boton de editar en boletaje cuando se encuentra en los siguientes estados
                if record.state in ['cancelated', 'closed']:
                    record.x_css += """
                        <style>
                            .o_form_view .o_form_statusbar > .o_statusbar_status {
                                max-width:85%;
                            }
                            .o_form_button_edit {
                                display: none !important;
                            }
                        </style>
                    """
            if is_boletaje_admin_global:
                #Ocultar boton de editar en boletaje cuando se encuentra en los siguientes estados
                if record.state in ['cancelated', 'closed']:
                    record.x_css += """
                        <style>
                            .o_form_view .o_form_statusbar > .o_statusbar_status {
                                max-width:85%;
                            }
                            .o_form_button_edit {
                                display: none !important;
                            }
                        </style>
                    """

            # En cualquiera de los estados, se limita el tamaño del statusbar para evitar tener
            # 'botones gordos'
            else:
                record.x_css += """
                    <style>
                        .o_form_view .o_form_statusbar > .o_statusbar_status{
                            max-width:85%;
                        }
                    </style>
                """


    def logged_user(self):
        is_boletaje_user_dep = self.env.user.has_group('jt_income.group_income_boletaje_user_dep')
        is_boletaje_administrator_dep = self.env.user.has_group('jt_income.group_income_boletaje_administrator_dep')
        is_boletaje_user_dgf = self.env.user.has_group('jt_income.group_income_boletaje_user_dgf')
        is_boletaje_director_dgf = self.env.user.has_group('jt_income.group_income_boletaje_director_dgf')
        is_boletaje_admin_global = self.env.user.has_group('jt_income.group_income_admin_user')
        views = [(self.env.ref('siif_income.siif_income_series_dependencia_boletaje_tree').id, 'tree'), (self.env.ref('siif_income.siif_income_series_dependencia_boletaje_form').id, 'form')]
        domain = []

        if is_boletaje_user_dep:
            domain = [('dependency_id', '=', self.env.user.dependency_income_ids.ids), ('sub_dependency_id', '=', self.env.user.sub_dependency_income_ids.ids)]

        if is_boletaje_admin_global:
            domain = [('dependency_id', '=', self.env.user.dependency_income_ids.ids), ('sub_dependency_id', '=', self.env.user.sub_dependency_income_ids.ids)]

        if is_boletaje_administrator_dep:
            domain = [('dependency_id', '=', self.env.user.dependency_income_ids.ids), ('sub_dependency_id', '=', self.env.user.sub_dependency_income_ids.ids)]

        if is_boletaje_user_dgf:
            domain = [('dependency_id', '=', self.env.user.dependency_income_ids.ids), ('sub_dependency_id', '=', self.env.user.sub_dependency_income_ids.ids)]

        if is_boletaje_director_dgf:
            domain = [('dependency_id', '=', self.env.user.dependency_income_ids.ids), ('sub_dependency_id', '=', self.env.user.sub_dependency_income_ids.ids)]

        return {
            'name': 'Serie Dependencia',
            "res_model": SIIF_INCOME_SERIES_DEP,
            "domain": domain,
            "type": MODEL_IR_ACT_WINDOW,
            "view_mode": STR_VIEW_MODE,
            "views": views,
        }
class SiifSeriesDotacionesLines(models.Model):
    _name = "siif.income.series.dotaciones.lines"
    _description = "Series Dotaciones Lines"
    _rec_name = "clv_dotacion"

    my_id = fields.Many2one(SIIF_INCOME_SERIES_DEP, STR_LINES_MODEL, ondelete="cascade")
    display_type = fields.Selection([('line_section', "Section"), ('line_note', "Note")], default=False, help=STR_HELP)
    sequence = fields.Integer(string = 'Sequence')

    clv_dotacion = fields.Char(string = STR_CVE_DOTACION)
    concepto = fields.Char(string = 'Concepto')
    evento = fields.Char(string = 'Evento')
    cantidad = fields.Integer(string = 'Cantidad')
    boleto_sold = fields.Integer(string='Boletos Vendidos')
    boleto_courtesy = fields.Integer(string='Boletos Cortesias')
    boleto_reprinted = fields.Integer(string='Boletos Reimpresos')
    boleto_other = fields.Integer(string='Boletos Otros')
    folios_rest = fields.Integer(string = 'Boletos Por Comprobar', compute="_compute_folio_rest", store=True)
    monto_comp = fields.Float(string = 'Monto de Boletos Comprobados')
    boleto_cancelated = fields.Integer(string = 'Boletos Cancelados')
    rob_extrav = fields.Integer(string = STR_ROB_EXTRAV)


    # Armar la dotación (secuencia) S(DEP 3)(SUB 2)/(Consecutivo 6)
    @api.model
    def create(self, vals):
        res = super(SiifSeriesDotacionesLines, self).create(vals)
        dependencia = self.env[SIIF_INCOME_SERIES_DEP].search([('id', '=', vals['my_id'])])
        dep = dependencia.dependency_id.dependency
        subdep = dependencia.sub_dependency_id.sub_dependency

        year_fiscal = self.env[SIIF_INCOME_SERIES_DEP].search([('id', '=', vals['my_id'])])
        year = year_fiscal.fiscal_year.name
        fiscal_year = ''
        year_sequence = self.env[MODEL_IR_SEQUENCE]
        if year:
            existing_sequence = self.env[MODEL_IR_SEQUENCE].search([('name', 'like', f'Secuencia Serie Dotación {year}')])
            if existing_sequence:
                fiscal_year = existing_sequence[0].code
            else:
                new_sequence = year_sequence.create({
                    'name': f'Secuencia Serie Dotación {year}',
                    'code': f'seq.series.dotacion.boletaje.{year}',
                    'padding': 6,
                    'implementation': 'standard',
                })
                fiscal_year = new_sequence.code
        seq = self.env[MODEL_IR_SEQUENCE].next_by_code(fiscal_year)

        res.clv_dotacion = "D/" + str(dep) + str(subdep) + "/" + str(year) + '/' + str(seq)
        return res

    @api.depends('cantidad','boleto_sold', 'boleto_cancelated', 'boleto_courtesy', 'boleto_other', 'rob_extrav')
    def _compute_folio_rest(self):
        for rec in self:
            rec.folios_rest = (rec.cantidad - (rec.boleto_sold + rec.boleto_cancelated + rec.boleto_courtesy + rec.boleto_other + rec.rob_extrav))
