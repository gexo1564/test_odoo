import io
from ..layouts import Layout, ResultLine

class LayoutBBVASIT(Layout):

    def load_file(self, file_data):
        # io.StringIO(file_data.decode("utf-8"))
        self.file = io.StringIO(file_data.decode("ISO-8859-1")) #open(file, encoding="ISO-8859-1")

    def load_body(self):
        return self.file.readlines()[3:]

    def close_file(self):
        self.file.close()

    def read_line(self, line):
        concepts = {
            "1": "sueldo",
            "2": "prestacion",
        }
        payment_type = concepts.get(line[7], "unknown")
        rfc = line[8:22].strip()
        amount = float("%s.%s" % (line[50:63], line[63:65]))
        bank_account = line[34:50]
        status = line[72:152].strip()
        ok = "O P E R A C I O N    E X I T O S A" in status or "OK" in status
        return ResultLine(ok, payment_type, {'rfc': rfc, 'amount': amount, 'bank_account': bank_account}, status)

    def get_ids_search_employee_payroll_file(self, payroll_processing_id: int, lines: list):
        rfc = tuple([l.columns.get('rfc') for l in lines])
        # Se obtienen los ids que hacen match con el criterio de búsqueda
        query = """
            select e.rfc, r.id
            from employee_payroll_file r
            join hr_employee e on r.employee_id = e.id
            where e.rfc in %s
            and r.payroll_processing_id = %s
        """
        self.env.cr.execute(query, (rfc, payroll_processing_id))
        result_set = {r[0]: r[1:] for r in self.env.cr.fetchall()}
        lines_ok, lines_err = [], []
        for line in lines:
            rfc = line.columns.get('rfc')
            res = result_set.get(rfc, None)
            if res:
                line.res_id = res[0]
                lines_ok.append(line)
            else:
                line.res_id = None
                line.error_message = "No se encontró un registro para el empleado con rfc %s" % rfc
                lines_err.append(line)
        return lines_ok, lines_err

    def get_ids_search_account_payment(self, batch_folio: str, lines: list):
        # Se obtienen los ids que hacen match con el criterio de búsqueda
        query = """
            select r.vat rfc, p.id, p.amount
            from account_payment p, res_partner_bank a, res_partner r
            where p.payment_bank_account_id = a.id
            and p.partner_id = r.id
            and batch_folio = %s
        """
        self.env.cr.execute(query, (batch_folio,))
        result_set = {r[0]: r[1:] for r in self.env.cr.fetchall()}
        lines_ok, lines_err = [], []
        for line in lines:
            rfc = line.columns.get('rfc')
            res = result_set.get(rfc, None)
            if res:
                if res[1] == line.columns.get('amount'):
                    line.res_id = res[0]
                    lines_ok.append(line)
                else:
                    line.res_id = None
                    line.error_message = f"El monto del pago ({line.columns.get('amount'):,.2f}) del beneficiario {rfc} no coincide con el registrado ({res[1]:,.2f})."
                    lines_err.append(line)
            else:
                line.res_id = None
                line.error_message = f"No se encontró un registro para el beneficiario {rfc}"
                lines_err.append(line)
        return lines_ok, lines_err