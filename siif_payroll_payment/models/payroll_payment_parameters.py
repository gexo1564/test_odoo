# -*- coding: utf-8 -*-

import logging
from odoo import models, fields, api, _
from datetime import datetime
from odoo.exceptions import UserError, ValidationError

class PayrollPaymentParameters(models.Model):

    _name = 'payroll.payment.parameters'

    name = fields.Char(default='Parámetros', store=False)
    partner_id = fields.Many2one('res.partner', 'Payroll beneficiary')
    partner_cash_id = fields.Many2one('res.partner', 'Beneficiary of cash payments')
    journal_id = fields.Many2one('account.journal', 'Journal')

    @api.model
    def create(self, vals):
        count = self.env['payroll.payment.parameters'].search_count([])
        if count >= 1:
            raise ValidationError("Solo se puede configurar un registro de parámetros")
        return super().create(vals)

    def name_get(self):
        return [(record.id, 'Parámetros de Pago de Nómina') for record in self]