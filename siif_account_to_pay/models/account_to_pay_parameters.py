# -*- coding: utf-8 -*-

import logging
from odoo import models, fields, api, _
from datetime import datetime
from odoo.exceptions import UserError, ValidationError

class PayrollPaymentParameters(models.Model):

    _name = 'account.to.pay.parameters'

    name = fields.Char(default='Parámetros', store=False)
    item_capitalization_ids = fields.Many2many('expenditure.item', string='Items that capitalize with the exception of the UMA')
    operation_type_cxp_general = fields.Many2one('operation.type', string='Operation Type Account to Pay General')
    operation_type_cxp_papiit = fields.Many2one('operation.type', string='Operation Type Account to Pay PAPIIT')

    @api.model
    def create(self, vals):
        count = self.env['account.to.pay.parameters'].search_count([])
        if count >= 1:
            raise ValidationError("Solo se puede configurar un registro de parámetros")
        return super().create(vals)

    def name_get(self):
        return [(record.id, 'Parámetros de Cuentas por Pagar') for record in self]

    def get_param(self, param):
        params = self.search([], limit=1)
        if not params:
            raise ValidationError("Configure los parámetros de las Cuentas por Pagar en el menú Configuración.")
        if param == 'item_capitalization':
            return params.item_capitalization_ids
        elif param == 'op_cxp_general':
            if not params.operation_type_cxp_general:
                raise ValidationError("Configure el tipo de operación para Cuentas por Pagar tipo general en el menú Configuración.")
            return params.operation_type_cxp_general
        elif param == 'op_cxp_papiit':
            if not params.operation_type_cxp_papiit:
                raise ValidationError("Configure el tipo de operación para Cuentas por Pagar tipo PAPIIT en el menú Configuración.")
            return params.operation_type_cxp_papiit
