# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields,api,_
from odoo.exceptions import ValidationError
from datetime import date
import json
from lxml import etree
import logging

"""Modelos"""
APP_REQUEST='application.request'
class Quotes(models.Model):
	#Cotizaciones
	_name = 'quote'
	_inherit = ['mail.thread', 'mail.activity.mixin']
	_description = 'Quotes'


	name = fields.Char(string='Name')
	status = fields.Selection(
			[
				('draft', 'Draft'),
				('published', 'Published'),
			], string="Denomination",default='draft',tracking=True)
	def _compute_app_request(self):
		domain= [('status','=','published')]
		return json.dumps(domain)

	application_request = fields.Many2one(APP_REQUEST,'Application Request',required=True, domain=_compute_app_request)
	amount_app_request = fields.Monetary('Amount App',currency_field='currency_id',compute='_get_amount',tracking=True)
	currency_fund_id = fields.Many2one('currency.funds',compute='_get_currency_fund',store=True,tracking=True)
	
	denominations_quote_ids = fields.One2many('total.denominations.quote','quote_id',string='Total Denominations',tracking=True)
	amount_denomination_quote = fields.Monetary('Amount Denomination of Quote',currency_field='currency_id',compute='_compute_amount_funds',default=0)
	
	bank_id = fields.Many2one('res.bank','Bank',tracking=True)
	quote_date = fields.Date('Quote Date',default=date.today(),tracking=True)
	currency_id = fields.Many2one('res.currency', default=lambda self: self.env.user.company_id.currency_id, string="Currency",tracking=True)
	
	exchange_rate_number = fields.Float(string='Exchange Rate', digits = (12, 2),tracking=True)
	amount_currency = fields.Integer('Amount of Currencies',tracking=True)
	withdrawal_amount = fields.Monetary('Withdrawal Amount', currency_field='currency_id', compute='_get_withdrawal_amount',tracking=True)
	
	equal_denominations =  fields.Boolean('Equal Denominations from Application Request',default=True)
	observations = fields.Char('Observations',tracking=True)
	doc_quote = fields.Binary('Document Quote',tracking=True)
	doc_quote_name = fields.Char(string='Doc Quote Name')
	selected_quote = fields.Boolean('Selected Quote',default=False,tracking=True)
	is_app_request_published =fields.Boolean('Is Application Request Published',compute='_get_status_app_request',store=False)
	#---------------------------------------------------------
	#					REGISTRO
	#---------------------------------------------------------
	@api.model
	def create(self, vals):
		app_request = self.env[APP_REQUEST].search([('id','=',vals.get('application_request'))])
		vals.update({'name': app_request.sequence_quote_id.next_by_id()})
		res = super(Quotes, self).create(vals)
		return res

	#---------------------------------------------------------
	#					REVISIÓN DE DATOS
	#---------------------------------------------------------

	#Revisión del monto
	@api.constrains('exchange_rate_number')
	def _check_amount_funds(self):
		if self.exchange_rate_number<=0:
			raise ValidationError(_('Exchange rate number cant be 0 o negative'))

	@api.onchange('equal_denominations','application_request','denominations_quote_ids')
	def partner_onchange(self):
		if self.equal_denominations:
			if self.application_request != False:
				self.amount_currency = len(self.application_request.total_denomination)
			else:
				self.amount_currency = 0
		else:
			self.amount_currency = len(self.denominations_quote_ids)
	
	@api.onchange('equal_denominations')
	def _onchange_equal_denomination(self):
		for rec in self:
			if rec.equal_denominations==False and len(rec.denominations_quote_ids)!=0:
				for den in rec.denominations_quote_ids:
					den.unlink()

	#Revisa si la cotización está publicada
	@api.depends('application_request')
	def _get_status_app_request(self):
		if self.application_request.status=='published':
			self.is_app_request_published=True
		else:
			self.is_app_request_published=False

	#Revisar si el monto de las denominaciones y el monto de la solicitud son iguales
	def check_equal_denominations(self,record):
		quote_amount=record.amount_denomination_quote
		app_req_amount=record.application_request.amount_denomination
		if quote_amount==app_req_amount:
			return True
		return False

	#Se calcula el monto de las denominaciones
	@api.depends('denominations_quote_ids')
	def _compute_amount_funds(self):
		for record in self:
			den=record.denominations_quote_ids
			if den != False:
				amount=0
				den_info={
					'den_500':500.00,
					'den_200':200.00,
					'den_100':100.00,
					'den_50':50.00,
					'den_20':20.00,
					'den_10':10.00,
					'den_5':5.00,
					'den_2':2.00,
					'den_1':1.00,
				}
				for i in den:
					#logging.critical("info"+str(i))
					if i.count != False and i.denominations != False:
						amount = amount+round(i.count*den_info[i.denominations],2)
				record.amount_denomination_quote = amount
				#Caso en que no hay denominaciones	
			else:
				record.amount_denomination_quote = 0.0

	@api.depends('amount_denomination_quote','exchange_rate_number')
	def _get_withdrawal_amount(self):
		for rec in self:
			if rec.equal_denominations:
				rec.withdrawal_amount = round(rec.exchange_rate_number*rec.amount_app_request,2)
			else:
				rec.withdrawal_amount = round(rec.exchange_rate_number*rec.amount_denomination_quote,2)

	@api.depends('application_request')
	def _get_amount(self):
		self.amount_app_request=self.application_request.amount_funds

	@api.depends('application_request')
	def _get_currency_fund(self):
		self.currency_fund_id=self.application_request.currency_fund_id


	#---------------------------------------------------------
	#					ACCIONES DE VISTA
	#---------------------------------------------------------
	def action_draft(self):
		for record in self:
			if record.is_app_request_published:
				record.status = 'draft'
				self.assign_best_quote(record)
			else:
				raise ValidationError(_('The quote cant change to draft if the application request is now quoted.'))

			
	def action_published(self):
		for record in self:
			if record.is_app_request_published:
				if record.equal_denominations==False:
					if self.check_equal_denominations(record):
						record.status = 'published'
						self.assign_best_quote(record)
					else:
						raise ValidationError(_('The denominations dont quantify the application request amount, check.'))
				else:
					self.create_denominations(record)
					record.status = 'published'
					self.assign_best_quote(record)
			else:
				raise ValidationError(_('The quote cant change to published if the application request is now quoted.'))
		
	#Asignar la cotización más barata
	def assign_best_quote(self,rec):
		quotes=self.env[APP_REQUEST].search([('id','=',rec.application_request.id),('status','=','published')]).published_quotes
		if len(quotes)!=0:
			quotes=sorted(quotes,key=lambda quotes: quotes.withdrawal_amount)
			for i in range(0,len(quotes)):
				if i == 0:
					quotes[i].selected_quote=True
				else:
					quotes[i].selected_quote=False
	
	#Función para crear y asignar las denominaciones iguales a la de la solicitud
	def create_denominations(self,record):
		new_info=[]
		if len(record.denominations_quote_ids)!=len(record.application_request.total_denomination):
			for den_quote in record.denominations_quote_ids:
				den_quote.unlink()
		
			for den in record.application_request.total_denomination:
				info={
					'quote_id':record.id,
					'count':den.count,
					'denominations':den.denominations}
				new_info.append(self.env['total.denominations.quote'].create(info))
			
			record.denominations_quote_id=new_info


	#Función de seguridad en Módulo de Finanza
	@api.model
	def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
		res = super().fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
		doc = etree.XML(res['arch'])

		is_admin_finance = self.env.user.has_group('jt_payroll_payment.group_on_finance_admin')
		is_finance_user = self.env.user.has_group('jt_payroll_payment.group_admin_finance_user')
		is_quote_user = self.env.user.has_group('jt_currency_purchase_req.group_quotes')

		"""
		Sección de Seguridad aplicado para:
			Módulo de Finanzas:
			-Solicitudes de recursos para la compra de divisas
				-Cotizaciones
		"""
		for node in doc.xpath("//" + view_type):
			if is_admin_finance or is_finance_user or is_quote_user:
				node.set('edit', '1')
				node.set('create', '1')
				node.set('delete', '1')
				node.set('import', '1')
				node.set('export_xlsx', '1')
				node.set('export', '1')
			else:
				node.set('edit', '0')
				node.set('create', '0')
				node.set('delete', '0')
				node.set('import', '0')
				node.set('export_xlsx', '0')
				node.set('export', '0')

		if 'form' in res['name']:
			#Botones de la barra de estados "Borrador y Publicar"
			buttons=['action_draft','action_published']
			for node in doc.xpath("//button"):
				if not(is_admin_finance or is_finance_user or is_quote_user) and (node.attrib['name'] in buttons):
					node.set('modifiers','{"invisible":true}')
				else:
					node.set('modifiers','{"invisible":false}')
					
		res['arch'] = etree.tostring(doc)

		return res