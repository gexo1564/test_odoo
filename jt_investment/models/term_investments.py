# -*- coding: utf-8 -*-
import logging
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
from datetime import datetime, timedelta, date
import calendar
import json
import pandas as pd

TERM_INVESTMENTS_MODEL = 'term.investments'
ACCOUNT_MOVE_MODEL = 'account.move'
INVESTMENT_PERIOD_RATE_MODEL = 'investment.period.rate'
DATE_FORMAT = '%Y-%m-%d'

class TermInvestments(models.Model):
    _name = TERM_INVESTMENTS_MODEL
    _description = 'Investment'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char("Name", required=True, default="")
    state = fields.Selection(
        [
            ('draft', 'Draft'),
            ('registered', 'Registered'),
            ('requested', 'Requested'),
            ('confirmed', 'Confirmed'),
            ('approved', 'Approved'),
            ('finished', 'Finished'),
        ], default='draft', track_visibility="always",
    )
    bank_account_id = fields.Many2one('account.journal', "Bank")
    accounting_account_id = fields.Many2one(
        "account.account", "Accounting account", domain="[('code','like','113.%%%.%%%.001')]")
    accrued_interest_journal_id = fields.Many2one(
        'account.journal', "Accrued Interest Journal")
    investment_amount = fields.Float('Investment Amount')
    investment_date = fields.Date('Investment Date', default=fields.Date.today)
    expiration_date = fields.Date('Expiration Date')
    term = fields.Integer('Term')
    currency_id = fields.Many2one(
        'res.currency', 'Currency', default=lambda self: self.env.company.currency_id)
    rate_type = fields.Selection([('fixed_rate', 'Fixed Rate'),
                                  ('variable_rate', 'Variable Rate'), ],
                                 'Rate Type')
    fixed_rate = fields.Float('Fixed Rate', digits=(
        2, 4), help="The value must be positive.")
    indicator = fields.Selection([('tiie', 'TIIE'),
                                  ('cetes', 'CETES'), ],
                                 'Indicator')
    surcharge = fields.Float('Surcharge', help="Value with 2 decimal places.")
    percentage = fields.Float('Percentage')
    line_ids = fields.One2many(
        'account.move.line', 'term_investments_id', string="Accounting Notes")
    accrued_interest_ids = fields.One2many(
        'accrued.interest', 'term_investments_id', string="Accrued Interest")
    # Si ya expiro y no volver a tener intereses devengados
    is_expired = fields.Float('Is expired')
    calculation_type = fields.Selection([('surcharge', 'Surcharge'),
                                         ('percentage', 'Percentage'), ],
                                         'Calculation Type')
    days_elapsed = fields.Integer('Days Elapsed', readonly = True)
    documentary_support = fields.Many2many("ir.attachment", string="Add documents")
    ## PARA TEST ##
    current_date = fields.Date('Fecha actual(TEST)',default=fields.Date.today)    
    

    @api.model
    def create(self, vals):
        if 'investment_amount' in vals:
            vals['investment_amount'] = abs(vals['investment_amount'])

        if 'investment_date' in vals and 'term' in vals:
            investment_date = vals['investment_date']
            term = vals['term'] 
            vals['expiration_date'] = self.set_expiration_date_in_business_day(investment_date, term)
            
        if 'fixed_rate' in vals:
            vals['fixed_rate'] = abs(vals['fixed_rate'])

        if 'surcharge' in vals:
            vals['surcharge'] = abs(vals['surcharge'])

        if 'percentage' in vals:
            vals['percentage'] = abs(vals['percentage'])

        result = super(TermInvestments, self).create(vals)
        return result


    def write(self,vals):
        if 'investment_date' in vals and 'term' in vals:
            investment_date = vals['investment_date']
            term = vals['term']
            vals['expiration_date'] = self.set_expiration_date_in_business_day(investment_date, term)
        
        if 'fixed_rate' in vals:
            vals['fixed_rate'] = abs(vals['fixed_rate'])

        if 'surcharge' in vals:
            vals['surcharge'] = abs(vals['surcharge'])

        if 'percentage' in vals:
            vals['percentage'] = abs(vals['percentage'])        
                
        res = super(TermInvestments,self).write(vals)
        return res
    

    def set_expiration_date_in_business_day(self, investment_date, term):
        term = term + 1
        expiration_date = datetime.strptime(str(investment_date), DATE_FORMAT) + timedelta(days=term)
        ts_expiration_date = pd.Timestamp(str(expiration_date))
        offset = pd.tseries.offsets.BusinessDay()
        date_in_business_day = ts_expiration_date - offset
    
        return date_in_business_day.date()
    
    def update_state(self, new_state):
        self.state = new_state
    
    def register(self):
        # self.approve()
        self.update_state('registered')
        
    def confirm(self):
        return self.action_confirm()

    def action_requested(self):
        self.state = 'requested'

    def action_approved(self):
        self.state = 'confirmed'        

    def action_confirm(self):
        today = datetime.today().date()
        user = self.env.user
        employee = self.env['hr.employee'].search([('user_id', '=', user.id)], limit=1)
        fund_type = False
            
        return {
            'name': _('Approve Request'),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': False,
            'res_model': 'approve.money.market.bal.req',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': {
                'default_amount': self.investment_amount,
                'default_date': today,
                'default_employee_id': employee.id if employee else False,
                'default_term_investment_id' : self.id,
                'default_fund_type' : fund_type,
                # 'default_bank_account_id' : self.bank_account_id and self.bank_account_id.id or False,
                'show_for_supplier_payment':1,
                'default_desti_bank_account_id': self.bank_account_id and self.bank_account_id .id or False,
            }
        }

    def approve(self):
        if not self.accounting_action():
            raise UserError(
                _('The accounting entries could not be generated.'))

        self.update_state('approved')
    # INVERSION
    def accounting_action(self):
        move_id = ''
        today = self.investment_date#datetime.today().date()
        partner_id = self.env.user.partner_id
        company_id = self.env.user.company_id
        move_obj = self.env[ACCOUNT_MOVE_MODEL]
        # default_debit_account_id
        move_vals = {'ref': '', 'conac_move': False,
                            'date': today, 'journal_id': self.bank_account_id.id, 'company_id': company_id.id,
                            'line_ids': [
                                (0, 0, {
                                    'name':	 f'Inversión a plazo {today}',
                                    'account_id': self.accounting_account_id.id,
                                    'debit': self.investment_amount,
                                    'partner_id': partner_id.id,
                                    'term_investments_id': self.id,
                                }),
                                (0, 0, {
                                    'name':	 f'Inversión a plazo {today}',
                                    'account_id': self.bank_account_id.default_credit_account_id.id,
                                    'credit': self.investment_amount,
                                    'partner_id': partner_id.id,
                                    'term_investments_id': self.id,
                                }),
                            ]}

        move_id = move_obj.create(move_vals)
        move_id.action_post()

        return move_id
    # INTERESES DEVENGADOS
    def accounting_accrued_interest(self, accrued_interest, investments, current_date):
        move_id = ''
        today = self.last_day_month(current_date)
        partner_id = self.env.user.partner_id
        company_id = self.env.user.company_id
        move_obj = self.env[ACCOUNT_MOVE_MODEL]

        move_vals = {'ref': '', 'conac_move': False,
                            'date': today, 'journal_id': investments.accrued_interest_journal_id.id, 'company_id': company_id.id,
                            'line_ids': [
                                (0, 0, {
                                'name':	 f'Interes devengado {today}',
                                'account_id': investments.accrued_interest_journal_id.default_debit_account_id.id,
                                'debit': accrued_interest,
                                'partner_id': partner_id.id,
                                'term_investments_id': investments.id,
                                }),
                                (0, 0, {
                                    'name':	 f'Interes devengado {today}',
                                    'account_id': investments.accrued_interest_journal_id.interest_account_id.id,
                                    'credit': accrued_interest,
                                    'partner_id': partner_id.id,
                                    'term_investments_id': investments.id,
                                }),
                            ]}

        move_id = move_obj.create(move_vals)
        move_id.action_post()

        return move_id
    # CANCELACION INTERESES
    def accounting_generated_interest(self, generated_interest, investments, current_date):
        move_id = ''
        today = self.last_day_month(current_date) + timedelta(days=1)
        partner_id = self.env.user.partner_id
        company_id = self.env.user.company_id
        move_obj = self.env[ACCOUNT_MOVE_MODEL]

        total_accrued_interest = round(self.total_accrued_interest(investments), 2)

        move_vals = {'ref': '', 'conac_move': False,   
                            'date': today, 'journal_id': investments.accrued_interest_journal_id.id, 'company_id': company_id.id,
                            'line_ids': [
                                (0, 0, {
                                    'name':	 f'Cancelación de los intereses reales {today}',
                                    'account_id': investments.bank_account_id.default_debit_account_id.id,
                                    'debit': total_accrued_interest,
                                    'partner_id': partner_id.id,
                                    'term_investments_id': investments.id,
                                }),
                                (0, 0, {
                                    'name':	 f'Cancelación de los intereses reales {today}',
                                    'account_id': investments.accrued_interest_journal_id.default_credit_account_id.id,#investments.accrued_interest_journal_id.default_credit_account_id.id,
                                    'credit': total_accrued_interest,
                                    'partner_id': partner_id.id,
                                    'term_investments_id': investments.id,
                                }),
                            ]}

        move_id = move_obj.create(move_vals)
        move_id.action_post()
        # Regreso al banco
        self.accounting_returned_bank(total_accrued_interest, investments, current_date)

        return move_id
    # VENCIMIENTO Y LIQUIDACIÓN
    def accounting_returned_bank(self, total_accrued_interest, investments, current_date):
        today = self.last_day_month(current_date) + timedelta(days=1)
        partner_id = self.env.user.partner_id
        company_id = self.env.user.company_id
        move_obj = self.env[ACCOUNT_MOVE_MODEL]

        move_vals = {'ref': '', 'conac_move': False,   
                            'date': today, 'journal_id': investments.accrued_interest_journal_id.id, 'company_id': company_id.id,
                            'line_ids': [
                                (0, 0, {
                                    'name':	 f'Vencimiento y liquidación de la Inv. A plazo {today}',
                                    'account_id': investments.bank_account_id.default_debit_account_id.id,
                                    'debit': investments.investment_amount,#total_accrued_interest,
                                    'partner_id': partner_id.id,
                                    'term_investments_id': investments.id,
                                }),
                                (0, 0, {
                                    'name':	 f'Vencimiento y liquidación de la Inv. A plazo {today}',
                                    'account_id': investments.accounting_account_id.id,
                                    'credit': investments.investment_amount, #total_accrued_interest,
                                    'partner_id': partner_id.id,
                                    'term_investments_id': investments.id,
                                }),
                      
                            ]}

        move_id = move_obj.create(move_vals)
        move_id.action_post()
           

    def total_accrued_interest(self, investments):
        accrued_interest = sum(x.accrued_interest for x in investments.accrued_interest_ids)

        return accrued_interest


    @api.constrains('bank_account_id')
    def _check_bank_account_id(self):
        if not self.bank_account_id:
            raise UserError(
                _('Select a bank.'))

    @api.constrains('accounting_account_id')
    def _check_accounting_account_id(self):
        if not self.accounting_account_id:
            raise UserError(
                _('Select an accounting account.'))

    @api.constrains('accrued_interest_journal_id')
    def _check_accrued_interest_journal_id(self):
        if not self.accrued_interest_journal_id:
            raise UserError(
                _('Select an accrued interest journal.'))

    @api.constrains('investment_amount')
    def _check_investment_amount(self):
        if self.investment_amount <= 0:
            raise UserError(
                _('The amount of the investment must be greater than 0.'))


    @api.constrains('rate_type')
    def _check_fixed_rate(self):
        if self.rate_type == 'fixed_rate':
            if self.fixed_rate <= 0:
                raise UserError(
                    _('The fixed rate must be greater than 0.'))

        elif self.rate_type == 'variable_rate':
            if not self.surcharge and not self.percentage:
                raise UserError(
                    _('Indicate surcharge or percentage.'))


    @api.constrains('term')
    def _check_term(self):
        if self.term <= 0:
            raise UserError(
                _('Term must be greater than 0.'))

    @api.constrains('surcharge', 'percentage')
    def _check_only_once(self):
        if self.surcharge and self.percentage:
            raise UserError(
                _('You can only select surcharge or percentage at a time.'))

    def is_expiration_date_investment(self, expiration_date, current_date):
        # Es fecha de vencimiento
        if expiration_date == current_date:
            return True
    
    def set_expired_finished(self, investments):
        investments.is_expired = True
        investments.state = 'finished'
     
    def interest_generated_last_business_day_month(self, investments, current_date):
        generated_interest = 0
        term = investments.term

        generated_interest = self.accrued_interest(investments, term, current_date)
        self.interest_accrued_last_business_day_month(investments, current_date)
        self.accounting_generated_interest(generated_interest, investments, current_date)

    def is_last_business_day_month(self, current_date):
        current_date_pd = pd.Timestamp(current_date)
        last_business_day_month = pd.tseries.offsets.BMonthEnd().rollforward(current_date)

        if current_date_pd == last_business_day_month:
            return True

    def update_elapsed_days(self, current_date, investments):
        investment_date = investments.investment_date
        init_date = datetime.strptime(str(investment_date), DATE_FORMAT)
        final_date = datetime.strptime(str(current_date), DATE_FORMAT)
        difference = final_date - init_date
        investments.days_elapsed = difference.days + 1
        #! Hay una diferencia de 1 dia porque empieza a contar desde el dia de la inversion
        if investments.days_elapsed > investments.term:
            investments.days_elapsed = investments.term

    def interest_accrued(self):
        term_investements_obj = self.env[TERM_INVESTMENTS_MODEL]
        term_investments = term_investements_obj.search([('is_expired', '=', False), ('state', '=', 'approved')])
        ##### TEST EN CASCADA #####
        # current_date = datetime.today().date()# 
        for investments in term_investments:
            current_date = self.set_current_day(investments)
            expiration_date = investments.expiration_date
            # Actualizar dias transcurridos
            self.update_elapsed_days(current_date, investments)
            # Verifica si es fecha de expiracion 
            if self.is_expiration_date_investment(expiration_date, current_date):
                self.interest_generated_last_business_day_month(investments, current_date)
                self.set_expired_finished(investments)

            elif self.is_last_business_day_month(current_date): 
                self.interest_accrued_last_business_day_month(investments, current_date)

    #! Se cambiara por la fecha actual datetime.now()        
    def set_current_day(self, investments):
        current_day = investments.current_date

        return current_day

    def interest_accrued_last_business_day_month(self, investments, current_date):
        days_period = self.days_period(investments, current_date)
        accrued_interest = self.accrued_interest(investments, days_period, current_date)
        # Si ya se hizo el interes devengado en esa fecha
        if not self.has_accrued_interest_on_date(investments, current_date):
            self.accounting_accrued_interest(accrued_interest, investments, current_date)
            self.insert_accrued_interest(investments, accrued_interest, days_period, current_date)


    def has_accrued_interest_on_date(self, investments, current_date):
        return any(investment.date == current_date for investment in investments.accrued_interest_ids)

    def is_fixed_rate(self, rate_type):
        if rate_type == 'fixed_rate':
            return True

    def is_variable_rate(self, rate_type):
        if rate_type == 'variable_rate':            
            return True

    def accrued_interest(self, investments, days_period, current_date):
        accrued_interest = 0
        rate_type = investments.rate_type
        investment_amount = investments.investment_amount

        if self.is_fixed_rate(rate_type):
            fixed_rate = investments.fixed_rate
            accrued_interest = self.accrued_if_fixed_rate(investment_amount, fixed_rate, days_period)

        if self.is_variable_rate(rate_type):
            type_indicator = investments.indicator
            accrued_interest = self.accrued_if_variable_rate(days_period, current_date, type_indicator, investments)    

        return accrued_interest
    
    def insert_accrued_interest(self, investments, accrued_interest, days_period, current_date):
        investments.accrued_interest_ids = [
        (0, 0, {
            'accrued_interest': accrued_interest,
            'days_period_elapsed': days_period,
            'date': current_date,
        })
        ]    

    def accrued_if_variable_rate(self, days_period, current_date, type_indicator, investments):
        interest = 0
        investment_date = investments.investment_date
        investment_amount = investments.investment_amount

        indicator = (self.indicator_average(
            investment_date, current_date, type_indicator)) / days_period

        
        if self.is_percentage(investments):
            percentage = investments.percentage
            interest = self.interest_with_percentage(investment_amount, indicator, days_period, percentage)    
  
        elif self.is_surcharge(investments):
            surcharge = investments.surcharge
            interest = self.interest_with_surcharge(investment_amount, indicator, days_period, surcharge)
        
        interest_round = round(interest, 4)
        return interest_round

    def accrued_if_fixed_rate(self, investment_amount, fixed_rate, days_period):
        interest = 0
        interest = investment_amount * \
            (((fixed_rate/100) / 360) * days_period)

        interest_round = round(interest, 4)
        return interest_round        

    def is_percentage(self, investments):
        if investments.percentage:
            return True

    def interest_with_percentage(self, investment_amount, indicator, days_period, percentage):
        interest = investment_amount * (((indicator * (percentage / 100)) / 360) * days_period)            
        
        return interest

    def is_surcharge(self, investments):
        if investments.surcharge:
            return True

    def interest_with_surcharge(self, investment_amount, indicator, days_period, surcharge):
        interest = investment_amount * (((indicator + (surcharge / 100)) / 360) * days_period)            
        
        return interest        

    
    def days_period(self, investments, current_date):
        days_period = 0

        if current_date.month == investments.investment_date.month and current_date.year == investments.investment_date.year:
            days_period = (current_date.day - investments.investment_date.day) + 1
        elif current_date == investments.expiration_date:
            days_period = investments.expiration_date.day
        else:
            days_period = calendar.monthrange(current_date.year, current_date.month)[1]

        return days_period 

    def indicator_is_tiie(self, type_indicator):
        if type_indicator == 'tiie':
            return True

    def indicator_is_cetes(self, type_indicator):
        if type_indicator == 'cetes':
            return True         
  
    def indicator_average(self, investment_date, current_date, type_indicator):
        rate_values = 0
        length = 0

        start_date, end_date = self.start_and_end_date(investment_date, current_date)
        if self.indicator_is_tiie(type_indicator):
            rate_values = self.rate_values_tiie(start_date, end_date)
            # Llena fechas vacias
            rate_values = self.fill_empty_values_rates(rate_values)
       
        # EN CETES no se llena fechas vacias
        elif(self.indicator_is_cetes(type_indicator)):
            rate_values = self.rate_values_cetes(start_date, end_date)
            # Convierte en lista para usar sum_rate_values(rate_values_list)
            rate_values = self.float_list_cetes(rate_values)

        length = len(rate_values)
        if length > 0:
            sum_rate_values = self.sum_rate_values(rate_values)
            indicator_average = sum_rate_values / length
            indicator_average_round = round(indicator_average, 4)

        return indicator_average_round
    
    def fill_empty_values_rates(self, original_rate_values):
        fill_rate_values = []

        for i in range(len(original_rate_values)):
            fill_rate_values.append(original_rate_values[i].rate_days_28)
            if i < len(original_rate_values)-1:
                rate_days_28 = original_rate_values[i].rate_days_28
                current_date = original_rate_values[i].rate_date
                next_date = original_rate_values[i+1].rate_date
                while current_date + timedelta(days=1) != next_date:
                    current_date = current_date + timedelta(days=1)
                    fill_rate_values.append(rate_days_28)

        first_date = original_rate_values[0].rate_date
        if not self.have_first_date_value(first_date):
            last_rate_previous_month = self.last_rate_previous_month(first_date)
            fill_rate_values = self.fill_first_day(first_date, fill_rate_values, last_rate_previous_month)

        return fill_rate_values

    def float_list_cetes(self, original_rate_values):
        rate_values_cetes = [rates.rate_days_28 for rates in original_rate_values]

        return rate_values_cetes    

    def last_rate_previous_month(self, current_date):
        ipr_obj = self.env[INVESTMENT_PERIOD_RATE_MODEL]

        prev_month_first_day, prev_month_last_day = self.prev_month_first_last_day(current_date)
        rate_values = ipr_obj.search(
                [('product_type', '=', 'TIIE'), ('rate_date', '>=', prev_month_first_day), ('rate_date', '<=', prev_month_last_day)])
        last_rate_days_28_previous_month = rate_values[-1].rate_days_28

        return last_rate_days_28_previous_month

    def have_first_date_value(self, current_date):
         if current_date.day == 1:
            return True

    def fill_first_day(self, first_date, fill_rate_values, last_rate_previous_month):
        first_day = first_date.day

        i = 1
        while i < first_day:
            fill_rate_values = [last_rate_previous_month] + fill_rate_values
            i += 1
    
        return fill_rate_values    
    
    def prev_month_first_last_day(self, current_date):
         # finding the first day of the previous month
        prev_month_first_day = current_date.replace(day=1) - timedelta(days=1)
        prev_month_first_day = prev_month_first_day.replace(day=1)
        # finding the last day of the previous month
        _, last_day = calendar.monthrange(prev_month_first_day.year, prev_month_first_day.month)
        prev_month_last_day = prev_month_first_day.replace(day=last_day)

        return prev_month_first_day, prev_month_last_day

    def last_day_month(self, current_date):
        last_day_of_month = calendar.monthrange(current_date.year, current_date.month)[1]
        last_day_of_month_full = date(current_date.year, current_date.month, last_day_of_month)

        return last_day_of_month_full    

    def rate_values_tiie(self, start_date, end_date):
        ipr_obj = self.env[INVESTMENT_PERIOD_RATE_MODEL]
        rate_values = 0

        rate_values = ipr_obj.search(
            [('product_type', '=', 'TIIE'), ('rate_date', '>=', start_date), ('rate_date', '<=', end_date)])
   
        return rate_values
   
    def rate_values_cetes(self, start_date, end_date):
        ipr_obj = self.env[INVESTMENT_PERIOD_RATE_MODEL]
        rate_values = 0

        rate_values = ipr_obj.search(
            [('product_type', '=', 'CETES'), ('rate_date', '>=', start_date), ('rate_date', '<=', end_date)])

        return rate_values  

    def sum_rate_values(self, rate_values):
        sum_rate = sum(rate_values)

        return sum_rate

    def start_and_end_date(self, investment_date, current_date):
        if (current_date.month == investment_date.month) and (current_date.year == investment_date.year):
            star_date = investment_date
            end_date = current_date
        else:
            date_string = f"{current_date.year}-{current_date.month}-01"
            date_date = datetime.strptime(date_string, DATE_FORMAT).date()
            star_date = date_date
            end_date = current_date

        return star_date, end_date    


class AccountMoveLine(models.Model):

    _inherit = 'account.move.line'

    term_investments_id = fields.Many2one(
        TERM_INVESTMENTS_MODEL, 'Term Investments')


class AccruedInterest(models.Model):

    _inherit = 'accrued.interest'

    term_investments_id = fields.Many2one(
        TERM_INVESTMENTS_MODEL, 'Investment')
