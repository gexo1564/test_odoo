# -*- coding: utf-8 -*-
import io, base64
import cProfile
from datetime import datetime
from odoo import api, fields, models, _
from odoo.tools.misc import xlsxwriter
import json

ACCOUNT_ACCOUNT = 'account.account'

class StatusProgramReport(models.AbstractModel):
    _inherit = 'jt_account_module_design.trial.balance.base'
    
    filter_all_entries = False

    def print_siif_report(self):
        self.env['siif_balance_report'].get_xlsx()

    def siif_print_xlsx(self, options):
        return {
                'type': 'ir_actions_account_report_download',
                'data': {'model': 'siif.balance.report',
                         'options': json.dumps(options),
                         'output_format': 'xlsx',
                         'financial_id': self.env.context.get('id'),
                         }
                }

    def _get_reports_buttons(self):
        return [
            {'name': _('Export to PDF'), 'sequence': 1, 'action': 'print_pdf', 'file_export_type': _('PDF')},
            {'name': _('Export (XLSX)'), 'sequence': 2, 'action': 'siif_print_xlsx', 'file_export_type': _('XLSX')},
        ]

    @api.model
    def _get_lines(self, options, line_id=None):
        unfold_all = options.get('unfold_all', False)
        domain_account = []
        domain_dep_sd = []
        where_aml = "parent_state = 'posted'"
        params_aml = ()
        is_add_fiter = False
        is_add_dep_fiter = False
        account_ids = options.get('account_ids', [])
        if account_ids:
            domain_account.append(('id', 'in', account_ids))
            where_aml += " and account_id in %s"
            params_aml += tuple(account_ids),
        sub_dependency_ids = options.get('domain_sd', [])
        if sub_dependency_ids:
            is_add_dep_fiter = True
            where_aml += " and sub_dependency_id in %s"
            params_aml += tuple(sub_dependency_ids),
            domain_dep_sd.append(('sub_dependency_id', 'in', sub_dependency_ids))
        dependency_ids = options.get('domain_dep', [])
        if dependency_ids:
            is_add_dep_fiter = True
            where_aml += "and dependency_id in %s"
            params_aml += tuple(dependency_ids),
            domain_dep_sd.append(('dependency_id', 'in', dependency_ids))

        if domain_dep_sd:
            unfold_all = True

        '''
        TODO: The filters below slow the function down by 7 seconds, so disabled
        for now
        '''
        # program_code_obj = self.env['program.code']

        # program_codes = program_code_obj.search(domain)

        # program_codes_account_ids = program_codes.mapped('item_id.unam_account_id')
        # program_codes_account_ids = program_codes_account_ids.ids

        move_lines_dep_account_ids = None
        if not line_id and is_add_dep_fiter:
            where_aml += " and date <= %s"
            params_aml += options.get('date').get('date_to'),
            query = """
                select id from account_account where id in (
                    select distinct account_id from account_move_line where %s
                )
                and dep_subdep_flag
            """ % where_aml
            self.env.cr.execute(query, params_aml)
            move_lines_dep_account_ids = self.env[ACCOUNT_ACCOUNT].browse(
                [res[0] for res in self.env.cr.fetchall()]
            )
        elif line_id:
            move_lines_dep_account_ids = self.env[ACCOUNT_ACCOUNT].browse(int(line_id[8:])) if line_id else []
        else:
            move_lines_dep_account_ids = self.env[ACCOUNT_ACCOUNT].search(domain_account)

        new_options = options.copy()
        options_list = self._get_options_periods_list(new_options)
        expanded_account = move_lines_dep_account_ids
        options_list[0]['unfold_all'] = True
        accounts_results, __ = self.env['account.general.ledger']._do_query(options_list, fetch_lines=False, expanded_account=expanded_account, domain=domain_dep_sd)
        options_list[0]['unfold_all'] = False

        date = options_list[0].get('date')
        posted_only = not options_list[0].get('all_entries')
        lines = []
        totals = [0.0] * (2 * (len(options_list) + 2))


        # Add lines
        for account, periods_results in accounts_results:

            # if is_add_fiter and not account.id in program_codes_account_ids:
            #     continue

            if is_add_dep_fiter and account.id not in move_lines_dep_account_ids.ids:
                continue

            if account_ids and account.id not in account_ids:
                continue

            sums = []
            account_balance = 0.0


            for i, period_values in enumerate(reversed(periods_results)):
                account_sum = period_values.get('sum', {})
                account_un_earn = period_values.get('unaffected_earnings', {})
                account_init_bal = period_values.get('initial_balance', {})

                if i == 0:
                    # Append the initial balances.
                    initial_balance = account_init_bal.get('balance', 0.0) + account_un_earn.get('balance', 0.0)
                    sums += [
                        initial_balance > 0 and initial_balance or 0.0,
                        initial_balance < 0 and -initial_balance or 0.0,
                    ]
                    account_balance += initial_balance

                # Append the debit/credit columns.
                sums += [
                    account_sum.get('debit', 0.0) - account_init_bal.get('debit', 0.0),
                    account_sum.get('credit', 0.0) - account_init_bal.get('credit', 0.0),
                ]
                account_balance += sums[-2] - sums[-1]

            # Append the totals.
            sums += [
                account_balance > 0 and account_balance or 0.0,
                account_balance < 0 and -account_balance or 0.0,
            ]

            # account.account report line.
            columns = []
            for i, value in enumerate(sums):
                # Update totals.
                totals[i] += value

                # Create columns.
                columns.append({'name': self.format_value(value, blank_if_zero=False), 'class': 'number', 'no_format_name': value})

            name = account.name_get()[0][1]
            if len(name) > 40 and not self._context.get('print_mode'):
                name = name[:40]+'...'


            '''
            The account should be unfolded if the account id matches the 
            expanded account id or if all accounts should be unfolded. If it is
            to be unfolded, also add the dependencies and subdependencies to the
            lines
            '''
            unfold_account = ((line_id and (account in expanded_account)) or unfold_all) and account.dep_subdep_flag
            lines.append(self.format_line(acc=account, children=account.dep_subdep_flag, cols=columns, extra_info={
                'caret_options': ACCOUNT_ACCOUNT,
                'level': 1,
                'unfolded': unfold_account
            }))


            if unfold_account:
                '''
                If the account is unfolded, add the dependency and subdepency
                lines
                '''
                comparison = options_list[0].get('comparison', {})
                current_period = [(fields.Date.from_string(date['date_from']),fields.Date.from_string(date['date_to']))]
                comparison_periods = list(map(lambda d: (fields.Date.from_string(d['date_from']), 
                    fields.Date.from_string(d['date_to'])), comparison.get('periods', [])))
                all_periods = (current_period  + comparison_periods)[::-1]

                dep_balance_opts = {
                    'posted_only': posted_only,
                    'init_balance': True,
                    'subdep_balance': True,
                    'dep_domain': dependency_ids,
                    'sd_domain': sub_dependency_ids,
                }
                account_dep_balances = self.env['dependency'].get_account_balances(account, all_periods, dep_balance_opts)

                for dep, dep_result in account_dep_balances:
                    lines.append(self.format_line(acc=account, dep=dep, children=dep_result.get('subdeps'),
                        cols=self.get_cols(dep_result.get('deps')), extra_info={
                        'caret_options': 'dependency',
                        'level': 2,
                        'unfolded': unfold_account
                    }))
                    for subdep in dep_result.get('subdeps', []):
                        lines.append(self.format_line(acc=account, dep=dep, subdep=subdep[0].get('subdep'), cols=self.get_cols(subdep),
                            extra_info={
                            'caret_options': 'subdependency',
                            'level': 3,
                            'unfolded': unfold_account
                        }))
            # if len(lines) > 200:
            #     break
        if not line_id:
            # Total report line.
            lines.append({
                'id': 'grouped_accounts_total',
                'name': _('Total'),
                'class': 'total',
                'columns': [{'name': self.format_value(total), 'class': 'number'} for total in totals],
                'level': 1,
            })

        
        return lines


    def get_cols(self, dep_vals):
        init_bal_cols = [
            {'name': self.format_value(abs(dep_vals[0]['balance']), blank_if_zero=False), 'class': 'number'},
            {'name': self.format_value(0, blank_if_zero=False), 'class': 'number'}
        ]
        if dep_vals[0]['balance'] < 0: init_bal_cols.reverse()

        cols = []
        balance = dep_vals[0]['balance']
        for col in dep_vals[1:]:
            cur_col = [
                {'name': self.format_value(col['debit'], blank_if_zero=False), 'class': 'number'},
                {'name': self.format_value(col['credit'], blank_if_zero=False), 'class': 'number'}

            ]
            balance += col['balance']
            cols += cur_col
        
        balance_cols = [
            {'name': self.format_value(abs(balance), blank_if_zero=False), 'class': 'number'},
            {'name': self.format_value(0, blank_if_zero=False), 'class': 'number'}
        ]
        '''
        If the balance is negative, then the values should be credited instead
        '''
        if balance < 0: balance_cols.reverse()

        return init_bal_cols + cols + balance_cols


    def get_html(self, options, line_id=None, additional_context=None):
        '''
        Add single_line to the context to be able to know that only a single 
        line is being added in _create_hierarchy
        '''
        if line_id:
            self = self.with_context(single_line=True)
        return super(StatusProgramReport, self).get_html(options, line_id, additional_context)

    def _create_hierarchy(self, lines, options):
        '''
        (Overridden) Several changes made here:
        1. Make the hierarchy work with dependencies and subdependencies
        2. Set the initial balance and final balance columns to show net 
            balances rather than both debits and credits
        3. Set the total row to sum up the debits and credits column by column
            (It does this out of the box but broke with change #2)
        '''

        account_lines = list(filter(lambda l: l.get('id').startswith('acc') or l.get('id') == 'grouped_accounts_total', lines))
        dep_lines = list(filter(lambda l: not l.get('id').startswith('acc'), lines))
        hierarchy_lines = super(StatusProgramReport, self)._create_hierarchy(account_lines, options)

        for hierarchy_line in hierarchy_lines:
            for col in hierarchy_line.get('columns', []):
                col['class'] = 'number'

        '''
        (Helper) Small function that is repeatedly used in this function. 
        Returns a formatted monetary cell in an account report
        '''
        def format_monetary_cell(value):
            return { 'name': self.format_value(abs(value), blank_if_zero=False), 'class': 'number' }

        level = options.get('levels', 0)
        unfold_all = options.get('unfold_all', False)
        from_pdf = options.get('from_pdf', False)

        order_control_accounts = self.env['control.accounts'].get_control_acounts()

        new_lines = []
        for line in hierarchy_lines:
            line['unfolded'] = unfold_all
            if line['name'] == 'Total':
                new_lines.append(line)
            elif level == 0:
                line['level'] = line['level'] - 1
                new_lines.append(line)
                line_id = line.get('id', '')
                if line_id.startswith('acc'):
                    dep_id = None
                    for dep_line in dep_lines:
                        '''
                        If dep_id was set, then the following lines are going to be
                        sub dependencies. If the parent doesn't match the dep_id,
                        then we already got all subdependencies
                        '''
                        if dep_id:
                            if dep_line.get('parent_id','') == dep_id:
                                dep_line['level'] = line.get('level') + 2
                                new_lines.append(dep_line)
                            else:
                                dep_id = None
                        if dep_line.get('parent_id','') == line_id:
                            dep_line['level'] = line.get('level') + 1
                            new_lines.append(dep_line)
                            dep_id = dep_line.get('id','')
            elif from_pdf and level < 4:
                if line['level'] == level + 3:
                    line['level'] = line['level'] - (level + 2)
                    line['parent_id'] = 'con'
                    new_lines.append(line)
            elif line['level'] >= level + 3:
                line['level'] = line['level'] - (level + 2)
                if line['level'] == 1:
                    line['parent_id'] = 'con'
                new_lines.append(line)
                line_id = line.get('id', '')
                if line_id.startswith('acc'):
                    dep_id = None
                    for dep_line in dep_lines:
                        '''
                        If dep_id was set, then the following lines are going to be
                        sub dependencies. If the parent doesn't match the dep_id,
                        then we already got all subdependencies
                        '''
                        if dep_id:
                            if dep_line.get('parent_id','') == dep_id:
                                dep_line['level'] = line.get('level') + 2
                                new_lines.append(dep_line)
                            else:
                                dep_id = None
                        if dep_line.get('parent_id','') == line_id:
                            dep_line['level'] = line.get('level') + 1
                            new_lines.append(dep_line)
                            dep_id = dep_line.get('id','')

        # Se recalculan las sumas de las dependencias / subdependencias cuando
        # se tiene un filtro
        # if options.get('domain_dep', []) or options.get('domain_sd', []):
        #     tree = Tree()
        #     for line in new_lines[0:-1]:
        #         if line['id'].startswith('subdep'):
        #             tree.add_leaf(line['parent_id'], line['columns'])
        #         else:
        #             tree.add_node(line)
        #     tree.update_columns('con')

        '''
        If only a single line is being loaded, then remove the hierarchy lines
        since those are already visible on the page
        '''
        if self._context.get('single_line'):
            new_lines = list(filter(lambda l: not l.get('id').startswith('hierarchy'), new_lines))

        initial_total = [0,0]
        move_total = [0,0]
        final_total= [0,0]

        for hierarchy_line in new_lines:
            cols = hierarchy_line.get('columns', [])

            if hierarchy_line.get('name', '')[0] not in order_control_accounts:
                '''
                Subtract the credits from debits from the initial and final balance to
                get the net balance, and then put it in the debit side if the balance is 
                positive and the credit side if it is negative.
                '''
                zero_col = format_monetary_cell(0)

                # Obtiene lo saldos iniciales como flotantes
                initial_debit = float(cols[0].get('name')[2:].replace(',',''))
                initial_credit = float(cols[1].get('name')[2:].replace(',', ''))
                initial_balance = initial_debit - initial_credit
                initial_value_col = format_monetary_cell(abs(initial_balance))
                cols[0 if initial_balance >= 0 else 1] = initial_value_col
                cols[1 if initial_balance >= 0 else 0] = zero_col


                final_debit = float(cols[-2].get('name')[2:].replace(',',''))
                final_credit = float(cols[-1].get('name')[2:].replace(',', ''))
                final_balance = final_debit - final_credit
                final_value_col = format_monetary_cell(abs(final_balance))
                cols[-2 if final_balance >= 0 else -1] = final_value_col
                cols[-1 if final_balance >= 0 else -2] = zero_col

                '''
                Only sum up the totals if the current row is a root account, and if
                the line is not the old total line
                '''
                if hierarchy_line.get('level', False) == 1 and hierarchy_line.get('name') != 'Total':
                    initial_total[0 if initial_balance > 0 else 1] += abs(initial_balance)
                    move_total[0] += float(cols[2].get('name')[2:].replace(',',''))
                    move_total[1] += float(cols[3].get('name')[2:].replace(',',''))
                    final_total[0 if final_balance > 0 else 1] += abs(final_balance)
            else:
                if hierarchy_line.get('level', False) == 1 and hierarchy_line.get('name') != 'Total':
                    initial_total[0] += float(cols[0].get('name')[2:].replace(',',''))
                    initial_total[1] += float(cols[1].get('name')[2:].replace(',', ''))
                    move_total[0] += float(cols[2].get('name')[2:].replace(',',''))
                    move_total[1] += float(cols[3].get('name')[2:].replace(',',''))
                    final_total[0] += float(cols[-2].get('name')[2:].replace(',',''))
                    final_total[1] += float(cols[-1].get('name')[2:].replace(',',''))

        if new_lines[-1].get('name') == 'Total':
            total_cols = new_lines[-1].get('columns')
            total_cols[0] = format_monetary_cell(initial_total[0])
            total_cols[1] = format_monetary_cell(initial_total[1])
            total_cols[2] = format_monetary_cell(move_total[0])
            total_cols[3] = format_monetary_cell(move_total[1])
            total_cols[-2] = format_monetary_cell(final_total[0])
            total_cols[-1] = format_monetary_cell(final_total[1])

        return new_lines

    def get_xlsx(self, options, response=None):
        '''
        (Overridden) Had to copy this function over from 
        jt_account_module_design/reports/trial_balance_base to the issue with 
        indentation not working as expected on Google Sheets. (Google Sheets 
        doesn't support excel's format for indentation so need to manually
        prepend spaces to everything)
        '''

        '''
        Set all lines to expand by default any time the excel is downloaded
        '''
        options['unfold_all'] = True

        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        sheet = workbook.add_worksheet(self._get_report_name()[:31])

        currency_style = workbook.add_format({
            'font_name': 'Arial',
            'font_size': 12,
            'font_color': '#666666',
            'num_format': '$#,##0.00'
        })

        date_default_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': 'yyyy-mm-dd'})
        date_default_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': 'yyyy-mm-dd'})
        default_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'})
        default_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'})
        title_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'bottom': 2})

        level_0_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 13, 'bottom': 6, 'font_color': '#666666'})
        level_1_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 13, 'bottom': 1, 'font_color': '#666666'})
        level_2_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666'})
        level_3_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'})
        level_4_style = workbook.add_format(
            {'font_name': 'Arial','font_size': 12, 'font_color': '#666666'})
        level_5_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'})
        level_6_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'})

        level_2_col1_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666'})
        level_3_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'})
        level_4_col1_style = level_2_col1_style
        level_5_col1_style = level_3_col1_style
        level_6_col1_styl = level_3_col1_style

        level_2_col1_total_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666'})
        level_3_col1_total_style = level_2_col1_total_style

        currect_date_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'align': 'right'})
        currect_date_style.set_border(0)
        super_col_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'align': 'center'})
        super_col_style.set_border(0)

        sheet.set_column(0, 0, 100)
        sheet.set_column(1, 1, 20)
        sheet.set_column(2, 2, 20)
        sheet.set_column(3, 3, 20)
        sheet.set_column(4, 4, 20)
        sheet.set_column(5, 5, 20)
        sheet.set_column(6, 6, 20)

        y_offset = 0
        col = 0
        start = datetime.strptime(str(options['date'].get('date_from')), '%Y-%m-%d').date()
        end = datetime.strptime(options['date'].get('date_to'), '%Y-%m-%d').date()
        start_month_name = start.strftime("%B")
        end_month_name = end.strftime("%B")
        
        if self.env.user.lang == 'es_MX':
            start_month_name = self.get_month_name(start.month)
            end_month_name = self.get_month_name(end.month)

        header_date = str(start.day).zfill(2) + " " + start_month_name+" DE "+str(start.year)
        header_date += " AL "+str(end.day).zfill(2) + " " + end_month_name +" DE "+str(end.year)

        sheet.merge_range(y_offset, col, 6, col, '', super_col_style)
        if self.env.user and self.env.user.company_id and self.env.user.company_id.header_logo:
            filename = 'logo.png'
            image_data = io.BytesIO(base64.standard_b64decode(
                self.env.user.company_id.header_logo))
            sheet.insert_image(0, 0, filename, {
                'image_data': image_data, 'x_offset': 8, 'y_offset': 3, 'x_scale': 0.6, 'y_scale': 0.6})
        col += 1
        header_title = '''UNIVERSIDAD NACIONAL AUTÓNOMA DE MÉXICO\nDIRECCIÓN GENERAL DE CONTROL PRESUPUESTAL-CONTADURÍA 
        GENERAL \nBALANZA DE COMPROBACIÓN DEL %s \nCifras en Pesos''' % (header_date)

        sheet.merge_range(y_offset, col, 5, col + 6, header_title, super_col_style)
        y_offset += 6
        col = 1
        currect_time_msg = "Fecha y hora de impresión: "
        currect_time_msg += datetime.today().strftime('%d/%m/%Y %H:%M')
        sheet.merge_range(y_offset, col, y_offset, col + 6, currect_time_msg, currect_date_style)
        y_offset += 1

        super_columns = self._get_super_columns(options)
        x = super_columns.get('x_offset', 0)
        for super_col in super_columns.get('columns', []):
            cell_content = super_col.get('string', '').replace('<br/>', ' ').replace('&nbsp;', ' ')
            x_merge = super_columns.get('merge')
            if x_merge and x_merge > 1:
                sheet.merge_range(y_offset, x, y_offset, x + (x_merge - 1), cell_content, super_col_style)
                x += x_merge
            else:
                sheet.write(y_offset, x, cell_content, super_col_style)
                x += 1

        y_offset += 1

        for row in self.get_header(options):
            x = 0
            for column in row:
                colspan = column.get('colspan', 1)
                header_label = column.get('name', '').replace(
                    '<br/>', ' ').replace('&nbsp;', ' ')
                if colspan == 1:
                    sheet.write(y_offset, x, header_label, title_style)
                else:
                    sheet.merge_range(y_offset, x, y_offset,
                                      x + colspan - 1, header_label, title_style)
                x += colspan
            y_offset += 1
        ctx = self._set_context(options)
        ctx.update({
            'no_format': False,
            'print_mode': True,
            'prefetch_fields': False
        })
        # deactivating the prefetching saves ~35% on get_lines running time
        lines = self.with_context(ctx)._get_lines(options)
        if options.get('hierarchy'):
            lines = self._create_hierarchy(lines, options)
        if options.get('selected_column'):
            lines = self._sort_lines(lines, options)
        # write all data rows
        for y in range(0, len(lines)):
            level = lines[y].get('level')
            if level == 0:
                y_offset += 1
                style = level_0_style
                col1_style = style
            elif level == 1:
                style = level_1_style
                col1_style = style
            elif level == 2:
                style = level_2_style
                col1_style = 'total' in lines[y].get('class', '').split(
                    ' ') and level_2_col1_total_style or level_2_col1_style
            elif level == 3:
                style = level_3_style
                col1_style = 'total' in lines[y].get('class', '').split(
                    ' ') and level_3_col1_total_style or level_3_col1_style
            elif level == 4:
                style = level_4_style
                col1_style = level_4_col1_style
            elif level == 5:
                style = level_5_style
                col1_style = level_5_col1_style        
            elif level == 6:
                style = level_6_style
                col1_style = level_6_col1_styl
            else:
                style = default_style
                col1_style = default_col1_style
            # write the first column, with a specific style to manage the
            # indentation
            cell_type, cell_value = self._get_cell_type_value(lines[y])

            '''
            Prepend the account name with a certain number of spaces 
            depending on it's level attribute
            '''
            row_spacing = '  ' * level

            if cell_type == 'date':
                sheet.write_datetime(y + y_offset, 0, row_spacing + str(cell_value), date_default_col1_style)
            else:
                sheet.write(y + y_offset, 0, row_spacing + str(cell_value), col1_style)
            # write all the remaining cells
            for x in range(1, len(lines[y]['columns']) + 1):
                cell_type, cell_value = self._get_cell_type_value(lines[y]['columns'][x - 1])

                if cell_type == 'date':
                    sheet.write_datetime(
                        y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, date_default_style)
                else:
                    if cell_type == 'monetary':
                        style = currency_style

                    sheet.write(y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, style)
        workbook.close()
        output.seek(0)
        generated_file = output.read()
        output.close()
        return generated_file

    def _get_cell_type_value(self, cell):
        '''
        (Overridden) Check if the cell is supposed to be a monetary field, and
        make it a float if it is
        '''
        cell_type, cell_value = super(StatusProgramReport, self)._get_cell_type_value(cell)
        if u'$\xa0' in cell_value:
            cell_value = cell_value.replace(u'$\xa0', '')
            cell_value = cell_value.replace(',', '')
            cell_value = float(cell_value)
            cell_type = 'monetary'
        return cell_type, cell_value

    def _get_options(self, previous_options=None):
        '''
        (Overridden) Set the default options to enable the "Hierarchy and
        Subtotals" option
        '''
        options = super(StatusProgramReport, self)._get_options(previous_options)
        if not previous_options:
            options['hierarchy'] = True
        return options

    @api.model
    def _get_templates(self):
        '''
        (Overridden) Add the Column Headers to the trial balance (Saldo Inicial,
        the months, and Saldo Final)
        '''
        templates = super(StatusProgramReport, self)._get_templates()
        templates['main_table_header_template'] = 'unam_custom_report_aggregation.template_trial_balance_base_table_header'
        return templates
