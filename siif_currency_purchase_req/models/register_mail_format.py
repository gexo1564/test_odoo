from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


MAIL_FORMAT='mail.format.money.fund'

class RegisterMailFormat(models.Model):
    _name = 'mail.format.money.fund'

    type =  fields.Selection(selection=[
            ('document_check', 'Document Check'),
            ('rejection','Rejection Mail'),

        ], )
    title = fields.Char(string="Title Mail")
    msg = fields.Html(string='Message')

    @api.model
    def create(self, vals):
        count = self.env[MAIL_FORMAT].search_count([('type','=',vals.get('type'))])
        if count >= 1:
            raise ValidationError("Solo se puede configurar el tipo del correo seleccionado 1 vez.")
        else:
            res = super(RegisterMailFormat, self).create(vals)
            return res
        
    def name_get(self):
        result = []
        for rec in self:
            result.append((rec.id,rec.type))
        return result
    
    

    #Enviar correo y preparar los datos con respecto a las especificaciones de la solicitud
    def _make_email_money_fund(self,type,solicitud):
        emails= solicitud.money_fund._get_emails()
        if solicitud._name=='money.funds.request.increment':
            msg='Solicitud de asignación'
        elif solicitud._name=='money.funds.request.ministration':
            msg='Solicitud de ministración'
        elif solicitud._name=='money.funds.cancel':
            msg='Solicitud de Cancelación'


        if type=="doc":
            if self.env[MAIL_FORMAT].search([('type','=','document_check')]):
                subject=self.env[MAIL_FORMAT].search([('type','=','document_check')]).title+'-'+msg
                body=self.env[MAIL_FORMAT].search([('type','=','document_check')]).msg+solicitud._get_info_request(type)
            else:
                raise UserError("Configure el tipo de correo 'Revisión a la documentación' en la pestaña Formatos de Correo del Fondo Fijo")
        elif type=="reject":
            if self.env[MAIL_FORMAT].search([('type','=','document_check')]):
                subject=self.env[MAIL_FORMAT].search([('type','=','rejection')]).title+'-'+msg
                body=self.env[MAIL_FORMAT].search([('type','=','rejection')]).msg+solicitud._get_info_request(type)
            else:
                raise UserError("Configure el tipo de correo 'Rechazo de solicitud' en la pestaña Formatos de Correo del Fondo Fijo")
        

        self._send_approve_email(emails,subject,body)

    #Envio de correo con respecto al rechazo de la solicitud del fondo fijo
    def _send_approve_email(self,email_delivery,subject,body):
        self.env.context = dict(self.env.context)
        self.env.context.update({
                'is_email' : True
            })

        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        template_obj = self.env['mail.template'].sudo().search([('name','=','mail_template_bac')], limit=1)
        aux_body = template_obj.body_html
        aux_body = aux_body.replace('--subject--',subject)
        aux_body = aux_body.replace('--summary--',body)
        aux_body = aux_body.replace('--base_url--',base_url)
        name_to = ""

        root_email = self.env['res.users'].search([('name','=','Admin correo sidia')])
        if not root_email:
            raise UserError('No se encuentra al usuario de correo admin')

        author_id = self.env['res.partner'].search([('id','=',root_email.partner_id.id)])

        mail_pool = self.env['mail.mail']
        values = {
                        'subject' : subject,
                        'email_to' : "",
                        'email_from' : author_id.email,
                        'author_id':author_id.id,
                        'reply_to' : 'Favor de no responder este mensaje.',
                        'body_html' : "",
                        'body' : body,
            }
        
        if email_delivery:
            for mail in email_delivery:
                values['email_to'] = str(mail[1])
                if email_delivery:
                    name_to = mail[0]

                aux_body = aux_body.replace('--name_to--',name_to)
                values['body_html'] = aux_body

                msg_id = mail_pool.create(values)

                if msg_id:
                    mail_pool.sudo().send([msg_id])

                else:
                    raise UserError('No se creó el mensaje')
        else:
            raise UserError('No se encontraron correos del secretari@')