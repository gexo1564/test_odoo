# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError
from datetime import datetime

from odoo.addons.sd_utils.models import message_wizard
from odoo.addons.siif_supplier_payment.tools import reject_wizard

ACCOUNT_PAYMENT = 'account.payment'
BATCH_MANAGEMENT = 'batch.management'
MAIL_THREAD = 'mail.thread'

class ResourceRequest(models.Model):
    _name = 'resource.request'
    _inherit = [MAIL_THREAD, 'mail.activity.mixin']

    # Estados de la solicitud
    state = fields.Selection(
        [
            ('draft', 'Draft'),
            ('requested', 'Requested'),
            ('approved', 'Approved'),
            ('schedule', 'Schedule'),
            ('confirmed', 'Confirmed'),
            ('rejected', 'Rejected'),
        ], string="Status", default="draft", tracking=True
    )
    # Tipo de folio asociado
    # type = fields.Selection([('manual','Manual'),('automatic','Automatic')],string="Type")
    # Folio de la solicitud
    folio = fields.Char('Folio')
    # Moneda
    currency_id = fields.Many2one('res.currency', default=lambda self: self.env.user.company_id.currency_id)
    # Tipo de solicitud
    transfer_request = fields.Selection([('finances','Finances')], string='Transfer Request')
    # Monto
    amount = fields.Monetary("Amount")
    # Fecha de Emisión
    date = fields.Date("Application date", default=fields.Date.today)
    # En atención a (Usuario al que va dirigida la solicitud)
    attention_to_emp_id = fields.Many2one("hr.employee", string="Attention to")
    # Usuario que solicita
    prepared_by_user_id = fields.Many2one("res.users",string="Creation Users",default=lambda self: self.env.user.id)
    # Usuario que autoriza
    authorized_by_user_id = fields.Many2one("res.users", string="Authorized Users")
    # Pagos Asociados
    payment_ids = fields.Many2many(ACCOUNT_PAYMENT, string="Payments")
    # Número de pagos
    number_of_transfers = fields.Integer(string="Number of payments")
    # Número de pagos confirmados
    number_of_transfers_validated = fields.Integer(computstring="Number of payments Validated")
    # Lineas asociadas a la solicitud de transferencia
    resource_request_line_ids = fields.One2many('resource.request.line', 'resource_request_id', 'Transfer Request Lines')
    # Motivo de Rechazo de la Solicitud
    reason_rejection = fields.Text("Reason Rejection", tracking=True)

    def name_get(self):
        return [(record.id, record.folio) for record in self]

    @api.model
    def create(self, vals):
        res = super(ResourceRequest, self).create(vals)
        res.folio = self.env['ir.sequence'].next_by_code('resource.request.folio')
        return res

    def check_resource_request(self):
        """
        Valida que los montos requeridos sean igual a los montos
        a transferir, se ejecuta en el status Solicitado.
        """
        folios = {}
        for line in self.resource_request_line_ids:
            if not line.src_bank_account_journal_id:
                raise UserError("La cuenta origen de la transferencia es requerida.")
            val = folios.get(line.folio, [line.required_amount, 0])
            val[1] =  val[1] + line.amount
            folios[line.folio] = val
        for folio, vals in folios.items():
            if round(vals[0], 2) != round(vals[1], 2):
                raise UserError(
                    "El monto total requerido para el folio: %s es de %s y la suma de las transferencias es de %s" % (
                        ", ".join([f.folio for f in folio]),
                        format(vals[0], ",.2f"),
                        format(vals[1], ",.2f"),
                    )
                )

    def approve(self):
        self.write({
            'state': 'approved',
            'authorized_by_user_id': self.env.user.id
        })

    def reject(self, reason_rejection):
        self.state = 'rejected'
        self.reason_rejection = reason_rejection
        for line in self.resource_request_line_ids:
            self.env[BATCH_MANAGEMENT].reject(line.folio)
        self.env[MAIL_THREAD].inbox_message(
            "Solicitud de Recursos Rechazada",
            "La Solicitud de Recursos con folio %s fue rechazada por los siguientes motivos: %s" % (self.folio, reason_rejection),
            [self.prepared_by_user_id]
        )

    def schedule(self):
        # Se valida el estatus previo
        if self.state != 'approved':
            raise UserError("Solo se pueden programar solicitudes de recursos con estatus 'Aprobado'.")
        # Se valida que las línes esten balanceadas (montos requeridos = montos a transferir)
        self.check_resource_request()
        for line in self.resource_request_line_ids:
            payment = self.env[ACCOUNT_PAYMENT].create({
                'payment_type': 'transfer',
                'amount': line.amount,
                'journal_id': line.src_bank_account_journal_id.id,
                'destination_journal_id': line.dst_bank_account_journal_id.id,
                'payment_date': datetime.today().date(),
                'payment_method_id': self.env.ref('account.account_payment_method_manual_in').id,
                # 'payment_issuance_method': self.type,
            })
            if payment:
                line.payment_id = payment
                self.payment_ids = [(4, payment.id)]
                self.number_of_transfers += 1
        self.state = 'schedule'
        return message_wizard.display_modal(self,
            'Pago Programado',
            '¡Se han programado los pagos exitosamente!',
            refresh_page=True
        )

    def update_number_of_transfers_validated(self):
        self.number_of_transfers_validated += 1
        if self.number_of_transfers_validated == self.number_of_transfers:
            self.state = 'confirmed'
            self.env[BATCH_MANAGEMENT].search([('resource_request_id', '=', self.id)]).write({
                'state': 'with_resources'
            })
            self.env[MAIL_THREAD].inbox_message(
            "Solicitud de Recursos Confirmada",
            "La Solicitud de Recursos con folio %s fue confirmada, ya es posible continuar con el flujo de pago." % self.folio,
            [self.prepared_by_user_id]
        )

    def action_sent(self):
        if not self.attention_to_emp_id:
            raise UserError("El nombre del usuario al que va dirigida la solicitud de recursos es obligatorio.")
        self.state = 'requested'
        self.env[MAIL_THREAD].inbox_message(
            "Nueva Solicitud de Recursos",
            "Se le ha asignado la Solicitud de Recursos con folio %s." % self.folio,
            [self.attention_to_emp_id.user_id]
        )

    def action_approve(self):
        return self.approve()

    def action_reject(self):
        return reject_wizard.display_modal(self, 'Rechazar Solicitud de Recursos')

    def action_schedule(self):
        return self.schedule()

    def action_open_payments(self):
        action = self.env.ref('siif_supplier_payment.account_payment_transfer_action').read()[0]
        action['context'] = {
            'default_payment_type': 'inbound',
            'default_partner_type': 'customer',
            'show_for_agreement': True
        }
        if self.payment_ids:
            action['domain'] = [('id', 'in', self.payment_ids.ids)]
        else:
            action['domain'] = [('id', 'in', [])]
        return action

class RequestOpenBalanceFinanceLine(models.Model):
    _name = 'resource.request.line'

    # Solicitud de transferencias asociada
    resource_request_id = fields.Many2one('resource.request')
    # Estatus de la solicitud
    parent_state = fields.Selection(related='resource_request_id.state', store=False)

    # Diario de la cuenta bancaria origen
    src_bank_account_journal_id = fields.Many2one('account.journal', string='Account Journal Src')
    # Diario de la cuenta bancaria destino
    dst_bank_account_journal_id = fields.Many2one('account.journal', string='Account Journal Dst')
    # Folios de pago asociados
    folio = fields.Many2many(BATCH_MANAGEMENT, string='Folios')
    # Concepto de pago
    concept = fields.Char('Concept')
    # Monto requerido
    required_amount = fields.Float('Amount total')
    # Monto a transferir
    amount = fields.Float('Amount')
    # Fecha de Programación de pago
    payment_schedule_date = fields.Date('Payment Schedule Date')
    # Pago asociado
    payment_id = fields.Many2one(ACCOUNT_PAYMENT, string="Payment")


    def duplicate_line(self):
        """
        Método para duplicar las líneas de la vista, permitiendo agregar más de una
        cuenta origen para la misma cuenta destino de un lote.
        """
        self.resource_request_id.resource_request_line_ids = [(0, 0, {
            'dst_bank_account_journal_id': self.dst_bank_account_journal_id.id,
            'folio': self.folio,
            'concept': self.concept,
            'required_amount': self.required_amount,
            'amount': 0,
            'payment_schedule_date': self.payment_schedule_date,
        })]

    @api.onchange('amount')
    def onchange_amount(self):
        """
        Valida que la suma de los montos a transferir para un mismo lote no sea mayor
        al monto requerido.
        """
        lines = self.resource_request_id.resource_request_line_ids.filtered(lambda l: l.folio == self.folio)
        total = round(sum([l.amount for l in lines]), 2)
        if total > round(self.required_amount, 2):
            raise UserError("El monto a transferir no puede ser mayor que el monto requerido.")

    @api.onchange('src_bank_account_journal_id')
    def onchange_src_bank_account_journal_id(self):
        lines = self.resource_request_id.resource_request_line_ids.filtered(lambda l: l.folio == self.folio)
        total = round(sum([l.amount for l in lines]), 2)
        self.amount = round(self.required_amount - total, 2)
        if self.amount == 0:
            raise UserError("El monto a transferir debe ser mayor que cero.")

    def action_open_payment(self):
        action = self.env.ref('siif_supplier_payment.account_payment_transfer_form_action').read()[0]
        action['res_id'] = self.payment_id.id
        return action