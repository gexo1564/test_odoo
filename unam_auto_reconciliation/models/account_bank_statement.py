# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import  UserError


class AccountBankStatement(models.Model):
    _inherit = 'account.bank.statement.line'


    def process_reconciliation(self, counterpart_aml_dicts=None, payment_aml_rec=None, new_aml_dicts=None): #TODO: Make multi
        counterpart_moves = super(AccountBankStatement, self).process_reconciliation(counterpart_aml_dicts, 
            payment_aml_rec, new_aml_dicts)

        for move in counterpart_moves:
            '''
            Assume the invoice counterpart amls are the ones that have a credit
            balance (since they will be zeroing out the debit created by the 
            invoice)
            '''
            invoice_counterpart_amls = move.line_ids.filtered(lambda aml: aml.debit == 0)
            '''
            The bank counterpart amls should just be the rest of the amls. (The
            ones that have a debit balance)
            '''
            bank_counterpart_amls = move.line_ids - invoice_counterpart_amls
            payment_id = invoice_counterpart_amls.payment_id

            '''
            We need to find the original invoice to update a couple of fields on
            it. We should be able to find it by searching for the counterpart
            amls by the full reconcile id. 
            '''
            invoice_id = (self.env['account.move.line'].search([
                ('full_reconcile_id', '=', invoice_counterpart_amls.full_reconcile_id.id)
            ]) - invoice_counterpart_amls).mapped('move_id')

            if not invoice_id:
                raise UserError(_('Could not find matching original invoice'))
            
            if len(invoice_id) != 1:
                raise UserError(_('Only 1 invoice was expected'))

            payment_id.dependancy_id = invoice_id.dependancy_id
            payment_id.sub_dependancy_id = invoice_id.sub_dependancy_id

            invoice_id.income_bank_journal_id = self.statement_id.journal_id

        return counterpart_moves
