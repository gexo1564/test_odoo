# -*- coding: utf-8 -*-
from odoo.exceptions import UserError

def check_authorization(*groups):
    def _check_authorization(function):
        def wrapper(self, *args, **kwargs):
            if not self.env.su:
                if not any([self.env.user.has_group(group) for group in groups]):
                    if self.env.lang == 'es_MX':
                        raise UserError("No tiene permisos para realizar esta acción.")
                    else:
                        raise UserError("You don't have permissions to perform this action.")
            return function(self, *args, **kwargs)
        return wrapper
    return _check_authorization

def check_only_allowed_fields(*allowed):
    allowed = set(allowed)
    def _check_only_allowed_fields(function):
        def wrapper(self, *args, **kwargs):
            budget_admin_user = self.env.user.has_group('jt_budget_mgmt.group_budget_admin_user')
            budget_catalogs_admin_user = self.env.user.has_group('jt_budget_mgmt.group_budget_catalogs_admin_user')

            if not (self.env.su or budget_admin_user or budget_catalogs_admin_user):
                columns = set(args[0].keys())
                not_allowed = columns - allowed
                if not_allowed:
                    if self.env.lang == 'es_MX':
                        raise UserError("No tiene permisos para modificar los campos: %s" % (
                            ", ".join(list(not_allowed))
                        ))
                    else:
                        raise UserError("You don't have permissions to modify the fields: %s" % (
                            ", ".join(list(not_allowed))
                        ))
            return function(self, *args, **kwargs)
        return wrapper
    return _check_only_allowed_fields
