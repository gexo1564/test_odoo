from odoo import models, fields
from odoo.exceptions import ValidationError
from datetime import datetime

import logging
class BudegtInsufficiencWiz(models.TransientModel):

    _inherit = 'budget.insufficien.wiz'

    def action_budget_allocation(self):
        move_ids = self.move_ids.ids if self.move_ids and not self.move_id else self.move_id.ids
        res = self.env['account.move'].sudo().approve_for_payment(move_ids)
        if res and not res[0][0]:
            raise ValidationError("Error al Aprobar la Solicitud de pago")