from odoo import models, fields,_,api
from odoo.exceptions import UserError, ValidationError
from datetime import datetime, timedelta
import logging
logger = logging.getLogger(__name__)

class BankBalanceCheck(models.TransientModel):

    _inherit = 'bank.balance.check'


    def create_journal_line_for_payment_procedure(self,invoice):
        #===== for the accounting impact of the "Accrued" Budget====#
        if invoice.journal_id and not invoice.journal_id.accured_credit_account_id \
            or not invoice.journal_id.conac_accured_credit_account_id \
            or not invoice.journal_id.accured_debit_account_id \
            or not invoice.journal_id.conac_accured_debit_account_id :
            raise ValidationError("Please configure UNAM and CONAC Accrued account in payment request journal!")
        amount_total = sum(x.price_total for x in invoice.invoice_line_ids.filtered(lambda x:x.program_code_id))
        if invoice.currency_id != invoice.company_id.currency_id:
            amount_currency = abs(amount_total)
            balance = invoice.currency_id._convert(amount_currency, invoice.company_currency_id, invoice.company_id, invoice.date)
            currency_id = invoice.currency_id and invoice.currency_id.id or False 
        else:
            balance = abs(amount_total)
            amount_currency = 0.0
            currency_id = False
            
        if (invoice.is_payment_request and (not invoice.is_different_payroll_request and not invoice.is_pension_payment_request and not invoice.is_payroll_payment_request)):
                logger.info("PAGO A PROVEEDOR")
        
        else:
            invoice.line_ids = [(0, 0, {
                                        'account_id': invoice.journal_id.accured_credit_account_id and invoice.journal_id.accured_credit_account_id.id or False,
                                        'coa_conac_id': invoice.journal_id.conac_accured_credit_account_id and invoice.journal_id.conac_accured_credit_account_id.id or False,
                                        'credit': balance, 
                                        'exclude_from_invoice_tab': True,
                                        'conac_move' : True,
                                        'amount_currency' : -amount_currency,
                                        'currency_id' : currency_id,
                                        'partner_id':invoice.partner_id and invoice.partner_id.id or False,
                                    }), 
                            (0, 0, {
                                        'account_id': invoice.journal_id.accured_debit_account_id and  invoice.journal_id.accured_debit_account_id.id or False,
                                        'coa_conac_id': invoice.journal_id.conac_accured_debit_account_id and invoice.journal_id.conac_accured_debit_account_id.id or False,
                                        'debit': balance,
                                        'exclude_from_invoice_tab': True,
                                        'conac_move' : True,
                                        'amount_currency' : amount_currency,
                                        'currency_id' : currency_id,
                                        'partner_id':invoice.partner_id and invoice.partner_id.id or False,
                                    })]
            #====== the Bank Journal, for the accounting impact of the "Exercised" Budget ======#
            if not self.journal_id.execercise_credit_account_id or not self.journal_id.conac_exe_credit_account_id \
                or not self.journal_id.execercise_debit_account_id or not self.journal_id.conac_exe_debit_account_id :
                raise ValidationError("Please configure UNAM and CONAC Exercised account in %s journal!" %
                                    self.journal_id.name)
            
            invoice.line_ids = [(0, 0, {
                                        'account_id': self.journal_id.execercise_credit_account_id and self.journal_id.execercise_credit_account_id.id or False,
                                        'coa_conac_id': self.journal_id.conac_exe_credit_account_id and self.journal_id.conac_exe_credit_account_id.id or False,
                                        'credit': balance, 
                                        'exclude_from_invoice_tab': True,
                                        'conac_move' : True,
                                        'amount_currency' : -amount_currency,
                                        'currency_id' : currency_id,
                                        'partner_id':invoice.partner_id and invoice.partner_id.id or False,
                                    }), 
                            (0, 0, {
                                        'account_id': self.journal_id.execercise_debit_account_id and self.journal_id.execercise_debit_account_id.id or False,
                                        'coa_conac_id': self.journal_id.conac_exe_debit_account_id and self.journal_id.conac_exe_debit_account_id.id or False,
                                        'debit': balance,
                                        'exclude_from_invoice_tab': True,
                                        'conac_move' : True,
                                        'amount_currency' : amount_currency,
                                        'currency_id' : currency_id,      
                                        'partner_id':invoice.partner_id and invoice.partner_id.id or False,                               
                                    })]

    def schedule_payment(self):
        """
        Programación de pagos de forma manual (flujo desde la interfaz de odoo)
        """
        self.env['account.payment'].schedule_payment_with_bank_account(self.journal_id, self.invoice_ids.ids)