from odoo import models, fields, api, _
from odoo.exceptions import UserError
import re

class Titulars(models.Model):
	_name = 'titulars'
	_description = 'Titulars'
	
	name = fields.Char(string="Name Titular",required=True)
	email = fields.Char(string="Email",required=True)
	phone = fields.Char(string="Phone")
	money_funds_id = fields.Many2one('money.funds',string="Money Fund")
		
	@api.model
	def create(self, vals):
		res = super(Titulars, self).create(vals)
		return res

	#======================================================
	#----------------Revisar datos
	#======================================================
	#Revisión de correo de Titular
	@api.constrains('email')
	def _check_email(self):
		regex="[\w\.]+@([\w]+\.)+[\w]{2,}"
		if self.email and (not re.search(regex,self.email)):
			raise UserError("Correo invalido del titular "+self.name+". Por favor ingrese un correo con formato válido (mail@mail.com).")
