import logging
from odoo import models, fields, api, _
from datetime import datetime, timedelta
from odoo.exceptions import UserError, ValidationError
import datetime as dt
import pandas
from pandas.tseries.offsets import BMonthEnd, BMonthBegin


# CONSTANTS FOR ACTIONS
IR_ACTIONS_ACT_WINDOW = 'ir.actions.act_window'

# CONSTANTS FOR FORMAT
DATE_FORMAT_YMD = '%Y-%m-%d'

# CONSTANTS FOR FIELDS DESCRIPTION
UDIBONOS_ID_FIELD_DESCRIPTION = 'Investment UDIBONOS'

# CONSTANTS FOR GROUPS
INVESTMENT_ADMIN_USER_GROUP_ID = 'jt_investment.group_investment_admin_user'

# CONSTANTS FOR MODELS
ACCOUNT_ACCOUNT_MODEL = 'account.account'
ACCOUNT_JOURNAL_MODEL = 'account.journal'
ACCRUED_INTEREST_MODEL = 'accrued.interest'
INVESTMENT_UDIBONOS_MODEL = 'investment.udibonos'
UDIBONOS_PERFORMANCE_MODEL = 'udibonos.performance'
UDI_VALUE_WIZARD_MODEL = 'udi.value.wizard'

# CONSTANTS FOR MESSAGES
NEGATIVE_OR_ZERO_VALUES_NOT_ALLOWED_MESSAGE = 'Negative values or values equal to zero are not allowed'
PERMISSION_DENIED_MESSAGE = "You don't have permission to perform this action."



class UDIBONOS(models.Model):

    _name = 'investment.udibonos'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = "Investment UDIBONOS"
    _rec_name = 'first_number'

    serie = fields.Char("Serie")
    days_of_periot = fields.Integer("Days of Period", digits=(5),default=182)
    settlement_weekday = fields.Selection([('monday', 'Monday'), ('tuesday', 'Tuesday'), ('wednesday', 'Wednesday'), (
        'thursday', 'Thursday'), ('friday', 'Friday')], default='thursday', string="Settlement Weekday")
    title_purchase_value = fields.Float("Title Purchase Value", digits=(8, 6))
    purchase_udi_value = fields.Float("Purchase UDI Value", digits=(4, 9))
    coupon_rate = fields.Float("Coupon Rate")
    amount_invest = fields.Float("Amount to invest", digits=(8, 6))
    supporting_documentation_files = fields.Many2many(
        "ir.attachment", string="Add Files")
    date_days_period_elapsed = fields.Date()
    journal_accrued_interest = fields.Many2one(
        ACCOUNT_JOURNAL_MODEL, 'Journal Accrued Interest')
    journal_performance = fields.Many2one(
        ACCOUNT_JOURNAL_MODEL, 'Journal Performance')
    add_days_of_periot = fields.Integer("Days of the Period Elapsed to the Purchase Date")

    # Calculados
    present_value_bond = fields.Float(
        string="Present Value of the Bond", compute="get_present_value_bond", store=True, digits=(8, 4))
    number_of_title = fields.Integer(
        "Number of Titles", compute="get_number_of_title", store=True)
    total_currency_rate = fields.Float(
        string="Total", compute="get_total_currency_rate", store=True, digits=(8, 4))
    remnant = fields.Float("Remnant", compute="get_remnant",
                           store="true", digits=(8, 4))
    title_value = fields.Float(
        "Title Value", compute="get_title_value", digits=(8, 6))

    # Cierre de mes
    accrued_interest_to_date = fields.Float(
        "Accrued Interest to date", digits=(6, 4), copy = False)
    last_month_accrued_interest_to_date = fields.Float(
        "Accrued Interest to date", digits=(6, 4), copy = False)
    udi_value_first_day_month = fields.Float(
        "UDI Value First Day Month", digits=(4, 6), copy = False)
    udi_value_end_month = fields.Float("UDI Value end Month", digits=(4, 6), copy = False)
    value_udi_expiration_date = fields.Float("value udi expiration date")
    accrued_interest = fields.Float("Temp Accrued Interest", digits=(6, 4), copy = False)

    days_period_elapsed = fields.Integer("Days Period Elapsed")
    value_of_tittle_on_purchase = fields.Float(
        "Present Value Bond", compute="get_value_of_tittle_on_purchase", store=True, digits=(8, 6))
    unrecorded_interest = fields.Float("Unrecorded Interest", digits=(4, 6), copy = False)
    last_month_unrecorded_interest = fields.Float(
        "Unrecorded Interest", digits=(4, 6), copy = False)
    coupon_payment_date = fields.Date("Coupon Payment Date")


    # Dato usado solo para pruebas
    movement_date = fields.Date("Fecha que se realiza el movimiento",copy=False)
    cantidad_liquidacion = fields.Float("Cantidad a liquidar", digits=(6, 4))
    line_ids = fields.One2many(
        'account.move.line', 'udibonos_id', string="Accounting Notes")

    line_ids_accrued_interest = fields.One2many(
        ACCRUED_INTEREST_MODEL, 'udibonos_id', string="Accrued Interest")

    line_ids_udibonos_performance = fields.One2many(
        UDIBONOS_PERFORMANCE_MODEL, 'udibonos_id', string="Udibonos Performance")

    first_number = fields.Char('First Number:')
    new_journal_id = fields.Many2one(ACCOUNT_JOURNAL_MODEL, 'Journal')

    folio = fields.Integer("Folio")
    date_time = fields.Datetime("Date Time")
    journal_id = fields.Many2one(ACCOUNT_JOURNAL_MODEL, 'Bank Account')
    journal_id_bank = fields.Char(String="Bank Used for the Transaction", store=True, compute = "get_journal_id_bank")
    journal_id_account = fields.Char(String="Accout Used for the Transaction", store=True, compute = "get_journal_id_account")

    instrument_done = fields.Char("Close Instrument", default="No")

    @api.depends('journal_id')
    def get_journal_id_bank(self):
        self.journal_id_bank = self.journal_id.bank_id.name
        
    @api.depends('journal_id')
    def get_journal_id_account(self):
        self.journal_id_account = self.journal_id.bank_account_id.acc_number

    # Campos para el cierre de mes en plusvalia / minusvalia
    previous_month_titles_value = fields.Float(digits=(8, 6))
    current_month_titles_value = fields.Float(digits=(8, 6))    

    bank_id = fields.Many2one(related="journal_id.bank_id")
    currency_id = fields.Many2one(
        'res.currency', string='Currency', default=lambda self: self.env.company.currency_id)
    company_id = fields.Many2one(
        'res.company', string='Company', default=lambda self: self.env.company)
    investment_rate_id = fields.Many2one(
        "investment.period.rate", "Exchange rate")
    concept = fields.Text("Application Concept")
    dependency_id = fields.Many2one('dependency', "Dependency")
    sub_dependency_id = fields.Many2one('sub.dependency', "Subdependency")
    reason_rejection = fields.Text("Reason Rejection")
    contract_id = fields.Many2one("investment.contract", "Contract")
    instrument_it = fields.Selection(
        [('bank', 'Bank'), ('paper_government', 'Paper Government Paper')], string="Document")
    account_executive = fields.Char("Account Executive")
    UNAM_operator = fields.Many2one("hr.employee", "UNAM Operator")
    is_federal_subsidy_resources = fields.Boolean(
        "Federal Subsidy Resourcesss")
    observations = fields.Text("Observations")
    origin_resource_id = fields.Many2one(
        'sub.origin.resource', "Origin of the resource")

    kind_of_product = fields.Selection(
        [('investment', 'Investment')], string="Kind Of Product", default="investment")
    key = fields.Char("Identification Key")
    issue_date = fields.Date('Date of issue')
    due_date = fields.Date('Due Date')
    nominal_value = fields.Float(
        related='amount_invest', string="Nominal Value")
    interest_rate = fields.Float("Interest Rate", digits='UDIBONOS')
    time_for_each_cash_flow = fields.Integer(
        string="Time for each cash flow", size=4)
    time_to_expiration_date = fields.Integer(
        string="Time to Expiration Date", size=4)
    coupon = fields.Float(
        string="Coupon", compute="get_coupon_amount", store=True)
    state = fields.Selection([('draft', 'Draft'), ('requested', 'Requested'), ('rejected', 'Rejected'), ('approved', 'Approved'),
                             ('confirmed', 'Confirmed'), ('active', 'Active'), ('done', 'Done')], string="Status", default='draft')

    estimated_interest = fields.Float(
        string="Estimated Interest", compute="get_estimated_interest", store=True)

    real_interest = fields.Float("Real Interest")

    profit_variation = fields.Float(
        string="Estimated vs Real Profit Variation", compute="get_profit_variation", store=True)

    month_key = fields.Char("Identification Key")
    month_issue_date = fields.Date('Date of issue')
    month_due_date = fields.Date('Due Date')

    udi_value = fields.Float("UDI value")
    udi_value_multiplied = fields.Float(
        string="The value of the Udi is multiplied by 100", default=100)
    period_days = fields.Float("Period days")

    monthly_nominal_value = fields.Float(
        string="Nominal value of the security in investment units", compute="get_monthly_nominal_value", store=True)
    monthly_estimated_interest = fields.Float(
        string="Estimated Interest", compute="get_monthly_estimated_interest", store=True)
    monthly_real_interest = fields.Float("Real Interest")

    monthly_profit_variation = fields.Float(
        string="Estimated vs Real Profit Variation", compute="get_month_profit_variation", store=True)
    rate_of_returns = fields.Many2one(
        'rate.of.returns', string="Rate Of Returns")

    # ====== Accounting Fields =========#

    investment_income_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, 'Income Account')
    investment_expense_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, 'Expense Account')
    investment_price_diff_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, 'Price Difference Account')

    return_income_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, 'Income Account')
    return_expense_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, 'Expense Account')
    return_price_diff_account_id = fields.Many2one(
        ACCOUNT_ACCOUNT_MODEL, 'Price Difference Account')

    request_finance_ids = fields.One2many(
        'request.open.balance.finance', 'udibonos_id', copy=False)

    fund_type_id = fields.Many2one('fund.type', "Type Of Fund")
    agreement_type_id = fields.Many2one(
        'agreement.agreement.type', 'Agreement Type')
    fund_id = fields.Many2one('agreement.fund', 'Fund')
    fund_key = fields.Char(related='fund_id.fund_key',
                           string="Password of the Fund")
    base_collaboration_id = fields.Many2one(
        'bases.collaboration', 'Name Of Agreements')

    investment_fund_id = fields.Many2one(
        'investment.funds', 'Investment Funds', copy=False)
    expiry_date = fields.Date(string="Expiration Date")
    yield_id = fields.Many2one('yield.destination', 'Yield Destination')


    purchase_tittle_value = fields.Float(
        string="Valor de Compra de los Titulos", compute="get_purchase_tittle_value", store=True, digits=(8, 6))

    @api.depends('purchase_udi_value')
    def get_purchase_tittle_value(self):
        self.purchase_tittle_value = self.purchase_udi_value * 100


    @api.depends('title_purchase_value')
    def get_value_of_tittle_on_purchase(self):
        self.value_of_tittle_on_purchase = self.title_purchase_value * 100

    @api.depends('amount_invest', 'purchase_tittle_value')
    def get_number_of_title(self):
        if self.amount_invest > 0:
            if (self.amount_invest and self.purchase_tittle_value):
                self.number_of_title = int(
                    self.amount_invest / self.purchase_tittle_value)
        else:
            self.number_of_title = 0

    @api.depends('number_of_title', 'purchase_tittle_value')
    def get_total_currency_rate(self):
        self.total_currency_rate = self.number_of_title * self.purchase_tittle_value
        

    @api.depends('amount_invest', 'total_currency_rate')
    def get_remnant(self):
        if self.amount_invest > 0:
            self.remnant = self.amount_invest - self.total_currency_rate
        else:
            self.remnant = 0

    @api.depends('number_of_title', 'value_of_tittle_on_purchase')
    def get_title_value(self):
        self.title_value = self.number_of_title * self.value_of_tittle_on_purchase

    @api.onchange('date_time')
    def onchange_date_time(self):
        if self.date_time:
            self.issue_date = self.date_time
            self.month_issue_date = self.date_time
        else:
            self.issue_date = False
            self.month_issue_date = False

    @api.onchange('expiry_date')
    def onchange_expiry_date(self):
        if self.expiry_date:
            self.due_date = self.expiry_date
            self.month_due_date = self.expiry_date
        else:
            self.due_date = False
            self.month_due_date = False

    @api.constrains('amount_invest')
    def check_min_balance(self):
        if self.amount_invest == 0:
            raise UserError(_('Please add amount invest'))

        
    @api.constrains('add_days_of_periot')
    def check_add_days_of_periot(self):
        if self.add_days_of_periot < 0:
            raise UserError(_('Negative days are not allowed'))
        
        if self.add_days_of_periot > 181:
            raise UserError(_('No more than 181 days can be added'))
        
    @api.constrains('title_purchase_value')
    def check_title_purchase_value(self):
        if self.title_purchase_value <= 0:
            raise UserError(_(NEGATIVE_OR_ZERO_VALUES_NOT_ALLOWED_MESSAGE))
        
    @api.constrains('purchase_udi_value')
    def check_purchase_udi_value(self):
        if self.purchase_udi_value <= 0:
            raise UserError(_(NEGATIVE_OR_ZERO_VALUES_NOT_ALLOWED_MESSAGE))
            
    @api.constrains('coupon_rate')
    def check_coupon_rate(self):
        if self.coupon_rate <= 0:
            raise UserError(_(NEGATIVE_OR_ZERO_VALUES_NOT_ALLOWED_MESSAGE))

    @api.constrains('amount_invest')
    def check_amount_invest(self):
        if self.amount_invest <= 0:
            raise UserError(_(NEGATIVE_OR_ZERO_VALUES_NOT_ALLOWED_MESSAGE))
        

    def unlink(self):       
        for rec in self:
            if rec.state not in ['draft']:
                raise UserError(_('You can delete only draft status data.'))
            # Eliminar los historicos de intereses devengados relacionados con udibonos
            accrued_records = rec.env[ACCRUED_INTEREST_MODEL].search([('udibonos_id', '=', rec.id)])
            for records in accrued_records:
                records.unlink()
                    
        return super(UDIBONOS, self).unlink()

    @api.onchange('contract_id')
    def onchange_contract_id(self):
        if self.contract_id:
            self.fund_type_id = self.contract_id.fund_type_id and self.contract_id.fund_type_id.id or False
            self.agreement_type_id = self.contract_id.agreement_type_id and self.contract_id.agreement_type_id.id or False
            self.fund_id = self.contract_id.fund_id and self.contract_id.fund_id.id or False
            self.base_collaboration_id = self.contract_id.base_collabaration_id and self.contract_id.base_collabaration_id.id or False
        else:
            self.fund_type_id = False
            self.agreement_type_id = False
            self.fund_id = False
            self.base_collaboration_id = False

    @api.depends('nominal_value', 'interest_rate')
    def get_coupon_amount(self):
        for rec in self:
            rec.coupon = (rec.nominal_value * rec.interest_rate)/100

    @api.depends('amount_invest')
    def get_total_currency_amount(self):
        if self.amount_invest:
            self.total_currency_rate = self.amount_invest
        else:
            self.total_currency_rate = 0

    @api.depends('interest_rate', 'time_for_each_cash_flow', 'nominal_value')
    def get_present_value_bond(self):
        for rec in self:
            VA = rec.nominal_value
            r = rec.interest_rate / 100
            n = rec.time_for_each_cash_flow
            value = (VA * r) * ((1 + r) ** n-1) / \
                (r + (1 + r) ** r) + VA * (1 / (1 + r) ** r)
            rec.present_value_bond = value

    @api.depends('present_value_bond', 'nominal_value')
    def get_estimated_interest(self):
        for rec in self:
            rec.estimated_interest = rec.present_value_bond - rec.nominal_value

    @api.depends('estimated_interest', 'real_interest')
    def get_profit_variation(self):
        for rec in self:
            rec.profit_variation = rec.real_interest - rec.estimated_interest

    @api.depends('number_of_title', 'udi_value', 'udi_value_multiplied')
    def get_monthly_nominal_value(self):
        for rec in self:
            rec.monthly_nominal_value = rec.number_of_title * \
                (rec.udi_value*rec.udi_value_multiplied)

    @api.depends('monthly_nominal_value', 'coupon_rate', 'period_days')
    def get_monthly_estimated_interest(self):
        for rec in self:
            rec.monthly_estimated_interest = rec.monthly_nominal_value * \
                ((rec.coupon_rate/100)/360)*rec.period_days

    @api.depends('monthly_estimated_interest', 'monthly_real_interest')
    def get_month_profit_variation(self):
        for rec in self:
            rec.monthly_profit_variation = rec.monthly_real_interest - \
                rec.monthly_estimated_interest

    def write(self, vals):
        


        res = super(UDIBONOS, self).write(vals)
        if vals.get('expiry_date'):
            pay_regis_obj = self.env['calendar.payment.regis']
            pay_regis_rec = pay_regis_obj.search([('date', '=', vals.get('expiry_date')),
                                                  ('type_pay', '=', 'Non Business Day')], limit=1)
            if pay_regis_rec:
                raise ValidationError(
                    _("You have choosen Non-Business Day on Expiry Date!"))
        return res
        

    @api.model
    def create(self, vals):


        vals['folio'] = self.env['ir.sequence'].next_by_code('folio.udibonos')
        res = super(UDIBONOS, self).create(vals)
        res.date_days_period_elapsed = res.issue_date
        res.instrument_done='No'

        sequence = res.new_journal_id and res.new_journal_id.sequence_id or False
        if not sequence:
            raise UserError(_('Please define a sequence on your journal.'))

        res.first_number = sequence.with_context(
            ir_sequence_date=res.date_time).next_by_id()

        return res
    
    def action_confirm(self):
        # Seguridad de la función
        if not self.env.user.has_group(INVESTMENT_ADMIN_USER_GROUP_ID):
            raise UserError(_(PERMISSION_DENIED_MESSAGE))

        today = datetime.today().date()
        user = self.env.user
        employee = self.env['hr.employee'].search(
            [('user_id', '=', user.id)], limit=1)

        return {
            'name': _('Approve Request'),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': False,
            'res_model': 'approve.money.market.bal.req',
            'type': IR_ACTIONS_ACT_WINDOW,
            'target': 'new',
            'context': {
                'default_amount': self.amount_invest,
                'default_date': today,
                'default_employee_id': employee.id if employee else False,
                'default_udibonos_id': self.id,
                'default_bank_account_id': self.journal_id and self.journal_id.id or False,
                'show_for_supplier_payment': 1,
                'default_desti_bank_account_id': self.new_journal_id and self.new_journal_id.id or False,
            }
        }

    def action_draft(self):
        self.state = 'draft'

    def action_reset_to_draft(self):
        # Seguridad de la función
        if not self.env.user.has_group(INVESTMENT_ADMIN_USER_GROUP_ID):
            raise UserError(_(PERMISSION_DENIED_MESSAGE))
        self.state = 'draft'


    def action_requested(self):
        self.state = 'requested'


    def action_approved(self):
        self.state = 'approved'


    def action_confirmed(self):
        self.state = 'confirmed'


    def action_done(self):
        self.state = 'done'
        self.liquidate()

    def action_reject(self):
        self.state = 'rejected'

    def action_canceled(self):
        pass


    def action_calculation(self):
        return

    def action_reinvestment(self):
        return

    def action_published_entries(self):
        return {
            'name': 'Published Entries',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'request.open.balance.finance',
            'domain': [('udibonos_id', '=', self.id)],
            'type': IR_ACTIONS_ACT_WINDOW,
            'context': {'default_udibonos_id': self.id}
        }

    def activate_investment(self):
        # Seguridad de la función
        if not self.env.user.has_group(INVESTMENT_ADMIN_USER_GROUP_ID):
            raise UserError(_(PERMISSION_DENIED_MESSAGE))
        self.state = 'active'
        self.issue_date = self.movement_date
        self.date_days_period_elapsed = self.issue_date

        self.calculate_date_to_pay_coupon()
        self.calculate_days_periot_elapsed()
        self.coupon_payment_activity_notification()

    def accounting_action(self, debit, credit, amount, description):
        move_id = ''
        today = self.movement_date  # datetime.today().date()
        partner_id = self.env.user.partner_id
        account_account_obj = self.env[ACCOUNT_ACCOUNT_MODEL]

        debit_account = account_account_obj.search([('code', '=', debit)])
        credit_account = account_account_obj.search([('code', '=', credit)])

        move_vals = {'ref': '', 'conac_move': False,
                            'date': today, 'journal_id': self.new_journal_id.id, 'company_id': self.env.user.company_id.id,
                            'line_ids': [(0, 0, {
                                'name':	 description,
                                'account_id': debit_account.id,
                                'debit': amount,
                                'partner_id': partner_id.id,
                                'udibonos_id': self.id,
                            }),
                                (0, 0, {
                                    'name':	 description,
                                    'account_id': credit_account.id,
                                    'credit': amount,
                                    'partner_id': partner_id.id,
                                    'udibonos_id': self.id,
                                }),
                            ]}

        move_obj = self.env['account.move']
        move_id = move_obj.create(move_vals)
        move_id.action_post()
        return move_id

    def calculate_days_of_periot(self):
        today = self.movement_date  # datetime.today().date()
        # Dias del periodo transcurridos
        self.days_period_elapsed = ((today-self.date_days_period_elapsed).days)

        if self.days_period_elapsed >= self.days_of_periot:
            self.calculate_coupon_payment()
        if today >= self.expiry_date:
            self.liquidate()


    def calculate_days_periot_elapsed(self):
        today = self.movement_date  # datetime.today().date()
        self.days_period_elapsed = ((today-self.date_days_period_elapsed).days)+self.add_days_of_periot

    def end_month(self):

        # Seguridad de la función
        if not self.env.user.has_group(INVESTMENT_ADMIN_USER_GROUP_ID):
            raise UserError(_(PERMISSION_DENIED_MESSAGE))

        # Si el registro esta en estado activo se realizan las acciones
        if self.state == 'active':
            today = self.movement_date  # datetime.today().date()


            # Verifica que la fecha sea el ultimo dia habil del mes
            current_date = pandas.Timestamp(today)

            if not current_date.is_month_end:
                raise UserError(_("The date is not the last day of the month."))
        
            # Dias del periodo transcurridos
            self.days_period_elapsed = ((today-self.date_days_period_elapsed).days)+self.add_days_of_periot

            if self.days_period_elapsed >= self.days_of_periot:
                raise UserError(_(f"Primero debe realizar un Pago el Pago de Cupón. Los días del periodo transcurridos ({self.days_period_elapsed}) son mayores a los días del periodo ( {self.days_of_periot}). "))
                
            # Valor nominal de la UDI desde banxico al final del mes
            udi_end_month_nominal_value = (self.udi_value_end_month * 100) * self.number_of_title

            # Calculo de los intereses devengados
            self.accrued_interest = udi_end_month_nominal_value * \
                (((self.coupon_rate/100)/360) * self.days_period_elapsed)

            # Se calculan los intereses devengados a la fecha
            self.accrued_interest_to_date = self.last_month_accrued_interest_to_date + \
                self.last_month_unrecorded_interest
            self.last_month_accrued_interest_to_date = self.accrued_interest_to_date

            # Calculo interes no registrado
            self.unrecorded_interest = self.accrued_interest - self.accrued_interest_to_date
            self.last_month_unrecorded_interest = self.unrecorded_interest

            # Se realiza registro de intereses devengados
            self.env[ACCRUED_INTEREST_MODEL].create(
                {
                    'date': today,
                    'udibonos_id': self.id,
                    'udi_value_end_month': self.udi_value_end_month,
                    'days_period_elapsed': self.days_period_elapsed,
                    'accrued_interest': self.accrued_interest,
                    'accrued_interest_to_date': self.accrued_interest_to_date,
                    'unrecorded_interest': self.unrecorded_interest,
                    'description': 'Movimiento',
                }
            )


            if self.unrecorded_interest > 0:
                # Apunte contable de los intereses devengados no registrados
                self.accounting_action(self.journal_accrued_interest.default_debit_account_id.code,
                                    self.journal_accrued_interest.interest_account_id.code, self.unrecorded_interest, "Intereses devengados")

    def calculate_coupon_payment(self):
        # Seguridad de la función
        if not self.env.user.has_group(INVESTMENT_ADMIN_USER_GROUP_ID):
            raise UserError(_(PERMISSION_DENIED_MESSAGE))

        if self.state == 'active':
            today = self.movement_date  # datetime.today().date()

            udi_value_coupon_date_payment = self.udi_value_end_month
            # Dias del periodo transcurridos
            self.days_period_elapsed = ((today-self.date_days_period_elapsed).days)+self.add_days_of_periot
            

            if self.days_period_elapsed < 182 or self.days_period_elapsed > 182:
                raise UserError(
                    #_("The period elapsed for coupon payment is 182 days"))
                    _(f"El periodo para pago de cupón son 182 días no {self.days_period_elapsed}"))
            

            # Valor nominal de la inversión en UDIBONOS a la fecha del pago del cupón
            investment_nominal_value = (
                udi_value_coupon_date_payment * 100)*self.number_of_title
            


            # Calculo del pago del cupon / Intereses devengados
            coupon_payment = investment_nominal_value * \
                (((self.coupon_rate/100)/360) * self.days_period_elapsed)

            # Se calculan los intereses devengados a la fecha
            self.accrued_interest_to_date = self.last_month_accrued_interest_to_date + \
                self.last_month_unrecorded_interest

           # Se calculan los intereses no registrados
            unrecorded_interest = coupon_payment - self.accrued_interest_to_date

            # Se registra el apunte contable del pago del cupón, los intereses no registrados y la cancelación de los intereses devengados
            # Apunte contable pago del cupon
            self.accounting_action(self.journal_id.default_debit_account_id.code,
                                self.journal_performance.interest_account_id.code, coupon_payment, "Pago de cupón")


            
            # Apunte contable de los intereses devengados no registrados
            self.accounting_action(self.journal_accrued_interest.interest_account_id.code,
                                   self.journal_accrued_interest.default_debit_account_id.code, self.accrued_interest_to_date, "Cancelación de intereses devengados")



            # Se realiza registro de pago del cupon
            self.env[ACCRUED_INTEREST_MODEL].create(
                {
                    'date': today,
                    'udibonos_id': self.id,
                    'udi_value_end_month': self.udi_value_end_month,
                    'days_period_elapsed': self.days_period_elapsed,
                    'coupon_payment': coupon_payment,
                    'accrued_interest_to_date': self.accrued_interest_to_date,
                    'unrecorded_interest': unrecorded_interest,
                    'description': 'Pago de Cupón',
                }
            )

            # Se reinician los días del periodo y el acumulado de los intereses devengados, dejando el histórico
            self.days_period_elapsed = 0
            self.add_days_of_periot = 0
            self.date_days_period_elapsed = today
            self.accrued_interest = 0
            self.unrecorded_interest = 0
            self.last_month_unrecorded_interest = 0
            self.last_month_accrued_interest_to_date = 0
            self.calculate_date_to_pay_coupon()
            self.coupon_payment_activity_notification()


    def liquidate(self):
        # Seguridad de la función
        if not self.env.user.has_group(INVESTMENT_ADMIN_USER_GROUP_ID):
            raise UserError(_(PERMISSION_DENIED_MESSAGE))

        if self.state == 'active':
            self.state = 'done'
            today = self.movement_date  # datetime.today().date()

            # Dias del periodo transcurridos
            self.days_period_elapsed = ((today-self.date_days_period_elapsed).days)+self.add_days_of_periot

            udi_value_coupon_date_liquidation = self.udi_value_end_month


            # valor nominal al vencimiento
            expiration_nominal_value = (
                udi_value_coupon_date_liquidation * 100) * self.number_of_title
            
            performance = ''

            # Calculo del pago del cupon
            coupon_interest = expiration_nominal_value * \
                (((self.coupon_rate/100)/360) * self.days_period_elapsed)

            # Calculo de Intereses no registrados
            unrecorded_interest = coupon_interest - self.accrued_interest_to_date

            # Se realiza registro de intereses devengados
            self.env[ACCRUED_INTEREST_MODEL].create(
                {
                    'date': today,
                    'udibonos_id': self.id,
                    'udi_value_end_month': udi_value_coupon_date_liquidation,
                    'days_period_elapsed': self.days_period_elapsed,
                    'coupon_payment': coupon_interest,
                    'accrued_interest_to_date': self.accrued_interest_to_date,
                    'unrecorded_interest': unrecorded_interest,
                    'performance': performance,
                    'description': 'Liquidación',   
                }
            )
        

            # Se registra el apunte contable del pago del cupón, los intereses no registrados y la cancelación de los intereses devengados
            
            if self.accrued_interest_to_date > 0:
                # Apunte contable pago del cupon
                self.accounting_action(self.journal_id.default_debit_account_id.code,
                                    self.journal_performance.interest_account_id.code, coupon_interest, "Pago de cupón")

                # Apunte contable cancelacion de intereses devengados
                self.accounting_action(self.journal_accrued_interest.interest_account_id.code,
                                    self.journal_accrued_interest.default_debit_account_id.code, self.accrued_interest_to_date+self.unrecorded_interest, "Cancelación de intereses devengados")
                                

            # Apunte contable liquidacion de la inversion
            self.accounting_action(self.journal_id.default_debit_account_id.code, self.new_journal_id.default_credit_account_id.code, self.total_currency_rate, "Liquidación")


    def get_latest_rate(self):
        # Consultar valores de las UDIS
        udi_values = self.env['investment.period.rate'].search(
            [('product_type', '=', 'UDIBONOS')])
        for rec in reversed(udi_values):
            if rec.rate_year_3 or rec.rate_year_3 != 0:
                latest_rate = rec.rate_year_3
                break
        return latest_rate

    def udi_value_methods(self):
        view_wizard = self.env.ref('jt_investment.udis_value_wizard')
        msj = 'Esta acción actualizara todos los UDIs de las inversiones en estado Activo'

        alert = self.env[UDI_VALUE_WIZARD_MODEL].create({
            'msj_on_wizard': msj,
            'id_udibono': self._origin.id,
            'udi_value_first_day_month': self.udi_value_first_day_month,
            'udi_value_end_month': self.udi_value_end_month
        })
        return {
            'name': _("Ingreso de Valores de UDI"),
            'res_model': UDI_VALUE_WIZARD_MODEL,
            'views': [(view_wizard.id, 'form')],
            'type': IR_ACTIONS_ACT_WINDOW,
            'target': 'new',
            'res_id': alert.id
        }

    def calculate_date_to_pay_coupon(self):
        # se usa dt y no data time por detalles con la libreria, es por eso que se tiene el "ad dt" en el import

        calculated_date = dt.datetime.strptime((self.date_days_period_elapsed.strftime(DATE_FORMAT_YMD)), DATE_FORMAT_YMD)
        days_to_pay_coupon = self.days_of_periot - self.add_days_of_periot 
        days_of_periot = dt.timedelta(days=days_to_pay_coupon)
        
        # Avanzar la fecha en los días transcurridos
        calculated_date += days_of_periot
        
        #Obtener el numero correspondiente al dia de la semana 
        weekdays = {
            'monday': 0,
            'tuesday': 1,
            'wednesday': 2,
            'thursday': 3,
            'friday': 4,
            'saturday': 5,
            'sunday': 6
        }
        
        weekday = weekdays[self.settlement_weekday]
        
        # Calcular el número de días hasta el próximo dia elegido
        difference = (weekday - calculated_date.weekday()) % 7
        # Avanzar la fecha hasta el próximo dia
        calculated_date += dt.timedelta(days=difference)

        #Si los dias calculados superan a los dias del periodo 
        verify_limit_days = calculated_date - dt.datetime.strptime(self.date_days_period_elapsed.strftime(DATE_FORMAT_YMD), DATE_FORMAT_YMD)
        if verify_limit_days > days_of_periot:
            calculated_date -= (verify_limit_days-days_of_periot)
            while calculated_date.weekday() != weekday:
                calculated_date -= dt.timedelta(1)
        #Registra la fecha de pago del cupon calculada calculada 
        self.coupon_payment_date = calculated_date


    def calculate_performance(self):

        # Seguridad de la función
        if not self.env.user.has_group(INVESTMENT_ADMIN_USER_GROUP_ID):
            raise UserError(_(PERMISSION_DENIED_MESSAGE))

        # Si el registro esta en estado activo se realizan las acciones
        if self.state == 'active':
            today = self.movement_date  # datetime.today().date()

            # Valor nominal en el cierre del mes anterior
            previous_month_nominal_value = self.previous_month_titles_value * self.number_of_title

            # Valor nominal al final del mes actual
            current_month_nominal_value = self.current_month_titles_value * self.number_of_title

            # Plusvalia o Minusvalia contra el monto invertido (Diferencia acumulada)
            accomulated_difference =current_month_nominal_value - self.amount_invest

            # Calculo de plusvalia o minusvalia
            difference = current_month_nominal_value-previous_month_nominal_value
            performance = ''

            if difference > 0:
                performance = "Plusvalia"
                # Apunte contable de Plusvalia
                self.accounting_action(self.journal_performance.default_debit_account_id.code,
                                        self.journal_performance.interest_account_id.code, difference, "Plusvalia")

            if difference < 0:
                performance = "Minusvalia"
                # Apunte contable de Minusvalia
                self.accounting_action(self.journal_performance.interest_account_id.code,
                                        self.journal_performance.default_debit_account_id.code, abs(difference), "Minusvalia")
                
            # Se realiza registro de rendimiento
            self.env[UDIBONOS_PERFORMANCE_MODEL].create(
                {
                    'date': today,
                    'udibonos_id': self.id,
                    'current_month_titles_value': self.current_month_titles_value,
                    'previous_month_titles_value': self.previous_month_titles_value,
                    'current_month_nominal_value': current_month_nominal_value,
                    'previous_month_nominal_value': previous_month_nominal_value,
                    'accomulated_difference' : accomulated_difference,
                    'difference': difference,
                    'performance': performance,
                    'description': 'Rendimiento',
                }
            )
            

    def calculate_performance_liquidation(self):

        # Seguridad de la función
        if not self.env.user.has_group(INVESTMENT_ADMIN_USER_GROUP_ID):
            raise UserError(_(PERMISSION_DENIED_MESSAGE))

        # Si el registro esta en estado activo se realizan las acciones
        if self.state == 'done':
            today = self.movement_date  # datetime.today().date()

            # Valor nominal en el cierre del mes anterior
            previous_month_nominal_value = self.previous_month_titles_value * self.number_of_title

            # Valor nominal al final del mes actual
            current_month_nominal_value = self.current_month_titles_value * self.number_of_title

            # Plusvalia o Minusvalia contra el monto invertido (Diferencia acumulada)
            accomulated_difference = current_month_nominal_value - self.amount_invest

            # Calculo de plusvalia o minusvalia
            difference = current_month_nominal_value-previous_month_nominal_value
            performance = ''

            # Apunte contable de rendimiento en la liquidación 

            if difference > 0:
                performance = "Plusvalia"
                # Apunte contable de Plusvalia
                self.accounting_action(self.journal_performance.default_debit_account_id.code,
                                        self.journal_performance.interest_account_id.code, difference, "Plusvalia")

            if difference < 0:
                performance = "Minusvalia"
                # Apunte contable de Minusvalia
                self.accounting_action(self.journal_performance.interest_account_id.code,
                                        self.journal_performance.default_debit_account_id.code, abs(difference), "Minusvalia")

            # Apunte contable de rendimiento acumulado
            
            if accomulated_difference > 0:
                # Apunte contable de Plusvalia
                self.accounting_action(self.journal_id.default_debit_account_id.code, self.journal_performance.default_debit_account_id.code, accomulated_difference, "Plusvalia Acumulada")

            if accomulated_difference < 0:
                # Apunte contable de Minusvalia
                self.accounting_action(self.journal_performance.default_debit_account_id.code, self.journal_id.default_debit_account_id.code, abs(accomulated_difference), "Minusvalia  Acumulada")
                
                
            # Se realiza registro de rendimiento
            self.env[UDIBONOS_PERFORMANCE_MODEL].create(
                {
                    'date': today,
                    'udibonos_id': self.id,
                    'current_month_titles_value': self.current_month_titles_value,
                    'previous_month_titles_value': self.previous_month_titles_value,
                    'current_month_nominal_value': current_month_nominal_value,
                    'previous_month_nominal_value': previous_month_nominal_value,
                    'accomulated_difference' : accomulated_difference,
                    'difference': difference,
                    'performance': performance,
                    'description': 'Rendimiento',
                }
            )


            self.instrument_done = "Finalizado"
        
            

    def get_udi_value(self):
        # Consultar valores de las UDIS
        return {
            'type': IR_ACTIONS_ACT_WINDOW,
            'name': 'UDI Value',
            'res_model': UDI_VALUE_WIZARD_MODEL,
            'view_mode': 'form',
            'view_type': 'form',
            'target': 'new',
        }
    
    def call_end_month_wizard(self):
        # Calcular intereses devengados

        return {
            'type': IR_ACTIONS_ACT_WINDOW,
            'name': 'Cierre de Mes',
            'res_model': 'udi.end.month.wizard',
            'view_mode': 'form',
            'view_type': 'form',
            'target': 'new',
        }
    def call_coupon_payment_wizard(self):
        # Realizar pago de cupon

        return {
            'type': IR_ACTIONS_ACT_WINDOW,
            'name': 'Pago de Cupón',
            'res_model': 'udi.coupon.payment.wizard',
            'view_mode': 'form',
            'view_type': 'form',
            'target': 'new',
        }
    
    def call_liquidate_wizard(self):
        # Realizar liquidacion

        return {
            'type': IR_ACTIONS_ACT_WINDOW,
            'name': 'Liquidación',
            'res_model': 'udi.liquidate.wizard',
            'view_mode': 'form',
            'view_type': 'form',
            'target': 'new',
        }
    
    def call_performance_wizard(self):
        # Realizar liquidacion

        return {
            'type': IR_ACTIONS_ACT_WINDOW,
            'name': 'Rendimiento',
            'res_model': 'udibonos.performance.wizard',
            'view_mode': 'form',
            'view_type': 'form',
            'target': 'new',
        }

    def call_performance_liquidation_wizard(self):
        # Realizar liquidacion

        return {
            'type': IR_ACTIONS_ACT_WINDOW,
            'name': 'Rendimiento',
            'res_model': 'udibonos.performance.liquidation.wizard',
            'view_mode': 'form',
            'view_type': 'form',
            'target': 'new',
        }

    def coupon_payment_activity_notification(self):
        # Notificar pago de cupon
        group = self.env.ref(INVESTMENT_ADMIN_USER_GROUP_ID)
        date = self.coupon_payment_date.strftime('%d/%m/%Y')
        
        users = self.env['res.users'].search([('groups_id', 'in', group.id)])
        
        for user in users:
            self.activity_schedule(
                'jt_investment.udibonos_activity_coupon_payment',
                user_id=user.id,
                note=f'Realizar Pago de Cupón {date}',
                date_deadline=self.coupon_payment_date
            )



    def calculate_total_performance(self):
        # Calcular rendimiento total
        performance = self.env[UDIBONOS_PERFORMANCE_MODEL].search([('udibonos_id', '=', self.id)])
        # Sumar accomulated_difference de el objeto performance para obtener el total
        total = 0
        for rec in performance:
            total += rec.accomulated_difference
        
        if total > 0:
            # Apunte contable Plus/Min Acumulada
            self.accounting_action(self.journal_id.default_credit_account_id.code, self.journal_performance.default_debit_account_id.code, total, "Plusvalía Acumulada")

        if total < 0:
            # Apunte contable de Minusvalia
            self.accounting_action(self.journal_performance.default_debit_account_id.code, self.journal_id.default_credit_account_id.code, abs(total), "Minusvalía Acumulada")
            

class AccountMoveLine(models.Model):

    _inherit = 'account.move.line'

    udibonos_id = fields.Many2one(
        INVESTMENT_UDIBONOS_MODEL, UDIBONOS_ID_FIELD_DESCRIPTION)


class AccruedInterestLine(models.Model):

    _inherit = ACCRUED_INTEREST_MODEL

    udibonos_id = fields.Many2one(
        INVESTMENT_UDIBONOS_MODEL, UDIBONOS_ID_FIELD_DESCRIPTION)


class UdibonosPerformanceLine(models.Model):

    _inherit = UDIBONOS_PERFORMANCE_MODEL

    udibonos_id = fields.Many2one(
        INVESTMENT_UDIBONOS_MODEL, UDIBONOS_ID_FIELD_DESCRIPTION)


