from odoo import models, fields, api,_
from datetime import datetime, timedelta
from odoo.exceptions import ValidationError
from lxml import etree

ACCOUNT_PAYMENT = 'account.payment'
class ExpenseRecognitionCriteria(models.Model):

    _name = 'expense.recognition'
    _description = "Expense recognition criteria"
    _rec_name = 'bank_account_id'

    # Cuenta bancaria
    bank_account_id = fields.Many2one('res.partner.bank', string='Account Bank')
    bank_id = fields.Many2one(related='bank_account_id.bank_id', string='Bank',store=True)

    # Método de pago
    l10n_mx_edi_payment_method_id = fields.Many2one('l10n_mx_edi.payment.method',
        string='Payment Way',
        default=lambda self: self.env.ref('l10n_mx_edi.payment_method_transferencia', raise_if_not_found=False))
    is_check = fields.Boolean(compute='_compute_is_check', store=False)

    desc_movement = fields.Char('Domain Movement')
    reference_client = fields.Char('Domain Reference Client')
    reference_plugin_1 = fields.Char('Domain Reference Plugin 1')
    reference_plugin_2 = fields.Char('Domain Reference plugin 2')
    reference_plugin_3 = fields.Char('Domain Reference plugin 3')

    # UPA
    upa_key = fields.Char('UPA')
    # Consecutivo del tipo de documento de la UPA
    upa_document_type = fields.Char('Document UPA Type')
    # Folio de la solicitud
    folio = fields.Char('Folio')
    # Número del cheque
    check_folio = fields.Char('Folio')

    @api.depends('l10n_mx_edi_payment_method_id')
    def _compute_is_check(self):
        check_method_id = self.env.ref('l10n_mx_edi.payment_method_cheque')
        for rec in self:
            rec.is_check = rec.l10n_mx_edi_payment_method_id == check_method_id

    def _get_reference(self, bank_movement, reference):
        index = reference[5:-1].split(":")
        start = int(index[0]) - 1
        end = int(index[1])
        if reference[0:4] == "refb":
            return bank_movement.ref[start:end]
        if reference[0:4] == "refc":
            return bank_movement.customer_ref[start:end]
        elif reference[0:4] == "ref1":
            return bank_movement.reference_plugin_1[start:end]
        elif reference[0:4] == "ref2":
            return bank_movement.reference_plugin_2[start:end]
        elif reference[0:4] == "ref3":
            return bank_movement.reference_plugin_3[start:end]

    def recognize_payment(self, bank_movement):
        # Obtiene el criterio de busqueda que conicide con el movimiento bancario
        query = """
            select id from expense_recognition where bank_account_id = %s
                and (desc_movement is null or upper(%s) like upper(desc_movement))
                and (reference_client is null or upper(%s) like upper(reference_client))
                and (reference_plugin_1 is null or upper(%s) like upper(reference_plugin_1))
                and (reference_plugin_2 is null or upper(%s) like upper(reference_plugin_2))
                and (reference_plugin_3 is null or upper(%s) like upper(reference_plugin_3))
        """
        refs = (
            bank_movement.customer_ref or '',
            bank_movement.reference_plugin_1 or '',
            bank_movement.reference_plugin_2 or '',
            bank_movement.reference_plugin_3 or '',
        )
        params = (
            bank_movement.income_bank_account.id,
            bank_movement.description_of_the_movement or '',
        ) + refs
        self.env.cr.execute(query, params)
        res = [r[0] for r in self.env.cr.fetchall()]
        # Si no encontro ningun criterio, no se puede reconocer el pago
        if not res:
            return False
        for expense_recognition in self.browse(res):
            # Reconocimiento de pagos por transferencia electronica
            if expense_recognition.l10n_mx_edi_payment_method_id == self.env.ref('l10n_mx_edi.payment_method_transferencia'):
                upa_key = self._get_reference(bank_movement, expense_recognition.upa_key)
                document_type = self._get_reference(bank_movement, expense_recognition.upa_document_type)
                folio = self._get_reference(bank_movement, expense_recognition.folio)
                folio_recibo = "%s/%s/%s" % (upa_key, document_type, folio)
                # Query para obtener el pago asociado
                query = """
                    select id from account_payment
                    where payment_type = 'outbound'
                    and folio like %s
                    and amount = %s
                """
                self.env.cr.execute(query, (("%" + folio_recibo), bank_movement.charge_amount))
                # Obtiene el resultado de ejecutar el query
                payment_id = self.env.cr.fetchone()
                if not payment_id:
                    return False
                payment_id = self.env[ACCOUNT_PAYMENT].browse(payment_id)
                payment_id.no_validate_payment = True
                return True
            # Reconocimiento de pagos por cheques
            elif expense_recognition.l10n_mx_edi_payment_method_id == self.env.ref('l10n_mx_edi.payment_method_cheque'):
                check_folio = self._get_reference(bank_movement, expense_recognition.check_folio)
                # Query para obtener el pago asociado
                query = """
                    select p.id from account_payment p, check_log c
                    where p.check_folio_id = c.id
                    and c.folio = %s
                    and amount = %s
                    and payment_state = 'for_payment_procedure'
                """
                self.env.cr.execute(query, (int(check_folio), bank_movement.charge_amount))
                # Obtiene el resultado de ejecutar el query
                payment_id = self.env.cr.fetchone()
                if not payment_id:
                    return False
                # payment_id = self.env[ACCOUNT_PAYMENT].browse(payment_id)
                ref = " ".join([r for r in refs if r])
                # Reconocimiento de pagos por cheques
                self.env[ACCOUNT_PAYMENT].payment_post(payment_id, ref)
                return True
        return False