// Oculta Archivar y Desarchivar del dropdown Acción a ciertos grupos
odoo.define('siif_income.BasicView', function (require) {
    "use strict";
    let session = require('web.session');
    let BasicView = require('web.BasicView');

    BasicView.include({
        init: function (viewInfo, params) {
            let self = this;
            this._super(...arguments);
            let model = ['association.distribution.ie.accounts'].includes(self.controllerParams.modelName) ? 'True' : 'False';
            let modelResUsers = ['res.users'].includes(self.controllerParams.modelName) ? 'True' : 'False';

            if (model) {
                session.user_has_group('jt_income.group_income_fac_administrator_user').then(function (has_group) {

                    if (has_group) {
                        self.controllerParams.archiveEnabled = 'False' in viewInfo.fields;
                    }

                });
            }
            // Ocultar desde ingresos al asignar configuracion/seguridad/dependencia/subdependencia a usuarios
            if (modelResUsers) {
                session.user_has_group('jt_income.group_income_admin_user').then(function (has_group) {

                    if (has_group) {
                        self.controllerParams.archiveEnabled = 'False' in viewInfo.fields;
                    }

                });
            }

        },
    });

});