from odoo import models, fields, api, _
from datetime import datetime
from odoo.exceptions import UserError, ValidationError


class CheckJournalCatalog(models.Model):
    _name = 'check.journal.catalog'
    _description = 'Check Journal Catalog'


    account_type = fields.Selection(
        [   ('supplier','Supplier'),
            ('project','Project'),
            ('pension','Pension'),
            ('nominal','Nominal'),
         ], string="Account type",required=True)
    
    journal_type = fields.Selection(
        [   ('protection','Protection'),

         ], string="Journal type",required=True)
    
    journal_id = fields.Many2one('account.journal',string="Journal", required=True)

    @api.constrains('account_type')
    def _check_account_type(self):
        account_types = ['supplier', 'project', 'pension', 'nominal']
        
        for account_type in account_types:
            count = self.env['check.journal.catalog'].search_count([('account_type', '=', account_type)])
            if count > 1:
                type_error_messages = {
                    'supplier': 'You can only have one Supplier account..',
                    'project': 'You can only have one Project account.',
                    'pension': 'You can only have one Pension account.',
                    'nominal': 'You can only have one Nominal account.'
                }
                raise UserError(_(type_error_messages[account_type]))
