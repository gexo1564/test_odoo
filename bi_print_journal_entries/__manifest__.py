# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Print Journal Entries Report in Odoo',
    'version': '13.0.0.0',
    'category': 'Account',
    'summary': 'Allow to print pdf report of Journal Entries.',
    'description': """
    Allow to print pdf report of Journal Entries.
    journal entry
    print journal entry 
    journal entries
    print journal entry reports
    account journal entry reports
    journal reports
    account entry reports""",
    'price': 000,
    'currency': 'EUR',
    'author': 'BrowseInfo',
    'depends': ['base','account'],
    'data': [
            'report/report_journal_entries.xml',
            'report/report_journal_entries_view.xml',
            'report/report_policy_journal.xml',
            'report/report_income_policy_view.xml',
            'report/report_policy_expenses.xml',
            'report/report_others.xml',
            'report/conac_report_journal_entries_view.xml',
            'report/conac_report_income_policy_view.xml',
            'report/conac_report_policy_journal.xml',
            'report/conac_report_policy_expenses.xml',
            'report/conac_report_others.xml'
    ],
    'installable': True,
    'auto_install': True,
    'live_test_url':'https://youtu.be/qehLT4WOWPs',
    "images":["static/description/Banner.png"],
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
