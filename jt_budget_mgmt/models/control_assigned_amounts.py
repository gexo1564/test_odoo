# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import base64
import io
import logging
import math
from datetime import datetime, timedelta
from odoo.exceptions import ValidationError
from odoo import models, fields, api, _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import re

IR_CRON = 'ir.cron'
PROGRAM_CODE = 'program.code'

class ControlAssignedAmounts(models.Model):

    _name = 'control.assigned.amounts'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Control of Assigned Amounts'
    _rec_name = 'folio'

    @api.depends('success_line_ids','success_line_ids.imported','success_line_ids.state','state')
    def _get_count(self):
        for record in self:
            record.record_number = len(record.success_line_ids)
            record.import_record_number = len(
                record.success_line_ids.filtered(lambda l: l.imported == True))

    name = fields.Char(string="Name", tracking=True)
    cron_running = fields.Boolean(string='Running CRON?')
    record_number = fields.Integer(
        string='Number of records', compute='_get_count',store=True)
    import_record_number = fields.Integer(
        string='Number of imported records', readonly=True, compute='_get_count',store=True)

    folio = fields.Char(string='Folio', states={'validated': [('readonly', True)], 'rejected': [
                        ('readonly', True)], 'canceled': [('readonly', True)]}, tracking=True)
    budget_id = fields.Many2one('expenditure.budget', string='Budget', states={'validated': [(
        'readonly', True)], 'rejected': [('readonly', True)], 'canceled': [('readonly', True)]}, tracking=True)
    user_id = fields.Many2one(
        'res.users', string='Made by', default=lambda self: self.env.user, tracking=True)
    import_date = fields.Date(string='Import date', default=fields.Date.context_today, states={'validated': [(
        'readonly', True)], 'rejected': [('readonly', True)], 'canceled': [('readonly', True)]})
    observations = fields.Text(string='Observations', tracking=True)
    state = fields.Selection(
        [
            ('draft', 'Draft'),
            ('process', 'In process'),
            ('validated', 'Validated'),
            ('rejected', 'Rejected'),
            ('canceled', 'Canceled')
        ], default='draft', required=True, string='Status', tracking=True
    )

    # Budget Lines
    line_ids = fields.One2many('control.assigned.amounts.lines',
                               'assigned_amount_id', string='Assigned amount lines', domain=[('state', '!=', 'success')], states={'validated': [('readonly', True)], 'rejected': [('readonly', True)], 'canceled': [('readonly', True)]})
    success_line_ids = fields.One2many(
        'control.assigned.amounts.lines', 'assigned_amount_id',
        string='Assigned amount lines', domain=[('state', '=', 'success')], states={'validated': [('readonly', True)], 'rejected': [('readonly', True)], 'canceled': [('readonly', True)]})

    @api.depends('line_ids','line_ids.state','success_line_ids','success_line_ids.state','state')
    def _compute_total_rows(self):
        for assigned_amount in self:
            assigned_amount.draft_rows = self.env['control.assigned.amounts.lines'].search_count(
                [('assigned_amount_id', '=', assigned_amount.id), ('state', 'in', ['draft', 'manual'])])
            assigned_amount.failed_rows = self.env['control.assigned.amounts.lines'].search_count(
                [('assigned_amount_id', '=', assigned_amount.id), ('state', '=', 'fail')])
            assigned_amount.success_rows = self.env['control.assigned.amounts.lines'].search_count(
                [('assigned_amount_id', '=', assigned_amount.id), ('state', '=', 'success')])
            assigned_amount.total_rows = self.env['control.assigned.amounts.lines'].search_count(
                [('assigned_amount_id', '=', assigned_amount.id)])

    # Rows Progress Tracking Details
    import_status = fields.Selection([
        ('draft', 'Draft'),
        ('in_progress', 'In Progress'),
        ('done', 'Completed')], default='draft', copy=False)
    failed_row_file = fields.Binary(string='Failed Rows File')
    fialed_row_filename = fields.Char(
        string='File name', default=lambda self: _("Failed_Rows.txt"))
    draft_rows = fields.Integer(
        string='Failed Rows', compute="_compute_total_rows",store=True)
    failed_rows = fields.Integer(
        string='Failed Rows', compute="_compute_total_rows",store=True)
    success_rows = fields.Integer(
        string='Success Rows', compute="_compute_total_rows",store=True)
    total_rows = fields.Integer(
        string="Total Rows", compute="_compute_total_rows",store=True)
    assigned_total = fields.Float(
        string="Total Assigned", compute="_compute_amt_total", store=True)

    _sql_constraints = [
        ('folio', 'unique(folio)', 'The folio must be unique.')]

    @api.depends('success_line_ids', 'success_line_ids.assigned')
    def _compute_amt_total(self):
        for seasonality in self:
            assigned_total = 0
            for line in seasonality.success_line_ids:
                assigned_total += line.assigned

            seasonality.assigned_total = assigned_total

    @api.constrains('folio')
    def _check_folio(self):
        if not str(self.folio).isnumeric():
            if self.env.user.lang == 'es_MX':
                raise ValidationError('Folio Debe ser un valor numérico!')
            else:
                raise ValidationError('Folio Must be numeric value!')

    def import_lines(self):
        ctx = self.env.context.copy()
        if self._context.get('reimport'):
            ctx['reimport'] = True
        return {
            'name': _("Import Lines"),
            'type': 'ir.actions.act_window',
            'res_model': 'import.assigned.amount.line',
            'view_mode': 'form',
            'view_type': 'form',
            'views': [(False, 'form')],
            'target': 'new',
            'context': ctx,
        }

    def validate_and_add_budget_line(self, record_id=False, cron_id=False):
        if record_id:
            self = self.env['control.assigned.amounts'].browse(int(record_id))

        if len(self.line_ids.ids) > 0:
            counter = 0
            failed_row = ""
            failed_line_ids = []
            success_line_ids = []
            self.env['expenditure.budget'].check_year_exist(self.line_ids[0])
            # Objects
            program_obj = self.env['program'].search_read([('program_key_id','!=',False)], fields=['id', 'key_unam','program_key_id'])
            subprogram_obj = self.env['sub.program'].search_read([('dependency_id','!=',False),('sub_dependency_id','!=',False),('unam_key_id','!=',False)], fields=['id','dependency_id','sub_dependency_id' ,'unam_key_id', 'sub_program'])
            dependancy_obj = self.env['dependency'].search_read([], fields=['id', 'dependency'])
            subdependancy_obj = self.env['sub.dependency'].search_read([],fields=['id', 'dependency_id', 'sub_dependency'])
            item_obj = self.env['expenditure.item'].search_read([], fields=['id', 'item', 'exercise_type'])
            origin_obj = self.env['resource.origin'].search_read([], fields=['id', 'key_origin'])
            activity_obj = self.env['institutional.activity'].search_read([], fields=['id', 'number'])
            shcp_obj = self.env['budget.program.conversion'].search_read([('shcp_name','!=',False),('program_key_id','!=',False),('conversion_key_id','!=',False)], fields=['id','program_key_id', 'shcp_name','conversion_key_id','federal_part'])
            dpc_obj = self.env['departure.conversion'].search_read([('item_id','!=',False)], fields=['id', 'federal_part','item_id'])
            expense_type_obj = self.env['expense.type'].search_read([], fields=['id', 'key_expenditure_type'])
            location_obj = self.env['geographic.location'].search_read([], fields=['id', 'state_key'])
            wallet_obj = self.env['key.wallet'].search_read([], fields=['id', 'wallet_password'])
            year_obj = self.env['year.configuration'].search_read([], fields=['id', 'name'])

            lines_to_execute = self.line_ids

            cron = False
            if cron_id:
                cron = self.env[IR_CRON].sudo().browse(int(cron_id))
                if cron:
                    lines_to_execute = self.env['control.assigned.amounts.lines'].search([('cron_id', '=', cron.id)])

            for line in lines_to_execute:
                # if counter == 5000:
                #     break
                counter += 1
                line_vals = [line.year, line.program, line.subprogram, line.dependency, line.subdependency, line.item,
                            line.dv, line.origin_resource, line.ai, line.conversion_program,
                            line.departure_conversion, line.expense_type, line.location, line.portfolio,
                            line.project_type, line.project_number, line.stage, line.agreement_type,
                            line.agreement_number, line.exercise_type]


                if line.state != 'success':
                    program_code_str = "".join(line_vals[0:19])
                    program_code = self.env[PROGRAM_CODE].sudo().search(
                        [('program_code', '=', program_code_str)],
                        limit=1
                    )
                    if not program_code:
                        # Validate start date
                        if line.start_date and line.start_date.day == 1 and line.start_date.month in [1, 4, 7, 10]:
                            pass
                        else:
                            failed_row += str(line_vals) + \
                                _("------>> Invalid start date based on quarter rules\n")
                            failed_line_ids.append(line.id)
                            continue

                        # Validate end date
                        if line.end_date:
                            flag = False
                            if line.end_date.month not in [3, 6, 9, 12] or line.end_date.day not in [31, 30]:
                                flag = True
                            if line.end_date.month == 3 and line.end_date.day != 31:
                                flag = True
                            if line.end_date.month == 6 and line.end_date.day != 30:
                                flag = True
                            if line.end_date.month == 9 and line.end_date.day != 30:
                                flag = True
                            if line.end_date.month == 12 and line.end_date.day != 31:
                                flag = True
                            if flag:
                                failed_row += str(line_vals) + \
                                    _("------>> Invalid end date based on quarter rules\n")
                                failed_line_ids.append(line.id)
                                continue

                        # Validate year format
                        if len(str(line.year)) > 3:
                            year_str = str(line.year)[:4]
                            if year_str.isnumeric():
                                year = list(filter(lambda yr: yr['name'] == year_str, year_obj))
                                if year:
                                    year = year[0]['id']
                                else:
                                    if not self._context.get('from_adequacies'):
                                        year = self.env['year.configuration'].create({'name': year_str}).id
                        # year = year_obj.validate_year(line.year)
                        if not year:
                            failed_row += str(line_vals) + \
                                _("------>> Invalid Year Format\n")
                            failed_line_ids.append(line.id)
                            continue

                        # Validate Program(PR)
                        program = False
                        program_key_id = False
                        if len(str(line.program)) > 1:
                            program_str = str(line.program).zfill(2)
                            if program_str.isnumeric():
                                program = list(filter(lambda prog: prog['key_unam'] == program_str, program_obj))
                                program_key_id = program[0]['program_key_id'][0] if program else False
                                program = program[0]['id'] if program else False
                        if not program:
                            failed_row += str(line_vals) + \
                                _("------>> Invalid Program(PR) Format\n")
                            failed_line_ids.append(line.id)
                            continue

                        # Validation Conversion Program SHCP
                        shcp = False
                        #program = False
                        conversion_item = False

                        departure_conversion_str = False
                        if len(str(line.departure_conversion)) > 4:
                            departure_conversion_str = str(line.departure_conversion).zfill(4)

                        program_str = False
                        if len(str(line.program)) > 1:
                            program_str = str(line.program).zfill(2)
                        if len(str(line.conversion_program)) > 3:
                            shcp_str = str(line.conversion_program)
                            if len(shcp_str) == 4 and (re.match("[A-Z]{1}\d{3}", str(shcp_str).upper())):
                                shcp = list(
                                    filter(lambda tmp: tmp['shcp_name'] == shcp_str and tmp['program_key_id'][0] == program_key_id and tmp['federal_part']==departure_conversion_str,
                                        shcp_obj))
                                #program = shcp[0]['unam_key_id'][0] if shcp and shcp[0]['unam_key_id'] else False
                                #conversion_item  = shcp[0]['dep_con_id'][0] if shcp and shcp[0]['dep_con_id'] else False
                                shcp = shcp[0]['id'] if shcp else False

                        if not shcp:
                            failed_row += str(line_vals) + \
                                _("------>> Invalid Conversion Program SHCP(CONPP) Format\n")
                            failed_line_ids.append(line.id)
                            continue



                        # Validate Dependency
                        # dependency = dependancy_obj.validate_dependency(
                        #     line.dependency)
                        dependency = False
                        if len(str(line.dependency)) > 2:
                            dependency_str = str(line.dependency).zfill(3)
                            if dependency_str.isnumeric():
                                dependency = list(filter(lambda dep: dep['dependency'] == dependency_str, dependancy_obj))
                                dependency = dependency[0]['id'] if dependency else False
                        if not dependency:
                            failed_row += str(line_vals) + \
                                _("------>> Invalid Dependency(DEP) Format\n")
                            failed_line_ids.append(line.id)
                            continue

                        # Validate Sub-Dependency
                        # subdependency = subdependancy_obj.validate_subdependency(
                        #     line.subdependency, dependency)
                        subdependency = False
                        subdependency_str = str(line.subdependency).zfill(2)
                        if subdependency_str.isnumeric():
                            subdependency = list(filter(
                                lambda sdo: sdo['sub_dependency'] == subdependency_str and sdo['dependency_id'][
                                    0] == dependency, subdependancy_obj))
                            subdependency = subdependency[0]['id'] if subdependency else False
                        if not subdependency:
                            failed_row += str(line_vals) + \
                                _("------>> Invalid Sub Dependency(DEP) Format\n")
                            failed_line_ids.append(line.id)
                            continue

                        # Validate Sub-Program
                        # subprogram = subprogram_obj.validate_subprogram(
                        #     line.subprogram, program)
                        subprogram = False
                        if len(str(line.subprogram)) > 1:
                            subprogram_str = str(line.subprogram).zfill(2)
                            if subprogram_str.isnumeric():
                                subprogram = list(filter(lambda subp: subp['sub_program'] == subprogram_str and subp['unam_key_id'][0] == program and subp['dependency_id'][0] == dependency and subp['sub_dependency_id'][0] == subdependency, subprogram_obj))
                                subprogram = subprogram[0]['id'] if subprogram else False
                        if not subprogram:
                            failed_row += str(line_vals) + \
                                _("------>> Invalid SubProgram(SP) Format\n")
                            failed_line_ids.append(line.id)
                            continue

                        # Validate Item
                        # item = item_obj.validate_item(
                        #     line.item, line.exercise_type)
                        item = False
                        if len(str(line.item)) > 2:
                            item_string = str(line.item).zfill(3)
                            typee = str(line.exercise_type).lower()
                            if typee not in ['r', 'c', 'd']:
                                typee = 'r'
                            if item_string.isnumeric():
                                item = list(filter(lambda itm: itm['item'] == item_string and itm['exercise_type'] == typee,
                                                item_obj))
                                if not item:
                                    item = list(filter(lambda itm: itm['item'] == item_string, item_obj))
                                if item:
                                    item = item[0]['id']
                        if not item:
                            failed_row += str(line_vals) + \
                                _("------>> Invalid Expense Item(PAR) Format\n")
                            failed_line_ids.append(line.id)
                            continue

                        # Validate Origin Of Resource
                        # origin_resource = origin_obj.validate_origin_resource(
                        #     line.origin_resource)
                        origin_resource = False
                        if len(str(line.origin_resource)) > 0:
                            origin_resource_str = str(line.origin_resource).replace('.', '').zfill(2)
                            if origin_resource_str.isnumeric():
                                origin_resource = list(
                                    filter(lambda ores: ores['key_origin'] == origin_resource_str, origin_obj))
                                origin_resource = origin_resource[0]['id'] if origin_resource else False
                        if not origin_resource:
                            failed_row += str(line_vals) + \
                                _("------>> Invalid Origin Of Resource(OR) Format\n")
                            failed_line_ids.append(line.id)
                            continue

                        # Validation Institutional Activity Number
                        # institutional_activity = activity_obj.validate_institutional_activity(
                        #     line.ai)
                        institutional_activity = False
                        if len(str(line.ai)) > 2:
                            institutional_activity_str = str(line.ai).zfill(5)
                            if institutional_activity_str.isnumeric():
                                institutional_activity = list(
                                    filter(lambda inact: inact['number'] == institutional_activity_str, activity_obj))
                                institutional_activity = institutional_activity[0][
                                    'id'] if institutional_activity else False
                        if not institutional_activity:
                            failed_row += str(line_vals) + \
                                _("------>> Invalid Institutional Activity Number(AI) Format\n")
                            failed_line_ids.append(line.id)
                            continue

                        #Validation Federal Item
                        conversion_item = False
                        if len(str(line.departure_conversion)) > 4:
                            conversion_item_str = str(line.departure_conversion).zfill(4)
                            if conversion_item_str.isnumeric():
                                conversion_item = list(
                                    filter(lambda coit: coit['federal_part'] == conversion_item_str and coit['item_id'][0]==item, dpc_obj))
                                conversion_item = conversion_item[0]['id'] if conversion_item else False
                        if not conversion_item:
                            failed_row += str(line_vals) + \
                                _("------>> Invalid SHCP Games(CONPA) Format\n")
                            failed_line_ids.append(line.id)
                            continue

                        # Validation Expense Type
                        expense_type = False
                        if len(str(line.expense_type)) > 1:
                            expense_type_str = str(line.expense_type).zfill(2)
                            if expense_type_str.isnumeric():
                                expense_type = list(
                                    filter(lambda exty: exty['key_expenditure_type'] == expense_type_str, expense_type_obj))
                                expense_type = expense_type[0]['id'] if expense_type else False
                        if not expense_type:
                            failed_row += str(line_vals) + \
                                _("------>> Invalid Expense Type(TG) Format\n")
                            failed_line_ids.append(line.id)
                            continue

                        # Validation Expense Type
                        geo_location = False
                        if len(str(line.location)) > 1:
                            location_str = str(line.location).zfill(2)
                            if location_str.isnumeric():
                                geo_location = list(filter(lambda geol: geol['state_key'] == location_str, location_obj))
                                geo_location = geo_location[0]['id'] if geo_location else False
                        if not geo_location:
                            failed_row += str(line_vals) + \
                                _("------>> Invalid Geographic Location (UG) Format\n")
                            failed_line_ids.append(line.id)
                            continue

                        # Validation Wallet Key
                        # wallet_key = wallet_obj.validate_wallet_key(line.portfolio)
                        wallet_key = False
                        if len(str(line.portfolio)) > 3:
                            wallet_key_str = str(line.portfolio).zfill(4)
                            if wallet_key_str.isnumeric():
                                wallet_key = list(
                                    filter(lambda wlke: wlke['wallet_password'] == wallet_key_str, wallet_obj))
                                wallet_key = wallet_key[0]['id'] if wallet_key else False
                        if not wallet_key:
                            failed_row += str(line_vals) + \
                                _("------>> Invalid Wallet Key(CC) Format\n")
                            failed_line_ids.append(line.id)
                            continue

                        # Validation Project Type
                        #* Nuevas consultas
                        # Catalogo de tipos de proyectos
                        project_type_custom = self.env['project.type.custom'].sudo().search(
                            [('name', '=', line.project_type)],
                            limit=1
                        )
                        if not project_type_custom:
                            failed_row += str(line_vals) + \
                                        _("------>> Invalid Project Type(TP)\n")
                            failed_line_ids.append(line.id)
                            continue

                        custom_stage = self.env['project.custom.stage'].sudo().search(
                            [('name', '=', line.stage)], limit=1
                        )
                        if not custom_stage:
                            failed_row += str(line_vals) + \
                                        _("------>> Invalid Stage (E)\n")
                            failed_line_ids.append(line.id)
                            continue

                        project = self.env['project.project'].sudo().search(
                            [
                                ('number', '=', line.project_number),
                                ('custom_project_type_id', '=', project_type_custom.id),
                                ('custom_stage_id', '=', custom_stage.id)
                            ],
                            limit=1
                        )
                        if not project:
                            failed_row += str(line_vals) + \
                                        _("------>> Invalid Project\n")
                            failed_line_ids.append(line.id)
                            continue

                        # Tipo de Proyecto
                        project_type = False
                        if len(str(line.project_type)) > 1:
                            project_type = self.env['project.type'].sudo().search(
                                [('project_id', '=', project.id)],
                                limit=1
                            )
                        if not project_type:
                            failed_row += str(line_vals) + \
                                        _("------>> Invalid Project Type(TP)\n")
                            failed_line_ids.append(line.id)
                            continue
                        else:
                            project_type = project_type.id
                        # Etapa
                        stage = None
                        if len(str(line.stage)) > 1:
                            stage = self.env['stage'].sudo().search(
                                [('project_id', '=', project.id)],
                                limit=1
                            )
                        if not stage:
                            failed_row += str(line_vals) + \
                                        _("------>> Stage (E) not found\n")
                            failed_line_ids.append(line.id)
                            continue
                        else:
                            stage = stage.id
                        # Acuerdo
                        agreement_type = None
                        if len(str(line.agreement_type)) > 1:
                            agreement_type = self.env['agreement.type'].sudo().search(
                                [('project_id', '=', project.id)],
                                limit=1
                            )
                        if not agreement_type:
                            failed_row += str(line_vals) + \
                                        _("------>> Invalid Agreement Type(TC) or Agreement Number Format\n")
                            failed_line_ids.append(line.id)
                            continue
                        else:
                            agreement_type = agreement_type.id

                        if not line.dv:
                            failed_row += str(line_vals) + \
                                _("------>> Digito Verificador is not added! \n")
                            failed_line_ids.append(line.id)
                            continue
                        dv_obj = self.env['verifying.digit']
                        vd = dv_obj.check_digit_from_codes(
                            program, subprogram, dependency, subdependency, item
                        )
                        if vd and line.dv:
                            line_dv = line.dv
                            if len(line.dv) == 1:
                                line_dv = '0' + line.dv
                            if vd != line_dv:
                                failed_row += str(line_vals) + \
                                    _("------>> Digito Verificador is not matched! \n")
                                failed_line_ids.append(line.id)
                                continue

                        # En caso de no entrar con ninguna de las anteriores,
                        # significa que el código si es válido, pero no existe
                        failed_row += str(line_vals) + \
                            _("-------> Program Code is not created. \n")
                        failed_line_ids.append(line.id)
                        continue

                    # Validation Importe 1a Asignacion
                    asigned_amount = 0
                    try:
                        asigned_amount = float(line.assigned)
                        if asigned_amount < 0:
                            failed_row += str(line_vals) + \
                                _("------>> Assigned Amount should be greater than or 0!")
                            failed_line_ids.append(line.id)
                            continue
                    except:
                        failed_row += str(line_vals) + \
                            _("------>> Invalid Asigned Amount Format")
                        failed_line_ids.append(line.id)
                        continue

                    # Validation Authorized Amount
                    authorized_amount = 0
                    # try:
                    #     authorized_amount = float(line.authorized)
                    #     if authorized_amount == 0:
                    #         failed_row += str(line_vals) + \
                    #             "------>> Authorized Amount should be greater than 0!"
                    #         failed_line_ids.append(line.id)
                    #         continue
                    # except:
                    #     failed_row += str(line_vals) + \
                    #         "------>> Invalid Authorized Amount Format"
                    #     failed_line_ids.append(line.id)
                    #     continue
                    budget_line = self.env['expenditure.budget.line'].search(
                        [
                            ('expenditure_budget_id', '=', self.budget_id.id),
                            ('program_code_id', '=', program_code.id),
                            ('start_date', '=', line.start_date),
                            ('end_date', '=', line.end_date)
                        ], limit=1
                    )
                    if budget_line:
                        failed_row += str(line_vals) + \
                            _("------>> Program Code Already Linked With Budget Line with same quarter!")
                        failed_line_ids.append(line.id)
                        continue
                    if program_code and not program_code.budget_id:
                        failed_row += str(line_vals) + \
                            _("-------> Either budget is not linked with program code or program code does not exist. \n")
                        failed_line_ids.append(line.id)
                        continue
                    if program_code and program_code.budget_id and program_code.budget_id.id != self.budget_id.id:
                        failed_row += str(line_vals) + \
                            _("-------> Either budget is not linked with program code or program code does not exist. \n")
                        failed_line_ids.append(line.id)
                    line.program_code_id = program_code.id
                    success_line_ids.append(line.id)
                    line.available = line.assigned

            failed_lines = self.env['control.assigned.amounts.lines'].search(
                [('assigned_amount_id', '=', self.id), ('id', 'in', failed_line_ids)])
            success_lines = self.env['control.assigned.amounts.lines'].search(
                [('assigned_amount_id', '=', self.id), ('id', 'in', success_line_ids)])
            success_lines.write({'state': 'success'})
            for l in failed_lines:
                if l.state == 'draft':
                    l.state = 'fail'

            failed_data = False
            vals = {}
            if failed_row != "":
                content = "\n"
                content += _("...................Failed Rows ") + \
                    str(datetime.today()) + "...............\n"
                content += str(failed_row)
                failed_data = base64.b64encode(content.encode('utf-8'))
                vals['failed_row_file'] = failed_data
            else:
                vals['failed_row_file'] = None

            if cron:
                lines_to_execute.write({'cron_id': False})
                model_id = self.env.ref('jt_budget_mgmt.model_control_assigned_amounts').id
                next_cron = self.env[IR_CRON].sudo().search([('prev_cron_id', '=', cron.id), ('active', '=', False),
                                                               ('model_id', '=', model_id)], limit=1)
                if next_cron:
                    nextcall = datetime.now()
                    nextcall = nextcall + timedelta(seconds=10)
                    next_cron.write({'nextcall': nextcall, 'active': True})
                else:
                    self.write({'cron_running': False})
                    if len(self.line_ids.ids) == 0:
                        self.write({'state': 'process'})
                    failed_count = success_count = 0
                    if self.success_line_ids:
                        success_count = len(self.success_line_ids)
                    if self.line_ids:
                        failed_count = len(self.line_ids)
                    self.send_notification_msg(self.user_id, failed_count, success_count)
                    self.user_id.notify_info(message='Control of Assigned Amounts - ' + str(self.folio) +
                    ' Lines Validation process completed. Please verify and correct lines, if any failed!',
                                    title='Control of Assigned Amounts', sticky=True)
                    msg = (_("Control of Assigned Amounts Validation Process Ended at %s") % datetime.strftime(
                        datetime.now(), DEFAULT_SERVER_DATETIME_FORMAT))
                    self.env['mail.message'].create({'model': 'control.assigned.amounts', 'res_id': self.id,
                                                     'body': msg})
            if vals:
                self.write(vals)


    def send_notification_msg(self, user, failed, successed):
        ch_obj = self.env['mail.channel']
        base_user = self.env.ref('base.user_root')
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        url = base_url + '/web#id=%s&view_type=form&model=control.assigned.amounts' % (self.id)
        body = (_("Control of Assigned Amounts Validation Process is Completed for \
                       <a href='%s' target='new'>%s</a>") % (url, self.name))
        body += (_("<ul><li>Total Successed Lines: %s </li>\
               <li>Total Failed Lines: %s </li></ul>") % (str(successed), str(failed)))
        if user:
            ch = ch_obj.sudo().search([('name', '=', str(base_user.name + ', ' + user.name)),
                                       ('channel_type', '=', 'chat')], limit=1)
            if not ch:
                ch = ch_obj.create({
                    'name': 'OdooBot, ' + user.name,
                    'public': 'private',
                    'channel_type': 'chat',
                    'channel_last_seen_partner_ids': [(0, 0, {'partner_id': user.partner_id.id,
                                                              'partner_email': user.partner_id.email}),
                                                      (0, 0, {'partner_id': base_user.partner_id.id,
                                                              'partner_email': base_user.partner_id.email})
                                                      ]
                })
            ch.message_post(attachment_ids=[], body=body, content_subtype='html',
                            message_type='comment', partner_ids=[], subtype='mail.mt_comment',
                            email_from=base_user.partner_id.email, author_id=base_user.partner_id.id)
        return True

    def remove_cron_records(self):
        crons = self.env[IR_CRON].sudo().search([('model_id', '=', self.env.ref('jt_budget_mgmt.model_control_assigned_amounts').id)])
        for cron in crons:
            if cron.control_assigned_id and not cron.control_assigned_id.cron_running:
                try:
                    cron.sudo().unlink()
                except Exception as e:
                    raise ValidationError(e)

    def confirm(self):
        # Total CRON to create

        msg = (_("Control of Assigned Amounts Validation Process Started at %s") % datetime.strftime(
            datetime.now(), DEFAULT_SERVER_DATETIME_FORMAT))
        self.env['mail.message'].create({'model': 'control.assigned.amounts', 'res_id': self.id,
                                         'body': msg})
        program_links = self.line_ids.filtered(lambda x:x.program_code_id)
        for line in program_links:
            line.state = 'success'
            line.available = line.assigned
        #program_links.write({'state': 'success'}) 
        if self.line_ids:
            total_cron = math.ceil(len(self.line_ids.ids) / 5000)
        else:
            total_cron = math.ceil(len(self.success_line_ids.ids) / 5000)

        total_lines = len(self.success_line_ids.filtered(
            lambda l: l.state == 'success'))

        if total_cron != 0:
            if self.success_rows != self.total_rows:
                self.validate_and_add_budget_line()
                total_lines = len(self.success_line_ids.filtered(
                    lambda l: l.state == 'success'))

            if total_lines == self.total_rows:
                self.state = 'process'

    def _month_to_quarter(self, month):
        return [1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4][month-1]

    def validate(self):
        vals_list = []
        # total_assigned_amount = sum(self.success_line_ids.mapped('authorized'))
        # total_budget_amount = sum(self.budget_id.success_line_ids.mapped('authorized'))
        # if total_assigned_amount != total_budget_amount:
        #     raise ValidationError("Authorized amount not matched with budget!")
        for line in self.success_line_ids:
            # Se obtiene la referencia de la linea anual para insertarla en las
            # lineas de los trimestres
            budget_line_yearly_id = self.env['expenditure.budget.line'].sudo().search(
                [
                    ('program_code_id', '=', line.program_code_id.id),
                    ('budget_line_yearly_id', '=', None)
                ],
                limit = 1
            )
            vals = {
                'program_code_id': line.program_code_id.id,
                'start_date': line.start_date,
                'end_date': line.end_date,
                'authorized': line.assigned,
                'assigned': line.assigned,
                # 'available': line.available,
                'budget_line_yearly_id': budget_line_yearly_id.id,
                'imported': line.imported,
                'imported_sessional': True,
                'state': line.state,
                'year': line.year,
                'program': line.program,
                'subprogram': line.subprogram,
                'dependency': line.dependency,
                'subdependency': line.subdependency,
                'item': line.item,
                'dv': line.dv,
                'origin_resource': line.origin_resource,
                'ai': line.ai,
                'conversion_program': line.conversion_program,
                'departure_conversion': line.departure_conversion,
                'expense_type': line.expense_type,
                'location': line.location,
                'portfolio': line.portfolio,
                'project_type': line.project_type,
                'project_number': line.project_number,
                'stage': line.stage,
                'agreement_type': line.agreement_type,
                'agreement_number': line.agreement_number,
                'exercise_type': line.exercise_type,
            }
            vals_list.append((0, 0, vals))
        self.budget_id.write({'success_line_ids': vals_list})
        self.state = 'validated'

    def reject(self):
        self.state = 'rejected'

    def unlink(self):
        for Camount in self:
            if Camount.state not in ('draft', 'process'):
                if self.env.user.lang == 'es_MX':
                    raise ValidationError(
                        'No se puede eliminar una Estacionalidad Trimestral validado!')
                else:
                    raise ValidationError(
                        'You can not delete processed Control of Assigned Amounts!')
        return super(ControlAssignedAmounts, self).unlink()

    def show_success_lines(self):
        action = self.env.ref(
            'jt_budget_mgmt.action_control_assigned_amounts_success_line').read()[0]
        action['limit'] = 1000
        action['domain'] = [('id', 'in', self.success_line_ids.ids)]
        action['search_view_id'] = (self.env.ref(
            'jt_budget_mgmt.control_assigned_amounts_lines_search_view').id,)
        return action

    @api.model
    def fields_get(self, fields=None, attributes=None):
        
        no_selectable_fields = [ 'message_needaction', 'activity_ids', 'activity_exception_decoration', 'failed_row_file', 'message_is_follower', 
                                 'import_status', 'failed_rows', 'draft_rows', 'success_rows', 'message_main_attachment_id', 'message_has_error',
                                 'line_ids', 'success_line_ids', 'activity_date_deadline', 'activity_summary', 'activity_type_id', 'user_id',
                                 'import_record_number', 'record_number', 'cron_running', 'message_follower_ids', 'message_channel_ids', 'message_partner_ids',
                                 'total_rows', 'activity_user_id', 'message_has_error', 'message_has_sms_error', 'success_line_ids', 'import_record_number',
                                 'fialed_row_filename','id', 'assigned_amount_id']
        
        no_sortable_field = [ 'import_status',  'import_date', 'failed_rows', 'draft_rows', 'failed_row_file', 'message_main_attachment_id', 
                                'fialed_row_filename','user_id', 'record_number', 'cron_running', 'total_rows' ]

        res = super(ControlAssignedAmounts, self).fields_get(fields, attributes=attributes)
        
        for key, value in res.items():
            if key in no_selectable_fields:
                value.update({'selectable': False})
        
        for key, value in res.items():
            if key in no_sortable_field:
                value.update({'sortable': False})
                
        return res   


class ControlAssignedAmountsLines(models.Model):

    _name = 'control.assigned.amounts.lines'
    _description = 'Control Assigned Amounts Lines'
    _rec_name = 'program_code_id'

    program_code_id = fields.Many2one(PROGRAM_CODE, string='Code program')

    start_date = fields.Date(string='Start date')
    end_date = fields.Date(string='End date')

    # authorized = fields.Integer(string='Authorized')
    assigned = fields.Float(string='Assigned')
    available = fields.Float(string='Available')
    currency_id = fields.Many2one(
        'res.currency', default=lambda self: self.env.company.currency_id)

    assigned_amount_id = fields.Many2one(
        'control.assigned.amounts', string='Assigned amount')
    budget_id = fields.Many2one('expenditure.budget', string='Budget')

    imported = fields.Boolean()
    state = fields.Selection([('manual', 'Manual'), ('draft', 'Draft'), (
        'fail', 'Fail'), ('success', 'Success')], string='Status', default='manual')

    # Fields for imported data
    year = fields.Char(string='Year')
    program = fields.Char(string='Program')
    subprogram = fields.Char(string='Sub-Program')
    dependency = fields.Char(string='Dependency')
    subdependency = fields.Char(string='Sub-Dependency')
    item = fields.Char(string='Expense Item')
    dv = fields.Char(string='Digit Varification')
    origin_resource = fields.Char(string='Origin Resource')
    ai = fields.Char(string='Institutional Activity')
    conversion_program = fields.Char(string='Conversion Program')
    departure_conversion = fields.Char(string='Federal Item')
    expense_type = fields.Char(string='Expense Type')
    location = fields.Char(string='State Code')
    portfolio = fields.Char(string='Key portfolio')
    project_type = fields.Char(string='Type of Project')
    project_number = fields.Char(string='Project Number')
    stage = fields.Char(string='Stage Identifier')
    agreement_type = fields.Char(string='Type of Agreement')
    agreement_number = fields.Char(string='Agreement number')
    exercise_type = fields.Char(string='Exercise type')
    cron_id = fields.Many2one(IR_CRON, string="CRON ID")
    is_create_from_adequacies = fields.Boolean(string="Line Create From Adequacies",default=False,copy=False)
    new_adequacies_id = fields.Many2one('adequacies',copy=False)
    
    @api.onchange('program_code_id')
    def onchange_program_code_id(self):
        if self.program_code_id:
            code = self.program_code_id
            self.year = code.year and code.year.name or ''
            self.program = code.program_id and code.program_id.key_unam or ''
            self.subprogram = code.sub_program_id and code.sub_program_id.sub_program or ''
            self.dependency = code.dependency_id and code.dependency_id.dependency or ''
            self.subdependency = code.sub_dependency_id and code.sub_dependency_id.sub_dependency or ''
            self.item = code.item_id and code.item_id.item or ''
            self.dv = code.check_digit
            self.ai = code.institutional_activity_id and code.institutional_activity_id.number or ''
            self.origin_resource = code.resource_origin_id and code.resource_origin_id.key_origin or ''
            self.conversion_program = code.budget_program_conversion_id and code.budget_program_conversion_id.shcp.name or ''
            self.departure_conversion = code.conversion_item_id and code.conversion_item_id.federal_part or ''
            self.expense_type = code.expense_type_id and code.expense_type_id.key_expenditure_type or ''
            self.location = code.location_id and code.location_id.state_key or ''
            self.portfolio = code.portfolio_id and code.portfolio_id.wallet_password or ''
            self.project_type = code.project_type_id and code.project_type_id.project_type_identifier or ''
            self.project_number = code.project_number
            if code.stage_id and code.stage_id.stage_identifier:
                self.stage = code.stage_id.stage_identifier
            if code.agreement_type_id and code.agreement_type_id.agreement_type:
                self.agreement_type = code.agreement_type_id.agreement_type
            self.agreement_number = code.number_agreement

    _sql_constraints = [
        ('unique_assigned_amount_id_program_code', 'unique(program_code_id,assigned_amount_id)', 'The program code must be unique per Control of Assigned Amounts')]

    @api.model
    def fields_get(self, fields=None, attributes=None):
        

        no_selectable_fields = [ 'cron_id', 'location','imported', 'program_code_id', 'new_adequacies_id',
                                'is_create_from_adequacies' ]
        
        no_sortable_field = [ 'cron_id', 'location',  'program_code_id', 'is_create_from_adequacies', 'new_adequacies_id',
                             'imported' ]

        res = super(ControlAssignedAmountsLines, self).fields_get(fields, attributes=attributes)
        
        for key, value in res.items():
            if key in no_selectable_fields:
                value.update({'selectable': False})
        
        for key, value in res.items():
            if key in no_sortable_field:
                value.update({'sortable': False})
                
        return res   
    
    
    
    @api.onchange('assigned')
    def onchange_assigned(self):
        if self.assigned:
            self.available = self.assigned

    def write(self, vals):
        if vals.get('assigned'):
            for line in self:
                line.available = vals.get('assigned')
        return super(ControlAssignedAmountsLines, self).write(vals)

    @api.constrains('start_date')
    def _check_start_date(self):
        for rec in self:
            if rec.start_date and rec.assigned_amount_id and rec.assigned_amount_id.budget_id:
                if rec.assigned_amount_id.budget_id.from_date:
                    if rec.assigned_amount_id.budget_id.from_date.year != rec.start_date.year:
                        raise ValidationError('Quarterly Seasonality and budget year should not different!')

    @api.constrains('end_date')
    def _check_end_date(self):
        for rec in self:
            if rec.end_date and rec.assigned_amount_id and rec.assigned_amount_id.budget_id:
                if rec.assigned_amount_id.budget_id.to_date:
                    if rec.assigned_amount_id.budget_id.to_date.year != rec.end_date.year:
                        raise ValidationError('Quarterly Seasonality and budget year should not different!')

    def update_control_line(self, available, program_code_id, quarter):
        # Se realiza la actualización del disponible para el trismestre dado
        query = """
            update control_assigned_amounts_lines set available = %s
            where program_code_id = %s and extract(quarter from start_date) = %s;
        """
        self._cr.execute(query, (available, program_code_id, quarter))

    def insert_sql(self, lines):
        def _get_column_stored_name():
            query = """
                select column_name from information_schema.columns
                where table_schema = 'public'
                and table_name   = 'control_assigned_amounts_lines' and column_name <> 'id'
            """
            self.env.cr.execute(query, ())
            return [f'"{res[0]}"' for res in self.env.cr.fetchall()]

        def _get_insert_sql():
            columns = _get_column_stored_name()
            column = ", ".join(columns)
            values = ", ".join(["%s"] * len(columns))
            return f"insert into control_assigned_amounts_lines ({column}) values({values}) returning id", columns


        def _get_insert_params(columns, vals):
            def update_vals(updates):
                for k, v in updates:
                    if k not in vals: vals.update({k: v})

            res = []
            # Pone los valores por defecto a los campos default o calculados de odoo
            updates = [
                ('assigned', 0.0),
                ('available', 0.0),
                ('currency_id', self.env.company.currency_id.id),
                ('imported', False),
                ('create_uid', self.env.user.id),
                ('write_uid', self.env.user.id),
                ('create_date', 'now()'),
                ('write_date', 'now()'),
            ]
            update_vals(updates)
            for column in columns:
                res.append(vals.get(column[1:-1], None))
            return tuple(res)

        query, columns = _get_insert_sql()
        # Creación de las lineas trimestrales
        params = []
        ids = []
        # Fechas de inicio y fin de los trimestres
        months = [
            ('%s-01-01', '%s-03-31'),
            ('%s-04-01', '%s-06-30'),
            ('%s-07-01', '%s-09-30'),
            ('%s-10-01', '%s-12-31'),
        ]
        for line in lines:
            year = line.get('year', datetime.today().year)
            for start_date, end_date in months:
                line.update({
                    'start_date': datetime.strptime(start_date % year, "%Y-%m-%d"),
                    'end_date': datetime.strptime(end_date % year, "%Y-%m-%d"),
                })
                params.append(_get_insert_params(columns, line))
        for p in params:
            self.env.cr.execute(query, p)
            res = (self.env.cr.fetchone() or [None])[0]
            if not res:
                raise ValidationError("Error al insertar el registro control.assigned.amounts.lines")
            ids.append(res)
        return ids