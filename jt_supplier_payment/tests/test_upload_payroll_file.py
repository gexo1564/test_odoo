from odoo.tests import TransactionCase
from odoo.tests import tagged
from unittest.mock import patch, MagicMock
from psycopg2 import sql

@tagged('jt_supplier_payment')
class GeneratePayrollWizard(TransactionCase):
    def test_payroll_all_payment_request_from_draft_to_reviewed_query1(self):
        seq_ids = self.env['ir.sequence'].search([('code', '=', 'seq.payroll.employee.reference'), ('company_id', 'in', [self.env.company.id, False])], order='company_id')
        
        seq_name = "ir_sequence_%03d" % seq_ids[0].id
        query = sql.SQL("""SELECT last_value from {}""").format(
            sql.Identifier(seq_name)
        )

        self.env.cr.execute(query)
        
        resultados = self.env.cr.fetchall()
        self.assertEqual(len(resultados), 1)
        print(F"PRUEBA DE QUERY SANITIZADO 1 payroll_all_payment_request_from_draft_to_reviewed\nRESULTADOS:{len(resultados)}")
        print(F"Query {query} ")

    def test_payroll_all_payment_request_from_draft_to_reviewed_query2(self):
        seq_name = 'ir_sequence_001'
        current_int = '10'

        current_query_last_value = sql.SQL("""SELECT setval('{}', {});""").format(
            sql.Identifier(seq_name),
            sql.Identifier(current_int)
        )
        # Simular la ejecución de la consulta
        with patch.object(self.env.cr, 'execute') as mock_execute:
            # Ejecutar la prueba
            self.env.cr.execute(current_query_last_value)
            # Asegurar que la consulta se haya ejecutado al menos una vez
            self.assertTrue(mock_execute.called, "La consulta current_query_last_value no se ejecutó.")
            print(F"PRUEBA DE QUERY SANITIZADO 2 payroll_all_payment_request_from_draft_to_reviewed\n{current_query_last_value}")
            print(F"Query {current_query_last_value} ")

    def test_payroll_all_payment_request_from_draft_to_reviewed_query3(self):
        value_temp = MagicMock(return_value=(('REF001',), 'revised', 123))
        # Intento de inyección
        test_inyeccion = 'SELECT id FROM productive_account where id = 1"'
        value_temp += test_inyeccion
           
        current_query = """
            UPDATE employee_payroll_file SET reference = tmp_data.reference, state = tmp_data.state 
            FROM (VALUES %(value_temp)s ) AS tmp_data(reference, state, id_element) WHERE id = tmp_data.id_element;
        """
        params = {'value_temp': value_temp,}
        # Simular la ejecución de la consulta
        with patch.object(self.env.cr, 'execute') as mock_execute:
            # Ejecutar la prueba
            self.env.cr.execute(current_query, params)
            # Asegurar que la consulta se haya ejecutado al menos una vez
            self.assertTrue(mock_execute.called, "La consulta current_query no se ejecutó.")
            print(F"PRUEBA DE QUERY SANITIZADO 3 payroll_all_payment_request_from_draft_to_reviewed\n{current_query}")   
                                
