# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
from datetime import datetime
import time
import json
import logging

# Define Constant
MAIL_THREAD = 'mail.thread'
MAIL_ACTIVITY_MIXIN = 'mail.activity.mixin'
GROSS_AMOUNT = 'Monto Bruto'
AMOUNT_COMMISSION = 'Monto Comisión'
PAYPAL_REFERENCE = 'Referencia PayPal'
CONSUMER_ID = 'Id Consumidor'
SIIF_INCOME_ACCOUNT_PAYPAL_DEPENDENCY = 'siif.income.account.paypal.dependency'
PAYPAL_ACCOUNT_ID = 'Id Cuenta PayPal'
KEY_DEPENDENCY = 'Clave Dependencia'
RELATED_KEY_DEPENDENCY = 'id_cta_paypal.cve_dep'
SUB_DEPENDECY = 'sub.dependency'
KEY_SUB_DEPENDENCY = 'Clave Subdependencia'
RELATED_KEY_SUB_DEPENDENCY = 'id_cta_paypal.cve_sdep'
CFDI_FOLIO = 'Folio CFDI'

class SiifMenuPaypal(models.Model):
    _name = "siif.income.paypal"
    _description = "Menú PayPal"
    
class SiifMovementsPaypal(models.Model):
    _name = "siif.income.movements.paypal"
    _description = "Movimientos PayPal"
    _rec_name = "id_tran_paypal"
    _inherit = [MAIL_THREAD, MAIL_ACTIVITY_MIXIN]

    id_movto = fields.Integer(string = "Id Movimiento")
    codigo_paypal = fields.Many2one("siif.income.codes.paypal", string = "Código PayPal")
    fecha_movto = fields.Char(string = "Fecha del Movimiento", tracking=True) # , default=datetime.today()
    divisa = fields.Char(string = "Divisa")
    monto_bruto = fields.Float(string = GROSS_AMOUNT)
    crdr = fields.Char(string = "CRDR")
    monto_comision = fields.Float(string = AMOUNT_COMMISSION)
    crdr_comision = fields.Char(string = "CRDR Comisión")
    monto_comision_otro = fields.Float(string = "Otro Monto Comisión")
    id_tran_paypal = fields.Char(string = "Id Transacción PayPal")
    ref_paypal = fields.Char(string = PAYPAL_REFERENCE)
    tporef_paypal = fields.Char(string = "Tipo de Referencia PayPal")
    desc_paypal = fields.Char(string = "Descuento PayPal")
    ref_consumidor = fields.Char(string = "Referencia Consumidor")
    nombre_articulo = fields.Char(string = "Nombre del Artículo")
    id_articulo = fields.Char(string = "Id Artículo")
    id_consumidor = fields.Char(string = CONSUMER_ID)
    nombre = fields.Char(string = "Nombre")
    apellidos = fields.Char(string = "Apellidos")
    empresa = fields.Char(string = "Empresa")
    id_cta_paypal = fields.Many2one(SIIF_INCOME_ACCOUNT_PAYPAL_DEPENDENCY, string = PAYPAL_ACCOUNT_ID)
    cve_dep = fields.Many2one("dependency", string = KEY_DEPENDENCY, related = RELATED_KEY_DEPENDENCY)
    cve_sdep = fields.Many2one(SUB_DEPENDECY, string = KEY_SUB_DEPENDENCY, related = RELATED_KEY_SUB_DEPENDENCY)
    fecha_act = fields.Char(string = "Fecha Act", tracking=True) # , default=datetime.today()
    estatus_paypal = fields.Char(string = "Estatus PayPal")
    id_carga = fields.Integer(string = "Id Carga")
    folio_boveda = fields.Char(string = "Folio Boveda")
    folio_cfdi = fields.Char(string = CFDI_FOLIO)
    serie_cfdi = fields.Char(string = "Serie CFDI")
    


class SiifPaypalSales(models.Model):
    _name = "siif.income.paypal.sales"
    _description = "Ventas PayPal"
    _inherit = [MAIL_THREAD, MAIL_ACTIVITY_MIXIN]

    id_movto = fields.Integer(string = "Id Movimiento")
    codigo_paypal = fields.Many2one("siif.income.codes.paypal", string = "Código PayPal")
    fecha_movto = fields.Char(string = "Fecha del Movimiento", tracking=True) # , default=datetime.today()
    divisa = fields.Char(string = "Divisa")
    monto_bruto = fields.Float(string = GROSS_AMOUNT)
    crdr = fields.Char(string = "CRDR")
    monto_comision = fields.Float(string = AMOUNT_COMMISSION)
    crdr_comision = fields.Char(string = "CRDR Comisión")
    monto_comision_otro = fields.Float(string = "Otro Monto Comisión")
    id_tran_paypal = fields.Many2one("siif.income.movements.paypal", string = "Id Transacción PayPal")
    ref_paypal = fields.Char(string = PAYPAL_REFERENCE)
    tporef_paypal = fields.Char(string = "Tipo de Referencia PayPal")
    desc_paypal = fields.Char(string = "Descuento PayPal")
    ref_consumidor = fields.Char(string = "Referencia Consumidor")
    nombre_articulo = fields.Char(string = "Nombre del Artículo")
    id_articulo = fields.Char(string = "Id Artículo")
    id_consumidor = fields.Char(string = CONSUMER_ID)
    nombre = fields.Char(string = "Nombre")
    apellidos = fields.Char(string = "Apellidos")
    empresa = fields.Char(string = "Empresa")
    id_cta_paypal = fields.Many2one(SIIF_INCOME_ACCOUNT_PAYPAL_DEPENDENCY, string = PAYPAL_ACCOUNT_ID)
    cve_dep = fields.Many2one("dependency", string = KEY_DEPENDENCY, related = RELATED_KEY_DEPENDENCY)
    cve_sdep = fields.Many2one(SUB_DEPENDECY, string = KEY_SUB_DEPENDENCY, related = RELATED_KEY_SUB_DEPENDENCY)
    fecha_act = fields.Char(string = "Fecha Act", tracking=True) # , default=datetime.today()
    estatus_paypal = fields.Char(string = "Estatus PayPal")
    id_carga = fields.Integer(string = "Id Carga")
    folio_boveda = fields.Char(string = "Folio Boveda")
    folio_cfdi = fields.Char(string = CFDI_FOLIO)
    serie_cfdi = fields.Char(string = "Serie CFDI")


class SiifCfdiRequest(models.Model):
    _name = "siif.income.cfdi.request"
    _description = "Solicitud de CFDI"

    _inherit = [MAIL_THREAD, MAIL_ACTIVITY_MIXIN]


    cve_request = fields.Char(string = "Clave Solicitud")
    cve_dep = fields.Many2one("dependency", string = KEY_DEPENDENCY)
    cve_sdep = fields.Many2one(SUB_DEPENDECY, string = KEY_SUB_DEPENDENCY)
    cfdi_folio = fields.Many2one("account.move", string = "CFDI Folio")
    partner_id = fields.Char(string = "Cliente")
    fecha_regis = fields.Datetime(string = "Fecha y hora de Registro", default=datetime.today(), tracking=True)
    invoice_uuid = fields.Char(string = "UUID")
    amount_total_sales = fields.Float(string = "Importe")
    amount_rest = fields.Float(string = "Importe Restante")
    cfdi_status = fields.Char(string = "Estado del CFDI")
    currency_sales = fields.Selection([("national currency", "Moneda Nacional"),
                                       ("foreign currency", "Moneda Extranjera"),
                                      ], string = "Moneda Ventas")
    amount_total_cfdi = fields.Float(string = "Importe")
    cfdi_serie = fields.Char(string = "CFDI Serie")
    folio_cfdi = fields.Char(string = CFDI_FOLIO)
    currency_id = fields.Char(string = "Moneda")
    currency_type = fields.Char(string = "Tipo de Moneda")
    description = fields.Char(string = "Descripción")

    line_ids_sales = fields.One2many('siif.income.sales.lines', 'my_id', 'Lines')
    sequence_tree = fields.Integer(string = 'Sequence', store = False)

class SiifSalesLines(models.Model):
    _name = "siif.income.sales.lines"
    _description = "Ventas"

    my_id = fields.Many2one('siif.income.cfdi.request', 'Lineas Model')

    display_type = fields.Selection([('line_section', "Section"), ('line_note', "Note")], default=False, help = "Technical field for UX purpose.", store = False)
    sequence = fields.Integer(string = 'Sequence', store = False)

    sale_id = fields.Many2one("siif.income.paypal.sales" , string = "ID Venta")
    id_tran_paypal = fields.Char(string = PAYPAL_REFERENCE)
    id_cta_paypal = fields.Many2one(SIIF_INCOME_ACCOUNT_PAYPAL_DEPENDENCY, string = PAYPAL_ACCOUNT_ID)
    cve_dep = fields.Many2one("dependency", string = KEY_DEPENDENCY, related = RELATED_KEY_DEPENDENCY)
    cve_sdep = fields.Many2one(SUB_DEPENDECY, string = KEY_SUB_DEPENDENCY, related = RELATED_KEY_SUB_DEPENDENCY)
    fecha_regis = fields.Datetime(string = "Fecha y hora de Registro", default=datetime.today(), tracking=True)
    divisa = fields.Char(string = "Divisa")
    id_consumidor = fields.Char(string = CONSUMER_ID)
    nombre = fields.Char(string = "Nombre")
    apellidos = fields.Char(string = "Apellidos")
    empresa = fields.Char(string = "Empresa")
    monto_bruto = fields.Float(string = GROSS_AMOUNT)
    monto_comision = fields.Float(string = AMOUNT_COMMISSION)
    monto_neto = fields.Float(string = "Monto Neto")
    status = fields.Char(string = "Estado")
