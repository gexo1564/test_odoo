import base64, io
import csv
from lxml import html
import xml.etree.ElementTree as ET
import pandas as pd
from ..layouts import Layout, ResultLine

class LayoutHSBC(Layout):

    def load_file(self, file_data):
        self.file = io.StringIO(file_data.decode("ISO-8859-1")) #open(file, encoding="ISO-8859-1")

    def load_body(self):
        return self.file.readlines()[17:]

    def close_file(self):
        self.file.close()

    def read_line(self, line):
        line = line.split('\t')
        bank_account = line[1].strip().zfill(10)
        amount = float(line[4].strip())
        beneficiary = line[2]
        status = line[6].strip()
        ok = status == "Processed"
        return ResultLine(ok, None, {'beneficiary': beneficiary, 'bank_account': bank_account, 'amount': amount}, status)

    def get_ids_search_employee_payroll_file(self, payroll_processing_id: int, lines: list):
        payment_method_id = self.env.ref('l10n_mx_edi.payment_method_transferencia').id
        query = """
            select substring(replace(name, 'Ñ', ' '), 1, 35) as name,
                lpad(b.acc_number, 10, '0') account,
                p.id partner_id
            from res_partner_bank b, res_partner p, pension_payment_line l, employee_payroll_file r
            where p.id = b.partner_id
            and l.partner_id = p.id
            and l.payroll_id = r.id
            and r.payroll_processing_id = %s
            and beneficiary_type = 'alimony'
            and l.l10n_mx_edi_payment_method_id = %s
        """
        self.env.cr.execute(query, (payroll_processing_id, payment_method_id))
        result_set = {(r[0], r[1]): r[2:] for r in self.env.cr.fetchall()}
        lines_ok, lines_err = [], []
        for line in lines:
            if line.payment_type == "pension":
                # Busqueda para PENSION
                beneficiary = line.columns.get('beneficiary')
                bank_account = line.columns.get('bank_account')
                res = result_set.get((beneficiary, bank_account), None)
                if res:
                    line.res_id = res[0]
                    lines_ok.append(line)
                else:
                    line.res_id = None
                    line.error_message = "No se encontró un registro para el beneficiario %s con cuenta %s" % (
                        beneficiary,
                        bank_account
                    )
                    lines_err.append(line)
            else:
                line.error_message = "No es pago de pensión."
                lines_err.append(line)
        return lines_ok, lines_err

    def get_ids_search_account_payment(self, batch_folio: str, lines: list):
        # Se obtienen los ids que hacen match con el criterio de búsqueda
        query = """
            select lpad(a.acc_number, 10, '0') bank_account, p.id, p.amount
            from account_payment p, res_partner_bank a
            where p.payment_bank_account_id = a.id
            and batch_folio = %s
        """
        self.env.cr.execute(query, (batch_folio,))
        result_set = {r[0]: r[1:] for r in self.env.cr.fetchall()}
        lines_ok, lines_err = [], []
        for line in lines:
            beneficiary = line.columns.get('beneficiary')
            res = result_set.get(line.columns.get('bank_account'), None)
            if res:
                if res[1] == line.columns.get('amount'):
                    line.res_id = res[0]
                    lines_ok.append(line)
                else:
                    line.res_id = None
                    line.error_message = f"El monto del pago ({line.columns.get('amount'):,.2f}) del beneficiario {beneficiary} no coincide con el registrado ({res[1]:,.2f})."
                    lines_err.append(line)
            else:
                line.res_id = None
                line.error_message = f"No se encontró un registro para el beneficiario {beneficiary}"
                lines_err.append(line)
        return lines_ok, lines_err