from odoo import models, fields, api,_

# CONSTANTS FOR MODELS
ACCOUNT_MOVE_MANUAL_PERMISSION_MODEL = 'account.move.manual.permission'
class AccountMoveLine(models.Model):

    _inherit = 'account.move.line'

    '''
    This is the model that will be inherited by the account.move.line model
    The purpose of this model is to add a new domain to some fields in account.move.line model when account.move.line
    is a manual accounting record.
    ''' 

    # only apply to manual accounting records
    @api.onchange('is_for_manual_accounting_record')
    def _onchange_is_for_manual_accounting_record(self):
        if self.is_for_manual_accounting_record:
            values = self._get_allowed_dependency()
            return values 

     # only apply to manual accounting records
    def _get_allowed_dependency(self):
        user = self.env.user
        user_items = self.env[ACCOUNT_MOVE_MANUAL_PERMISSION_MODEL].search([('res_users_id','=',user.id)])
        dependencies_to_user = user_items.allowed_dependencies
        values = {}       
        values.update({'domain': {'dependency_id': [('id', 'in', dependencies_to_user.ids)]}})
        return values
    
    # only apply to manual accounting records
    @api.onchange('dependency_id')
    def _get_allowed_subdependency(self):
        self.sub_dependency_id = False
        user = self.env.user
        user_items = self.env[ACCOUNT_MOVE_MANUAL_PERMISSION_MODEL].search([('res_users_id','=',user.id)])
        subdependencies_to_user = user_items.allowed_subdependencies
        sub_dep_list = []
        values = {}
        for val in subdependencies_to_user:
            if val.dependency_id.id == self.dependency_id.id:
                sub_dep_list.append(val.id)
        values.update({'domain': {'sub_dependency_id': [('id', 'in', sub_dep_list)]}})
        return values