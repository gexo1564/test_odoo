# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import logging
from datetime import datetime
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

from odoo.addons.siif_security.tools.utils import check_authorization, check_only_allowed_fields

class DepartureConversion(models.Model):

    _name = 'departure.conversion'
    _description = 'Conversion with Departure'
    _rec_name = 'conversion_key_id'

    federal_part = fields.Char(string='FP', size=5)

    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')

    item_id = fields.Many2one('expenditure.item','Item of Expenditure')
    conversion_key_id = fields.Many2one('shcp.game','Conversion Key')
    federal_part_desc = fields.Text(related='conversion_key_id.conversion_key_desc',string='Federal part description')
    
    _sql_constraints = [('federal_part', 'unique(conversion_key_id,item_id,start_date,end_date)', 'The Conversion Key must be unique per Item of Expenditure.')]

    def name_get(self):
        result = []
        for rec in self:
            name = ''
            if rec.item_id and self.env.context and self.env.context.get('show_item_name',False): 
                name += '[' + rec.item_id.item + '] '
            if rec.conversion_key_id:
                name += rec.conversion_key_id.conversion_key
            result.append((rec.id, name))
        return result

    @api.constrains('start_date', 'end_date')
    def check_end_date(self):
        if not self.start_date:
            raise ValidationError(_("The start date of validity is required."))
        if self.end_date and self.end_date <= self.start_date:
            raise ValidationError(_("The effective end date cannot be less than or equal to the effective date."))

#     @api.constrains('federal_part')
#     def _check_federal_part(self):
#         if not str(self.federal_part).isnumeric():
#             raise ValidationError(_('The Institutional activity number must be numeric value'))

    def fill_zero(self, code):
        return str(code).zfill(5)
    
    
    @api.model
    def fields_get(self, fields=None, attributes=None):
        
        no_selectable_fields = [ 'federal_part_desc' ]
        
        res = super(DepartureConversion, self).fields_get(fields, attributes=attributes)
        
        for key, value in res.items():
            if key in no_selectable_fields:
                value.update({'selectable': False})
    
                
        return res
    
    

    @api.model
    def create(self, vals):
        if vals.get('federal_part') and len(vals.get('federal_part')) != 5:
            vals['federal_part'] = self.fill_zero(vals.get('federal_part'))
        rec = super(DepartureConversion, self).create(vals)
        if rec.conversion_key_id:
            rec.sudo().federal_part = rec.conversion_key_id.conversion_key
        return rec

    @check_authorization(
        'jt_budget_mgmt.group_budget_catalogs_admin_user',
        'jt_budget_mgmt.group_budget_admin_user'
    )
    @check_only_allowed_fields('start_date', 'end_date')
    def write(self, vals):
        if vals.get('federal_part') and len(vals.get('federal_part')) != 5:
            vals['federal_part'] = self.fill_zero(vals.get('federal_part'))
        return super(DepartureConversion, self).write(vals)

    @check_authorization(
        'jt_budget_mgmt.group_budget_catalogs_admin_user',
        'jt_budget_mgmt.group_budget_admin_user'
    )
    def unlink(self):
        for conversion in self:
            program_code = self.env['program.code'].search([('conversion_item_id', '=', conversion.id)], limit=1)
            if program_code:
                raise ValidationError('No puedes eliminar un CONPA que este ligado a un Código Programático!')
        return super(DepartureConversion, self).unlink()

    def validate_conversion_item(self, conversion_item_string,item):
        if len(str(conversion_item_string)) > 4:
            conversion_item_str = str(conversion_item_string).zfill(4)
            if conversion_item_str.isnumeric():
                conversion_item = self.search(
                    [('federal_part', '=', conversion_item_str),('item_id','=',item)], limit=1)
                if conversion_item:
                    return conversion_item
        return False

    def get_conpa(self, item, date=datetime.now()):
        query = f"""
        select federal_part conpa from departure_conversion dc, expenditure_item ei
        where dc.item_id = ei.id and ei.item = '{item}'
        and dc.start_date <= '{date}' and (dc.end_date is null or dc.end_date >= '{date}')
        """
        self._cr.execute(query, ())
        res = self.env.cr.dictfetchall()
        return res
    
    # if not return value is not valid #
    def get_validity_conpa(self, conpa, today = datetime.now()):
        query = """
                SELECT id
                FROM departure_conversion
                WHERE 
                    (
                        (start_date IS NULL AND end_date IS NULL AND federal_part = %(conpa)s) OR
                        (start_date IS NOT NULL AND end_date IS NULL AND federal_part = %(conpa)s AND start_date <= %(today)s) OR
                        (start_date IS NOT NULL AND end_date IS NOT NULL AND federal_part = %(conpa)s AND start_date <= %(today)s AND end_date >= %(today)s)
                    )
                """
        params = {'conpa' : str(conpa),
                  'today': today}
        self.env.cr.execute(query, params)
        conpa_validity = self.env.cr.fetchall()

        return conpa_validity