import logging
from odoo.tests import common
from odoo.exceptions import UserError, ValidationError
from psycopg2.errors import UniqueViolation
CURRENCY_FUND='currency.funds'
class TestCurrencyFunds(common.TransactionCase):
    def setUp(self):
        
        super().setUp()
        res_currency=self.env['res.currency']
        self.account_account = self.env['account.account'].search([('name','=','FF EFECTIVO EN DÓLARES')])
        self.journal = self.env['account.journal'].search([('name','=','BBVA BANCOMER-8141 (BBV01)')])
        self.currency_prueba_01 = res_currency.search([('name','=','AED')])
        self.currency_prueba_02 = res_currency.search([('name','=','GBP')])
        self.currency_prueba_03 = res_currency.search([('name','=','JPY')])
        self.currency_prueba_04 = res_currency.search([('name','=','CHF')])
        self.currency_prueba_06 = res_currency.search([('name','=','AMD')])
        self.currency_mx        = res_currency.search([('name','=','MXN')])
        self.user               = self.env['res.partner'].search([('password_beneficiary','=','N1420924')])
        self.dep                = self.env['dependency'].search([('dependency','=','211')])
        self.sub_dep            = self.env['sub.dependency'].search([('dependency_id','=',self.dep.dependency),('sub_dependency','=','01')])
    
    #Revisar si se crea una solicitud de compra inicial
    def test_create(self):
        prueba = self.env[CURRENCY_FUND].create({
            'associated_account':self.account_account.id,
            'journal_id':self.journal.id,
            'currency_id':self.currency_prueba_01.id,
            'fund_limit':8000.00,
            'currency_reorder_point':1000.00})

        #Se revisa si se asigna el nombre con la moneda asignada
        self.assertEqual(prueba.name,'Fondo de Divisa (AED)')

        #Se revisa si se crea una solicitud de compra    
        request = self.env['application.request'].search([('currency_fund_id','=',prueba.id)])
        self.assertEqual(request.name,'Solicitud Inicial Fondo de Divisa (AED)')
        self.assertEqual(request.amount_funds,8000)
    
    #Prueba para probar la moneda única
    def test_check_unique_currency(self):
        valores_prueba_01={
            'associated_account':self.account_account.id,
            'journal_id':self.journal.id,
            'currency_id':self.currency_prueba_01.id,
            'fund_limit':500000.00,
            'currency_reorder_point':1000.00,
        }
        valores_prueba_02={
            'associated_account':self.account_account.id,
            'journal_id':self.journal.id,
            'currency_id':self.currency_prueba_01.id,
            'fund_limit':400000.00,
            'currency_reorder_point':1000.00,
        }
        """
        Escenario 1: Asignar un Fondo de divisas con una misma moneda
        """
        self.env[CURRENCY_FUND].create(valores_prueba_01)
        try:
            self.env[CURRENCY_FUND].create(valores_prueba_02)
        except UniqueViolation:
            print("No se puede generar un Fondo Fijo con una moneda repetida.")
        
        
        
    #Prueba para revisar los montos asignados
    def test_check_funds(self):
        """
        Escenario 1: Monto límite en 0
        """
        with self.assertRaises(ValidationError):
            prueba = self.env[CURRENCY_FUND].create({
                'associated_account':self.account_account.id,
                'journal_id':self.journal.id,
                'currency_id':self.currency_prueba_01.id,
                'fund_limit':0.00,
                'currency_reorder_point':1000.00,
            })

            #prueba.unlink()
        """
        Escenario 2: Monto límite en negativo
        """
        with self.assertRaises(ValidationError):
            prueba = self.env[CURRENCY_FUND].create({
                'associated_account':self.account_account.id,
                'journal_id':self.journal.id,
                'currency_id':self.currency_prueba_02.id,
                'fund_limit':-6000.00,
                'currency_reorder_point':1000.00,
            })

            #prueba.unlink()
        """
        Escenario 3: Punto de reorden en 0
        """
        with self.assertRaises(ValidationError):
            prueba = self.env[CURRENCY_FUND].create({
                'associated_account':self.account_account.id,
                'journal_id':self.journal.id,
                'currency_id':self.currency_prueba_03.id,
                'fund_limit':1000000.00,
                'currency_reorder_point':0.00,
            })
        
            #prueba.unlink()
        
        """
        Escenario 4: Punto de reorden en negativos
        """
        with self.assertRaises(ValidationError):
            prueba = self.env[CURRENCY_FUND].create({
                'associated_account':self.account_account.id,
                'journal_id':self.journal.id,
                'currency_id':self.currency_prueba_04.id,
                'fund_limit':1000000.00,
                'currency_reorder_point':-80.00,
            })
        
            #prueba.unlink()
        """
        Escenario 6: Punto de reorden 0 y Monto limite en negativo
        """
        with self.assertRaises(ValidationError):
            prueba = self.env[CURRENCY_FUND].create({
                'associated_account':self.account_account.id,
                'journal_id':self.journal.id,
                'currency_id':self.currency_prueba_06.id,
                'fund_limit':-5000.00,
                'currency_reorder_point':0.00,
            })


    #Revisar si el fondo tiene suficiencia
    def test_check_balance(self):
        valores_prueba_01={
            'associated_account':self.account_account.id,
            'journal_id':self.journal.id,
            'currency_id':self.currency_prueba_01.id,
            'fund_limit':500000.00,
            'actual_fund':20000.00,
            'currency_reorder_point':1000.00,
        }
        """Fondo de divisas con 2000 de fondo actual"""
        prueba_01 = self.env[CURRENCY_FUND].create(valores_prueba_01)

        #Verdadero si tiene fondos suficientes
        self.assertEqual(self.env[CURRENCY_FUND].check_balance(prueba_01.currency_id.id,1000.00),True)

        #Falso si no tiene fondos
        self.assertEqual(self.env[CURRENCY_FUND].check_balance(prueba_01.currency_id.id,10000000.00),False)

    #Revisar si se crea una solicitud de compra
    def test_create_application_request(self):
        valores_prueba_01={
            'associated_account':self.account_account.id,
            'journal_id':self.journal.id,
            'currency_id':self.currency_prueba_01.id,
            'fund_limit':500000.00,
            'actual_fund':10000.00,
            'currency_reorder_point':1000.00,
        }
        """Fondo de divisas con 2000 de fondo actual"""
        prueba_01 = self.env[CURRENCY_FUND].create(valores_prueba_01)

        #Si es que una solicitud de pago pide dinero que no se puede solventar

        aux = self.env[CURRENCY_FUND].create_application_request(prueba_01.currency_id.id,30000,345)

        self.assertEqual(aux.amount_funds,20000)
        #Se revisa que se haya creado el registro del dinero comprometido
        comprometido = self.env['money.commited'].search([('folio','=',345)])
        self.assertEqual(comprometido.money,10000)


    #Probar si disminuye el fondo y si se crea una solicitud de compra.
    def test_sustract_amount_new_app_request(self):
        prueba = self.env[CURRENCY_FUND].create({
            'associated_account':self.account_account.id,
            'journal_id':self.journal.id,
            'currency_id':self.currency_prueba_01.id,
            'fund_limit':5000.00,
            'actual_fund':5000.00,
            'currency_reorder_point':1000.00})

        lineas = self.env['account.move'].create({
            'partner_id':self.user.id,
            'baneficiary_key':self.user.password_beneficiary,
            'rfc':self.user.vat,
            'dependancy_id':self.dep.id,
            'sub_dependancy_id':self.sub_dep.id,
            'is_payment_request':True,
        })
        list_line=[]
        list_line.append(lineas.id)
        #Caso en donde no se crea un registro 
        self.env[CURRENCY_FUND].sustract_actual_fund(self.currency_prueba_01.id,450,list_line,False)
        self.assertEqual(prueba.actual_fund,4550)
    
        #Revisar si la creación de la solicitud de compra cuando pasa el punto de reorden
        self.env[CURRENCY_FUND].sustract_actual_fund(self.currency_prueba_01.id,4000,list_line,False)
        self.assertEqual(prueba.actual_fund,550)
        
        request = self.env['application.request'].search([('name','=','Solicitud (AED) No. 000001')])
        self.assertEqual(request.amount_funds,4450)
