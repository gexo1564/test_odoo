# -*- coding: utf-8 -*-
import json

from odoo import api, models, fields, _
from odoo.exceptions import UserError, ValidationError

ACCOUNT_MOVE = 'account.move'

class PayrollPaymentBatchWizard(models.TransientModel):

    _name = 'payroll.payment.batch.wizard'

    move_id = fields.Many2one(ACCOUNT_MOVE, 'Payroll Payment Request')

    payroll_processing_id = fields.Many2one('custom.payroll.processing','Payroll Processing')

    payment_schedule_date = fields.Date('Payment Schedule Date', default=fields.Date.today())

    journal_id = fields.Many2one('account.journal', 'Account Bank')

    payment_type = fields.Selection(
        [
            ('salary_and_benefits', 'Salary and Benefits'),
            ('alimony', 'Alimony')
        ], default='salary_and_benefits', string='Payment Type'
    )

    res_bank_ids = fields.Many2many('res.bank', string='Res Banks')
    domain_banks = fields.Char(compute="_compute_domain_banks", store=False)

    @api.depends('payment_type')
    def _compute_domain_banks(self):
        for record in self:
            bank_ids = self.env['payroll.payment.batch.bank'].get_unpaid_banks_ids(
                record.payment_type,
                record.payroll_processing_id
            )
            record.domain_banks = json.dumps([('id', 'in', bank_ids)])
            print(record.domain_banks)

    bbva = fields.Boolean('BBVA')
    show_bbva = fields.Boolean()

    santander = fields.Boolean('Santander')
    show_santader = fields.Boolean()

    hsbc = fields.Boolean('HSBC')
    show_hsbc = fields.Boolean()

    banamex = fields.Boolean('Banamex')
    show_banamex = fields.Boolean()

    inbursa = fields.Boolean('Inbursa')
    show_inbursa = fields.Boolean()

    scotiabank = fields.Boolean('Scotiabank')
    show_scotiabank = fields.Boolean()

    banorte = fields.Boolean('Banorte')
    show_banorte = fields.Boolean()

    @api.onchange('payment_type')
    def onchange_payment_type(self):
        # Restart values
        self.res_bank_ids = None

    def generate_payroll_batch(self):
        payroll_payment_batch = self.env['payroll.payment.batch.bank']

        # Get selected banks
        banks = [b.name for b in self.res_bank_ids]
        if len(banks) == 0:
            raise UserError("You need to choose a bank.")
        # Get alloed banks
        banks_allowed = payroll_payment_batch.get_unpaid_banks(
            self.payment_type,
            self.payroll_processing_id
        )
        # Validates that only batches from the allowed banks are created
        if set(banks) - set(banks_allowed):
            raise ValidationError(_('Only one batch can be generated for each bank that receives the payment.'))

        payment_method_id = self.env.ref('l10n_mx_edi.payment_method_transferencia')
        create_payroll_payment = self.env[ACCOUNT_MOVE].create_payroll_payment
        for bank in banks:
            if self.payment_type == 'salary_and_benefits':
                create_payroll_payment(payment_method_id, self.journal_id, self.move_id, 'payroll_payment', bank, self.payment_schedule_date)
                create_payroll_payment(payment_method_id, self.journal_id, self.move_id, 'additional_benefits', bank, self.payment_schedule_date)
            else:
                create_payroll_payment(payment_method_id, self.journal_id, self.move_id, 'alimony', bank, self.payment_schedule_date)

        # Update Payment Generation
        payroll_payment_batch.update_payment_generation(
            self.payroll_processing_id,
            self.payment_type,
            banks
        )

        return {'type': 'ir.actions.act_window_close'}

    def open_wizard(self, move_id):
        move_id = self.env[ACCOUNT_MOVE].browse(move_id)
        if not move_id:
            return {}
        return {
            'name': _('Payroll Payment Batch Wizard'),
            'res_model': 'payroll.payment.batch.wizard',
            'view_mode': 'form',
            'view_id': self.env.ref('siif_payroll_payment.payroll_payment_batch_wizard').id,
            'context': {
                'default_move_id': move_id.id,
                'default_payroll_processing_id': move_id.payroll_processing_id.id,
            },
            'target': 'new',
            'type': 'ir.actions.act_window',
            'views': [(False, 'form')],
        }