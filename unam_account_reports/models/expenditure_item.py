from odoo import fields, models


class ExpenditureItem(models.Model):
    _name = 'expenditure.item'
    _inherit = ['expenditure.item', 'excel.detailed.information']

    def _compute_excel_display_name(self):
        for record in self:
            record.excel_display_name = '%s - %s' % (record.item, record.description)
