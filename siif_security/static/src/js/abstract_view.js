odoo.define('siif.AbstractView', function (require) {
  "use strict"

  let AbstractView = require("web.AbstractView");

  AbstractView.include({
    init: function (viewInfo, params) {
      this._super.apply(this, arguments)
      this.controllerParams.activeActions = {
        ...this.controllerParams.activeActions,
        export: this.arch.attrs.export ? !!JSON.parse(this.arch.attrs.export) : true,
      }
    },
  })
})