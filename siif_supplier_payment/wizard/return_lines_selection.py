from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
import logging


class SelectReturnLines(models.TransientModel):
    
    _name = 'select.return.lines'
    _description = 'Wizard for create and select return lines'
    
    account_returns = fields.Many2one('account.returns')
    return_lines = fields.Many2many('account.returns.line','account_return_account_return_lines_rel','account_returns','id',string="Account Return Lines")
    amount_return = fields.Float(string="Monto a devolver", compute="get_amount")
    

    @api.depends('return_lines')
    def get_amount(self):
        for rec in self:
            for line in rec.return_lines:
                rec.amount_return+=line.price_return

    
    def _check_return_lines(self):
        count_selected_lines=0
        for line in self.return_lines:
            if line.selected_line:
                count_selected_lines+=1

        if count_selected_lines==0:
            raise UserError("No se tienen lineas seleccionadas para hacer la devolución, Revisar lineas.")
        
        if self.amount_return==0:
            raise UserError('El monto de la devolución es igual a 0, Revisar lineas seleccionadas')
        

    def assign_return_line_selection(self):
        self._check_return_lines()
        lines=[]
        if self.amount_return>=0:
            req = self.env['account.returns'].search([('id','=',self.env.context['id_return'])])

            for line in self.return_lines:
                line.write({'account_returns_line_ids':req.id})
            if self.env.context['edition']==False:
                req.write({
                    'name':self.env['account.returns']._gen_name_return(req.account_move_id),
                    'acc_return_lines':self.return_lines,
                    'amount_return':self.amount_return,
                    'has_return_lines':True,
                })
            else:
                req.write({
                    'acc_return_lines':self.return_lines,
                    'amount_return':self.amount_return,
                    'has_return_lines':True,
                })





    