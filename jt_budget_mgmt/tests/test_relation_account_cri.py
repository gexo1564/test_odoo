# -*- coding: utf-8 -*-
import logging
import unittest
from odoo.exceptions import UserError, ValidationError
from datetime import datetime
from odoo.tests import common, tagged
_logger = logging.getLogger(__name__)

@tagged('-at_install','post_install','relation_account_cri','jt_budget_mgmt')
class RelationAccountCriTest(common.TransactionCase):

    # Prepara entorno de pruebas
    def setUp(self):
        super(RelationAccountCriTest,self).setUp()
        self.model = self.env['relation.account.cri']
        self.cri = self.env['classifier.income.item']
        self.cuentas_id = self.env['account.account']

    def test_existing_cri(self):
        cri_name = "910000"
        cri_id = self.cri.browse([cri_name])
        cri = self.cri.search([('name', '=', cri_id.id)])

        res = self.model.default_get([])

        self.assertEqual(res['cri_id'], cri.name.id)
        print("----------[test_existing_cri]-----[CRI Existente]-----[OK]----------")
    
    # Test a los constraint del catalogo de relacion de cuenta contable con clasificador por rubro de ingresos
    def test_non_existing_cri(self):
        cri_name = "999999"
        cri_id = self.cri.browse([cri_name])
        cri = self.cri.search([('name', '=', cri_id.id)])

        res = self.model.default_get([])

        self.assertEqual(res['cri_id'], cri.name.id)
        print("----------[test_non_existing_cri]-----[CRI No Existente]-----[OK]----------")

    def test_existing_cuenta(self):
        cuenta_name = "410.001.001.001"
        cuenta_id = self.cuentas_id.browse([cuenta_name])
        cuenta = self.cuentas_id.search([('name', '=', cuenta_id.id)])

        res = self.model.default_get([])

        self.assertEqual(res['cuenta_id'], cuenta.name.id)
        print("----------[test_existing_cuenta]-----[Cuenta Existente]-----[OK]----------")

    def test_non_existing_cuenta(self):
        cuenta_name = "999.999.999.999"
        cuenta_id = self.cuentas_id.browse([cuenta_name])
        cuenta = self.cuentas_id.search([('name', '=', cuenta_id.id)])

        res = self.model.default_get([])

        self.assertEqual(res['cuenta_id'], cuenta.name.id)
        print("----------[test_non_existing_cuenta]-----[Cuenta No Existente]-----[OK]----------")