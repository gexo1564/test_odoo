from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
import logging
from datetime import date
import datetime
from lxml import etree
import json
import re

"""Modelos"""
MONEY_FUND='money.funds'
DEPENDENCY='dependency'
SUB_DEP='sub.dependency'
ACC_JOURNAL='account.journal'
ACC_ACCOUNT='account.account'
MONEY_FUND_INC='money.funds.request.increment'
MONEY_FUND_MIN='money.funds.request.ministration'
MONEY_FUND_RET='money.funds.request.returns'
IR_SEQ='ir.sequence'
IR_WINDOW='ir.actions.act_window'

"""MENSAJES"""
INE_DOC="INE"
INE_NAME='INE Doc Name'
ADD_DOC="Address Document"
ADD_NAME='Address Doc Name'
NOMBR_DOC="Nombramiento"
NOMBR_NAME='Nombramiento Doc Name'

"""Info"""
VIEW_MODES='tree,form'

class MoneyFunds(models.Model):
	_name = "money.funds"
	_description = 'Money Funds'
	_inherit = ['mail.thread', 'mail.activity.mixin']

	name = fields.Char(string='Name')
	def get_domain_dependency(self):
		dep = self.env.user.dependency_for_money_fund_ids.ids
		dep += [d.id for d in self.env.user.sub_dependency_for_money_fund_ids.dependency_id]
		return json.dumps([('id', 'in', dep)])
	
	dependency_id = fields.Many2one(DEPENDENCY,string="Dependency",required=True,domain=get_domain_dependency)

	sub_dependency_id = fields.Many2one(SUB_DEP, string="Subdependency",required=True)
	domain_sub_dependency = fields.Char(compute="_compute_domain_sub_dependency", store=False)

	is_new_fund = fields.Boolean('Is new Fund?',default=True)
	
	bank_id	= fields.Many2one('res.bank',string="Bank", compute='_get_bank')
	account_journal = fields.Many2one(ACC_JOURNAL,string="Account Journal")
	bank_account_journal = fields.Many2one(ACC_JOURNAL,string="Bank Account Journal ",domain="[('type', '=', 'bank')]",required=False)
	name_bank_account = fields.Char(string="Name of Bank Account")
	debtor_account = fields.Many2one(ACC_ACCOUNT,string="Associated Account", related='bank_account_journal.default_debit_account_id')
	res_partner_id =fields.Many2one('res.partner',string="Res partner",required=True)
	currency_id = fields.Many2one('res.currency',string='Currency',required=True,default=lambda self: self.env.company.currency_id)
	new_fund = fields.Monetary("Inicial Fund Limit",currency_field='currency_id',default=0)
	import_fund = fields.Monetary("Fund Limit",currency_field='currency_id',default=0,readonly=True)

	support_doc = fields.Binary(string="Support Documentation")
	support_doc_name = fields.Char(string="Support Documentation")

	#Firmantes


	#Titulares
	titulars_id = fields.One2many('titulars','money_funds_id',string="Titulars")
	no_titulars = fields.Integer(string='No of Titulars', default=0)

	dependency_mail = fields.Char(string="Dependency Mail",required=True)
	dependency_address = fields.Char(string="Dependency Address",required=True)

	request_inc_ids = fields.One2many(MONEY_FUND_INC,'money_fund',string="Money Fund Requests Increment")
	request_min_ids = fields.One2many(MONEY_FUND_MIN,'money_fund',string="Money Fund Requests Ministration")
	request_dev_ids = fields.One2many(MONEY_FUND_RET,'money_fund',string="Money Fund Requests Return")

	state = fields.Selection(selection=[
			('request','Request'),
			('on_use', 'On Use'),
			('cancelled', 'Cancelled'),
		], string='State', default='request')

	min_seq = fields.Many2one(IR_SEQ, string='MIN Sequence', required=True, copy=False)
	inc_seq = fields.Many2one(IR_SEQ, string='INC Sequence', required=True, copy=False)
	dev_seq = fields.Many2one(IR_SEQ, string='DEV Sequence', required=True, copy=False)

	request_account = fields.Many2one('request.accounts','Solicitud de apertura de cuenta asociada')

	#Firmantes
	name_proxy = fields.Char(string="Name Proxy",required=True)
	email_proxy = fields.Char(string="Email",required=True)
	phone_proxy = fields.Char(string="Phone Proxy")
	
	ine_doc_proxy = fields.Binary(string=INE_DOC,required=True)
	ine_doc_name_proxy = fields.Char(string=INE_NAME)
	address_doc_proxy = fields.Binary(string=ADD_DOC,required=True)
	address_doc_name_proxy = fields.Char(string=ADD_NAME)
	nombramiento_doc_proxy = fields.Binary(string=NOMBR_DOC,required=True)
	nombramiento_doc_name_proxy = fields.Char(string=NOMBR_NAME)
	

	name_tesorero = fields.Char(string="Name tesorero",required=True)
	email_tesorero = fields.Char(string="Email",required=True)
	phone_tesorero = fields.Char(string="Phone tesorero")
	
	ine_doc_tesorero = fields.Binary(string=INE_DOC,required=True)
	ine_doc_name_tesorero = fields.Char(string=INE_NAME)
	address_doc_tesorero = fields.Binary(string=ADD_DOC,required=True)
	address_doc_name_tesorero = fields.Char(string=ADD_NAME)
	nombramiento_doc_tesorero = fields.Binary(string=NOMBR_DOC,required=True)
	nombramiento_doc_name_tesorero = fields.Char(string=NOMBR_NAME)



	name_owner = fields.Char(string="Name Owner",required=True)
	email_owner = fields.Char(string="Email",required=True)
	phone_owner = fields.Char(string="Phone Owner")
	
	ine_doc_owner = fields.Binary(string=INE_DOC,required=True)
	ine_doc_name_owner = fields.Char(string=INE_NAME)
	address_doc_owner = fields.Binary(string=ADD_DOC,required=True)
	address_doc_name_owner = fields.Char(string=ADD_NAME)
	nombramiento_doc_owner = fields.Binary(string=NOMBR_DOC,required=True)
	nombramiento_doc_name_owner = fields.Char(string=NOMBR_NAME)


	name_secretary = fields.Char(string="Name Secretary",required=True)
	email_secretary = fields.Char(string="Email",required=True)
	phone_secretary = fields.Char(string="Phone Secretary")
	
	ine_doc_secretary = fields.Binary(string=INE_DOC,required=True)
	ine_doc_name_secretary = fields.Char(string=INE_NAME)
	address_doc_secretary = fields.Binary(string=ADD_DOC,required=True)
	address_doc_name_secretary = fields.Char(string=ADD_NAME)
	nombramiento_doc_secretary = fields.Binary(string=NOMBR_DOC,required=True)
	nombramiento_doc_name_secretary = fields.Char(string=NOMBR_NAME)


	name_third = fields.Char(string="Name Proxy",required=True)
	email_third = fields.Char(string="Email",required=True)
	phone_third = fields.Char(string="Phone Third")
	
	ine_doc_third = fields.Binary(string=INE_DOC,required=True)
	ine_doc_name_third = fields.Char(string=INE_NAME)
	address_doc_third = fields.Binary(string=ADD_DOC,required=True)
	address_doc_name_third = fields.Char(string=ADD_NAME)
	
	flag_firma = fields.Boolean(string="Firma Electrónica",default=False)

	#==============================================================
	#-----------------------Revisar datos
	#==============================================================

		
	#Revisar si se tienen titulares en el fondo fijo
	@api.constrains('no_titulars')
	def _check_titulars_id(self):
		if self.no_titulars<=0:
			raise ValidationError(_('The Fund must need titulars.'))
	

	#Revisión de correo de Secretario
	@api.constrains('dependency_mail')
	def _check_dependency_mail(self):
		regex="[\w\.]+@([\w]+\.)+[\w]{2,}"
		if self.dependency_mail and not re.search(regex,self.dependency_mail):
			raise UserError("Correo invalido de la dependencia. Por favor ingrese un correo con formato válido (mail@mail.com).")

	#Revisar si se tienen titulares en el fondo fijo
	@api.constrains('new_fund')
	def _check_new_fund(self):
		if self.new_fund<=0:
			raise ValidationError('La solicitud necesita agregar el importe inicial mayor a 0.')
		
	@api.onchange('is_new_fund')
	def _onchange_is_new_fund(self):
		if self.is_new_fund:
			self.account_journal=False
			self.bank_account_journal=False
	#==============================================================
	#----------------------Condiciones del cambio
	#==============================================================


	#Revisar número de titulares nuevos
	@api.onchange('titulars_id')
	def _set_no_titulars(self):
		if self.titulars_id:
			self.no_titulars = len(self.titulars_id)
		else:
			self.no_titulars=0
			

	#Obtener dominio de la subdependencia
	@api.depends('dependency_id')
	def _compute_domain_sub_dependency(self):
		for record in self:
			domain = [('dependency_id', '=', record.dependency_id.id)]
			if record.dependency_id.id not in self.env.user.dependency_for_money_fund_ids.ids:
				domain.append(('id', 'in', self.env.user.sub_dependency_for_money_fund_ids.ids))
			record.domain_sub_dependency = json.dumps(domain)
		
			
	#Borrar Subdependencia si se cambia la dependencia
	@api.onchange('dependency_id')
	def _onchange_subdependency(self):
		self.sub_dependency_id=''


	@api.depends('bank_account_journal')
	def _get_bank(self):
		for rec in self:
			rec.bank_id=rec.bank_account_journal.bank_id
		


	#==============================================================
	#----------------------Acciones
	#==============================================================
	#Función de creación
	@api.model
	def create(self, vals):
		vals.update({
			'name':    self._generate_name(vals),
			'inc_seq': self.sudo()._create_inc_sequence(vals).id,
			'min_seq': self.sudo()._create_min_sequence(vals).id,
			'dev_seq': self.sudo()._create_dev_sequence(vals).id,
		})
		res = super(MoneyFunds, self).create(vals)
		logging.critical(str(res))
		self.env[MONEY_FUND]._gen_first_request(res)

		return res
	
	#Eliminar titulares de cada fondo para que no quede información base
	def unlink(self):
		for titular in self.titulars_id:
			titular.unlink()
		return super(MoneyFunds, self).unlink()
	
	#GEnerar nombre
	def _generate_name(self,vals):
		size=len(self.env[MONEY_FUND].search([('dependency_id','=',vals.get('dependency_id')),('sub_dependency_id','=',vals.get('sub_dependency_id'))]))+1
		dp=self.env[DEPENDENCY].search([('id','=',vals.get('dependency_id'))]).dependency
		sd=self.env[SUB_DEP].search([('id','=',vals.get('sub_dependency_id'))]).sub_dependency
		if size<=9:
			name = "Fondo Fijo-"+str(dp)+"-"+str(sd)+"-0"+str(size)
		else:
			name = "Fondo Fijo-"+str(dp)+"-"+str(sd)+"-"+str(size)
		return name

	def _gen_first_request(self,rec):
		vals={
			'dependency_id':rec.dependency_id.id,
			'sub_dependency_id':rec.sub_dependency_id.id,
			'has_funds':True,
			'is_first_request':True,
			'money_fund':rec.id,
			'amount_request':rec.new_fund,
		}
		self.env[MONEY_FUND_INC].create(vals)


	#==============================================================
	#-----------------Creación de Secuencias
	#==============================================================

	#Secuencia para las solicitudes de incremento o asignación
	@api.model
	def _create_inc_sequence(self, vals, refund=False):
		""" Create new no_gap entry sequence for every new Journal"""

		dep_number = self.env[DEPENDENCY].search([('id','=',vals['dependency_id'])]).dependency
		sdp_number = self.env[SUB_DEP].search([('id','=',vals['sub_dependency_id'])]).sub_dependency
		seq = {
			'name': _('%s Sequence ') % "INC Money Fund",
			'implementation': 'no_gap',
			'prefix':  'FF-INC-%(range_year)s-'+str(dep_number)+'-'+str(sdp_number)+'-',
			'padding': 1,
			'number_increment': 1,
			'use_date_range': True,
		}
		if 'company_id' in vals:
			seq['company_id'] = vals['company_id']
		seq = self.env[IR_SEQ].create(seq)
		seq_date_range = seq._get_current_sequence()
		seq_date_range.number_next = vals.get('sequence_number_next', 1)
		return seq
	
	#Secuencia para las solicitudes de ministración
	@api.model
	def _create_min_sequence(self, vals, refund=False):
		""" Create new no_gap entry sequence for every new Journal"""

		dep_number = self.env[DEPENDENCY].search([('id','=',vals['dependency_id'])]).dependency
		sdp_number = self.env[SUB_DEP].search([('id','=',vals['sub_dependency_id'])]).sub_dependency
		seq = {
			'name': _('%s Sequence') % "MIN Money Fund",
			'implementation': 'no_gap',
			'prefix':  'FF-MIN-%(range_year)s-'+str(dep_number)+'-'+str(sdp_number)+'-',
			'padding': 1,
			'number_increment': 1,
			'use_date_range': True,
		}
		if 'company_id' in vals:
			seq['company_id'] = vals['company_id']
		seq = self.env[IR_SEQ].create(seq)
		seq_date_range = seq._get_current_sequence()
		seq_date_range.number_next = vals.get('sequence_number_next', 1)
		return seq
	
	#Secuencia para las solicitudes de devolución
	@api.model
	def _create_dev_sequence(self, vals, refund=False):
		""" Create new no_gap entry sequence for every new Journal"""

		dep_number = self.env[DEPENDENCY].search([('id','=',vals['dependency_id'])]).dependency
		sdp_number = self.env[SUB_DEP].search([('id','=',vals['sub_dependency_id'])]).sub_dependency
		seq = {
			'name': _('%s Sequence') % "Return Money Fund",
			'implementation': 'no_gap',
			'prefix':  'FF-DEV-%(range_year)s-'+str(dep_number)+'-'+str(sdp_number)+'-',
			'padding': 1,
			'number_increment': 1,
			'use_date_range': True,
		}
		if 'company_id' in vals:
			seq['company_id'] = vals['company_id']
		seq = self.env[IR_SEQ].create(seq)
		seq_date_range = seq._get_current_sequence()
		seq_date_range.number_next = vals.get('sequence_number_next', 1)
		return seq
	
	#==============================================================
	#-------------Información de las solicitudes relacionadas
	#==============================================================
	#Boton de la vista de Fondo Fijo para solicitudes de asignación
	def money_req_inc_button(self):
		return {
			'name': 'Solicitudes de Asignaciones',
			'view_type': 'form',
			'view_mode': VIEW_MODES,
			'res_model': MONEY_FUND_INC,
			'domain': [('money_fund', '=', self.id)],
			'type': IR_WINDOW,
			'context':{'move_type_req':'add'}
		}
	#Boton de la vista de Fondo Fijo para solicitudes de ministraciones
	def money_req_min_button(self):
		return {
			'name': 'Solicitudes de Ministraciones',
			'view_type': 'form',
			'view_mode': VIEW_MODES,
			'res_model': MONEY_FUND_MIN,
			'domain': [('money_fund', '=', self.id)],
			'type': IR_WINDOW,
			'context':{'move_type_req':'min'}
		}
	#Boton de la vista de Fondo Fijo para solicitudes de devoluciones
	def money_req_dev_button(self):
		return {
			'name': 'Solicitudes de Devoluciones',
			'view_type': 'form',
			'view_mode': VIEW_MODES,
			'res_model': MONEY_FUND_RET,
			'domain': [('money_fund', '=', self.id)],
			'type': IR_WINDOW,
		}
	
	#Mandar correos
	def _get_emails(self):
		list_emails=[]
		list_emails.append((self.name_proxy,self.email_proxy))
		list_emails.append((self.name_owner,self.email_owner))
		list_emails.append((self.name_secretary,self.email_secretary))
		list_emails.append((self.name_third,self.email_third))

		for mail in self.titulars_id:
			list_emails.append((mail.name,mail.email))


		return list_emails
	
	@api.model
	def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
		res = super().fields_view_get(view_id=view_id,
									  view_type=view_type, toolbar=toolbar, submenu=submenu)
		doc = etree.XML(res['arch'])

		is_admin = self.env.user.has_group('siif_currency_purchase_req.admin_user')
		is_finance_user = self.env.user.has_group('siif_currency_purchase_req.finance_user')
		is_upa_user = self.env.user.has_group('siif_currency_purchase_req.upa_user')
		is_dep_user =self.env.user.has_group('siif_currency_purchase_req.dep_user')


		#Vista para el FONDO FIJO
		#   Gestión del fondo fijo

		if (is_admin or is_finance_user or is_upa_user or is_dep_user):
			for node in doc.xpath("//" + view_type):
				node.set('create', '1')
				node.set('import', '1')
				node.set('edit', '1')
				node.set('delete', '1')
				node.set('export_xlsx', '1')
				node.set('export', '1')
				node.set('duplicate', '1')

				
		else:
			for node in doc.xpath("//" + view_type):
				node.set('create', '0')
				node.set('import', '0')
				node.set('edit', '0')
				node.set('delete', '0')
				node.set('export_xlsx', '0')
				node.set('export', '0')
				node.set('duplicate', '0')

				if 'toolbar' in res and 'print' in res['toolbar']:
						res['toolbar']['print'] = []



		res['arch'] = etree.tostring(doc)

		return res




class MoneyFundsConfiguration(models.Model):
	_name = 'money.funds.conf'
	_description = 'Money Funds Configuration'

	#Configuración base de los montos de fondo fijo
	name = fields.Char(default='Parámetros', store=False)
	currency_id = fields.Many2one('res.currency',string='Currency',required=True,default=lambda self: self.env.company.currency_id)
	max_mount = fields.Monetary("Max mount",currency_field='currency_id',default=0)
	money_range = fields.Monetary("Money range",currency_field='currency_id',default=0)
	check_range_min = fields.Monetary("Check range min",currency_field='currency_id',default=0)
	check_range_max = fields.Monetary("Check range min",currency_field='currency_id',default=0)
	approved_range = fields.Monetary("Approve Range",currency_field='currency_id',default=0)
	
	account_return = fields.Many2one(ACC_ACCOUNT,string="Account Return",required=True)

	account_return_interest = fields.Many2one(ACC_ACCOUNT,string="Account Return Interest",required=True)
	account_return_excedent = fields.Many2one(ACC_ACCOUNT,string="Account Return Excedent",required=True)
	journal_ministration = fields.Many2one(ACC_JOURNAL,string="Journal Ministration", required=True)
	account_ministration = fields.Many2one(ACC_ACCOUNT,string="Account Ministration", required=True)

	@api.model
	def create(self, vals):
		count = self.env['money.funds.conf'].search_count([])
		if count >= 1:
			raise ValidationError("Solo se puede configurar un registro para la configuración de referencias bancarias")
		return super().create(vals)

	#==============================================================
	#--------Revisión de configuraciones
	#==============================================================
	@api.onchange('check_range_min','check_range_max')
	def _check_money_range(self):
		self.money_range = self.check_range_min
		self.approved_range = self.check_range_max 

	@api.onchange('journal_ministration')
	def _assign_acc_ministration(self):
		for rec in self:
			rec.account_ministration=rec.journal_ministration.default_credit_account_id

	#Rango de cheques
	@api.constrains('check_range_min','check_range_max')
	def _constrain_check_range(self):
		if self.check_range_min>=self.check_range_max:
			raise ValidationError("El rango para realizar pago por cheques está incorrecto. Revisar.")

	#Rango de cheques
	@api.constrains('check_range_max','max_mount')
	def _constrain_check_range(self):
		if self.check_range_max>=self.max_mount:
			raise ValidationError("El rango mayor es menor al rango de la Aprobación de la DGF. Revisar.")
		
	def get_param(self, param):
		params = self.search([], limit=1)
		if not params:
			raise ValidationError("Configure los parámetros de la 'Configuración de Fondo Fijo'")
		"""
			Param_validations:
				Diccionario con la información
					(nombre de la variable, mensaje de error si no se encuentra el valor)
		"""
		param_validations = {
			'min': ('check_range_min', "Cantidad mínima de cheques normativos"),
			'max': ('check_range_max', "Cantidad máxima de cheques normativos"),
			'top': ('max_mount', "Cantidad máxima para el fondo"),
			'account_return': ('account_return', "Cuenta de devolución de ejercicios anteriores"),
			'account_return_interest': ('account_return_interest', "Cuenta de devolución de intereses"),
			'account_return_excedent': ('account_return_excedent', "Cuenta de devolución de excedente"),
			'journal_ministration': ('journal_ministration', "Diario para las ministraciones"),
			'account_ministration': ('account_ministration', "Cuenta para las ministraciones"),
		}
		#Se revisa si el parametro se encuentra en el diccionario
		if param in param_validations:
			#Obtiene el nombre de la variable y el mensaje de error
			attr_name, error_msg = param_validations[param]
			#obtiene el valor del parametro
			value = getattr(params, attr_name)
			#Si no encuentra el valor del parametro, mandará el mensaje de error
			if not value:
				raise ValidationError(f"Configure la {error_msg}. Configurelo en 'Configuración de Fondo Fijo'.")
			return value
		#Si el parametro no existe
		else:
			raise ValueError("Parámetro no válido")
		
	@api.model
	def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
		res = super().fields_view_get(view_id=view_id,
									  view_type=view_type, toolbar=toolbar, submenu=submenu)
		doc = etree.XML(res['arch'])

		is_admin = self.env.user.has_group('siif_currency_purchase_req.admin_user')
		
		if (is_admin):
				for node in doc.xpath("//" + view_type):
					node.set('create', '1')
					node.set('import', '1')
					node.set('edit', '1')
					node.set('delete', '1')
					node.set('export_xlsx', '1')
					node.set('export', '1')
					node.set('duplicate', '1')
		else:
			for node in doc.xpath("//" + view_type):
				node.set('create', '0')
				node.set('import', '0')
				node.set('edit', '0')
				node.set('delete', '0')
				node.set('export_xlsx', '0')
				node.set('export', '0')
				node.set('duplicate', '0')

				if 'toolbar' in res and 'print' in res['toolbar']:
					res['toolbar']['print'] = []



		res['arch'] = etree.tostring(doc)

		return res
		
		
	
	
