from odoo import models, fields, api, _
from odoo.exceptions import RedirectWarning, UserError, ValidationError, AccessError
from datetime import datetime, timedelta, date
import time
import logging
logger = logging.getLogger(__name__)


class AccountTopayRejection(models.TransientModel):

  _name = 'account.topay.rejection'
  _description = 'Account Topay Rejection'

  reason_rejection = fields.Many2one(comodel_name='rejection.reason', string='Reason for Rejection', help='Reason for Rejection')
  note = fields.Text(string='Notes')

  #Función para asociar el motivo de rechazo y la descripción del rechazo con la cuenta por pagar actual
  def rejected(self):
    info = self.env['account.topay'].search([('id','=',self.env.context['account_topay_id'])])
    info.sudo().write({'reason_rejection':self.reason_rejection,'reason_rejection_desc': self.note,'state':self.env.context['next_state']})
    name_email=info['secretary_admin']
    email= info['secretary_admin_email']
    reason = self.reason_rejection.name
    desc = self.note
    state = self.env.context['next_state']

    self.email_format(state,name_email,email,reason,desc)

  def email_format(self,state,name,emails,reason,description):
    subject=''
    body=''
    if state=='rejected_by_documentation':
      subject = 'Solicitud de Cuenta por Pagar Rechazada por Documentación'
      body = "<p>Debido a los documentos entregados para la solicitud de la cuenta por pagar, esta fue rechazada.</p>"
    elif state=='rejected':
      subject = 'Solicitud Cuenta por Pagar Rechazada'
      body = "<p>La Solicitud fue rechazada debido al siguiente motivo</p>"
    elif state=='rejected_by_insufficiency':
      subject = 'Solicitud Cuenta por Pagar Rechazada por Insuficiencia Presupuestal'
      body = "<p>La Solicitud fue rechazada debido a la falta de dinero para generar la cuenta por pagar</p>"
    body+='<br><p>Tipo de Rechazo: {reason}</p><p>Descripción del rechazo: </p><p>{desc}</p>'.format(reason=reason, desc=description)
    self.send_rejection_email(emails,name,subject,body)

  #Envio de correo con respecto al rechazo de cuenta por pagar
  def send_rejection_email(self,email_delivery,name_delivery,subject,body):
    self.env.context = dict(self.env.context)
    self.env.context.update({
        'is_email' : True
      })

    base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
    template_obj = self.env['mail.template'].sudo().search([('name','=','mail_template_bac')], limit=1)
    aux_body = template_obj.body_html
    aux_body = aux_body.replace('--subject--',subject)
    aux_body = aux_body.replace('--summary--',body)
    aux_body = aux_body.replace('--base_url--',base_url)
    name_to = ""

    root_email = self.env['res.users'].search([('name','=','Admin correo sidia')])
    if not root_email:
      raise UserError('No se encuentra al usuario de correo admin')

    author_id = self.env['res.partner'].search([('id','=',root_email.partner_id.id)])

    mail_pool = self.env['mail.mail']
    values = {
            'subject' : subject,
            'email_to' : "",
            'email_from' : author_id.email,
            'author_id':author_id.id,
            'reply_to' : 'Favor de no responder este mensaje.',
            'body_html' : "",
            'body' : body,
      }

    if email_delivery:
      aux_mails=email_delivery.split(',')
      for mail in aux_mails:
        values['email_to'] = str(mail)
        if name_delivery:
          name_to = name_delivery
        aux_body = aux_body.replace('--name_to--',name_to)
        values['body_html'] = aux_body
        msg_id = mail_pool.create(values)
        if msg_id:
          mail_pool.sudo().send([msg_id])
          logging.critical("***** Correo electrónico enviado a " + values['email_to'])
        else:
          raise UserError('No se creó el mensaje')
    else:
      raise UserError('No se encontraron correos del secretari@')