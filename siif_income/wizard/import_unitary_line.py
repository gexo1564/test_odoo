# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields, api, _
import base64
from odoo.modules.module import get_resource_path
from xlrd import open_workbook
from odoo.exceptions import UserError, ValidationError
import logging
import math

# Define Constant
SIIF_INCOME_CORTE_CAJA = 'siif.income.corte.caja'

class ImportUnitaryLine(models.TransientModel):

    _name = 'import.unitary.line'
    _description = 'Import Unitary Line'

    domain_clv_dotacion = fields.Char('Domain Clave Dotacion')
    clv_dotacion = fields.Many2one(comodel_name="siif.income.series.dotaciones.lines", string = 'Clave Dotación')
    concepto_dot = fields.Char(string = 'Concepto Dotación')
    record_number = fields.Integer(string='Número de Registros')

    file = fields.Binary(string='Archivo')
    download_file = fields.Binary(string='Descargar Archivo')
    download_filename = fields.Char(string='Download File name')
    total_boletos_comp_dotacion = fields.Integer('Total Boletos Por Comprobar', store=True)


    def import_line_folios(self):
        if not self.file:
            raise UserError(_('Please Upload File.'))

        try:
            data = base64.decodestring(self.file)
            book = open_workbook(file_contents=data or b'')
        except UserError as e:
            raise UserError(e)

        sheet = book.sheet_by_index(0)


        total_rows = self.record_number + 1
        if sheet.nrows != total_rows:
            raise UserError(_('Number of records do not match with file'))

        lines = []
        corte_caja_id = self.env[SIIF_INCOME_CORTE_CAJA].browse(self._context.get('active_id'))
        sum_sold_concepto = 0
        sum_cancelated_concepto = 0
        sum_courtesy_concepto = 0
        sum_reprinted_concepto = 0
        sum_rob_extrav_concepto = 0
        sum_other_concepto = 0
        for concept in corte_caja_id.line_ids:
            sum_sold_concepto += concept.boleto_sold
            sum_cancelated_concepto += concept.boleto_cancelated
            sum_courtesy_concepto += concept.boleto_courtesy
            sum_reprinted_concepto += concept.boleto_reprinted
            sum_rob_extrav_concepto += concept.rob_extrav
            sum_other_concepto += concept.boleto_other


        sum_amount_total_cfdis = 0
        for cfdi in corte_caja_id.line_ids_cfdi:
            sum_amount_total_cfdis += cfdi.importe_cfdi
        
            
        series_ids = self.env['siif.income.series.dependencia.boletaje'].search([('id', '=', corte_caja_id.serie.id)])
        
        sum_sold_unitary = 0
        sum_cancelated_unitary = 0
        sum_courtesy_unitary = 0
        sum_reprinted_unitary = 0
        sum_rob_extrav_unitary = 0
        sum_other_unitary = 0
        sum_amount_total_unitary = 0

        for row in range(1, sheet.nrows):

            clave = self.env['siif.income.series.dotaciones.lines'].search([('my_id', '=', series_ids.id), ('clv_dotacion', '=', sheet.cell_value(row, 0))])
            if not clave:
                raise ValidationError(_("La clave dotación no existe para la serie seleccionada!!!"))

            depend = str(sheet.cell_value(row, 1))
            primer_elemento = depend[0]
            segundo_elemento = depend[1]
            tercer_elemento = depend[2]
            dep = primer_elemento+segundo_elemento+tercer_elemento
            dependencia = self.env[SIIF_INCOME_CORTE_CAJA].search([('dependency_id.dependency', '=', dep)])
            if not dependencia.dependency_id.dependency:
                raise ValidationError(_("En uno o varios registros la Dependencia no coincide, favor de revisar antes de cargar su archivo!!!"))

            cuarto_elemento = depend[3]
            quinto_elemento = depend[4]
            subdep = cuarto_elemento+quinto_elemento

            
            corte = self.env[SIIF_INCOME_CORTE_CAJA].search([('id', '=', corte_caja_id.id)])
            if subdep != corte.sub_dependency_id.sub_dependency:
                raise ValidationError(_("En uno o varios registros la Subdependencia no coincide, favor de revisar antes de cargar su archivo!!!"))

            lines.append((0,0,{
                'my_id': corte_caja_id.id,
                'clv_dotacion': clave.id,
                'dep_subdep': sheet.cell_value(row, 1),
                'id_boleto': sheet.cell_value(row, 2),
                'folio_boleto': sheet.cell_value(row, 3),
                'boleto_sold': sheet.cell_value(row, 4),
                'boleto_cancelated': sheet.cell_value(row, 5),
                'boleto_courtesy': sheet.cell_value(row, 6),
                'boleto_reprinted': sheet.cell_value(row, 7),
                'rob_extrav': sheet.cell_value(row, 8),
                'boleto_other': sheet.cell_value(row, 9),
                'fecha_emision': sheet.cell_value(row, 10),
                'fecha_evento': sheet.cell_value(row, 11),
                'evento': sheet.cell_value(row, 12),
                'precio_unitario': sheet.cell_value(row, 13),
                'cfdi_id': sheet.cell_value(row, 14),
                'concepto_boleto': sheet.cell_value(row, 15),
                'nombre_recinto': sheet.cell_value(row, 16),
                'status_compra': sheet.cell_value(row, 17),
              
            }))

            #VALIDACIÓN DE BOLETOS VENDIDOS CONCEPTOS VS UNITARIOS
            sold_unitary = str(sheet.cell_value(row, 4))
            sum_sold_unitary += int(float(sold_unitary))
            
            #VALIDACIÓN DE BOLETOS CANCELADOS CONCEPTOS VS UNITARIOS
            cancelated_unitary = str(sheet.cell_value(row, 5))
            sum_cancelated_unitary += int(float(cancelated_unitary))

            #VALIDACIÓN DE BOLETOS CORTESIA CONCEPTOS VS UNITARIOS
            courtesy_unitary = str(sheet.cell_value(row, 6))
            sum_courtesy_unitary += int(float(courtesy_unitary))


            #VALIDACIÓN DE BOLETOS REIMPRESOS CONCEPTOS VS UNITARIOS
            reprinted_unitary = str(sheet.cell_value(row, 7))
            sum_reprinted_unitary += int(float(reprinted_unitary))

            #VALIDACIÓN DE BOLETOS ROBADOS Y/O EXTRAVIADOS CONCEPTOS VS UNITARIOS
            rob_extrav_unitary = str(sheet.cell_value(row, 8))
            sum_rob_extrav_unitary += int(float(rob_extrav_unitary))

            #VALIDACIÓN DE OTROS BOLETOS CONCEPTOS VS UNITARIOS
            other_unitary = str(sheet.cell_value(row, 9))
            sum_other_unitary += int(float(other_unitary))

            #VALIDACION DE MONTOS TOTALES DE CFDI'S VS MONTOS TOTALES DE FOLIOS
            amount_unitary = str(sheet.cell_value(row, 13))
            sum_amount_total_unitary += float(amount_unitary)
        
        if sum_sold_unitary != sum_sold_concepto:
            raise ValidationError(_('Los Boletos Vendidos (Unitarios) que desea comprobar son %s y los Boletos Vendidos (Conceptos) son %s por lo tanto no coinciden, favor de verificarlo!!!' % (sum_sold_unitary,sum_sold_concepto)))
   
        if sum_cancelated_unitary != sum_cancelated_concepto:
            raise ValidationError(_('Los Boletos Cancelados (Unitarios) que desea comprobar son %s y los Boletos Cancelados (Conceptos) son %s por lo tanto no coinciden, favor de verificarlo!!!' % (sum_cancelated_unitary,sum_cancelated_concepto)))

        if sum_courtesy_unitary != sum_courtesy_concepto:
            raise ValidationError(_('Los Boletos Cortesías (Unitarios) que desea comprobar son %s y los Boletos Cortesías (Conceptos) son %s por lo tanto no coinciden, favor de verificarlo!!!' % (sum_courtesy_unitary,sum_courtesy_concepto)))

        if sum_reprinted_unitary != sum_reprinted_concepto:
            raise ValidationError(_('Los Boletos Reimpresos (Unitarios) que desea comprobar son %s y los Boletos Reimpresos (Conceptos) son %s por lo tanto no coinciden, favor de verificarlo!!!' % (sum_reprinted_unitary,sum_reprinted_concepto)))

        if sum_rob_extrav_unitary != sum_rob_extrav_concepto:
            raise ValidationError(_('Los Boletos Robados y/o Extraviados (Unitarios) que desea comprobar son %s y los Boletos Robados y/o Extraviados (Conceptos) son %s por lo tanto no coinciden, favor de verificarlo!!!' % (sum_rob_extrav_unitary,sum_rob_extrav_concepto)))

        if sum_other_unitary != sum_other_concepto:
            raise ValidationError(_('Los Otros Tipos de Boletos (Unitarios) que desea comprobar son %s y los Otros Tipos de Boletos (Conceptos) son %s por lo tanto no coinciden, favor de verificarlo!!!' % (sum_other_unitary,sum_other_concepto)))

        if sum_amount_total_unitary != sum_amount_total_cfdis:
            raise ValidationError(_("Los CFDI's que desea comprobar tienen un monto total de $%s y la suma de Precios Unitarios es $%s. Favor de verificar la suma de Precios Unitarios debe ser igual a la de los CFDI's que desea comprobar!!!" % (sum_amount_total_cfdis,sum_amount_total_unitary)))


        corte_caja_id.write({'line_ids_unitary': lines})

