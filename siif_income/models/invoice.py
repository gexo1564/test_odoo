from odoo import models, fields, api,_
from babel.dates import format_datetime, format_date

from odoo.exceptions import RedirectWarning, UserError, ValidationError, AccessError
from odoo.tools import float_is_zero, float_compare, safe_eval, date_utils, email_split, email_escape_char, email_re
from odoo.tools.misc import formatLang, format_date, get_lang

from datetime import date, timedelta, datetime
from itertools import groupby
from itertools import zip_longest
from hashlib import sha256
from json import dumps

import json
import re
import logging

model_move = 'account.move'

class Invoice(models.Model):

    _inherit = model_move

    record_date = fields.Date("Record Date")
    bank_movement_date = fields.Date("Bank Movement Date")
    consignment_date = fields.Date("Consignment Date")
    accounting_record_date = fields.Date("Accounting Record Date")
    policy_date = fields.Date("Policy Date")
    check_issuance_date = fields.Date("Check Issuance Date")
    effective_collection_date = fields.Date("Update date")
    update_date = fields.Date("Efective Collection Date")
    type_of_revenue_collection = fields.Selection(selection_add=[('education_servs', 'Education Services'),
                                                    ('aspirants','Aspirants'),('exam_prep', 'Exam Preparation'),('bank_interest','Bank Interest')])
    comission_id = fields.Many2one('comission.settings', "Comission")
    cfdi_status = fields.Char("Estatus CFDI")
    collection_type = fields.Selection([('education_servs', 'Education Services'),
                                        ('aspirants','Aspirants'),('exam_prep', 'Exam Preparation')])
    is_income = fields.Boolean('Is Income')
    afectation_account_id = fields.Many2one('association.distribution.ie.accounts', "Afectation Account")

    account_id = fields.Many2one('account.account',"Certificate Account")
    reference_plugin_2 = fields.Char("Reference plugin 2")
    reference_plugin_3 = fields.Char("Reference plugin 3")
    id_movto_ban = fields.Char("Key movement bank")
    comission_cfdi = fields.Float("Comission CFDI")
    iva_comission_cfdi = fields.Float("Comission CFDI IVA")
    activity_key_id = fields.Many2one('activity.catalog',"Activity")
    support_documentation_line_ids = fields.One2many('support.documentation','move_id','Support documentation')
    description_certificate = fields.Text(related="operation_key_id.description", string="Certificate description", store=True)
    type_of_invoice = fields.Selection([('invoice', 'Invoice'),
                                        ('cash_closing','Cash closing')])
    certificate_state_system = fields.Char("Certificate state system")
    invoice_date_receipt = fields.Date("Invoice date receipt")
    invoice_date_bank = fields.Date("Invoice date bank")
    invoice_date_conciliation = fields.Date("Invoice concilication date")
    me_amount = fields.Float("ME Amount")
    special_certificates_line_ids = fields.One2many('special.certificates','certificate_id','Special certificates')
    refund_move_line_ids = fields.One2many(
        'account.move.line', 'refund_line_ids', string="Journal Items")
    invoice_situation = fields.Many2one('invoice.situation', "Invoice situation")
    total_note = fields.Monetary("Total note")

    credit_note_line_ids = fields.One2many(
        'credit.note.line', 'credit_note_id', string="Credit note")
    

    '''@api.onchange('l10n_mx_edi_payment_method_id','type_of_revenue_collection','income_bank_journal_id')
    def set_comission_id(self):
        if self.type_of_revenue_collection and self.income_bank_journal_id and self.l10n_mx_edi_payment_method_id:
            comission = self.env['comission.settings'].search([('type_of_revenue_collection','=',self.type_of_revenue_collection),
                                ('income_bank_journal_id','=',self.income_bank_journal_id.id),
                                ('l10n_mx_edi_payment_method_id','=',self.l10n_mx_edi_payment_method_id.id)])
            if comission:
                self.comission_id = comission.id
            else:
                self.comission_id = False'''

    @api.constrains('cfdi_serie', 'folio_cfdi', 'type')
    def _check_invoice(self):
        for rec in self:
            if rec.cfdi_serie and rec.folio_cfdi and rec.type=='out_invoice':
                invoice_id = self.env[model_move].search([('cfdi_serie','=',rec.cfdi_serie),('folio_cfdi','=',rec.folio_cfdi), ('type','=','out_invoice'), ('id','!=',rec.id)],limit=1)
                if invoice_id:
                    raise ValidationError(_("There is already a invoice with the same serie and folio")) 

    @api.onchange('type_of_revenue_collection')
    def set_date_no_bill(self):
        if self.type_of_revenue_collection and self.type_of_revenue_collection != 'billing':
            if self.settlement_date:
                self.date = self.settlement_date
                self.invoice_date = self.settlement_date
        elif self.type_of_revenue_collection:
            self.date = self.invoice_date

    @api.onchange('state')
    def set_date_posted(self):
        if self.type_of_revenue_collection and self.type_of_revenue_collection != 'billing' and self.state:
            if self.settlement_date:
                self.date = self.settlement_date
                self.invoice_date = self.settlement_date
        elif self.type_of_revenue_collection and self.state:
            self.date = self.invoice_date
        self.with_context(check_move_validity=False)._recompute_dynamic_lines(recompute_all_taxes=True, recompute_tax_base_amount=True)


    @api.onchange('collection_type')
    def set_type_of_revenue(self):
        self.type_of_revenue_collection = self.collection_type

    @api.onchange('line_ids')
    def set_dep_subdep(self):
        for line in self.line_ids:
            if line.account_id.dep_subdep_flag and self.type_of_revenue_collection:
                line.dependency_id = line.move_id.dependancy_id.id
                line.sub_dependency_id = line.move_id.sub_dependancy_id.id


    def create_income_invoice_line_ids(self,lines):
        invoice_line = []
        for line in lines:
            if line.move_id.type_of_revenue_collection != 'deposit_cer': 
                id_cta_cargo = line.move_id.journal_id.default_debit_account_id.id

                line_vals = {
                                'product_id':line.product_id and line.product_id.id or False,
                                'name' : line.name,
                                'account_id' : id_cta_cargo,
                                'price_unit' : line.price_unit,   
                                'exclude_from_invoice_tab' : False,
                                'quantity' : line.quantity,
                                'income_line_id' : line.id,
                                'account_ie_id' : line.account_ie_id,
                                'discount' : line.discount,
                                'currency_id' : line.currency_id and line.currency_id.id or False,
                                'partner_id' : line.partner_id and line.partner_id.id or False,
                                'product_uom_id' : line.product_uom_id and line.product_uom_id.id or False,
                                'tax_ids' : line.tax_ids and line.tax_ids.ids or False,
                                'unidentified_product' : line.unidentified_product,
                                'income_sub_account' : line.income_sub_account,
                                'income_sub_subaccount' : line.income_sub_subaccount,
                                'ddi_office_accounting' : line.ddi_office_accounting,
                                'amount_of_check' : line.amount_of_check,
                                'deposit_for_check_recovery' : line.deposit_for_check_recovery,
                                'cfdi_20' : line.cfdi_20,
                                'program_code_id' : line.program_code_id and line.program_code_id.id or False,
                                'fixed_discount' : line.fixed_discount,
                                'dependency_id':line.move_id.dependancy_id.id if line.account_id.dep_subdep_flag else False,
                                'sub_dependency_id':line.move_id.sub_dependancy_id.id if line.account_id.dep_subdep_flag else False,
                            } 
                invoice_line.append((0,0,line_vals))
                                        
            else:
                line_vals = {
                                'product_id':line.product_id and line.product_id.id or False,
                                'name' : line.name,
                                'account_id' : line.account_id and line.account_id.id or False,
                                'price_unit' : line.price_unit,   
                                'exclude_from_invoice_tab' : False,
                                'quantity' : line.quantity,
                                'income_line_id' : line.id,
                                'account_ie_id' : line.account_ie_id and line.account_ie_id.id or False,
                                'discount' : line.discount,
                                'currency_id' : line.currency_id and line.currency_id.id or False,
                                'partner_id' : line.partner_id and line.partner_id.id or False,
                                'product_uom_id' : line.product_uom_id and line.product_uom_id.id or False,
                                'tax_ids' : line.tax_ids and line.tax_ids.ids or False,
                                'unidentified_product' : line.unidentified_product,
                                'income_sub_account' : line.income_sub_account,
                                'income_sub_subaccount' : line.income_sub_subaccount,
                                'ddi_office_accounting' : line.ddi_office_accounting,
                                'amount_of_check' : line.amount_of_check,
                                'deposit_for_check_recovery' : line.deposit_for_check_recovery,
                                'cfdi_20' : line.cfdi_20,
                                'program_code_id' : line.program_code_id and line.program_code_id.id or False,
                                'fixed_discount' : line.fixed_discount,
                                'dependency_id':line.move_id.dependancy_id.id if line.account_id.dep_subdep_flag else False,
                                'sub_dependency_id':line.move_id.sub_dependancy_id.id if line.account_id.dep_subdep_flag else False,
                            } 
                
                invoice_line.append((0,0,line_vals))
            
        self.invoice_line_ids = False
        self.invoice_line_ids = invoice_line
        self.invoice_line_ids._onchange_price_subtotal()
        self._onchange_invoice_line_ids()

        for line in lines.move_id.line_ids:
            if line.account_id.dep_subdep_flag:
                line.dependency_id = line.move_id.dependancy_id.id
                line.sub_dependency_id = line.move_id.sub_dependancy_id.id
            line.uuid_related =  line.move_id.invoice_uuid
            line.serie_related =  line.move_id.cfdi_serie
            line.folio_related =  line.move_id.folio_cfdi

    def reversed_invoice(self):
        for record in self:
            line_vals = []
            invoice_line = []
            if record.line_ids and not record.refund_move_line_ids:
                for line in record.line_ids:
                    if line.debit>0.0:
                        line_vals = {
                                                    
                            'account_id': line.account_id.id,
                            'credit': line.debit,                                   
                            'refund_line_ids' : self.id,
                            'name': "Reversión: "+line.name,
                            'dependency_id': line.dependency_id.id, 
                            'sub_dependency_id': line.sub_dependency_id.id,
                            'folio_related': self.folio_cfdi,
                            'serie_related': self.cfdi_serie,
                            'uuid_related': self.invoice_uuid
                        }
                        invoice_line.append((0,0,line_vals))
                    else:
                        line_vals = {
                                                    
                            'account_id': line.account_id.id,
                            'debit': line.credit,                                   
                            'refund_line_ids' : self.id,
                            'name': "Reversión: "+line.name,
                            'dependency_id': line.dependency_id.id, 
                            'sub_dependency_id': line.sub_dependency_id.id,
                            'folio_related': self.folio_cfdi,
                            'serie_related': self.cfdi_serie,
                            'uuid_related': self.invoice_uuid
                        }
                        invoice_line.append((0,0,line_vals))

                record.income_status='reversed'

                unam_move_val = {'ref': "Reversión de : "+record.name, 'type': 'entry',
                                 'date': record.cancellation_date, 'journal_id': record.journal_id.id, 'company_id': record.env.user.company_id.id,
                                 'cfdi_serie': record.cfdi_serie, 'invoice_uuid': record.invoice_uuid, 'folio_cfdi': record.folio_cfdi, 
                                 'dependancy_id': record.dependancy_id.id, 'sub_dependancy_id': record.sub_dependancy_id.id, 'type_of_revenue_collection': record.type_of_revenue_collection,
                                 'cfdi_status': record.cfdi_status,
                                 'line_ids': invoice_line}


                move_obj = self.env[model_move]
                unam_move = move_obj.create(unam_move_val).with_context(check_move_validity=False)
                unam_move.action_post()

    def previous_exercise(self):
        
        for record in self:
            record.state='previous_exercise'
            record.line_ids.unlink()

    def cancel_invoice(self):        
        for record in self:
            if record.cfdi_status == 'R':
                record.state='rejected'
            if record.cfdi_status == 'E':
                record.state='error'
            if record.cfdi_status == 'D':
                record.state='disabled'
            record.name="/"
            record.line_ids.unlink()

    # Funcion para personalizar el _rec_name solo si se le pasa por contexto {'boletaje': True}
    def name_get(self, context=None):
        #logging.info(f"******************* {self.env.context}")
        res = []
        if self.env.context.get('boletaje', False):
        
            for rec in self:
                
                res.append((rec.id, rec.cfdi_folio))
            return res
        return super().name_get()

    def inherit_invoice(self):
        for record in self:
            if record.cfdi_status == 'H':
                record.state='inherit'
                record.reversed_invoice()
            if record.cfdi_status == 'G':
                record.state='inherit_substitution'
                record.line_ids.unlink()

    def validate_credit_note(self):
        for record in self:
            if not record.reversed_entry_id:
                if record.env.user.lang == 'es_MX':
                    raise ValidationError(_("No hay factura asociada a la nota de crédito"))
                else:
                    raise ValidationError(_("The credit note not has invoice"))

            if not record.credit_note_line_ids:
                if record.env.user.lang == 'es_MX':
                    raise ValidationError(_("No hay detalle de la nota de crédito para validar"))
                else:
                    raise ValidationError(_("Please add lines in credit note"))

        
        return True


    @api.constrains('serie_note', 'folio_note', 'type_of_registry_payment')
    def _check_credit_note(self):
        for record in self:
            if record.serie_note and record.folio_note and record.type_of_registry_payment=='credit_note':
                invoice_id = self.env[model_move].search([('serie_note', '=', record.serie_note), ('folio_note', '=', record.folio_note), ('type_of_registry_payment', '=', 'credit_note')])
                if invoice_id:
                    if record.env.user.lang == 'es_MX':
                        raise ValidationError(_("Ya existe una nota de crédito con la misma serie y folio, nota: "+invoice_id.name))
                    else:
                        raise ValidationError(_("There is an invoice associate to the credit note"))


    def post_credit_note(self, credit_notes_ids=None):
        cri_obj = self.env['affectation.income.budget']
        cri = ""
        journal = ""
        name = ""
        affectation_amount = 0
        date_affectation = ""
        credit_note_message = "Nota de crédito de: "
        for record in credit_notes_ids or self:
            if record.validate_credit_note() and not record.line_ids:
                invoice_line = []
                cta_214 = record.journal_id.default_debit_account_id
                cta_210 = record.reversed_entry_id.journal_id.default_debit_account_id
                move_id = record.id
                if record.reversed_entry_id.invoice_payment_state=='paid':
                    for line in record.credit_note_line_ids:
                        if line.tax_id.description=='16':
                            price = line.total_note-round(line.total_note/1.16, 6)
                            iva = line.total_note - price
                        else:
                            price = line.total_note
                            iva=0.0
                        for account_line in line.account_ie_id.ie_account_line_ids.filtered(lambda x:x.percentage > 0):
                            price_per = (price*account_line.percentage)/100

                            # Busqueda de CRI
                            account_code = account_line.account_id.code
                            cri = cri_obj.search([]).associate_cri_account_code(account_code)

                            line_vals = {
                                                    
                                            'account_id': account_line.account_id and account_line.account_id.id or False,
                                            'coa_conac_id': account_line.account_id and account_line.account_id.coa_conac_id.id or False,
                                            'debit': price_per,
                                            'conac_move' : True,                                  
                                            'move_id' : move_id,
                                            'partner_id': record.partner_id.id,
                                            'name': credit_note_message+record.reversed_entry_id.name,
                                            'dependency_id':record.dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                            'sub_dependency_id':record.sub_dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                            'folio_related': record.serie_note,
                                            'serie_related': record.folio_note,
                                            'uuid_related': record.folio_uuid,
                                            'cri' : cri.name if cri else False
                                        }
                            invoice_line.append((0,0,line_vals))

                            if(account_line.account_id.code[0]=='2' and account_line.account_id.code[1]=='1' and account_line.account_id.code[2]=='3'):
                                dep_213 = self.env['dependency']
                                id_dep_213 = dep_213.search([('dependency','=', '764')])[0]
                                sub_dep_213 = self.env['sub.dependency']
                                id_sub_dep_213 = sub_dep_213.search([('dependency_id','=', id_dep_213['id']),('sub_dependency','=','01')])[0]

                                line_vals = {'name': credit_note_message+record.reversed_entry_id.name,
                                             'account_id': account_line.account_id and account_line.account_id.id or False,
                                             'coa_conac_id': account_line.account_id and account_line.account_id.coa_conac_id.id or False,
                                             'credit': price_per, 
                                             'conac_move' : True,
                                             'amount_currency' : 0.0,
                                             #'currency_id' : payment.currency_id.id,                                     
                                             'move_id' : move_id,
                                             'partner_id': record.partner_id.id,
                                             'dependency_id':record.dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                             'sub_dependency_id':record.sub_dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                             'account_ie_id': line.account_ie_id and line.account_ie_id.id or False,
                                             # Se busca el CRI de la cuenta, si tiene CRI se pega al Asiento contable
                                             #'cri' : cri.name if cri else False
                                        }
                                invoice_line.append((0,0,line_vals))

                                line_vals = {'name': credit_note_message+record.reversed_entry_id.name,
                                             'account_id': account_line.account_id and account_line.account_id.id or False,
                                             'coa_conac_id': account_line.account_id and account_line.account_id.coa_conac_id.id or False,
                                             'debit': price_per, 
                                             'conac_move' : True,
                                             'amount_currency' : 0.0,
                                             #'currency_id' : payment.currency_id.id,                                     
                                             'move_id' : move_id,
                                             'partner_id': record.partner_id.id,
                                             'dependency_id': id_dep_213['id'],
                                             'sub_dependency_id': id_sub_dep_213['id'],
                                             'account_ie_id': line.account_ie_id and line.account_ie_id.id or False,
                                             # Se busca el CRI de la cuenta, si tiene CRI se pega al Asiento contable
                                             #'cri' : cri.name if cri else False
                                        }
                                invoice_line.append((0,0,line_vals))

                            if(account_line.account_id.code[0]=='4' and account_line.account_id.budget_registry):
                                # Busqueda de CRI
                                account_code = account_line.account_id.code
                                cri = cri_obj.search([]).associate_cri_account_code(account_code)
                                '''logging.info(f'account_line.account_id.code[0]==4 | Cuenta: {account_code} CRI: {cri.name}')'''
                                line_vals = {                
                                                'name': credit_note_message+record.reversed_entry_id.name,
                                                'account_id': record.journal_id.accrued_income_credit_account_id and record.journal_id.accrued_income_credit_account_id.id or False,
                                                'coa_conac_id': record.journal_id.conac_accrued_income_credit_account_id and record.journal_id.conac_accrued_income_credit_account_id.id or False,
                                                'debit': price_per, 
                                                'conac_move' : True,
                                                'amount_currency' : 0.0,
                                                #'currency_id' : payment.currency_id.id,                                     
                                                'move_id' : move_id,
                                                'partner_id': record.partner_id.id,
                                                'cri':cri.name if cri else False
                                            }
                                invoice_line.append((0,0,line_vals))
                                line_vals = {               
                                                'name': credit_note_message+record.reversed_entry_id.name,
                                                'account_id': record.journal_id.accrued_income_debit_account_id and record.journal_id.accrued_income_debit_account_id.id or False,
                                                'coa_conac_id': record.journal_id.conac_accrued_income_debit_account_id and record.journal_id.conac_accrued_income_debit_account_id.id or False,
                                                'credit': price_per,
                                                'conac_move' : True,
                                                'amount_currency' : 0.0,
                                                #'currency_id' : payment.currency_id.id,                                     
                                                'move_id' : move_id,
                                                'partner_id': record.partner_id.id,
                                                'cri':cri.name if cri else False
                                            }
                                invoice_line.append((0,0,line_vals))

                                line_vals = {               
                                                'name': credit_note_message+record.reversed_entry_id.name,
                                                'account_id': record.journal_id.recover_income_credit_account_id and record.journal_id.recover_income_credit_account_id.id or False,
                                                'coa_conac_id': record.journal_id.conac_recover_income_credit_account_id and record.journal_id.conac_recover_income_credit_account_id.id or False,
                                                'debit': price_per, 
                                                'conac_move' : True,
                                                'amount_currency' : 0.0,
                                                #'currency_id' : payment.currency_id.id,                                     
                                                'move_id' : move_id,
                                                'partner_id': record.partner_id.id,
                                                'cri':cri.name if cri else False
                                            }
                                invoice_line.append((0,0,line_vals))
                                line_vals = {                
                                                'name': credit_note_message+record.reversed_entry_id.name,
                                                'account_id': record.journal_id.recover_income_debit_account_id and record.journal_id.recover_income_debit_account_id.id or False,
                                                'coa_conac_id': record.journal_id.conac_recover_income_debit_account_id and record.journal_id.conac_recover_income_debit_account_id.id or False,
                                                'credit': price_per,
                                                'conac_move' : True,
                                                'amount_currency' : 0.0,
                                                #'currency_id' : payment.currency_id.id,                                     
                                                'move_id' : move_id,
                                                'partner_id': record.partner_id.id,
                                                'cri':cri.name if cri else False
                                            }
                                invoice_line.append((0,0,line_vals))

                                if cri:
                                    affectation_amount = price_per
                                    journal = self.journal_id.id
                                    date_affectation = record.date

                        if line.tax_id.description=='16':
                            line_vals = {
                                                    
                                            'account_id': line.tax_id.iva_transferred_account_id and line.tax_id.iva_transferred_account_id.id or False,
                                            'coa_conac_id': line.tax_id.iva_transferred_account_id and line.tax_id.iva_transferred_account_id.coa_conac_id.id or False,
                                            'debit': iva,
                                            'conac_move' : True,                                  
                                            'move_id' : move_id,
                                            'partner_id': record.partner_id.id,
                                            'name': credit_note_message+record.reversed_entry_id.name,
                                            'dependency_id':record.dependancy_id.id if line.tax_id.iva_transferred_account_id.dep_subdep_flag else False,
                                            'sub_dependency_id':record.sub_dependancy_id.id if line.tax_id.iva_transferred_account_id.dep_subdep_flag else False,
                                            'folio_related': record.serie_note,
                                            'serie_related': record.folio_note,
                                            'uuid_related': record.folio_uuid
                                        }
                            invoice_line.append((0,0,line_vals))
                        line_vals = {
                                                    
                                        'account_id': cta_214 and cta_214.id or False,
                                        'coa_conac_id': cta_214 and cta_214.coa_conac_id.id or False,
                                        'credit': iva+price,
                                        'conac_move' : True,                                  
                                        'move_id' : move_id,
                                        'partner_id': record.partner_id.id,
                                        'name': credit_note_message+record.reversed_entry_id.name,
                                        'dependency_id':record.dependancy_id.id if cta_214.dep_subdep_flag else False,
                                        'sub_dependency_id':record.sub_dependancy_id.id if cta_214.dep_subdep_flag else False,
                                        'folio_related': record.serie_note,
                                        'serie_related': record.folio_note,
                                        'uuid_related': record.folio_uuid
                                    }
                        invoice_line.append((0,0,line_vals))

                    record.with_context(check_move_validity=False).line_ids = invoice_line

                else:
                    for line in record.credit_note_line_ids:
                        if line.tax_id.description=='16':
                            price = line.total_note-round(line.total_note/1.16, 6)
                            iva = line.total_note - price
                        else:
                            price = line.total_note
                            iva = 0.0
                        line_vals = {
                                                
                                        'account_id': cta_210 and cta_210.id or False,
                                        'coa_conac_id': cta_210 and cta_210.coa_conac_id.id or False,
                                        'debit': price,
                                        'conac_move' : True,                                  
                                        'move_id' : move_id,
                                        'partner_id': record.partner_id.id,
                                        'name': credit_note_message+record.reversed_entry_id.name,
                                        'dependency_id':record.dependancy_id.id if cta_210.dep_subdep_flag else False,
                                        'sub_dependency_id':record.sub_dependancy_id.id if cta_210.dep_subdep_flag else False,
                                        'folio_related': record.serie_note,
                                        'serie_related': record.folio_note,
                                        'uuid_related': record.folio_uuid
                                    }
                        invoice_line.append((0,0,line_vals))
                        if line.tax_id.description=='16':
                            line_vals = {
                                                    
                                            'account_id': line.tax_id.iva_to_move_account_id and line.tax_id.iva_to_move_account_id.id or False,
                                            'coa_conac_id': line.tax_id.iva_to_move_account_id and line.tax_id.iva_to_move_account_id.coa_conac_id.id or False,
                                            'debit': iva,
                                            'conac_move' : True,                                  
                                            'move_id' : move_id,
                                            'partner_id': record.partner_id.id,
                                            'name': credit_note_message+record.reversed_entry_id.name,
                                            'dependency_id':record.dependancy_id.id if line.tax_id.iva_to_move_account_id.dep_subdep_flag else False,
                                            'sub_dependency_id':record.sub_dependancy_id.id if line.tax_id.iva_to_move_account_id.dep_subdep_flag else False,
                                            'folio_related': record.serie_note,
                                            'serie_related': record.folio_note,
                                            'uuid_related': record.folio_uuid
                                        }
                            invoice_line.append((0,0,line_vals))
                        line_vals = {
                                                    
                                        'account_id': cta_214 and cta_214.id or False,
                                        'coa_conac_id': cta_214 and cta_214.coa_conac_id.id or False,
                                        'credit': iva+price,
                                        'conac_move' : True,                                  
                                        'move_id' : move_id,
                                        'partner_id': record.partner_id.id,
                                        'name': credit_note_message+record.reversed_entry_id.name,
                                        'dependency_id':record.dependancy_id.id if cta_214.dep_subdep_flag else False,
                                        'sub_dependency_id':record.sub_dependancy_id.id if cta_214.dep_subdep_flag else False,
                                        'folio_related': record.serie_note,
                                        'serie_related': record.folio_note,
                                        'uuid_related': record.folio_uuid
                                    }
                        invoice_line.append((0,0,line_vals))

                    record.with_context(check_move_validity=False).line_ids = invoice_line
            
            
            record.action_post()

            # Enviar a la funcion
            name = self.name
            self._income_budget_afectation(cri,journal,name,affectation_amount,date_affectation)

    def _income_budget_afectation(self,cri,journal,name,affectation_amount,date_affectation):
        if cri and journal and name and affectation_amount and date_affectation:
            # Objetos
            obj_affetation = self.env['affectation.income.budget']
            obj_logbook = self.env['income.adequacies.function']
            obj_fiscal_year = self.env['account.fiscal.year']
            # Afectación al presupuesto
            type_affectation = 'eje-rec'
            date_accrued = False
            amount = affectation_amount
            # Mandar datos a la clase de afectación
            cri_id = cri.id
            # logging.info(f"CRI: {cri.name} Origen {origin} Fecha: {date_affectation} Monto: {amount}")
            obj_affetation.search([]).affectation_income_budget(type_affectation,cri_id, date_accrued,date_affectation,amount)
            # Guarda la bitacora de la adecuación
            # Busca el id del año fiscal
            year = date_affectation.year
            fiscal_year_id = obj_fiscal_year.search([('name', '=', year)])
            movement_type = 'income'
            movement = 'for_exercising_collected'
            origin_type = 'income-nc'
            id_name = name
            
            journal_id = journal
            update_date_record = datetime.today()
            update_date_income = datetime.today()
            obj_logbook.search([]).logbook_income_budget(fiscal_year_id.id,cri_id,date_affectation,movement_type,movement,origin_type,id_name,journal_id,update_date_record,amount,update_date_income)






class IncomeIncomeMoveLine(models.Model):

    _inherit = 'income.invoice.move.line'

    income_bank_journal_id = fields.Many2one('account.journal', "Bank")
    income_bank_account = fields.Many2one(related="income_bank_journal_id.bank_account_id", string="Bank Account")
    ref = fields.Char("Bank reference")


class SupportDocumentation(models.Model):
    _name = 'support.documentation'
    _description = "Support documentation"

    document = fields.Binary("Document")
    name =  fields.Char("Name")
    move_id = fields.Many2one(model_move,string='Documentation',ondelete='cascade')

class SpecialCertificates(models.Model):
    _name = 'special.certificates'
    _description = "Special certificates"

    dependency_id = fields.Many2one('dependency','Dependency')
    sub_dependency_id = fields.Many2one('sub.dependency', 'Sub Dependency')
    amount = fields.Float("Amount")

    certificate_id = fields.Many2one(model_move,string='Documentation',ondelete='cascade')

class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    refund_line_ids = fields.Many2one(model_move, 'Refund line ids')

class CreditNoteLine(models.Model):
    _name = 'credit.note.line'
    _description = "Credit note line"


    credit_note_id = fields.Many2one(model_move, 'Credit note id')
    name = fields.Char("Description")
    product_id = fields.Many2one('l10n_mx_edi.product.sat.code', "Product")
    account_ie_id = fields.Many2one('association.distribution.ie.accounts', "Afectation account")
    quantity = fields.Float("Quantity")
    price_unit = fields.Float("Price unit")
    tax_id = fields.Many2one('account.tax', "Tax")
    total_note = fields.Float("Total")

    @api.onchange('quantity')
    def calculate_total_quantity(self):
        self.total_note = self.quantity*self.price_unit

    @api.onchange('price_unit')
    def calculate_total_price(self):
        self.total_note = self.quantity*self.price_unit


    