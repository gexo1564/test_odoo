from odoo import models, fields,api,_
from odoo.exceptions import ValidationError

class CertificateSettings(models.Model):

    _name = 'certificate.settings'
    _description = "Certificates settings 00010030"
    _rec_name = 'certificate_type'

    certificate_type =  fields.Many2one('deposit.certificate.type', "Certificate type")
    dgpu_account_id = fields.Many2one('account.account', "DGPU account")
    certificates_settings_line_ids = fields.One2many('certificates.settings.line','certificate_setting_id','Distribution')

class CertificatesSettingsLine(models.Model):

    _name = 'certificates.settings.line'
    _description = 'Distribution of accounts in certificates'

    certificate_setting_id = fields.Many2one('certificate.settings', string='Certificate setting', ondelete='cascade')
    percentage = fields.Float('%')
    account_id = fields.Many2one("account.account",'Account')
    percentage_bridge_account = fields.Float('%')
    bridge_account_id = fields.Many2one("account.account",'Bridge account')
    bridge_dependency_id = fields.Many2one("dependency", 'Bridge dependency')
    bridge_sub_dependency_id = fields.Many2one("sub.dependency", 'Bridge subdependency')


