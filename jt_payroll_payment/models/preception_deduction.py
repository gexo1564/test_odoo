# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from email.policy import default
import re
from odoo import models, fields, api, _
from odoo.exceptions import UserError

class Preception(models.Model):

    _name = 'preception'
    _description = "Preception"
    _rec_name = 'key'

    key = fields.Char("Key")
    concept = fields.Char("Concept")
    credit_account_id = fields.Many2one('account.account','Credit Account')
    debit_account_id = fields.Many2one('account.account','Debit account')
    active = fields.Boolean(string='Active',default=True)
    is_net_pay = fields.Boolean(string='Is for Net Pay?',default=True)

class Deduction(models.Model):

    _name = 'deduction'
    _description = "Deduction"
    _rec_name = 'key'

    key = fields.Char("Key")
    concept = fields.Char("Concept")
    credit_account_id = fields.Many2one('account.account','Credit Account')
    debit_account_id = fields.Many2one('account.account','Debit account')
    active = fields.Boolean(string='Active',default=True)

    def name_get(self):
        result = []
        name = ''
        for record in self:
            name = record.key+' - '+record.concept or ''
            if  self.env.context.get('show_for_code_description',False):
                name = record.key+' - '+record.concept
            result.append((record.id,name))
        return result
    
    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
        args = args or []
        if name:
            records = self.search(['|',('key',operator,name),('concept',operator,name)])
            return records.name_get()
        return self.search([('key',operator,name),('concept',operator,name)]+args, limit=limit).name_get()