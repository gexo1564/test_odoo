import logging
import unittest
from odoo.exceptions import UserError,ValidationError
from datetime import datetime
from odoo.tests import tagged

from odoo.tests import common
_logger = logging.getLogger(__name__)

# Modelos
MODEL_SOLICITUD_BOLETAJE = 'siif.income.solicitud.boletaje'

# Strings
STR_EST_1 = 'ESTACIONAMIENTO 1'
STR_EVEN_VARIOS = 'EVENTOS VARIOS'

@tagged('-at_install','post_install', 'siif_income_solicitud_boletaje','siif_income')
class BoletajeTest(common.TransactionCase):

    def setUp(self):
        super(BoletajeTest,self).setUp()
        self.model = self.env[MODEL_SOLICITUD_BOLETAJE]

    def test_data_create(self):

        # Series
        test_data_1 = {
                'state': 'active', 
                'dependency_id': 236, 
                'sub_dependency_id': 1146, 
                'description': 'BOLETOS ESTADIO', 
                'tipo_control': 'Rango', 
                'boletos_aviso': 1000, 
                'total_boletos_asig': 0, 
                'total_boletos_comp': 0, 
                'total_boletos_rest': 0, 
                'boletos_restriccion': 0, 
                'porc_boletos_restricción': 0, 
                'line_ids_dotaciones': [[0, 'virtual_49', {'display_type': False, 'concepto': STR_EST_1, 'evento': STR_EVEN_VARIOS}], [0, 'virtual_51', {'display_type': False, 'concepto': 'ESTACIONAMIENTO 2', 'evento': STR_EVEN_VARIOS}]],

                # 'state': 'authorized', 
                # 'serie': 353, 
                # 'tipo_control': 'Rango', 
                #'tipo_boleto': 'printed', 
                # 'dependency_id': 236, 
                # 'sub_dependency_id': 1146, 
                # 'folio_inicial': 1, 
                # 'total_boletos_asig': 2000, 
                #'concepto_solicitud': 'Boletos X', 
                # 'feu_signature_1': False, 
                # '__last_update': False, 
                # 'documentary_support': [[6, False, []]], 
                #'line_ids_dotaciones': [[0, 'virtual_81', {'display_type': False, 'clv_dotacion': 194, 'concepto': STR_EST_1, 'evento': STR_EVEN_VARIOS, 'cantidad': 500, 'folio_inicial': 1, 'folio_final': 500, 'import_boleto': 0}], [0, 'virtual_84', {'display_type': False, 'clv_dotacion': 195, 'concepto': 'ESTACIONAMIENTO 2', 'evento': STR_EVEN_VARIOS, 'cantidad': 1500, 'folio_inicial': 501, 'folio_final': 2000, 'import_boleto': 0}]],

                # 'state': 'validated', 
                # 'serie': 353, 
                # 'tipo_control': 'Rango', 
                # 'dependency_id': 236, 
                # 'sub_dependency_id': 1146, 
                # '__last_update': False, 
                # 'documentary_support': [[6, False, []]], 
                # 'folio_inicial': 1, 
                # 'total_boletos_comp': 500, 
                # 'total_boletos_rest': 2000, 
                # 'folio_final': 500, 
                # 'line_ids_cfdi': [[0, 'virtual_49', {'display_type': False, 'nombre_cfdi': '01', 'nombre_archivo': '01', 'importe_cfdi': 5000}]], 'line_ids': [[0, 'virtual_51', {'display_type': False, 'clv_dotacion': 194, 'concepto_dot': STR_EST_1, 'concepto_boleto': 'GENERAL', 'nombre_cfdi': 716, 'total_boletos_comp_dotacion': 500, 'cantidad': 500, 'cancelados': 0, 'rob_extrav': 0, 'folio_inicial': 1, 'folio_final': 500, 'importe_corte': 10, 'total_cost_for_boletos': 5000}]]
            }
        
            # Solicitudes
        test_data_2 = {
                'state': 'authorized', 
                'serie': 353, 
                'tipo_control': 'Rango', 
                'tipo_boleto': 'printed', 
                'dependency_id': 236, 
                'sub_dependency_id': 1146, 
                'folio_inicial': 1, 
                'total_boletos_asig': 2000, 
                'concepto_solicitud': 'Boletos X', 
                'feu_signature_1': False, 
                '__last_update': False, 
                'documentary_support': [[6, False, []]], 
                'line_ids_dotaciones': [[0, 'virtual_81', {'display_type': False, 'clv_dotacion': 194, 'concepto': STR_EST_1, 'evento': STR_EVEN_VARIOS, 'cantidad': 500, 'folio_inicial': 1, 'folio_final': 500, 'import_boleto': 0}], [0, 'virtual_84', {'display_type': False, 'clv_dotacion': 195, 'concepto': 'ESTACIONAMIENTO 2', 'evento': STR_EVEN_VARIOS, 'cantidad': 1500, 'folio_inicial': 501, 'folio_final': 2000, 'import_boleto': 0}]]

            }

        self.assertNotEquals(test_data_1,test_data_2,'Son Iguales')
        print('------------------NotEquals data test-----------------------------' + 'OK')


        print('-------------------------Test Data Create Result:-------------------------' + 'OK')

        with self.assertRaises(ValidationError):
            self.env[MODEL_SOLICITUD_BOLETAJE].create({
                                        "dependency_id": False,
                                        "sub_dependency_id": False,
                                        })
        print('-------wrong data test[dependency and sub-dependency={}]----------OK'.format(False))