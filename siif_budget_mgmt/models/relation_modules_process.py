# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
import logging
  
class RelationModulesProcess(models.Model):
    _name = 'siif.budget.mgmt.relation.modules.process'
    _description = 'Relación Modulo - Proceso'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'name'
    _order = "name asc"

    name = fields.Char(string='Nombre', compute="_compute_name", tracking=True)
    module_name = fields.Many2one('siif.budget.mgmt.relation.modules', string='Nombre del modulo', tracking=True, required=True)
    name_process = fields.Char(string='Nombre del proceso', tracking=True, required=True)
    short_name_process = fields.Char(string='Nombre corto del proceso', tracking=True, required=True) #, compute="_compute_short_name")
    description = fields.Text(string='Descripción del proceso', tracking=True, required=True)
    

    @api.onchange('module_name', 'short_name_process')
    def _compute_name(self):
        for record in self:
            computed_name = ''
            if record.module_name:
                computed_name += str(record.module_name.name)
                computed_name += "/"
            if record.short_name_process:
                record.short_name_process = str(record.short_name_process).upper()
                computed_name += str(record.short_name_process)
            record.name = computed_name

    # To check that the size of the item is exactly 1 and is a numeric value
    @api.constrains('short_name_process')
    def _check_item(self):
        if len(str(self.short_name_process)) > 9:
            raise ValidationError(('El tamaño de el nombre corto debe ser menor o igual que 9.'))

    _sql_constraints = [('name', 'unique(name)', ('El nombre de la relación modulo proceso debe ser único'))]