# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError

from odoo.addons.siif_security.tools.utils import check_authorization, check_only_allowed_fields
from datetime import datetime
class ExpenditureItem(models.Model):

    _name = 'expenditure.item'
    _description = 'Item of Expenditure'
    _rec_name = 'item'

    item = fields.Char(string='Item', size=3)
    item_group = fields.Integer(compute='get_item_group',store=True)
    exercise_type = fields.Selection(
        [('r', 'R'), ('c', 'C'), ('d', 'D')], string='Exercise type')
    description = fields.Text(string='Item description')
    unam_account_id = fields.Many2one('account.account', string='UNAM account')
    shcp = fields.Char(string='Expenditure Item SHCP')
    desc_shcp = fields.Char(string='Description of expenditure item of SHCP')
    cog_id = fields.Many2one('coa.conac', string='CONAC Code')
    cog_desc = fields.Char(string='Account name COG CONAC')
    assigned_account = fields.Char(string='Assigned account')
    heading = fields.Many2one('cog.conac', string="Heading")
    cog_conac = fields.Char(string='COG CONAC')
    des_cog_conac = fields.Char(string='Description of COG CONAC')
    concept_cog_conac = fields.Char(string='Concept COG CONAC')
    departure_group_id = fields.Many2one('departure.group', string = 'Departure group')

    is_admin_budget = fields.Boolean(default="_get_default_is_admin_budget", compute="_compute_type_of_admin", store=False)
    is_admin_account = fields.Boolean(default="_get_default_is_admin_account", compute="_compute_type_of_admin", store=False)

    active = fields.Boolean('Active', default=True)

    overdraft = fields.Boolean('Overdraft', default=False)

    _sql_constraints = [('item', 'unique(item, start_date, end_date)', 'The item must be unique.')]

    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')

    @api.constrains('start_date', 'end_date')
    def check_end_date(self):
        if not self.start_date:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("Configure la fecha inicial"))
            else:
                raise ValidationError(_("The start date of validity is required"))
        if self.end_date and self.end_date <= self.start_date:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("La fecha de fin de vigencia no puede ser menor o igual a la fecha de inicio de vigencia"))
            else:
                raise ValidationError(_("The effective end date cannot be less than or equal to the effective date"))

    @api.depends('item')
    def get_item_group(self):
        for rec in self:
            if rec.item:
                rec.item_group =int(rec.item[0])
            else:
                rec.item_group = 0

    @api.model
    def _get_default_is_admin_budget(self):
        groups_budget = (
            'jt_budget_mgmt.group_budget_catalogs_admin_user',
            'jt_budget_mgmt.group_budget_admin_user'
        )
        return any([self.env.user.has_group(g) for g in groups_budget])

    @api.model
    def _get_default_is_admin_account(self):
        groups_account = (
            'jt_account_base.group_account_admin_user',
            'jt_account_base.group_account_super_admin_user'
        )
        return any([self.env.user.has_group(g) for g in groups_account])

    def _compute_type_of_admin(self):
        for record in self:
            groups_budget = (
                'jt_budget_mgmt.group_budget_catalogs_admin_user',
                'jt_budget_mgmt.group_budget_admin_user'
            )
            groups_account = (
                'jt_account_base.group_account_admin_user',
                'jt_account_base.group_account_super_admin_user'
            )
            record.is_admin_budget = any([self.env.user.has_group(g) for g in groups_budget])
            record.is_admin_account = any([self.env.user.has_group(g) for g in groups_account])

    @api.model
    def fields_get(self, fields=None, attributes=None):
        no_selectable_fields = [ 'concept_cog_conac', 'item_group' ]
        no_sortable_field = [ 'item_group' ]
        res = super(ExpenditureItem, self).fields_get(fields, attributes=attributes)
        for key, value in res.items():
            if key in no_selectable_fields:
                value.update({'selectable': False})
        for key, value in res.items():
            if key in no_sortable_field:
                value.update({'sortable': False})
        return res

    @api.constrains('item')
    def _check_item(self):
        if not str(self.item).isnumeric():
            raise ValidationError(_('The Item value must be numeric value'))

    def fill_zero(self, code):
        return str(code).zfill(3)

    @api.model
    def create(self, vals):
        if vals.get('item') and len(vals.get('item')) != 3:
            vals['item'] = self.fill_zero(vals.get('item'))
        item = self.search([('item', '=', vals.get('item'))])
        if item:
            raise ValidationError(_("The item must be unique."))
        return super(ExpenditureItem, self).create(vals)

    def write(self, vals):
        if vals.get('active') and not (self.is_admin_budget or self.is_admin_account):
            raise UserError("No tiene permisos para realizar esta acción.")
        return super(ExpenditureItem, self).write(vals)

    @api.onchange('unam_account_id')
    def _compute_unam_account_id(self):
        for record in self:
            if record.unam_account_id:
                # Prepare Expenditure Item SHCP and Description of expenditure item of SHCP From Account tags
                tag_code = ''
                tag_name = ''
                for tag in record.unam_account_id.tag_ids:
                    split_list = str(tag.name).split(' ')
                    if len(split_list) > 0:
                        tag_code += split_list[0]
                    if len(split_list) > 1:
                        tag_name += ' '.join(split_list[1:])
                # Set Expenditure Item SHCP
                record.shcp = tag_code
                # Set Description of expenditure item of SHCP
                record.desc_shcp = tag_name
                if record.unam_account_id.coa_conac_id:
                    record.cog_id = record.unam_account_id.coa_conac_id.id
                else:
                    record.cog_id = False
                if record.unam_account_id.conac_name:
                    record.cog_desc = record.unam_account_id.conac_name
                else:
                    record.cog_desc = None
                if record.unam_account_id.name:
                    record.assigned_account = record.unam_account_id.name
                else:
                    record.assigned_account = False
                if record.unam_account_id.cog_conac_id:
                    record.heading = record.unam_account_id.cog_conac_id
                    record.cog_conac = record.heading.chapter or ''
                    record.des_cog_conac = record.heading.name or ''
                    record.concept_cog_conac = record.heading.concept or ''
                else:
                    record.heading = False
                    record.cog_conac = None
                    record.des_cog_conac = None
                    record.concept_cog_conac = None
            else:
                record.shcp = False
                record.desc_shcp = False
                record.cog_id = False
                record.cog_desc = None
                record.assigned_account = False
                record.heading = False
                record.cog_conac = None
                record.des_cog_conac = None
                record.concept_cog_conac = None

    @check_authorization(
        'jt_budget_mgmt.group_budget_catalogs_admin_user',
        'jt_budget_mgmt.group_budget_admin_user'
    )
    def unlink(self):
        for item in self:
            program_code = self.env['program.code'].search([('item_id', '=', item.id)], limit=1)
            if program_code:
                raise ValidationError('No puedes eliminar una Partida que este ligada a un Código Programático!')
        return super(ExpenditureItem, self).unlink()

    def validate_item(self, item_string, typee):
        if len(str(item_string)) > 2:
            item_str = str(item_string).zfill(3)
            typee = str(typee).lower()
            if typee not in ['r', 'c', 'd']:
                typee = 'r'
            if item_str.isnumeric():
                item = self.search(
                    [('item', '=', item_str), ('exercise_type', '=', typee)], limit=1)
                if not item:
                    item = self.search(
                        [('item', '=', item_str)], limit=1)
                if item:
                    return item
        return False
    
    # if not return value is not valid #
    def get_validity_expenditure_item(self, expenditure_item, today = datetime.now()):
        query = """
                SELECT id
                FROM expenditure_item
                WHERE 
                    (
                        (start_date IS NULL AND end_date IS NULL AND item = %(item)s) OR
                        (start_date IS NOT NULL AND end_date IS NULL AND item = %(item)s AND start_date <= %(today)s) OR
                        (start_date IS NOT NULL AND end_date IS NOT NULL AND item = %(item)s AND start_date <= %(today)s AND end_date >= %(today)s)
                    )
                """
        params = {'item' : str(expenditure_item),
                  'today': today}
        self.env.cr.execute(query, params)
        expenditure_item_validity = self.env.cr.fetchall()

        return expenditure_item_validity
