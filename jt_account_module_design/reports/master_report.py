# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2023 UNAM.
#    Author: SIIF UNAM
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


import io
import base64
import json
from datetime import datetime
import pytz

from odoo import models, api, _
from odoo.tools.misc import xlsxwriter
from odoo.tools import config
from odoo.exceptions import ValidationError

DATE_FORMAT = '%Y-%m-%d'
CURRENCY_FORMAT = '"$"#,##0.00'
class MasterReport(models.AbstractModel):

    _name = "master.report"
    _inherit = "account.report"
    _description = "Master Report that shows accounts and balances"

    filter_date = None
    filter_all_entries = None
    filter_journals = None
    filter_unfold_all = None
    filter_cash_basis = None
    filter_hierarchy = None
    MAX_LINES = None
    filter_levels = None
    filter_master_report_filter = 0
    filter_first_date = 0
    filter_second_date = 0
    filter_third_date = 0
    filter_fourth_date = 0
    filter_fifth_date = 0

    def _get_reports_buttons(self):
        return [
            {'name': _('Export (XLSX)'),
             'sequence': 1,
             'action': 'print_xlsx',
             'file_export_type': _('XLSX')
             },
        ]

    def print_xlsx(self, options, extra_data):
        options['is_xlsx_report'] = True
        return {
                'type': 'ir_actions_account_report_download',
                'data': {'model': self.env.context.get('model'),
                         'options': json.dumps(options),
                         'output_format': 'xlsx',
                         'financial_id': self.env.context.get('id'),
                         }
                }

    @api.model
    def _get_columns_name(self, options):
        date_consults = [
            options['first_date'],
            options['second_date'],
            options['third_date'],
            options['fourth_date'],
            options['fifth_date']
        ]
        
        all_dates_valid = all(self.is_valid_date(date) for date in date_consults)
        if not all_dates_valid:
            return [{'name': _('Select five valid dates to generate the report'), 'class': 'text-center'}]
        
        columns = [
            {'name': _('Code'), 'style': 'width:20%'},
        ]

        if options.get('xlsx'):
            newline4 = newline5 = ''
        else:
            newline4 = '<br>' + _('until') + ' ' + str(options['fourth_date'])
            newline5 = '<br>' + _('until') + ' ' + str(options['fifth_date'])
            
        return columns + [
            {'name': _('Accounting Balance') + '<br>' + _('until') + ' ' + str(options[date]), 'class': 'text-center'} for date in ('first_date', 'second_date', 'third_date')
                ] + [
            {'name': _('Budget Resources'), 'class': 'text-center'},
            {'name': _('Own Resources') + newline4, 'class': 'text-center'},
            {'name': _('Total'), 'class': 'text-center'},
                ] + [
            {'name': _('Budget Resources'), 'class': 'text-center'},
            {'name': _('Own Resources') + newline5, 'class': 'text-center'},
            {'name': _('Total'), 'class': 'text-center'},
                ]

    @api.model
    def _get_lines(self, options, line_id=None):
        # Create new options with 'unfold_all' to compute the initial balances.
        # Then, the '_do_query' will compute all sums/unaffected earnings/initial balances for all comparisons.
        
        # Groups that have access to the report
        list_groups = [
            'jt_account_base.group_account_admin_user',
            'jt_account_base.group_account_super_admin_user',
            'jt_account_base.group_account_guest_user',
            'jt_account_base.group_account_approval_user']
        
        self.env['account.module.security'].check_user_permissions(list_groups)        

        lines = []

        date_consults = [
            options['first_date'],
            options['second_date'],
            options['third_date'],
            options['fourth_date'],
            options['fifth_date']
        ]

        all_dates_valid = all(self.is_valid_date(date) for date in date_consults)
        

        if all_dates_valid:

            account_balances = self._get_account_sub_dep_balances(date_consults) 
            for account in account_balances.keys():
                for sub_dep in account_balances[account].keys():
                    columns = []
                    for value in account_balances[account][sub_dep][1:]:
                        columns.append({'name': self.format_value(value), 'class': 'number', 'raw_value': value})
                    lines.append({
                        'id': 'acc_' + str(account) + '_subdep_' + str(sub_dep),
                        'name': account_balances[account][sub_dep][0],
                        'columns': columns,
                        'level': 5,
                    })
            account_group_obj = self.env['account.group'] 
            groups = account_group_obj.search([('parent_path', 'like', '%/%/%/%')])
            for group in groups:
                lines.append({
                    'id': 'group_' + str(group.id),
                    'name': group.code_prefix,
                    'columns': [],
                    'level': (group.parent_path.count('/') - 3),
                })

            accounts = self.env['account.account'].search([])
            for account in accounts:
                lines.append({
                    'id': 'account_' + str(account.id),
                    'name': account.code,
                    'columns': [],
                    'level': 3,
                })
                
            lines.sort(key = lambda line: line['name'])
            self._sum_columns_add_deps(lines)
            return lines
        
    @api.model
    def _get_report_name(self):
        return _("Master Report")

    def get_month_name(self, month):
        month_name = ''
        if month == 1:
            month_name = 'Enero'
        elif month == 2:
            month_name = 'Febrero'
        elif month == 3:
            month_name = 'Marzo'
        elif month == 4:
            month_name = 'Abril'
        elif month == 5:
            month_name = 'Mayo'
        elif month == 6:
            month_name = 'Junio'
        elif month == 7:
            month_name = 'Julio'
        elif month == 8:
            month_name = 'Agosto'
        elif month == 9:
            month_name = 'Septiembre'
        elif month == 10:
            month_name = 'Octubre'
        elif month == 11:
            month_name = 'Noviembre'
        elif month == 12:
            month_name = 'Diciembre'

        return month_name.upper()

    def get_xlsx(self, options, response=None):
        date_consults = [
            options['first_date'],
            options['second_date'],
            options['third_date'],
            options['fourth_date'],
            options['fifth_date']
        ]
        all_dates_valid = all(self.is_valid_date(date) for date in date_consults)
        if not all_dates_valid:
            raise ValidationError(_("Date format is not valid"))

        options['xlsx'] = True

        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        sheet = workbook.add_worksheet(self._get_report_name()[:31])
        date_default_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2, 'num_format': 'yyyy-mm-dd'})
        date_default_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': 'yyyy-mm-dd'})
        default_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2})
        default_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'})
        title_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'bottom': 2, 'align': 'center'})
        super_col_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'align': 'center'})
        level_0_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 13, 'bottom': 6, 'font_color': '#666666', 'num_format': CURRENCY_FORMAT})
        level_1_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 13, 'bottom': 1, 'font_color': '#666666', 'num_format': CURRENCY_FORMAT})
        level_2_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 0})
        level_2_col1_total_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666'})
        level_2_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'num_format': CURRENCY_FORMAT})
        level_3_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 0})
        level_3_col1_total_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 0})
        level_3_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': CURRENCY_FORMAT})
        level_4_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 0})
        level_4_col1_total_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 0})
        level_4_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': CURRENCY_FORMAT})
        level_5_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 0})
        level_5_col1_total_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 0})
        level_5_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': CURRENCY_FORMAT})
        currect_date_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'align': 'right'})
        currect_date_style.set_border(0)
        super_col_style.set_border(0)
        # Set columns width
        sheet.set_column(0, 0, 25)
        sheet.set_column(1, 1, 10)
        sheet.set_column(2, 2, 15)
        sheet.set_column(3, 3, 17)
        sheet.set_column(4, 4, 5)
        sheet.set_column(5, 5, 5)
        sheet.set_column(6, 6, 17)
        sheet.set_column(7, 7, 17)
        sheet.set_column(8, 8, 17)
        sheet.set_column(9, 9, 20)
        sheet.set_column(10, 10, 17)
        sheet.set_column(11, 11, 17)
        sheet.set_column(12, 12, 20)
        sheet.set_column(13, 13, 17)
        sheet.set_column(14, 14, 17)
        
        y_offset = 0
        col = 0
        
        sheet.merge_range(y_offset, col, 6, col, '', super_col_style)
        if self.env.user and self.env.user.company_id and self.env.user.company_id.header_logo:
            filename = 'logo.png'
            image_data = io.BytesIO(base64.standard_b64decode(self.env.user.company_id.header_logo))
            sheet.insert_image(0,0, filename, {'image_data': image_data,'x_offset':60,'y_offset':15,'x_scale':0.6,'y_scale':0.6})
        
        col += 1
        x_offset = 1

        tz = pytz.timezone('America/Mexico_City')
        actual_date = datetime.now(tz)
        currect_time_msg = "Fecha y hora de impresión: "
        month_name = self.get_month_name(actual_date.month)
        currect_time_msg += str(actual_date.day) + " de " + month_name + " del " + str(actual_date.year) + str(actual_date.strftime(" %I:%M:%S %p"))
        user_msg = " Reporte generado por: " + self.env.user.name
        sheet.merge_range(y_offset, x_offset, y_offset, x_offset+9, '', super_col_style)
        sheet.merge_range(y_offset+1, x_offset, y_offset+1, x_offset+13, 'UNIVERSIDAD NACIONAL AUTÓNOMA DE MÉXICO', super_col_style)
        sheet.merge_range(y_offset+2, x_offset, y_offset+2, x_offset+13, 'PATRONATO UNIVERSITARIO', super_col_style)
        sheet.merge_range(y_offset+3, x_offset, y_offset+3, x_offset+13, 'TESORERÍA', super_col_style)
        sheet.merge_range(y_offset+4, x_offset, y_offset+4, x_offset+13, self._get_report_name().upper(), super_col_style)
        sheet.merge_range(y_offset+5, x_offset, y_offset+5, x_offset+13, currect_time_msg, super_col_style)
        sheet.merge_range(y_offset+6, x_offset, y_offset+6, x_offset+13, user_msg, super_col_style)
        y_offset += 10
        total_columns = 0
        str_saldos = 'Saldos Contables'
        str_pre_ejer = 'Presupuesto Ejercido'
        dict_extra_header = {
            '6': [str_saldos,1],
            '7': [str_saldos,1],
            '8': [str_saldos,1],
            '9': [str_pre_ejer,3],
            '12': [str_pre_ejer,3],
        }
        dict_date_column = {
            '6': self._get_date_formated('first_date',options),
            '7': self._get_date_formated('second_date',options),
            '8': self._get_date_formated('third_date',options),           
            '9': self._get_date_formated('fourth_date',options),
            '10': self._get_date_formated('fourth_date',options),
            '11': self._get_date_formated('fourth_date',options),
            '12': self._get_date_formated('fifth_date',options),
            '13': self._get_date_formated('fifth_date',options),
            '14': self._get_date_formated('fifth_date',options),      
            }
        for row in self.get_header(options):
            x = 0
            for column in row:
                colspan = column.get('colspan', 1)
                header_label = column.get('name', '').replace('<br/>', ' ').replace('&nbsp;', ' ')
                extra_header = dict_extra_header.get(str(x), [' ',0])
                date_consult = dict_date_column.get(str(x), ' ')
                if extra_header[1] == 1:
                    sheet.write(y_offset-2, x, extra_header[0], title_style)
                    sheet.merge_range(y_offset-1,x,y_offset,x,date_consult,title_style)
                elif extra_header[1] > 1:
                    sheet.merge_range(y_offset-2,x,y_offset-2,x + extra_header[1]-1,extra_header[0],title_style)
                    sheet.write(y_offset-1, x, date_consult, title_style)
                elif extra_header[1] == 0 and x > 0:
                    sheet.write(y_offset-1, x, date_consult, title_style)

                if x == 0:
                    sheet.merge_range(y_offset,x,y_offset,x + colspan + 2,'Cuenta contable',title_style)
                    sheet.write(y_offset, x + colspan + 3, 'D', title_style)
                    sheet.write(y_offset, x + colspan + 4, 'SD', title_style)
                    x += 5
                if colspan == 1 and x >= 6:
                    sheet.write(y_offset, x, header_label, title_style)
                x += colspan
            total_columns = x
            y_offset += 1
        ctx = self._set_context(options)
        ctx.update({'no_format':True, 'print_mode':True, 'prefetch_fields': False})

        # deactivating the prefetching saves ~35% on get_lines running time
        lines = self.with_context(ctx)._get_lines(options)
        if options.get('hierarchy'):
            lines = self._create_hierarchy(lines, options)
        if options.get('selected_column'):
            lines = self._sort_lines(lines, options)            
        
        # write all data rows
        prev_line = [None] * 5
        for y in range(0, len(lines)):
            level = lines[y].get('level')
            if lines[y].get('caret_options'):
                style = level_3_style
                col1_style = level_3_col1_style
            elif level == 0:
                style = level_0_style
                col1_style = style
            elif level == 1:
                style = level_1_style
                col1_style = style
            elif level == 2:
                style = level_2_style
                col1_style = 'total' in lines[y].get('class', '').split(
                    ' ') and level_2_col1_total_style or level_2_col1_style
            elif level == 3:
                style = level_3_style
                col1_style = 'total' in lines[y].get('class', '').split(
                    ' ') and level_3_col1_total_style or level_3_col1_style
            elif level == 4:
                style = level_4_style
                col1_style = 'total' in lines[y].get('class', '').split(
                    ' ') and level_4_col1_total_style or level_4_col1_style
            elif level == 5:
                style = level_5_style
                col1_style = 'total' in lines[y].get('class', '').split(
                    ' ') and level_5_col1_total_style or level_5_col1_style
            else:
                style = default_style
                col1_style = default_col1_style
            # write the first column, with a specific style to manage the
            # indentation
            cell_type, cell_value = self._get_cell_type_value(lines[y])
            for x in range(0,total_columns):
                if x < level:
                    x_val = prev_line[x]
                elif x == level:
                    x_val = cell_value
                else:
                    x_val = ''
                if x < 5:
                    prev_line[x] = x_val
                sheet.write(y + y_offset, x, x_val, col1_style)
            # write all the remaining cells
            for x in range(1, len(lines[y]['columns']) + 1):
                cell_type, cell_value = self._get_cell_type_value(
                    lines[y]['columns'][x - 1])
                if cell_type == 'date':
                    sheet.write_datetime(
                        y + y_offset, x + lines[y].get('colspan', 1) + 4, cell_value, date_default_style)
                else:
                    sheet.write(
                        y + y_offset, x + lines[y].get('colspan', 1) + 4, cell_value, style)
        workbook.close()
        output.seek(0)
        generated_file = output.read()
        output.close()
        return generated_file    

    def _get_account_sub_dep_balances(self,date_consults):
        """Returns a dictionary with account ids, their balances and sub dependencies"""
        
        account_balances = {}
        query = """
        
        SELECT
        COALESCE(ACCOUNTING.account_id, BUDGET.account_id, NO_MOVES.account_id) account_id,
        COALESCE(ACCOUNTING.sub_dependency_id, BUDGET.sub_dependency_id, NO_MOVES.sub_dependency_id) sub_dependency_id,
        COALESCE(ACCOUNTING.name, BUDGET.name, NO_MOVES.name) AS name,
        COALESCE(ACCOUNTING.accounting_0, 0) accounting_0,
        COALESCE(ACCOUNTING.accounting_1, 0) accounting_1,
        COALESCE(ACCOUNTING.accounting_2, 0) accounting_2,
        COALESCE(BUDGET.zero_0, 0) zero_0,
        COALESCE(BUDGET.not_zero_0, 0) not_zero_0,
        COALESCE(BUDGET.total_0, 0) total_0,
        COALESCE(BUDGET.zero_1, 0) zero_1,
        COALESCE(BUDGET.not_zero_1, 0) not_zero_1,
        COALESCE(BUDGET.total_1, 0) total_1
        FROM (
        SELECT
        MOVE.account_id,
        COALESCE(MOVE.sub_dependency_id, 0) sub_dependency_id,
        CONCAT(ACCOUNT.code, '-', DEP.dependency, '-', SUBDEP.sub_dependency) AS name,
        SUM(CASE WHEN MOVE.date <= %(date_consult1)s THEN MOVE.balance ELSE 0 END) AS accounting_0,
        SUM(CASE WHEN MOVE.date <= %(date_consult2)s THEN MOVE.balance ELSE 0 END) AS accounting_1,
        SUM(CASE WHEN MOVE.date <= %(date_consult3)s THEN MOVE.balance ELSE 0 END) AS accounting_2
        FROM
        account_move_line AS MOVE
        LEFT JOIN
        account_account AS ACCOUNT ON MOVE.account_id = ACCOUNT.id
        LEFT JOIN
        sub_dependency AS SUBDEP ON MOVE.sub_dependency_id = SUBDEP.id
        LEFT JOIN
        dependency AS DEP ON DEP.id = SUBDEP.dependency_id
        WHERE
        MOVE.parent_state = 'posted'
        GROUP BY
        ACCOUNT.code,
        MOVE.account_id,
        SUBDEP.sub_dependency,
        MOVE.sub_dependency_id,
        SUBDEP.dependency_id,
        DEP.dependency
        ) AS ACCOUNTING
        FULL JOIN (
        SELECT
        MOVE.account_id,
        COALESCE(MOVE.sub_dependency_id, 0) sub_dependency_id,
        CONCAT(ACCOUNT.code, '-', DEP.dependency, '-', SUBDEP.sub_dependency) AS name,
        SUM(CASE WHEN MOVE.date <= %(date_consult4)s AND PROG.resource_origin_id = 22 THEN MOVE.balance ELSE 0 END) AS zero_0,
        SUM(CASE WHEN MOVE.date <= %(date_consult4)s AND PROG.resource_origin_id != 22 THEN MOVE.balance ELSE 0 END) AS not_zero_0,
        SUM(CASE WHEN MOVE.date <= %(date_consult4)s AND PROG.resource_origin_id = 22 THEN MOVE.balance ELSE 0 END) +
        SUM(CASE WHEN MOVE.date <= %(date_consult4)s AND PROG.resource_origin_id != 22 THEN MOVE.balance ELSE 0 END) AS total_0,
        SUM(CASE WHEN MOVE.date <= %(date_consult5)s AND PROG.resource_origin_id = 22 THEN MOVE.balance ELSE 0 END) AS zero_1,
        SUM(CASE WHEN MOVE.date <= %(date_consult5)s AND PROG.resource_origin_id != 22 THEN MOVE.balance ELSE 0 END) AS not_zero_1,
        SUM(CASE WHEN MOVE.date <= %(date_consult5)s AND PROG.resource_origin_id = 22 THEN MOVE.balance ELSE 0 END) +
        SUM(CASE WHEN MOVE.date <= %(date_consult5)s AND PROG.resource_origin_id != 22 THEN MOVE.balance ELSE 0 END) AS total_1
        FROM
        account_move_line AS MOVE
        LEFT JOIN
        account_account AS ACCOUNT ON MOVE.account_id = ACCOUNT.id
        LEFT JOIN
        sub_dependency AS SUBDEP ON MOVE.sub_dependency_id = SUBDEP.id
        LEFT JOIN
        dependency AS DEP ON DEP.id = SUBDEP.dependency_id
        JOIN
        program_code AS PROG ON MOVE.program_code_id = PROG.id
        WHERE
        MOVE.parent_state = 'posted'
        GROUP BY
        ACCOUNT.code,
        MOVE.account_id,
        SUBDEP.sub_dependency,
        MOVE.sub_dependency_id,
        SUBDEP.dependency_id,
        DEP.dependency
        ) AS BUDGET ON ACCOUNTING.account_id = BUDGET.account_id AND ACCOUNTING.sub_dependency_id = BUDGET.sub_dependency_id
        FULL JOIN (
        SELECT
        id account_id,
        0 sub_dependency_id,
        CONCAT(code, '--') AS name
        FROM
        account_account
        WHERE
        id NOT IN (
        SELECT
        account_id
        FROM
        account_move_line
        WHERE
        parent_state = 'posted'
        GROUP BY
        account_id)
        ) AS NO_MOVES ON ACCOUNTING.account_id = NO_MOVES.account_id AND ACCOUNTING.sub_dependency_id = NO_MOVES.sub_dependency_id;

        """
        params = {
            'date_consult1': date_consults[0],
            'date_consult2': date_consults[1],
            'date_consult3': date_consults[2],
            'date_consult4': date_consults[3],
            'date_consult5': date_consults[4],
        }
        self.env.cr.execute(query,params)
        result = self.env.cr.fetchall()
        
        for row in result:
            account_id, sub_dependency = row[0], row[1]
            try:
                account_balances[account_id][sub_dependency] = list(row[2:])
            except KeyError:
                account_balances[account_id] = {}
                account_balances[account_id][sub_dependency] = list(row[2:])
        return account_balances

    def _sum_columns_add_deps(self, lines):        
        col_num = 9
        columns = acc_sum = gp2_sum = gp1_sum = gp0_sum = [0] * col_num
        dep = prev_dep = None
        for i in range(len(lines) - 1, -1, -1):
            level = lines[i]['level']
            if level == 5:
                dep = lines[i]['name'].split('-')[-2]
                lines[i]['name'] = lines[i]['name'].split('-')[-1]
                if dep != prev_dep:
                    if prev_dep != None:
                        if prev_dep != '':
                            lines.insert(i + 1,
                                         {
                                             'id': lines[i]['name'] + '_' + prev_dep + '_' + lines[i]['id'],
                                             'name': prev_dep,
                                             'columns': [{'name': self.format_value(value), 'class': 'number'} for value in columns],
                                             'level': 4,
                                         })
                        else:
                            lines.pop(i + 1)
                        for j in range(col_num):
                            acc_sum[j] += columns[j]
                    columns = [elem['raw_value'] for elem in lines[i]['columns']]
                else:
                    for j in range(col_num):
                        columns[j] += lines[i]['columns'][j]['raw_value']
                prev_dep = dep
            elif level == 3:
                if lines[i + 1]['level'] == 5:
                    if prev_dep != '':
                        lines.insert(i + 1,
                                     {
                                         'id': lines[i]['name'] + '_' + prev_dep + '_' + lines[i]['id'],
                                         'name': prev_dep,
                                         'columns': [{'name': self.format_value(value), 'class': 'number', 'raw_value': value} for value in columns],
                                         'level': 4,
                                     })
                    else:
                        lines.pop(i + 1)
                    for j in range(col_num):
                            acc_sum[j] += columns[j]
                dep = prev_dep = None
                lines[i]['columns'] = [{'name': self.format_value(value), 'class': 'number'} for value in acc_sum]
                for j in range(col_num):
                    gp2_sum[j] += acc_sum[j]
                acc_sum = [0] * col_num
            elif level == 2:
                lines[i]['columns'] = [{'name': self.format_value(value), 'class': 'number'} for value in gp2_sum]
                for j in range(col_num):
                    gp1_sum[j] += gp2_sum[j]
                gp2_sum = [0] * col_num
            elif level == 1:
                lines[i]['columns'] = [{'name': self.format_value(value), 'class': 'number'} for value in gp1_sum]
                for j in range(col_num):
                    gp0_sum[j] += gp1_sum[j]
                gp1_sum = [0] * col_num
            elif level == 0:
                lines[i]['columns'] = [{'name': self.format_value(value), 'class': 'number'} for value in gp0_sum]
                gp0_sum = [0] * col_num

    def is_valid_date(self,date_str):
        try:
            datetime.strptime(str(date_str), DATE_FORMAT)
            return True
        except ValueError:
            return False

    def _get_date_formated(self, date_number, options):
        date = options[date_number]
        date_obj = datetime.strptime(date, '%Y-%m-%d')
        date_result = "al "+date_obj.strftime('%d-%m-%Y')
        return date_result
