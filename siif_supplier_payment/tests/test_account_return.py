import logging
from odoo.tests import common
from odoo.exceptions import UserError
from odoo.tests import common
import base64
from datetime import date
"""Modelos"""
PROGRAM_CODE='program.code'
ACCOUNT_ACC='account.account'
EGRESS_KEY='egress.keys'
YEAR_CONF='year.configuration'
ACC_RETURN='account.returns'
BANK_REF='bank.reference'
class TestAccountReturns(common.TransactionCase):
    def setUp(self):
        super().setUp()
        self.dep                = self.env['dependency'].search([('dependency','=','211')])
        self.sub_dep            = self.env['sub.dependency'].search([('dependency_id','=',self.dep.id),('sub_dependency','=','09')])
        self.user               = self.env['res.users'].search([('name','=','adminTesoreria')])

        self.program_code_pa_id = self.env[PROGRAM_CODE].search([('program_code','=','2023422021109523030024208E0112170101090000000000000000000000')]).id
        self.program_code_ie_id = self.env[PROGRAM_CODE].search([('program_code','=','2023420221109711080124208E0113190301090000000000000000000000')]).id
        self.program_code_cxp_id =self.env[PROGRAM_CODE].search([('program_code','=','2023420221109115020024208E0111130101090000000000000000000000')]).id

        self.account_ie = self.env['association.distribution.ie.accounts'].search([('ie_key', '=', '202')],limit=1)
        self.account_id_dep = self.env[ACCOUNT_ACC].search([('code', '=', '221.003.011.001')])
        self.account_id = self.env[ACCOUNT_ACC].search([('code', '=', '221.003.011.001')])
        
        self.cxp_type = self.env['account.topay.type'].create({
            'name':'CXP PRUEBA',
            'description':'CXP PRUEBA',
            'prefix':'CXP',
            'journal_id':self.env['account.journal'].search([('name','=','Diario de Cuentas por Pagar MXN (CCP04) Original')]).id,
            'recording_accounting_period_start':'2000-01-01',
            'is_allocated_budget_allowed':True,
            'is_nonrecurring_income_allowed':False,
            'is_account_allowed':False,
            'is_capitalized':False,
            'account_for_balance_on_account_payable_id':self.env[ACCOUNT_ACC].search([('code','=','221.003.011.001')]).id,

        })
        self.cxp_line = self.env['account.topay.line'].create({
            'origin_resource_sel':self.env['resource.origin'].search([('key_origin','=','00')]).id,
            'program_code_id':self.program_code_cxp_id,
            'parity_origin_money':1.00,
            'origin_money':2000.00,
            'mx_money':2000.00,
        })


        self.acc_topay = self.env['account.topay'].create({
            'type_provision':self.cxp_type.id,
            'partner_id':self.env['res.partner'].search([('vat','=','UNA2972401002')]).id,
            'dependency_id':self.dep.id,
            'sub_dependency_id':self.sub_dep.id,
            'date_receipt':date.today(),
            'state':'draft',
            'invoice_date':date.today(),
            'no_of_document':1,
            'currency_id':self.env['res.currency'].search([('name','=','MXN')]).id,
            'upa_document_type':self.env['upa.document.type'].search([('document_number','=','02')]).id,
            'document_type':'national',
            'secretary_admin':'ADMIN',
            'secretary_admin_email':'a@mail.com',   
        })
        
        self.account_move = self.env['account.move'].create({
            'partner_id':self.env['res.partner'].search([('vat','=','UNA2972401002')]).id,
            'journal_id':self.env['payment.parameters'].get_param('national_journal').id,
            'dependancy_id':self.dep.id,
            'sub_dependancy_id':self.sub_dep.id,
            'date_receipt':date.today(),
            'type':'in_invoice',
            'state':'draft',
            'invoice_date':date.today(),
            'folio_dependency':'DEVOLUCION',
            'administrative_forms':1,
            'l10n_mx_edi_payment_method_id':self.env['l10n_mx_edi.payment.method'].search([('code','=','99')]).id,
            'currency_id':self.env['res.currency'].search([('name','=','MXN')]).id,
            'upa_key':self.env['policy.keys'].search([('organization','=','REC')]).id,
            'upa_document_type':self.env['upa.document.type'].search([('document_number','=','02')]).id,
            'document_type':'national',
            'payment_issuance_method':'manual',
            'operation_type_id':self.env['operation.type'].search([('op_number','=','03')]).id,
            'payment_bank_id':self.env['res.bank'].search([('name','=','HSBC')]).id,
            'payment_bank_account_id':self.env['res.partner.bank'].search([('acc_number','=','04043049055')]).id,
            'invoice_line_ids':[
                {
                    'egress_key_id':self.env[EGRESS_KEY].search([('key','=','PA')]).id,
                    'program_code_id':self.program_code_pa_id,
                    'account_id': self.account_id,
                    'quantity':1.00,
                    'price_unit':100.00
                },
                {
                    'egress_key_id':self.env[EGRESS_KEY].search([('key','=','IEMN')]).id,
                    'program_code_id':self.program_code_ie_id,
                    'account_id': self.account_id,
                    'quantity':1.00,
                    'price_unit':100.00,
                    'account_ie':self.account_ie.id,
                },
                {
                    'egress_key_id':self.env[EGRESS_KEY].search([('key','=','CPN')]).id,
                    'program_code_id':self.program_code_cxp_id,
                    'quantity':1.00,
                    'price_unit':100.00,
                    'account_to_pay_id':self.acc_topay.id,
                }
            ]

        })

    def test_create_acc_return(self):
        file_pdf = open('siif_currency_purchase_req/static/tests/file_pdf.pdf',"rb")
        out = file_pdf.read()
        file_pdf.close()
        files = base64.b64encode(out)
        qr = open('siif_currency_purchase_req/static/tests/QR.png',"rb")
        out = qr.read()
        qr.close()
        qr_img = base64.b64encode(out)

        archivo=self.env['ir.attachment'].create({
				'name':"Archivo prueba",
				'res_model':"account.returns",
				'company_id':1,
                'store_fname':files,
				})

        self.cxp_line.write({'account_topay_id':self.acc_topay.id})
        self.acc_topay.validate_available_origin_resource()
        self.acc_topay.action_validate()
        self.acc_topay.approve(False)

        self.assertEqual(self.acc_topay.state,'approved')

        self.account_move.write({'previous_number':self.acc_topay.id})
        self.account_move.action_register()
        self.env['account.move'].approve_for_payment(self.account_move.id)
        
        self.account_move.payment_state='paid'
        self.account_move.name='PRUEBADEV'

        return_info_01={
            'year':self.env[YEAR_CONF].search([('name','=',str(date.today().year))]).id,
            'justify_files':archivo,
            'support_files':archivo}

        #Crear devolución. Devolución sin solicitud de pago
        with self.assertRaises(UserError):
            self.env[ACC_RETURN].create(return_info_01)

        devolucion=self.env[ACC_RETURN].create({
            'year':self.env[YEAR_CONF].search([('name','=',str(date.today().year))]).id,
            'account_move_id':self.account_move.id,
            'justify_files':archivo,
            'support_files':archivo,
            
        })
        #Generar la información de las lineas
        lineas=devolucion._get_return_lines()

        lines=[]
        monto=0
        for l in lineas:
            
            #Revisión si el monto de devolución por cada linea es mayor o menor
            l['selected_line']=True
            l['account_returns_line_ids']=devolucion.id
            l['price_return']=10
            aux=self.env['account.returns.line'].create(l)

            monto+=aux.price_return
            lines.append(aux)


        devolucion.has_return_lines=True
        devolucion.amount_return=monto
        self.assertEqual(devolucion.amount_return,30.00)
        self.assertNotEqual(devolucion.acc_return_lines,False)


        #Publicar con firma
        devolucion.write({'qr_signature_dep':qr_img})
        devolucion.action_for_publish()

        for i in devolucion.acc_return_lines:
            logging.critical(i.selected_line)

        #Aprobar
        devolucion.action_for_approve()

        #Generar referencia
        folio='00001/'+str(date.today().year)
        ref=self.env[BANK_REF]._gen_short_ref(self.account_move.dependancy_id,self.account_move.sub_dependancy_id,folio,'money_return')
        reference=self.env[BANK_REF].calculate_bank_ref(ref,devolucion.amount_return,date.today())
        ref_bank=self.env[BANK_REF].create({
            'reference':reference,
            'short_ref':ref,
            'amount':devolucion.amount_return,
            'date':date.today(),
        })
        devolucion.write({'folio':folio,'bank_ref':ref_bank.id})


        #Obtener movimiento bancario
        bank=self.env['bank.movements'].create({'amount_interest':devolucion.amount_return,'ref':ref_bank.reference})
        #Reconocer movimiento bancario
        bank.recognize()

        #Terminado devolución
        self.assertEqual(devolucion.state,'done')
        self.assertNotEqual(devolucion.acc_move_lines,False)
        self.assertEqual(devolucion.account_move_id.payment_state,'with_returns')


        #Crear otra solicitud
        return_info_02={
            'year':self.env[YEAR_CONF].search([('name','=',str(date.today().year))]).id,
            'account_move_id':self.account_move.id,
            'justify_files':archivo,
            'support_files':archivo}

        #Crear devolución. Devolución sin solicitud de pago
        self.env[ACC_RETURN].create(return_info_02)


