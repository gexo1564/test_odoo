
from odoo import models, fields, api,_
from odoo.exceptions import ValidationError


class AccountMoveLine(models.Model):

    _inherit = 'account.move.line'

    '''
    This is the model that will be inherited by the account.move.line model
    The purpose of this model is to add a new field in account.move.line model 
    is a manual accounting record.
    ''' 

class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    is_for_manual_accounting_record = fields.Boolean('Is for manual accounting record')

    @api.constrains('name')
    def _is_required_name_for_manual_accounting_record(self):
        for record in self:
            if record.is_for_manual_accounting_record and not record.name:
                raise ValidationError(_('Please enter a name for the manual accounting record'))
