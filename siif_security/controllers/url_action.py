from odoo import http, _
from odoo.exceptions import AccessError
from odoo.http import request
from odoo.addons.web.controllers import main

class UrlAction(main.Action):

  @http.route()
  def load(self, action_id, additional_context=None):
    condition = '%%,%s' % action_id
    menu = request.env['ir.ui.menu'].sudo().search(
      [('action', '=like', condition)]
    )
    user = request.env['res.users'].sudo().browse(request.session.uid)
    # Se valida si el usuario tiene derechos de acceso
    if not self._user_has_access_right(action_id, menu, user):
      # Si el usuario no tiene derecho a acceder se lanza una excepcion
      raise AccessError(_("You don't have permissions to access the view."))
    return super().load(action_id, additional_context=additional_context)


  def _user_has_access_right(self, action_id, menu, user):
    # Validación de que los action_id solo sean de tipo int, de lo contrario
    # devuelve True
    if not isinstance(action_id, int):
      return True
    # Se valida que el usuario perteneza a los grupos definidos por la acción.
    action = request.env['ir.actions.act_window'].sudo().search([('id','=',action_id)], limit=1)
    if action and len(action.groups_id) > 0:
      return any(elem in user.groups_id.ids for elem in action.groups_id.ids)

    # Si la accion no tiene grupos defindos se revisan los menus en cada nivel.
    # Devuleve False si no se tiene acceso a alguno de los niveles de los menus.
    if len(menu) == 0:
      return True
    access_right = False
    for m in menu:
      access = True
      while m:
        if m and len(m.groups_id.ids) > 0:
          access &= any(elem in user.groups_id.ids for elem in m.groups_id.ids)
        m = m.parent_id
      access_right |= access
    return access_right