from odoo.tests import TransactionCase
from odoo.tests import tagged
from unittest.mock import patch, MagicMock
from psycopg2 import sql

@tagged('jt_finance')
class ReportCalendarAmountAssign(TransactionCase):
    def test_init(self):
        # Intento de inyección el cual no se realiza porque esta parametrizado
        test_inyeccion = 'SELECT id FROM productive_account where id = 1"'
        tabla = 'tabla'
        tabla += test_inyeccion

        query = sql.SQL(""""
            CREATE OR REPLACE VIEW {} AS (
            select max(id) as id, year, new_item_id, item_id_first as item_first,item_id_second as item_second,max(id) as line_id,
                sum(annual_amount) as annual_amount,sum(january) as january,sum(amount_deposite_january) as amount_deposite_january,(COALESCE(sum(january),0)-COALESCE(sum(amount_deposite_january),0)) as pending_january,
                sum(february) as february,sum(amount_deposite_february) as amount_deposite_february,(COALESCE(sum(february),0)-COALESCE(sum(amount_deposite_february),0)) as pending_february,
                sum(march) as march,sum(amount_deposite_march) as amount_deposite_march,(COALESCE(sum(march),0)-COALESCE(sum(amount_deposite_march),0)) as pending_march,
                sum(april) as april,sum(amount_deposite_april) as amount_deposite_april,(COALESCE(sum(april),0)-COALESCE(sum(amount_deposite_april),0)) as pending_april,
                sum(may) as may,sum(amount_deposite_may) as amount_deposite_may,(sum(may)-sum(amount_deposite_may)) as pending_may,
                sum(june) as june,sum(amount_deposite_june) as amount_deposite_june,(COALESCE(sum(june),0)-COALESCE(sum(amount_deposite_june),0)) as pending_june,
                sum(july) as july,sum(amount_deposite_july) as amount_deposite_july,(COALESCE(sum(july),0)-COALESCE(sum(amount_deposite_july),0)) as pending_july,
                sum(august) as august,sum(amount_deposite_august) as amount_deposite_august,(COALESCE(sum(august),0)-COALESCE(sum(amount_deposite_august),0)) as pending_august,
                sum(september) as september,sum(amount_deposite_september) as amount_deposite_september,(COALESCE(sum(september),0)-COALESCE(sum(amount_deposite_september),0)) as pending_september,
                sum(october) as october,sum(amount_deposite_october) as amount_deposite_october,(COALESCE(sum(october),0)-COALESCE(sum(amount_deposite_october),0)) as pending_october,
                sum(november) as november,sum(amount_deposite_november) as amount_deposite_november,(COALESCE(sum(november),0)-COALESCE(sum(amount_deposite_november),0)) as pending_november,
                sum(december) as december,sum(amount_deposite_december) as amount_deposite_december,(COALESCE(sum(december),0)-COALESCE(sum(amount_deposite_december),0)) as pending_december 
            from calendar_assigned_amounts_lines
            group by year, new_item_id, item_id_first,item_id_second
            )""").format(
                sql.Identifier(tabla)
            )
        
        # Simular la ejecución de la consulta
        with patch.object(self.env.cr, 'execute') as mock_execute:           
            # Ejecutar la prueba
            self.env.cr.execute(query)
            # Asegurar que la consulta se haya ejecutado al menos una vez
            self.assertTrue(mock_execute.called, "La consulta query no se ejecutó.")
            print(F"PRUEBA DE QUERY SANITIZADO 1 test__init\n{query}")
