# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

from odoo.addons.siif_security.tools.utils import check_authorization, check_only_allowed_fields
from datetime import datetime
class ResourceOrigin(models.Model):

    _name = 'resource.origin'
    _description = 'Origin of the Resource'
    _rec_name = 'key_origin'

    key_origin = fields.Char(string='Key origin of the resource')
    desc = fields.Char(string='Description of origin of the resource')

    _sql_constraints = [('key_origin', 'unique(key_origin, start_date, end_date)', 'The key origin must be unique.')]

    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')

    @api.constrains('start_date', 'end_date')
    def check_end_date(self):
        if not self.start_date:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("Configure la fecha inicial"))
            else:
                raise ValidationError(_("The start date of validity is required"))
        if self.end_date and self.end_date <= self.start_date:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("La fecha de fin de vigencia no puede ser menor o igual a la fecha de inicio de vigencia"))
            else:
                raise ValidationError(_("The effective end date cannot be less than or equal to the effective date"))

    @check_authorization(
        'jt_budget_mgmt.group_budget_catalogs_admin_user',
        'jt_budget_mgmt.group_budget_admin_user'
    )
    @check_only_allowed_fields('desc', 'start_date', 'end_date')
    def write(self, vals):
        return super(ResourceOrigin, self).write(vals)

    @check_authorization(
        'jt_budget_mgmt.group_budget_catalogs_admin_user',
        'jt_budget_mgmt.group_budget_admin_user'
    )
    def unlink(self):
        for ro in self:
            program_code = self.env['program.code'].search([('resource_origin_id', '=', ro.id)], limit=1)
            if program_code:
                raise ValidationError('You can not delete origin resource which are mapped with program code!')
        return super(ResourceOrigin, self).unlink()

    def validate_origin_resource(self, origin_resource_string):
        if len(str(origin_resource_string)) > 0:
            origin_resource_str = str(
                origin_resource_string).replace('.', '').zfill(2)
            if origin_resource_str.isnumeric():
                origin_resource = self.search(
                    [('key_origin', '=', origin_resource_str)], limit=1)
                if origin_resource:
                    return origin_resource
        return False
    
    # if not return value is not valid #
    def get_validity_resource_origin(self, resource_origin, today = datetime.now()):
        query = """
                SELECT id
                FROM resource_origin
                WHERE 
                    (
                        (start_date IS NULL AND end_date IS NULL AND key_origin = %(key_origin)s) OR
                        (start_date IS NOT NULL AND end_date IS NULL AND key_origin = %(key_origin)s AND start_date <= %(today)s) OR
                        (start_date IS NOT NULL AND end_date IS NOT NULL AND key_origin = %(key_origin)s AND start_date <= %(today)s AND end_date >= %(today)s)
                    )
                """
        params = {'key_origin' : str(resource_origin),
                  'today': today}
        self.env.cr.execute(query, params)
        resource_origin_validity = self.env.cr.fetchall()

        return resource_origin_validity
