# -*- coding: utf-8 -*-
from datetime import datetime, date
import json
import logging
from odoo import models, fields, api, _
STR_ACC_TOPAY='Account To Pay Reference'
class AccountToPayLine(models.Model):
    _name = 'account.topay.line'

    account_topay_id = fields.Many2one('account.topay',STR_ACC_TOPAY)
    type_provision = fields.Many2one('account.topay.type', related='account_topay_id.type_provision',string=STR_ACC_TOPAY)
    project_pappit = fields.Many2one('project.project', related='account_topay_id.project_pappit',string=STR_ACC_TOPAY, store=False)
    currency_id = fields.Many2one('res.currency', related='account_topay_id.currency_id', string='Currency')
    origin_resource_sel = fields.Many2one('resource.origin', 'Origen Resource')
    domain_origin_resource = fields.Char(compute="_compute_domain_origin_resource", store=False)
    program_code_id = fields.Many2one('program.code','Program Code', index=True)
    domain_program_code = fields.Char(compute="_compute_domain_origin_resource", store=False)

    account_ie = fields.Many2one('association.distribution.ie.accounts', string='Account IE')
    account_id = fields.Many2one('account.account', string='Account')

    def get_domain_dependency(self):
        dep = self.env.user.dependency_ids.ids
        dep += [d.id for d in self.env.user.sub_dependency_ids.dependency_id]
        return json.dumps([('id', 'in', dep)])
    dependency_id = fields.Many2one('dependency', string='Dependency', domain=get_domain_dependency)

    sub_dependency_id = fields.Many2one('sub.dependency', string='Sub Dependency')
    domain_sub_dependency = fields.Char(compute="_compute_domain_sub_dependency", store=False)

    expenditure_item = fields.Many2one('expenditure.item', 'Expenditure Item')
    origin_money = fields.Monetary(string='Origin Money',currency_field='currency_id')
    mx_money = fields.Monetary(string='MX Money',currency_field='currency_id', compute='_compute_origin_money', store=True)
    parity_origin_money = fields.Monetary(string='Paritary Origin Money',currency_field='currency_id', compute='_compute_origin_money', store=True)
    amount_used = fields.Monetary(string='Amount Used',currency_field='currency_id')
    key_origin = fields.Char(related='origin_resource_sel.key_origin',store=False)

    other_parity = fields.Float(string="Other Parity", default=0)


    @api.onchange('key_origin')
    def onchange_key_origin(self):
        self.program_code_id = None
        self.account_id = None
        self.account_ie = None
        self.dependency_id = None
        self.sub_dependency_id = None
        self.expenditure_item = None

    @api.onchange('program_code_id')
    def onchange_program_code(self):
        if self.program_code_id:
            if self.program_code_id.item_id and self.program_code_id.item_id.unam_account_id:
                self.account_id = self.program_code_id.item_id.unam_account_id.id
            self.dependency_id = self.program_code_id.dependency_id
            self.sub_dependency_id = self.program_code_id.sub_dependency_id
    #Obtener origen del la moneda (extranjera o nacional) junto con su cambio de moneda
    @api.depends('origin_money', 'currency_id')
    def _compute_origin_money(self):
        currency_mx = self.env['res.currency'].search([('name','=','MXN')])
        for record in self:
            if record.account_topay_id.from_web_service==False:
                if record.currency_id != currency_mx:
                    record.mx_money = record.currency_id.compute(record.origin_money, currency_mx)
                    record.parity_origin_money = record.currency_id.compute(1, currency_mx)
                else:
                    record.mx_money = record.origin_money
                    record.parity_origin_money = 1
            else:
                record.mx_money = record.origin_money*record.other_parity
                record.parity_origin_money = record.other_parity
    #Obtención de los origenes de recurso
    def _get_origin_resources(self,type_provision):
        origin_resource=[]
        if type_provision.is_allocated_budget_allowed:
            origin_resource.append('00')
        if type_provision.is_nonrecurring_income_allowed:
            origin_resource.append('01')
        if type_provision.is_account_allowed:
            origin_resource.append('05')
        return origin_resource
    
    #Obtener partida de gasto (PAR)
    def _get_items(self,type_provision):
        items=[]
        if type_provision.specific_items_allowed_ids:
            items += type_provision.specific_items_allowed_ids.ids
        if type_provision.item_groups_allowed:
            item_groups = type_provision.item_groups_allowed.split(",")
            query = """
                select id from expenditure_item where substring(item,1,1) in %s
            """
            self._cr.execute(query, (tuple([i[0] for i in item_groups]),))
            res = self.env.cr.fetchall()
            items += [r[0] for r in res]
        return items
    
    @api.depends('type_provision', 'origin_resource_sel', 'project_pappit')
    def _compute_domain_origin_resource(self):
        for record in self:
            # Dominio del Origen del recurso
            type_provision = record.account_topay_id.type_provision
            record.domain_origin_resource = json.dumps([('key_origin', 'in', self._get_origin_resources(type_provision))])
            # Dominio del Código Programático
            # Casos para los que no se debe mostrar el código programático porque los campos estan incompletos
            if (not record.origin_resource_sel) or (type_provision.is_papiit and not record.project_pappit):
                record.domain_program_code = json.dumps([('id', '=', False)])
                continue
            program_code = [('budget_id.state','=','validate')]
            # Año del código programático
            today = date.today()
            year = [str(today.year)]
            # Se valida si el cierre del año fiscal anterior aún no pasa, si es así
            # se muestran también los códigos del años pasado.
            res_company = self.env['res.company'].search([])
            last_day = res_company.fiscalyear_last_day
            last_month = res_company.fiscalyear_last_month
            if last_day and last_month:
                fiscal_year = datetime.strptime(f"{today.year}-{last_month}-{last_day}", "%Y-%m-%d")
                current_date = datetime.strptime(f"{today.year}-{today.month}-{today.day}", "%Y-%m-%d")
                if current_date <= fiscal_year:
                    year.append(str(int(year[0]) - 1))
            program_code.append(('year.name', 'in', year))
            items = self._get_items(type_provision)
            if items:
                program_code.append(('item_id', 'in', items))
            program_code.insert(0, ('resource_origin_id', '=', record.origin_resource_sel.id))
            if type_provision.is_papiit:
                program_code.insert(0, ('project_id', '=', record.project_pappit.id))
            dep = self.env.user.dependency_ids.ids
            sd = self.env.user.sub_dependency_ids.ids
            program_code += ['|', ('dependency_id', 'in', dep), ('sub_dependency_id', 'in', sd)]
            record.domain_program_code = json.dumps(program_code)

    @api.depends('dependency_id')
    def _compute_domain_sub_dependency(self):
        for record in self:
            domain = [('dependency_id', '=', record.dependency_id.id)]
            if record.dependency_id.id not in self.env.user.dependency_ids.ids:
                domain.append(('id', 'in', self.env.user.sub_dependency_ids.ids))
            record.domain_sub_dependency = json.dumps(domain)