import logging
import itertools

from odoo import api, SUPERUSER_ID

_logger = logging.getLogger(__name__)


def migrate(cr, version):
    return
    env = api.Environment(cr, SUPERUSER_ID, {})
    aml_model = env['ir.model'].sudo().search([('model', '=', 'account.move.line')]).id
    account_model = env['ir.model'].sudo().search([('model', '=', 'account.account')]).id
    account_group_model = env['ir.model'].sudo().search([('model', '=', 'account.group')]).id 
    res_partner_model = env['ir.model'].sudo().search([('model', '=', 'res.partner')]).id

    _logger.info("Starting updating related fields for account move lines")

    _logger.info("Creating display names in ir_model_fields (PRE)")

    cr.execute("select id from ir_model_fields where name='excel_display_name' and model='account.account'")
    if not cr.fetchall():
        cr.execute("""insert into ir_model_fields 
            (model, model_id, name, field_description, ttype, store, state)
            values ('account.account', %s, 'excel_display_name', 'Excel Display Name', 'char', 't', 'manual')""" % (account_model))
    
    cr.execute("select id from ir_model_fields where name='excel_display_name' and model='account.group'")
    if not cr.fetchall():
        cr.execute("""insert into ir_model_fields 
            (model, model_id, name, field_description, ttype, store, state)
            values ('account.group', %s, 'excel_display_name', 'Excel Display Name', 'char', 't', 'manual')""" % (account_group_model))

    cr.execute("select id from ir_model_fields where name='excel_display_name' and model='res.partner'")
    if not cr.fetchall():
        cr.execute("""insert into ir_model_fields 
            (model, model_id, name, field_description, ttype, store, state)
            values ('res.partner', %s, 'excel_display_name', 'Excel Display Name', 'char', 't', 'manual')""" % (res_partner_model))
    

    _logger.info("Started setting display names for models (PRE)")

    cr.execute("update account_account set excel_display_name=code || ' ' || name where name is not null")
    cr.execute("update account_group set excel_display_name=code_prefix || '-' || name where name is not null")
    cr.execute("update res_partner set excel_display_name=name where name is not null")
    
    cr.execute("alter table account_move_line add column if not exists unam_account_id int")
    cr.execute("alter table account_move_line add column if not exists provider_id int")

    cr.execute("select id from ir_model_fields where name='provider_id' and model='account.move.line'")
    if not cr.fetchall():
        cr.execute("""insert into ir_model_fields 
            (model, model_id, name, field_description, ttype, relation, store, related, state)
            values ('account.move.line', %s, 'provider_id', 'Provider', 'many2one', 'res.partner', 't', 'partner_id', 'manual')""" % (aml_model))
    
    cr.execute("select id from ir_model_fields where name='unam_account_id' and model='account.move.line'")
    if not cr.fetchall():
        cr.execute("""insert into ir_model_fields
            (model, model_id, name, field_description, ttype, relation, store, related, state)
            values ('account.move.line', %s, 'unam_account_id', 'UNAM Account', 'many2one', 'account.account', 't', 'account_id', 'manual')""" % (aml_model))

    cr.execute("update account_move_line as aml set unam_account_id=aml.account_id, provider_id=aml.partner_id")

    _logger.info("Finished updating related fields for account move lines")
