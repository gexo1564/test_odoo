# -*- coding: utf-8 -*-
from email.policy import default
import logging
from odoo import models, fields, api, _
from lxml import etree
from odoo.exceptions import ValidationError, UserError


class AccountMove(models.Model):
    _inherit = 'account.move'

    @api.model
    def fields_get(self, fields=None, attributes=None):
        no_selectable_fields = ['l10n_mx_edi_usage','key_prod_serv','dependence_id','budget_standardization_id','payment_method','date_status_conta_cfd',
                                'is_tax_closing','income_invoice_billing','foreign_currency_amount','id','invoice_incoterm_id','is_income','leaves','l10n_mx_closing_move',
                                'legal_number','is_hide_provision_from_view','template1','template2','template3','template4','template5','template6',
                                'policy_cancellation_number','policy_date_cfd','policy_date_cfd_canc','policy_number','receipt_date','sub_depend_id','is_recognized',
                                'total_provision_move','is_provision_request_generate','status_conta_cfd','type_of_changes','collection_type','is_show_destination',
                                'is_show_origin','sub_origin_resource_name','template1_sender_professional_title','template2_recipient_professional_title',
                                'template2_sender_professional_title','template3_recipient_professional_title','template3_sender_professional_title',
                                'template4_recipient_professional_title','template4_sender_professional_title','template5_recipient_professional_title',
                                'template5_sender_professional_title','template6_recipient_professional_title','template6_sender_professional_title',
                                'template1_recipient_professional_title','template1_recipient_title','template6_recipient_title','template5_sender_title',
                                'template6_sender_title','template1_sender_title','template2_recipient_title','template2_sender_title','template3_recipient_title',
                                'template3_sender_title','template4_recipient_title','template4_sender_title','template5_recipient_title',
                                'programatic_code','excercise','manual_rfc','status','related_cfdi','income_payment_ref','income_activity']       #lista de filtrar por
        no_sortable_field = ['l10n_mx_edi_usage','key_prod_serv','dependence_id','budget_standardization_id','payment_method','date_status_conta_cfd',
                                'is_tax_closing','income_invoice_billing','foreign_currency_amount','id','invoice_incoterm_id','is_income','leaves','l10n_mx_closing_move',
                                'legal_number','is_hide_provision_from_view','template1','template2','template3','template4','template5','template6',
                                'policy_cancellation_number','policy_date_cfd','policy_date_cfd_canc','policy_number','receipt_date','sub_depend_id','is_recognized',
                                'total_provision_move','is_provision_request_generate','status_conta_cfd','type_of_changes','collection_type','is_show_destination',
                                'is_show_origin','sub_origin_resource_name','template1_sender_professional_title','template2_recipient_professional_title',
                                'template2_sender_professional_title','template3_recipient_professional_title','template3_sender_professional_title',
                                'template4_recipient_professional_title','template4_sender_professional_title','template5_recipient_professional_title',
                                'template5_sender_professional_title','template6_recipient_professional_title','template6_sender_professional_title',
                                'template1_recipient_professional_title','template1_recipient_title','template6_recipient_title','template5_sender_title',
                                'template6_sender_title','template1_sender_title','template2_recipient_title','template2_sender_title','template3_recipient_title',
                                'template3_sender_title','template4_recipient_title','template4_sender_title','template5_recipient_title',
                                'programatic_code','excercise','manual_rfc','status','related_cfdi','income_payment_ref','income_activity']        #lista de agrupar por

        res = super(AccountMove, self).fields_get()
        for key, value in res.items():
            if key in no_selectable_fields:
                value.update({'selectable': False})

        for key, value in res.items():
            if key in no_sortable_field:
                value.update({'sortable': False})
        return res

      #- Método que modifica el css de la vista en Certificados de deposito
    x_css = fields.Html(string='CSS', sanitize=False, compute='_compute_css', store=False)
    @api.depends('state')
    def _compute_css(self):
        is_cd_user_finanzas = self.env.user.has_group('jt_income.group_income_cd_finance_user')
        is_cd_user_contabilidad = self.env.user.has_group('jt_income.group_income_cd_accounting_user')
        is_cd_administrator = self.env.user.has_group('jt_income.group_income_cd_administrator')
        is_group_income_bi_guest_user = self.env.user.has_group('jt_income.group_income_bi_guest_user')
    
        for record in self:
            record.x_css = ""
            
            if (is_cd_user_finanzas or  is_cd_user_contabilidad or is_cd_administrator or is_group_income_bi_guest_user):
                #Ocultar boton de editar para todos los grupos de certificados de deposito cuando se encuentra publicado
                if record.state in ('posted'):
                    record.x_css += """
                        <style>
                            .o_form_view .o_form_statusbar > .o_statusbar_status{
                            max-width:85%;
                            }
                            .o_form_button_edit {
                                display: none !important;
                            }

                        </style>
                    """
                    if is_cd_administrator:
                            record.x_css += """
                                <style>
                                    .o_form_view .o_form_statusbar > .o_statusbar_status{
                                    max-width:85%;
                                    }
                                    button[name="197"] {
                                        display: none !important;
                                    }
                                </style>
                            """   
                    # Ocultar botones solo usuario de cd usuario finanzas, el campo "name 197" es para ASIENTO DE REVERSIÓN
                    if is_cd_user_contabilidad:
                            record.x_css += """
                                <style>
                                    .o_form_view .o_form_statusbar > .o_statusbar_status{
                                    max-width:85%;
                                    }
                                    button[name="button_draft"] {
                                        display: none !important;
                                    }
                                </style>
                           """
                    # Ocultar botones solo usuario de cd usuario finanzas
                    if is_cd_user_finanzas:
                            record.x_css += """
                                <style>
                                    .o_form_view .o_form_statusbar > .o_statusbar_status{
                                    max-width:85%;
                                    }
                                    button[name="197"] {
                                        display: none !important;
                                    }
                                    button[name="button_draft"] {
                                        display: none !important;
                                    }
                                </style>
                            """
                    # Solo de consulta intereses bancarios   
                    if not is_cd_user_contabilidad:     
                        if is_group_income_bi_guest_user:
                                record.x_css += """
                                    <style>
                                        .o_form_view .o_form_statusbar > .o_statusbar_status{
                                        max-width:85%;
                                        }
                                        button[name="197"] {
                                            display: none !important;
                                        }
                                    </style>
                                """        

            # Ocultar botones para certificados de deposito cuando esta  cancelado     
            if (is_cd_user_finanzas or is_cd_user_contabilidad):
                if record.state in ('cancel'):
                    record.x_css = """
                        <style>
                            .o_form_view .o_form_statusbar > .o_statusbar_status{
                            max-width:85%;
                            }
                            .o_form_button_edit {
                                display: none !important;
                            }
                            button[name="button_draft"] {
                                display: none !important;
                            }

                        </style>
                    """
                    # Ocultar botones en estado cancelado para solo usuario de cd usuario finanzas
                    if is_cd_user_finanzas:
                            record.x_css += """
                                <style>
                                    .o_form_view .o_form_statusbar > .o_statusbar_status{
                                    max-width:85%;
                                    }
                                    .o_form_button_create {
                                        display: none !important;
                                    }
                                    .o_form_button_edit {
                                        display: none !important;
                                    }
                                </style>
                            """

            # En cualquiera de los estados, se limita el tamaño del statusbar para evitar tener 
            # 'botones gordos'
            else:
                record.x_css += """
                    <style>
                        .o_form_view .o_form_statusbar > .o_statusbar_status{
                            max-width:85%;
                        }
                    </style>
                """

    # Facturacion digital
    is_group_income_fac_administrator_user = fields.Boolean(compute="_compute_is_group_income_fac_administrator_user")
    is_group_income_fac_finance_user = fields.Boolean(compute="_compute_is_group_income_fac_finance_user")
    is_group_income_fac_accounting_user = fields.Boolean(compute="_compute_is_group_income_fac_accounting_user")
    x_css_fac = fields.Html(string='CSS', sanitize=False, compute='_compute_css_fac', store=False)

    @api.depends('is_group_income_fac_administrator_user')
    def _compute_is_group_income_fac_administrator_user(self):
        self.is_group_income_fac_administrator_user = self.env.user.has_group('jt_income.group_income_fac_administrator_user')

    @api.depends('is_group_income_fac_finance_user')
    def _compute_is_group_income_fac_finance_user(self):
        self.is_group_income_fac_finance_user = self.env.user.has_group('jt_income.group_income_fac_finance_user')
   
    @api.depends('is_group_income_fac_accounting_user')
    def _compute_is_group_income_fac_accounting_user(self):
        self.is_group_income_fac_accounting_user = self.env.user.has_group('jt_income.group_income_fac_accounting_user')        

    @api.depends('is_group_income_fac_administrator_user','is_group_income_fac_finance_user','is_group_income_fac_accounting_user')
    def _compute_css_fac(self):
        is_group_income_fac_accounting_user = self.env.user.has_group('jt_income.group_income_fac_accounting_user')  
        is_group_income_fac_finance_user = self.env.user.has_group('jt_income.group_income_fac_finance_user')
        is_group_income_fac_administrator_user = self.env.user.has_group('jt_income.group_income_fac_administrator_user')
        for record in self:
            record.x_css_fac = ""
            if is_group_income_fac_accounting_user or  is_group_income_fac_finance_user or is_group_income_fac_administrator_user:
                record.x_css_fac += """
                    <style>
                        .o_form_view .o_form_statusbar > .o_statusbar_status{
                        max-width:85%;
                        }
                        button[name="action_reverse"] {
                            display: none !important;
                        }


                    </style>
                """
                if record.type_of_revenue_collection == 'billing':
                    record.x_css_fac += """
                                        <style>
                                            .o_form_view .o_form_statusbar > .o_statusbar_status{
                                            max-width:85%;
                                            }
                                            button[name="button_draft"] {
                                                display: none !important;
                                            }

                                        </style>
                                    """
            else:
                record.x_css_fac += """
                    <style>
                        .o_form_view .o_form_statusbar > .o_statusbar_status{
                            max-width:85%;
                        }
                    </style>
                """

    # Sobrescritura de la funcion previsualizar para asignar permisos
    def preview_invoice(self):
        allowed_groups = ['jt_income.group_income_admin_user',]
        
        if not any(self.env.user.has_group(group) for group in allowed_groups):
            raise UserError(_("You aren't allowed to perform this action."))

        res = super(AccountMove, self).preview_invoice()
        
        return res
    
    # Sobrescritura de la funcion enviar e imprimir
    def action_invoice_sent(self):
        allowed_groups = ['jt_income.group_income_admin_user',]
        
        if not any(self.env.user.has_group(group) for group in allowed_groups):
            raise UserError(_("You aren't allowed to perform this action."))

        res = super(AccountMove, self).action_invoice_sent()
        
        return res                           
