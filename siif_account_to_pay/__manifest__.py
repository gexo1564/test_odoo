{
    'name': 'SIIF Account Payable',
    'summary': 'SIIF Account Payable is used to improve functional requirements',
    'version': '13.0.0.1.1',
    'category': 'Accounting',
    'author': 'SIIF',
    'maintainer': 'SIIF',
    'license': 'AGPL-3',
    'depends': ['jt_budget_mgmt', 'jt_supplier_payment', 'siif_account_design','mail'],
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        #Vistas
        'views/res_users_dep_sd.xml',
        'views/account_move.xml',
        'views/account_topay_configuration_views.xml',
        'views/account_topay.xml',
        'views/account_topay_menu.xml',
        'views/account_to_pay_parameters.xml',
        'views/permission_users_to_type_of_account_payable.xml',
        'views/permission_type_of_account_payable_to_users.xml',
        'views/upa_permissions_views.xml',
        'views/user_upa_permissions_views.xml',
        'views/invoice_inh.xml',
        'views/menus.xml',
        'views/pay_handbooks.xml',
        
        #data
        'data/sequence.xml',
        
        #Wizard
        'wizard/account_topay_rejection.xml',
        'wizard/account_topay_documents.xml',
        'wizard/account_topay_approve.xml',
        'wizard/import_account_to_pay_lines.xml',
        'wizard/update_documents_account_to_pay.xml',
        ],
    'demo': [

    ],
    'application': True,
    'installable': True,
    'auto_install': False,
}
