odoo.define('siff.BasicView', function (require) {
  "use strict";

  let BasicView = require('web.BasicView')

  BasicView.include({
    init: function(viewInfo, params) {
      this._super.apply(this, arguments)
      if(!this.controllerParams.activeActions.edit)
        this.controllerParams.archiveEnabled = 'False' in viewInfo.fields;
    },
  });
});