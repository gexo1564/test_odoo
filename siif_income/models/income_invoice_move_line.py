from odoo import models, fields, api,_
from babel.dates import format_datetime, format_date

from odoo.exceptions import RedirectWarning, UserError, ValidationError, AccessError
from odoo.tools import float_is_zero, float_compare, safe_eval, date_utils, email_split, email_escape_char, email_re
from odoo.tools.misc import formatLang, format_date, get_lang

from datetime import date, timedelta
from itertools import groupby
from itertools import zip_longest
from hashlib import sha256
from json import dumps

import json
import re

class IncomeIncomeMoveLine(models.Model):

    _inherit = 'account.move'

    pys_id = fields.Many2one('product.template', string='PyS')
    camp = fields.Char('Elemento')
