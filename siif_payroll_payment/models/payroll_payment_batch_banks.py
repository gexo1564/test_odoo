# -*- coding: utf-8 -*-
from odoo import api, models, fields, _

class PayrollPaymentBatchBank(models.Model):

    _name = 'payroll.payment.batch.bank'

    payroll_processing_id = fields.Many2one('custom.payroll.processing','Payroll Processing')

    payment_type = fields.Selection(
        [
            ('salary_and_benefits', 'Salary and Benefits'),
            ('alimony', 'Alimony')
        ], string='Payment Type'
    )

    bank = fields.Char('Bank')

    _sql_constraints = [(
        'payroll_payment_batch_bank_unique',
        'unique(payroll_processing_id, payment_type, bank)',
        _('Only one batch can be generated for each bank that receives the payment.')
    )]


    def _get_unpaid_banks_salary_and_benfits(self, payroll_processing_id):
        query = """
            select distinct bank_key_name
            from employee_payroll_file
            where casualties_and_cancellations is null
            and l10n_mx_edi_payment_method_id = %s
            and payroll_processing_id = %s
            except
            select bank from payroll_payment_batch_bank
            where payroll_processing_id = %s
            and payment_type = 'salary_and_benefits'
        """
        self.env.cr.execute(query, (
            self.env.ref('l10n_mx_edi.payment_method_transferencia').id,
            payroll_processing_id.id,
            payroll_processing_id.id,
        ))
        return [r[0] for r in self.env.cr.fetchall()]

    def _get_unpaid_banks_alimony(self, payroll_processing_id):
        query = """
            select distinct b.name
            from pension_payment_line pe, employee_payroll_file r, res_bank b
            where pe.payroll_id = r.id
            and pe.bank_id = b.id
            and r.casualties_and_cancellations is null
            and pe.l10n_mx_edi_payment_method_id = %s
            and r.payroll_processing_id = %s
            except
            select bank from payroll_payment_batch_bank
            where payroll_processing_id = %s
            and payment_type = 'alimony'
        """
        self.env.cr.execute(query, (
            self.env.ref('l10n_mx_edi.payment_method_transferencia').id,
            payroll_processing_id.id,
            payroll_processing_id.id,
        ))
        return [r[0] for r in self.env.cr.fetchall()]

    def get_unpaid_banks(self, payment_type, payroll_processing_id):
        if payment_type == 'salary_and_benefits':
            return self._get_unpaid_banks_salary_and_benfits(payroll_processing_id)
        elif payment_type == 'alimony':
            return self._get_unpaid_banks_alimony(payroll_processing_id)

    def get_unpaid_banks_ids(self, payment_type, payroll_processing_id):
        banks = self.get_unpaid_banks(payment_type, payroll_processing_id)
        if banks:
            self.env.cr.execute("select id from res_bank where name in %s", (tuple(banks),))
            return [r[0] for r in self.env.cr.fetchall()]
        return []

    def update_payment_generation(self, payroll_processing_id, payment_type, banks):
        for bank in banks:
            self.create({
                'payroll_processing_id': payroll_processing_id.id,
                'payment_type': payment_type,
                'bank': bank
            })