from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError



class RelationAnnexAccountGroup(models.Model):
    _name = 'relation.annex.account.group'
    _description = "Relation Annex Account Group"
    _inherit = ['mail.thread', 'mail.activity.mixin']


    key = fields.Char(string='Key', required=True)
    group_name = fields.Char(string='Group name', required=True)
    accounts_ids = fields.Many2many('account.account', string='Annex Account Groups')

    _sql_constraints = [
        ('group_name_uniq', 'unique(group_name)', _('Group name must be unique'))]