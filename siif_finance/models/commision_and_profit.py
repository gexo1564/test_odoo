from odoo import models, fields, api,_
from datetime import datetime, timedelta
from odoo.exceptions import ValidationError
import math
import logging
from lxml import etree

# Define constant
AFFECTATION_INCOME_BUDGET_MODEL = 'affectation.income.budget'

class CommisionAndProfit(models.Model):
    
    _inherit = 'comission.profit'
    
    record_number = fields.Char('Record Number')
    account_id = fields.Many2one('account.account','Account')
    pay_journal = fields.Many2one('account.journal', string='Payment Journal')
    comission_id =  fields.Many2one('comission.settings.finance', string='Comission setting')
    apportionment_line_ids = fields.One2many('apportionment', 'comission_profit_id', string="Apportionment")
    iva_amount = fields.Float('IVA Amount')
    is_apportionment = fields.Boolean('Is apportionment')
    movement_date = fields.Date('Movement date')
    settlement_date = fields.Date('Settlement date')
    system_folio =  fields.Char('Folio of system')
    bank_branch =  fields.Char('Bank branch')
    ref = fields.Char('Bank reference')
    customer_ref = fields.Char('Customer reference')
    reference_plugin = fields.Char('Plugin reference')
    reference_plugin_2 = fields.Char('Plugin reference 2')
    reference_plugin_3 = fields.Char('Plugin reference 3')
    dependency_id = fields.Many2one('dependency','Dependency')
    sub_dependency_id = fields.Many2one('sub.dependency', 'Sub Dependency')
    siiof_folio = fields.Char("SIIOF folio")
    id_movto_ban = fields.Char("ID bank movement")
    type_registry_spent_related = fields.Selection(related="comission_id.type_registry_spent", string="Type Registry Spent", store=True)
    type_authorized_budget_related = fields.Selection(related="comission_id.type_authorized_budget", string="Type Autorized Budget", store=True)
    concept_related = fields.Many2one(related="comission_id.concept_id", string="Concept", store=True)
    comission_type_related = fields.Char(related="comission_id.comission_type", string="Comission type configurated", store=True)
    currency_related = fields.Char(related="journal_id.currency_id.name", string="Currency")
    observations_validation =  fields.Char("Observations validation")
    me_amount = fields.Float("ME amount")
    bank = fields.Many2one(related="journal_id.bank_account_id.bank_id",string="Bank", store=True)

    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        res = super().fields_view_get(view_id=view_id,view_type=view_type,toolbar=toolbar,submenu=submenu)
        doc = etree.XML(res['arch'])
        is_group_approval_user = self.env.user.has_group('jt_payroll_payment.group_approval_user')
        is_group_consult_user = self.env.user.has_group('jt_payroll_payment.group_consult_user')

        if is_group_approval_user or is_group_consult_user:
            for node in doc.xpath("//" + view_type):
                node.set('export','0')
                node.set('export_xlsx','0')

        res['arch'] = etree.tostring(doc)
        return res

    @api.onchange('journal_id','description')
    def set_comission_id(self):
        if self.journal_id and self.description and self.description!='' and not self.dependency_id and not self.sub_dependency_id:
            comission = self.env['comission.settings.finance'].search([('income_bank_journal_id','=',self.journal_id.id),
                                ('comission_description_mov','=',self.description)])
            if comission:
                self.comission_id = comission.id
            else:
                self.comission_id = False
        elif self.journal_id and self.description and self.description!='' and self.dependency_id and self.sub_dependency_id:
            comission = self.env['comission.settings.finance'].search([('income_bank_journal_id','=',self.journal_id.id),
                                ('comission_description_mov','=',self.description),('dependency_id', '=', self.dependency_id.id),('sub_dependency_id', '=', self.sub_dependency_id.id)])
            if comission:
                self.comission_id = comission.id
            else:
                self.comission_id = False

    @api.onchange('amount')
    def set_iva_amount(self):
        if self.amount>0.0:
            amount = self.amount*0.16
            aux = math.floor(amount * 10 ** 2) / 10 ** 2
            self.iva_amount = float(aux)

    @api.onchange('status')
    def set_observations(self):
        if self.status == 'approved':
            self.observations_validation = "Aprobado"

    @api.onchange('comission_id')
    def set_pay_journal(self):
        if self.comission_id and self.comission_id.type_of_currency=='foreign':
           pay_journal_id =  self.env['payment.journals'].search([('type_of_currency','=', 'foreign')])
           self.pay_journal = pay_journal_id['journal_id'].id
        elif self.comission_id and self.comission_id.type_of_currency=='national':
           pay_journal_id =  self.env['payment.journals'].search([('type_of_currency','=', 'national')])
           self.pay_journal = pay_journal_id['journal_id'].id

    def avalability_for_several_program_code(self,program_code_tuple, date, order_by_availability):

        if len(program_code_tuple)>1:
            if order_by_availability==True:
                self.env.cr.execute(f"""select bl.id, program_code_id, program_code, start_date, end_date, assigned, available, authorized, budget_line_yearly_id
                    from expenditure_budget_line bl, program_code p
                    where bl.program_code_id in {str(program_code_tuple).replace('[','(').replace(']',')')}
                    and assigned is not null and %s>=start_date and program_code = 'SUMAS' and available is not null and available != 0.0
                    union
                    select 1, program_code_id, 'SUMAS' as program_code, now(), now(), sum(assigned), sum(available), sum(authorized), 1
                    from expenditure_budget_line bl, program_code p
                    where bl.program_code_id = p.id and p.id in {str(program_code_tuple).replace('[','(').replace(']',')')} and assigned is not null and available != 0.0 and %s>=start_date
                    group by program_code, bl.program_code_id
                    order by program_code, start_date, end_date, available DESC""",(date,date))
            else:
                self.env.cr.execute(f"""select bl.id, program_code_id, program_code, start_date, end_date, assigned, available, authorized, budget_line_yearly_id
                    from expenditure_budget_line bl, program_code p
                    where bl.program_code_id in {str(program_code_tuple).replace('[','(').replace(']',')')}
                    and assigned is not null and %s>=start_date and program_code = 'SUMAS' and available is not null and available != 0.0
                    union
                    select 1, program_code_id, 'SUMAS' as program_code, now(), now(), sum(assigned), sum(available), sum(authorized), 1
                    from expenditure_budget_line bl, program_code p
                    where bl.program_code_id = p.id and p.id in {str(program_code_tuple).replace('[','(').replace(']',')')} and assigned is not null and available != 0.0 and %s>=start_date
                    group by program_code, bl.program_code_id
                    order by program_code, start_date, end_date""",(date,date))

            origin_datas = self.env.cr.fetchall()
        else:
            self.env.cr.execute("""select bl.id, program_code_id, program_code, start_date, end_date, assigned, available, authorized, budget_line_yearly_id
                    from expenditure_budget_line bl, program_code p
                    where bl.program_code_id = %s
                    and assigned is not null and %s>=start_date and program_code = 'SUMAS'
                    union
                    select 1, program_code_id, 'SUMAS' as program_code, now(), now(), sum(assigned), sum(available), sum(authorized), 1
                    from expenditure_budget_line bl, program_code p
                    where bl.program_code_id = p.id and p.id = %s 
                    and assigned is not null and %s>=start_date
                    group by program_code, bl.program_code_id
                    order by program_code, start_date, end_date""",(program_code_tuple[0],date,program_code_tuple[0],date))

            origin_datas = self.env.cr.fetchall()
        '''select bl.id, program_code_id, program_code, start_date, end_date, assigned, available, authorized, budget_line_yearly_id
            from expenditure_budget_line bl, program_code p
            where bl.program_code_id = p.id and p.id = %s
            and assigned is not null and %s>=start_date and program_code = 'SUMAS'
            union
            select 1, program_code_id, 'SUMAS' as program_code, now(), now(), sum(assigned), sum(available), sum(authorized), 1
            from expenditure_budget_line bl, program_code p
            where bl.program_code_id = p.id and p.id = %s 
            and assigned is not null and %s>=start_date
            group by program_code, bl.program_code_id
            order by program_code, start_date, end_date'''


        return origin_datas

    def compute_ledger_account(self,account_id,dependency_id,sub_dependency_id, amount):
        if dependency_id != None and sub_dependency_id != None:  
            self.env.cr.execute('''select
                                coalesce(SUM(debit)) as debit,
                                coalesce(SUM(credit)) as credit
                            from
                                account_move_line
                            where
                                account_id = %s
                                and dependency_id = %s
                                and sub_dependency_id = %s
                                and parent_state = 'posted';

                                    ''', (account_id.id, dependency_id.id, sub_dependency_id.id))
        else:
            self.env.cr.execute('''select
                                coalesce(SUM(debit)) as debit,
                                coalesce(SUM(credit)) as credit
                            from
                                account_move_line
                            where
                                account_id = %s
                                and parent_state = 'posted';

                                    '''%(account_id.id))


        resultado = self.env.cr.dictfetchall()

        if (resultado[0]['credit'] is None or resultado[0]['debit'] is None):
            if dependency_id != None and sub_dependency_id != None:
                raise ValidationError(_(
                    "No hay saldo para la cuenta contable: %s \nDependencia: %s \nSubdependencia: %s") %
                                      (account_id.code,
                                       dependency_id.dependency,
                                       sub_dependency_id.sub_dependency))
            else:
                raise ValidationError(_(
                    "No hay saldo para la cuenta contable: %s") %
                                      (account_id.code))


        debit = 0.0 if resultado[0]['debit'] is None else resultado[0]['debit']
        credit = 0.0 if resultado[0]['credit'] is None else resultado[0]['credit']

        saldo = credit - debit


        if (saldo >= amount):
            return True
        else:
            if dependency_id != None and sub_dependency_id != None:
                raise ValidationError(_(
                    "No hay suficiencia para IEMN \n Cuenta contable: %s  \n Dependencia: %s \n Subdependencia: %s \n\n Saldo de la cuenta: %s \n Monto egreso: %s") %
                                      (account_id.code,
                                       dependency_id.dependency,
                                       sub_dependency_id.sub_dependency,
                                       saldo,
                                       amount))
            else:
                raise ValidationError(_(
                    "No hay saldo para la cuenta contable: %s") %
                                      (account_id.code))

    def get_available_by_account(self, account_id, dependency_id, sub_dependency_id):
        if dependency_id and sub_dependency_id:
            self.env.cr.execute('''select
                                coalesce(SUM(debit)) as debit,
                                coalesce(SUM(credit)) as credit
                            from
                                account_move_line
                            where
                                account_id = %s
                                and dependency_id = %s
                                and sub_dependency_id = %s
                                and parent_state = 'posted';
                                ''', (account_id.id, dependency_id.id, sub_dependency_id.id))
        else:
            self.env.cr.execute('''select
                                coalesce(SUM(debit)) as debit,
                                coalesce(SUM(credit)) as credit
                            from
                                account_move_line
                            where
                                account_id = %s
                                and parent_state = 'posted';
                                    '''%(account_id.id))

        resultado = self.env.cr.dictfetchall()
        debit = 0.0 if resultado[0]['debit'] is None else resultado[0]['debit']
        credit = 0.0 if resultado[0]['credit'] is None else resultado[0]['credit']
        saldo = credit - debit
        return saldo


    def compute_apportionment(self, record_date):
        for record in self:
            if record.comission_id.type_registry_spent == 'authorized_budget':
                program_code_list=[]
                program_code_decrease_budget=[]
                exclude_program_code_list=[]
                budget_total = 0.0
                residual_amount = 0.0
                index = 0.0
                total_amount = record.amount+record.iva_amount
                if record.comission_id.type_authorized_budget=='one_program_code':
                    for line in record.comission_id.comission_line_ids:
                        program_code_list.append(line.program_code_id.id)
                else:
                    year =  self.env['year.configuration'].search([('name','=', record_date.year)])[0]
                    exclude_program_code =  self.env['exclude.program.code'].search([('program_code_id','!=', None)])
                    for program in exclude_program_code:
                        exclude_program_code_list.append(program['program_code_id'].id)

                    program_code_list = self.env['program.code'].search([('item_id','=',record.comission_id.item_key_id.id),('year','=',year['id']),('id','not in',exclude_program_code_list)]).ids

                program_code_tuple = program_code_list

                if record.comission_id.type_authorized_budget=='availability':
                    origin_datas=record.avalability_for_several_program_code(program_code_tuple, record_date, True)
                else:
                    origin_datas=record.avalability_for_several_program_code(program_code_tuple, record_date, False)

                program_code_list=[]

                for data in origin_datas:
                    if data[6]!=None:   
                        budget_total += data[6]

                if budget_total<total_amount:
                    record.observations_validation = "El disponible total (" + str(budget_total) +") es menor al monto de la comisión (" + str(total_amount) +")"
                    self.payment_without_budget()
                    return False
                    


                data_long = len(origin_datas)

                if record.comission_id.type_authorized_budget=='availability':
                    residual_amount = total_amount
                    for data in origin_datas:
                        if data[6]<=residual_amount and residual_amount > 0.0:
                            spent=data[6]
                            residual_amount -=  spent
                        else:
                            spent=residual_amount
                            residual_amount -= spent

                        program_code_vals = {
                            'program_code_id': data[1],
                            'available_date': data[6],
                            'available_percentage': 0.0,
                            'spent': spent,
                            'comission_profit_id': record.id,
                            'residual_amount': residual_amount,
                        }
                        program_code_list.append(program_code_vals)
                        program_code_decrease_budget.append((data[1],spent))

                elif record.comission_id.type_authorized_budget=='one_program_code' or record.comission_id.type_authorized_budget=='apportionment': 
                    residual_amount = total_amount
                    for data in origin_datas:
                        if residual_amount > 0.0:
                            residual_amount -=  round(((record.amount+record.iva_amount)*(data[6]/budget_total)),2)
                        spent = (record.amount+record.iva_amount)*(data[6]/budget_total)
                        if data[6]!=None and data[6] > 0.0 and index<data_long-1 and residual_amount>0.0: 
                            program_code_vals = {
                                'program_code_id': data[1],
                                'available_date': data[6],
                                'available_percentage': (data[6]/budget_total)*100,
                                'spent': spent,
                                'comission_profit_id': record.id

                            }
                            program_code_list.append(program_code_vals)
                            program_code_decrease_budget.append((data[1],spent))
                            index += 1
                        else:
                            program_code_vals = {
                                'program_code_id': data[1],
                                'available_date': data[6],
                                'available_percentage': (data[6]/budget_total)*100,
                                'spent': spent+residual_amount,
                                'comission_profit_id': record.id

                            }
                            program_code_list.append(program_code_vals)
                            program_code_decrease_budget.append((data[1],spent+residual_amount))
                            index += 1
                            residual_amount-=residual_amount


                #self.apportionment_line_ids = program_code_list

                obj = record.env['apportionment']
                program_records = obj.create(program_code_list)

                record.env['expenditure.budget.line'].update_avalability_for_several_program_code(program_code_decrease_budget, record_date)
            elif record.comission_id.type_registry_spent == 'ledger_account':
                amount= record.amount+record.iva_amount
                if record.comission_id.dependency_id and record.comission_id.sub_dependency_id:
                    record.compute_ledger_account(record.comission_id.account_id,record.comission_id.dependency_id,record.comission_id.sub_dependency_id,amount)
                else:
                    record.compute_ledger_account(record.comission_id.account_id,None,None,amount)

            elif record.comission_id.type_registry_spent == 'extra_income':
                amount= record.amount+record.iva_amount
                if record.comission_id.dependency_id and record.comission_id.sub_dependency_id:
                    record.compute_ledger_account(record.comission_id.afectation_account_id.ie_account_line_ids.filtered(lambda x:x.main_account == True).account_id,record.comission_id.dependency_id,record.comission_id.sub_dependency_id,amount)
                else:
                    record.compute_ledger_account(record.comission_id.afectation_account_id.ie_account_line_ids.filtered(lambda x:x.main_account == True).account_id,None,None,amount)
                
                program_code_list=[]
                program_code_decrease_budget=[]
                budget_total = 0.0
                total_amount = record.amount+record.iva_amount
                
                for line in record.comission_id.comission_line_ids:
                    program_code_list.append(line.program_code_id.id)

                program_code_tuple = program_code_list
                origin_datas=record.avalability_for_several_program_code(program_code_tuple, record_date, False)

                for data in origin_datas:
                    if data[6]!=None:   
                        budget_total += data[6]

                if budget_total<total_amount:
                    record.observations_validation = "El disponible total (" + str(budget_total) +") es menor al monto de la comisión (" + str(total_amount) +")"
                    self.payment_without_budget()
                    return False

                program_code_decrease_budget.append((data[1],total_amount))

                record.env['expenditure.budget.line'].update_avalability_for_several_program_code(program_code_decrease_budget, record_date)

        return True

    def apportionment(self):
        today = self.movement_date
        flag = self.compute_apportionment(today)
        if flag:
            self.is_apportionment=True
            self.status = 'available'
        else:
            self.is_apportionment=False


    def payment_without_budget(self):

        for record in self:
            record.status = 'pay_budget'

            journal = record.journal_id
            provisional_account_id = self.env['provisional.account.comission'].search([('account_id','!=', None)]).account_id.id
            journal_account_id = journal.default_credit_account_id.id
            journal_conac_account = journal.conac_credit_account_id.id

            today = datetime.today().date()
            user = self.env.user
            partner_id = user.partner_id.id
            amount = record.amount

            if not provisional_account_id:
                if self.env.user.lang == 'es_MX':
                    raise ValidationError(_("Por favor configure la cuenta contable provisional de las comisiones en la configuración del módulo"))
                else:
                    raise ValidationError(_("Please configure the provisional account comission in settings"))

            if not record.move_line_ids_budget:
                line_vals = []
                program_line_vals = []
                line_vals = {
                             'account_id': journal_account_id,
                             'coa_conac_id': journal_conac_account,
                             'credit': amount+record.iva_amount,
                             'partner_id': partner_id,
                             'name': record.concept_related.concept,
                             'comission_paid_budget': record.id,
                             'egress_key_id': record.comission_id.egress_key_id.id,
                             
                             }
                program_line_vals.append((0,0,line_vals,{'context':{'check_move_validity': False}}))


                line_vals = {
                             'account_id': provisional_account_id,
                             #'coa_conac_id': journal_conac_account,
                             'debit': record.iva_amount+amount,
                             'partner_id': partner_id,
                             'name': record.concept_related.concept,
                             'comission_paid_budget': record.id,
                             'egress_key_id': record.comission_id.egress_key_id.id,
                             
                             }
                program_line_vals.append((0,0,line_vals,{'context':{'check_move_validity': False}}))

                unam_move_val = {'ref': self.ref, 'conac_move': True, 'type': 'entry', 'type_of_registry_payment': 'comission', 
                                 'date': record.movement_date, 'journal_id': journal.id, 'company_id': self.env.user.company_id.id,
                                 'line_ids': program_line_vals}


                move_obj = self.env['account.move']
                unam_move = move_obj.create(unam_move_val).with_context(check_move_validity=False)
                unam_move.action_post()




    def approve(self):
        cri = False
        for record in self:
            today = datetime.today().date()
            if record.move_line_ids_budget:
                account_date = today
            else:
                account_date = record.movement_date

            record.status = 'approved'
            if record.journal_id and record.comission_id:
                journal = record.journal_id
                journal_pay = record.pay_journal
                if not journal.default_credit_account_id \
                        or not journal.conac_credit_account_id:
                    if self.env.user.lang == 'es_MX':
                        raise ValidationError(_("Por favor configure la cuenta contable UNAM y CONAC en diario!"))
                    else:
                        raise ValidationError(_("Please configure UNAM and CONAC account in journal!"))

                if not record.comission_id:
                    if self.env.user.lang == 'es_MX':
                        raise ValidationError(_("No hay configuración para este tipo de comisión, verifique las configuraciones."))
                    else:
                        raise ValidationError(_("Please configure the comission to this record."))
                
                if record.comission_id.type_registry_spent=='authorized_budget':
                    for comission_code in record.comission_id.comission_line_ids:
                        if not comission_code.program_code_id.item_id.unam_account_id or not comission_code.program_code_id.item_id.unam_account_id.coa_conac_id:
                            if self.env.user.lang == 'es_MX':
                                raise ValidationError(_("Por favor configure la cuenta UNAM y CONAC en la partida de gasto del código programático"))
                            else:
                                raise ValidationError(_("Please configure UNAM and CONAC account in Item!"))
                         
                user = self.env.user
                partner_id = user.partner_id.id
                amount = self.amount


                if not record.move_line_ids_budget:
                    journal_account_id = journal.default_credit_account_id.id
                    journal_conac_account = journal.conac_credit_account_id.id
                else:
                    journal_account_id = self.env['provisional.account.comission'].search([('account_id','!=', None)]).account_id.id
                    journal_conac_account =  False

                    if not journal_account_id:
                        if self.env.user.lang == 'es_MX':
                            raise ValidationError(_("Por favor configure la cuenta contable provisional de las comisiones en la configuración del módulo"))
                        else:
                            raise ValidationError(_("Please configure the provisional account comission in settings"))

                if record.comission_id.type_registry_spent=='authorized_budget' and record.comission_id.type_authorized_budget=='one_program_code':
                    unam_move_val = {'ref': self.ref,  'conac_move': True, 'type': 'entry', 'type_of_registry_payment': 'comission',
                                     'date': account_date, 'journal_id': journal.id, 'company_id': self.env.user.company_id.id, 'is_payment_request': True, 'payment_state': 'paid',
                                     'line_ids': [(0, 0, {
                                         'account_id': journal_account_id,
                                         'coa_conac_id': journal_conac_account,
                                         'credit': amount+record.iva_amount,
                                         'partner_id': partner_id,
                                         'name': record.concept_related.concept,
                                         'commision_profit_id': self.id,
                                         'egress_key_id': record.comission_id.egress_key_id.id,
                                         
                                         }),
                                         (0, 0, {
                                         'account_id': record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.id,
                                         'coa_conac_id': record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.coa_conac_id.id,
                                         'debit': amount+record.iva_amount,
                                         'partner_id': partner_id,
                                         'name': record.concept_related.concept,
                                         'commision_profit_id': self.id,
                                         'dependency_id':record.comission_id.comission_line_ids.program_code_id.dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                         'sub_dependency_id':record.comission_id.comission_line_ids.program_code_id.sub_dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                         'program_code_id': record.comission_id.comission_line_ids.program_code_id.id,
                                         'egress_key_id': record.comission_id.egress_key_id.id,
                                         'price_total': amount+record.iva_amount,
                                         'exclude_from_invoice_tab': False,
                                         
                                         }),
                                         (0, 0, {
                                         'account_id': journal_pay.default_credit_account_id and journal_pay.default_credit_account_id.id or False,
                                         'coa_conac_id': journal_pay.conac_credit_account_id and journal_pay.conac_credit_account_id.id or False,
                                         'credit': amount + record.iva_amount, 
                                         'conac_move' : True,
                                         'partner_id': partner_id,
                                         'name': record.concept_related.concept,
                                         'commision_profit_id': self.id,
                                         'dependency_id':record.comission_id.comission_line_ids.program_code_id.dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                         'sub_dependency_id':record.comission_id.comission_line_ids.program_code_id.sub_dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                         'program_code_id': record.comission_id.comission_line_ids.program_code_id.id,
                                         'egress_key_id': record.comission_id.egress_key_id.id,
                                         
                                                         
                                                     }), 
                                         (0, 0, {
                                         'account_id': journal_pay.default_debit_account_id and journal_pay.default_debit_account_id.id or False,
                                         'coa_conac_id': journal_pay.conac_debit_account_id and journal_pay.conac_debit_account_id.id or False,
                                         'debit': amount + record.iva_amount,
                                         'conac_move' : True,
                                         'partner_id': partner_id,
                                         'name': record.concept_related.concept,
                                         'commision_profit_id': self.id,
                                         'dependency_id':record.comission_id.comission_line_ids.program_code_id.dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                         'sub_dependency_id':record.comission_id.comission_line_ids.program_code_id.sub_dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                         'program_code_id': record.comission_id.comission_line_ids.program_code_id.id,
                                         'egress_key_id': record.comission_id.egress_key_id.id,
                                         
                                                         
                                                       
                                                     }),
                                         (0, 0, {
                                         'account_id': journal_pay.accured_credit_account_id and journal_pay.accured_credit_account_id.id or False,
                                         'coa_conac_id': journal_pay.conac_accured_credit_account_id and journal_pay.conac_accured_credit_account_id.id or False,
                                         'credit': amount + record.iva_amount, 
                                         'conac_move' : True,
                                         'partner_id': partner_id,
                                         'name': record.concept_related.concept,
                                         'commision_profit_id': self.id,
                                         'dependency_id':record.comission_id.comission_line_ids.program_code_id.dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                         'sub_dependency_id':record.comission_id.comission_line_ids.program_code_id.sub_dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                         'program_code_id': record.comission_id.comission_line_ids.program_code_id.id,
                                         'egress_key_id': record.comission_id.egress_key_id.id,
                                         
                                                         
                                                     }), 
                                         (0, 0, {
                                         'account_id': journal_pay.accured_debit_account_id and journal_pay.accured_debit_account_id.id or False,
                                         'coa_conac_id': journal_pay.conac_accured_debit_account_id and journal_pay.conac_accured_debit_account_id.id or False,
                                         'debit': amount + record.iva_amount,
                                         'conac_move' : True,
                                         'partner_id': partner_id,
                                         'name': record.concept_related.concept,
                                         'commision_profit_id': self.id,
                                         'dependency_id':record.comission_id.comission_line_ids.program_code_id.dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                         'sub_dependency_id':record.comission_id.comission_line_ids.program_code_id.sub_dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                         'program_code_id': record.comission_id.comission_line_ids.program_code_id.id,
                                         'egress_key_id': record.comission_id.egress_key_id.id,
                                         
                                                       
                                                     }),
                                         (0, 0, {
                                         'account_id': journal_pay.execercise_credit_account_id and journal_pay.execercise_credit_account_id.id or False,
                                         'coa_conac_id': journal_pay.conac_exe_credit_account_id and journal_pay.conac_exe_credit_account_id.id or False,
                                         'credit': amount + record.iva_amount, 
                                         'conac_move' : True,
                                         'partner_id': partner_id,
                                         'name': record.concept_related.concept,
                                         'commision_profit_id': self.id,
                                         'dependency_id':record.comission_id.comission_line_ids.program_code_id.dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                         'sub_dependency_id':record.comission_id.comission_line_ids.program_code_id.sub_dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                         'program_code_id': record.comission_id.comission_line_ids.program_code_id.id,
                                         'egress_key_id': record.comission_id.egress_key_id.id,
                                                         
                                                     }), 
                                         (0, 0, {
                                         'account_id': journal_pay.execercise_debit_account_id and journal_pay.execercise_debit_account_id.id or False,
                                         'coa_conac_id': journal_pay.conac_exe_debit_account_id and journal_pay.conac_exe_debit_account_id.id or False,
                                         'debit': amount + record.iva_amount,
                                         'conac_move' : True,
                                         'partner_id': partner_id,
                                         'name': record.concept_related.concept,
                                         'commision_profit_id': self.id,
                                         'dependency_id':record.comission_id.comission_line_ids.program_code_id.dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                         'sub_dependency_id':record.comission_id.comission_line_ids.program_code_id.sub_dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                         'program_code_id': record.comission_id.comission_line_ids.program_code_id.id,
                                         'egress_key_id': record.comission_id.egress_key_id.id,
                                         
                                                       
                                                     }),
                                         (0, 0, {
                                         'account_id': journal_pay.paid_credit_account_id and journal_pay.paid_credit_account_id.id or False,
                                         'coa_conac_id': journal_pay.conac_paid_credit_account_id and journal_pay.conac_paid_credit_account_id.id or False,
                                         'credit': amount + record.iva_amount, 
                                         'conac_move' : True,
                                         'partner_id': partner_id,
                                         'name': record.concept_related.concept,
                                         'commision_profit_id': self.id,
                                         'dependency_id':record.comission_id.comission_line_ids.program_code_id.dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                         'sub_dependency_id':record.comission_id.comission_line_ids.program_code_id.sub_dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                         'program_code_id': record.comission_id.comission_line_ids.program_code_id.id,
                                         'egress_key_id': record.comission_id.egress_key_id.id,
                                         
                                                         
                                                     }), 
                                         (0, 0, {
                                         'account_id': journal_pay.paid_debit_account_id and journal_pay.paid_debit_account_id.id or False,
                                         'coa_conac_id': journal_pay.conac_paid_debit_account_id and journal_pay.conac_paid_debit_account_id.id or False,
                                         'debit': amount + record.iva_amount,
                                         'conac_move' : True,
                                         'partner_id': partner_id,
                                         'name': record.concept_related.concept,
                                         'commision_profit_id': self.id,
                                         'dependency_id':record.comission_id.comission_line_ids.program_code_id.dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                         'sub_dependency_id':record.comission_id.comission_line_ids.program_code_id.sub_dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                         'program_code_id': record.comission_id.comission_line_ids.program_code_id.id,
                                         'egress_key_id': record.comission_id.egress_key_id.id,
                                         
                                                       
                                                     }),


                
                                     ]}

                ###### Prorrateo ######

                elif record.comission_id.type_registry_spent=='authorized_budget' and (record.comission_id.type_authorized_budget=='apportionment' or record.comission_id.type_authorized_budget=='availability'):
                    if record.is_apportionment:
                        program_line_vals = []
                        line_vals = []
                        line_vals = {
                                     'account_id': journal_account_id,
                                     'coa_conac_id': journal_conac_account,
                                     'credit': amount+record.iva_amount,
                                     'partner_id': partner_id,
                                     'name': record.concept_related.concept,
                                     'commision_profit_id': self.id,
                                     'egress_key_id': record.comission_id.egress_key_id.id,
                                     
                                     }
                        program_line_vals.append((0,0,line_vals,{'context':{'check_move_validity': False}}))


                        for program_line in record.apportionment_line_ids.filtered(lambda x:x.spent > 0.0):
                            if program_line.spent > 0.0:
                                line_vals = {
                                             'account_id': program_line.program_code_id.item_id.unam_account_id.id,
                                             'coa_conac_id': program_line.program_code_id.item_id.unam_account_id.coa_conac_id.id,
                                             'debit': round(program_line.spent,12),
                                             'partner_id': partner_id,
                                             'name': record.concept_related.concept,
                                             'commision_profit_id': self.id,
                                             'dependency_id':program_line.program_code_id.dependency_id.id if program_line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                             'sub_dependency_id':program_line.program_code_id.sub_dependency_id.id if program_line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                             'program_code_id': program_line.program_code_id.id, 
                                             'price_total': round(program_line.spent,12),
                                             'exclude_from_invoice_tab': False,
                                             'egress_key_id': record.comission_id.egress_key_id.id,
                                             
                                            }
                                program_line_vals.append((0,0,line_vals,{'context':{'check_move_validity': False}}))

                                line_vals = {
                                             'account_id': journal_pay.default_credit_account_id and journal_pay.default_credit_account_id.id or False,
                                             'coa_conac_id': journal_pay.conac_credit_account_id and journal_pay.conac_credit_account_id.id or False,
                                             'credit': round(program_line.spent,12), 
                                             'conac_move' : True,
                                             'partner_id': partner_id,
                                             'name': record.concept_related.concept,
                                             'commision_profit_id': self.id,
                                             'program_code_id': program_line.program_code_id.id,
                                             'dependency_id':program_line.program_code_id.dependency_id.id if program_line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                             'sub_dependency_id':program_line.program_code_id.sub_dependency_id.id if program_line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                             'program_code_id': program_line.program_code_id.id,
                                             'egress_key_id': record.comission_id.egress_key_id.id,
                                               
                                                             
                                            }
                                program_line_vals.append((0,0,line_vals,{'context':{'check_move_validity': False}}))

                                line_vals = {
                                             'account_id': journal_pay.default_debit_account_id and journal_pay.default_debit_account_id.id or False,
                                             'coa_conac_id': journal_pay.conac_debit_account_id and journal_pay.conac_debit_account_id.id or False,
                                             'debit': round(program_line.spent,12),
                                             'conac_move' : True,
                                             'partner_id': partner_id,
                                             'name': record.concept_related.concept,
                                             'commision_profit_id': self.id,
                                             'program_code_id': program_line.program_code_id.id,
                                             'dependency_id':program_line.program_code_id.dependency_id.id if program_line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                             'sub_dependency_id':program_line.program_code_id.sub_dependency_id.id if program_line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                             'program_code_id': program_line.program_code_id.id, 
                                             'egress_key_id': record.comission_id.egress_key_id.id,
                                             
                                                                        
                                            }
                                program_line_vals.append((0,0,line_vals,{'context':{'check_move_validity': False}}))
                                            
                                line_vals = {
                                             'account_id': journal_pay.accured_credit_account_id and journal_pay.accured_credit_account_id.id or False,
                                             'coa_conac_id': journal_pay.conac_accured_credit_account_id and journal_pay.conac_accured_credit_account_id.id or False,
                                             'credit': round(program_line.spent,12), 
                                             'conac_move' : True,
                                             'partner_id': partner_id,
                                             'name': record.concept_related.concept,
                                             'commision_profit_id': self.id,
                                             'program_code_id': program_line.program_code_id.id, 
                                             'dependency_id':program_line.program_code_id.dependency_id.id if program_line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                             'sub_dependency_id':program_line.program_code_id.sub_dependency_id.id if program_line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                             'program_code_id': program_line.program_code_id.id,
                                             'egress_key_id': record.comission_id.egress_key_id.id, 
                                             
                                                             
                                            }
                                program_line_vals.append((0,0,line_vals,{'context':{'check_move_validity': False}}))

                                line_vals = {
                                             'account_id': journal_pay.accured_debit_account_id and journal_pay.accured_debit_account_id.id or False,
                                             'coa_conac_id': journal_pay.conac_accured_debit_account_id and journal_pay.conac_accured_debit_account_id.id or False,
                                             'debit': round(program_line.spent,12),
                                             'conac_move' : True,
                                             'partner_id': partner_id,
                                             'name': record.concept_related.concept,
                                             'commision_profit_id': self.id,
                                             'program_code_id': program_line.program_code_id.id, 
                                             'dependency_id':program_line.program_code_id.dependency_id.id if program_line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                             'sub_dependency_id':program_line.program_code_id.sub_dependency_id.id if program_line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                             'program_code_id': program_line.program_code_id.id,
                                             'egress_key_id': record.comission_id.egress_key_id.id,
                                              
                                                           
                                            }
                                program_line_vals.append((0,0,line_vals,{'context':{'check_move_validity': False}}))

                                line_vals = {
                                             'account_id': journal_pay.execercise_credit_account_id and journal_pay.execercise_credit_account_id.id or False,
                                             'coa_conac_id': journal_pay.conac_exe_credit_account_id and journal_pay.conac_exe_credit_account_id.id or False,
                                             'credit': round(program_line.spent,12), 
                                             'conac_move' : True,
                                             'partner_id': partner_id,
                                             'name': record.concept_related.concept,
                                             'commision_profit_id': self.id,
                                             'program_code_id': program_line.program_code_id.id,
                                             'dependency_id':program_line.program_code_id.dependency_id.id if program_line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                             'sub_dependency_id':program_line.program_code_id.sub_dependency_id.id if program_line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                             'program_code_id': program_line.program_code_id.id, 
                                             'egress_key_id': record.comission_id.egress_key_id.id,
                                             
                                                             
                                            }
                                program_line_vals.append((0,0,line_vals,{'context':{'check_move_validity': False}}))

                                line_vals = {
                                             'account_id': journal_pay.execercise_debit_account_id and journal_pay.execercise_debit_account_id.id or False,
                                             'coa_conac_id': journal_pay.conac_exe_debit_account_id and journal_pay.conac_exe_debit_account_id.id or False,
                                             'debit': round(program_line.spent,12),
                                             'conac_move' : True,
                                             'partner_id': partner_id,
                                             'name': record.concept_related.concept,
                                             'commision_profit_id': self.id,
                                             'program_code_id': program_line.program_code_id.id, 
                                             'dependency_id':program_line.program_code_id.dependency_id.id if program_line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                             'sub_dependency_id':program_line.program_code_id.sub_dependency_id.id if program_line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                             'program_code_id': program_line.program_code_id.id, 
                                             'egress_key_id': record.comission_id.egress_key_id.id,
                                             
                                                           
                                            }
                                program_line_vals.append((0,0,line_vals,{'context':{'check_move_validity': False}}))

                                line_vals = {
                                             'account_id': journal_pay.paid_credit_account_id and journal_pay.paid_credit_account_id.id or False,
                                             'coa_conac_id': journal_pay.conac_paid_credit_account_id and journal_pay.conac_paid_credit_account_id.id or False,
                                             'credit': round(program_line.spent,12), 
                                             'conac_move' : True,
                                             'partner_id': partner_id,
                                             'name': record.concept_related.concept,
                                             'commision_profit_id': self.id,
                                             'program_code_id': program_line.program_code_id.id,
                                             'dependency_id':program_line.program_code_id.dependency_id.id if program_line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                             'sub_dependency_id':program_line.program_code_id.sub_dependency_id.id if program_line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                             'program_code_id': program_line.program_code_id.id, 
                                             'egress_key_id': record.comission_id.egress_key_id.id,
                                             
                                                             
                                            }
                                program_line_vals.append((0,0,line_vals,{'context':{'check_move_validity': False}}))

                                line_vals = {
                                             'account_id': journal_pay.paid_debit_account_id and journal_pay.paid_debit_account_id.id or False,
                                             'coa_conac_id': journal_pay.conac_paid_debit_account_id and journal_pay.conac_paid_debit_account_id.id or False,
                                             'debit': round(program_line.spent,12),
                                             'conac_move' : True,
                                             'partner_id': partner_id,
                                             'name': record.concept_related.concept,
                                             'commision_profit_id': self.id,
                                             'program_code_id': program_line.program_code_id.id, 
                                             'dependency_id':program_line.program_code_id.dependency_id.id if program_line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                             'sub_dependency_id':program_line.program_code_id.sub_dependency_id.id if program_line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                             'program_code_id': program_line.program_code_id.id,
                                             'egress_key_id': record.comission_id.egress_key_id.id,
                                              
                                                           
                                            }
                                program_line_vals.append((0,0,line_vals,{'context':{'check_move_validity': False}}))

                        unam_move_val = {'ref': self.ref,  'conac_move': True, 'type': 'entry', 'type_of_registry_payment': 'comission',
                                             'date': account_date, 'journal_id': journal.id, 'company_id': self.env.user.company_id.id, 'is_payment_request': True, 'payment_state': 'paid',
                                             'line_ids': program_line_vals}
                    else:
                        if self.env.user.lang == 'es_MX':
                            raise ValidationError(_("No se pudo calcular el prorrateo, verifique su configuración"))


                ##### Cuenta Contable #####

                elif record.comission_id.type_registry_spent=='ledger_account':
                    journal = record.journal_id
                    if not journal.default_credit_account_id \
                            or not journal.conac_credit_account_id:
                        if self.env.user.lang == 'es_MX':
                            raise ValidationError(_("Por favor configure la cuenta UNAM y CONAC en diario!"))

                    today = datetime.today().date()
                    user = self.env.user
                    partner_id = user.partner_id.id
                    amount = self.amount+self.iva_amount


                    unam_move_val = {'ref': self.ref,  'conac_move': False, 'type': 'entry', 'type_of_registry_payment': 'comission',
                                     'date': account_date, 'journal_id': journal.id, 'company_id': self.env.user.company_id.id, 'is_payment_request': True, 'payment_state': 'paid',
                                     'line_ids': [(0, 0, {
                                         'account_id': journal_account_id,
                                         #'coa_conac_id': journal_conac_account,
                                         'credit': amount,
                                         'partner_id': partner_id,
                                         'name': record.concept_related.concept,
                                         'commision_profit_id': self.id,
                                         'egress_key_id': record.comission_id.egress_key_id.id,
                                         
                                         }),
                                         (0, 0, {
                                         'account_id': record.comission_id.account_id.id,
                                         #'coa_conac_id': record.programmatic_code_id.item_id.unam_account_id.coa_conac_id.id,
                                         'debit': amount,
                                         'partner_id': partner_id,
                                         'name': record.concept_related.concept,
                                         'commision_profit_id': self.id,
                                         'dependency_id':record.programmatic_code_id.dependency_id.id if record.programmatic_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                         'sub_dependency_id':record.programmatic_code_id.sub_dependency_id.id if record.programmatic_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                         'egress_key_id': record.comission_id.egress_key_id.id,
                                         })
                                    ]}
                elif record.comission_id.type_registry_spent=='extra_income':
                    journal = record.journal_id
                    program_line_vals = []
                    line_vals = []
                    if not journal.default_credit_account_id \
                            or not journal.conac_credit_account_id:
                        if self.env.user.lang == 'es_MX':
                            raise ValidationError(_("Por favor configure la cuenta UNAM y CONAC en diario!"))

                    today = datetime.today().date()
                    user = self.env.user
                    partner_id = user.partner_id.id
                    amount = self.amount+self.iva_amount

                    line_vals = {
                                 'account_id': journal_account_id,
                                 'coa_conac_id': journal_conac_account,
                                 'credit': amount,
                                 'partner_id': partner_id,
                                 'name': record.concept_related.concept,
                                 'commision_profit_id': self.id,
                                 'egress_key_id': record.comission_id.egress_key_id.id,
                                 
                                }
                    program_line_vals.append((0,0,line_vals,{'context':{'check_move_validity': False}}))
                    
                    line_vals = {
                                 'account_id': record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.id,
                                 'coa_conac_id': record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.coa_conac_id.id,
                                 'debit': amount,
                                 'partner_id': partner_id,
                                 'name': record.concept_related.concept,
                                 'commision_profit_id': self.id,
                                 'dependency_id':record.comission_id.comission_line_ids.program_code_id.dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                 'sub_dependency_id':record.comission_id.comission_line_ids.program_code_id.sub_dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                 'program_code_id': record.comission_id.comission_line_ids.program_code_id.id, 
                                 'price_total': amount,
                                 'exclude_from_invoice_tab': False,
                                 'egress_key_id': record.comission_id.egress_key_id.id,
                                 
                                }
                    program_line_vals.append((0,0,line_vals,{'context':{'check_move_validity': False}}))

                    for line in record.comission_id.afectation_account_id.ie_account_line_ids.filtered(lambda x:x.main_account == True):
                        # Busqueda de CRI
                        account_code = line.account_id.revenue_recognition_account_id.code
                        cri = self.env[AFFECTATION_INCOME_BUDGET_MODEL].search([]).associate_cri_account_code(account_code)
                        line_vals = {
                                     'account_id': line.account_id.revenue_recognition_account_id.id,
                                     #'coa_conac_id': record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.coa_conac_id.id,
                                     'credit': amount,
                                     'partner_id': partner_id,
                                     'name': record.concept_related.concept,
                                     'commision_profit_id': self.id,
                                     'dependency_id':record.comission_id.dependency_id.id if line.account_id.revenue_recognition_account_id.dep_subdep_flag else False,
                                     'sub_dependency_id':record.comission_id.sub_dependency_id.id if line.account_id.revenue_recognition_account_id.dep_subdep_flag else False,
                                     'egress_key_id': record.comission_id.egress_key_id.id,
                                     'cri' : cri.name if cri else False
                                    }
                        program_line_vals.append((0,0,line_vals,{'context':{'check_move_validity': False}}))

                        line_vals = {
                                     'account_id': line.account_id.id,
                                     #'coa_conac_id': record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.coa_conac_id.id,
                                     'debit': amount,
                                     'partner_id': partner_id,
                                     'name': record.concept_related.concept,
                                     'commision_profit_id': self.id,
                                     'dependency_id':record.comission_id.dependency_id.id if line.account_id.dep_subdep_flag else False,
                                     'sub_dependency_id':record.comission_id.sub_dependency_id.id if line.account_id.dep_subdep_flag else False,
                                     'egress_key_id': record.comission_id.egress_key_id.id,
                                     
                                    }
                        program_line_vals.append((0,0,line_vals,{'context':{'check_move_validity': False}}))

                        ### presupuestales de ingresos ##
                        if(line.account_id.revenue_recognition_account_id.code[0]=='4'):
                            # Busqueda de CRI
                            account_code = line.account_id.revenue_recognition_account_id.code
                            cri = self.env[AFFECTATION_INCOME_BUDGET_MODEL].search([]).associate_cri_account_code(account_code)
                            line_vals= {
                                         'account_id': journal.accrued_income_credit_account_id and journal.accrued_income_credit_account_id.id or False,
                                         'coa_conac_id': journal.conac_accrued_income_credit_account_id and journal.conac_accrued_income_credit_account_id.id or False,
                                         'credit': amount, 
                                         'conac_move' : True,
                                         'amount_currency' : 0.0,
                                         #'currency_id' : journal.currency_id.id,                                     
                                         'commision_profit_id': self.id,
                                         'partner_id': partner_id,
                                         'name': record.concept_related.concept,
                                         'dependency_id':record.comission_id.dependency_id.id if line.account_id.revenue_recognition_account_id.dep_subdep_flag else False,
                                         'sub_dependency_id':record.comission_id.sub_dependency_id.id if line.account_id.revenue_recognition_account_id.dep_subdep_flag else False,
                                         'egress_key_id': record.comission_id.egress_key_id.id,
                                         'cri' : cri.name if cri else False
                                                         
                                        }
                            program_line_vals.append((0,0,line_vals))
                            line_vals = {
                                         'account_id': journal.accrued_income_debit_account_id and journal.accrued_income_debit_account_id.id or False,
                                         'coa_conac_id': journal.conac_accrued_income_debit_account_id and journal.conac_accrued_income_debit_account_id.id or False,
                                         'debit': amount,
                                         'conac_move' : True,
                                         'amount_currency' : 0.0,
                                         #'currency_id' : payment.currency_id.id,                                     
                                         'commision_profit_id': self.id,
                                         'partner_id': partner_id,
                                         'name': record.concept_related.concept,
                                         'dependency_id':record.comission_id.dependency_id.id if line.account_id.revenue_recognition_account_id.dep_subdep_flag else False,
                                         'sub_dependency_id':record.comission_id.sub_dependency_id.id if line.account_id.revenue_recognition_account_id.dep_subdep_flag else False,
                                         'egress_key_id': record.comission_id.egress_key_id.id,
                                         'cri' : cri.name if cri else False
                                                       
                                        }
                            program_line_vals.append((0,0,line_vals))

                            line_vals = {
                                         'account_id': journal.recover_income_credit_account_id and journal.recover_income_credit_account_id.id or False,
                                         'coa_conac_id': journal.conac_recover_income_credit_account_id and journal.conac_recover_income_credit_account_id.id or False,
                                         'credit': amount, 
                                         'conac_move' : True,
                                         'amount_currency' : 0.0,
                                         #'currency_id' : payment.currency_id.id,                                     
                                         'commision_profit_id': self.id,
                                         'partner_id': partner_id,
                                         'name': record.concept_related.concept,
                                         'dependency_id':record.comission_id.dependency_id.id if line.account_id.revenue_recognition_account_id.dep_subdep_flag else False,
                                         'sub_dependency_id':record.comission_id.sub_dependency_id.id if line.account_id.revenue_recognition_account_id.dep_subdep_flag else False,
                                         'egress_key_id': record.comission_id.egress_key_id.id,
                                         'cri' : cri.name if cri else False
                                                         
                                        }
                            program_line_vals.append((0,0,line_vals))
                            line_vals = {
                                         'account_id': journal.recover_income_debit_account_id and journal.recover_income_debit_account_id.id or False,
                                         'coa_conac_id': journal.conac_recover_income_debit_account_id and journal.conac_recover_income_debit_account_id.id or False,
                                         'debit': amount,
                                         'conac_move' : True,
                                         'amount_currency' : 0.0,
                                         #'currency_id' : payment.currency_id.id,                                     
                                         'commision_profit_id': self.id,
                                         'partner_id': partner_id,
                                         'name': record.concept_related.concept,
                                         'dependency_id':record.comission_id.dependency_id.id if line.account_id.revenue_recognition_account_id.dep_subdep_flag else False,
                                         'sub_dependency_id':record.comission_id.sub_dependency_id.id if line.account_id.revenue_recognition_account_id.dep_subdep_flag else False,
                                         'egress_key_id': record.comission_id.egress_key_id.id,
                                         'cri' : cri.name if cri else False
                                                       
                                        }
                            program_line_vals.append((0,0,line_vals))
                            if cri:
                                # Objetos
                                obj_affetation = self.env[AFFECTATION_INCOME_BUDGET_MODEL]
                                obj_logbook = self.env['income.adequacies.function']
                                obj_fiscal_year = self.env['account.fiscal.year']
                                date_affectation = self.movement_date
                                # Afectación al presupuesto
                                type_affectation = 'eje-rec'
                                date_accrued = False
                                # Mandar datos a la clase de afectación
                                cri_id = cri.id
                                
                                obj_affetation.search([]).affectation_income_budget(type_affectation,cri_id,date_accrued,date_affectation,amount)
                                # Guarda la bitacora de la adecuación
                                # Busca el id del año fiscal
                                year = date_affectation.year
                                fiscal_year_id = obj_fiscal_year.search([('name', '=', year)])
                                movement_type = 'income'
                                movement = 'for_exercising_collected'
                                origin_type = 'income-com'
                                movement_id = self.folio
                                journal_id = journal.id
                                update_date_record = datetime.today()
                                update_date_income = datetime.today()
                                obj_logbook.search([]).logbook_income_budget(fiscal_year_id.id,cri_id,date_affectation,movement_type,movement,origin_type,movement_id,journal_id,update_date_record,amount,update_date_income)

                    ### Presupuestales de pago ###

                    line_vals = {
                                 'account_id': journal_pay.default_credit_account_id and journal_pay.default_credit_account_id.id or False,
                                 'coa_conac_id': journal_pay.conac_credit_account_id and journal_pay.conac_credit_account_id.id or False,
                                 'credit': amount, 
                                 'conac_move' : True,
                                 'partner_id': partner_id,
                                 'name': record.concept_related.concept,
                                 'commision_profit_id': self.id,
                                 'program_code_id': record.comission_id.comission_line_ids.program_code_id.id,
                                 'dependency_id':record.comission_id.comission_line_ids.program_code_id.dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                 'sub_dependency_id':record.comission_id.comission_line_ids.program_code_id.sub_dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                 'program_code_id': record.comission_id.comission_line_ids.program_code_id.id, 
                                 'egress_key_id': record.comission_id.egress_key_id.id,

                                 
                                                 
                                }
                    program_line_vals.append((0,0,line_vals,{'context':{'check_move_validity': False}}))

                    line_vals = {
                                 'account_id': journal_pay.default_debit_account_id and journal_pay.default_debit_account_id.id or False,
                                 'coa_conac_id': journal_pay.conac_debit_account_id and journal_pay.conac_debit_account_id.id or False,
                                 'debit': amount,
                                 'conac_move' : True,
                                 'partner_id': partner_id,
                                 'name': record.concept_related.concept,
                                 'commision_profit_id': self.id,
                                 'program_code_id': record.comission_id.comission_line_ids.program_code_id.id, 
                                 'dependency_id':record.comission_id.comission_line_ids.program_code_id.dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                 'sub_dependency_id':record.comission_id.comission_line_ids.program_code_id.sub_dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                 'program_code_id': record.comission_id.comission_line_ids.program_code_id.id,
                                 'egress_key_id': record.comission_id.egress_key_id.id,

                                 
                                                            
                                }
                    program_line_vals.append((0,0,line_vals,{'context':{'check_move_validity': False}}))
                                
                    line_vals = {
                                 'account_id': journal_pay.accured_credit_account_id and journal_pay.accured_credit_account_id.id or False,
                                 'coa_conac_id': journal_pay.conac_accured_credit_account_id and journal_pay.conac_accured_credit_account_id.id or False,
                                 'credit': amount, 
                                 'conac_move' : True,
                                 'partner_id': partner_id,
                                 'name': record.concept_related.concept,
                                 'commision_profit_id': self.id,
                                 'program_code_id': record.comission_id.comission_line_ids.program_code_id.id,
                                 'dependency_id':record.comission_id.comission_line_ids.program_code_id.dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                 'sub_dependency_id':record.comission_id.comission_line_ids.program_code_id.sub_dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                 'program_code_id': record.comission_id.comission_line_ids.program_code_id.id,
                                 'egress_key_id': record.comission_id.egress_key_id.id,
                                 
                                                 
                                }
                    program_line_vals.append((0,0,line_vals,{'context':{'check_move_validity': False}}))

                    line_vals = {
                                 'account_id': journal_pay.accured_debit_account_id and journal_pay.accured_debit_account_id.id or False,
                                 'coa_conac_id': journal_pay.conac_accured_debit_account_id and journal_pay.conac_accured_debit_account_id.id or False,
                                 'debit': amount,
                                 'conac_move' : True,
                                 'partner_id': partner_id,
                                 'name': record.concept_related.concept,
                                 'commision_profit_id': self.id,
                                 'program_code_id': record.comission_id.comission_line_ids.program_code_id.id, 
                                 'dependency_id':record.comission_id.comission_line_ids.program_code_id.dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                 'sub_dependency_id':record.comission_id.comission_line_ids.program_code_id.sub_dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                 'program_code_id': record.comission_id.comission_line_ids.program_code_id.id,
                                 'egress_key_id': record.comission_id.egress_key_id.id,
                                 
                                               
                                }
                    program_line_vals.append((0,0,line_vals,{'context':{'check_move_validity': False}}))

                    line_vals = {
                                 'account_id': journal_pay.execercise_credit_account_id and journal_pay.execercise_credit_account_id.id or False,
                                 'coa_conac_id': journal_pay.conac_exe_credit_account_id and journal_pay.conac_exe_credit_account_id.id or False,
                                 'credit': amount, 
                                 'conac_move' : True,
                                 'partner_id': partner_id,
                                 'name': record.concept_related.concept,
                                 'commision_profit_id': self.id,
                                 'program_code_id': record.comission_id.comission_line_ids.program_code_id.id,
                                 'dependency_id':record.comission_id.comission_line_ids.program_code_id.dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                 'sub_dependency_id':record.comission_id.comission_line_ids.program_code_id.sub_dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                 'program_code_id': record.comission_id.comission_line_ids.program_code_id.id,
                                 'egress_key_id': record.comission_id.egress_key_id.id,
                                 
                                                 
                                }
                    program_line_vals.append((0,0,line_vals,{'context':{'check_move_validity': False}}))

                    line_vals = {
                                 'account_id': journal_pay.execercise_debit_account_id and journal_pay.execercise_debit_account_id.id or False,
                                 'coa_conac_id': journal_pay.conac_exe_debit_account_id and journal_pay.conac_exe_debit_account_id.id or False,
                                 'debit': amount,
                                 'conac_move' : True,
                                 'partner_id': partner_id,
                                 'name': record.concept_related.concept,
                                 'commision_profit_id': self.id,
                                 'program_code_id': record.comission_id.comission_line_ids.program_code_id.id, 
                                 'dependency_id':record.comission_id.comission_line_ids.program_code_id.dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                 'sub_dependency_id':record.comission_id.comission_line_ids.program_code_id.sub_dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                 'egress_key_id': record.comission_id.egress_key_id.id,
                                               
                                }
                    program_line_vals.append((0,0,line_vals,{'context':{'check_move_validity': False}}))

                    line_vals = {
                                 'account_id': journal_pay.paid_credit_account_id and journal_pay.paid_credit_account_id.id or False,
                                 'coa_conac_id': journal_pay.conac_paid_credit_account_id and journal_pay.conac_paid_credit_account_id.id or False,
                                 'credit': amount, 
                                 'conac_move' : True,
                                 'partner_id': partner_id,
                                 'name': record.concept_related.concept,
                                 'commision_profit_id': self.id,
                                 'program_code_id': record.comission_id.comission_line_ids.program_code_id.id, 
                                 'dependency_id':record.comission_id.comission_line_ids.program_code_id.dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                 'sub_dependency_id':record.comission_id.comission_line_ids.program_code_id.sub_dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                 'program_code_id': record.comission_id.comission_line_ids.program_code_id.id,
                                 'egress_key_id': record.comission_id.egress_key_id.id,
                                 
                                                 
                                }
                    program_line_vals.append((0,0,line_vals,{'context':{'check_move_validity': False}}))

                    line_vals = {
                                 'account_id': journal_pay.paid_debit_account_id and journal_pay.paid_debit_account_id.id or False,
                                 'coa_conac_id': journal_pay.conac_paid_debit_account_id and journal_pay.conac_paid_debit_account_id.id or False,
                                 'debit': amount,
                                 'conac_move' : True,
                                 'partner_id': partner_id,
                                 'name': record.concept_related.concept,
                                 'commision_profit_id': self.id,
                                 'program_code_id': record.comission_id.comission_line_ids.program_code_id.id,
                                 'dependency_id':record.comission_id.comission_line_ids.program_code_id.dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                 'sub_dependency_id':record.comission_id.comission_line_ids.program_code_id.sub_dependency_id.id if record.comission_id.comission_line_ids.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                 'program_code_id': record.comission_id.comission_line_ids.program_code_id.id, 
                                 'egress_key_id': record.comission_id.egress_key_id.id,
                                 
                                               
                                }
                    program_line_vals.append((0,0,line_vals,{'context':{'check_move_validity': False}}))

                        
                    unam_move_val = {'ref': self.ref,  'conac_move': True, 'type': 'entry', 'type_of_registry_payment': 'comission',
                                         'date': account_date, 'journal_id': journal.id, 'company_id': self.env.user.company_id.id, 'is_payment_request': True, 'payment_state': 'paid',
                                         'line_ids': program_line_vals}
                                                
                                        
                move_obj = self.env['account.move']
                unam_move = move_obj.create(unam_move_val).with_context(check_move_validity=False)
                unam_move.action_post()
                #self.create_payment_request(unam_move) 

class Apportionment(models.Model):

    _name = 'apportionment'
    _description = "Apportionment"

    comission_profit_id = fields.Many2one('comission.profit','Comission Profit')

    program_code_id = fields.Many2one('program.code',string="Program Code", index=True)
    available_date =  fields.Float("Available date")
    available_percentage =  fields.Float("Available percentage")
    spent =  fields.Float("Spent to record", digits=(12,2))
    residual_amount =  fields.Float("Residual amount", digits=(12,2))
