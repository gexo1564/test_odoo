odoo.define('remove_export_option.remove_export_option', function (require) {
"use strict";

var Sidebar = require('web.Sidebar');
var core = require('web.core');
var _t = core._t;
var _lt = core._lt;
var rpc = require('web.rpc');

const showMenus = (self, model, method) => Promise.resolve(
  rpc.query({
    'model': model,
    'method': method,
    'args': [0],
    'kwargs': {context: {}},
  }).then(function(result) {
    const action = result.filter((e)=> e[0] == 'action').map((e) => e[1])
    const print = result.filter((e)=> e[0] == 'print').map((e) => e[1])
    //arreglo de acciones
    const actions = []
    //arreglo de acciones de print
    const prints = []
    const accions_n_print = [...self.items['print'],...self.items['other']]
    console.log(accions_n_print) 
    accions_n_print.forEach(element => {
      if (element.action){
        if (action.includes(element.action.id)){
          actions.push(element)
        }
        else if (print.includes(element.action.id)){
          prints.push(element)
        }
      }
      else
        actions.push(element)
    });
    console.log(actions);
    console.log(prints);
    self.items['other'] = actions;
    self.items['print'] = prints;
  })
).then(self._super.bind(self));


    Sidebar.include({
        start: function () {
            var self = this;
            var def;
      if (self.env.context.hide_project_payment_action_button)
            {

    def = rpc.query({
      'model': 'account.move',
      'method': 'get_all_action_ids',
      'args': [0],
      'kwargs': {context: self.env.context},
        }).then(function(result) {
      self.items['other'] = $.grep(self.items['other'], function(i){
           if (i && i.action)
           {
        if (!(result.includes(i.action.id)))
        {
          return i;	
        }
                 }		
           else
           {
        return i;
                 }
            });
                });
                return Promise.resolve(def).then(this._super.bind(this));
            }

        if (self.env.context.hide_project_payment_action_button_factura)
            {
        def = rpc.query({
      'model': 'account.move',
      'method': 'get_all_action_ids_factura',
      'args': [0],
      'kwargs': {context: self.env.context},
        }).then(function(result) {
      self.items['other'] = $.grep(self.items['other'], function(i){
           if (i && i.action)
           {
        if (!(result.includes(i.action.id)))
        {
          return i;
        }
                 }
           else
           {
        return i;
                 }
            });
                });
                return Promise.resolve(def).then(this._super.bind(this));
            }

        if (self.env.context.hide_project_payment_action_button_nota)
            {
        def = rpc.query({
      'model': 'account.move',
      'method': 'get_all_action_ids_nota',
      'args': [0],
      'kwargs': {context: self.env.context},
        }).then(function(result) {
      self.items['other'] = $.grep(self.items['other'], function(i){
           if (i && i.action)
           {
        if (!(result.includes(i.action.id)))
        {
          return i;
        }
                 }
           else
           {
        return i;
                 }
            });
                });
                return Promise.resolve(def).then(this._super.bind(this));
            }

        if (self.env.context.hide_project_payment_action_button_cobro)
            {
        def = rpc.query({
      'model': 'account.payment',
      'method': 'get_all_action_ids_cobro',
      'args': [0],
      'kwargs': {context: self.env.context},
        }).then(function(result) {
      self.items['other'] = $.grep(self.items['other'], function(i){
           if (i && i.action)
           {
        if (!(result.includes(i.action.id)))
        {
          return i;
        }
                 }
           else
           {
        return i;
                 }
            });
                });
                return Promise.resolve(def).then(this._super.bind(this));
            }
  // Pago/Unidad de Procesos Administrativos/Solicitud de Pago
  if (!self.env.context.income_payment_action && self.env.context.payment_request)
    return showMenus(self, 'account.move', 'get_toolbar_payment_request_ids')
  // Finanzas/Gestión de Pagos/Programación de Pago
  if (!self.env.context.income_payment_action && self.env.context.payment_schedule)
    return showMenus(self, 'account.move', 'get_toolbar_payment_schedule_ids')
  // Finanzas/Fondo Fijo/Solicitudes de Pago Reembolsos
  if (!self.env.context.income_payment_action && self.env.context.payment_request_ministration)
    return showMenus(self, 'account.move', 'get_toolbar_payment_request_ministration')
  // Finanzas/Gestión de Pagos/Pagos
  if (!self.env.context.income_payment_action && self.env.context.default_payment_type)
    return showMenus(self, 'account.payment', 'get_toolbar_payment_ids')
  // Finanzas/Gestión de Pagos/Pagos en Efectivo
  if (!self.env.context.income_payment_action && self.env.context.payments_cash)
    return showMenus(self, 'account.payment', 'get_toolbar_payments_cash_ids')
    // Control de checkes/proteccion de cheques(nomina)
  if (self.env.context.if_you_print_n_change)
    return showMenus(self, 'payment.batch.supplier', 'get_toolbar_print_checks_and_set_status')

  if (self.env.context.income_payment_action && self.env.context.default_payment_type)
            {

    def = rpc.query({
      'model': 'account.payment',
      'method': 'get_hide_action_income_ids',
      'args': [0],
      'kwargs': {context: {}},
        }).then(function(result) {
      self.items['other'] = $.grep(self.items['other'], function(i){
           if (i && i.action)
           {
        if (!(result.includes(i.action.id)))
        {
          return i;	
        }
                 }		
           else
           {
        return i;
                 }
            });
                });
                return Promise.resolve(def).then(this._super.bind(this));
            }
        },
    });
});
