# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
from datetime import datetime
import time
import json
import logging

# Define Constant
NAME_MODULE = 'account.move'
NAME_LINES = 'account.move.line'
MODIFICATIONS_FEDERAL_SUBSIDY = 'modifications.federal.subsidy'

class AccountMove(models.Model):
    _inherit = NAME_MODULE
    _order = 'create_date desc'

    account_lines_ids = fields.Many2one(MODIFICATIONS_FEDERAL_SUBSIDY, 'Movements Subsidy Lines', ondelete="cascade")

class AccountMoveLine(models.Model):
    _inherit = NAME_LINES

    account_lines_ids = fields.Many2one(MODIFICATIONS_FEDERAL_SUBSIDY, 'Movements Subsidy Lines', ondelete="cascade")