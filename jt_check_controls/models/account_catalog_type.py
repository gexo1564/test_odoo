from odoo import models, fields, api, _
from datetime import datetime
from odoo.exceptions import UserError, ValidationError

ACC_CATALOG_TYPE='account.catalog.type'
class AccountCatalogType(models.Model):
    _name = ACC_CATALOG_TYPE
    _description = 'Account Catalog Type for account Checks'

    name= fields.Char('Name',compute='_get_name_account')
    catalog_type = fields.Selection(
        [   ('supplier','Supplier'),
            ('project','Project'),
            ('pension','Pension'),
            ('nomina','Nominal'),
         ], string="Catalog",required=True)
    
    account_id = fields.Many2one('account.account',string="Acount", required=True)

    type_for_nomina = fields.Selection(
        [   ('normal','Normal'),
            ('check_cancel', 'Check Cancel'),
            ('check_forwarded','Check Forwarded'),
         ], string="Nomina Type", required=False)
            

    #Revisar si la cuenta contable no se repita
    @api.constrains('catalog_type')
    def _check_catalog_type(self):
        catalog_type=self.env[ACC_CATALOG_TYPE]
        if catalog_type.search_count([('catalog_type','=','supplier')])>1:
            raise UserError('Solo se puede tener una unica cuenta de proveedor.')
        if catalog_type.search_count([('catalog_type','=','project')])>1:
            raise UserError('Solo se puede tener una unica cuenta de proyectos.')
        if catalog_type.search_count([('catalog_type','=','pension')])>1:
            raise UserError('Solo se puede tener una unica cuenta de pensión.')
        
    #Revisar si la cuenta contable para las de nómina
    @api.constrains('catalog_type','type_for_nomina')
    def _chec_catalog_nomina(self):
        catalog_type=self.env[ACC_CATALOG_TYPE]
        if catalog_type.search_count([('catalog_type','=','nomina'),('type_for_nomina','=','normal')])>1:
            raise UserError('Solo se puede tener una unica cuenta de nómina de Acreedores.')
        if catalog_type.search_count([('catalog_type','=','nomina'),('type_for_nomina','=','check_cancel')])>1:
            raise UserError('Solo se puede tener una unica cuenta de nómina de Acreedores por cheque cancelado')
        if catalog_type.search_count([('catalog_type','=','nomina'),('type_for_nomina','=','check_forwarded')])>1:
            raise UserError('Solo se puede tener una unica cuenta de nómina de Acreedores por cheque reexpedido')
        
    def _get_name_account(self):
        for rec in self:
            nombre=dict(rec._fields['catalog_type']._description_selection(self.env))
            if rec.catalog_type=='nomina':
                type_nom=dict(rec._fields['type_for_nomina']._description_selection(self.env))
                rec.name='%s (%s)' %(nombre.get('nomina'),type_nom.get(rec.type_for_nomina))
            else:
                rec.name=nombre.get(rec.catalog_type)

    def _get_nominal_param(self,type):
        catalog_type=self.env[ACC_CATALOG_TYPE]
        if type == 'normal':
            if not catalog_type.search([('catalog_type','=','nomina'),('type_for_nomina','=','normal')]):
                raise ValidationError("Configure la cuenta de nómina de acreedores por sueldo en el menú de 'Catalogo de cuentas para cheques'.")
            return catalog_type.search([('catalog_type','=','nomina'),('type_for_nomina','=','normal')]).account_id
        elif type == 'check_cancel':
            if not catalog_type.search([('catalog_type','=','nomina'),('type_for_nomina','=','check_cancel')]):
                raise ValidationError("Configure la cuenta de nómina de acreedores por cheque cancelado en el menú de 'Catalogo de cuentas para cheques'.")
            return catalog_type.search([('catalog_type','=','nomina'),('type_for_nomina','=','check_cancel')]).account_id
        elif type == 'check_forwarded':
            if not catalog_type.search([('catalog_type','=','nomina'),('type_for_nomina','=','check_forwarded')]):
                raise ValidationError("Configure la cuenta de nómina de acreedores por cheque reexpedido en el menú de 'Catalogo de cuentas para cheques'.")
            return catalog_type.search([('catalog_type','=','nomina'),('type_for_nomina','=','check_forwarded')]).account_id