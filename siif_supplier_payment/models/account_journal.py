# -*- coding: utf-8 -*-
from datetime import datetime
from odoo import models, fields

class AccountJournal(models.Model):
    _inherit = 'account.journal'

    def check_balances(self, total_amount):
        # Validar disponible de la cuenta bancaria:
        account_id = self.default_debit_account_id
        check_req = self.env['check.log'].search([
            ('checklist_id.checkbook_req_id.bank_id', '=', self.id),
            ('status', 'in', ('Delivered','In transit'))
        ])
        total_check_amt = sum(x.check_amount for x in check_req)
        amt_data = self.env['account.move.line'].read_group(domain=[
            ('move_id.state', '=', 'posted'),
            ('account_id', '=', account_id.id)
        ],fields=['account_id','balance'], groupby=['account_id'])
        mapped_data = dict([(m['account_id'][0], (m['balance'])) for m in amt_data])
        account_balance = mapped_data.get(account_id.id, 0)
        today_date = datetime.today().date()
        transfer_request_sent = self.env['request.open.balance.finance'].search([('date_required','=',today_date),('state','=','sent'),('desti_bank_account_id','=',self.id)])
        transfer_request_confirm = self.env['request.open.balance.finance'].search([('date_required','=',today_date),('state','=','confirmed'),('desti_bank_account_id','=',self.id)])
        minimum_balance = self and self.min_balance or 0
        amount_trasnfer_sent = sum(x.amount for x in transfer_request_sent)
        amount_trasnfer_confirmed = sum(x.amount for x in transfer_request_confirm)
        balance_available = account_balance - total_check_amt + amount_trasnfer_sent + amount_trasnfer_confirmed
        # Si no hay disponible se debe notificar al usuario
        if (balance_available - total_amount) < minimum_balance:
            return False, (balance_available, minimum_balance, total_amount)
        return True, (balance_available, minimum_balance, total_amount)
