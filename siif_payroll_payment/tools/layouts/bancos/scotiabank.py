import io
import csv
from ..layouts import Layout, ResultLine

class LayoutScotiabank(Layout):

    dict_payment_type = {
        'SUELDO QUINCENAL': 'Sueldos',
        'PRESTACION ADIC.': 'Prestaciones Adicionales',
        'DEPOSITO PENSION': 'Pensiones Alimenticias',
    }

    def load_file(self, file_data):
        # io.StringIO(file_data.decode("utf-8"))
        self.file = io.StringIO(file_data.decode("ISO-8859-1")) #open(file, encoding="ISO-8859-1")

    def load_body(self):
        return self.file.readlines()[2:]

    def close_file(self):
        self.file.close()

    def read_line(self, line):
        payment_type = self.dict_payment_type.get(line[213:229])
        bank_account = line[132:152].zfill(20)
        amount = float("%s.%s" % (line[8:21], line[21:23]))
        beneficiary = line[66:106].strip()
        status = line[323:326]
        ok = status == "000"
        return ResultLine(ok, payment_type, {'beneficiary': beneficiary, 'bank_account': bank_account, 'amount': amount}, status)

    def get_ids_search_employee_payroll_file(self, payroll_processing_id: int, lines: list):
        return [], []

    def get_ids_search_account_payment(self, batch_folio: str, lines: list):
        payment_type = self.env['batch.management'].search([('folio', '=', batch_folio)])
        # Se obtienen los ids que hacen match con el criterio de búsqueda
        query = """
            select lpad(a.acc_number, 20, '0') bank_account, p.id, p.amount
            from account_payment p, res_partner_bank a
            where p.payment_bank_account_id = a.id
            and batch_folio = %s
        """
        self.env.cr.execute(query, (batch_folio,))
        result_set = {r[0]: r[1:] for r in self.env.cr.fetchall()}
        lines_ok, lines_err = [], []
        for line in lines:
            if line.payment_type == payment_type.concept:
                beneficiary = line.columns.get('beneficiary')
                res = result_set.get(line.columns.get('bank_account'), None)
                if res:
                    if res[1] == line.columns.get('amount'):
                        line.res_id = res[0]
                        lines_ok.append(line)
                    else:
                        line.res_id = None
                        line.error_message = f"El monto del pago ({line.columns.get('amount'):,.2f}) del beneficiario {beneficiary} no coincide con el registrado ({res[1]:,.2f})."
                        lines_err.append(line)
                else:
                    line.res_id = None
                    line.error_message = f"No se encontró un registro para el beneficiario {beneficiary}"
                    lines_err.append(line)
        return lines_ok, lines_err