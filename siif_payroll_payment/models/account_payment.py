# -*- coding: utf-8 -*-

import logging
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from datetime import datetime, date

class AccountPayment(models.Model):
    _inherit = 'account.payment'

    employee_payroll_id = fields.Many2one('employee.payroll.file', 'Employee Payroll File')
    payroll_request_type = fields.Selection(
        selection=[
            ('payroll_payment', 'Payroll Payment'),
            ('additional_benefits', 'Additional Benefits'),
            ('special_payroll_payment', 'Special Payroll Payment'),
            ('alimony', 'Alimony Payment')
        ], string='Type of Request for Payroll Payment'
    )

    is_payroll_payment_reissue = fields.Boolean()
    adjustment_case_id = fields.Many2one(comodel_name='adjustment.cases', string='Adjustment Case')

    def insert_sql(self, lines):
        def _get_column_stored_name():
            query = """
                select column_name from information_schema.columns
                where table_schema = 'public'
                and table_name   = 'account_payment' and column_name <> 'id'
            """
            self.env.cr.execute(query, ())
            return [f'"{res[0]}"' for res in self.env.cr.fetchall()]

        def _get_insert_sql():
            columns = _get_column_stored_name()
            column = ", ".join(columns)
            values = ", ".join(["%s"] * len(columns))
            return f"insert into account_payment ({column}) values({values}) returning id", columns

        def _get_insert_params(columns, vals):
            def update_vals(updates):
                for k, v in updates:
                    if k not in vals: vals.update({k: v})

            res = []
            amount = vals.get('amount', 0)
            updates = [
                ('payment_type', ('inbound' if amount > 0 else 'outbound')),
                ('supplier', 'supplier'),
                ('currency_id', self.env.company.currency_id.id),
                ('create_date', 'now()'),
                ('write_date', 'now()'),
                ('create_uid', self.env.user.id),
                ('write_uid', self.env.user.id),
            ]
            update_vals(updates)
            for column in columns:
                res.append(vals.get(column[1:-1], None))
            return tuple(res)

        query, columns = _get_insert_sql()
        params = []
        ids = []
        for line in lines:
            params.append(_get_insert_params(columns, line))
        for p in params:
            self.env.cr.execute(query, p)
            res = (self.env.cr.fetchone() or [None])[0]
            if not res:
                raise ValidationError("Error al insertar el registro account.payment")
            ids.append(res)
        return ids

    def post(self):
        """
        Creación de los asientos contables presupuestales de pago para el pago
        de la la nómina de cada empleado
        """
        for record in self:
            if record.payment_request_type == 'payroll_payment':
                self.payroll_paymet_post([record.id])
            else:
                super(AccountPayment, record).post()

    def payroll_paymet_post(self, ids, validate=False, ref=None):
        # Configuraciones y parámetros iniciales.
        parameters = self.env['payroll.payment.parameters'].search([])
        if not (parameters and parameters.partner_id):
            raise ValidationError("Configure el beneficiario de la Solicitud de Pago en el Ménu de Parámetros de Pago de Nómina.")
        # Beneficiario
        partner_id = parameters.partner_id
        if not partner_id.property_account_payable_id:
            raise ValidationError("Configure la cuenta a pagar del beneficiario de la Solicitud de Pago.")
        self._payroll_paymet_post(ids, partner_id, validate, ref)

    def _payroll_paymet_post(self, ids, partner_id, validate=False, ref=None):
        """
            Método optimizado para realizar el post de los pagos de nómina
        """
        # logging.info("Inicio ...")
        # Se obtienen los datos de los pagos para la póliza de account.move
        query_move = """
            select
                p.id,
                p.partner_id,
                p.communication as ref,
                p.payment_date,
                p.journal_id,
                p.amount price_unit,
                p.employee_payroll_id,
                p.payment_method_id,
                p.dependancy_id,
                p.sub_dependancy_id,
                p.fornight,
                p.folio,
                p.payroll_request_type,
                p.l10n_mx_edi_payment_method_id,
                p.check_folio_id,
                p.payment_state account_payment_state,
                j.default_credit_account_id account_id,
                j.paid_credit_account_id,
                j.conac_paid_credit_account_id,
                j.paid_debit_account_id,
                j.conac_paid_debit_account_id,
                p.is_payroll_payment_reissue
            from account_payment p, account_journal j
            where p.journal_id = j.id
            and p.id in %s
            and payment_state = 'for_payment_procedure'
        """
        self.env.cr.execute(query_move, (tuple(ids), ))
        sequence_code = 'account.payment.supplier.invoice'
        for payment in self.env.cr.dictfetchall():
            # Revisión del status del pago
            if payment.get('account_payment_state') == 'posted':
                query_update = """
                    update account_payment no_validate_payment = True where id = %s
                """
                self.env.cr.execute(query_update, (payment.get('id'),))
                continue
            # Cuenta de la solicitud de pago/cheque
            account_id = partner_id.property_account_payable_id.id
            # Para pagos por cheques
            is_check = payment.get('l10n_mx_edi_payment_method_id', '') == self.env.ref('l10n_mx_edi.payment_method_cheque').id
            if is_check:
                account_id = None
                # Cuenta para acreedores por reexpedicion de cheque
                if payment.get('is_payroll_payment_reissue'):
                    account_id = self.env['account.catalog.type']._get_nominal_param('check_forwarded').id
                else:
                # Cuenta para acreedores de cheques
                    account_id = self.env['account.catalog.type']._get_nominal_param('normal').id
                # Se actualiza el status del cheque a cobrado
                query = "update check_log set status = 'Charged' where id = %s"
                self.env.cr.execute(query, (payment.get('check_folio_id', -1),))
            payment.update({'type': 'entry'})
            # Se genera el nombre del pago a partir de la secuencia
            # Para nómina se emplea account.payment.supplier.invoice
            payment_name = self.env['ir.sequence'].next_by_code(
                sequence_code,
                sequence_date=payment.get('payment_date')
            )
            # Creación de de la Póliza contable (account.move)
            # # logging.info("Creación de la poliza de pago ...")
            move_id = self.env['account.move'].insert_sql([payment])[0]
            # # logging.info("Ok")
            # Creación de los apuntes contables de pago: Cuenta de nomina vs Banco
            # # logging.info("Generación de las líneas de los apuntes contables ...")
            # Si se recibe una referencia preestablecida por paramatros, se actualiza
            # la misma para las apuntes contables
            if ref:
                payment.update({'ref': ref})
            lines = []
            # Referencia del asiento contable
            name = "Pago de nómina quincena %s %s" % (
                payment.get('fornight', ''),
                payment.get('payment_date').strftime('%Y')
            )
            # Débito: Cuenta de por pagar de nómina
            lines.append({
                'price_unit': payment.get('price_unit'),
                'amount_residual': payment.get('price_unit'),
                'date_maturity': payment.get('payment_date'),
                'account_id': account_id,
                'dependency_id': payment.get('dependancy_id'),
                'sub_dependency_id': payment.get('sub_dependancy_id'),
                'payment_id': payment.get('id'),
                'ref': payment.get('ref'),
            })
            # Crédito: Cuenta de bancos
            lines.append({
                'price_unit': -payment.get('price_unit'),
                'amount_residual': -payment.get('price_unit'),
                'date_maturity': payment.get('payment_date'),
                'account_id': payment.get('account_id'),
                'payment_id': payment.get('id'),
                'ref': payment.get('ref'),
            })
            # Generación de las PRESUSPUESTALES DE PAGO
            query_program_code = None
            if payment.get('payroll_request_type', '') == 'payroll_payment':
                query_program_code = """
                    select l.program_code_id program_code_id,
                        pc.dependency_id,
                        pc.sub_dependency_id,
                        sum(amount)::numeric price_unit
                    from preception_line l, preception p, program_code pc
                    where l.preception_id = p.id
                        and l.program_code_id = pc.id
                        and p.is_net_pay is True
                        and l.payroll_id = %s
                    group by l.program_code_id, pc.dependency_id, pc.sub_dependency_id
                """
            elif payment.get('payroll_request_type', '') == 'additional_benefits':
                query_program_code = """
                    select l.program_code_id program_code_id,
                        pc.dependency_id,
                        pc.sub_dependency_id,
                        sum(amount)::numeric price_unit
                    from additional_payments_line l, program_code pc
                    where l.program_code_id = pc.id
                        and l.payroll_id = %s
                    group by l.program_code_id, pc.dependency_id, pc.sub_dependency_id
                """
            if query_program_code and not is_check:
                self.env.cr.execute(query_program_code, (payment.get('employee_payroll_id'),))
                # Para cada código programático
                for vals in self.env.cr.dictfetchall():
                    # Inicialización de valores
                    vals.update({
                        'exclude_from_invoice_tab': True,
                        'conac_move': True,
                        'payment_id': payment.get('id'),
                        'ref': payment.get('ref'),
                    })
                    # Crédito: Cuenta 825.001.001.001
                    line_credit = vals.copy()
                    line_credit.update({
                        'account_id': payment.get('paid_credit_account_id'),
                        'coa_conac_id': payment.get('conac_paid_credit_account_id'),
                        'price_unit': -vals.get('price_unit'),
                    })
                    lines.append(line_credit)
                    # Debito: Cuenta 826.001.001.001
                    line_debit = vals.copy()
                    line_debit.update({
                        'account_id': payment.get('paid_debit_account_id'),
                        'coa_conac_id': payment.get('conac_paid_debit_account_id'),
                        'price_unit': vals.get('price_unit'),
                    })
                    lines.append(line_debit)
            # Creación de los apuntes contables
            self.env['account.move.line'].insert_sql(move_id, lines, name=name)
            # logging.info("Obteniendo account.move ...")
            # move_id = self.env['account.move'].browse([move_id])
            # logging.info("Ok")
            # Se publican los asientos contables
            # logging.info("Publicando los asientos contables ...")
            self.env['account.move']._post(move_id, payment.get('journal_id'))
            # logging.info("Ok")
            move_name = payment_name
            # move_name =  self._get_move_name_transfer_separator().join(payment_name)
            # Actualización de los objetos pago
            query_update = """
                update account_payment set state = 'posted',
                    payment_state = 'posted',
                    name = %s,
                    move_name = %s,
                    no_validate_payment = %s
                where id = %s
            """
            self.env.cr.execute(query_update, (payment_name, move_name, validate, payment.get('id')))
        # logging.info("Ok")

    def paymet_reject(self, payment_id, reason_for_rejection):
        query_update = """
            update account_payment set state = 'cancelled',
                payment_state = 'rejected',
                reason_for_rejection = %s
            where id = %s
            returning batch_folio, amount
        """
        self.env.cr.execute(query_update, (reason_for_rejection, payment_id))
        # Update batch folio
        batch_folio, amount = (self.env.cr.fetchall() or [(None, None)])[0]
        if batch_folio:
            self.env['batch.management'].update_number_of_payments_rejected(str(batch_folio), amount)

    def _get_payment_data(self, payment_ids):
        query_move = """
            select
                p.id,
                p.partner_id,
                p.communication as ref,
                p.payment_date,
                p.journal_id,
                p.amount price_unit,
                p.employee_payroll_id,
                p.payment_method_id,
                p.dependancy_id,
                p.sub_dependancy_id,
                p.fornight,
                p.folio,
                p.payroll_request_type,
                p.l10n_mx_edi_payment_method_id,
                p.check_folio_id,
                p.payment_state account_payment_state,
                j.default_credit_account_id,
                j.paid_credit_account_id,
                j.conac_paid_credit_account_id,
                j.paid_debit_account_id,
                j.conac_paid_debit_account_id,
                p.is_payroll_payment_reissue,
                p.payment_request_type,
                p.batch_folio
            from account_payment p, account_journal j
            where p.journal_id = j.id
            and p.id in %s
        """
        self.env.cr.execute(query_move, (tuple(payment_ids),))
        return self.env.cr.dictfetchall()

    def _get_type_of_payment(self, payment):
        payment_method, payment_type = None, None

        # Get payment_method
        payment_method_id = payment.get('l10n_mx_edi_payment_method_id')
        if payment_method_id == self.env.ref('l10n_mx_edi.payment_method_cheque').id:
            payment_method = 'check'
        elif payment_method_id == self.env.ref('l10n_mx_edi.payment_method_transferencia').id:
            payment_method = 'transfer'

        # Get payment type
        if payment.get('payment_request_type') == 'payroll_payment':
            # Payroll Payments
            payment_type = payment.get('payroll_request_type')
        elif payment.get('is_payment_request'):
            # General Payments
            payment_type = 'payment_request'

        return payment_method, payment_type

    def _get_payment_name(self, payment, payment_type):
        name = ''
        if payment_type != 'payment_request':
            name = "Pago de nómina quincena %s %s" % (
                payment.get('fornight', ''),
                payment.get('payment_date').strftime('%Y')
            )
        return name

    def _update_program_code(self, payment, program_code_data):
        lines = []
        for vals in program_code_data:
            common_values = {
                **vals,
                'exclude_from_invoice_tab': True,
                'conac_move': True,
                'partner_id': payment.get('partner_id'),
                'ref': payment.get('ref'),
                'payment_id': payment.get('id'),
            }
            # Crédito: Cuenta 825.001.001.001
            lines.append({
                **common_values,
                'account_id': payment.get('paid_credit_account_id'),
                'coa_conac_id': payment.get('conac_paid_credit_account_id'),
                'price_unit': -vals['price_unit'],
            })
            # Debito: Cuenta 826.001.001.001
            lines.append({
                **common_values,
                'account_id': payment.get('paid_debit_account_id'),
                'coa_conac_id': payment.get('conac_paid_debit_account_id'),
                'price_unit': vals['price_unit'],
            })

        return lines

    def _get_move_lines_payment_request(self, payment):
        return {}

    def _get_move_lines_payroll_payment(self, payment):
        query_program_code = """
            select l.program_code_id program_code_id,
                pc.dependency_id,
                pc.sub_dependency_id,
                sum(amount)::numeric price_unit
            from preception_line l, preception p, program_code pc
            where l.preception_id = p.id
            and l.program_code_id = pc.id
            and p.is_net_pay is True
            and l.payroll_id = %s
            group by l.program_code_id, pc.dependency_id, pc.sub_dependency_id
        """
        self.env.cr.execute(query_program_code, (payment.get('employee_payroll_id'),))
        program_code_data = self.env.cr.dictfetchall()

        return self._update_program_code(payment, program_code_data)

    def _get_move_lines_additional_benefits(self, payment):
        query_program_code = """
            select l.program_code_id program_code_id,
                pc.dependency_id,
                pc.sub_dependency_id,
                sum(amount)::numeric price_unit
            from additional_payments_line l, program_code pc
            where l.program_code_id = pc.id
            and l.payroll_id = %s
            group by l.program_code_id, pc.dependency_id, pc.sub_dependency_id
        """
        self.env.cr.execute(query_program_code, (payment.get('employee_payroll_id'),))
        program_code_data = self.env.cr.dictfetchall()

        return self._update_program_code(payment, program_code_data)

    def _get_account_id(self, payment, payment_method):
        account_id = None
        if payment_method == 'transfer':
            # General Payment
                partner_id = self.env['res.partner'].browse([payment.get('partner_id')])
                account_id = partner_id.property_account_payable_id
        elif payment_method == 'check':
            account_id = self.env['account.catalog.type']._get_nominal_param('normal')
        return account_id

    def _get_move_lines(self, payment, payment_method, payment_type):
        lines = []
        # Débito: Cuenta de por pagar de la Solicitud de Pago
        account_id = self._get_account_id(payment, payment_method)
        lines.append({
            'price_unit': payment.get('price_unit'),
            'amount_residual': payment.get('price_unit'),
            'date_maturity': payment.get('payment_date'),
            'account_id': account_id.id,
            'dependency_id': payment.get('dependancy_id'),
            'sub_dependency_id': payment.get('sub_dependancy_id'),
            'payment_id': payment.get('id'),
            'ref': payment.get('ref'),
        })
        # Crédito: Cuenta de bancos
        lines.append({
            'price_unit': -payment.get('price_unit'),
            'amount_residual': -payment.get('price_unit'),
            'date_maturity': payment.get('payment_date'),
            'account_id': payment.get('default_credit_account_id'),
            'payment_id': payment.get('id'),
            'ref': payment.get('ref'),
        })
        if payment_method == 'transfer':
            if payment_type == 'payment_request':
                lines += self._get_move_lines_payment_request(payment)
            elif payment_type == 'payroll_payment':
                lines += self._get_move_lines_payroll_payment(payment)
            elif payment_type == 'additional_benefits':
                lines += self._get_move_lines_additional_benefits(payment)
        return lines

    def payment_post(self, payment_ids, ref=None):
        batch_management = self.env['batch.management']

        for payment in self._get_payment_data(payment_ids):

            payment_method, payment_type = self._get_type_of_payment(payment)

            if payment.get('account_payment_state') == 'for_payment_procedure':
                batch_folio = str(payment.pop('batch_folio'))
                payment.update({'type': 'entry'})
                if ref:
                    payment.update({'ref': ref})
                move_id = self.env['account.move'].insert_sql([payment])[0]

                name = self._get_payment_name(payment, payment_type)

                lines = self._get_move_lines(payment, payment_method, payment_type)
                self.env['account.move.line'].insert_sql(move_id, lines, name=name)

                self.env['account.move']._post(move_id, payment.get('journal_id'))

                move_name = payment_name = self.env['ir.sequence'].next_by_code(
                    'account.payment.supplier.invoice',
                    sequence_date=payment.get('payment_date')
                )

                query_update = """
                    update account_payment set state = 'posted',
                        payment_state = 'posted',
                        name = %s,
                        move_name = %s,
                        no_validate_payment = True
                    where id = %s
                """
                self.env.cr.execute(query_update,
                    (payment_name, move_name, payment.get('id'))
                )

                batch_management.update_number_of_payments_validated(batch_folio, payment.get('price_unit'))

                if payment_method == 'check':
                    query = "update check_log set status = 'Charged' where id = %s"
                    self.env.cr.execute(query, (payment.get('check_folio_id'),))
