# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
import logging
  
class RelationModules(models.Model):
    _name = 'siif.budget.mgmt.relation.modules'
    _description = 'Relación modulos'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'name'
    _order = "name asc"

    name = fields.Char(string='Nombre', tracking=True)
    description = fields.Text(string='Descripción', tracking=True, required=True)
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('confirmed', 'Confirmado'),
    ],default='draft', required=True, string='Estado', tracking=True)

    _sql_constraints = [('name', 'unique(name)', ('El modulo debe ser único'))]