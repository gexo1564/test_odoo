# -*- coding: utf-8 -*-
from datetime import datetime, date
from odoo import api, fields, models, _
from odoo.tools.misc import groupby


class Subdependency(models.Model):
    _inherit = 'sub.dependency'

    _order = 'sub_dependency asc'

    @api.model
    def get_subdependencies_for_account(self, account, dep, move_line_domain):
        '''
        (New) Returns all of the sub dependency accounts that have had 
        account.move.line entries. Will return empty list if the account should
        not be grouped by subdependencies
        :param move_line_domain An extra domain that can be added to the search
        for account.move.lines. Typically this param can restrict the move lines
        for certain periods
        '''
        if not account.dep_subdep_flag:
            return []

        domain = [
            ('account_id', '=', account.id),
            ('dependency_id', '=', dep.id)
        ] + move_line_domain

        sub_deps = self.env['account.move.line'].search(domain).sorted(lambda aml: aml.sub_dependency_id.sub_dependency if aml.sub_dependency_id else '0')\
            .mapped('sub_dependency_id')
        empty_sub_dep = self.env['account.move.line'].search(
            domain + [('sub_dependency_id', '=', False)])

        if empty_sub_dep:
            sub_deps += self.env['sub.dependency'].browse([None])

        return sub_deps
