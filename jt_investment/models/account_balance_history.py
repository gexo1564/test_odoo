import calendar
from odoo import models, fields, api, _
from datetime import date, datetime
from odoo.exceptions import UserError, ValidationError


class AccountBalanceHistory(models.Model):
    _name = 'account.balance.history'
    _description = "Account Balance History"

    name = fields.Char("Account Name")
    account_number_id = fields.Many2one('res.partner.bank')
    date = fields.Date("Date")
    balance = fields.Float("Balance", digits=(15, 2))

    # Obtiene el saldo de la cuenta bancaria del último registro en la fecha seleccionada
    def get_balance_history_in_date(self, date, account):

        obj = self.search([('date', '=', date), ('account_number_id.acc_number', '=', account)])

        if len(obj) == 0:
            raise UserError(
                _('"No se encontraron registros de saldos para la fecha seleccionada'))

        if len(obj) > 0:
            # ordena los elementos del objeto obj por el campo write_date
            obj = obj.sorted(key=lambda r: r.write_date, reverse=True)
            balance = obj[0].balance
            return balance
        else:
            balance = obj[0].balance

            return balance

    # Obtiene el valor promedio de los saldos de los registros en el mes y año seleccionado
    def get_balance_history_in_month(self, month, year, account):
        # Realiza la busqueda en el rango de primer dia y ultimo dia del mes
        records = self.search([('account_number_id.acc_number', '=', account), ('date', '>=', date(
            year, month, 1)), ('date', '<=', date(year, month, calendar.monthrange(year, month)[1]))])


        if len(records) == 0:
            raise UserError(
                _('"No se encontraron registros de saldos para la fecha seleccionada'))

        # Sacamos el promedio de los saldos de los registros encontrados
        total_amount = 0
        for rec in records:
            total_amount += rec.balance
        average = total_amount / len(records)

        return average
