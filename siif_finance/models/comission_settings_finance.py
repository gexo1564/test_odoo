from odoo import models, fields,api,_
from odoo.exceptions import ValidationError
from datetime import datetime, timedelta
from lxml import etree

class ComissionSettingsFinance(models.Model):

    _name = 'comission.settings.finance'
    _description = "Comission Settings Finance"
    _rec_name = 'comission_description_mov'

    income_bank_journal_id = fields.Many2one('account.journal', "Bank")
    income_bank_account = fields.Many2one(related="income_bank_journal_id.bank_account_id", string="Bank Account", store=True)
    comission_type = fields.Char('Comission Type')
    comission_identifier = fields.Char('Comission Identifier')
    comission_description_mov = fields.Char('Comission Description Mov')
    comission_iva_identifier = fields.Char('Comission IVA Identifier')
    comission_iva_description_mov = fields.Char('Comission IVA Description Mov')
    type_of_currency = fields.Selection(
        [('national', 'National Currency'), ('foreign', 'Foreign Currency')])
    comission_line_ids = fields.One2many('comission.settings.finance.line','comission_settings_finance_id','Definition')
    egress_key_id = fields.Many2one('egress.keys', "Egress Key")
    type_registry_spent = fields.Selection([('ledger_account', 'Ledger account'),('authorized_budget', 'Authorized budget'),('extra_income','Extraordinary income')])
    type_authorized_budget = fields.Selection([('one_program_code','One program code'),('apportionment','Apportionment'),('availability','Availability')])
    #sum_percentage = fields.Float("Sum Percentage", compute="_compute_percentage")
    item_key_id = fields.Many2one('expenditure.item', "Expenditure Item")
    account_id = fields.Many2one('account.account','Account')
    dependency_id = fields.Many2one('dependency','Dependency')
    sub_dependency_id = fields.Many2one('sub.dependency', 'Sub Dependency')
    afectation_account_id = fields.Many2one('association.distribution.ie.accounts', "Afectation Account")
    resource_origin_id = fields.Many2one('resource.origin', "Resource origin")
    concept_id = fields.Many2one('comission.concept','Concept')
    currency_related = fields.Char(related="income_bank_journal_id.currency_id.name", string="Currency")
    bank = fields.Many2one(related="income_bank_journal_id.bank_account_id.bank_id",string="Bank", store=True)
    short_program_code = fields.Char("Short program code")
    automatic_adequation = fields.Boolean("Automatic adequation")

    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        res = super().fields_view_get(view_id=view_id,view_type=view_type,toolbar=toolbar,submenu=submenu)
        doc = etree.XML(res['arch'])
        is_group_approval_user = self.env.user.has_group('jt_payroll_payment.group_approval_user')
        is_group_consult_user = self.env.user.has_group('jt_payroll_payment.group_consult_user')

        if is_group_approval_user or is_group_consult_user:
            for node in doc.xpath("//" + view_type):
                node.set('create','0')
                node.set('delete','0')
                node.set('export','0')
                node.set('export_xlsx','0')

        if is_group_consult_user:
            for node in doc.xpath("//" + view_type):
                node.set('edit','0')

        res['arch'] = etree.tostring(doc)
        return res

    @api.constrains('item_key_id')
    def _check_item_key(self):
        if (self.type_authorized_budget=='apportionment' or self.type_authorized_budget=='availability') and not self.item_key_id:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("Por favor configure la partida de gasto"))
            else:
                raise ValidationError(_("Please configure item key"))

    @api.constrains('comission_line_ids')
    def _check_program_code(self):
        if (self.type_authorized_budget=='one_program_code' or self.type_registry_spent=='extra_income') and not self.comission_line_ids.program_code_id:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("Por favor configure el código programático"))
            else:
                raise ValidationError(_("Please configure the program code"))

    @api.constrains('dependency_id')
    def _check_dep_subdep(self):
        if (self.type_registry_spent=='extra_income' ) and not self.dependency_id and not self.sub_dependency_id:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("Por favor configure la dependencia y subdependencia"))
            else:
                raise ValidationError(_("Please configure dependency and subdependency"))

    @api.constrains('afectation_account_id')
    def _check_afectation_account(self):
        if self.type_registry_spent=='extra_income' and not self.afectation_account_id:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("Por favor configure la cuenta de ingresos"))
            else:
                raise ValidationError(_("Please configure the income account"))

        if self.type_registry_spent=='extra_income' and not self.afectation_account_id.ie_account_line_ids.filtered(lambda x:x.main_account == True):
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("Por favor configure la cuenta principal de la cuenta de ingresos"))
            else:
                raise ValidationError(_("Please configure the main account of income account"))
        
        if self.type_registry_spent=='extra_income' and not self.afectation_account_id.ie_account_line_ids.filtered(lambda x:x.main_account == True).account_id.revenue_recognition_account_id:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("Por favor configure la cuenta de reconocimiento de ingresos"))
            else:
                raise ValidationError(_("Please configure the revenue recognition account"))

    @api.onchange('comission_line_ids')
    def _compute_domain(self):
        self.comission_line_ids.item_related = self.item_key_id.id
        self.comission_line_ids.year_related = datetime.today().year
        self.comission_line_ids.origin_related = self.resource_origin_id

    @api.onchange('type_registry_spent')
    def _compute_registry_spent(self):
        if self.type_registry_spent=='extra_income':
            origin =  self.env['resource.origin'].search([('key_origin', '=', '01')])
            self.resource_origin_id = origin.id
        if self.type_registry_spent=='authorized_budget':
            origin =  self.env['resource.origin'].search([('key_origin', '=', '00')])
            self.resource_origin_id = origin.id


    def convert_code(self):
        for record in self:
            program_line_vals = []
            if record.short_program_code not in ('', None):
                program_code = record.env['program.code'].convert_program_code(record.short_program_code, datetime.today())
                program_code_id = record.env['program.code'].search([('porgram_code', '=', program_code)])

                if type(program_code_id) == int:
                    program_code_vals={
                        'program_code_id': program_code_id,
                        'comission_settings_finance_id': record.id,

                    }

                    program_line_vals.append((0,0,program_code_vals))
                    
                    record.comission_line_ids = program_line_vals


            else:
                if self.env.user.lang == 'es_MX':
                    raise ValidationError(_("No hay código corto"))
                else:
                    raise ValidationError(_("There arent code"))



class ComissionSettingsFinanceLine(models.Model):
    
    _name = 'comission.settings.finance.line'
    _description = "Comission Settings Finance Line"
    
    comission_settings_finance_id = fields.Many2one('comission.settings.finance',string='Comission',ondelete='cascade')
    account_id = fields.Many2one("account.account",'Accounts')
    program_code_id = fields.Many2one('program.code',"Program Code")
    percentage_program_code = fields.Float('%')
    item_related = fields.Integer(string="Item id")
    year_related = fields.Char('Year related')
    origin_related = fields.Integer(string="Origin id")

    '''@api.model_create_multi
    def create(self,vals):
        line = super(ComissionSettingsFinanceLine, self).create(vals)
        for res in line:
            if res.comission_settings_finance_id.item_key_id:
                res.item_related=res.comission_settings_finance_id.item_key_id.id

        return line'''

