# -*- coding: utf-8 -*-

import logging
from this import d
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

from datetime import datetime, date
class ProgramCode(models.Model):

    _inherit = 'program.code'

    @api.model
    def fields_get(self, fields=None, attributes=None):
        no_selectable_fields = ['custom_project_program_ids', 'conacyt_code', 'program_code_copy', 'desc_program', 'desc_sub_program', 'desc_dependency',
                                'desc_sub_dependency', 'desc_item', 'desc_resource_origin', 'desc_institutional_activity', 'desc_budget_program_conversion',
                                'desc_conversion_item', 'desc_expense_type', 'desc_location', 'desc_project_type', 'desc_stage', 'id',
                                'key_unam_digit_id', 'name_agreement', 'name_portfolio', 'location_id', 'project_number', 'conacyt_project_id', 'sg_id', 'search_key','parent_program_id'
                                ]
        no_sortable_field = ['parent_program_id', 'conacyt_code', 'parent_program_id', 'conacyt_project_id']

        res = super(ProgramCode, self).fields_get()
        for key, value in res.items():
            if key in no_selectable_fields:
                value.update({'selectable': False})

        for key, value in res.items():
            if key in no_sortable_field:
                value.update({'sortable': False})
        return res

    @api.model
    def convert_program_code(self, program_code, date_code=None):
        if not date_code:
            date_code = datetime.now()
        else:
            date_code = datetime.strptime(date_code, "%Y-%m-%d")
        anio = ""
        programa = program_code[0:2]
        subprograma = program_code[2:4]
        dependencia = program_code[4:7]
        subdependencia = program_code[7:9]
        partida_gasto = program_code[9:12]
        origen_recurso = "0" + program_code[12]
        digito_verificador = "0" + program_code[13]
        actividad_institucional = ""
        conversion_programa = ""
        conversion_partida = ""
        tipo_gasto = ""
        ubicacion_geografica = ""
        clave_cartera = "0000"
        tipo_proyecto = "00"
        numero_proyecto = "000000"
        etapa = "00"
        convenio = "00"
        numero_convenio = "000000"

        # Validación de los valores recibidos
        # Validación del programa
        pr = self.env['program'].sudo().search([('key_unam', '=', programa)], limit=1)
        if not pr:
            raise ValidationError("El programa '%s' no es válido." % programa)

        # Validación de la Dependecia
        dp = self.env['dependency'].sudo().search([('dependency', '=', dependencia)], limit=1)
        if not dp:
            raise ValidationError("La dependencia '%s' no es válida." % dependencia)

        # Validación de la sub dependencia
        sd = self.env['sub.dependency'].sudo().search(
            [('sub_dependency', '=', subdependencia), ('dependency_id', '=', dp.id)],
            limit=1
        )
        if not sd:
            raise ValidationError("La subdependencia '%s' no es válida." % subdependencia)

        # Validación del sub programa
        sp =  self.env['sub.program'].sudo().search(
            [
                ('sub_program', '=', subprograma),
                ('unam_key_id', '=', pr.id),
                ('dependency_id', '=', dp.id),
                ('sub_dependency_id', '=', sd.id)
            ],limit=1
        )
        if not sp:
            raise ValidationError("El sub programa '%s' no es válido." % subprograma)

        # Validación de la partida
        par = self.env['expenditure.item'].sudo().search(
            [('item', '=', partida_gasto)],
            limit=1
        )
        if not par:
            raise ValidationError("La partida '%s' no es válida." % partida_gasto)
        # Validación del origen del recurso
        origen = self.env['conversion.resource.origin'].get_resource_origin(subprograma, partida_gasto)
        origen_recurso_gasto = "00" # Se consideran todos los códigos de gasto como si fueran origen 00
        if not list(filter(lambda x: (origen_recurso_gasto == x['resource_origin']), origen)):
            raise ValidationError("El origen del recurso es erróneo")
        # Validación del digito verificador
        dv = self.env['verifying.digit'].check_digit_from_values(
            programa,
            subprograma,
            dependencia,
            subdependencia,
            partida_gasto
        )
        if dv != digito_verificador:
            raise ValidationError("El dígito verificador es erróneo, se esperaba el %s y se encontró el %s" % (dv, digito_verificador))
        # GENERACIÓN DE LOS NUEVOS VALORES
        # Obtención del Año
        anio = date_code.strftime("%Y")

        # Obtención de la Actividad Institucional
        ai = self.env['conversion.institutional.activity'].sudo().search(
            [('program_id.key_unam', '=', programa)],
            limit=1
        )
        if ai:
            actividad_institucional = ai.institutional_activity_id.number
        else:
            raise ValidationError("No se pudo encontrar una Actividad Institucional asociada al programa: (%s)" %(programa))

        # Obtención del CONPA
        conpa = self.env['departure.conversion'].get_conpa(partida_gasto, date_code)
        if len(conpa) == 1:
            conversion_partida = conpa[0]['conpa']
        elif len(conpa) > 1:
            raise ValidationError("Revise los catálogos, se encontró más de un CONPA asociado a la partida (%s)." %(partida_gasto))
        else:
            raise ValidationError("No se pudo encontrar un CONPA asociado la partida: (%s)" %(partida_gasto))

        # Obtención del CONPP
        conpp = self.env['budget.program.conversion'].sudo().search(
            [
                '&',
                    '&',
                        '&',
                            ('start_date', '<=', date_code),
                            '|',
                                ('end_date', '=', False),
                                ('end_date', '>=', date_code),
                        ('program_key_id.name', '=', programa[0]),
                    ('conversion_key_id.conversion_key', '=', conversion_partida)
            ]
        )
        if conpp:
            if len(conpp) != 1:
                raise ValidationError("Revise los catálogos, se encontró más de un posible CONPP.")
            conversion_programa = conpp[0].shcp.name
        else:
            __, conpp = self.env['conversion.conpp'].get_conpp(programa, subprograma, dependencia, subdependencia, partida_gasto)
            if not conpp:
                raise ValidationError("No se pudo encontrar el CONPP: (%s)" %(partida_gasto))
            conversion_programa = conpp

        # Obtención del tipo de gasto
        __, tg = self.env['conversion.expense.type'].get_expense_type(partida_gasto)
        if not tg:
            raise ValidationError("Error al obtener el Tipo de Gasto (TG)")
        tipo_gasto = tg

        # Obtención de la Ubicación Geografica
        ug = self.env['sub.program'].sudo().search(
            [
                ('unam_key_id.key_unam', '=', programa),
                ('sub_program', '=', subprograma),
                ('dependency_id.dependency', '=', dependencia),
                ('sub_dependency_id.sub_dependency', '=', subdependencia),
            ],
            limit=1
        )
        if ug and ug.geographic_location_id.state_key:
            ubicacion_geografica = ug.geographic_location_id.state_key
        else:
            raise ValidationError("No se pudo encontrar la ubicación Geográfica")

        # Conversión del código programático
        return anio + programa + subprograma + dependencia + subdependencia + partida_gasto + \
            digito_verificador + origen_recurso + actividad_institucional + conversion_programa + \
            conversion_partida + tipo_gasto + ubicacion_geografica + \
            clave_cartera + tipo_proyecto + numero_proyecto + etapa + convenio + numero_convenio

    def test_convert_program_code(self):
        vector_test = [
            ['10014110115910', '2023100141101159000125305E0101310201090000000000000000000000'],
        ]

        error = 0
        for v in vector_test:
            result = self.convert_program_code(v[0])
            if result != v[1]:
                logging.critical(result)
                logging.critical(v[1])
                logging.critical("Failed.")
                error += 1
            else:
                logging.info("ok")
        if error == 0:
            logging.info("SUCCESSFUL TEST!")
        else:
            logging.critical("WRONG TEST!")

    def validate_conversion_origin_resource(self, item, sub_program, resource_origin, validity_date = datetime.now()):
        # Validación del origen del recurso
        origen = self.env['conversion.resource.origin'].get_resource_origin(sub_program, item, validity_date)
        origen_recurso_gasto = resource_origin
        if not list(filter(lambda x: (origen_recurso_gasto == x['resource_origin']), origen)):
            return False

    def validate_conversion_activity_institutional(self, program, validity_date = datetime.now()):
        # Obtención de la Actividad Institucional
        ai = self.env['conversion.institutional.activity'].sudo().search(
            [
                '&',('program_id.key_unam', '=', program),
                '&', ('start_date', '<=', validity_date),
                    '|', ('end_date', '=', False), ('end_date', '>=', validity_date)
            ],
            limit=1
        )
        if not ai:
            return False

    def validate_conpp_conversion(self, program, sub_program, dep, subdep, item, validity_date = datetime.now()):
        __, conpp = self.env['conversion.conpp'].get_conpp(program, sub_program, dep, subdep, item)
        if not conpp:
            return False

    def validate_type_expense_conversion(self, item, validity_date = datetime.now()):
         # Obtención del tipo de gasto
        __, tg = self.env['conversion.expense.type'].get_expense_type(item, validity_date)
        if not tg:
            return False