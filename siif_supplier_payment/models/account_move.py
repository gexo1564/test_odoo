# -*- coding: utf-8 -*-
import logging
import json
from lxml import etree
from odoo import models, fields, api, _
from datetime import datetime
from odoo.exceptions import UserError, ValidationError

from odoo.addons.siif_supplier_payment.tools.utils import Dictionary

ACCOUNT_MOVE_LINE = 'account.move.line'
EXPENDITURE_BUDGET_LINE = 'expenditure.budget.line'
MONEY_FUNDS_REQUEST_MINISTRATION = 'money.funds.request.ministration'
PAYMENT_PARAMETERS = 'payment.parameters'

class AccountMove(models.Model):
    _inherit = 'account.move'

    # Cuenta por pagar
    previous_number = fields.Many2one('account.topay', string='Previous Number')
    domain_account_to_pay = fields.Char(compute="_compute_domain_account_to_pay", store=False)
    # Estados del pago
    payment_state = fields.Selection(selection=[
        ('draft', 'Draft'),
        ('registered', 'Registered'),
        ('to_be_approved_dgf', 'To be Approved DGF'),
        ('approved_payment_dgf', 'Approved DGF'),
        ('approved_payment', 'Approved for payment'),
        ('for_payment_procedure', 'For Payment Procedure'),
        ('paid', 'Paid'),
        ('payment_not_applied', 'Payment not Applied'),
        ('origin_account_change', 'Origin account change'),
        ('done', 'Done'),
        ('rejected', 'Rejected'),
        ('rejected_dgf', 'Rejected DGF'),
        ('cancel', 'Cancel'),
        ('payment_method_cancelled', 'Payment method cancelled'),
        ('rotated','Rotated'),
        ('assigned_payment_method','Assigned Payment Method'),
        ('request_return','Request Return'),
        ('with_returns', 'With Returns')
    ], default='draft', copy=False, tracking=True)
    # Tipo de documento
    document_type = fields.Selection(
        [('national', 'National Currency'), ('foreign', 'Foreign Currency')],
        default='national'
    )
    # Dominios
    domain_operation_type = fields.Char(compute="_compute_domain_operation_type", store=False)
    domain_dependency = fields.Char(default=lambda self: self._get_domain_dependency(), store=False)
    domain_sub_dependency = fields.Char(compute="_compute_domain_sub_dependency", store=False)
    domain_policy_keys = fields.Char(default=lambda self: self._get_domain_policy_keys(), store=False)

    # Líneas de donde se extrajo el recurso
    account_to_pay_link_ids = fields.One2many('account.to.pay.line.links','account_move_id')
    # Método de Emisión de pago
    payment_issuance_method = fields.Selection(
        [('manual', 'Manual'),('automatic', 'Automatic'),('check', 'Check')],
        string='Record Type'
    )
    is_ministration = fields.Boolean('Ministration', default=False)
    is_refund = fields.Boolean('Refunds', default=False)
    # Documentación Soporte
    support_documentation = fields.Many2many('ir.attachment', string='Support Documentation')

    set_readonly_into_payment = fields.Boolean(string='Set Readonly', copy=False)
    num_account_returns = fields.Integer('All account returns',compute='_count_account_return',default=0)
    bool_account_returns = fields.Boolean('Check account returns',compute='_check_account_returns_count',default=False)
    check_folio = fields.Integer(string="Check Folio")
    exchange_rate = fields.Monetary(string='Exchange Rate', default=1)

    @api.onchange('currency_id')
    def _get_exchange_rate_assigned(self):
        for rec in self:
            if rec.currency_id:
                rec.exchange_rate = 1/rec.currency_id.rate
            else:
                rec.exchange_rate= 1
    @api.depends('dependancy_id', 'sub_dependancy_id', 'operation_type_id')
    def _compute_domain_account_to_pay(self):
        params = self.env[PAYMENT_PARAMETERS]
        for record in self:
            if record.type != 'in_invoice':
                record.domain_account_to_pay = json.dumps([])
                continue
            dep = record.dependancy_id
            sd = record.sub_dependancy_id
            if dep and sd:
                domain = []
                domain.append(('state', '=', 'approved'))
                domain.append(('previous_number', '!=', ''))
                domain.append(('available', '>', 0))
                domain.append(('document_type', '=', record.document_type))
                if record.operation_type_id == params.get_param('ministration_op_type'):
                    ministration = self.env[MONEY_FUNDS_REQUEST_MINISTRATION].search([
                        ('dependency_id', '=', dep.id),
                        ('sub_dependency_id', '=', sd.id),
                        ('state', '=', 'deposited'),
                    ])
                    if ministration:
                        domain.append(('id', 'in', [m.account_topay_id.id for m in ministration]))
                record.domain_account_to_pay = json.dumps(domain)
            else:
                record.domain_account_to_pay = json.dumps([('id', '=', False)])

    @api.model
    def create(self,vals):
        if self._context.get('default_type')=='in_invoice' and vals.get('payment_state')=='draft':
            if self.env['res.currency'].search([('id','=',vals.get('currency_id'))]).name == 'MXN':
                vals.update({'journal_id':self.env['payment.journals'].search([('type_of_currency','=','national')]).journal_id.id})
            else:
                vals.update({'journal_id':self.env['payment.journals'].search([('type_of_currency','=','foreign')]).journal_id.id})

        res=super(AccountMove,self).create(vals)
        return res

    @api.depends('upa_key', 'document_type', 'payment_issuance_method')
    def _compute_domain_operation_type(self):
        for record in self:
            if record.type != 'in_invoice':
                record.domain_operation_type = json.dumps([])
                continue
            domain = [('id', '=', False)]
            if record.upa_key and record.document_type:
                domain = [
                    ('type', '=', record.upa_key.type),
                    ('currency_type', '=', record.document_type),
                    ('payment_issuance_method', '=', record.payment_issuance_method),
                ]
                if record.is_refund:
                    refund_op_type = self.env[PAYMENT_PARAMETERS].get_param('refunds_op_type')
                    domain.append(('id', '=', refund_op_type.id))
                    if not record.operation_type_id:
                        record.operation_type_id = refund_op_type
            record.domain_operation_type = json.dumps(domain)

    def _get_domain_dependency(self):
        dep = self.env.user.dependency_payment_ids.ids
        dep += [d.id for d in self.env.user.sub_dependency_payment_ids.dependency_id]
        return json.dumps([('id', 'in', dep)])

    def _get_domain_policy_keys(self):
        return json.dumps([('id', 'in', self.env.user.permission_upa_payment.ids)])

    @api.depends('dependancy_id')
    def _compute_domain_sub_dependency(self):
        for record in self:
            domain = [('dependency_id', '=', record.dependancy_id.id)]
            if record.dependancy_id.id not in self.env.user.dependency_payment_ids.ids:
                domain.append(('id', 'in', self.env.user.sub_dependency_payment_ids.ids))
            record.domain_sub_dependency = json.dumps(domain)

    @api.onchange('upa_key', 'document_type', 'payment_issuance_method')
    def _onchange_payment_issuance_method(self):
        self.operation_type_id = None

    @api.onchange('operation_type_id')
    def _compute_money_funds(self):
        if self.type != 'in_invoice':
            return
        parameters = self.env[PAYMENT_PARAMETERS]
        if self.operation_type_id:
            ministration_op_type = parameters.get_param('ministration_op_type')
            refunds_op_type = parameters.get_param('refunds_op_type')
            self.is_ministration = self.operation_type_id == ministration_op_type
            self.is_refund = self.operation_type_id == refunds_op_type
        else:
            self.is_ministration = False
            self.is_refund = False

    def get_balance_by_program_code(self):
        """
        Obtiene el monto moneda nacional de las afectaciones de cada código programático
        """
        lines_ids = self.invoice_line_ids
        program_code_ids = set([l.program_code_id for l in lines_ids.filtered(lambda l: l.program_code_id)])
        lines = []
        for program_code_id in program_code_ids:
            amount_total = sum(x.price_total for x in lines_ids.filtered(lambda l: l.program_code_id == program_code_id))
            if self.currency_id != self.company_id.currency_id:
                balance = self.currency_id._convert(amount_total, self.company_currency_id, self.company_id, self.date)
                amount_currency = abs(amount_total)
            else:
                balance = abs(amount_total)
                amount_currency = 0.0
            lines.append((program_code_id, balance, amount_currency))
        return lines

    edit_payment_state_css = fields.Html(string='CSS', sanitize=False, compute='_compute_edit_payment_state_css', store=False)
    #- Método que modifica el css de la vista
    @api.depends('payment_state')
    def _compute_edit_payment_state_css(self):
        for record in self:
            # se esconde el botón de Editar y se cambia el estilo de los botón Editar y Crear si se termina
            # la solicitud y se ha cargado el Reporte General de movimientos
            if record.is_payment_request and record.payment_state not in ('draft'):
                record.edit_payment_state_css = """
                    <style>
                        .o_form_view .o_form_statusbar > .o_statusbar_status{
                        max-width:85%;
                        }
                        .o_form_button_edit {
                            display: none !important;
                        }
                    </style>
                """
            else:
                record.edit_payment_state_css = ""

    def validate_available_account(self, lines, type = 'cc'):
        msgs = []
        # Diccionario para las cuentas contables por dependencia/sub_dependencia
        account_account = Dictionary()
        for account_id, dependency_id, sub_dependency_id, amount in lines:
            if account_id.dep_subdep_flag:
                if not dependency_id or not sub_dependency_id:
                    raise ValidationError("La dependencia/subdependencia es requerida por la cuenta")
            else:
                dependency_id, sub_dependency_id = None, None
            account_account.add_sum((account_id, dependency_id, sub_dependency_id), amount)
        # Validación de los disponibles por montos totales
        for (account_id, dependency_id, sub_dependency_id), amount in account_account.items():
            params = (account_id.id,)
            msg = ''
            query = """
                select coalesce(sum(credit-debit), 0)::numeric balance
                from account_move_line
                where account_id = %s and parent_state = 'posted'
            """
            if dependency_id and sub_dependency_id:
                query += """ and dependency_id = %s and sub_dependency_id = %s"""
                params += (dependency_id.id, sub_dependency_id.id)
                msg = " con dependencia: {0} y subdependencia: {1},".format(
                    dependency_id.dependency,
                    sub_dependency_id.sub_dependency
                )
            self.env.cr.execute(query, params)
            available =  (self.env.cr.fetchone() or [0.0])[0]
            if available < amount:
                msgs.append(self._get_insufficiency_message(
                    "la cuenta {0}{1}".format(account_id.code, msg),
                    available,
                    amount,
                    type,
                ))
        return msgs

    def validate_available_account_to_pay(self, lines):
        msgs = []
        account_topay = Dictionary()
        account_topay_pa = Dictionary()
        account_topay_ie = Dictionary()
        account_topay_cc = Dictionary()
        # Se agrupan las líneas por cuenta por pagar
        for line in lines:
            account_topay_id = line[0]
            account_topay.add_list(account_topay_id, line[1:])
        # Para cada cuenta por pagar se obtienen las agrupaciones por tipo linea
        for account_topay_id, lines in account_topay.items():
            for line in lines:
                resource_origin = None
                program_code_id, account_id, account_ie, dependency_id, sub_dependency_id, amount = line
                # Se obtiene el tipo de origen del recuros de la línea
                if program_code_id:
                    # Si tiene código programático y cuenta de ie (01)
                    if account_ie:
                        resource_origin = '01'
                    # Si tiene código programático y no tiene cuenta ie (00)
                    else:
                        resource_origin = '00'
                # Si no tiene código programático y tiene cuenta contable (05)
                elif account_id:
                    resource_origin = '05'
                # Se busca la línea según el origen del recurso
                if resource_origin == '00':
                    account_topay_pa.add_sum(program_code_id, amount)
                elif resource_origin == '01':
                    account_topay_ie.add_sum((program_code_id, account_ie), amount)
                elif resource_origin == '05':
                    account_topay_cc.add_sum((account_id, dependency_id, sub_dependency_id), amount)
            # Validación de las líneas de PA
            msgs += account_topay_id.get_available_pa(account_topay_pa.items())
            # Validación de las líneas de IE
            msgs += account_topay_id.get_available_ie(account_topay_ie.items())
            # Validación de las líneas de CC
            msgs += account_topay_id.get_available_cc(account_topay_cc.items())
        return msgs

    def update_available_account_to_pay(self, lines, move_id):
        account_topay = Dictionary()
        account_topay_pa = Dictionary()
        account_topay_ie = Dictionary()
        account_topay_cc = Dictionary()
        # Se agrupan las líneas por cuenta por pagar
        for line in lines:
            account_topay_id = line[0]
            account_topay.add_list(account_topay_id, line[1:])
        # Para cada cuenta por pagar se obtienen las agrupaciones por tipo linea
        for account_topay_id, lines in account_topay.items():
            for line in lines:
                resource_origin = None
                program_code_id, account_id, account_ie, dependency_id, sub_dependency_id, amount = line
                # Se obtiene el tipo de origen del recuros de la línea
                if program_code_id:
                    # Si tiene código programático y cuenta de ie (01)
                    if account_ie:
                        resource_origin = '01'
                    # Si tiene código programático y no tiene cuenta ie (00)
                    else:
                        resource_origin = '00'
                # Si no tiene código programático y tiene cuenta contable (05)
                elif account_id:
                    resource_origin = '05'
                # Se busca la línea según el origen del recurso
                if resource_origin == '00':
                    account_topay_pa.add_sum(program_code_id, amount)
                elif resource_origin == '01':
                    account_topay_ie.add_sum((program_code_id, account_ie), amount)
                elif resource_origin == '05':
                    account_topay_cc.add_sum((account_id, dependency_id, sub_dependency_id), amount)
            # Actualización del disponible de las líneas de PA
            res = account_topay_id.update_available_pa(account_topay_pa.items(), move_id)
            if res:
                return False, res
            # Actualización del disponible de las líneas de IE
            res = account_topay_id.update_available_ie(account_topay_ie.items(), move_id)
            if res:
                return False, res
            # Actualización del disponible de las líneas de CC
            res = account_topay_id.update_available_cc(account_topay_cc.items(), move_id)
            if res:
                return False, res
        return True, None

    def validate_get_available_program_code_pa(self, lines, quarter):
        budget = self.env[EXPENDITURE_BUDGET_LINE]
        msgs = []
        program_code_ids = self.program_code_tuple_to_dict(lines)
        for program_code_id, amount in program_code_ids.items():
            available = budget.get_available_by_program_code(program_code_id.id, quarter)
            available = round(available, 2)
            if available < amount:
                # Si la partida no admite sobregiros, se notifica al usuario que
                # la falta de disponibilidad.
                if not program_code_id.item_id.overdraft:
                    msgs.append(self._get_insufficiency_message(
                        "el código programático {0}".format(program_code_id.program_code),
                        available,
                        amount,
                        'pa',
                    ))
        return msgs

    def program_code_tuple_to_dict(self, lines):
        """
        Convierte una lista de tuples de los códigos programáticos y sus montos
        en un diccionario con el total de afectaciones por código programático
        Args:
        list ([(program_code_id, amount)]): Arreglo de tuplas con los registros de los código programaticos y el monto.
        Ej. [(program_code(1), 1000), (program_code(2), 1000)]
        """
        diccionario = {}
        for key, value in lines:
            diccionario[key] = diccionario.get(key, 0.0) + value
        return diccionario

    def _create_message_overdraft(self, msgs_ok):
        msg = '<ul class="o_mail_thread_message_tracking">'
        for m in msgs_ok:
            msg += (f"""<li>{m}</li>""")
        msg += '</ul>'
        return msg

    def update_available_program_code_pa(self, lines, quarter, move_id = None):
        budget = self.env[EXPENDITURE_BUDGET_LINE]
        msgs = []
        msgs_ok = []
        budget_lines = []
        program_code_ids = self.program_code_tuple_to_dict(lines)
        for program_code_id, amount in program_code_ids.items():
            ok, available, b_lines = budget.update_available_program_code(program_code_id.id, amount, quarter)
            budget_lines += b_lines
            if not ok:
                # Si la partida no admite sobregiros, se notifica al usuario que
                # la falta de disponibilidad.
                if not program_code_id.item_id.overdraft:
                    msgs.append(self._get_insufficiency_message(
                        "el código programático {0}".format(program_code_id.program_code),
                        available,
                        amount,
                        'pa'
                    ))
                # Se sobregira el código programático con el monto faltante
                else:
                    b_lines = budget.update_available_with_overdraft([{
                        'program_code_id': program_code_id.id,
                        'amount': amount
                    }], quarter=quarter)
                    msgs_ok.append("El código programático %s será aprobado con sobregiro de: $%s" % (
                        program_code_id.program_code,
                        format(amount - (available if available > 0 else 0), ",.2f")
                    ))
                    budget_lines += b_lines
        if msgs:
            return False, msgs
        # Si se trata de una solicitud de pago, se registran las líneas de presupuesto
        # afectadas y el monto. Esto para realizar devolución de presupuesto
        # en caso de cancelación.
        if move_id:
            budget_line_links = []
            for b_line in budget_lines:
                b_line.update({'account_move_id': move_id.id})
                budget_line_links.append((0, 0, b_line))
            move_id.budget_line_link_ids = budget_line_links
        if msgs_ok:
            return True, self._create_message_overdraft(msgs_ok)
        return True, None

    def _get_insufficiency_message(self, name, available, required, type = 'pa'):
        if type == 'pa':
            msg = 'Insuficiencia Presupuestal,'
        elif type == 'ie':
            msg = 'Insuficiencia de los Ingresos Extraordinarios,'
        elif type == 'cc':
            msg = 'Insuficiencia Contable,'
        msg += ' {0} tiene un disponible de: ${1} y se requiere: ${2}'
        return msg.format(name, format(available, ",.2f"), format(required, ",.2f"))

    def validate_available_account_ie(self, lines):
        lines_tmp = []
        for account_ie, program_code_id, amount in lines:
            # Validación de la suficiencia de la cuenta de ingresos extraordinarios
            main_account_id = account_ie.ie_account_line_ids.filtered(lambda x:x.main_account == True)
            if not main_account_id:
                raise UserError("La cuenta de Ingresos extraordinarios no tiene una cuenta principal asociada")
            account_id = main_account_id.account_id
            if account_id.dep_subdep_flag:
                dep, sd = program_code_id.dependency_id, program_code_id.sub_dependency_id
            else:
                dep, sd = None, None
            lines_tmp.append((account_id, dep, sd, amount))
        return self.validate_available_account(lines_tmp, 'ie')


    def validate_available_program_code_ie(self, lines, quarter, date=datetime.today()):
        item_ie = self.env[PAYMENT_PARAMETERS].get_param('item_ie').item
        budget = self.env[EXPENDITURE_BUDGET_LINE]
        msgs = []
        # Validación del disponible de la cuenta de IE
        msgs += self.validate_available_account_ie(lines)
        # Diccionario con los valores actualizados del disponible
        program_code = Dictionary()
        # Diccionario con los códigos sin disponible y el monto faltante
        program_code_without_available = Dictionary()
        # Diccionario con los acumulados por código programático
        program_code_original_ids = self.program_code_tuple_to_dict([(l[1], l[2]) for l in lines])
        # Se válida que el código programático tenga disponible
        for program_code_id, amount in program_code_original_ids.items():
            if not program_code.contains(program_code_id):
                available = budget.get_available_by_program_code(program_code_id.id, quarter)
                program_code.add_sum(program_code_id, available)
            # Se obtiene el disponible registrado en memoria
            available = program_code.get(program_code_id)
            # Sí el código no tiene disponible
            if available == 0:
                program_code_without_available.add_sum(program_code_id, amount)
            # Si el código tiene cuenta con el disponible requerido
            elif available >= amount:
                program_code.subtract_value(program_code_id, amount)
            # Si el código tiene disponible, pero no el requerido
            else:
                # Se consume el disponible del código
                program_code.subtract_value(program_code_id, available)
                # Se registra el código y el faltante
                program_code_without_available.add_sum(program_code_id, amount-available)
        # De los códigos que por si mismos no cuentan con disponible, se consulta
        # el disponible sus partidas 711 asociadas
        program_code_by_dep_sd = Dictionary(size=3)
        # Genera un nuevo diccionario, agrupando el monto faltante por dependencia/subdependencia
        # y el disponible total
        for program_code_id, amount in program_code_without_available.items():
            dep = program_code_id.dependency_id
            sd = program_code_id.sub_dependency_id
            program_code_by_dep_sd.add_sum((dep, sd), amount, 0)
            program_code_by_dep_sd.add_list((dep, sd), program_code_id, 2)
        # Se llena el segundo elemento elemento de la tupla del diccionario
        # que contienene el disponible total
        budget_id = self.env['expenditure.budget'].search([
            ('state', '=', 'validate'),
            ('from_date', '<=', date),
            ('to_date', '>=', date),
        ])
        for (dep, sd), amount in program_code_by_dep_sd.items():
            # Se obtiene los códigos programáticos de ie asociados a la dependencia/subdependencia
            program_code_ids = self.env['program.code'].search([
                ('budget_id', '=', budget_id.id),
                ('dependency_id', '=', dep.id),
                ('sub_dependency_id', '=', sd.id),
                ('item_id.item', '=', item_ie),
                ('resource_origin_id.key_origin', '=', '01'),
            ])
            for program_code_id in program_code_ids:
                if not program_code.contains(program_code_id):
                    available = budget.get_available_by_program_code(program_code_id.id, quarter)
                    program_code.add_sum(program_code_id, available)
                available = program_code.get(program_code_id)
                program_code_by_dep_sd.add_sum((dep, sd), available, 1)
            if not program_code_ids:
                program_code_by_dep_sd.add_sum((dep, sd), 0, 1)
        # Se recorre program_code_by_dep_sd para comparar los disponibles contra
        # los requeridos por dependencia / subdependencia.
        for (dep, sd), (requeried, available, program_code_ids) in program_code_by_dep_sd.items():
            if available < requeried:
                message_codigos = []
                for program_code_id in program_code_ids:
                    req = program_code_original_ids.get(program_code_id, 0.0)
                    dis = program_code_without_available.get(program_code_id, 0.0)
                    message_codigos.append("\n\t•{0}. Disponible: {1}, requerido: {2}.".format(
                        program_code_id.program_code,
                        format(req - dis, ",.2f"),
                        format(req, ",.2f")
                    ))
                msgs.append(self._get_insufficiency_message(
                    "la partida {0} para la Dependencia: {1} / Subdependencia: {2}".format(
                        item_ie,
                        dep.dependency,
                        sd.sub_dependency,
                    ),
                    available,
                    requeried,
                    'ie'
                ) + " para los siguientes códigos: {0}".format("".join(message_codigos)))
        return msgs

    def update_available_program_code_ie(self, lines, quarter, date=datetime.today(), move_id=None, cxp_id = None):
        item_ie = self.env[PAYMENT_PARAMETERS].get_param('item_ie').item
        budget = self.env[EXPENDITURE_BUDGET_LINE]
        budget_id = self.env['expenditure.budget'].search([
            ('state', '=', 'validate'),
            ('from_date', '<=', date),
            ('to_date', '>=', date),
        ])
        msgs = []
        # Validación del disponible de la cuenta de IE
        msgs += self.validate_available_account_ie(lines)
        # Diccionario con los valores actualizados del disponible
        program_code = Dictionary()
        # Diccionario con los códigos sin disponible y el monto faltante
        program_code_without_available = Dictionary()
        # Diccionarios con los códigos y montos para generar las adecuaciones
        program_code_src_adeq = Dictionary()
        # Diccionario con los acumulados por código programático
        program_code_original_ids = self.program_code_tuple_to_dict([(l[1], l[2]) for l in lines])
        # Se válida que el código programático tenga disponible
        for program_code_id, amount in program_code_original_ids.items():
            if not program_code.contains(program_code_id):
                available = budget.get_available_by_program_code(program_code_id.id, quarter)
                program_code.add_sum(program_code_id, available)
            # Se obtiene el disponible registrado en memoria
            available = program_code.get(program_code_id)
            # Sí el código no tiene disponible
            if available == 0:
                program_code_without_available.add_sum(program_code_id, amount)
            # Si el código tiene cuenta con el disponible requerido
            elif available >= amount:
                program_code.subtract_value(program_code_id, amount)
            # Si el código tiene disponible, pero no el requerido
            else:
                # Se consume el disponible del código
                program_code.subtract_value(program_code_id, available)
                # Se registra el código y el faltante
                program_code_without_available.add_sum(program_code_id, amount-available)
        # De los códigos que por si mismos no cuentan con disponible, se consulta
        # el disponible sus partidas 711 asociadas
        program_code_by_dep_sd = Dictionary(size=3)
        # Genera un nuevo diccionario, agrupando el monto faltante por dependencia/subdependencia
        # y el disponible total
        for program_code_id, amount in program_code_without_available.items():
            dep = program_code_id.dependency_id
            sd = program_code_id.sub_dependency_id
            program_code_by_dep_sd.add_sum((dep, sd), amount, 0)
            program_code_by_dep_sd.add_list((dep, sd), program_code_id, 2)
        # Se llena el segundo elemento elemento de la tupla del diccionario
        # que contienene el disponible total
        for (dep, sd), value in program_code_by_dep_sd.items():
            tmp_amount = value[0]
            # Se obtiene los códigos programáticos de ie asociados a la dependencia/subdependencia
            program_code_ids = self.env['program.code'].search([
                ('budget_id', '=', budget_id.id),
                ('dependency_id', '=', dep.id),
                ('sub_dependency_id', '=', sd.id),
                ('item_id.item', '=', item_ie),
                ('resource_origin_id.key_origin', '=', '01'),
            ])
            for program_code_id in program_code_ids:
                if not program_code.contains(program_code_id):
                    available = budget.get_available_by_program_code(program_code_id.id, quarter)
                    program_code.add_sum(program_code_id, available)
                available = program_code.get(program_code_id)
                program_code_by_dep_sd.add_sum((dep, sd), available, 1)
                if available >= tmp_amount:
                    program_code_src_adeq.add_sum(program_code_id, tmp_amount)
                    break
                elif available > 0:
                    program_code_src_adeq.add_sum(program_code_id, available)
                    tmp_amount -= available
            if not program_code_ids:
                program_code_by_dep_sd.add_sum((dep, sd), 0, 1)
        # Se recorre program_code_by_dep_sd para comparar los disponibles contra
        # los requeridos por dependencia / subdependencia.
        for (dep, sd), (requeried, available, program_code_ids) in program_code_by_dep_sd.items():
            if available < requeried:
                message_codigos = []
                for program_code_id in program_code_ids:
                    req = program_code_original_ids.get(program_code_id, 0.0)
                    dis = program_code_without_available.get(program_code_id, 0.0)
                    message_codigos.append("\n\t•{0}. Disponible: {1}, requerido: {2}.".format(
                        program_code_id.program_code,
                        format(req - dis, ",.2f"),
                        format(req, ",.2f")
                    ))
                msgs.append(self._get_insufficiency_message(
                    "la partida {0} para la Dependencia: {1} / Subdependencia: {2}".format(
                        item_ie,
                        dep.dependency,
                        sd.sub_dependency,
                    ),
                    available,
                    requeried,
                    'ie'
                ) + " para los siguientes códigos: {0}".format("".join(message_codigos)))
        # Si no se genero ningún mensaje de error, significa que hay suficiencia
        # o que se debe generar una adecuación para dar la suficiencia.
        if not msgs:
            lines_ad = []
            for program_code_id, amount in program_code_src_adeq.items():
                lines_ad.append((0, 0, {
                    'program': program_code_id.id,
                    'line_type': 'decrease',
                    'amount': amount,
                    'decrease_type': amount,
                    'creation_type': 'automatic'
                }))
            for program_code_id, amount in program_code_without_available.items():
                lines_ad.append((0, 0, {
                    'program': program_code_id.id,
                    'line_type': 'increase',
                    'amount': amount,
                    'increase_type': amount,
                    'creation_type': 'automatic'
                }))
            if lines_ad:
                journal_id = self.env.ref('jt_conac.comp_adequacy_jour')
                adequacie = {
                    'budget_id': budget_id.id,
                    'adaptation_type': 'compensated',
                    'state': 'confirmed',
                    'journal_id': journal_id and journal_id.id or False,
                    'date_of_budget_affected': date,
                    'adequacies_lines_ids': lines_ad,
                    'is_dgpo_authorization': True,
                    'observation': 'Autorización partida %s' % item_ie,
                    'origin_of_the_adequacy': 'automatic',
                }
                adequacie = self.env['adequacies'].create(adequacie)
                name = ""
                if cxp_id:
                    name = cxp_id.previous_number
                elif move_id:
                    name = move_id.folio
                adequacie.accept('Ingreso - gasto : [%s]' % name)
            # Una vez que se le dio suficiencia a los códigos programáticos,
            # Se sigue el mismo proceso que los de códigos de PA
            return self.update_available_program_code_pa([(l[1], l[2]) for l in lines], quarter, move_id)
        return False, msgs

    @api.model
    def approve_for_payment(self, move_ids):
        """
        Este metodo pone las solicitudes de pago en el status aprobadas para pago
        """
        def budget_allocation(move_id, date_approve):
            # Trimestre de afectación del presupuesto
            quarter = self._month_to_quarter(move_id.invoice_date.month)
            currency_id = move_id.currency_id
            currency_mx = self.env.user.company_id.currency_id
            lines_pa = []
            lines_ie = []
            lines_cc = []
            lines_cxp = []
            for line in move_id.invoice_line_ids:
                line_amount = abs(line.price_total)
                # Conversión a moneda nacional
                if currency_id != currency_mx:
                    line_amount = currency_id.compute(line_amount, currency_mx)
                amount = round(line_amount, 2)
                # Línea de Presupuesto Asignado
                if line.egress_key_id.key == 'PA':
                    lines_pa.append((line.program_code_id, amount))
                # Línea de Ingresos Extraordinarios
                elif line.egress_key_id.key in ('IEMN', 'IEME'):
                    lines_ie.append((line.account_ie, line.program_code_id, amount))
                # Línea de Ingresos Extraordinarios
                elif line.egress_key_id.key == 'CC':
                    lines_cc.append((line.account_id, line.dependency_id, line.sub_dependency_id, amount))
                # Línea de Cuenta por pagar
                elif line.egress_key_id.key in ('CPN', 'CPE'):
                    lines_cxp.append((
                        move_id.previous_number,
                        line.program_code_id,
                        line.account_id,
                        line.account_ie,
                        line.dependency_id,
                        line.sub_dependency_id,
                        amount
                    ))
            # Validación de la disponibilidad por cuenta contable
            res = self.validate_available_account(lines_cc)
            if res:
                return False, res
            # Disminución del disponible de códigos programáticos de PA
            ok, res = self.update_available_program_code_pa(lines_pa, quarter, move_id)
            if not ok:
                self.env.cr.rollback()
                return False, res
            else:
                if res:
                    move_id.message_post(body=res)
            # Disminución del disponible de códigos programáticos de PA
            ok, res = self.update_available_program_code_ie(lines_ie, quarter, date_approve, move_id=move_id)
            if not ok:
                self.env.cr.rollback()
                return False, res
            # Disminucón del disponible de las cuentas por pagar
            ok, res = self.update_available_account_to_pay(lines_cxp, move_id)
            if not ok:
                self.env.cr.rollback()
                return False, res
            # Si llega a este punto es que todas las líneas contaban con disponible
            # Se crean lo asientos contables
            move_id.create_journal_line_for_approved_payment()
            # Se hace capitalizaciónaction_post
            move_id.capitalizacion()
            # Se publican los asientos contables
            move_id.action_post()
            return True, None

        # Método para aprovar para pago o rechazar una solicitud de pago
        res = []
        for move_id in self.env['account.move'].browse(move_ids):
            date_approve = move_id.invoice_date
            # Flujo alterno para Reembolsos previo a la aprobación
            if move_id.is_refund:
                if move_id.payment_state == 'registered':
                    if move_id.amount_total >= 5000:
                        move_id.payment_state = 'to_be_approved_dgf'
                        continue
            ok, msgs = budget_allocation(move_id, date_approve)
            if ok:
                # Flujo alterno para Reembolsos
                if move_id.is_refund:
                    if move_id.payment_state == 'to_be_approved_dgf':
                        move_id.payment_state = 'approved_payment_dgf'
                move_id.payment_state = 'approved_payment'
                move_id.date_approval_request = date_approve
                move_id.is_from_reschedule_payment = False
                # Se establece el folio por lotes en 0
                move_id.batch_folio = 0
                # Flujo alterno para Ministraciones
                if move_id.is_ministration:
                    # Se modifica la ministración para reducir el monto comprobado
                    self.env[MONEY_FUNDS_REQUEST_MINISTRATION].get_acc_moves(move_id.id, move_id.amount_total)
                    # La solicitud pasa al status pagada
                    move_id.payment_state = 'paid'
                    # Se crean los asientos contables de pagado del presupuesto
                    move_id.paid_payment_request()
                res.append((True, ""))
            else:
                move_id.payment_state = 'rejected'
                move_id.reason_rejection = "\r\n".join(msgs)
                res.append((False, move_id.reason_rejection))
        return res

    def paid_payment_request(self):
        lines = []
        for program_code_id, balance, amount_currency in self.get_balance_by_program_code():
            lines.append((0, 0, {
                'account_id': self.journal_id.paid_credit_account_id.id,
                'coa_conac_id': self.journal_id.conac_paid_credit_account_id.id,
                'credit': balance,
                'amount_currency': -amount_currency,
                'exclude_from_invoice_tab': True,
                'conac_move': True,
                'partner_id': self.partner_id.id,
                'program_code_id': program_code_id.id,
                'dependency_id': program_code_id.dependency_id.id if self.journal_id.paid_credit_account_id.dep_subdep_flag else False,
                'sub_dependency_id': program_code_id.sub_dependency_id.id if self.journal_id.paid_credit_account_id.dep_subdep_flag else False,
            }))
            lines.append((0, 0, {
                'account_id': self.journal_id.paid_debit_account_id.id,
                'coa_conac_id': self.journal_id.conac_paid_debit_account_id.id,
                'debit': balance,
                'amount_currency': amount_currency,
                'exclude_from_invoice_tab': True,
                'conac_move': True,
                'partner_id': self.partner_id.id,
                'program_code_id': program_code_id.id,
                'dependency_id': program_code_id.dependency_id.id if self.journal_id.paid_debit_account_id.dep_subdep_flag else False,
                'sub_dependency_id': program_code_id.sub_dependency_id.id if self.journal_id.paid_debit_account_id.dep_subdep_flag else False,
            }))
        
        #Agregar lineas en la primera ventana de las vistas

        self.line_ids=lines
        
        
        # payment = {
        #     'ref': self.name,
        #     'date': self.date_approval_request,
        #     'conac_move': True,
        #     'dependancy_id' : self.dependancy_id and self.dependancy_id.id or False,
        #     'sub_dependancy_id': self.sub_dependancy_id and self.sub_dependancy_id.id or False,
        #     'journal_id': self.journal_id.id,
        #     'type': 'entry',
        #     'company_id': self.env.user.company_id.id,
        #     'line_ids': lines
        # }
        # # Creación de los apuntes contables de Pago
        # move_obj = self.env['account.move']
        # payment = move_obj.create(payment).with_context(check_move_validity=False)
        # payment.action_post()
        # self.payment_move_line_ids = [(4, id) for id in payment.line_ids.ids]
        # self.show_payment_line_ids()
        return True

    def reject_payment_request(self, reason_for_rejection, state='rejected'):
        self.write({
            'payment_state': state,
            'reason_rejection': reason_for_rejection
        })
        if self.is_ministration or self.is_refund:
            # TODO implementar método para noticar a usuarios del fondo
            pass


    def action_reject(self, state='rejected'):
        """
        Acción de Rechazar una Solicitud de Pago
        """
        return {
            'name': ('Rechazar Solicitud de Pago'),
            'res_model': 'reject.payment.request',
            'view_mode': 'form',
            'view_id': self.env.ref('siif_supplier_payment.reject_payment_request_form_wizard').id,
            'context': {
                'default_move_id': self.id,
                'default_state': state,
            },
            'target': 'new',
            'type': 'ir.actions.act_window',
        }

    def action_approve_for_ministration(self):
        """
        Acción de Aprobar solicitud de pago para ministración
        """
        return self.action_validate_budget()

    def action_reject_for_ministration(self):
        """
        Acción de Rechazar una solicitud de pago para ministración
        """
        return self.action_reject()

    def action_approve_for_refund(self):
        """
        Acción de Aprobar solicitud de pago para ministración
        """
        return self.approve_for_payment([self.id])

    def action_reject_for_refund(self):
        """
        Acción de Rechazar solicitud de pago para ministración
        """
        return self.action_reject('rejected_dgf')

    @api.onchange('operation_type_id', 'previous_number')
    def _onchange_operation_type(self):
        self._recompute_payment_terms_lines()

    # Se sobreescribe el método de odoo para cambiar la cuenta contable de la cuenta
    # a pagar de la solicitud por la de la ministración en vez de la del proveedor
    # cuando se trata de una solicitud de comprobación de la ministración.
    def _recompute_payment_terms_lines(self):
        ''' Compute the dynamic payment term lines of the journal entry.'''
        self.ensure_one()
        in_draft_mode = self != self._origin
        today = fields.Date.context_today(self)
        self = self.with_context(force_company=self.journal_id.company_id.id)

        def _get_payment_terms_computation_date(self):
            ''' Get the date from invoice that will be used to compute the payment terms.
            :param self:    The current account.move record.
            :return:        A datetime.date object.
            '''
            if self.invoice_payment_term_id:
                return self.invoice_date or today
            else:
                return self.invoice_date_due or self.invoice_date or today

        def _get_payment_terms_account(self, payment_terms_lines):
            ''' Get the account from invoice that will be set as receivable / payable account.
            :param self:                    The current account.move record.
            :param payment_terms_lines:     The current payment terms lines.
            :return:                        An account.account record.
            '''
            if self.is_payroll_payment_reissue:
                return self.env['account.catalog.type']._get_nominal_param('check_cancel')
            elif self.is_ministration and self.previous_number:
                ministration_id =  self.env[MONEY_FUNDS_REQUEST_MINISTRATION].search([
                    ('account_topay_id', '=', self.previous_number.id),
                    ('state', '=', 'deposited'),
                ])
                if ministration_id and ministration_id.min_account_id:
                    return ministration_id.min_account_id
            if self.partner_id:
                # Retrieve account from partner.
                if self.is_sale_document(include_receipts=True):
                    return self.partner_id.property_account_receivable_id
                else:
                    return self.partner_id.property_account_payable_id
            elif payment_terms_lines:
                # Retrieve account from previous payment terms lines in order to allow the user to set a custom one.
                return payment_terms_lines[0].account_id
            else:
                # Search new account.
                domain = [
                    ('company_id', '=', self.company_id.id),
                    ('internal_type', '=', 'receivable' if self.type in ('out_invoice', 'out_refund', 'out_receipt') else 'payable'),
                ]
                return self.env['account.account'].search(domain, limit=1)

        def _compute_payment_terms(self, date, total_balance, total_amount_currency):
            ''' Compute the payment terms.
            :param self:                    The current account.move record.
            :param date:                    The date computed by '_get_payment_terms_computation_date'.
            :param total_balance:           The invoice's total in company's currency.
            :param total_amount_currency:   The invoice's total in invoice's currency.
            :return:                        A list <to_pay_company_currency, to_pay_invoice_currency, due_date>.
            '''
            if self.invoice_payment_term_id:
                to_compute = self.invoice_payment_term_id.compute(total_balance, date_ref=date, currency=self.company_id.currency_id)
                if self.currency_id != self.company_id.currency_id:
                    # Multi-currencies.
                    to_compute_currency = self.invoice_payment_term_id.compute(total_amount_currency, date_ref=date, currency=self.currency_id)
                    return [(b[0], b[1], ac[1]) for b, ac in zip(to_compute, to_compute_currency)]
                else:
                    # Single-currency.
                    return [(b[0], b[1], 0.0) for b in to_compute]
            else:
                return [(fields.Date.to_string(date), total_balance, total_amount_currency)]

        def _compute_diff_payment_terms_lines(self, existing_terms_lines, account, to_compute):
            ''' Process the result of the '_compute_payment_terms' method and creates/updates corresponding invoice lines.
            :param self:                    The current account.move record.
            :param existing_terms_lines:    The current payment terms lines.
            :param account:                 The account.account record returned by '_get_payment_terms_account'.
            :param to_compute:              The list returned by '_compute_payment_terms'.
            '''
            # As we try to update existing lines, sort them by due date.
            existing_terms_lines = existing_terms_lines.sorted(lambda line: line.date_maturity or today)
            existing_terms_lines_index = 0

            # Recompute amls: update existing line or create new one for each payment term.
            new_terms_lines = self.env[ACCOUNT_MOVE_LINE]
            for date_maturity, balance, amount_currency in to_compute:
                if self.journal_id.company_id.currency_id.is_zero(balance) and len(to_compute) > 1:
                    continue

                if existing_terms_lines_index < len(existing_terms_lines):
                    # Update existing line.
                    candidate = existing_terms_lines[existing_terms_lines_index]
                    existing_terms_lines_index += 1
                    candidate.update({
                        'account_id': account.id,
                        'date_maturity': date_maturity,
                        'amount_currency': -amount_currency,
                        'debit': balance < 0.0 and -balance or 0.0,
                        'credit': balance > 0.0 and balance or 0.0,
                    })
                else:
                    # Create new line.
                    create_method = in_draft_mode and self.env[ACCOUNT_MOVE_LINE].new or self.env[ACCOUNT_MOVE_LINE].create
                    candidate = create_method({
                        'name': self.invoice_payment_ref or '',
                        'debit': balance < 0.0 and -balance or 0.0,
                        'credit': balance > 0.0 and balance or 0.0,
                        'quantity': 1.0,
                        'amount_currency': -amount_currency,
                        'date_maturity': date_maturity,
                        'move_id': self.id,
                        'currency_id': self.currency_id.id if self.currency_id != self.company_id.currency_id else False,
                        'account_id': account.id,
                        'partner_id': self.commercial_partner_id.id,
                        'exclude_from_invoice_tab': True,
                        'dependency_id': self.dependancy_id,
                        'sub_dependency_id': self.sub_dependancy_id,
                    })
                new_terms_lines += candidate
                if in_draft_mode:
                    candidate._onchange_amount_currency()
                    candidate._onchange_balance()
            return new_terms_lines

        existing_terms_lines = self.line_ids.filtered(lambda line: line.account_id.user_type_id.type in ('receivable', 'payable'))
        others_lines = self.line_ids.filtered(lambda line: line.account_id.user_type_id.type not in ('receivable', 'payable'))
        company_currency_id = (self.company_id or self.env.company).currency_id
        total_balance = sum(others_lines.mapped(lambda l: company_currency_id.round(l.balance)))
        total_amount_currency = sum(others_lines.mapped('amount_currency'))

        if not others_lines:
            self.line_ids -= existing_terms_lines
            return

        computation_date = _get_payment_terms_computation_date(self)
        account = _get_payment_terms_account(self, existing_terms_lines)
        to_compute = _compute_payment_terms(self, computation_date, total_balance, total_amount_currency)
        new_terms_lines = _compute_diff_payment_terms_lines(self, existing_terms_lines, account, to_compute)

        # Remove old terms lines that are no longer needed.
        self.line_ids -= existing_terms_lines - new_terms_lines

        if new_terms_lines:
            self.invoice_payment_ref = new_terms_lines[-1].name or ''
            self.invoice_date_due = new_terms_lines[-1].date_maturity

    def get_payment_type(self):
        if self.is_payment_request:
            return 'supplier'
        elif self.is_payroll_payment_request:
            return 'payroll'
        elif self.is_different_payroll_request:
            return 'different_payroll'
        elif self.is_project_payment:
            return 'project_payment'
        elif self.is_pension_payment_request:
            return 'pension_payment'

    def _generate_next_batch_sheet_folio(self, payment_type):
        """
        Devulve el siguiente valor de la secuencia del folio por lotes
        de acuerdo al tipo solicitud de pago.
        """
        seq_code = ''
        if payment_type == 'supplier':
            seq_code = 'batch.sheet.folio'
        elif payment_type == 'payroll':
            seq_code = 'payroll.payment.batch.sheet.folio'
        elif payment_type == 'different_payroll':
            seq_code = 'different.payroll.payment.batch.sheet.folio'
        elif payment_type == 'project_payment':
            seq_code = 'project.payroll.payment.batch.sheet.folio'
        elif payment_type == 'pension_payment':
            seq_code = 'pension.payroll.payment.batch.sheet.folio'
        return self.env['ir.sequence'].next_by_code(seq_code)

    def _update_batch_folio(self, folio, move_ids):
        """
        Actualiza el folio por lotes de las solicitudes de pago
        """
        if not move_ids:
            raise UserError("No se encontraron solicitudes de pago para asignarle su folio por lotes.")
        query_folio = """
            update account_move set batch_folio = %s where id in %s
        """
        self.env.cr.execute(query_folio, (folio, tuple(move_ids)))
        for move_id in self.browse(move_ids):
            move_id.message_post(body=('Asignación de folio por lotes: %s' % folio))

        # * Flujo para el pago de cheques
        # Se crean las solicitudes de cheques para seguir el flujo en el módulo
        # de control de cheques. Solo aplica en caso de que el método de pago
        # sea cheques, de lo contrario se ignora.
        self.env['account.payment'].create_payment_check_request(move_ids)

    def generate_payment_batch(self, payment_type, move_ids):
        """
        Genera el folio y se lo asigna a las solicitudes de pago 'supplier'
        """
        folio = self._generate_next_batch_sheet_folio(payment_type)
        self._update_batch_folio(folio, move_ids)
        return folio

    def _count_account_return(self):
        for rec in self:
            rec.num_account_returns = len(self.env['account.returns'].search([('account_move_id','=',rec.id),('state','in',('approved','done'))]))

    @api.depends('num_account_returns')
    def _check_account_returns_count(self):
        for rec in self:
            if rec.num_account_returns==0:
                rec.bool_account_returns=False
            else:
                rec.bool_account_returns=True
    #Función para mostrar las solicitudes de devolucion
    def check_account_return_info(self):
        return {
            'name': 'Devoluciones',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.returns',
            'domain': [('account_move_id', '=', self.id),('state','=',('done','approved'))],
            'type': 'ir.actions.act_window',
        }