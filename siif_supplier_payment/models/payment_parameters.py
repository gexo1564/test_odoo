# -*- coding: utf-8 -*-

import logging
from odoo import models, fields, api, _
from datetime import datetime
from odoo.exceptions import UserError, ValidationError

"""Modelos"""
ACC_JOURNAL='account.journal'
OP_TYPE='operation.type'
class PayrollPaymentParameters(models.Model):

    _name = 'payment.parameters'

    name = fields.Char(default='Parámetros', store=False)
    # Partida de Ingresos Extraordinarios.
    item_ie_id = fields.Many2one('expenditure.item', 'Item of extraordinary income')
    # Tipo de Operación para Viaticos.
    type_of_operation_for_travel_expenses_id = fields.Many2one(OP_TYPE, string='Type of Operation for Travel Expenses')
    # Tipo de Operación para Ministración.
    type_of_operation_for_ministration_id = fields.Many2one(OP_TYPE, string='Type of Operation for Ministration')
    # Tipo de Operación para Reembolsos.
    type_of_operation_for_refunds_id = fields.Many2one(OP_TYPE, string='Type of Operation for Refuns')

    uma_capitalization = fields.Float('UMA Capitalization')

    last_exercise_account = fields.Many2one('account.account',string="Last exercise return")

    journal_return = fields.Many2one(ACC_JOURNAL,string="Account Journal Returns")

    flag_feu = fields.Boolean('Sin Firma Electrónica',default=False)

    journal_mn = fields.Many2one(ACC_JOURNAL,string="National Currency Journal")
    journal_me = fields.Many2one(ACC_JOURNAL,string="Foreign Currency Journal")
    @api.model
    def create(self, vals):
        count = self.env['payment.parameters'].search_count([])
        if count >= 1:
            raise ValidationError("Solo se puede configurar un registro de parámetros")
        return super().create(vals)

    def name_get(self):
        return [(record.id, 'Parámetros de Solicitudes de Pago') for record in self]


    def get_param(self, param):
        params = self.search([], limit=1)
        if not params:
            raise ValidationError("Configure los parámetros en el menú 'Parámetros de Solicitudes de Pago'")
    
        """
            Param_validations:
                Diccionario con la información
                    (nombre de la variable, mensaje de error si no se encuentra el valor)
        """
        param_validations = {
            'item_ie': ('item_ie_id', "Partida de ingresos extraordinarios"),
            'uma_capitalization': ('uma_capitalization', "Monto de la uma"),
            'travel_expenses_op_type': ('type_of_operation_for_travel_expenses_id', "Tipo de operación para viáticos"),
            'ministration_op_type': ('type_of_operation_for_ministration_id', "Tipo de operación para ministraciones"),
            'refunds_op_type': ('type_of_operation_for_refunds_id', "Tipo de operación para reembolsos"),
            'last_exercise': ('last_exercise_account', "Cuenta contable para las operaciones de ejercicios anteriores"),
            'journal_return': ('journal_return', "Diario para Devoluciones"),
            'flag_feu': ('flag_feu', None),
            'national_journal': ('journal_mn', "Diario de Solicitudes de pago en Moneda Nacional"),
            'foreign_journal': ('journal_me', "Diario de Solicitudes de pago en Moneda Extranjera"),
        }
        #Se revisa si el parametro se encuentra en el diccionario
        if param in param_validations:
            #Obtiene el nombre de la variable y el mensaje de error
            attr_name, error_msg = param_validations[param]
            #obtiene el valor del parametro
            value = getattr(params, attr_name)
            #Si no encuentra el valor del parametro, mandará el mensaje de error
            if error_msg and not value:
                raise ValidationError(f"Configure '{error_msg}' en el menú de 'Parámetros de Solicitudes de Pago'.")
            return value
        #Si el parametro no existe
        else:
            raise ValueError("Parámetro no encontrado")