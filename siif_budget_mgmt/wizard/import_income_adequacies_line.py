# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields, api, _
import base64
from odoo.modules.module import get_resource_path
from xlrd import open_workbook
from odoo.exceptions import UserError, ValidationError
import logging
from datetime import datetime
import pytz

CLASSIFIER_INCOME_ITEM_MODEL = 'classifier.income.item'

class ImportIncomeAdequaciesLine(models.TransientModel):

    _name = 'import.income.adequacies.line'
    _description = 'Import Income Adequacies Line'

    record_number = fields.Integer(string='Número de Registros', required=True)
    file = fields.Binary(string='Archivo')
    filename = fields.Char(string='File name')
    download_file = fields.Binary(string='Descargar Archivo')
    download_filename = fields.Char(string='Download File name')
    father_adequacy = fields.Many2one('income.adequacies', ondelete="cascade")

    # Descarga el archivo de plantilla
    def download(self):
        file_path = get_resource_path(
            'siif_budget_mgmt', 'static/file/import_line', 'SIIF_Plantilla_Adecuacion-Recalendarizacion.xls')
        file = False
        with open(file_path, 'rb') as file_date:
            file = base64.b64encode(file_date.read())
        self.download_filename = 'SIIF_Plantilla_Adecuacion-Recalendarizacion.xls'
        self.download_file = file

        return {
            'name': 'Download Sample File',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': False,
            'res_model': 'import.income.adequacies.line',
            'domain': [],
            'type': 'ir.actions.act_window',
            'target': 'new',
            'res_id': self.id,
        }

    @api.onchange('file') # Linea Nueva
    def onchange_file(self): # Linea Nueva
        self.father_adequacy.budget_file = self.file
        self.father_adequacy.filename = self.filename

    def import_line(self):
        self._verify_is_numeric_and_positive(self.record_number)
        
        valid_month = ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 
                       'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre']
        # Get adequacies info
        fiscal_year = self.father_adequacy.fiscal_year
        budget_id = self.father_adequacy.budget_id
        movement_type = self.father_adequacy.type
        adequacy_type = self.father_adequacy.adequacie_type
        adequacy_date = self.father_adequacy.date_adequacie
        # Initialize lists
        correct_lines, error_lines, error_messages = [], [], []
        # Validate file
        if not self.file:
            raise UserError(_("Please Upload File"))
        try:
            data = base64.decodestring(self.file)
            book = open_workbook(file_contents=data or b'')
        except UserError as e:
            raise UserError(e)
        sheet = book.sheet_by_index(0)
        total_rows = self.record_number + 1
        if sheet.nrows != total_rows:
            raise UserError(_("Number of records do not match with file"))
        # Validate lines
        for row in range(1, sheet.nrows):
            cri = sheet.cell_value(row, 0)
            line_type = sheet.cell_value(row, 1)
            month_from = sheet.cell_value(row, 2)
            month_to = sheet.cell_value(row, 3)
            increase_line = sheet.cell_value(row, 4)
            decrease_line = sheet.cell_value(row, 5)
            line = {}
            is_cri_invalid = self._cri_validation(cri, sheet, row, error_lines, error_messages)
            if is_cri_invalid:
                continue
            is_line_type_month_invalid = self._line_type_and_month_validation(line_type, month_to, month_from, valid_month, sheet, row, error_lines, error_messages)
            if is_line_type_month_invalid:
                continue
            is_amount_invalid = self._amount_validation(increase_line, decrease_line, line_type, month_from, month_to, sheet, row, error_lines, error_messages)
            if is_amount_invalid:
                continue
            line.update({
                'cri_id': self._get_cri_id(cri),
                'line_type' : line_type,
                'month_from': month_from,
                'month_to' : month_to,
                'increase_line' : increase_line,
                'decrease_line' : decrease_line,
                'creation_type' : 'imported',
                'imported' : 'True',
                'fiscal_year' : fiscal_year.id,
                'budget_id' : budget_id.id,
                'movement_type' : movement_type,
                'adequacy_type' : adequacy_type,
                'adequacy_date' : adequacy_date,
            })
            # If all validations are correct, add line to correct list
            correct_lines.append((0, 0, line))

        if error_messages:
            tz = pytz.timezone('America/Mexico_City')
            today = str(datetime.now(tz).strftime("%Y-%m-%d %H:%M:%S"))
            content = "."*25 + (_("Error logs ")) + today + "."*25 + "\r\n"
            content += "\r\n".join(error_messages)  # Updated line
            self.father_adequacy.failed_row_file = base64.b64encode(content.encode('utf-8'))
        else:
            self.father_adequacy.failed_row_file = None
        
        self.father_adequacy.adequacies_lines_ids = None
        self.father_adequacy.adequacies_lines_ids = correct_lines 
        self.father_adequacy.total_rows = self.record_number
        self.father_adequacy.success_rows = len(self.father_adequacy.adequacies_lines_ids) 
        self.father_adequacy.import_record_number = len(self.father_adequacy.adequacies_lines_ids)    
        self.father_adequacy.failed_rows = (self.record_number - len(self.father_adequacy.adequacies_lines_ids))

    def _set_line_error(self, sheet, row, message, error_lines, error_messages):
        # Add line to error list
        error_lines.append((0,0,{
            'cri_id': sheet.cell_value(row, 0),
            'line_type': sheet.cell_value(row, 1),
            'month_from': sheet.cell_value(row, 2),
            'month_to': sheet.cell_value(row, 3),
            'increase_line': sheet.cell_value(row, 4),
            'decrease_line': sheet.cell_value(row, 5),
        }))
        # Error message
        line = ("[" + "'%s', "*5 + "%s]") % tuple([sheet.cell_value(row, i) for i in range(6)])
        error_messages.append("%s ------->> %s" % (line, message))

    def _verify_is_numeric_and_positive(self, value):
        if not isinstance(value, int):
            raise ValidationError(_("Value must be numeric"))
        if not value or value <= 0:
            raise ValidationError(_("Value must be positive"))
    
    def _get_cri_id(self, cri):
        cri_id = self.env[CLASSIFIER_INCOME_ITEM_MODEL].search([('name', '=', cri)])
        if cri_id: 
            return cri_id.id

    def _cri_validation(self, cri, sheet, row, error_lines, error_messages):
        if not str(cri).isnumeric():
            message = _("CRI Code %s in cell (A %s) is not numeric.") % (cri, row+1)
            self._set_line_error(sheet, row, message, error_lines, error_messages)
            return True
        if len(cri) != 6:
            message = _("CRI Code %s in cell (A %s) does not have 6 digits.") % (cri, row+1)
            self._set_line_error(sheet, row, message, error_lines, error_messages)
            return True
        cri_id = self._get_cri_id(cri)
        if not cri_id: 
            message = _("CRI Code %s in cell (A %s) does not exist.") % (cri, row+1)
            self._set_line_error(sheet, row, message, error_lines, error_messages)
            return True
        
    def _line_type_and_month_validation(self, line_type, month_to, month_from, valid_month, sheet, row, error_lines, error_messages):
        if line_type not in ['aumento', 'disminucion']:
            message = _("Line type %s in cell (B %s) is not valid.") % (line_type, row+1)
            self._set_line_error(sheet, row, message, error_lines, error_messages)
            return True
        if line_type == 'aumento' and not month_to:
            message = _("Month to is required in cell (D %s) for line type increase.") % (row+1)
            self._set_line_error(sheet, row, message, error_lines, error_messages)
            return True
        if line_type == 'disminucion' and not month_from:
            message = _("Month from is required in cell (C %s) for line type decrease.") % (row+1)
            self._set_line_error(sheet, row, message, error_lines, error_messages)
            return True
        if month_from and month_to:
            message = _("Only month from or month to must be placed in cell (C %s) or (D %s).") % (row+1, row+1)
            self._set_line_error(sheet, row, message, error_lines, error_messages)
            return True
        if line_type == 'aumento' and month_to and month_to not in valid_month:
            message = _("Month to %s in cell (D %s) is not valid.") % (month_to, row+1)
            self._set_line_error(sheet, row, message, error_lines, error_messages)
            return True
        if line_type == 'disminucion' and month_from and month_from not in valid_month:
            message = _("Month from %s in cell (C %s) is not valid.") % (month_from, row+1)
            self._set_line_error(sheet, row, message, error_lines, error_messages)
            return True
        if not month_from and not month_to:
            message = _("Month from or month to is required in cell (C %s) or (D %s).") % (row+1, row+1)
            self._set_line_error(sheet, row, message, error_lines, error_messages)
            return True
        
    def _amount_validation(self, increase_line, decrease_line, line_type, month_from, month_to, sheet, row, error_lines, error_messages):
        if not increase_line and not decrease_line:
            message = _("Increase or decrease is required in cell (E %s) or (F %s).") % (row+1, row+1)
            self._set_line_error(sheet, row, message, error_lines, error_messages)
            return True
        if increase_line and decrease_line:
            message = _("Only increase or decrease must be placed in cell (E %s) or (F %s).") % (row+1, row+1)
            self._set_line_error(sheet, row, message, error_lines, error_messages)
            return True
        
        if line_type == 'aumento':
            return self._increase_amount_valdation(increase_line, decrease_line, month_to, sheet, row, error_lines, error_messages)
        elif line_type == 'disminucion':
            return self._decrease_amount_validation(increase_line, decrease_line, month_from, sheet, row, error_lines, error_messages)

    def _increase_amount_valdation(self, increase_line, decrease_line, month_to, sheet, row, error_lines, error_messages):
        if not isinstance(increase_line, (int, float)):
            message = _("Increase %s in cell (E %s) is not valid.") % (increase_line, row+1)
            self._set_line_error(sheet, row, message, error_lines, error_messages)
            return True
        if not increase_line or increase_line <= 0:
            message = _("Increase %s in cell (E %s) must be positive.") % (increase_line, row+1)
            self._set_line_error(sheet, row, message, error_lines, error_messages)
            return True
        if decrease_line > 0:
            message = _("Decrease %s in cell (F %s) must be 0.") % (decrease_line, row+1)
            self._set_line_error(sheet, row, message, error_lines, error_messages)
            return True
        if not month_to:
            message = _("Month to is required in cell (D %s).") % (row+1)
            self._set_line_error(sheet, row, message, error_lines, error_messages)
            return True
        
    def _decrease_amount_validation(self, increase_line, decrease_line, month_from, sheet, row, error_lines, error_messages):
        if not isinstance(decrease_line, (int, float)):
            message = _("Decrease %s in cell (F %s) is not valid.") % (decrease_line, row+1)
            self._set_line_error(sheet, row, message, error_lines, error_messages)
            return True
        if not decrease_line or decrease_line <= 0:
            message = _("Decrease %s in cell (F %s) must be more than 0.") % (decrease_line, row+1)
            self._set_line_error(sheet, row, message, error_lines, error_messages)
            return True
        if increase_line > 0:
            message = _("Increase %s in cell (E %s) must be more than 0.") % (increase_line, row+1)
            self._set_line_error(sheet, row, message, error_lines, error_messages)
            return True
        if not month_from:
            message = _("Month from is required in cell (C %s).") % (row+1)
            self._set_line_error(sheet, row, message, error_lines, error_messages)
            return True
