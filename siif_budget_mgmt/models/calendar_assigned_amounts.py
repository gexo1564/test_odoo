# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
from datetime import datetime
import time
import json
import logging

class CalendarAssignedAmounts(models.Model):
    _inherit = 'calendar.assigned.amounts'

    # Lineas Asientos Contables
    budget_lines = fields.One2many('siif.budget.mgmt.income.budgets.lines', 'my_id_subsidy', 'Líneas Presupuesto Ingresos')
    sequence_tree = fields.Integer(string = 'Sequence', store = False)