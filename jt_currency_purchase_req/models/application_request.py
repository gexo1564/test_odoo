# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields, api, _
from datetime import date,datetime
from odoo.exceptions import ValidationError, UserError
from lxml import etree

"""Modelo"""
CURRENCY='currency.funds'
ACCOUNT_MOVE='account.move'
ACC_MOVE_LINE='account.move.line'
QUOTE='quote'
"""Grupos"""
GROUP_ON_FINANCE_ADMIN='jt_payroll_payment.group_on_finance_admin'
GROUP_FINANCE_USER='jt_payroll_payment.group_admin_finance_user'
GROUP_CURRENCY_FUNDS='jt_currency_purchase_req.group_currency_funds'
GROUP_QUOTES='jt_currency_purchase_req.group_quotes'
GROUP_REQUEST_APPROVAL='jt_currency_purchase_req.group_request_approval'

class ApplicationRequest(models.Model):

	_name = 'application.request'
	_description = 'Application Request for Currency Fund'
	_inherit = ['mail.thread', 'mail.activity.mixin']

	name = fields.Char('Name')
	status = fields.Selection(
		[
			('draft','Draft'),
			('published','Published'),
			('quoted','Quoted'),
			('rejected','Rejected'),
			('approved','Approved'),
			('with_resources','With Resources'),
			('adquired','Adquired'),
			('recived','Recived'),
		],string='Status',default='draft',tracking=True)
	currency_fund_id =  fields.Many2one('currency.funds',string='Currency Fund',required=True)
	currency_id = fields.Many2one('res.currency','Currency',compute='_get_currency_fund',tracking=True)
	required_date = fields.Date('Date Receipt',default=date.today(),tracking=True)
	amount_funds = fields.Monetary('Amount Funds',currency_field='currency_id',default=0)
	account_journal = fields.Many2one('account.journal',string='Account Journal',domain="[('type', '=', 'bank')]",tracking=True)
	observations = fields.Char('Observations',tracking=True)
	batch = fields.Integer('Batch')

	total_denomination = fields.One2many('total.denominations','application_request',string='Total Denominations',tracking=True)
	amount_denomination = fields.Monetary('Amount Denomination',currency_field='currency_id',compute='_compute_amount_funds',default=0)


	quotes =  fields.One2many(QUOTE,'application_request',string="Quotes")
	published_quotes = fields.One2many(QUOTE,'application_request',store=False,string="Quotes",compute='_get_quote',tracking=True)

	
	#Solicitud de Transferencia Vinculada
	transfer_request = fields.Many2one('request.open.balance.finance',string='Transfer_request',tracking=True)
	transfer_acc_move_lines = fields.One2many(ACC_MOVE_LINE, string="Lineas de Transferencia",compute="_get_transfer_moves",tracking=True)

	#Apuntes contables

	buy_acc_move_lines 	   = fields.One2many(ACC_MOVE_LINE, 'app_request_buy_id', string="Foreign Currency Payment",tracking=True)
	recived_acc_move_lines = fields.One2many(ACC_MOVE_LINE, 'app_request_recived_id', string="Currency Reception",tracking=True)

	#Campos de Rechazo
	reason_rejection = fields.Many2one('application.request.rejection.reason', string="Reason for Rejection",tracking=True)
	reason_rejection_desc = fields.Text(string="Description of Reason for Rejection",tracking=True)

	is_request_for_payment_request = fields.Boolean('Is Request For payment_request?',default=False)
	has_quotes = fields.Boolean('Quotes?',default=False)

	doc_close = fields.Binary(string='Exercise Close Doc',tracking=True)
	doc_close_name = fields.Char(string='Exercise Close Name')

	doc_transfer = fields.Binary(string='Transfer Doc',tracking=True)
	doc_transfer_name = fields.Char(string='Transfer Doc Name')

	#Comprobante de pago
	proof_payment_doc = fields.Binary(string='Proof of payment Doc',tracking=True)
	proof_payment_doc_name = fields.Char(string='Proof of payment Doc Name')
		
	#CFDI
	doc_CFDI_pdf = fields.Binary(string='CFDI',tracking=True)
	doc_CFDI_pdf_name = fields.Char(string='CDFI Name')
	doc_CFDI_xml = fields.Binary(string='CFDI xml',tracking=True)
	doc_CFDI_xml_name = fields.Char(string='CDFI xml Name')
	
	sequence_quote_id = fields.Many2one('ir.sequence', string='Entry Sequence', required=True, copy=False)


	#---------------------------------------------------------
	#					REGISTRO
	#---------------------------------------------------------

	#Creación de las Solicitudes de compra
	@api.model
	def create(self, vals):
		if vals.get('name')==None:
			fund = self.env[CURRENCY].search([('id','=',vals.get('currency_fund_id'))])
			vals.update({'name': fund.sequence_app_request_id.next_by_id()})
		vals.update({'sequence_quote_id':self.sudo()._create_sequence(vals).id})
		res = super(ApplicationRequest, self).create(vals)		
		return res

	@api.model
	def _create_sequence(self, vals, refund=False):
		seq = {
			'name': _('%s Sequence') % 'app_request_quote',
			'implementation': 'no_gap',
			'prefix': 'Cotización No. ',
			'padding': 3,
			'number_increment': 1,
		}
		seq = self.env['ir.sequence'].create(seq)
		seq_date_range = seq._get_current_sequence()
		seq_date_range.number_next = vals.get('sequence_number_next', 1)
		return seq

	#---------------------------------------------------------
	#					ACCIONES DE VISTA
	#---------------------------------------------------------

	#Acción para pasar a publicar la solicitud
	def action_approve_published(self):
		if len(self.total_denomination)!=0:
			if self.amount_denomination==self.amount_funds:
				self.status = 'published'
			else:
				raise ValidationError(_('The sum of the denominations is different from the amount entered, check'))
		else:
			raise  ValidationError(_('The request dont have denominations, check'))

	#Acción para la vista emergente para la aprobación o rechazo de la solicitud
	def action_approved_rejected(self):
		if self.doc_close!=False:
			return {
				'name': _('Aprobación Solicitud de Compra de divisas'),
				'type': 'ir.actions.act_window',
				'res_model': 'application.request.approval',
				'view_mode': 'form',
				'view_type': 'form',
				'context': {'current_app_request_id': self.id},
				'views': [(False, 'form')],
				'target': 'new'
				}
		else:
			raise ValidationError(_('The application request dont have doc close.'))

	#Acción para asignar como borrador
	def action_draft(self):
		self.status = 'draft'
		if self.reason_rejection!=False:
			self.reason_rejection= None
			self.reason_rejection_desc= None 

	#Acción para asignar el estado de "Cotizada", si es que hay cotizaciones
	def action_quotes(self):
		if len(self.published_quotes)>0:
			self.status='quoted'	
		else:
			raise ValidationError(_('The application request dont have quotes'))
	
	#Acción para asignar como con recursos
	def action_resources(self):
		self.status = 'with_resources'

	def action_accept_adquired(self):
		cuenta_puente=self.env['account.movements'].search([('applications','=','divisas')]).account_id
		if self.doc_transfer!=False:
			if len(cuenta_puente)==1:
				self.status = 'adquired'
				self._create_move_lines(self,'adquired',cuenta_puente)
			else:
				raise UserError("No se pueden generar los apuntes contables. Revisar si se tiene una cuenta contable en la ventana 'Configuración fondo de divisas' con aplicación 'Fondo de Divisas'")
		else:
			raise UserError(_('The application request doesnt have the support documentation. Edit the record and add it in Transfer Doc.'))

	def action_recived(self):
		cuenta_puente=self.env['account.movements'].search([('applications','=','divisas')]).account_id
		if self.proof_payment_doc!=False:
			if self.check_filename(self):
				if len(cuenta_puente)==1:
					self.status = 'recived'
					self._create_move_lines(self,'recived',cuenta_puente)
					self._add_funds(self)
					self._minus_amount(self)
					if self.is_request_for_payment_request:
						self.env['account.payment'].notify_currency_purchase(self, self.batch)
				else:
					raise UserError("No se pueden generar los apuntes contables. Revisar si se tiene una cuenta contable en la ventana 'Configuración fondo de divisas' con aplicación 'Fondo de Divisas'")
			else:
				raise UserError(_('The application request doesnt have the CFDI documentation. Edit the record and add it in CFDI Doc.'))
		else:
			raise UserError('Se necesita subir el Documento soporte del comprobante bancario de pago para continuar el proceso.')



	def _get_quote(self):
		for rec in self:
			quotes=self.env[QUOTE].search([('application_request','=',rec.id),('status','=','published')])
			if len(quotes)!=0:
				rec.has_quotes=True
				rec.published_quotes=quotes
			else:
				rec.has_quotes=False
				rec.published_quotes=False
	
	@api.depends('transfer_request')
	def _get_transfer_moves(self):
		if len(self.transfer_request.payment_ids.move_line_ids)!=0:
			self.transfer_acc_move_lines=self.transfer_request.payment_ids.move_line_ids
	#		if(self.status=='approved'):
	#			self.status = 'with_resources'
		else:
			self.transfer_acc_move_lines=False
	


	#---------------------------------------------------------
	#					REVISIÓN DE INFORMACIÓN
	#---------------------------------------------------------
	def notify_rejected_request(self,request):
		self.env['mail.thread'].inbox_message(
			"Se rechazó la %s" % request.name,
			"Motivo de rechazo: %s" % request.reason_rejection.name,
			[self.env.user])
	
	#Revisión del monto
	@api.constrains('amount_funds')
	def _check_amount_funds(self):
		if self.amount_funds<=0:
			raise ValidationError(_('Amount fund cant be 0 o negative'))
		
	@api.depends('status','quotes')
	def _has_quotes(self):
		if self.status == 'published':
			if len(self.quotes)>0:
				self.has_quotes=True
			else:
				self.has_quotes=False

	@api.onchange('proof_payment_doc')
	def _check_proof_payment(self):
		if self.proof_payment_doc==False:
			self.doc_CFDI_pdf=False
			self.doc_CFDI_xml=False

	#Asignación de la moneda por el fondo fijo
	@api.depends('currency_fund_id')
	def _get_currency_fund(self):
		self.currency_id=self.currency_fund_id.currency_id

	def check_filename(self,record):
		if record.doc_CFDI_pdf==False or record.doc_CFDI_xml==False:
			return False		
		else:
			# Check the file's extension
			tmp_pdf = record.doc_CFDI_pdf_name.split('.')
			ext_pdf = tmp_pdf[len(tmp_pdf)-1]
			tmp_xml = record.doc_CFDI_xml_name.split('.')
			ext_xml = tmp_xml[len(tmp_xml)-1]
			if ext_pdf != 'pdf' or ext_xml != 'xml':
				return False
			else:
				return True
			
	def count_quotes(self):
		return {
		'name': 'Cotizaciones',
		'view_type': 'form',
		'view_mode': 'tree,form',
		'views': [(self.env.ref("jt_currency_purchase_req.quote_finance_tree").id, 'tree'), (self.env.ref("jt_currency_purchase_req.quote_finance_from").id, 'form')],
		'res_model': QUOTE,
		'domain': [('application_request', '=', self.id)],
		'type': 'ir.actions.act_window',
		}

	@api.depends('total_denomination')
	def _compute_amount_funds(self):
		for rec in self:
			denominations=rec.total_denomination
			#Si hay denominaciones asignar el valor total en la suma de denominaciones
			if denominations != False:
				amount=0.0
				den_info={
					'den_500':500.00,
					'den_200':200.00,
					'den_100':100.00,
					'den_50':50.00,
					'den_20':20.00,
					'den_10':10.00,
					'den_5':5.00,
					'den_2':2.00,
					'den_1':1.00,}

				for den in denominations:
					if den.count!= False and den.denominations!= False:
						amount=amount+round((den.count*den_info[den.denominations]),2)
				
				rec.amount_denomination = amount

			#Caso en que no hay denominaciones	
			else:
				rec.amount_denomination = 0.0

	#---------------------------------------------------------
	#			MODIFICACIÓN DE INFORMACIÓN
	#---------------------------------------------------------
	def _add_funds(self,record):
		aux_currency_fund=record.currency_fund_id
		amount=record.amount_funds
		aux_currency_fund.actual_fund = aux_currency_fund.actual_fund+amount

	def _minus_amount(self, record):
		if record.is_request_for_payment_request:
			committed= self.env['money.commited'].search([('folio','=',record.batch),('app_request','=',record.id)])
			moves = self.env[ACCOUNT_MOVE].search([('batch_folio','=',record.batch)])
			if len(committed)==1:
				committed.unlink()
			record.currency_fund_id.actual_fund = 0
			for sol in moves:
				record.currency_fund_id.account_move_id = [(4,sol.id)]
			self.env[CURRENCY].set_new_application_request(record.currency_id)
		


	def _create_move_lines(self,record,state,cuenta_puente):
		lines=[]
		quote=self.env[QUOTE].search([('application_request','=',record.id),('status','=','published'),('selected_quote','=',True)])
		acc_move=self.env[ACCOUNT_MOVE]
		nombre='Anticipo de compra de divisas'
		if len(cuenta_puente)==1:
			if state=="adquired":
				lines.append((0, 0, {
					'date':date.today(),
					'name':'Cuenta de retiro UNAM',
					'account_id': record.account_journal.default_credit_account_id.id,
					'credit':quote.withdrawal_amount,
				}))
				lines.append((0, 0, {
					'date':date.today(),
					'name':nombre,
					'account_id': cuenta_puente.id,
					'debit':quote.withdrawal_amount,
				}))

				move_val = {
					'ref': "Pago de divisas "+record.name,
					'conac_move': False,
					'date': datetime.now(),
					'journal_id': record.currency_fund_id.journal_id.id,
					'company_id': self.env.user.company_id.id,
					'line_ids': lines
					}
				# Creación de los apuntes contables
				move = acc_move.create(move_val)
				move.action_post()
				self.buy_acc_move_lines = move.line_ids
				
			
			
			elif state=="recived":
				lines.append((0, 0, {
					'date':date.today(),
					'name':nombre,
					'account_id': cuenta_puente.id,
					'credit':quote.withdrawal_amount,
				}))
				lines.append((0, 0, {
					'date':date.today(),
					'name':nombre,
					'account_id': record.currency_fund_id.associated_account.id,
					'debit':quote.withdrawal_amount,
				}))
				move_val = {
					'ref': "Recepción de divisas " + record.name,
					'conac_move': False,
					'date': datetime.now(),
					'journal_id': record.currency_fund_id.journal_id.id,
					'company_id': self.env.user.company_id.id,
					'line_ids': lines
					}
				# Creación de los apuntes contables
				move = acc_move.create(move_val)
				move.action_post()
				self.recived_acc_move_lines = move.line_ids



	#Función de seguridad en Módulo de Finanzas
	def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
		res = super().fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
		doc = etree.XML(res['arch'])

		"""Grupos de usuario"""
		user = self.env.user
		is_admin_finance = user.has_group(GROUP_ON_FINANCE_ADMIN)
		is_finance_user = user.has_group(GROUP_FINANCE_USER)
		is_currency_fund_user = user.has_group(GROUP_CURRENCY_FUNDS)
		is_quote_user = user.has_group(GROUP_QUOTES)
		is_approve_user = user.has_group(GROUP_REQUEST_APPROVAL)

		"""Botones"""
		buttons_quote_user = ['action_draft', 'action_approve_published', 'action_quotes']
		buttons_approve_user = ['action_approved_rejected', 'action_accept_adquired', 'action_recived']

		"""Permisos"""
		list_create=(is_admin_finance, is_finance_user, is_currency_fund_user)
		list_quote=(is_admin_finance, is_finance_user, is_quote_user)
		list_approve=(is_admin_finance, is_finance_user, is_approve_user)


		for node in doc.xpath("//" + view_type):
			node.set('create', '1' if any(list_create) else '0')

			if is_admin_finance or is_finance_user or is_quote_user:
				node.set('edit', '1')
				node.set('delete', '1')
				node.set('import', '1')
				node.set('export_xlsx', '1')
				node.set('export', '1')
				node.set('duplicate', '1')
			else:
				node.set('edit', '0')
				node.set('delete', '0')
				node.set('import', '0')
				node.set('export_xlsx', '0')
				node.set('export', '0')
				node.set('duplicate', '0')
		for node in doc.xpath("//button"):
			if (not any(list_quote)) and  (node.attrib['name'] in buttons_quote_user):
				node.set('modifiers', '{"invisible":true}')
			if (not any(list_approve)) and (node.attrib['name'] in buttons_approve_user):
				node.set('modifiers', '{"invisible":true}')

		res['arch'] = etree.tostring(doc)
		return res

	
class RejectionReason(models.Model):
	_name = 'application.request.rejection.reason'
	_description = 'Rejection Reason for Application Requests'

	name = fields.Char(string='Name', help='Rejection Reason Name', size=100)
	description = fields.Char(string='Description', help='Rejection Reason Description')

	#Función de seguridad en Módulo de Finanza
	@api.model
	def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
		res = super().fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
		doc = etree.XML(res['arch'])

		is_admin_finance = self.env.user.has_group(GROUP_ON_FINANCE_ADMIN)
		is_finance_user = self.env.user.has_group(GROUP_FINANCE_USER)
		
		"""
		Sección de Seguridad aplicado para:
			Módulo de Finanzas
			Solicitudes de recursos para la compra de divisas
				-Motivos de Rechazo
		"""
		for node in doc.xpath("//" + view_type):
			if is_admin_finance or is_finance_user:
				node.set('edit', '1')
				node.set('create', '1')
				node.set('delete', '1')
				node.set('import', '1')
				node.set('export_xlsx', '1')
			else:
				node.set('edit', '0')
				node.set('create', '0')
				node.set('delete', '0')
				node.set('import', '0')
				node.set('export_xlsx', '0')
	
		res['arch'] = etree.tostring(doc)

		return res
