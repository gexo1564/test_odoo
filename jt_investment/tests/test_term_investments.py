from unittest import mock
from odoo.tests import TransactionCase
from datetime import datetime, timedelta, date
from odoo.tests import tagged
from unittest.mock import patch
from unittest.mock import MagicMock
import calendar
from odoo.exceptions import UserError,ValidationError

TERM_INVESTMENTS_MODEL = 'term.investments'
INVESTMENT_PERIOD_RATE_MODEL = 'investment.period.rate'
DATE_FORMAT = '%Y-%m-%d'

@tagged('term_investments')
class TestTermInvestments(TransactionCase):
    ## Nuevos Unit test ##
    def test_set_expiration_date_in_business_day(self):
        investment = self.env[TERM_INVESTMENTS_MODEL].create([{
            'current_date': '2022-04-28',
            'investment_date': '2022-04-28',
            'term': 222, 
        }])

        investment_date = '2022-04-28'
        term = 222
        expected_date = '2022-12-06'

        result = investment.set_expiration_date_in_business_day(investment_date, term)
        self.assertEqual(str(result), expected_date)
        print('TEST -> ASIGNAR FECHA DIA HABIL MAS PROXIMO')


    def test_total_accrued_interest(self):
        investment = self.env[TERM_INVESTMENTS_MODEL].create([{
            'accrued_interest_ids': [(0, 0, {'accrued_interest': 100.50}), (0, 0, {'accrued_interest': 200})]
        }])

        expected_total = 300.50
        result = investment.total_accrued_interest(investment)
        self.assertEqual(result, expected_total)
        print('TEST -> SUMA DE INTERESES DEVENGADOS')


    def test_set_expired_finished(self):
        investment = self.env[TERM_INVESTMENTS_MODEL].create([{
            'is_expired': False,
            'state': 'approved',
                
        }])

        investment.set_expired_finished(investment)
        self.assertTrue(investment.is_expired)
        self.assertEqual(investment.state, 'finished')
        print('TEST -> SE ASIGNO FINALIZADO Y EXPIRADO')
    

    def test_is_last_business_day_month(self):
        investment = self.env[TERM_INVESTMENTS_MODEL].create([{
            'investment_amount': 1000.50,                
        }])

        current_date = '2022-01-31'
        self.assertTrue(investment.is_last_business_day_month(current_date))
        
        current_date = '2022-12-30'
        self.assertTrue(investment.is_last_business_day_month(current_date))

        current_date = '2022-12-31'
        self.assertFalse(investment.is_last_business_day_month(current_date))
        print('TEST -> SI ES ULTIMO DIA HABIL DEL MES')


    def test_update_elapsed_days(self):
        investment = self.env[TERM_INVESTMENTS_MODEL].create({
            'investment_date': '2022-04-28', 
            'term': 2,
        })
        investment_2 = self.env[TERM_INVESTMENTS_MODEL].create({
            'investment_date': '2022-04-28', 
            'term': 222,
        })
        
        current_date = '2022-12-06'
        investment_2.update_elapsed_days(current_date, investment_2)
        self.assertEqual(investment_2.days_elapsed, 222)
        
        current_date = '2022-04-29'
        investment.update_elapsed_days(current_date, investment)
        self.assertEqual(investment.days_elapsed, 2)
        print('TEST -> ACTUALIZA TIEMPO TRANSCURRIDO')
        

    def test_is_fixed_rate(self):
        investment = self.env[TERM_INVESTMENTS_MODEL].create({
            'investment_amount': 1000, 
        })

        rate_type = 'fixed_rate'
        self.assertTrue(investment.is_fixed_rate(rate_type))
        
        rate_type = 'variable_rate'
        self.assertFalse(investment.is_fixed_rate(rate_type))
        print('TEST -> ES TASA FIJA')
    
    def test_is_variable_rate(self):
        investment = self.env[TERM_INVESTMENTS_MODEL].create({
            'investment_amount': 1000, 
        })

        rate_type = 'fixed_rate'
        self.assertFalse(investment.is_variable_rate(rate_type))
        
        rate_type = 'variable_rate'
        self.assertTrue(investment.is_variable_rate(rate_type))
        print('TEST -> ES TASA VARIABLE')

    
    def test_accrued_interest(self):
        investment = self.env[TERM_INVESTMENTS_MODEL].create({
            'investment_amount': 6000000000, 
            'rate_type': 'fixed_rate', 
            'fixed_rate': 8.31
            })

        current_date = '2022-01-10'

        days_period = 2
        self.assertEqual(investment.accrued_interest(investment, days_period, current_date), 2770000.00)
        
        print('TEST -> INTERES DEVENGADO')


    def test_accrued_if_fixed_rate(self):
        investment = self.env[TERM_INVESTMENTS_MODEL].create({
            'investment_amount': 1000,
            'fixed_rate': 8,
        })
        investment_amount = 1000 
        fixed_rate = 8 
        days_period = 30

        result = investment.accrued_if_fixed_rate(investment_amount, fixed_rate, days_period)
        self.assertEqual(result, 6.6667)
        print('TEST -> INTERES SI ES TASA FIJA')

    def test_is_percentage(self):
        investment = self.env[TERM_INVESTMENTS_MODEL].create({
            'rate_type': 'variable_rate',
            'indicator': 'tiie',
            'percentage': 8.31, 
        })
   
        self.assertTrue(investment.is_percentage(investment))
        
        investment.percentage = 0
        self.assertFalse(investment.is_percentage(investment))
        print('TEST -> ES INVERSION CON PORCENTAJE')

    def test_is_surcharge(self):
        investment = self.env[TERM_INVESTMENTS_MODEL].create({
            'rate_type': 'variable_rate',
            'indicator': 'tiie',
            'surcharge': 2, 
        })
   
        self.assertTrue(investment.is_surcharge(investment))
        
        investment.surcharge = 0
        self.assertFalse(investment.is_surcharge(investment))
        print('TEST -> ES INVERSION CON SOBRETASA')

    
    def test_days_period(self):
        investment = self.env[TERM_INVESTMENTS_MODEL].create({
            'investment_date': '2022-04-01',
        })
        current_date =  datetime.strptime('2022-04-02', DATE_FORMAT).date()
      
        result = investment.days_period(investment, current_date)
        self.assertEqual(result, 2)
        print('TEST -> CALCULA DIAS DEL PERIODO EN EL MES')

    def test_indicator_average(self):
        # Prepare test data
        start_date = datetime.strptime('2022-09-01', DATE_FORMAT).date()
        end_date = datetime.strptime('2022-09-30', DATE_FORMAT).date()
        term_investments_obj = self.env[TERM_INVESTMENTS_MODEL]

        type_indicator = 'tiie'
        rate_values_tiie = term_investments_obj.rate_values_tiie(start_date, end_date)
        new_rate_values = term_investments_obj.fill_empty_values_rates(rate_values_tiie)
        sum_rate_values = term_investments_obj.sum_rate_values(new_rate_values)
        new_length = len(new_rate_values)
        expected_indicator_average_tiie = sum_rate_values / new_length
        expected_indicator_average_round_tiie = round(expected_indicator_average_tiie, 4)

        indicator_average_round = term_investments_obj.indicator_average(start_date, end_date, type_indicator)
        self.assertEqual(indicator_average_round, expected_indicator_average_round_tiie)

        type_indicator = 'cetes'
        rate_values_cetes = term_investments_obj.rate_values_cetes(start_date, end_date)
        new_rate_values = term_investments_obj.float_list_cetes(rate_values_cetes)
        sum_rate_values = term_investments_obj.sum_rate_values(new_rate_values)
        new_length = len(new_rate_values)
        expected_indicator_average_cetes = sum_rate_values / new_length
        expected_indicator_average_round_cetes = round(expected_indicator_average_cetes, 4)

        indicator_average_round = term_investments_obj.indicator_average(start_date, end_date, type_indicator)
        self.assertEqual(indicator_average_round, expected_indicator_average_round_cetes)

    def test_indicator_is_tiie(self):
        # Prepare test data
        type_indicator = 'tiie'
        expected_result = True

        # Call the function to be tested
        result = self.env[TERM_INVESTMENTS_MODEL].indicator_is_tiie(type_indicator)
        # Assert the expected result
        self.assertEqual(result, expected_result)

        type_indicator = 'cetes'
        expected_result = False
        result = self.env[TERM_INVESTMENTS_MODEL].indicator_is_tiie(type_indicator)
        self.assertFalse(result, expected_result)
        print('TEST -> EL INDICADOR ES TIIE')

    def test_indicator_is_cetes(self):
        # Prepare test data
        type_indicator = 'tiie'
        expected_result = False
        # Call the function to be tested
        result = self.env[TERM_INVESTMENTS_MODEL].indicator_is_cetes(type_indicator)
        # Assert the expected result
        self.assertFalse(result, expected_result)

        type_indicator = 'cetes'
        expected_result = True
        result = self.env[TERM_INVESTMENTS_MODEL].indicator_is_cetes(type_indicator)
        self.assertTrue(result, expected_result)
        print('TEST -> EL INDICADOR ES CETES')    
    
    def test_fill_empty_values_rates(self):
        # Prepare test data
        start_date = '2022-10-01'
        end_date = '2022-10-31'
        expected_length = 31
        expected_average_round = round(9.52853225806451, 4)
        expected_rates_october = [9.02, 9.02, 9.535, 9.5366, 9.5453, 9.5515, 9.5451, 9.5451, 9.5451, 9.5465, 
                        9.545, 9.5515, 9.5595, 9.5635, 9.5635, 9.5635, 9.5655, 9.5665, 9.567, 9.5663, 
                        9.567, 9.567, 9.567, 9.5675, 9.575, 9.584, 9.591, 9.59, 9.59, 9.59, 9.594]
              
        rate_values = self.env[INVESTMENT_PERIOD_RATE_MODEL].search([('product_type', '=', 'TIIE'), ('rate_date', '>=', start_date), ('rate_date', '<=', end_date)])
       


        new_rate_values_october = self.env[TERM_INVESTMENTS_MODEL].fill_empty_values_rates(rate_values)
        new_length = len(new_rate_values_october)
        sum_rate_values = self.env[TERM_INVESTMENTS_MODEL].sum_rate_values(new_rate_values_october)
        new_average = sum_rate_values / new_length
        new_average_round = round(new_average, 4)

        self.assertEqual(new_rate_values_october, expected_rates_october)
        self.assertEqual(new_length, expected_length)
        self.assertEqual(new_average_round, expected_average_round)
        print('TEST -> NUEVOS VALORES LLENADOS')

    def test_last_rate_previous_month(self):
        current_date = date(2022, 10, 1)
        expected_last_rate = 9.0200
        last_rate_previous_month = self.env[TERM_INVESTMENTS_MODEL].last_rate_previous_month(current_date)

        self.assertEqual(last_rate_previous_month, expected_last_rate)
        print('TEST -> ULTIMO VALOR DE TASA DEL MES ANTERIOR')
    
    def test_have_first_date_value(self):
        current_date_true = date(2022, 10, 1)
        current_date_false = date(2022, 10, 3)
        have_first_date_value = self.env[TERM_INVESTMENTS_MODEL].have_first_date_value(current_date_true)
        not_have_first_date_value = self.env[TERM_INVESTMENTS_MODEL].have_first_date_value(current_date_false)

        self.assertTrue(have_first_date_value)
        self.assertFalse(not_have_first_date_value)
        print('TEST -> NUEVOS VALORES LLENADOS')

    def test_fill_first_day(self):
        first_date = date(2022, 10, 3)
        fill_rate_values = [5, 4, 5]
        last_rate_previous_month = 1
        expected_fill_rate_values = [1, 1, 5, 4, 5]

        fill_rate_values  = self.env[TERM_INVESTMENTS_MODEL].fill_first_day(first_date, fill_rate_values, last_rate_previous_month)
        
        self.assertEqual(fill_rate_values, expected_fill_rate_values)
        print('TEST -> VALORES LLENADOS SINO HAY PRIMEROS DIAS')

    def test_prev_month_first_last_day(self):
        current_date = datetime(2023, 2, 28)
        expected_prev_mont_first_day = datetime(2023, 1, 1)
        expected_prev_mont_last_day = datetime(2023, 1 , 31)
        prev_month_first_day, prev_month_last_day = self.env[TERM_INVESTMENTS_MODEL].prev_month_first_last_day(current_date)

        self.assertEqual(prev_month_first_day, expected_prev_mont_first_day)
        self.assertEqual(prev_month_last_day, expected_prev_mont_last_day)
        print('TEST -> PRIMER Y ULTIMO DIA DEL MES ANTERIOR DADA UNA FECHA')

    def test_last_day_month(self):
        current_date = date(2023, 2, 25)
        expected_last_day_of_month = date(2023, 2, 28)

        last_day_of_month = self.env[TERM_INVESTMENTS_MODEL].last_day_month(current_date)
        self.assertEqual(last_day_of_month, expected_last_day_of_month)
        print('TEST -> ULTIMO DIA DEL MES DE LA FECHA ACTUAL')       

    def test_rate_values_tiie(self):
        # Prepare test data
        start_date = '2022-09-01'
        end_date = '2022-09-30'
        expected_rate_values = self.env[INVESTMENT_PERIOD_RATE_MODEL].search([('product_type', '=', 'TIIE'), ('rate_date', '>=', start_date), ('rate_date', '<=', end_date)])
        print(expected_rate_values)

        # Call the function to be tested
        rate_values = self.env[TERM_INVESTMENTS_MODEL].rate_values_tiie(start_date, end_date)
        # Assert the expected result
        self.assertEqual(rate_values, expected_rate_values)
        print('TEST -> VALORES DE TIIE Y NUMEROS DE REGISTROS')   
    
    def test_rate_values_cetes(self):
        # Prepare test data
        start_date = '2022-09-01'
        end_date = '2022-09-30'
        expected_rate_values = self.env[INVESTMENT_PERIOD_RATE_MODEL].search([('product_type', '=', 'CETES'), ('rate_date', '>=', start_date), ('rate_date', '<=', end_date)])

        # Call the function to be tested
        rate_values = self.env[TERM_INVESTMENTS_MODEL].rate_values_cetes(start_date, end_date)
        # Assert the expected result
        self.assertEqual(rate_values, expected_rate_values)
        print('TEST -> VALORES DE CETES Y NUMEROS DE REGISTROS')

    def test_sum_rate_values(self):
        investment = self.env[TERM_INVESTMENTS_MODEL].create({
            'investment_amount': 6000000000,
        })
        rate_values = [10, 10, 8.5]

        result = investment.sum_rate_values(rate_values)
        self.assertEqual(result, 28.5)
        print('TEST -> SUMA DE LOS INTERESES DEVENGADOS DEL REGISTRO') 
        

    def test_start_and_end_date(self):
        investment = self.env[TERM_INVESTMENTS_MODEL].create({
            'investment_date': '2022-04-28',
            'current_date': '2022-04-29',
        })
        investment_date = datetime.strptime('2022-04-28', DATE_FORMAT).date()
        current_date = datetime.strptime('2022-04-29', DATE_FORMAT).date()
        expected_start_date = investment_date
        expected_end_date = current_date
        start_date, end_date = investment.start_and_end_date(investment_date, current_date)
        self.assertEqual(start_date, expected_start_date)
        self.assertEqual(end_date, expected_end_date)

        investment_date = datetime.strptime('2022-04-28', DATE_FORMAT).date()
        current_date = datetime.strptime('2022-05-31', DATE_FORMAT).date()
        expected_start_date = datetime.strptime('2022-05-01', DATE_FORMAT).date()
        expected_end_date = datetime.strptime('2022-05-31', DATE_FORMAT).date()
        start_date, end_date = investment.start_and_end_date(investment_date, current_date)
        self.assertEqual(start_date, expected_start_date)
        self.assertEqual(end_date, expected_end_date)
        print('TEST -> FECHA DE INICIO Y FINAL PARA CONSULTAR LOS VALORES DE TASA')

         

 