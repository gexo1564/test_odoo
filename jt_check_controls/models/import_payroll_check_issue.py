import json
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError,UserError
from datetime import date, datetime, timedelta
from odoo.tools.misc import profile
import logging
_logger = logging.getLogger(__name__)

# Constants
YEAR_CONFIGURATION = 'year.configuration'
IMPORT_PAYROLL_CHECK_ISSUE = 'import.payroll.check.issue'
ACCOUNT_MOVE = 'account.move'

# Selections Values
FORTNIGHT = [(str(i).zfill(2),) * 2 for i in range(1, 25)]
STATE = [
    ('draft', 'Draft'),
    ('requested', 'Requested'),
    ('done', 'Done'),
    ('rejected', 'Rejected'),
]
SEARCH_TYPE = [
    ('check', 'Check'),
    ('employee', 'Employee')
]


class ImportPayrollCheckIssue(models.Model):

    _name = IMPORT_PAYROLL_CHECK_ISSUE
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = "Payroll check reissues"
    _rec_name = 'id'

    # Obtiene el id del año actual
    def _default_year(self):
        return self.env[YEAR_CONFIGURATION].search([('name', '=', date.today().year)], limit=1).id
    # Original Fortnight information
    original_year = fields.Many2one(YEAR_CONFIGURATION, string="Original Exercise", default=_default_year)
    original_fortnight = fields.Selection(FORTNIGHT, string="Select original fortnight")
    payroll_processing_id = fields.Many2one('custom.payroll.processing', 'Payroll')
    payment_request_original_id = fields.Many2one(ACCOUNT_MOVE,'Payment Request Original')
    # New fortnight information
    new_year = fields.Many2one(YEAR_CONFIGURATION,string="New Exercise", default=_default_year)
    new_fortnight = fields.Selection(FORTNIGHT, string='New fortnight', help='Select new fortnight')
    payment_request_reissue_id = fields.Many2one(ACCOUNT_MOVE,'Payment Request Reissue')
    # Employee information
    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee ID', help='Employee ID for reissue')
    employee_rfc = fields.Char(related='employee_id.rfc', string='Employee RFC', help='Employee RFC')
    employee_number = fields.Char(related='employee_id.worker_number', string='Employee number', help='Employee number')
    # Adjustment Case
    case_id = fields.Many2one(comodel_name='adjustment.cases', string='Adjustment case for reissue', help='Select adjustment case for reissue')
    # Original Check information
    original_check_id = fields.Many2one(comodel_name='check.log', string='Original check', help='Select original check number')
    original_check_status = fields.Selection(related='original_check_id.status', string='Status')
    original_check_general_status = fields.Selection(related='original_check_id.general_status', string='General Status')
    original_bank_id = fields.Many2one(comodel_name='res.bank', string="Original bank", help='Select original bank')
    original_amount = fields.Float(string='Original amount', help='Enter original import')
    # New Check information
    new_check_id = fields.Many2one('check.log', string='New check number', help='Select new check number for reissue')
    new_bank_id = fields.Many2one(comodel_name='res.bank', string="New bank", help='Select new bank for reissue')
    new_amount = fields.Float(string='New amount', help='Enter new amount')
    new_check_status = fields.Selection(related='new_check_id.status', string='Status')
    new_check_general_status = fields.Selection(related='new_check_id.general_status', string='General Status')

    upload_date = fields.Date(string='Date creation', default=datetime.today(), help='Date creation')
    # Reissue Status
    status = fields.Selection(STATE, required=True, readonly=True, copy=False, tracking=True, default='draft', string='Status', help='Reissue status')
    # Deposit Number
    deposite_number = fields.Char("Deposit number")
    # Flag for known if the reissue is from check or transfer
    is_from_check = fields.Boolean(compute='_compute_is_from_check', store=False)
    # Selection for search by check folio or the employee name

    _sql_constraints = [('original_check_id_new_check_id', 'unique(original_check_id,new_check_id)', 'Original check and new check already reissued')]


    @api.depends('case_id')
    def _compute_is_from_check(self):
        for record in self:
            record.is_from_check = record.case_id.case != 'S' or True


    def _get_payment_request_original_id(self):
        for record in self:
            if not record.original_check_id:
                record.payment_request_original_id = None
                continue
            query, param =  None, None
            if not record.original_check_id.related_check_folio_id:
                query = """
                    select id from account_move where
                    payroll_processing_id = %s
                    and check_folio_id is null
                    and type = 'in_invoice'
                    and is_payroll_payment_request is True
                """
                param = record.payroll_processing_id.id
            else:
                query = """
                    select id from account_move
                    where check_folio_id = %s
                    and is_payroll_payment_reissue is True
                    and type = 'in_invoice'
                """
                param = record.original_check_id.id
            self.env.cr.execute(query, (param,))
            record.payment_request_original_id = (self.env.cr.fetchone() or [None])[0]

    @api.onchange('new_check_id')
    def onchange_new_check_id(self):
        if self.new_check_id:
            self.new_bank_id = self.new_check_id.bank_account_id.bank_id

    @api.onchange('original_check_id')
    def onchange_original_check_id(self):
        if self.original_check_id:
            self.original_bank_id = self.original_check_id.bank_account_id.bank_id

    def action_from_draft_to_done(self):
        selected_draft_list_obj = self.filtered(lambda draft_state_selected:draft_state_selected.status == 'draft')
        for selected_draft_list_obj_element in selected_draft_list_obj:
            if selected_draft_list_obj_element.case_id.case in ['A', 'C', 'D', 'E', 'F', 'H', 'L', 'P', 'R', 'S', 'V', 'Z']:
                selected_draft_list_obj_element.original_check_id.write({'general_status' : 'cancelled', 'status' : 'Cancelled'})
                selected_draft_list_obj_element.new_check_id.write({'general_status' : 'assigned', 'status' : 'Printed'})
                selected_draft_list_obj_element.write({'status' : 'done'})

            # Change status of check
            elif selected_draft_list_obj_element.case_id.case in ['B', 'U']:
                selected_draft_list_obj_element.new_check_id.write({'general_status' : 'assigned', 'status' : 'Printed'})
                selected_draft_list_obj_element.write({'status' : 'done'})


    def action_all_from_draft_to_done(self):
        all_draft_list_obj = self.search([('status', '=', 'draft')])
        for all_draft_list_obj_element in all_draft_list_obj:
            if all_draft_list_obj_element.case_id.case in ['A', 'C', 'D', 'E', 'F', 'H', 'L', 'P', 'R', 'S', 'V', 'Z']:
                all_draft_list_obj_element.original_check_id.write({'general_status' : 'cancelled', 'status' : 'Cancelled'})
                all_draft_list_obj_element.new_check_id.write({'general_status' : 'assigned', 'status' : 'Printed'})
                all_draft_list_obj_element.write({'status' : 'done'})

            # Change status of check
            elif all_draft_list_obj_element.case_id.case in ['B', 'U']:
                all_draft_list_obj_element.new_check_id.write({'general_status' : 'assigned', 'status' : 'Printed'})
                all_draft_list_obj_element.write({'status' : 'done'})

    def _create_payroll_payment_reissue_request(self, first_reissue):
        # Asignación de la cuenta de la solicitud de pago
        if first_reissue:
            # Cuenta de acredores por sueldos/nómina
            account_id = self.env['account.catalog.type']._get_nominal_param('normal').id
        else:
            account_id = self.env['account.catalog.type']._get_nominal_param('check_forwarded').id
        return self.env[ACCOUNT_MOVE].create_payroll_payment_reissue_request(
            self.employee_id,
            self.payroll_processing_id,
            account_id,
            self.original_check_id,
            self.new_check_id,
            self.original_amount,
            self.case_id
        )

    def action_request(self):
        self.action_notify()
        # Se actualiza el status de la reexpedicón
        self.status = 'requested'


    def action_notify(self):
        check_control_admin_group = self.env.ref('jt_check_controls.group_check_control_admin')
        finance_admin_group = self.env.ref('jt_payroll_payment.group_finance_gestion')
        check_control_admin_users = check_control_admin_group.users
        finance_admin_users = finance_admin_group.users

        '''activity_type = self.env.ref('mail.mail_activity_data_todo').id
        summary = "Aprobar solicitud de cambios al cheque (Nómina)"
        activity_obj = self.env['mail.activity']
        model_id = self.env['ir.model'].sudo().search([('model', '=', 'import.payroll.check.issue')]).id'''
        user_set = set()

        for user in check_control_admin_users | finance_admin_users:
            if user.id not in user_set:
                '''activity_obj.create({
                    'activity_type_id': activity_type,
                    'res_model': IMPORT_PAYROLL_CHECK_ISSUE,
                    'res_id': self.id,
                    'res_model_id': model_id,
                    'summary': summary,
                    'user_id': user.id
                })'''
                user_set.add(user.id)

    def action_approve(self):
        # Si el cheque original no tiene un cheque relacionado, es la primer
        # reexpedición, por lo tanto, se crea la solicitud de reexpedición
        if not self.original_check_id.related_check_folio_id:
            # Se crea la solicitud de pago de reexpedición
            # Cuentas de acreedores/cancelado
            self.payment_request_reissue_id = self._create_payroll_payment_reissue_request(True)
        else:
            # Se cancela el método de pago de la solicitud de pago existente
            query = """
                update account_move set payment_state = 'payment_method_cancelled'
                where is_payroll_payment_reissue is True
                and type = 'in_invoice'
                and payment_state = 'paid'
                and check_folio_id = %s
            """
            self.env.cr.execute(query, (self.original_check_id.id,))
            # Se crea la solicitud de pago de reexpedición
            # Cuentas de reexpedido/cancelado
            self.payment_request_reissue_id = self._create_payroll_payment_reissue_request(False)
            # # Se actualiza la referencia del cheque de la solicitud de pago
            # # y se regresa a aprobada para pago.
            # payment_request_reissue_id = self.env[ACCOUNT_MOVE].search([
            #     ('is_payroll_payment_reissue', '=', True),
            #     ('check_folio_id', '=', self.original_check_id.id),
            #     ('type', '=', 'in_invoice')
            # ])
            # payment_request_reissue_id.write({
            #     'check_folio_id': self.new_check_id.id,
            #     'related_check_history': self.original_check_id.folio_ch,
            #     'payment_state': 'approved_payment',
            #     'batch_folio': 0
            # })
            # pass
        # Se cancela el pago asociado al cheque
        query_payment = "select id from account_payment where check_folio_id = %s and payment_state = 'for_payment_procedure'"
        self.env.cr.execute(query_payment, (self.original_check_id.id,))
        payment_id = (self.env.cr.fetchone() or [None])[0]
        if payment_id:
            payment_id = self.env['account.payment'].browse([payment_id])
            payment_id.paymet_reject(payment_id.id, 'Reexpedición de cheque.')
        # Actualiza datos del nuevo cheque
        self.new_check_id.write({
            'check_amount': self.new_amount,
            'date_printing': date.today(),
            'related_check_folio_id': self.original_check_id.id,
        })
        # Actualiza el status del cheque a reexpedir
        self.original_check_id.write({
            'status': 'Reissued',
            'general_status': 'cancelled',
        })
        # Actualiza el recibo de pago del empleado
        query = """
            update employee_payroll_file set
                check_folio_id = null,
                check_final_folio_id = %s
            where check_folio_id = %s or check_final_folio_id = %s
        """
        self.env.cr.execute(query,(self.new_check_id.id, self.original_check_id.id, self.original_check_id.id))
        # Se actualiza el status de la reexpedicón
        self.status = 'done'


    """
    @api.constrains('original_check', 'new_check')
    def folio_constraints(self):
        if not self.original_check:
            raise UserError(_("El campo Número de cheque original no debe estar vacío."))
        elif not self.new_check:
            raise UserError(_("El campo Nuevo número de cheque no debe estar vacío."))

    def _compute_folio_clave(self):
        if self.original_check and self.original_bank_code:
            cod = self.env['check.log'].search([('bank_id.bank_id.l10n_mx_edi_code','=',self.original_bank_code),
                                                ('folio','=',self.original_check)],limit=1)
            if cod:
                self.original_check_id = cod.id
        if not self.original_check:
            self.original_check = '1'

    def _compute_folio_clave_nuevo(self):
        if self.new_bank_code and self.new_check:
            cod_2 = self.env['check.log'].search([('bank_id.bank_id.l10n_mx_edi_code','=',self.new_bank_code),
                                                ('folio','=',self.new_check)],limit=1)
            if cod_2:
                self.new_check_id = cod_2.id
        if not self.new_check:
            self.new_check = '1'


    def action_update_check_and_amount(self):
        for rec in self:
            if rec.new_amount >= 0 and rec.status == 'Borrador':
                if rec.original_check_id:
                    move_id = self.env[ACCOUNT_MOVE].search([('check_folio_id','=',rec.original_check_id.id),
                                                               ('partner_id.vat','=',rec.rfc)],limit=1)
                    cheque_nomina = self.env['payment.batch.supplier'].search([
                        ('payment_issuing_bank_id.bank_id.l10n_mx_edi_code','=',rec.original_bank_code),
                        ('payment_req_ids.check_folio_id','=',rec.original_check_id.id)],limit=1)

                    if move_id.check_status == 'Charged':
                        raise UserError(_("No se puede reexpedir un cheque en estatus Cobrado"))
                    
                    if move_id: 
                        #move_id.action_draft()
                        #move_id.action_register()
                        if rec.new_check_id:
                            move_id.check_folio_id = rec.new_check_id.id
                            move_id.related_check_history = rec.original_check_id.folio
                            rec.new_check_id.status = 'Printed'
                            cheque_nomina.payment_req_ids.check_folio_id = rec.new_check_id.id
                        rec.original_check_id.status = 'Reissued'
                        rec.status = 'Hecho'
                        notification = {
                            'type': 'ir.actions.client',
                            'tag': 'display_notification',
                            'params': {
                                'title': _('¡Realizado!'),
                                'message': _('Registro actualizado'),
                                'type': 'success',  # types: success,warning,danger,info
                                'sticky': True, # True/False will display for few seconds if false
                            },
                        }
                        return notification
                    else:
                        raise UserError(_("No existen coincidencias en las Solicitudes de Pago de Nómina"))

            # if rec.new_amount==0:
            #     if rec.original_check_id:
            #         move_id = self.env[ACCOUNT_MOVE].search([('check_folio_id','=',rec.original_check_id.id)],limit=1)
            #         if move_id:
            #             if rec.new_check_id:
            #                 move_id.check_folio_id = rec.new_check_id.id
            #                 move_id.related_check_history = rec.original_check_id.folio
            #                 rec.new_check_id.status = 'Printed'
            #         rec.original_check_id.status = 'Reissued'
            #         rec.status = 'Hecho'
            #         notification = {
            #             'type': 'ir.actions.client',
            #             'tag': 'display_notification',
            #             'params': {
            #                 'title': _('¡Realizado!'),
            #                 'message': _('Registro actualizado'),
            #                 'type': 'success',  # types: success,warning,danger,info
            #                 'sticky': True, # True/False will display for few seconds if false
            #             },
            #         }
            #         return notification

            if rec.status == 'Hecho':
                notification = {
                    'type': 'ir.actions.client',
                    'tag': 'display_notification',
                    'params': {
                    'title': _('¡Error!'),
                    'message': _('Registro ya procesado'),
                    'type': 'danger',  # types: success,warning,danger,info
                    'sticky': True,  # True/False will display for few seconds if false
                    },
                }
            return notification
    """

    def unlink(self):
        for rec in self:
            if rec.status == 'done':
                raise UserError(_("You can not delete records in done state"))

        return super(ImportPayrollCheckIssue, self).unlink()
