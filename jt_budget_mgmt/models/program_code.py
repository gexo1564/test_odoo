# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import datetime
import logging
from odoo.exceptions import ValidationError
from odoo import models, fields, api, _
import json

PROGRAM_CODE = 'program.code'
YEAR_CONFIGURATION = 'year.configuration'
SUB_PROGRAM = 'sub.program'
SUB_DEPENDENCY = 'sub.dependency'
EXPENDITURE_ITEM = 'expenditure.item'
RESOURCE_ORIGIN = 'resource.origin'
INSTITUTIONAL_ACTIVITY = 'institutional.activity'
BUDGET_PROGRAM_CONVERSION = 'budget.program.conversion'
DEPARTURE_CONVERSION = 'departure.conversion'
EXPENSE_TYPE = 'expense.type'
KEY_WALLET = 'key.wallet'
PROJECT_TYPE = 'project.type'
AGREEMENT_TYPE = 'agreement.type'
EXPENDITURE_BUDGET_LINE = 'expenditure.budget.line'
PROJECT_PROJECT = 'project.project'

class ProgramCode(models.Model):

    _name = PROGRAM_CODE
    _description = 'Program Code'
    _rec_name = 'program_code'

    budget_id = fields.Many2one('expenditure.budget', index=True)

    year = fields.Many2one(YEAR_CONFIGURATION, string='Year (YEAR)', states={
                           'validated': [('readonly', True)]}, index=True)

    # Program Relations
    program_id = fields.Many2one('program', string='Program')
    key_unam_digit_id = fields.Many2one(related="program_id.program_key_id",string='KU')
    desc_program = fields.Text(
        string='Description KEY UNAM', related="program_id.desc_key_unam")

    # Sub Program Relation
    sub_program_id = fields.Many2one(SUB_PROGRAM, string='Sub program', states={
                                     'validated': [('readonly', True)]})
    desc_sub_program = fields.Text(
        string='Sub Program Description', related="sub_program_id.desc")

    # Dependency Relation
    dependency_id = fields.Many2one('dependency', string='Dependency')
    desc_dependency = fields.Text(
        string='Dependency Description', related="dependency_id.description")

    # Sub Dependency Relation
    sub_dependency_id = fields.Many2one(
        SUB_DEPENDENCY, string='Sub dependency')
    desc_sub_dependency = fields.Text(
        string='Sub-dependency Description', related="sub_dependency_id.description")

    # Item Relation
    item_id = fields.Many2one(
        EXPENDITURE_ITEM, string='Item')
    desc_item = fields.Text(string='Description of Item',
                            related="item_id.description")

    @api.depends('program_id', 'program_id.key_unam',
                 'sub_program_id', 'sub_program_id.sub_program',
                 'dependency_id', 'dependency_id.dependency',
                 'sub_dependency_id', 'sub_dependency_id.sub_dependency',
                 'item_id', 'item_id.item'
                 )
    def _compute_check_digit(self):
        dv_obj = self.env['verifying.digit']
        for pc in self:
            pc.check_digit = '00'
            if pc.program_id and pc.sub_program_id and pc.dependency_id and pc.sub_dependency_id and pc.item_id:
                vd = dv_obj.check_digit_from_codes(
                    pc.program_id, pc.sub_program_id, pc.dependency_id, pc.sub_dependency_id, pc.item_id)
                pc.check_digit = vd

    check_digit = fields.Char(
        string='Check Digit (DV)', size=2, compute="_compute_check_digit", store=True)

    # Resource Origin Relation
    resource_origin_id = fields.Many2one(
        RESOURCE_ORIGIN, string='Key Origin resource', states={'validated': [('readonly', True)]})
    desc_resource_origin = fields.Char(string='Description Resource Origin', related="resource_origin_id.desc")

    # Institutional Activity Relation
    institutional_activity_id = fields.Many2one(
        INSTITUTIONAL_ACTIVITY, string='Institutional Activity Number', states={'validated': [('readonly', True)]})
    desc_institutional_activity = fields.Text(
        string='Activity Description Institutional', related="institutional_activity_id.description")

    # Budget ProgramConversion Relation
    budget_program_conversion_id = fields.Many2one(
        BUDGET_PROGRAM_CONVERSION, string='Conversion Program SHCP', states={'validated': [('readonly', True)]})
    desc_budget_program_conversion = fields.Text(
        string='Description Conversion Program SHCP', related="budget_program_conversion_id.description")

    # Federal Item Relation
    conversion_item_id = fields.Many2one(
        DEPARTURE_CONVERSION, string='Federal Item', states={'validated': [('readonly', True)]})
    desc_conversion_item = fields.Text(
        string='Description of Federal Item', related="conversion_item_id.federal_part_desc")
    sg_id = fields.Many2one(related="conversion_item_id.conversion_key_id",string="SG")
    # Expense Type Relation
    expense_type_id = fields.Many2one(
        EXPENSE_TYPE, string='Key Expenditure Type', states={'validated': [('readonly', True)]})
    desc_expense_type = fields.Text(
        string='Description Expenditure Type', related="expense_type_id.description_expenditure_type")

    # Geographic Location Relation
    location_id = fields.Many2one(
        'geographic.location', string='Geographic Location', states={'validated': [('readonly', True)]},
        compute="_compute_location_id",store=True
    )
    desc_location = fields.Text(
        string='Name of Geographic Location', related="location_id.state_name")

    # Wallet Password Relation
    portfolio_id = fields.Many2one(KEY_WALLET, string='Key portfolio', states={
                                   'validated': [('readonly', True)]})
    name_portfolio = fields.Text(
        string='Name of Portfolio Key', related="portfolio_id.wallet_password_name")

    # Project Type Relation
    project_type_id = fields.Many2one(PROJECT_TYPE, string='Type of Project', states={
                                      'validated': [('readonly', True)]})
    desc_project_type = fields.Char(
        string='Description Type of Project', related="project_type_id.desc_stage")
    project_number = fields.Char(
        string='Project Number', related='project_type_id.number')

    # Stage Relation
    stage_id = fields.Many2one('stage', string='Stage', states={
                               'validated': [('readonly', True)]})
    desc_stage = fields.Text(string='Stage Description',
                             related='stage_id.desc_stage')

    # Agreement Relation
    agreement_type_id = fields.Many2one(AGREEMENT_TYPE, string='Type of Agreement', states={
                                        'validated': [('readonly', True)]})
    name_agreement = fields.Text(
        string='Name type of Agreement', related='agreement_type_id.name_agreement')
    number_agreement = fields.Char(
        string='Agreement number', related='agreement_type_id.number_agreement')

    total_assigned_amt = fields.Float(
        string="Assigned Total Annual", compute="_compute_amt")
    total_1_assigned_amt = fields.Float(
        string="Assigned 1st Annual", compute="_compute_amt")
    total_2_assigned_amt = fields.Float(
        string="Assigned 2nd Annual", compute="_compute_amt")
    total_3_assigned_amt = fields.Float(
        string="Assigned 3rd Annual", compute="_compute_amt")
    total_4_assigned_amt = fields.Float(
        string="Assigned 4th Annual", compute="_compute_amt")
    total_authorized_amt = fields.Float(
        string="Authorized", compute="_compute_amt")

    _sql_constraints = [('program_code', 'unique(program_code)', _('Program code must be unique.'))]

    def _compute_amt(self):
        bud_line_obj = self.env[EXPENDITURE_BUDGET_LINE]
        for code in self:
            lines = bud_line_obj.search([('program_code_id', '=', code.id),
                                         ('expenditure_budget_id.state', '=', 'validate')])
            authorized = assigned = st_ass = nd_ass = rd_ass = th_ass = 0
            for line in lines:
                if not line.imported_sessional:
                    authorized += line.authorized
                assigned += line.assigned
                if line.start_date and line.end_date and line.start_date.month == 1 and \
                        line.start_date.day == 1 and line.end_date.month == 3 and line.end_date.day == 31:
                    st_ass += line.assigned
                elif line.start_date and line.end_date and line.start_date and line.end_date and line.start_date.month == 4 and \
                        line.start_date.day == 1 and line.end_date.month == 6 and line.end_date.day == 30:
                    nd_ass += line.assigned
                elif line.start_date and line.end_date and line.start_date.month == 7 and \
                        line.start_date.day == 1 and line.end_date.month == 9 and line.end_date.day == 30:
                    rd_ass += line.assigned
                elif line.start_date and line.end_date and line.start_date.month == 10 and \
                        line.start_date.day == 1 and line.end_date.month == 12 and line.end_date.day == 31:
                    th_ass += line.assigned
            code.total_assigned_amt = assigned
            code.total_authorized_amt = authorized
            code.total_1_assigned_amt = st_ass
            code.total_2_assigned_amt = nd_ass
            code.total_3_assigned_amt = rd_ass
            code.total_4_assigned_amt = th_ass

    @api.constrains('program_code_copy')
    def _check_program_code(self):
        for record in self:
            logging.critical(record.program_code_copy)
            if len(record.program_code_copy) != 60:
                raise ValidationError('Program code must be 60 characters!')

    @api.depends('year', 'year.name', 'program_id', 'program_id.key_unam',
                 'sub_program_id', 'sub_program_id.sub_program',
                 'dependency_id', 'dependency_id.dependency',
                 'sub_dependency_id', 'sub_dependency_id.sub_dependency',
                 'item_id', 'item_id.item', 'check_digit',
                 'resource_origin_id', 'resource_origin_id.key_origin',
                 'institutional_activity_id', 'institutional_activity_id.number',
                 'budget_program_conversion_id', 'budget_program_conversion_id.shcp', 'budget_program_conversion_id.shcp.name',
                 'conversion_item_id', 'conversion_item_id.federal_part',
                 'expense_type_id', 'expense_type_id.key_expenditure_type',
                 'location_id', 'location_id.state_key',
                 'portfolio_id', 'portfolio_id.wallet_password',
                 'project_type_id', 'project_type_id.project_type_identifier', 'project_type_id.number',
                 'stage_id', 'stage_id.stage_identifier',
                 'agreement_type_id', 'agreement_type_id.agreement_type', 'agreement_type_id.number_agreement',
                 )
    def _compute_program_code(self):
        for pc in self:
            program_code = ''
            if pc.year:
                program_code += str(pc.year.name)
            if pc.program_id and pc.program_id.key_unam:
                program_code += str(pc.program_id.key_unam)
            if pc.sub_program_id and pc.sub_program_id.sub_program:
                program_code += str(pc.sub_program_id.sub_program)
            if pc.dependency_id and pc.dependency_id.dependency:
                program_code += str(pc.dependency_id.dependency)
            if pc.sub_dependency_id and pc.sub_dependency_id.sub_dependency:
                program_code += str(pc.sub_dependency_id.sub_dependency)
            if pc.item_id and pc.item_id.item:
                program_code += str(pc.item_id.item)
            if pc.check_digit:
                program_code += str(pc.check_digit)
            if pc.resource_origin_id and pc.resource_origin_id.key_origin:
                program_code += str(pc.resource_origin_id.key_origin)
            if pc.institutional_activity_id and pc.institutional_activity_id.number:
                program_code += str(pc.institutional_activity_id.number)
            if pc.budget_program_conversion_id and pc.budget_program_conversion_id.shcp and pc.budget_program_conversion_id.shcp.name:
                program_code += str(pc.budget_program_conversion_id.shcp.name)
            if pc.conversion_item_id and pc.conversion_item_id.federal_part:
                program_code += str(pc.conversion_item_id.federal_part)
            if pc.expense_type_id and pc.expense_type_id.key_expenditure_type:
                program_code += str(pc.expense_type_id.key_expenditure_type)
            if pc.location_id and pc.location_id.state_key:
                program_code += str(pc.location_id.state_key)
            if pc.portfolio_id and pc.portfolio_id.wallet_password:
                program_code += str(pc.portfolio_id.wallet_password)

            # Project Related Fields Data
            if pc.project_type_id and pc.project_type_id.project_type_identifier:
                program_code += str(pc.project_type_id.project_type_identifier)

            if pc.project_type_id and pc.project_type_id.number:
                program_code += str(pc.project_type_id.number)

            if pc.stage_id and pc.stage_id.stage_identifier:
                program_code += str(pc.stage_id.stage_identifier)

            if pc.agreement_type_id and pc.agreement_type_id.agreement_type:
                program_code += str(pc.agreement_type_id.agreement_type)

            if pc.agreement_type_id and pc.agreement_type_id.number_agreement:
                program_code += str(pc.agreement_type_id.number_agreement)

            pc.program_code_copy = program_code

    program_code = fields.Text(string='Programmatic Code')
    program_code_copy = fields.Text(string='Programmatic Code', compute="_compute_program_code", store=True)

    search_key = fields.Text(string='Search Key', index=True,
                             store=True, compute="_compute_program_search_key")

    @api.depends('year', 'program_id', 'sub_program_id', 'dependency_id', 'sub_dependency_id',
                 'item_id', 'resource_origin_id', 'institutional_activity_id', 'budget_program_conversion_id',
                 'conversion_item_id', 'expense_type_id', 'location_id', 'portfolio_id', 'project_type_id',
                 'stage_id', 'agreement_type_id')
    def _compute_program_search_key(self):
        for record in self:
            key_fields = ('year', 'program_id', 'sub_program_id', 'dependency_id', 'sub_dependency_id',
                          'item_id', 'resource_origin_id', 'institutional_activity_id', 'budget_program_conversion_id',
                          'conversion_item_id', 'expense_type_id', 'location_id', 'portfolio_id', 'project_type_id',
                          'stage_id', 'agreement_type_id')
            record.search_key = ';'.join(
                [str(record[fld].id) if fld in record else '' for fld in key_fields])

    @api.depends('sub_program_id')
    def _compute_location_id(self):
        for record in self:
            record.location_id = self.sub_program_id.geographic_location_id

    state = fields.Selection(
        [('draft', 'Draft'), ('validated', 'Validated')], default='draft', string='Status')

    @api.model
    def default_get(self, fields):
        res = super(ProgramCode, self).default_get(fields)
        year_str = str(datetime.today().year)
        year = self.env[YEAR_CONFIGURATION].sudo().search(
            [('name', '=', year_str)], limit=1)
        if not year:
            year = self.env[YEAR_CONFIGURATION].sudo().create({
                'name': year_str})
        if year:
            res['year'] = year.id
        return res

    def verify_program_code(self):
        for pc in self:
            program_code = ''
            if pc.year:
                program_code += str(pc.year.name)
            if pc.program_id and pc.program_id.key_unam:
                program_code += str(pc.program_id.key_unam)
            if pc.sub_program_id and pc.sub_program_id.sub_program:
                program_code += str(pc.sub_program_id.sub_program)
            if pc.dependency_id and pc.dependency_id.dependency:
                program_code += str(pc.dependency_id.dependency)
            if pc.sub_dependency_id and pc.sub_dependency_id.sub_dependency:
                program_code += str(pc.sub_dependency_id.sub_dependency)
            if pc.item_id and pc.item_id.item:
                program_code += str(pc.item_id.item)
            if pc.check_digit:
                program_code += str(pc.check_digit)
            if pc.resource_origin_id and pc.resource_origin_id.key_origin:
                program_code += str(pc.resource_origin_id.key_origin)
            if pc.institutional_activity_id and pc.institutional_activity_id.number:
                program_code += str(pc.institutional_activity_id.number)
            if pc.budget_program_conversion_id and pc.budget_program_conversion_id.shcp and pc.budget_program_conversion_id.shcp.name:
                program_code += str(pc.budget_program_conversion_id.shcp.name)
            if pc.conversion_item_id and pc.conversion_item_id.federal_part:
                program_code += str(pc.conversion_item_id.federal_part)
            if pc.expense_type_id and pc.expense_type_id.key_expenditure_type:
                program_code += str(pc.expense_type_id.key_expenditure_type)
            if pc.location_id and pc.location_id.state_key:
                program_code += str(pc.location_id.state_key)
            if pc.portfolio_id and pc.portfolio_id.wallet_password:
                program_code += str(pc.portfolio_id.wallet_password)

            # Project Related Fields Data
            if pc.project_type_id and pc.project_type_id.project_type_identifier:
                program_code += str(pc.project_type_id.project_type_identifier)

            if pc.project_type_id and pc.project_type_id.number:
                program_code += str(pc.project_type_id.number)

            if pc.stage_id and pc.stage_id.stage_identifier:
                program_code += str(pc.stage_id.stage_identifier)

            if pc.agreement_type_id and pc.agreement_type_id.agreement_type:
                program_code += str(pc.agreement_type_id.agreement_type)

            if pc.agreement_type_id and pc.agreement_type_id.number_agreement:
                program_code += str(pc.agreement_type_id.number_agreement)

            return program_code

    @api.model
    def create(self, vals):
        res = super(ProgramCode, self).create(vals)
#         if res.sub_program_id:
#             subprogram_obj = self.env[SUB_PROGRAM].search_read([('dependency_id','!=',False),('sub_dependency_id','!=',False),('unam_key_id','!=',False)], fields=['id','dependency_id','sub_dependency_id' ,'unam_key_id', 'sub_program'])
#             subprogram = list(filter(lambda subp: subp['sub_program'] == res.sub_program_id.sub_program and subp['unam_key_id'][0] == res.program_id.id and subp['dependency_id'][0] == res.dependency_id.id and subp['sub_dependency_id'][0] == res.sub_dependency_id.id, subprogram_obj))
#             subprogram = subprogram[0]['id'] if subprogram else False
#             if subprogram:
#                 res.sub_program_id = subprogram
#         code = res.verify_program_code()
#         program_code = self.search(
#             [('program_code_copy', '=', code), ('id', '!=', res.id)], limit=1)
#         if program_code:
#             raise ValidationError("Program code must be unique")
        res.program_code = res.program_code_copy
        return res

#   def write(self, vals):
#       code = self.verify_program_code()
#       program_code = self.search([('program_code_copy', '=', code), ('id', '!=', self.id)], limit=1)
#       if program_code:
#           raise ValidationError("Program code must be unique")
#       res = super(ProgramCode, self).write(vals)
#       return res

    def unlink(self):
        for code in self:
            if code.state == 'validated':
                raise ValidationError(
                    _('You can not delete validated program code!'))
            # line = self.env[EXPENDITURE_BUDGET_LINE].search(
            #     [('program_code_id', '=', code.id)], limit=1)
            # if line:
            #     raise ValidationError(
            #         'You can not delete program code which are mapped with expenditure budgets!')
        return super(ProgramCode, self).unlink()

    def update_program_master_data(self):

        subprogram_obj = self.env[SUB_PROGRAM].search_read([('dependency_id','!=',False),('sub_dependency_id','!=',False),('unam_key_id','!=',False)], fields=['id','dependency_id','sub_dependency_id' ,'unam_key_id', 'sub_program'])
        dpc_obj = self.env[DEPARTURE_CONVERSION].search_read([('item_id','!=',False)], fields=['id', 'federal_part','item_id'])
        shcp_obj = self.env[BUDGET_PROGRAM_CONVERSION].search_read([('shcp_name','!=',False),('program_key_id','!=',False),('conversion_key_id','!=',False)], fields=['id','program_key_id', 'shcp_name','conversion_key_id','federal_part'])
        subdependancy_obj = self.env[SUB_DEPENDENCY].search_read([],
                                                                       fields=['id', 'dependency_id', 'sub_dependency'])
        project_type_obj = self.env[PROJECT_TYPE].search_read([],
                                                                fields=['id', 'project_type_identifier', 'number'])
        agreement_type_obj = self.env[AGREEMENT_TYPE].search_read([], fields=['id', 'agreement_type',
                                                                                'number_agreement'])
        
        for rec in self:
            if rec.sub_dependency_id and rec.dependency_id.id != rec.sub_dependency_id.dependency_id.id:   
                subdependency = list(filter(
                    lambda sdo: sdo['sub_dependency'] == rec.sub_dependency_id.sub_dependency and sdo['dependency_id'][
                        0] == rec.dependency_id.id, subdependancy_obj))
                subdependency = subdependency[0]['id'] if subdependency else False                
                if subdependency:
                    rec.sub_dependency_id = subdependency
                    
            if rec.sub_program_id:
                subprogram = list(filter(lambda subp: subp['sub_program'] == rec.sub_program_id.sub_program and subp['unam_key_id'][0] == rec.program_id.id and subp['dependency_id'][0] == rec.dependency_id.id and subp['sub_dependency_id'][0] == rec.sub_dependency_id.id, subprogram_obj))
                subprogram = subprogram[0]['id'] if subprogram else False
                if subprogram:
                    rec.sub_program_id = subprogram
                    
            if rec.item_id and rec.conversion_item_id and rec.conversion_item_id.item_id.id != rec.item_id.id:
                
                conversion_item = list(filter(lambda coit: coit['federal_part'] == rec.conversion_item_id.federal_part and coit['item_id'][0]==rec.item_id.id, dpc_obj))
                conversion_item = conversion_item[0]['id'] if conversion_item else False
                if conversion_item and conversion_item != rec.conversion_item_id.id:
                    rec.conversion_item_id = conversion_item
                    
            if rec.budget_program_conversion_id and rec.conversion_item_id and rec.key_unam_digit_id:
                shcp = list(filter(lambda tmp: tmp['shcp_name'] == rec.budget_program_conversion_id.shcp_name and tmp['program_key_id'][0] == rec.key_unam_digit_id.id and tmp['conversion_key_id'][0]==rec.conversion_item_id.conversion_key_id.id,shcp_obj))
                shcp = shcp[0]['id'] if shcp else False
                if shcp and shcp != rec.budget_program_conversion_id.id:
                    rec.budget_program_conversion_id = shcp
            
            if rec.project_type_id:
                project_type = list(filter(
                                lambda pt: pt['project_type_identifier'] == rec.project_type_id.project_type_identifier and pt[
                                    'number'] == rec.project_id.number,
                                project_type_obj))
                project_type = project_type[0]['id'] if project_type else False
                if project_type:
                    rec.project_type_id = project_type
            
            if rec.agreement_type_id:
                agreement_type = list(
                                filter(lambda aty: aty['agreement_type'] == rec.agreement_type_id.agreement_type and
                                                   aty['number_agreement'] == rec.project_id.number_agreement,
                                       agreement_type_obj))
                agreement_type = agreement_type[0]['id'] if agreement_type else False
                if agreement_type:
                    rec.agreement_type = agreement_type

    def sql_program_code(self):
        return """
            INSERT INTO program_code(
                budget_id, year, program_id, sub_program_id, dependency_id, sub_dependency_id, item_id,
                check_digit, resource_origin_id, institutional_activity_id, budget_program_conversion_id,
                conversion_item_id, expense_type_id, location_id, portfolio_id, project_type_id, stage_id,
                agreement_type_id, project_id,
                program_code, program_code_copy,
                search_key, state,
                conacyt_code,
                create_uid, write_uid, create_date, write_date)
            VALUES (%s, %s, %s, %s, %s, %s, %s,
                %s, %s, %s, %s,
                %s, %s, %s, %s, %s, %s,
                %s, %s,
                %s, %s,
                %s, %s,
                false,
                %s, %s, now(), now()
            )
            RETURNING id
        """


    def sql_params_program_code(self, program_vals):
        """
            create_sql_program_code crea la instruccion SQL INSERT para crear un
            código programático
        """
        # Creación de la search_key usando la combinación ids, la definición se
        # tomo del modelo program.code
        search_key = ("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s" % (
            program_vals['year'],
            program_vals['program_id'],
            program_vals['sub_program_id'],
            program_vals['dependency_id'],
            program_vals['sub_dependency_id'],
            program_vals['item_id'],
            program_vals['resource_origin_id'],
            program_vals['institutional_activity_id'],
            program_vals['budget_program_conversion_id'],
            program_vals['conversion_item_id'],
            program_vals['expense_type_id'],
            program_vals['location_id'],
            program_vals['portfolio_id'],
            program_vals['project_type_id'],
            program_vals['stage_id'],
            program_vals['agreement_type_id']
        ))
        return [(
            program_vals['budget'],
            program_vals['year'],
            program_vals['program_id'],
            program_vals['sub_program_id'],
            program_vals['dependency_id'],
            program_vals['sub_dependency_id'],
            program_vals['item_id'],
            program_vals['check_digit'],
            program_vals['resource_origin_id'],
            program_vals['institutional_activity_id'],
            program_vals['budget_program_conversion_id'],
            program_vals['conversion_item_id'],
            program_vals['expense_type_id'],
            program_vals['location_id'],
            program_vals['portfolio_id'],
            program_vals['project_type_id'],
            program_vals['stage_id'],
            program_vals['agreement_type_id'],
            program_vals['project_id'],
            program_vals['program_code'],
            program_vals['program_code'],
            search_key,
            program_vals['state'],
            self.env.user.id,
            self.env.user.id,
        )]

    def get_ids_program_code(self,params,query):
        ids=[]
        for p in params:
            self.env.cr.execute(query, p)
            res = (self.env.cr.fetchone() or [None])[0]
            if not res:
                raise ValidationError("Error al insertar el registro program.code")
            ids.append(res)
        return ids

    def insert_sql(self, lines,exercise=False):
        def _get_column_stored_name():
            query = """
                select column_name from information_schema.columns
                where table_schema = 'public'
                and table_name   = 'program_code' and column_name <> 'id'
            """
            self.env.cr.execute(query, ())
            return [f'"{res[0]}"' for res in self.env.cr.fetchall()]

        def _get_insert_sql():
            columns = _get_column_stored_name()
            column = ", ".join(columns)
            values = ", ".join(["%s"]* len(columns))
            return f"insert into program_code ({column}) values({values}) returning id", columns


        def _get_insert_params(columns, vals):
            def update_vals(updates):
                for k, v in updates:
                    if k not in vals: vals.update({k: v})

        

            res = []
            search_key = ("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s" % (
                vals.get('year', ''),
                vals.get('program_id', ''),
                vals.get('sub_program_id', ''),
                vals.get('dependency_id', ''),
                vals.get('sub_dependency_id', ''),
                vals.get('item_id', ''),
                vals.get('resource_origin_id', ''),
                vals.get('institutional_activity_id', ''),
                vals.get('budget_program_conversion_id', ''),
                vals.get('conversion_item_id', ''),
                vals.get('expense_type_id', ''),
                vals.get('location_id', ''),
                vals.get('portfolio_id', ''),
                vals.get('project_type_id', ''),
                vals.get('stage_id', ''),
                vals.get('agreement_type_id', '')
            ))
            # Pone los valores por defecto a los campos default o calculados de odoo
            updates = [
                ('program_code_copy', vals.get('program_code', '')),
                ('search_key', search_key),
                ('conacyt_code', False),
                ('create_uid', self.env.user.id),
                ('write_uid', self.env.user.id),
                ('create_date', 'now()'),
                ('write_date', 'now()'),
            ]
            update_vals(updates)
            for column in columns:
                res.append(vals.get(column[1:-1], None))
            return tuple(res)

        query, columns = _get_insert_sql()
        # Creación de los códigos programáticos
        params = []
        for line in lines:
            params.append(_get_insert_params(columns, line))
        ids = self.get_ids_program_code(params,query)
        
        if not exercise:
            i = 0
            budget_lines = []
            for line in lines:
                budget_lines.append({
                    'expenditure_budget_id': line.get('budget_id'),
                    'budget_id': line.get('budget_id'),
                    'program_code_id': ids[i],
                    'state': 'success',
                    'year': line.get('program_code')[0:4],
                    'program': line.get('program_code')[4:6],
                    'subprogram': line.get('program_code')[6:8],
                    'dependency': line.get('program_code')[8:11],
                    'subdependency': line.get('program_code')[11:13],
                    'item': line.get('program_code')[13:16],
                    'dv': line.get('program_code')[16:18],
                    'origin_resource': line.get('program_code')[18:20],
                    'ai': line.get('program_code')[20:25],
                    'conversion_program': line.get('program_code')[25:29],
                    'departure_conversion': line.get('program_code')[29:34],
                    'expense_type': line.get('program_code')[34:36],
                    'location': line.get('program_code')[36:38],
                    'portfolio': line.get('program_code')[38:42],
                    'project_type': line.get('program_code')[42:44],
                    'project_number': line.get('program_code')[44:50],
                    'stage': line.get('program_code')[50:52],
                    'agreement_type': line.get('program_code')[52:54],
                    'agreement_number': line.get('program_code')[54:60],
                })
            # Creación de las líneas de presupuesto
            self.env[EXPENDITURE_BUDGET_LINE].insert_sql(budget_lines)
            # Creación de las lineas de las trimestralidades
            self.env['control.assigned.amounts.lines'].insert_sql(budget_lines)
        return ids

    

###############################################################################
# Creación del código programático usando criterios del convertidor

    def get_domain_dependency(self):
        domain = []
        if self.env.context.get('filter_dependency', None) == 'payments':
            dep = self.env.user.dependency_payment_ids.ids
            dep += [d.id for d in self.env.user.sub_dependency_payment_ids.dependency_id]
            domain.append(('id', 'in', dep))
        elif self.env.context.get('filter_dependency', None) == 'account_to_pay':
            dep = self.env.user.dependency_ids.ids
            dep += [d.id for d in self.env.user.sub_dependency_ids.dependency_id]
            domain.append(('id', 'in', dep))
        return json.dumps(domain)

    tmp_program_code = fields.Char(string='Program Code', compute='_compute_tmp_program_code', store=False)
    tmp_year_id = fields.Many2one(YEAR_CONFIGURATION, string='Year', store=False)
    tmp_dependency_id = fields.Many2one('dependency', string='Dependency', domain=get_domain_dependency, store=False)
    tmp_sub_dependency_id = fields.Many2one(SUB_DEPENDENCY, string='Sub dependency', store=False)
    tmp_domain_sub_dependency = fields.Char(compute="_compute_tmp_domain_sub_dependency", store=False)
    tmp_program_id = fields.Many2one('program', string='Program', store=False)
    tmp_sub_program_id = fields.Many2one(SUB_PROGRAM, string='Sub Program', store=False)
    tmp_item_id = fields.Many2one(EXPENDITURE_ITEM, string='Item', store=False)
    tmp_item_allowed = fields.Char(string='Domain Item', store=False)
    tmp_domain_item = fields.Char(string='Domain Item', compute='_compute_tmp_domain_item', store=False)
    tmp_resource_origin_id = fields.Many2one(RESOURCE_ORIGIN, string='Resource Origin', store=False)
    tmp_resource_origin_allowed = fields.Char(string='Domain Resource Origin', store=False)
    tmp_domain_resource_origin = fields.Char(string='Domain Resource Origin', compute='_compute_tmp_domain_resource_origin', store=False)
    tmp_check_digit = fields.Char(string='Check Digit', size=2, compute='_compute_tmp_check_digit', store=False)
    tmp_institutional_activity_id = fields.Many2one(INSTITUTIONAL_ACTIVITY, string='Institutional Activity Number', compute='_compute_tmp_institutional_activity_id', store=False)
    tmp_conpp_id = fields.Many2one(BUDGET_PROGRAM_CONVERSION, string='CONPP', compute='_compute_tmp_conpp_id', store=False)
    tmp_conpa_id = fields.Many2one(DEPARTURE_CONVERSION, string='CONPA', compute='_compute_tmp_conpa_id', store=False)
    tmp_expense_type_id = fields.Many2one(EXPENSE_TYPE, string='Expense Type', compute='_compute_tmp_conpa_id', store=False)
    tmp_location_id = fields.Many2one('geographic.location', string='Geographic location', compute='_compute_tmp_location_id', store=False)
    tmp_wallet_key_id = fields.Many2one(KEY_WALLET, string='Key Wallet', store=False)
    tmp_project_type_id = fields.Many2one('project.type.custom', string='Project Type', store=False)
    tmp_project_stage_id = fields.Many2one('project.custom.stage', string='Project Stage', store=False)
    tmp_project_id = fields.Many2one(PROJECT_PROJECT, string='Project', store=False)
    tmp_agreement_type_id = fields.Many2one(AGREEMENT_TYPE, string='Agreement Type', compute='_compute_tmp_agreement', store=False)
    tmp_number_agreement = fields.Char(string='Agreement Number', size=6, store=False)

    @api.depends('tmp_dependency_id')
    def _compute_tmp_domain_sub_dependency(self):
        for record in self:
            domain = []
            if record.tmp_dependency_id:
                if self.env.context.get('filter_dependency', None) == 'payments':
                    domain = [('dependency_id', '=', record.tmp_dependency_id.id)]
                    if record.tmp_dependency_id.id not in self.env.user.dependency_payment_ids.ids:
                        domain = [('id', 'in', self.env.user.sub_dependency_payment_ids.ids)]
                elif self.env.context.get('filter_dependency', None) == 'account_to_pay':
                    domain = [('dependency_id', '=', record.tmp_dependency_id.id)]
                    if record.tmp_dependency_id.id not in self.env.user.dependency_ids.ids:
                        domain = [('id', 'in', self.env.user.sub_dependency_ids.ids)]
                else:
                    domain = [('dependency_id', '=', record.tmp_dependency_id.id)]
            else:
                domain.append(('id', '=', False))
            record.tmp_domain_sub_dependency = json.dumps(domain)

    @api.depends('tmp_item_allowed')
    def _compute_tmp_domain_item(self):
        for record in self:
            domain = []
            if record.tmp_item_allowed:
                domain.append(('item', 'in', record.tmp_item_allowed.split(',')))
            record.tmp_domain_item = json.dumps(domain)

    @api.depends('tmp_program_id', 'tmp_sub_program_id', 'tmp_dependency_id', 'tmp_sub_dependency_id', 'tmp_item_id')
    def _compute_tmp_check_digit(self):
        for record in self:
            record.tmp_check_digit = self.env['verifying.digit'].check_digit_from_codes(
                record.tmp_program_id,
                record.tmp_sub_program_id,
                record.tmp_dependency_id,
                record.tmp_sub_dependency_id,
                record.tmp_item_id
            )

    @api.depends('tmp_resource_origin_allowed')
    def _compute_tmp_domain_resource_origin(self):
        for record in self:
            domain = []
            if record.tmp_resource_origin_allowed:
                domain.append(('key_origin', 'in', record.tmp_resource_origin_allowed.split(',')))
            record.tmp_domain_resource_origin = json.dumps(domain)

    @api.depends('tmp_program_id')
    def _compute_tmp_institutional_activity_id(self):
        for record in self:
            if record.tmp_program_id:
                conversion_activity = self.env['conversion.institutional.activity'].search([
                    ('program_id', '=', self.tmp_program_id.id)
                ])
                if not conversion_activity:
                    raise ValidationError(
                        "No se encontró una actividad Institucional asociada con el programa %s" %
                        self.tmp_program_id.program
                    )
                record.tmp_institutional_activity_id = conversion_activity.institutional_activity_id
            else:
                record.tmp_institutional_activity_id = None


    @api.depends('tmp_item_id')
    def _compute_tmp_conpa_id(self):
        for record in self:
            if record.tmp_item_id:
                conpa = self.env[DEPARTURE_CONVERSION].search([
                    ('item_id', '=', record.tmp_item_id.id),
                    ('start_date', '<=', datetime.today()),
                    '|', ('end_date', '=', None), ('end_date', '>=', datetime.today())
                ])
                if not conpa:
                    raise ValidationError(
                        "No se pudo encontrar un CONPA asociado al programa: %s" %
                        record.tmp_item_id.item
                    )
                if len(conpa) > 1:
                    raise ValidationError(
                        "Se encontró más de un posible CONPA (%s) asociado al programa: %s" % (
                            ", ".join([c.conversion_key_id.conversion_key for c in conpa]),
                            record.tmp_item_id.item
                        )
                    )
                record.tmp_conpa_id = conpa.id
                expense_type_id, __ = self.env['conversion.expense.type'].get_expense_type(record.tmp_item_id.item)
                if not expense_type_id:
                    raise ValidationError(
                        "No se pudo encontrar un TG asociado al programa: (%s)" %
                        record.tmp_item_id.item
                    )
                record.tmp_expense_type_id = self.env[EXPENSE_TYPE].browse([expense_type_id])
            else:
                record.tmp_conpa_id = None
                record.tmp_expense_type_id = None

    @api.depends('tmp_sub_program_id')
    def _compute_tmp_location_id(self):
        for record in self:
            if record.tmp_sub_program_id:
                record.tmp_location_id = record.tmp_sub_program_id.geographic_location_id
            else:
                record.tmp_location_id = None

    @api.depends('tmp_project_id')
    def _compute_tmp_agreement(self):
        for record in self:
            if record.tmp_project_id:
                agreement = self.env[AGREEMENT_TYPE].search([
                    ('project_id', '=', record.tmp_project_id.id),
                    ('agreement_type', '=', record.tmp_project_id.agreement_type),
                    ('number_agreement', '=', record.tmp_project_id.number_agreement),
                ])
                if not agreement:
                    raise ValidationError("No se encontro un tipo de convenio para el pryecto %s" % record.tmp_project_id.number)
                record.tmp_agreement_type_id = agreement
                record.tmp_number_agreement = record.tmp_project_id.number_agreement
            else:
                record.tmp_agreement_type_id = None
                record.tmp_number_agreement = None

    @api.depends(
        'tmp_dependency_id',
        'tmp_sub_dependency_id',
        'tmp_program_id',
        'tmp_sub_program_id',
        'tmp_item_id',
        'tmp_conpa_id',
    )
    def _compute_tmp_conpp_id(self):
        for record in self:
            if record.tmp_program_id and record.tmp_sub_program_id and record.tmp_dependency_id and record.tmp_sub_dependency_id and record.tmp_item_id and record.tmp_conpa_id:
                conpp_id, conpp_str = self.env['conversion.conpp'].get_conpp(
                    record.tmp_program_id.key_unam,
                    record.tmp_sub_program_id.sub_program,
                    record.tmp_dependency_id.dependency,
                    record.tmp_sub_dependency_id.sub_dependency,
                    record.tmp_item_id.item
                )
                if not conpp_id:
                    raise ValidationError("No se pudo encontrar el CONPP: (%s)" % (record.tmp_item_id.item))
                program_key_id = self.env['program.key'].search([('name', '=', record.tmp_program_id.key_unam[0])])
                conpp = self.env[BUDGET_PROGRAM_CONVERSION].sudo().search([
                    ('program_key_id', '=', program_key_id.id),
                    ('conversion_key_id', '=', record.tmp_conpa_id.conversion_key_id.id),
                    ('shcp', '=', conpp_id),
                    ('start_date', '<=', datetime.today()),
                    '|', ('end_date', '=', None), ('end_date', '>=', datetime.today())
                ])
                if not conpp:
                    raise ValidationError("El CONPP (%s) no se encontró en el cátalogo Conversión de Programa Presupuestario o sus vigencias son ínvalidas" % (conpp_str))
                record.tmp_conpp_id = conpp
            else:
                record.tmp_conpp_id = None

    @api.depends(
        'tmp_year_id',
        'tmp_dependency_id',
        'tmp_sub_dependency_id',
        'tmp_program_id',
        'tmp_sub_program_id',
        'tmp_item_id',
        'tmp_resource_origin_id',
        'tmp_check_digit',
        'tmp_institutional_activity_id',
        'tmp_conpp_id',
        'tmp_conpa_id',
        'tmp_expense_type_id',
        'tmp_location_id',
        'tmp_wallet_key_id',
        'tmp_project_type_id',
        'tmp_project_id',
        'tmp_project_stage_id',
        'tmp_agreement_type_id',
        'tmp_number_agreement',
    )
    def _compute_tmp_program_code(self):
        for record in self:
            record.tmp_program_code = "%s"*19 % (
                record.tmp_year_id.name or '',
                record.tmp_program_id.key_unam or '',
                record.tmp_sub_program_id.sub_program or '',
                record.tmp_dependency_id.dependency or '',
                record.tmp_sub_dependency_id.sub_dependency or '',
                record.tmp_item_id.item or '',
                record.tmp_check_digit,
                record.tmp_resource_origin_id.key_origin or '',
                record.tmp_institutional_activity_id.number or '',
                record.tmp_conpp_id.shcp.name or '',
                record.tmp_conpa_id.conversion_key_id.conversion_key or '',
                record.tmp_expense_type_id.key_expenditure_type or '',
                record.tmp_location_id.state_key or '',
                record.tmp_wallet_key_id.wallet_password or '',
                record.tmp_project_type_id.name or '',
                record.tmp_project_id.number or '',
                record.tmp_project_stage_id.name or '',
                record.tmp_agreement_type_id.agreement_type or '',
                record.tmp_number_agreement or '',
            )

    @api.model
    def default_get(self, fields):
        res = super().default_get(fields)
        # Año
        year_str = str(datetime.today().year)
        year = self.env[YEAR_CONFIGURATION].sudo().search([('name', '=', year_str)], limit=1)
        if not year:
            year = self.env[YEAR_CONFIGURATION].sudo().create({'name': year_str})
        res['tmp_year_id'] = year.id
        # Clave de cartera
        wallet_key = self.env[KEY_WALLET].sudo().search([('wallet_password', '=', '0000')])
        if wallet_key:
            res['tmp_wallet_key_id'] = wallet_key.id
        # Etapa
        stage = self.env['project.custom.stage'].search([('name', '=', '00')])
        if stage:
            res['tmp_project_stage_id'] = stage.id
        # Tipo de Proyecto
        type_project = self.env['project.type.custom'].search([('name', '=', '00')])
        if type_project:
            res['tmp_project_type_id'] = type_project.id
        # Proyecto
        if type_project and stage:
            project = self.env[PROJECT_PROJECT].search([
                ('number', '=', '000000'),
                ('custom_project_type_id', '=', type_project.id),
                ('custom_stage_id', '=', stage.id),
            ])
            if type_project:
                res['tmp_project_id'] = project.id
        return res

    @api.model
    def create(self, vals):
        if not self.env.context.get('create_program_code'):
            return super().create(vals)
        vals = self._get_params_for_insert_sql(vals)
        ids = self.insert_sql([vals])
        program_code_ids = self.browse(ids)
        if program_code_ids:
            # Para los códigos programáticos asociados con proyectos, se debe
            # ligar el código programatico a las lineas de los proyectos PAPIIT
            project_type = vals.get('program_code')[42:44]
            project_number = vals.get('program_code')[44:50]
            custom_stage = vals.get('program_code')[50:52]
            if project_type != "00" and project_number != "000000" and custom_stage != "00":
                program_code_ids[0].project_id.project_programcode_ids = [(0, 0, {
                    'program_code_id': program_code_ids[0].id
                })]
        return program_code_ids

    def _get_params_for_insert_sql(self, vals):
        year = vals['tmp_program_code'][0:4]
        query = """
            select id from expenditure_budget
            where extract(year from from_date) = %s and extract(year from to_date) = %s
        """
        self.env.cr.execute(query, (year, year))
        budget_id = (self.env.cr.fetchone() or [None])[0]
        if not budget_id:
            raise ValidationError("No se encontró un presupuesto asociado al año %s" % year)
        project_type = self.env[PROJECT_TYPE].search([('project_id', '=', vals['tmp_project_id'])])
        stage = self.env['stage'].search([('project_id', '=', vals['tmp_project_id'])])
        return {
            'budget_id': budget_id,
            'year': vals['tmp_year_id'],
            'program_id': vals['tmp_program_id'],
            'sub_program_id': vals['tmp_sub_program_id'],
            'dependency_id': vals['tmp_dependency_id'],
            'sub_dependency_id': vals['tmp_sub_dependency_id'],
            'item_id': vals['tmp_item_id'],
            'check_digit': vals['tmp_check_digit'],
            'resource_origin_id': vals['tmp_resource_origin_id'],
            'institutional_activity_id': vals['tmp_institutional_activity_id'],
            'budget_program_conversion_id': vals['tmp_conpp_id'],
            'conversion_item_id': vals['tmp_conpa_id'],
            'expense_type_id': vals['tmp_expense_type_id'],
            'location_id': vals['tmp_location_id'],
            'portfolio_id': vals['tmp_wallet_key_id'],
            'project_type_id': project_type and project_type.id or None,
            'stage_id': stage and stage.id or None,
            'agreement_type_id': vals['tmp_agreement_type_id'],
            'program_code': vals['tmp_program_code'],
            'state': 'validated',
            'project_id': vals['tmp_project_id'],
        }

    def validate_program_code_structure(self, program_code, date=datetime.today()):
        resource_origin = program_code[19:20]
        short_program_code = "%s"*7 % (
            program_code[4:6],
            program_code[6:8],
            program_code[8:11],
            program_code[11:13],
            program_code[13:16],
            # program_code[19:20],
            '0',
            program_code[17:18],
        )
        program_code_conv = self.convert_program_code(short_program_code, date.strftime("%Y-%m-%d"))
        program_code_conv = program_code_conv[0:19] + resource_origin + program_code_conv[20:]
        return (program_code_conv[0:38] == program_code[0:38], program_code_conv)

    def get_vals_program_code(self, program_code, date=datetime.today(),exercise=False):
        # Obtención del id del presupuesto
        year = program_code[0:4]
        query = """
            select id from expenditure_budget
            where extract(year from from_date) = %s and extract(year from to_date) = %s
        """
        self.env.cr.execute(query, (year, year))
        budget_id = (self.env.cr.fetchone() or [None])[0]
        if not exercise and not budget_id:
            return (False, "No se encontró un presupuesto asociado al año %s" % year)
        # Obtención del id del año
        year_id = self.env[YEAR_CONFIGURATION].search([('name', '=', year)])
        if not year_id:
            return (False, "No se encontró el registro para el año (%s)" % year)
        # Obtención del id de la dependencia
        dependency = program_code[8:11]
        dependency_id = self.env['dependency'].search([
            ('dependency', '=', dependency), 
            ('start_date', '<=', date),
            '|', ('end_date', '=', None), ('end_date', '>=', date)])
        if not dependency_id:
            return (False, "No se encontró el registro para la dependencia (%s)" % dependency)
        # Obtención del id de la subdependencia
        sub_dependency = program_code[11:13]
        sub_dependency_id = self.env[SUB_DEPENDENCY].search([
            ('dependency_id', '=', dependency_id.id),
            ('sub_dependency', '=', sub_dependency),
            ('start_date', '<=', date),
            '|', ('end_date', '=', None), ('end_date', '>=', date)
        ])
        if not sub_dependency_id:
            return (False, "No se encontró el registro para la subdependencia (%s)" % sub_dependency)
        # Obtención del id del programa
        program = program_code[4:6]
        program_id = self.env['program'].search([
            ('key_unam', '=', program),
            ('start_date', '<=', date),
            '|', ('end_date', '=', None), ('end_date', '>=', date)
            ])
        if not program_id:
            return (False, "No se encontró el registro para el programa (%s)" % program)
        # Obtención del id del subprograma
        sub_program = program_code[6:8]
        sub_program_id = self.env[SUB_PROGRAM].search([
            ('dependency_id', '=', dependency_id.id),
            ('sub_dependency_id', '=', sub_dependency_id.id),
            ('unam_key_id', '=', program_id.id),
            ('sub_program', '=', sub_program),
            ('start_date', '<=', date),
            '|', ('end_date', '=', None), ('end_date', '>=', date)
        ])
        if not sub_program_id:
            return (False, "No se encontró el registro para el subprograma (%s)" % sub_program)
        # Obtención del id de la partida
        item = program_code[13:16]
        item_id = self.env[EXPENDITURE_ITEM].search([
            ('item', '=', item),
            ('start_date', '<=', date),
            '|', ('end_date', '=', None), ('end_date', '>=', date)])
        if not item_id:
            return (False, "No se encontró el registro para la partida (%s)" % item)
        # Obtención del id del origen del recurso
        resource_origin = program_code[18:20]
        resource_origin_id = self.env[RESOURCE_ORIGIN].search([
            ('key_origin', '=', resource_origin),
            ('start_date', '<=', date),
            '|', ('end_date', '=', None), ('end_date', '>=', date)
            ])
        if not resource_origin_id:
            return (False, "No se encontró el origen del recurso (%s)" % resource_origin)
        # Obtención del id de la actividad institicional
        institutional_activity = program_code[20:25]
        institutional_activity_id = self.env[INSTITUTIONAL_ACTIVITY].search([
            ('number', '=', institutional_activity),
            ('start_date', '<=', date),
            '|', ('end_date', '=', None), ('end_date', '>=', date)
            ])
        if not institutional_activity_id:
            return (False, "No se encontró el registro para la actividad institucional (%s)" % institutional_activity)
        # Obtención del id del CONPA
        conpa = program_code[29:34]
        conpa_id = self.env[DEPARTURE_CONVERSION].search([
            ('item_id', '=', item_id.id),
            ('start_date', '<=', date),
            '|', ('end_date', '=', None), ('end_date', '>=', date)
        ])
        if not conpa_id:
            return (False, "No se encontró el registro para el CONPA (%s)" % conpa)
        # Obtención del id del CONPP
        program_key_id = self.env['program.key'].search([
            ('name', '=', program[0]),
            ('start_date', '<=', date),
            '|', ('end_date', '=', None), ('end_date', '>=', date)
            ])
        conpp = program_code[25:29]
        conpp_id = self.env[BUDGET_PROGRAM_CONVERSION].sudo().search([
            ('program_key_id', '=', program_key_id.id),
            ('conversion_key_id', '=', conpa_id.conversion_key_id.id),
            ('shcp.name', '=', conpp),
            ('start_date', '<=', date),
            '|', ('end_date', '=', None), ('end_date', '>=', date)
        ])
        if not conpp_id:
            return (False, "No se encontró el registro el conpp (%s)" % conpp)
        # Obtención del id del tipo de gasto
        expense_type = program_code[34:36]
        expense_type_id = self.env[EXPENSE_TYPE].search([
            ('key_expenditure_type', '=', expense_type),
            ('start_date', '<=', date),
            '|', ('end_date', '=', None), ('end_date', '>=', date)
            ])
        if not expense_type_id:
            return (False, "No se encontró el registro para el tipo de gasto (%s)" % expense_type)
        # Obtención del id de la ubicación geografica
        location_id = sub_program_id.geographic_location_id
        # Obtención del id de la clave de cartera
        wallet_password = program_code[38:42]
        wallet_password_id = self.env[KEY_WALLET].search([
            ('wallet_password', '=', wallet_password),
            ('start_date', '<=', date),
            '|', ('end_date', '=', None), ('end_date', '>=', date)
            ])
        if not wallet_password_id:
            return (False, "No se encontró el registro para la clave de cartera (%s)" % wallet_password)
        # Obtención de los ids de los proyectos
        project_type = program_code[42:44]
        project_number = program_code[44:50]
        custom_stage = program_code[50:52]
        project_id = self.env[PROJECT_PROJECT].search([
            ('number', '=', project_number),
            ('custom_project_type_id.name', '=', project_type),
            ('custom_stage_id.name', '=', custom_stage),
        ])
        if not project_id:
            return (False, "No se encontró el registro para el proyecto: %s, con tipo de proyecto: %s y etapa: %s" % (project_number, project_type, custom_stage))
        project_type_id = self.env[PROJECT_TYPE].search([('project_id', '=', project_id.id)])
        if not project_type_id:
            return (False, "No se encontró el registro del tipo de proyecto (%s)" % project_type)
        stage_id = self.env['stage'].search([('project_id', '=', project_id.id)])
        if not stage_id:
            return (False, "No se encontró el registro de la etapa (%s)" % custom_stage)
        agreement_id = self.env[AGREEMENT_TYPE].search([
            ('project_id', '=', project_id.id),
            ('agreement_type', '=', project_id.agreement_type),
            ('number_agreement', '=', project_id.number_agreement),
        ])
        if not agreement_id:
            return(False, "No se encontró el tipo de acuerdo (%s)" % project_id.agreement_type)
        return (True, {
            'budget_id': budget_id,
            'year': year_id.id,
            'program_id': program_id.id,
            'sub_program_id': sub_program_id.id,
            'dependency_id': dependency_id.id,
            'sub_dependency_id': sub_dependency_id.id,
            'item_id': item_id.id,
            'check_digit': program_code[16:18],
            'resource_origin_id': resource_origin_id.id,
            'institutional_activity_id': institutional_activity_id.id,
            'budget_program_conversion_id': conpp_id.id,
            'conversion_item_id': conpa_id.id,
            'expense_type_id': expense_type_id.id,
            'location_id': location_id.id,
            'portfolio_id': wallet_password_id.id,
            'project_type_id': project_type_id.id,
            'stage_id': stage_id.id,
            'project_id': project_id.id,
            'agreement_type_id': agreement_id.id,
            'program_code': program_code,
            'state': 'validated',
        })

    @api.model
    def get_or_create_program_code_id(self, program_code, date=None,exercise=False):
        if not date:
            date = datetime.now()
        else:
            date = datetime.strptime(date, "%Y-%m-%d")
        # Se busca si el código programático existe
        program_code_id = self.env[PROGRAM_CODE].search([
            ('program_code', '=', program_code),
        ])
        
        # Si existe, se devuelve el id
        if program_code_id:
            return (program_code_id.id, False)
        if not exercise:
            # Sí no existe, se válida que sea un código programático válido usando
            # el convertidor
            ok, program_code = self.validate_program_code_structure(program_code, date)
            # Sí el código es distinto al generado por el convertidor, se devuelve
            # un id invalido, el mensaje de error, y el código correcto.
            if not ok:
                return (-1, (
                    "La estructura del código programático no coincide con las reglas de presupuesto",
                    program_code
                ))
        # Sí el código cumple con las reglas de presupuesto
        ok, vals = self.get_vals_program_code(program_code, date,exercise)
        if not ok:
            return(-1, ("Error al obtener los ids del código programático", vals))
        ids = self.insert_sql([vals],exercise)
        return (ids[0], False)

    """
    Se crea este método para solucionar el problema códigos programáticos que
    quedaron sin lineas de presupuesto y estacionalidad.
    """
    @api.model
    def create_budget_lines(self, program_code):
        program_code_id = self.env[PROGRAM_CODE].search([('program_code', '=', program_code)])
        vals = {}
        vals['program_code'] = program_code
        vals.update({
            'expenditure_budget_id': program_code_id.budget_id.id,
            'budget_id': program_code_id.budget_id.id,
            'program_code_id': program_code_id.id,
            'state': 'success',
            'year': vals.get('program_code')[0:4],
            'program': vals.get('program_code')[4:6],
            'subprogram': vals.get('program_code')[6:8],
            'dependency': vals.get('program_code')[8:11],
            'subdependency': vals.get('program_code')[11:13],
            'item': vals.get('program_code')[13:16],
            'dv': vals.get('program_code')[16:18],
            'origin_resource': vals.get('program_code')[18:20],
            'ai': vals.get('program_code')[20:25],
            'conversion_program': vals.get('program_code')[25:29],
            'departure_conversion': vals.get('program_code')[29:34],
            'expense_type': vals.get('program_code')[34:36],
            'location': vals.get('program_code')[36:38],
            'portfolio': vals.get('program_code')[38:42],
            'project_type': vals.get('program_code')[42:44],
            'project_number': vals.get('program_code')[44:50],
            'stage': vals.get('program_code')[50:52],
            'agreement_type': vals.get('program_code')[52:54],
            'agreement_number': vals.get('program_code')[54:60],
        })
        # Creación de las líneas de presupuesto
        self.env[EXPENDITURE_BUDGET_LINE].insert_sql([vals])
        # Creación de las lineas de las trimestralidades
        self.env['control.assigned.amounts.lines'].insert_sql([vals])
        return 0
