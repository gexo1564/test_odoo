from odoo import api, fields, models, _


class AccountAccount(models.Model):
    _name = 'account.account'
    _inherit = ['account.account', 'excel.detailed.information']

    annex_ids = fields.Many2many('account.financial.html.report.line')

    def _compute_excel_display_name(self):
        for record in self:
            record.excel_display_name = '%s - %s' % (record.code, record.name)
