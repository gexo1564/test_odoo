from odoo import models, fields, api,_

class AccountMoveLine(models.Model):

    _inherit = 'account.move.line'

    dependency_id = fields.Many2one('dependency','Dependency')
    sub_dependency_id = fields.Many2one('sub.dependency', 'Sub Dependency')
    agreement_number = fields.Char("Agreement Number")
    agreement_related_number = fields.Char(related='request_id.agreement_number', string="Agreement Number Related", store=True)
    origin_resource_related = fields.Char(related='program_code_id.resource_origin_id.key_origin', string="Origin Resource", store=True)
    item_related = fields.Many2one(related='program_code_id.item_id', string="Item", store=True)
    exercise_related = fields.Char(related='move_id.exercise', string="Exercise", store=True)
    uuid_related = fields.Char(string="UUID")
    folio_related = fields.Char(string="Folio CFDI")
    serie_related = fields.Char(string="Serie CFDI")
    bank_related = fields.Many2one(related='move_id.income_bank_journal_id', string="Banco", store=True)
    cta_ban_related = fields.Many2one(related='bank_related.bank_account_id', string="Cuenta bancaria", store=True)
    dep_sub_flag = fields.Boolean(related='account_id.dep_subdep_flag', string="Variable Dep/SubDep")

    @api.model_create_multi
    def create(self,vals):
        line = super(AccountMoveLine, self).create(vals)
        for res in line:
            if res.dependency_id and res.sub_dependency_id:
                if res.sub_dependency_id.dependency_id and res.sub_dependency_id.dependency_id.id != res.dependency_id.id:
                    res.sub_dependency_id = self.env['sub.dependency'].get_sub_dep_records(res.sub_dependency_id.sub_dependency,res.dependency_id.id)
        return line
