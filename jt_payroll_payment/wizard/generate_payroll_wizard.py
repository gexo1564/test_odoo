# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields, api, _
import base64
import xlrd
from datetime import datetime, timedelta
from odoo.modules.module import get_resource_path
from xlrd import open_workbook
from odoo.exceptions import UserError, ValidationError
from odoo.tools.misc import ustr
import io
import openpyxl
import sqlite3
import tempfile
from datetime import datetime
import copy
import json


REBATE_PROGRAM_CODE_PATTERN = '________0000099900000000000000000000000000000000000000000000'
IMPORT_TEXT=" Import\n"
class GeneratePayrollWizard(models.TransientModel):

    _name = 'generate.payroll.wizard'
    _description = "Generate Payroll Wizard"

    type_of_movement = fields.Selection([('payroll_perceptions','Payroll Perceptions'),
                                         ('payroll_deductions','Payroll Deductions'),
                                         ('pension_payments','Pension Payments'),
                                         ('additional_payments','Additional Payments'),
                                         ('additional_pension_payments','Additional Pension Payments')
                                         ],string='Type of Movement')
    
    file = fields.Binary('File to import')
    filename = fields.Char('FileName')
    
    employee_ids = fields.Many2many('hr.employee','employee_generate_payroll_wizard_rel','employee_id','wizard_id','Employees')
    payroll_process_id = fields.Many2one('custom.payroll.processing','Payroll Process')
    file_row = fields.Integer(default=0)
    cron_id = fields.Many2one('ir.cron',"Active Cron")
    
    def check_employee_data(self,failed_row,rfc,import_type,counter):
        failed_row += "Row " +str(counter)+" : " + str(rfc) + "------>> Invalid Employee RFC In "+str(import_type)+IMPORT_TEXT
        return failed_row
    
    def check_category_data(self,failed_row,cat,import_type,counter):
        failed_row +="Row " +str(counter)+" : " + str(cat) + "------>> Invalid Category Key In "+str(import_type)+IMPORT_TEXT
        return failed_row

    def check_program_code_data(self,failed_row,program,import_type,counter):
        failed_row +="Row " +str(counter)+" : " + str(program) + "------>> Invalid Program Code In "+str(import_type)+IMPORT_TEXT
        return failed_row

    def check_payment_method_data(self,failed_row,method,import_type,counter):
        failed_row +="Row " +str(counter)+" : " + str(method) + "------>> Invalid Payment Method In "+str(import_type)+IMPORT_TEXT
        return failed_row

    def check_bank_key_data(self,failed_row,key,import_type,counter):
        failed_row +="Row " +str(counter)+" : " + str(key) + "------>> Invalid Bank Key In "+str(import_type)+IMPORT_TEXT
        return failed_row

    def check_checklog_data(self,failed_row,number,import_type,counter):
        failed_row +="Row " +str(counter)+" : " + str(number) + "------>> Invalid Check Number In "+str(import_type)+IMPORT_TEXT
        return failed_row

    def check_bank_accunt_data(self,failed_row,acount,import_type,counter):
        failed_row +="Row " +str(counter)+" : " + str(acount) + "------>> Invalid Bank Account In "+str(import_type)+IMPORT_TEXT
        return failed_row

    def check_perception_data(self,failed_row,perception,import_type,counter):
        failed_row +="Row " +str(counter)+" : " + str(perception) + "------>> Invalid Payment key In "+str(import_type)+IMPORT_TEXT
        return failed_row

    def check_deduction_data(self,failed_row,deduction,import_type,counter):
        failed_row +="Row " +str(counter)+" : " + str(deduction) + "------>> Invalid Deduction key In "+str(import_type)+IMPORT_TEXT
        return failed_row
    def check_parter_data(self,failed_row,partner,import_type,counter):
        failed_row +="Row " +str(counter)+" : " + str(partner) + "------>> Invalid Beneficiaries In "+str(import_type)+IMPORT_TEXT
        return failed_row
    
                     
    def generate(self,record_id=False):
        if record_id:
            self = self.env['generate.payroll.wizard'].browse(int(record_id))
        
        if self.file:
            failed_row = ""

            result_dict = {
                }
            line_data = []
            exit_payroll_id = False
            #======================== payroll_perceptions ================# 
            if self.type_of_movement == 'payroll_perceptions':
                self.payroll_process_id.perception_file = self.file
                self.payroll_process_id.perception_filename = self.filename
                self.payroll_process_id.perception_file_index = 1
                self.payroll_process_id.perception_file_load = True
                self.payroll_process_id.perception_hide = False

                current_query = f'SELECT count(fpe.id) as total FROM employee_payroll_file fpe WHERE fpe.payroll_processing_id = {self.payroll_process_id.id}'
                self.env.cr.execute(current_query)
                postgres_employee_payroll_file_total = self.env.cr.fetchall()
                postgres_employee_payroll_file_total = int(postgres_employee_payroll_file_total[0][0])

                if postgres_employee_payroll_file_total == 0:
                    perception_header = ['rfc', 'category', 'program_code', 'payment_place', 'medium', 'bank_key', 'check_number', 'deposit_number', 'deposit_account_number', 'perception_key', 'amount']
                    defined_rules_for_payment_method = ['Transferencia electrónica de fondos', 'Cheque nominativo']
                    file_path_db = tempfile.mkdtemp()
                    fortnight = self.payroll_process_id.fornight
                    year = self.payroll_process_id.fornight
                    perception_table_name = f'perception_{fortnight}{year}'

                    db_payroll_path = f"{file_path_db}/payroll_{fortnight}{year}.db"
                    con = sqlite3.connect(db_payroll_path)
                    cur = con.cursor()



                    current_query = f"DROP TABLE IF EXISTS {perception_table_name};"
                    cur.execute(current_query)
                    perception_header_table_creation = [element + ' TEXT' for element in perception_header]
                    perception_header_table_creation = ','.join(perception_header_table_creation)
                    current_query = f'CREATE TABLE {perception_table_name}(id INTEGER PRIMARY KEY,{perception_header_table_creation});'
                    cur.execute(current_query)


                    # To open the workbook
                    # Workbook object is created
                    wb_obj = openpyxl.load_workbook(filename=io.BytesIO(base64.b64decode(self.file)), read_only=True, data_only=True, keep_links=False)

                    # Get workbook active sheet object
                    # from the active attribute
                    sheet_obj = wb_obj.active

                    max_row = sheet_obj.max_row
                    max_column = sheet_obj.max_column
                    if max_column == len(perception_header):
                        data = []
                        rfc_tmp = ''
                        counter = 1
                        for i in sheet_obj.iter_rows(values_only=True, min_row=2):
                            rfc = str(i[0]).strip() if i[0] is not None else ''
                            category = str(i[1]).strip() if i[1] is not None else ''
                            program_code = str(i[2]).strip() if i[2] is not None else ''
                            payment_place = str(i[3]).strip() if i[3] is not None else ''
                            medium = str(i[4]).strip() if i[4] is not None else ''
                            bank_key = str(i[5]).strip() if i[5] is not None else ''
                            check_number = str(i[6]).strip() if i[6] is not None else ''
                            deposit_number = str(i[7]).strip() if i[7] is not None else ''
                            deposit_account_number = str(i[8]).strip() if i[8] is not None else ''
                            perception_key = str(i[9]).strip() if i[9] is not None else ''
                            amount = str(i[10]).strip() if i[10] is not None else ''
                            if rfc != '':
                                rfc_tmp = rfc
                            current_query = f"INSERT INTO {perception_table_name} VALUES({counter}, \'{rfc_tmp}\', \'{category}\', \'{program_code}\', \'{payment_place}\', \'{medium}\', \'{bank_key}\', \'{check_number}\', \'{deposit_number}\', \'{deposit_account_number}\', \'{perception_key}\', \'{amount}\');"
                            counter = counter + 1
                            cur.execute(current_query)
                    
                    
                        con.commit()

                        data_not_found_dict = {
                            'employee' : {
                                'transfer': {},
                                'check' : {},
                            },
                            'employee_bank_account' : {},
                            'program_code' : {},
                            'payment_place' : {},
                            'payment_method' : {},
                            'check' : {},
                            'category' : {},
                            'bank' : {},
                            'perception_key' : {},
                            'rebase' : {},
                        }

                        lite_employee_deep_copy_tmp = {}


                        ######################################################################
                        ######################################################################
                        # Catalogs from odoo database
                        # Employees Postgres, RFC dict
                        self.env.cr.execute("""
                            select he.rfc as rfc, he.id as id_employee, rb.l10n_mx_edi_code as bank_key, rb.id as bank_id, 
                            rpb.acc_number, rpb.id as res_partner_bank_id from hr_employee he 
                            left join res_partner_bank rpb on he.id = rpb.employee_id  
                            left join res_bank rb on rpb.bank_id = rb.id
                            where he.rfc ~* \'([A-Za-z])+\' and he.active = true;
                            """)
                        postgres_employee_rfc_list = self.env.cr.fetchall()
                        postgres_employee_rfc_dict = {}
                        for postgres_employee_rfc_element in postgres_employee_rfc_list:
                            if postgres_employee_rfc_element[0] not in postgres_employee_rfc_dict:
                                postgres_employee_rfc_dict[postgres_employee_rfc_element[0]] = {'employee_id' : postgres_employee_rfc_element[1], 'banks' : { postgres_employee_rfc_element[2] : { 'bank_id' : postgres_employee_rfc_element[3], postgres_employee_rfc_element[4] : postgres_employee_rfc_element[5]} } if postgres_employee_rfc_element[2] is not None and postgres_employee_rfc_element[3] and postgres_employee_rfc_element[4] and postgres_employee_rfc_element[5] else {} }
                            elif postgres_employee_rfc_element[2] is not None and postgres_employee_rfc_element[3] and postgres_employee_rfc_element[4] and postgres_employee_rfc_element[5] and postgres_employee_rfc_element[2] in postgres_employee_rfc_dict[postgres_employee_rfc_element[0]]['banks'].keys():
                                postgres_employee_rfc_dict[postgres_employee_rfc_element[0]]['banks'][postgres_employee_rfc_element[2]].update({postgres_employee_rfc_element[4] : postgres_employee_rfc_element[5]})
                            elif postgres_employee_rfc_element[2] is not None and postgres_employee_rfc_element[3] and postgres_employee_rfc_element[4] and postgres_employee_rfc_element[5] and postgres_employee_rfc_element[2] not in postgres_employee_rfc_dict[postgres_employee_rfc_element[0]]['banks'].keys():
                                postgres_employee_rfc_dict[postgres_employee_rfc_element[0]]['banks'][postgres_employee_rfc_element[2]] = {'bank_id' : postgres_employee_rfc_element[3],postgres_employee_rfc_element[4] : postgres_employee_rfc_element[5]}


                        # Employee Postgres, categories dict
                        self.env.cr.execute("""
                            SELECT he.rfc, string_agg(concat(hec.name , ',', category_id::varchar), ';') as categories
                            FROM employee_category_rel ecr inner join hr_employee he ON ecr.emp_id = he.id 
                            inner join hr_employee_category hec ON ecr.category_id = hec.id 
                            where he.rfc ~* \'([A-Za-z])+\' and he.active = true group by ecr.emp_id, he.rfc ;
                            """)

                        postgres_employee_rfc_category_list = self.env.cr.fetchall()
                        for postgres_employee_rfc_category_element in postgres_employee_rfc_category_list:
                            postgres_employee_rfc_dict[postgres_employee_rfc_category_element[0]]['categories'] = dict((key, value) for key, value in [element.split(',') for element in postgres_employee_rfc_category_element[1].split(';')])


                        # Postgres program codes, program code dict
                        current_query = f'SELECT cp.program_code, cp.id, ie.unam_account_id FROM program_code cp LEFT JOIN expenditure_item ie ON cp.item_id = ie.id WHERE cp.state = \'validated\';'
                        self.env.cr.execute(current_query)
                        postgres_employee_program_code_list = self.env.cr.fetchall()
                        postgres_employee_program_code_dict = {}
                        for postgres_employee_program_code_element in postgres_employee_program_code_list:
                            postgres_employee_program_code_dict[postgres_employee_program_code_element[0]] = {'id' : postgres_employee_program_code_element[1], 'unam_account_id' : postgres_employee_program_code_element[2] or ''}


                        # Postgres payment place, payment place dict
                        current_query = f'SELECT name, id FROM payment_place WHERE name IS NOT NULL;'
                        self.env.cr.execute(current_query)
                        postgres_employee_payment_place_list = self.env.cr.fetchall()
                        postgres_employee_payment_place_dict = {}
                        for postgres_employee_payment_place_element in postgres_employee_payment_place_list:
                            postgres_employee_payment_place_dict[postgres_employee_payment_place_element[0]] = {'id' : postgres_employee_payment_place_element[1]}

                        # Postgres payment methods, payment method dict
                        current_query = f'SELECT name, code, id FROM l10n_mx_edi_payment_method;'
                        self.env.cr.execute(current_query)
                        postgres_employee_payment_method_list = self.env.cr.fetchall()
                        postgres_employee_payment_method_dict = {}
                        for postgres_employee_payment_method_list_element in postgres_employee_payment_method_list:
                            postgres_employee_payment_method_dict[postgres_employee_payment_method_list_element[0]] = {'code' : postgres_employee_payment_method_list_element[1], 'id' : postgres_employee_payment_method_list_element[2]}

                        # Postgres banks, bank dict
                        current_query = f'SELECT l10n_mx_edi_code, name, id FROM res_bank WHERE l10n_mx_edi_code IS NOT NULL AND LENGTH(TRIM(l10n_mx_edi_code)) > 1;'
                        self.env.cr.execute(current_query)
                        postgres_employee_bank_list = self.env.cr.fetchall()
                        postgres_employee_bank_dict = {}
                        for postgres_employee_bank_element in postgres_employee_bank_list:
                            postgres_employee_bank_dict[postgres_employee_bank_element[0]] = {'name' : postgres_employee_bank_element[1], 'id' : postgres_employee_bank_element[2]}

                        # Postgres perception key dict
                        current_query = f'SELECT key, id FROM preception;'
                        self.env.cr.execute(current_query)
                        postgres_employee_perception_list = self.env.cr.fetchall()
                        postgres_employee_perception_dict = {}
                        for postgres_employee_perception_list_element in postgres_employee_perception_list:
                            postgres_employee_perception_dict[postgres_employee_perception_list_element[0]] = postgres_employee_perception_list_element[1]

                        # Postgres deduction catalog dict
                        current_query = f'SELECT key, id FROM deduction;'
                        self.env.cr.execute(current_query)
                        postgres_employee_deduction_list = self.env.cr.fetchall()
                        postgres_employee_deduction_dict = {}
                        for postgres_employee_deduction_list_element in postgres_employee_deduction_list:
                            postgres_employee_deduction_dict[postgres_employee_deduction_list_element[0]] = {'deduction_id' : postgres_employee_deduction_list_element[1]}




                        ######################################################################
                        ######################################################################
                        #Verify if employee exists
                        lite_employee_rfc_payment = {
                            'transfer' : {},
                            'check' : {},
                        }

                        # Employee payment with checks from perception file
                        current_query = f'SELECT rfc, bank_key, check_number, payment_place FROM {perception_table_name} WHERE medium = \'Cheque nominativo\';'
                        cur.execute(current_query)
                        lite_employee_rfc_check_payment_list = cur.fetchall()
                        lite_employee_rfc_check_payment_dict = {}
                        for lite_employee_rfc_check_payment_element in lite_employee_rfc_check_payment_list:
                            lite_employee_rfc_payment['check'].update({lite_employee_rfc_check_payment_element[0] : {'bank_key' : lite_employee_rfc_check_payment_element[1], 'check_number' : lite_employee_rfc_check_payment_element[2], 'payment_place' : lite_employee_rfc_check_payment_element[3]}})

                        # Verify if employee exists in checks in postgres database
                        lite_employee_rfc_not_found_for_check = []
                        for lite_employee_rfc_payment_element in lite_employee_rfc_payment['check']:
                            if lite_employee_rfc_payment_element not in postgres_employee_rfc_dict.keys():
                                lite_employee_rfc_not_found_for_check.append(lite_employee_rfc_payment_element)

                        for lite_employee_rfc_not_found_for_check_element in lite_employee_rfc_not_found_for_check:
                            data_not_found_dict['employee']['check'].update({ lite_employee_rfc_not_found_for_check_element : lite_employee_rfc_payment['check'].pop(lite_employee_rfc_not_found_for_check_element)})

                        # Employees payment with transfers
                        current_query = f'SELECT rfc, bank_key, deposit_account_number, deposit_number, payment_place FROM {perception_table_name} WHERE medium = \'Transferencia electrónica de fondos\';'
                        cur.execute(current_query)
                        lite_employee_rfc_transfer_payment_list = cur.fetchall()
                        lite_employee_rfc_transfer_payment_dict = {}
                        for lite_employee_rfc_transfer_payment_element in lite_employee_rfc_transfer_payment_list:
                            lite_employee_rfc_payment['transfer'].update({lite_employee_rfc_transfer_payment_element[0] : {'bank_key' : lite_employee_rfc_transfer_payment_element[1], 'deposit_account_number' : lite_employee_rfc_transfer_payment_element[2], 'deposit_number' : lite_employee_rfc_transfer_payment_element[3], 'payment_place' : lite_employee_rfc_transfer_payment_element[4]}})

                        # Verify if employee exists in transfers
                        lite_employee_rfc_not_found_for_tranfer = []
                        for lite_employee_rfc_payment_element in lite_employee_rfc_payment['transfer']:
                            if lite_employee_rfc_payment_element not in postgres_employee_rfc_dict.keys():
                                lite_employee_rfc_not_found_for_tranfer.append(lite_employee_rfc_payment_element)

                        for lite_employee_rfc_not_found_for_tranfer_element in lite_employee_rfc_not_found_for_tranfer:
                            data_not_found_dict['employee']['transfer'].update({lite_employee_rfc_not_found_for_tranfer_element : lite_employee_rfc_payment['transfer'].pop(lite_employee_rfc_not_found_for_tranfer_element)})

                        lite_employee_rfc_bank_account_not_found = []
                        for lite_employee_rfc_payment_element in lite_employee_rfc_payment['transfer']:
                            if lite_employee_rfc_payment['transfer'][lite_employee_rfc_payment_element]['bank_key'] not in postgres_employee_rfc_dict[lite_employee_rfc_payment_element]['banks'] or lite_employee_rfc_payment['transfer'][lite_employee_rfc_payment_element]['deposit_account_number'] not in postgres_employee_rfc_dict[lite_employee_rfc_payment_element]['banks'][lite_employee_rfc_payment['transfer'][lite_employee_rfc_payment_element]['bank_key']]:
                                lite_employee_rfc_bank_account_not_found.append(lite_employee_rfc_payment_element)

                        for lite_employee_rfc_bank_account_not_found_element in lite_employee_rfc_bank_account_not_found:
                            data_not_found_dict['employee_bank_account'].update({lite_employee_rfc_bank_account_not_found_element : lite_employee_rfc_payment['transfer'].pop(lite_employee_rfc_bank_account_not_found_element)})


                        # Catalogs from perception file
                        # Verify if category exits
                        current_query = f'SELECT DISTINCT(category) FROM {perception_table_name} WHERE category IS NOT NULL AND LENGTH(category) > 0;'
                        cur.execute(current_query)
                        lite_employee_category_list = cur.fetchall()
                        lite_employee_category_dict = {}
                        for lite_employee_category_element in lite_employee_category_list:
                            lite_employee_category_dict[lite_employee_category_element[0]] = ''

                        # Verify if program code exits
                        current_query = f'SELECT DISTINCT(program_code) FROM {perception_table_name} WHERE program_code NOT LIKE \'{REBATE_PROGRAM_CODE_PATTERN}\';'
                        cur.execute(current_query)
                        lite_employee_program_code_list = cur.fetchall()
                        lite_employee_program_code_dict = {}
                        for lite_employee_program_code_element in lite_employee_program_code_list:
                            lite_employee_program_code_dict[lite_employee_program_code_element[0]] = ''

                        lite_employee_program_code_not_found = []
                        for lite_employee_program_code_element in lite_employee_program_code_dict:
                            if lite_employee_program_code_element not in postgres_employee_program_code_dict.keys():
                                lite_employee_program_code_not_found.append(lite_employee_program_code_element)


                        for lite_employee_program_code_element in lite_employee_program_code_not_found:
                            data_not_found_dict['program_code'].update({lite_employee_program_code_element : lite_employee_program_code_dict.pop(lite_employee_program_code_element)})

                        # Verify rebase register
                        current_query = f'SELECT DISTINCT(program_code) FROM {perception_table_name} WHERE program_code LIKE \'{REBATE_PROGRAM_CODE_PATTERN}\';'
                        cur.execute(current_query)
                        lite_employee_program_code_rebase_list = cur.fetchall()
                        lite_employee_program_code_rebase_dict = {}
                        for lite_employee_program_code_rebase_element in lite_employee_program_code_rebase_list:
                            lite_employee_program_code_rebase_dict[lite_employee_program_code_rebase_element[0]] = ''

                        for lite_employee_program_code_rebase_dict_element in lite_employee_program_code_rebase_dict:
                            lite_employee_program_code_deduction = lite_employee_program_code_rebase_dict_element[4:8].strip('0')
                            if lite_employee_program_code_deduction not in postgres_employee_deduction_dict:
                                data_not_found_dict['rebase'].update({lite_employee_program_code_rebase_dict_element : lite_employee_program_code_deduction })


                        # Verify if payment place exits
                        current_query = f'SELECT DISTINCT(payment_place) FROM {perception_table_name} WHERE payment_place IS NOT NULL AND LENGTH(TRIM(payment_place)) > 0;'
                        cur.execute(current_query)
                        lite_employee_payment_place_list = cur.fetchall()
                        lite_employee_payment_place_dict = {}
                        for lite_employee_payment_place_element in lite_employee_payment_place_list:
                            lite_employee_payment_place_dict[lite_employee_payment_place_element[0]] = ''

                        lite_employee_payment_place_not_found = []
                        for lite_employee_payment_place_element in lite_employee_payment_place_dict:
                            if lite_employee_payment_place_element not in postgres_employee_payment_place_dict:
                                lite_employee_payment_place_not_found.append(lite_employee_payment_place_element)

                        for lite_employee_payment_place_element in lite_employee_payment_place_not_found:
                            data_not_found_dict['payment_place'].update({lite_employee_payment_place_element : lite_employee_payment_place_dict.pop(lite_employee_payment_place_element)})

                        # Verify if payment medium exits
                        current_query = f'SELECT DISTINCT(medium) FROM {perception_table_name} WHERE medium IS NOT NULL AND LENGTH(TRIM(medium)) > 0;'
                        cur.execute(current_query)
                        lite_employee_medium_list = cur.fetchall()
                        lite_employee_medium_dict = {}
                        for lite_employee_medium_element in lite_employee_medium_list:
                            lite_employee_medium_dict[lite_employee_medium_element[0]] = ''

                        # Odoo could have payment methods defined but there are not rules defined for that payment method
                        lite_employee_payment_method_not_found = []
                        for lite_employee_medium_dict_element in lite_employee_medium_dict.keys():
                            if lite_employee_medium_dict_element not in postgres_employee_payment_method_dict:
                                lite_employee_payment_method_not_found.append(lite_employee_medium_dict_element)
                            elif lite_employee_medium_dict_element not in defined_rules_for_payment_method:
                                lite_employee_payment_method_not_found.append(lite_employee_medium_dict_element)

                        for lite_employee_payment_method_not_found_element in lite_employee_payment_method_not_found:
                            data_not_found_dict['payment_method'].update({lite_employee_payment_method_not_found_element : lite_employee_medium_dict.pop(lite_employee_payment_method_not_found_element)})


                        # Verify if bank key exits
                        current_query = f'SELECT DISTINCT(bank_key) FROM {perception_table_name} WHERE bank_key IS NOT NULL AND LENGTH(TRIM(bank_key)) > 0;'
                        cur.execute(current_query)
                        lite_employee_bank_key_list = cur.fetchall()
                        lite_employee_bank_key_dict = {}
                        for lite_employee_bank_key_element in lite_employee_bank_key_list:
                            lite_employee_bank_key_dict[lite_employee_bank_key_element[0]] = ''

                        lite_employee_bank_key_not_found = []
                        for lite_employee_bank_key_dict_element in lite_employee_bank_key_dict:
                            if lite_employee_bank_key_dict_element not in postgres_employee_bank_dict.keys():
                                lite_employee_bank_key_not_found.append(lite_employee_bank_key_dict_element)

                        for lite_employee_bank_key_not_found_element in lite_employee_bank_key_not_found:
                            data_not_found_dict['bank'].update({lite_employee_bank_key_not_found_element: lite_employee_bank_key_dict.pop(lite_employee_bank_key_not_found_element)})


                        # Get check list from Odoo database
                        postgres_employee_bank_id_list = [postgres_employee_bank_dict[lite_employee_bank_key_dict_element]['id'] for lite_employee_bank_key_dict_element in lite_employee_bank_key_dict]
                        current_query = """SELECT rb.l10n_mx_edi_code, STRING_AGG(concat(cl.folio, ',', cl.id), ';') 
                        FROM check_log cl INNER JOIN res_partner_bank rpb on cl.bank_account_id = rpb.id 
                        INNER JOIN res_bank rb on rpb.bank_id = rb.id where cl.status IN 
                        ('Available for printing') 
                        AND cl.general_status = 'available' AND rb.id IN %(postgres_employee_bank_id_list)s group by rb.l10n_mx_edi_code"""
                        
                        params = {'postgres_employee_bank_id_list': tuple(postgres_employee_bank_id_list)}
                        self.env.cr.execute(current_query, params)

                        postgrs_employee_check_log_list = self.env.cr.fetchall()
                        postgres_employee_check_log_dict = {}

                        for postgres_employee_check_log_list_element in postgrs_employee_check_log_list:
                            postgres_employee_check_log_dict[postgres_employee_check_log_list_element[0]] = dict((key, value) for key, value in [element.split(',') for element in postgres_employee_check_log_list_element[1].split(';')])


                        # Check folios list per bank, perception file
                        current_query = f'SELECT bank_key, GROUP_CONCAT(check_number, \',\') as checks FROM {perception_table_name} WHERE TRIM(medium) = \'Cheque nominativo\' GROUP BY bank_key ORDER BY bank_key;'
                        cur.execute(current_query)
                        lite_employee_check_per_bank_list = cur.fetchall()
                        lite_employee_check_folio_per_bank_dict = {}
                        for lite_employee_check_per_bank_list_element in lite_employee_check_per_bank_list:
                            lite_employee_check_folio_per_bank_dict[lite_employee_check_per_bank_list_element[0]] = dict((employee_check_per_bank_folio, '') for employee_check_per_bank_folio in lite_employee_check_per_bank_list_element[1].split(','))

                        for lite_employee_check_folio_per_bank_dict_element in lite_employee_check_folio_per_bank_dict:
                            data_not_found_dict['check'].update({lite_employee_check_folio_per_bank_dict_element : {}})


                        lite_employee_deep_copy_tmp = copy.deepcopy(lite_employee_check_folio_per_bank_dict)
                        for lite_employee_check_folio_per_bank_dict_element in lite_employee_deep_copy_tmp:
                            if lite_employee_check_folio_per_bank_dict_element in postgres_employee_check_log_dict:
                                for lite_employee_check_folio_element in lite_employee_deep_copy_tmp[lite_employee_check_folio_per_bank_dict_element]:
                                    if lite_employee_check_folio_element not in postgres_employee_check_log_dict[lite_employee_check_folio_per_bank_dict_element]:
                                        data_not_found_dict['check'][lite_employee_check_folio_per_bank_dict_element].update({lite_employee_check_folio_element : lite_employee_check_folio_per_bank_dict[lite_employee_check_folio_per_bank_dict_element].pop(lite_employee_check_folio_element)})
                            else:
                                data_not_found_dict['check'][lite_employee_check_folio_per_bank_dict_element] = lite_employee_check_folio_per_bank_dict.pop(lite_employee_check_folio_per_bank_dict_element)

                        lite_employee_deep_copy_tmp = {}

                        # Verify wheter perception keys exist
                        current_query = f'SELECT DISTINCT(perception_key) FROM {perception_table_name} WHERE LENGTH(TRIM(perception_key)) > 0;'
                        cur.execute(current_query)
                        lite_employee_perception_key_list = cur.fetchall()
                        lite_employee_perception_key_dict = {}

                        for lite_employee_perception_key_list_element in lite_employee_perception_key_list:
                            lite_employee_perception_key_dict[lite_employee_perception_key_list_element[0]] = ''


                        lite_employee_perception_key_not_found = []
                        for lite_employee_perception_key_dict_element in lite_employee_perception_key_dict:
                            if lite_employee_perception_key_dict_element not in postgres_employee_perception_dict.keys():
                                lite_employee_perception_key_not_found.append(lite_employee_perception_key_dict_element)

                        for lite_employee_perception_key_not_found_element in lite_employee_perception_key_not_found:
                            data_not_found_dict['perception_key'].update({lite_employee_perception_key_not_found_element : lite_employee_perception_key_dict[lite_employee_perception_key_not_found_element]})


                        there_is_error = False
                        content = f'###################################################\nDATOS ERRÓNEOS PERCEPCIONES : {datetime.now()}\n##################################################\n\n\n'
                        if len(data_not_found_dict['employee']['transfer']) > 0:
                            content = content  + f"\n################################################################\nEmpleados no encontrados en sistema, pago por transferencia\n################################################################\n[{', '.join(data_not_found_dict['employee']['transfer'])}]" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['employee']['check']) > 0:
                            content = content  + f"\n#######################################################\nEmpleados no encontrados en sistema, pago por cheque\n#######################################################\n[{', '.join(data_not_found_dict['employee']['check'])}]" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['employee_bank_account']) > 0:
                            content = content  + f"\n########################################################\nCuentas bancarias de empleados no encontradas en sistema\n########################################################\n{[data_not_found_dict_element['deposit_account_number'] for data_not_found_dict_element in data_not_found_dict['employee_bank_account'].values()]}" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['program_code']) > 0:
                            content = content  + f"\n###############################################\nCódigos programáticos no encontrados en sistema\n###############################################\n[{tuple(data_not_found_dict['program_code'].keys())}]" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['payment_place']) > 0:
                            content = content  + f"\n#########################################\nLugares de pago no encontrados en sistema\n#########################################\n[{', '.join(data_not_found_dict['payment_place'])}]" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['payment_method']) > 0:
                            content = content  + f"\n#########################################\nMétodos de pago no encontrados en sistema\n#########################################\n[{', '.join(data_not_found_dict['payment_method'])}]" + '\n\n\n'
                            there_is_error = True
                        if sum([len(data_not_found_dict['check'][dnfd_check]) or 0 for dnfd_check in data_not_found_dict['check']]) > 0:
                            content = content  + f"\n##################################################################\nNúmeros de cheque no encontrados en sistema o con estado no valido\n##################################################################\n[{json.dumps(data_not_found_dict['check'])}]" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['category']) > 0:
                            content = content  + f"\n##############################################\nClaves de categorías no encontradas en sistema\n##############################################\n[{', '.join(data_not_found_dict['category'])}]" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['bank']) > 0:
                            content = content  + f"\n#########################################\nClaves de banco no encontradas en sistema\n#########################################\n[{', '.join(data_not_found_dict['bank'])}]" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['perception_key']) > 0:
                            content = content  + f"\n##############################################\nClaves de percepción no encontradas en sistema\n##############################################\n[{', '.join(data_not_found_dict['perception_key'])}]" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['rebase']) > 0:
                            content = content  + f"\n##############################################\nClaves de deducciones para devoluciones no encontradas en sistema\n##############################################\n[{', '.join(data_not_found_dict['rebase'].values())}]" + '\n\n\n'
                            there_is_error = True




                        if not there_is_error:
                            payroll_processing_id = self.payroll_process_id.id
                            period_start = str(self.payroll_process_id.period_start)
                            period_end = str(self.payroll_process_id.period_end)

                            # Sum of perception, we do sum here because there is no sense make sum in validation zone
                            current_query = f'SELECT rfc, printf("%.2f", sum(amount)), (CASE WHEN medium = \'Transferencia electrónica de fondos\' THEN \'T\' WHEN medium = \'Cheque nominativo\' THEN \'C\' ELSE \'E\' END) AS which, group_concat(program_code||\',\'||perception_key||\',\'||amount, \';\') AS program_codes FROM {perception_table_name} WHERE rfc IS NOT NULL AND LENGTH(TRIM(rfc)) > 0 GROUP BY rfc ORDER BY id;'
                            cur.execute(current_query)
                            lite_employee_rfc_perception_total_list = cur.fetchall()

                            for lite_employee_rfc_perception_total_list_elememt in lite_employee_rfc_perception_total_list:
                                if lite_employee_rfc_perception_total_list_elememt[2] == 'T':
                                    lite_employee_rfc_payment['transfer'][lite_employee_rfc_perception_total_list_elememt[0]].update({'total_perception' : lite_employee_rfc_perception_total_list_elememt[1], 'program_codes' : [(program_code, perception_key, amount) for program_code, perception_key, amount in [element.split(',') for element in lite_employee_rfc_perception_total_list_elememt[3].split(';')]] })
                                else:
                                    lite_employee_rfc_payment['check'][lite_employee_rfc_perception_total_list_elememt[0]].update({'total_perception' : lite_employee_rfc_perception_total_list_elememt[1], 'program_codes' : [(program_code, perception_key, amount) for program_code, perception_key, amount in [element.split(',') for element in lite_employee_rfc_perception_total_list_elememt[3].split(';')]]})


                            # Query for transfer payment
                            current_query_general_value = """
                                    (nextval('employee_payroll_file_id_seq'), {}, (now() at time zone 'UTC'), {}, 
                                    (now() at time zone 'UTC'), '{}', '{}', 
                                    {}, '{}', false, {}, 
                                    'direct_employee', {}, {}, '{}', 
                                    '{}', {}, 'draft', 
                                    {}, {}, '{}', {},
                                    'payroll_benefit_payment'),
                            """
                            current_query = """
                                INSERT INTO "employee_payroll_file" ("id", "create_uid", "create_date", "write_uid", 
                                    "write_date", "bank_key", "deposite_number", 
                                    "employee_id", "fornight", "is_pension_payment_request", "l10n_mx_edi_payment_method_id", 
                                    "payment_request_type", "payroll_processing_id", "payroll_register_user_id", "period_end", 
                                    "period_start", "receiving_bank_acc_pay_id", "state",
                                    "payment_place_id", "bank_receiving_payment_id", "bank_key_name", "total_preception", 
                                    "request_type") 
                                VALUES """

                            value_tmp = ''
                            for lite_employee_rfc_payment_element in lite_employee_rfc_payment['transfer']:
                                value_tmp = current_query_general_value.format(self.env.user.id, self.env.user.id,
                                    lite_employee_rfc_payment['transfer'][lite_employee_rfc_payment_element]['bank_key'], lite_employee_rfc_payment['transfer'][lite_employee_rfc_payment_element]['deposit_number'],
                                    postgres_employee_rfc_dict[lite_employee_rfc_payment_element]['employee_id'], fortnight, postgres_employee_payment_method_dict['Transferencia electrónica de fondos']['id'],
                                    payroll_processing_id, self.env.user.id, period_end,
                                    period_start, ((postgres_employee_rfc_dict[lite_employee_rfc_payment_element]['banks'])[lite_employee_rfc_payment['transfer'][lite_employee_rfc_payment_element]['bank_key']])[lite_employee_rfc_payment['transfer'][lite_employee_rfc_payment_element]['deposit_account_number']],
                                    postgres_employee_payment_place_dict[lite_employee_rfc_payment['transfer'][lite_employee_rfc_payment_element]['payment_place']]['id'], postgres_employee_bank_dict[lite_employee_rfc_payment['transfer'][lite_employee_rfc_payment_element]['bank_key']]['id'], postgres_employee_bank_dict[lite_employee_rfc_payment['transfer'][lite_employee_rfc_payment_element]['bank_key']]['name'], lite_employee_rfc_payment['transfer'][lite_employee_rfc_payment_element]['total_perception'] )

                                current_query = current_query + value_tmp

                            if value_tmp:
                                current_query = current_query.strip().strip(',')
                                self.env.cr.execute(current_query)



                            # Query for check payment
                            current_query_general_value = """
                                    (nextval('employee_payroll_file_id_seq'), {}, (now() at time zone 'UTC'), {},
                                    (now() at time zone 'UTC'), '{}', '{}', 
                                    {}, '{}', false, {}, 
                                    'direct_employee', {}, {}, '{}', 
                                    '{}', 'draft', {},
                                    {}, '{}', {}, 'payroll_benefit_payment'),
                            """
                            current_query = """
                                INSERT INTO "employee_payroll_file" ("id", "create_uid", "create_date", "write_uid", 
                                    "write_date", "bank_key", "check_folio_id", 
                                    "employee_id", "fornight", "is_pension_payment_request", "l10n_mx_edi_payment_method_id", 
                                    "payment_request_type", "payroll_processing_id", "payroll_register_user_id", "period_end", 
                                    "period_start", "state", "check_number",
                                    "payment_place_id", "bank_key_name", "total_preception", "request_type") 
                                VALUES """


                            value_tmp = ''
                            for lite_employee_rfc_payment_element in lite_employee_rfc_payment['check']:
                                value_tmp = current_query_general_value.format(self.env.user.id, self.env.user.id,
                                    lite_employee_rfc_payment['check'][lite_employee_rfc_payment_element]['bank_key'], postgres_employee_check_log_dict[lite_employee_rfc_payment['check'][lite_employee_rfc_payment_element]['bank_key']][lite_employee_rfc_payment['check'][lite_employee_rfc_payment_element]['check_number']],
                                    postgres_employee_rfc_dict[lite_employee_rfc_payment_element]['employee_id'], fortnight, postgres_employee_payment_method_dict['Cheque nominativo']['id'],
                                    payroll_processing_id, self.env.user.id, period_end,
                                    period_start, lite_employee_rfc_payment['check'][lite_employee_rfc_payment_element]['check_number'],
                                    postgres_employee_payment_place_dict[lite_employee_rfc_payment['check'][lite_employee_rfc_payment_element]['payment_place']]['id'], postgres_employee_bank_dict[lite_employee_rfc_payment['check'][lite_employee_rfc_payment_element]['bank_key']]['name'], lite_employee_rfc_payment['check'][lite_employee_rfc_payment_element]['total_perception'] )

                                current_query = current_query + value_tmp

                            if value_tmp:
                                current_query = current_query.strip().strip(',')
                                self.env.cr.execute(current_query)

                            # Postgres get rfc and id from employee_payroll_file
                            current_query = f'SELECT eh.rfc, fpe.id FROM employee_payroll_file fpe INNER JOIN hr_employee eh ON fpe.employee_id = eh.id WHERE fpe.payroll_processing_id = {payroll_processing_id}'
                            self.env.cr.execute(current_query)
                            postgres_employee_rfc_payroll_file_list = self.env.cr.fetchall()
                            postgres_employee_rfc_payroll_file_dict = {}

                            for postgres_employee_rfc_payroll_file_list_element in postgres_employee_rfc_payroll_file_list:
                                postgres_employee_rfc_payroll_file_dict[postgres_employee_rfc_payroll_file_list_element[0]] = postgres_employee_rfc_payroll_file_list_element[1]

                            current_query_general_value = """
                                    (nextval('preception_line_id_seq'), {}, (now() at time zone 'UTC'), {}, 
                                    (now() at time zone 'UTC'), {}, false, false, 
                                    {}, {}, {}, {}),
                            """

                            current_query = """
                                INSERT INTO "preception_line" ("id", "create_uid", "create_date", "write_uid", 
                                    "write_date", "amount", "is_adjustment_update", "is_create_from_this", 
                                    "payroll_id", "preception_id", "program_code_id", "account_id") 
                                VALUES """

                            rebate_current_query_general_value = """
                                    (nextval('rebate_line_id_seq'), {}, {}, {}, 
                                    {}, {}, (now() at time zone 'UTC'), {}, 
                                    (now() at time zone 'UTC')),
                            """

                            rebate_current_query = """
                                INSERT INTO "rebate_line" ("id", "payroll_id", "perception_key_id", "deduction_key_id", 
                                    "amount", "create_uid", "create_date", "write_uid", 
                                    "write_date")
                                VALUES """

                            current_query_tmp = current_query
                            rebate_current_query_tmp = rebate_current_query

                            for lite_employee_rfc_payment_element in lite_employee_rfc_payment['transfer']:
                                for lite_employee_rfc_payment_element_program_code in lite_employee_rfc_payment['transfer'][lite_employee_rfc_payment_element]['program_codes']:
                                    if lite_employee_rfc_payment_element_program_code[0].find(REBATE_PROGRAM_CODE_PATTERN.strip('_')) != -1:
                                        rebate_value_tmp = rebate_current_query_general_value.format(postgres_employee_rfc_payroll_file_dict[lite_employee_rfc_payment_element], postgres_employee_perception_dict[lite_employee_rfc_payment_element_program_code[1]], postgres_employee_deduction_dict[lite_employee_rfc_payment_element_program_code[0][4:8].strip('0')]['deduction_id'],
                                            lite_employee_rfc_payment_element_program_code[2], self.env.user.id, self.env.user.id)

                                        rebate_current_query = rebate_current_query + rebate_value_tmp
                                    else:
                                        value_tmp = current_query_general_value.format(self.env.user.id, self.env.user.id,
                                            lite_employee_rfc_payment_element_program_code[2],
                                            postgres_employee_rfc_payroll_file_dict[lite_employee_rfc_payment_element], postgres_employee_perception_dict[lite_employee_rfc_payment_element_program_code[1]], postgres_employee_program_code_dict[lite_employee_rfc_payment_element_program_code[0]]['id'], postgres_employee_program_code_dict[lite_employee_rfc_payment_element_program_code[0]]['unam_account_id'])
                                
                                        current_query = current_query + value_tmp

                            
                            for lite_employee_rfc_payment_element in lite_employee_rfc_payment['check']:
                                for lite_employee_rfc_payment_element_program_code in lite_employee_rfc_payment['check'][lite_employee_rfc_payment_element]['program_codes']:
                                    if lite_employee_rfc_payment_element_program_code[0].find(REBATE_PROGRAM_CODE_PATTERN.strip('_')) != -1:
                                        rebate_value_tmp = rebate_current_query_general_value.format(postgres_employee_rfc_payroll_file_dict[lite_employee_rfc_payment_element], postgres_employee_perception_dict[lite_employee_rfc_payment_element_program_code[1]], postgres_employee_deduction_dict[lite_employee_rfc_payment_element_program_code[0][4:8].strip('0')]['deduction_id'],
                                            lite_employee_rfc_payment_element_program_code[2], self.env.user.id, self.env.user.id)

                                        rebate_current_query = rebate_current_query + rebate_value_tmp

                                    else:
                                        value_tmp = current_query_general_value.format(self.env.user.id, self.env.user.id,
                                            lite_employee_rfc_payment_element_program_code[2],
                                            postgres_employee_rfc_payroll_file_dict[lite_employee_rfc_payment_element], postgres_employee_perception_dict[lite_employee_rfc_payment_element_program_code[1]], postgres_employee_program_code_dict[lite_employee_rfc_payment_element_program_code[0]]['id'], postgres_employee_program_code_dict[lite_employee_rfc_payment_element_program_code[0]]['unam_account_id'])

                                        current_query = current_query + value_tmp

                            if current_query_tmp != current_query:
                                current_query = current_query.strip().strip(',')
                                self.env.cr.execute(current_query)

                            if rebate_current_query_tmp != rebate_current_query:
                                rebate_current_query = rebate_current_query.strip().strip(',')
                                self.env.cr.execute(rebate_current_query)

                            self.env.cr.commit()

                            return {
                                'name': 'Importación exitosa',
                                'type': 'ir.actions.act_window',
                                'view_mode': 'form',
                                'res_model': 'payroll.payment.message.wizard',
                                'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Importación exitosa de percepciones.'}).id,
                                'target': 'new'
                            }

                        else:
                            self.payroll_process_id.update_failed_file(content, 'payroll_perceptions', 'completed', self.env.user.id, self.filename)
                            return {
                                'name': 'Error en validación',
                                'type': 'ir.actions.act_window',
                                'view_mode': 'form',
                                'res_model': 'payroll.payment.message.wizard',
                                'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Se encontraron errores durante la validación del archivo de percepciones, corriga los errores e intente nuevamente.'}).id,
                                'target': 'new'
                            }

                    else:
                        return {
                            'name': 'Error en columnas de archivo',
                            'type': 'ir.actions.act_window',
                            'view_mode': 'form',
                            'res_model': 'payroll.payment.message.wizard',
                            'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Número de columnas del archivo de percepciones, no coincide con las columnas esperadas.'}).id,
                            'target': 'new'
                        }

                else:
                    return {
                        'name': 'Error en importación',
                        'type': 'ir.actions.act_window',
                        'view_mode': 'form',
                        'res_model': 'payroll.payment.message.wizard',
                        'res_id': self.env['payroll.payment.message.wizard'].create({'message': f'Error en imporación, la nómina [{self.payroll_process_id.name}], ya tiene percepciones cargadas. Por favor, elimínelas o genere una nueva nómina.'}).id,
                        'target': 'new'
                    }




            #======================== payroll_deductions ================#
            if self.type_of_movement == 'payroll_deductions':

                self.payroll_process_id.deductions_file = self.file
                self.payroll_process_id.deductions_filename = self.filename
                self.payroll_process_id.deductions_file_index = 1
                self.payroll_process_id.deductions_file_load = True
                self.payroll_process_id.deductions_hide = False

                current_query = f'SELECT count(fpe.id) as total FROM employee_payroll_file fpe WHERE fpe.payroll_processing_id = {self.payroll_process_id.id}'
                self.env.cr.execute(current_query)
                postgres_employee_payroll_file_total = self.env.cr.fetchall()
                postgres_employee_payroll_file_total = int(postgres_employee_payroll_file_total[0][0])

                current_query = f'SELECT count(*) as total FROM deduction_line ld INNER JOIN employee_payroll_file fpe ON ld.payroll_id = fpe.id WHERE fpe.payroll_processing_id = {self.payroll_process_id.id}'
                self.env.cr.execute(current_query)
                postgres_employee_deduction_line_total = self.env.cr.fetchall()
                postgres_employee_deduction_line_total = int(postgres_employee_deduction_line_total[0][0])

                if postgres_employee_payroll_file_total == 0:
                    return {
                        'name': 'Importación',
                        'type': 'ir.actions.act_window',
                        'view_mode': 'form',
                        'res_model': 'payroll.payment.message.wizard',
                        'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Error en importación, debe cargar primero archivo de percepciones.'}).id,
                        'target': 'new'
                    }
                elif postgres_employee_deduction_line_total > 0:
                    return {
                        'name': 'Importación',
                        'type': 'ir.actions.act_window',
                        'view_mode': 'form',
                        'res_model': 'payroll.payment.message.wizard',
                        'res_id': self.env['payroll.payment.message.wizard'].create({'message': f'Error en importación, la nómina [{self.payroll_process_id.name}], ya tiene deducciones cargadas. Por favor, elimínelas o genere una nueva nómina.'}).id,
                        'target': 'new'
                    }

                else:
                    deduction_header = ['rfc', 'account_accountant', 'deduction_key', 'amount', 'net_salary']
                    file_path_db = tempfile.mkdtemp()
                    fortnight = self.payroll_process_id.fornight
                    year = self.payroll_process_id.fornight
                    deduction_table_name = f'deduction_{fortnight}{year}'

                    db_payroll_path = f"{file_path_db}/payroll_{fortnight}{year}.db"
                    con = sqlite3.connect(db_payroll_path)
                    cur = con.cursor()


                    current_query = f"DROP TABLE IF EXISTS {deduction_table_name};"
                    cur.execute(current_query)
                    deduction_header_table_creation = [element + ' TEXT' for element in deduction_header]
                    deduction_header_table_creation = ','.join(deduction_header_table_creation)
                    current_query = f'CREATE TABLE {deduction_table_name}(id INTEGER PRIMARY KEY,{deduction_header_table_creation});'
                    cur.execute(current_query)


                    # To open the workbook
                    # Workbook object is created
                    wb_obj = openpyxl.load_workbook(filename=io.BytesIO(base64.b64decode(self.file)), read_only=True, data_only=True, keep_links=False)

                    # Get workbook active sheet object
                    # from the active attribute
                    sheet_obj = wb_obj.active

                    max_row = sheet_obj.max_row
                    max_column = sheet_obj.max_column

                    if max_column == len(deduction_header):
                        data = []
                        rfc_tmp = ''
                        counter = 1
                        for i in sheet_obj.iter_rows(values_only=True, min_row=2):
                            rfc = str(i[0]).strip() if i[0] is not None else ''
                            account_accountant = str(i[1]).strip() if i[1] is not None else ''
                            deduction_key = str(i[2]).strip() if i[2] is not None else ''
                            amount = str(i[3]).strip() if i[3] is not None else ''
                            net_salary = str(i[4]).strip() if i[4] is not None else ''
                            if rfc != '':
                                rfc_tmp = rfc
                            current_query = f"INSERT INTO {deduction_table_name} VALUES({counter}, \'{rfc_tmp}\', \'{account_accountant}\', \'{deduction_key}\', \'{amount}\', \'{net_salary}\');"
                            counter = counter + 1
                            cur.execute(current_query)
                    
                    
                        con.commit()

                        data_not_found_dict = {
                            'employee' : {},
                            'employee_not_in_deduction' : {},
                            'deduction_key' : {}
                        }

                        # Get total perceptions, verify if employee's total in perceptions are equals to employee's total in deductions
                        current_query = f'SELECT COUNT(DISTINCT(rfc)) FROM {deduction_table_name} WHERE LENGTH(TRIM(rfc)) > 0;'
                        cur.execute(current_query)
                        lite_total_deduction = cur.fetchall()

                        lite_total_deduction = int(lite_total_deduction[0][0])
                        #if lite_total_deduction == postgres_employee_payroll_file_total:


                        ######################################################################
                        ######################################################################
                        # Catalogs from odoo database
                        # Postgres employee perception dict, for deduction
                        current_query = f'SELECT eh.rfc, fpe.id FROM employee_payroll_file fpe INNER JOIN hr_employee eh ON fpe.employee_id = eh.id WHERE fpe.payroll_processing_id = {self.payroll_process_id.id}'
                        self.env.cr.execute(current_query)
                        postgres_employee_rfc_perception_list = self.env.cr.fetchall()
                        postgres_employee_rfc_employee_payroll_file_dict = {}
                        for postgres_employee_rfc_employee_payroll_file_list_element in postgres_employee_rfc_perception_list:
                            postgres_employee_rfc_employee_payroll_file_dict[postgres_employee_rfc_employee_payroll_file_list_element[0]] = { 'employee_payroll_file_id' : postgres_employee_rfc_employee_payroll_file_list_element[1]}

                        # Postgres deduction catalog dict
                        current_query = f'SELECT key, id FROM deduction;'
                        self.env.cr.execute(current_query)
                        postgres_employee_deduction_list = self.env.cr.fetchall()
                        postgres_employee_deduction_dict = {}
                        for postgres_employee_deduction_list_element in postgres_employee_deduction_list:
                            postgres_employee_deduction_dict[postgres_employee_deduction_list_element[0]] = {'deduction_id' : postgres_employee_deduction_list_element[1]}





                        ######################################################################
                        ######################################################################
                        # Catalogs from data file
                        # Lite employee rfc list from file
                        current_query = f'SELECT DISTINCT(rfc) FROM {deduction_table_name} WHERE LENGTH(TRIM(rfc)) > 0;'
                        cur.execute(current_query)
                        lite_employee_deduction_rfc_list = cur.fetchall()
                        lite_employee_deduction_rfc_dict = {}
                        for lite_employee_deduction_rfc_list_element in lite_employee_deduction_rfc_list:
                            lite_employee_deduction_rfc_dict[lite_employee_deduction_rfc_list_element[0]] = {}


                        # Lite employee deduction key list
                        current_query = f'SELECT DISTINCT(deduction_key) as deduction_key FROM {deduction_table_name};'
                        cur.execute(current_query)
                        lite_employee_deduction_key_list = cur.fetchall()
                        lite_employee_deduction_key_dict = {}

                        for lite_employee_deduction_key_list_element in lite_employee_deduction_key_list:
                            lite_employee_deduction_key_dict[lite_employee_deduction_key_list_element[0]] = {}

                        lite_employee_rfc_employee_payroll_file_not_found = []
                        for lite_employee_deduction_rfc_dict_element in lite_employee_deduction_rfc_dict.keys():
                            if lite_employee_deduction_rfc_dict_element not in postgres_employee_rfc_employee_payroll_file_dict:
                                lite_employee_rfc_employee_payroll_file_not_found.append(lite_employee_deduction_rfc_dict_element)

                        for lite_employee_rfc_employee_payroll_file_not_found_element in lite_employee_rfc_employee_payroll_file_not_found:
                            data_not_found_dict['employee'].update({lite_employee_rfc_employee_payroll_file_not_found_element : lite_employee_rfc_employee_payroll_file_not_found_element})

                        for postgres_employee_rfc_employee_payroll_file_dict_element in postgres_employee_rfc_employee_payroll_file_dict:
                            if postgres_employee_rfc_employee_payroll_file_dict_element not in lite_employee_deduction_rfc_dict:
                                data_not_found_dict['employee_not_in_deduction'].update({postgres_employee_rfc_employee_payroll_file_dict_element : postgres_employee_rfc_employee_payroll_file_dict_element})



                        lite_employee_deduction_key_not_found = []
                        for lite_employee_deduction_key_dict_element in lite_employee_deduction_key_dict:
                            if lite_employee_deduction_key_dict_element not in postgres_employee_deduction_dict:
                                lite_employee_deduction_key_not_found.append(lite_employee_deduction_key_dict_element)

                        for lite_employee_deduction_key_not_found_element in lite_employee_deduction_key_not_found:
                            data_not_found_dict['deduction_key'].update({lite_employee_deduction_key_not_found_element : lite_employee_deduction_key_dict.pop(lite_employee_deduction_key_not_found_element)})

                        there_is_error = False
                        content = f'###################################################\nDATOS ERRÓNEOS DEDUCCIONES : {datetime.now()}\n##################################################\n\n\n'
                        if len(data_not_found_dict['employee']) > 0:
                            content = content  + f"\n##########################\nEmpleados sin percepciones\n##########################\n[{', '.join(data_not_found_dict['employee'])}]" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['employee_not_in_deduction']) > 0:
                            content = content  + f"\n##########################\nEmpleados en percepciones pero no en deducciones\n##########################\n[{', '.join(data_not_found_dict['employee_not_in_deduction'])}]" + '\n\n\n'
                            there_is_error = True

                        if len(data_not_found_dict['deduction_key']) > 0:
                            content = content  + f"\n##############################################\nClaves de deducciones no encontradas en sistema\n##############################################\n[{', '.join(data_not_found_dict['deduction_key'])}]" + '\n\n\n'
                            there_is_error = True

                        if not there_is_error:
                            current_query = f'SELECT rfc, GROUP_CONCAT(deduction_key||\',\'||printf("%.2f", amount), \';\'), printf("%.2f", net_salary), printf("%.2f", sum(amount) ) FROM {deduction_table_name} group by rfc order by id;'
                            cur.execute(current_query)
                            lite_employee_deduction_file_list = cur.fetchall()
                            lite_employee_deduction_file_dict = {}

                            for lite_employee_deduction_file_list_element in lite_employee_deduction_file_list:
                                lite_employee_deduction_file_dict[lite_employee_deduction_file_list_element[0]] = { 'deduction' : [(key, value) for key, value in [ledfle.split(',') for ledfle in lite_employee_deduction_file_list_element[1].split(';')]], 'net_salary' : lite_employee_deduction_file_list_element[2], 'total_deduction' : lite_employee_deduction_file_list_element[3]}


                            current_query_general_value = """
                                    (nextval('deduction_line_id_seq'), {}, (now() at time zone 'UTC'), {}, 
                                    (now() at time zone 'UTC'), {}, false, false, 
                                    {}, {}, {}),
                            """

                            current_query = """
                                INSERT INTO "deduction_line" ("id", "create_uid", "create_date", "write_uid", 
                                    "write_date", "amount", "is_adjustment_update", "is_create_from_this", 
                                    "payroll_id", "deduction_id", "net_salary") 
                                VALUES """

                            for lite_employee_deduction_file_dict_element in lite_employee_deduction_file_dict:
                                for deduction_element in lite_employee_deduction_file_dict[lite_employee_deduction_file_dict_element]['deduction']:
                                    value_tmp = current_query_general_value.format(self.env.user.id, self.env.user.id,
                                        deduction_element[1],
                                        postgres_employee_rfc_employee_payroll_file_dict[lite_employee_deduction_file_dict_element]['employee_payroll_file_id'], postgres_employee_deduction_dict[deduction_element[0]]['deduction_id'], lite_employee_deduction_file_dict[lite_employee_deduction_file_dict_element]['net_salary'])

                                    current_query = current_query + value_tmp

                            current_query = current_query.strip().strip(',')
                            self.env.cr.execute(current_query)


                            current_query_general_value = """
                                ({}, {}, {}),
                            """

                            current_query = """
                                UPDATE employee_payroll_file SET 
                                total_deduction = tmp_data.total_deduction,
                                net_salary = tmp_data.net_salary
                                FROM ( VALUES {} ) AS tmp_data(id_element, total_deduction, net_salary) WHERE id = tmp_data.id_element;
                            """

                            value_tmp = ''
                            for lite_employee_deduction_file_dict_element in lite_employee_deduction_file_dict:
                                value_tmp = value_tmp + current_query_general_value.format(postgres_employee_rfc_employee_payroll_file_dict[lite_employee_deduction_file_dict_element]['employee_payroll_file_id'], lite_employee_deduction_file_dict[lite_employee_deduction_file_dict_element]['total_deduction'], lite_employee_deduction_file_dict[lite_employee_deduction_file_dict_element]['net_salary'])

                            value_tmp = value_tmp.strip().strip(',')
                            current_query = current_query.format(value_tmp)
                            self.env.cr.execute(current_query)
                            self.env.cr.commit()


                            return {
                                'name': 'Importación exitosa',
                                'type': 'ir.actions.act_window',
                                'view_mode': 'form',
                                'res_model': 'payroll.payment.message.wizard',
                                'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Importación exitosa de deducciones.'}).id,
                                'target': 'new'
                            }


                        else:
                            self.payroll_process_id.update_failed_file(content, 'payroll_deductions', 'completed', self.env.user.id, self.filename)
                            return {
                                'name': 'Importación',
                                'type': 'ir.actions.act_window',
                                'view_mode': 'form',
                                'res_model': 'payroll.payment.message.wizard',
                                'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Error en importación, se encontraron errores durante la validación de deducciones.'}).id,
                                'target': 'new'
                            }

                    else:
                        return {
                            'name': 'Error en columnas de archivo',
                            'type': 'ir.actions.act_window',
                            'view_mode': 'form',
                            'res_model': 'payroll.payment.message.wizard',
                            'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Número de columnas del archivo de deducciones, no coincide con las columnas esperadas.'}).id,
                            'target': 'new'
                        }



            #======================== pension_payment ================# 
            if self.type_of_movement == 'pension_payments':
                data = base64.decodestring(self.file)
                book = open_workbook(file_contents=data or b'')
                sheet = book.sheet_by_index(0)


                current_query = f'SELECT count(fpe.id) as total FROM employee_payroll_file fpe WHERE fpe.payroll_processing_id = {self.payroll_process_id.id}'
                self.env.cr.execute(current_query)
                postgres_employee_payroll_file_total = self.env.cr.fetchall()
                postgres_employee_payroll_file_total = int(postgres_employee_payroll_file_total[0][0])

                current_query = f'SELECT count(*) as total FROM deduction_line ld INNER JOIN employee_payroll_file fpe ON ld.payroll_id = fpe.id WHERE fpe.payroll_processing_id = {self.payroll_process_id.id}'
                self.env.cr.execute(current_query)
                postgres_employee_deduction_line_total = self.env.cr.fetchall()
                postgres_employee_deduction_line_total = int(postgres_employee_deduction_line_total[0][0])

                current_query = f'SELECT count(*) as total FROM pension_payment_line lpp INNER JOIN employee_payroll_file fpe ON lpp.payroll_id = fpe.id WHERE fpe.payroll_processing_id = {self.payroll_process_id.id}'
                self.env.cr.execute(current_query)
                postgres_employee_pension_payment_line_total = self.env.cr.fetchall()
                postgres_employee_pension_payment_line_total = int(postgres_employee_pension_payment_line_total[0][0])

                if postgres_employee_payroll_file_total == 0 or postgres_employee_deduction_line_total == 0:
                    return {
                        'name': 'Importación',
                        'type': 'ir.actions.act_window',
                        'view_mode': 'form',
                        'res_model': 'payroll.payment.message.wizard',
                        'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Error en importación de pago de pensión alimenticia, debe importar primero percepciones y deducciones.'}).id,
                        'target': 'new'
                    }
                elif postgres_employee_pension_payment_line_total > 0:
                    return {
                        'name': 'Importación',
                        'type': 'ir.actions.act_window',
                        'view_mode': 'form',
                        'res_model': 'payroll.payment.message.wizard',
                        'res_id': self.env['payroll.payment.message.wizard'].create({'message': f'Error en importación de pagos a beneficiarios de pensión alimenticia, ya existen pagos a beneficiarios cargados en la nomina actual({self.payroll_process_id.name}).'}).id,
                        'target': 'new'
                    }

                else:
                    alimony_payment_header = ['rfc', 'beneficiary_name', 'medium', 'bank_key', 'check_number', 'deposit_number', 'deposit_account_number', 'net']
                    file_path_db = tempfile.mkdtemp()
                    fortnight = self.payroll_process_id.fornight
                    year = self.payroll_process_id.fornight
                    alimony_payment_table_name = f'alimony_payment{fortnight}{year}'

                    db_payroll_path = f"{file_path_db}/payroll_{fortnight}{year}.db"
                    con = sqlite3.connect(db_payroll_path)
                    cur = con.cursor()


                    current_query = f"DROP TABLE IF EXISTS {alimony_payment_table_name};"
                    cur.execute(current_query)
                    alimony_payment_header_table_creation = [element + ' TEXT' for element in alimony_payment_header]
                    alimony_payment_header_table_creation = ','.join(alimony_payment_header_table_creation)
                    current_query = f'CREATE TABLE {alimony_payment_table_name}(id INTEGER PRIMARY KEY,{alimony_payment_header_table_creation});'
                    cur.execute(current_query)


                    # To open the workbook
                    # Workbook object is created
                    wb_obj = openpyxl.load_workbook(filename=io.BytesIO(base64.b64decode(self.file)), read_only=True, data_only=True, keep_links=False)

                    # Get workbook active sheet object
                    # from the active attribute
                    sheet_obj = wb_obj.active

                    max_row = sheet_obj.max_row
                    max_column = sheet_obj.max_column
                    if max_column == len(alimony_payment_header):
                        counter = 1
                        for i in sheet_obj.iter_rows(values_only=True, min_row=2):
                            rfc = str(i[0]).strip() if i[0] is not None else ''
                            beneficiary_name = str(i[1]).strip() if i[1] is not None else ''
                            medium = str(i[2]).strip() if i[2] is not None else ''
                            bank_key = str(i[3]).strip() if i[3] is not None else ''
                            check_number = str(i[4]).strip() if i[4] is not None else ''
                            deposit_number = str(i[5]).strip() if i[5] is not None else ''
                            deposit_account_number = str(i[6]).strip() if i[6] is not None else ''
                            net = str(i[7]).strip() if i[7] is not None else ''
                            current_query = f"INSERT INTO {alimony_payment_table_name} VALUES({counter}, \'{rfc}\', \'{beneficiary_name}\', \'{medium}\', \'{bank_key}\', \'{check_number}\', \'{deposit_number}\', \'{deposit_account_number}\', printf('%.2f', {net}));"
                            counter = counter + 1
                            cur.execute(current_query)
                    
                    
                        con.commit()

                        current_query = f"SELECT {','.join(alimony_payment_header)} FROM {alimony_payment_table_name};"
                        cur.execute(current_query)
                        lite_alimony_payment_table_data_list = cur.fetchall()
                        lite_alimony_payment_table_data_dict = {}
                        for lite_alimony_payment_table_data_list_element in lite_alimony_payment_table_data_list:
                            if lite_alimony_payment_table_data_list_element[0] not in lite_alimony_payment_table_data_dict:
                                lite_alimony_payment_table_data_dict[lite_alimony_payment_table_data_list_element[0]] = [(lite_alimony_payment_table_data_list_element[1], lite_alimony_payment_table_data_list_element[2], lite_alimony_payment_table_data_list_element[3], lite_alimony_payment_table_data_list_element[4], lite_alimony_payment_table_data_list_element[5], lite_alimony_payment_table_data_list_element[6], lite_alimony_payment_table_data_list_element[7])]
                            else:
                                lite_alimony_payment_table_data_dict[lite_alimony_payment_table_data_list_element[0]].append((lite_alimony_payment_table_data_list_element[1], lite_alimony_payment_table_data_list_element[2], lite_alimony_payment_table_data_list_element[3], lite_alimony_payment_table_data_list_element[4], lite_alimony_payment_table_data_list_element[5], lite_alimony_payment_table_data_list_element[6], lite_alimony_payment_table_data_list_element[7]))


                        ######################################################################
                        ######################################################################
                        # Catalogs from odoo database
                        # Postgres employee perception dict, for alimony payment
                        current_query = f'SELECT eh.rfc, fpe.id FROM employee_payroll_file fpe INNER JOIN hr_employee eh ON fpe.employee_id = eh.id WHERE fpe.payroll_processing_id = {self.payroll_process_id.id}'
                        self.env.cr.execute(current_query)
                        postgres_employee_rfc_perception_list = self.env.cr.fetchall()
                        postgres_employee_rfc_employee_payroll_file_dict = {}
                        for postgres_employee_rfc_employee_payroll_file_list_element in postgres_employee_rfc_perception_list:
                            postgres_employee_rfc_employee_payroll_file_dict[postgres_employee_rfc_employee_payroll_file_list_element[0]] = { 'employee_payroll_file_id' : postgres_employee_rfc_employee_payroll_file_list_element[1]}


                        # Beneficiaries of employees
                        current_query = "SELECT eh.rfc, STRING_AGG(concat(pr.name, ',', pr.id),'ß') as beneficiary FROM hr_employee_res_partner_rel rpreh INNER JOIN hr_employee eh ON rpreh.hr_employee_id = eh.id INNER JOIN res_partner pr ON rpreh.res_partner_id = pr.id WHERE LENGTH(TRIM(eh.rfc)) > 0 GROUP BY eh.rfc;"
                        self.env.cr.execute(current_query)
                        postgres_employee_rfc_alimony_list = self.env.cr.fetchall()

                        for postgres_employee_rfc_alimony_list_element in postgres_employee_rfc_alimony_list:
                            if postgres_employee_rfc_alimony_list_element[0] in postgres_employee_rfc_employee_payroll_file_dict:
                                postgres_employee_rfc_employee_payroll_file_dict[postgres_employee_rfc_alimony_list_element[0]].update({'beneficiary' : dict((key, value) for key, value in [beneficiary.split(',') for beneficiary in postgres_employee_rfc_alimony_list_element[1].split('ß')]) })


                        current_query = """
                            SELECT eh.rfc as employee_rfc, pr.name as beneficiary_name, pr.id as beneficiary_id, bpr.key_bank, bpr.acc_number, bpr.id
                            FROM hr_employee_res_partner_rel rpreh 
                            INNER JOIN hr_employee eh ON rpreh.hr_employee_id = eh.id 
                            INNER JOIN res_partner pr ON rpreh.res_partner_id = pr.id
                            INNER JOIN res_partner_bank bpr on rpreh.res_partner_id = bpr.partner_id
                            WHERE LENGTH(TRIM(eh.rfc)) > 0 ORDER BY eh.rfc;
                        """

                        self.env.cr.execute(current_query)
                        postgres_employee_rfc_alimony_res_partner_bank_list = self.env.cr.fetchall()
                        postgres_employee_rfc_alimony_res_partner_bank_dict = {}

                        for postgres_employee_rfc_alimony_res_partner_bank_list_element in postgres_employee_rfc_alimony_res_partner_bank_list:
                            if postgres_employee_rfc_alimony_res_partner_bank_list_element[0] in postgres_employee_rfc_alimony_res_partner_bank_dict:
                                if postgres_employee_rfc_alimony_res_partner_bank_list_element[1] in postgres_employee_rfc_alimony_res_partner_bank_dict[postgres_employee_rfc_alimony_res_partner_bank_list_element[0]]:
                                    if postgres_employee_rfc_alimony_res_partner_bank_list_element[3] in postgres_employee_rfc_alimony_res_partner_bank_dict[postgres_employee_rfc_alimony_res_partner_bank_list_element[0]][postgres_employee_rfc_alimony_res_partner_bank_list_element[1]]:
                                        postgres_employee_rfc_alimony_res_partner_bank_dict[postgres_employee_rfc_alimony_res_partner_bank_list_element[0]][postgres_employee_rfc_alimony_res_partner_bank_list_element[1]][postgres_employee_rfc_alimony_res_partner_bank_list_element[3]].update({postgres_employee_rfc_alimony_res_partner_bank_list_element[4] : postgres_employee_rfc_alimony_res_partner_bank_list_element[5]})
                                    else:
                                        postgres_employee_rfc_alimony_res_partner_bank_dict[postgres_employee_rfc_alimony_res_partner_bank_list_element[0]][postgres_employee_rfc_alimony_res_partner_bank_list_element[1]].update({postgres_employee_rfc_alimony_res_partner_bank_list_element[3] : {postgres_employee_rfc_alimony_res_partner_bank_list_element[4] : postgres_employee_rfc_alimony_res_partner_bank_list_element[5]}})
                                else:
                                    postgres_employee_rfc_alimony_res_partner_bank_dict[postgres_employee_rfc_alimony_res_partner_bank_list_element[0]].update({postgres_employee_rfc_alimony_res_partner_bank_list_element[1] : { 'beneficiary_id' : postgres_employee_rfc_alimony_res_partner_bank_list_element[2], postgres_employee_rfc_alimony_res_partner_bank_list_element[3] : {postgres_employee_rfc_alimony_res_partner_bank_list_element[4] : postgres_employee_rfc_alimony_res_partner_bank_list_element[5]}} })
                            else:
                                postgres_employee_rfc_alimony_res_partner_bank_dict[postgres_employee_rfc_alimony_res_partner_bank_list_element[0]] = {postgres_employee_rfc_alimony_res_partner_bank_list_element[1] : { 'beneficiary_id' : postgres_employee_rfc_alimony_res_partner_bank_list_element[2], postgres_employee_rfc_alimony_res_partner_bank_list_element[3] : {postgres_employee_rfc_alimony_res_partner_bank_list_element[4] : postgres_employee_rfc_alimony_res_partner_bank_list_element[5]}} }

                        data_not_found_dict = {
                            'employee' : {},
                            'beneficiary' : [],
                            'medium' : {},
                            'bank_key' : {},
                            'check_number' : {},
                            'deposit_account_number' : [],
                        }

                        # Postgres payment methods, payment method dict
                        current_query = f'SELECT name, code, id FROM l10n_mx_edi_payment_method;'
                        self.env.cr.execute(current_query)
                        postgres_alimony_payment_method_list = self.env.cr.fetchall()
                        postgres_alimony_payment_method_dict = {}
                        for postgres_alimony_payment_method_list_element in postgres_alimony_payment_method_list:
                            postgres_alimony_payment_method_dict[postgres_alimony_payment_method_list_element[0]] = {'code' : postgres_alimony_payment_method_list_element[1], 'id' : postgres_alimony_payment_method_list_element[2]}


                        lite_alimony_payment_table_data_dict_tmp = copy.deepcopy(lite_alimony_payment_table_data_dict)
                        for lite_alimony_payment_table_data_dict_element in lite_alimony_payment_table_data_dict:
                            if lite_alimony_payment_table_data_dict_element not in postgres_employee_rfc_employee_payroll_file_dict:
                                data_not_found_dict['employee'].update({lite_alimony_payment_table_data_dict_element : ''})
                                lite_alimony_payment_table_data_dict_tmp.pop(lite_alimony_payment_table_data_dict_element)

                        for lite_alimony_payment_table_data_dict_tmp_element in lite_alimony_payment_table_data_dict_tmp:
                            for lite_alimony_payment_table_data_dict_beneficiary in lite_alimony_payment_table_data_dict_tmp[lite_alimony_payment_table_data_dict_tmp_element]:
                                if not ('beneficiary' in postgres_employee_rfc_employee_payroll_file_dict[lite_alimony_payment_table_data_dict_tmp_element] and lite_alimony_payment_table_data_dict_beneficiary[0] in postgres_employee_rfc_employee_payroll_file_dict[lite_alimony_payment_table_data_dict_tmp_element]['beneficiary']):
                                    data_not_found_dict['beneficiary'].append((lite_alimony_payment_table_data_dict_tmp_element, lite_alimony_payment_table_data_dict_beneficiary[0]))

                        # Postgres banks, bank dict
                        current_query = f'SELECT l10n_mx_edi_code, name, id FROM res_bank WHERE l10n_mx_edi_code IS NOT NULL AND LENGTH(TRIM(l10n_mx_edi_code)) > 1;'
                        self.env.cr.execute(current_query)
                        postgres_alimony_bank_list = self.env.cr.fetchall()
                        postgres_alimony_bank_dict = {}
                        for postgres_alimony_bank_element in postgres_alimony_bank_list:
                            postgres_alimony_bank_dict[postgres_alimony_bank_element[0]] = {'name' : postgres_alimony_bank_element[1], 'id' : postgres_alimony_bank_element[2]}


                        # Verify if bank key exits
                        current_query = f'SELECT DISTINCT(bank_key) FROM {alimony_payment_table_name};'
                        cur.execute(current_query)
                        lite_alimony_bank_key_list = cur.fetchall()
                        lite_alimony_bank_key_dict = {}
                        for lite_alimony_bank_key_element in lite_alimony_bank_key_list:
                            lite_alimony_bank_key_dict[lite_alimony_bank_key_element[0]] = ''

                        for lite_alimony_bank_key_dict_element in lite_alimony_bank_key_dict:
                            if lite_alimony_bank_key_dict_element not in postgres_alimony_bank_dict:
                                data_not_found_dict['bank_key'].update({lite_alimony_bank_key_dict_element : ''})

                        for data_not_found_dict_element in data_not_found_dict['bank_key'].keys():
                            lite_alimony_bank_key_dict.pop(data_not_found_dict_element)


                        current_query = f'SELECT DISTINCT(medium) FROM {alimony_payment_table_name};'
                        cur.execute(current_query)
                        lite_alimony_payment_method_list = cur.fetchall()

                        for lite_alimony_payment_method_list_element in lite_alimony_payment_method_list:
                            if lite_alimony_payment_method_list_element[0] not in postgres_alimony_payment_method_dict:
                                data_not_found_dict['medium'].update({lite_alimony_payment_method_list_element[0] : ''})


                        # Get check list from Odoo database
                        postgres_alimony_bank_id_list = [postgres_alimony_bank_dict[lite_alimony_bank_key_dict_element]['id'] for lite_alimony_bank_key_dict_element in lite_alimony_bank_key_dict]
                        current_query = """SELECT rb.l10n_mx_edi_code, STRING_AGG(concat(cl.folio, ',', cl.id), ';') 
                        FROM check_log cl INNER JOIN res_partner_bank rpb on cl.bank_account_id = rpb.id 
                        INNER JOIN res_bank rb on rpb.bank_id = rb.id where cl.status IN 
                        ('Available for printing') 
                        AND cl.general_status = 'available' AND rb.id IN %(postgres_alimony_bank_id_list)s group by rb.l10n_mx_edi_code"""

                        params = {'postgres_alimony_bank_id_list': tuple(postgres_alimony_bank_id_list)}
                        self.env.cr.execute(current_query, params)
                        
                        postgres_alimony_check_log_list = self.env.cr.fetchall()
                        postgres_alimony_check_log_dict = {}

                        for postgres_alimony_check_log_list_element in postgres_alimony_check_log_list:
                            postgres_alimony_check_log_dict[postgres_alimony_check_log_list_element[0]] = dict((key, value) for key, value in [element.split(',') for element in postgres_alimony_check_log_list_element[1].split(';')])




                        # Check folios list per bank, perception file
                        current_query = f'SELECT bank_key, GROUP_CONCAT(check_number, \',\') as checks FROM {alimony_payment_table_name} WHERE TRIM(medium) = \'Cheque nominativo\' GROUP BY bank_key ORDER BY bank_key;'
                        cur.execute(current_query)
                        lite_alimony_check_per_bank_list = cur.fetchall()
                        lite_alimony_check_folio_per_bank_dict = {}
                        for lite_alimony_check_per_bank_list_element in lite_alimony_check_per_bank_list:
                            lite_alimony_check_folio_per_bank_dict[lite_alimony_check_per_bank_list_element[0]] = dict((alimony_check_per_bank_folio, '') for alimony_check_per_bank_folio in lite_alimony_check_per_bank_list_element[1].split(','))

                        for lite_alimony_check_folio_per_bank_dict_element in lite_alimony_check_folio_per_bank_dict:
                            data_not_found_dict['check_number'].update({lite_alimony_check_folio_per_bank_dict_element : {}})


                        lite_alimony_deep_copy_tmp = copy.deepcopy(lite_alimony_check_folio_per_bank_dict)
                        for lite_alimony_check_folio_per_bank_dict_element in lite_alimony_deep_copy_tmp:
                            if lite_alimony_check_folio_per_bank_dict_element in postgres_alimony_check_log_dict:
                                for lite_alimony_check_folio_element in lite_alimony_deep_copy_tmp[lite_alimony_check_folio_per_bank_dict_element]:
                                    if lite_alimony_check_folio_element not in postgres_alimony_check_log_dict[lite_alimony_check_folio_per_bank_dict_element]:
                                        data_not_found_dict['check_number'][lite_alimony_check_folio_per_bank_dict_element].update({lite_alimony_check_folio_element : lite_alimony_check_folio_per_bank_dict[lite_alimony_check_folio_per_bank_dict_element].pop(lite_alimony_check_folio_element)})
                            else:
                                data_not_found_dict['check_number'][lite_alimony_check_folio_per_bank_dict_element] = lite_alimony_check_folio_per_bank_dict.pop(lite_alimony_check_folio_per_bank_dict_element)

                        lite_alimony_deep_copy_tmp = {}



                        current_query = f'SELECT rfc, beneficiary_name, bank_key, deposit_account_number FROM {alimony_payment_table_name} WHERE medium = \'Transferencia electrónica de fondos\' GROUP BY bank_key, deposit_account_number;'
                        cur.execute(current_query)
                        lite_employee_rfc_alimony_bank_bank_account_list = cur.fetchall()

                        for lite_employee_rfc_alimony_bank_bank_account_list_element in lite_employee_rfc_alimony_bank_bank_account_list:
                            if lite_employee_rfc_alimony_bank_bank_account_list_element[0] in lite_alimony_payment_table_data_dict_tmp and not (lite_employee_rfc_alimony_bank_bank_account_list_element[0] in postgres_employee_rfc_alimony_res_partner_bank_dict and lite_employee_rfc_alimony_bank_bank_account_list_element[1] in postgres_employee_rfc_alimony_res_partner_bank_dict[lite_employee_rfc_alimony_bank_bank_account_list_element[0]] and lite_employee_rfc_alimony_bank_bank_account_list_element[2] in postgres_employee_rfc_alimony_res_partner_bank_dict[lite_employee_rfc_alimony_bank_bank_account_list_element[0]][lite_employee_rfc_alimony_bank_bank_account_list_element[1]] and lite_employee_rfc_alimony_bank_bank_account_list_element[3] in postgres_employee_rfc_alimony_res_partner_bank_dict[lite_employee_rfc_alimony_bank_bank_account_list_element[0]][lite_employee_rfc_alimony_bank_bank_account_list_element[1]][lite_employee_rfc_alimony_bank_bank_account_list_element[2]]):
                                data_not_found_dict['deposit_account_number'].append((lite_employee_rfc_alimony_bank_bank_account_list_element[1], lite_employee_rfc_alimony_bank_bank_account_list_element[2], lite_employee_rfc_alimony_bank_bank_account_list_element[3]))

                        current_query = f'SELECT rfc, SUM(net) FROM {alimony_payment_table_name} GROUP BY rfc ORDER BY rfc;'
                        cur.execute(current_query)
                        lite_employee_rfc_group_list = cur.fetchall()
                        lite_employee_rfc_group_dict = {}

                        for lite_employee_rfc_group_list_element in lite_employee_rfc_group_list:
                            lite_employee_rfc_group_dict[lite_employee_rfc_group_list_element[0]] = {'total_alimony_payment' : lite_employee_rfc_group_list_element[1]}



                        there_is_error = False
                        content = f'###################################################\nDATOS ERRÓNEOS BENEFICIARIOS DE PENSION ALIMENTICIA : {datetime.now()}\n##################################################\n\n\n'
                        if len(data_not_found_dict['employee']) > 0:
                            content = content  + f"\n################################################################\nEmpleados no encontrados(RFC) en la nómina actual para pago de beneficiarios de pensión alimenticia\n################################################################\n[{', '.join(data_not_found_dict['employee'].keys())}]" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['beneficiary']) > 0:
                            content = content  + f"\n#######################################################\nBeneficiarios de pensión alimenticia no asociados con empleados(RFC).\n#######################################################\n{data_not_found_dict['beneficiary']}" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['medium']) > 0:
                            content = content  + f"\n########################################################\nMedios de pago no encontrados en sistema\n########################################################\n[{', '.join(data_not_found_dict['medium'].keys())}]" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['bank_key']) > 0:
                            content = content  + f"\n########################################################\nClaves de banco no encontradas\n########################################################\n[{', '.join(data_not_found_dict['bank_key'].keys())}]" + '\n\n\n'
                            there_is_error = True
                        if sum([len(data_not_found_dict['check_number'][dnfd_check]) or 0 for dnfd_check in data_not_found_dict['check_number']]) > 0:
                            content = content  + f"\n###############################################\nNúmeros de cheque no encontrados en sistema\n###############################################\n[{dict(zip([key for key in data_not_found_dict['check_number'].keys()], [list(data_not_found_dict['check_number'][element].keys()) for element in list(data_not_found_dict['check_number'].keys())])) }]" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['deposit_account_number']) > 0:
                            content = content  + f"\n###############################################\nCuentas bancarias no encontradas en sistema\n###############################################\n{data_not_found_dict['deposit_account_number']}" + '\n\n\n'
                            there_is_error = True


                        if not there_is_error:
                            
                            current_query = """
                                INSERT INTO "pension_payment_line"
                                ("id", "payroll_id", "partner_id", "l10n_mx_edi_payment_method_id", 
                                "deposit_number", "check_number", "total_pension", 
                                "bank_acc_number", "bank_id", "bank_key", "create_uid", 
                                "create_date", "write_uid", "write_date", "check_folio_id")
                                VALUES
                            """

                            current_query_general_value = """
                                (nextval('pension_payment_line_id_seq'), {}, {}, {}, 
                                '{}', '{}', '{}', 
                                {}, {}, '{}', {}, 
                                (now() at time zone 'UTC'), {}, (now() at time zone 'UTC'), {}),
                            """

                            is_pension_payment_request_list = []
                            value_tmp = ''
                            for lite_alimony_payment_table_data_dict_element in lite_alimony_payment_table_data_dict:
                                for internal_value in lite_alimony_payment_table_data_dict[lite_alimony_payment_table_data_dict_element]:
                                    value_tmp = value_tmp + current_query_general_value.format(
                                        postgres_employee_rfc_employee_payroll_file_dict[lite_alimony_payment_table_data_dict_element]['employee_payroll_file_id'], 
                                        postgres_employee_rfc_employee_payroll_file_dict[lite_alimony_payment_table_data_dict_element]['beneficiary'][internal_value[0]],
                                        postgres_alimony_payment_method_dict[internal_value[1]]['id'],
                                        internal_value[4] or '',
                                        internal_value[3] or '',
                                        internal_value[6] or '',
                                        postgres_employee_rfc_alimony_res_partner_bank_dict[lite_alimony_payment_table_data_dict_element][internal_value[0]][internal_value[2]][internal_value[5]] if internal_value[1] == 'Transferencia electrónica de fondos' else 'NULL',
                                        postgres_alimony_bank_dict[internal_value[2]]['id'],
                                        internal_value[2],
                                        self.env.user.id,
                                        self.env.user.id,
                                        postgres_alimony_check_log_dict[internal_value[2]][internal_value[3]] if internal_value[1] == 'Cheque nominativo' else 'NULL'
                                        )
                                    is_pension_payment_request_list.append(postgres_employee_rfc_employee_payroll_file_dict[lite_alimony_payment_table_data_dict_element]['employee_payroll_file_id'])

                            if value_tmp:
                                current_query = current_query + value_tmp.strip().strip(',')
                                self.env.cr.execute(current_query)
                                current_query = f'UPDATE "employee_payroll_file" SET "is_pension_payment_request" = true WHERE "id" in {tuple(is_pension_payment_request_list)};'
                                self.env.cr.execute(current_query)

                            current_query_general_value = """
                                ({}, {}),
                            """

                            current_query = """
                                UPDATE employee_payroll_file SET 
                                total_alimony_payment = tmp_data.total_alimony_payment
                                FROM ( VALUES {} ) AS tmp_data(id_element, total_alimony_payment) WHERE id = tmp_data.id_element;
                            """

                            value_tmp = ''
                            for lite_employee_rfc_group_dict_element in lite_employee_rfc_group_dict:
                                value_tmp = value_tmp + current_query_general_value.format(postgres_employee_rfc_employee_payroll_file_dict[lite_employee_rfc_group_dict_element]['employee_payroll_file_id'], lite_employee_rfc_group_dict[lite_employee_rfc_group_dict_element]['total_alimony_payment'])

                            if value_tmp:
                                value_tmp = value_tmp.strip().strip(',')
                                current_query = current_query.format(value_tmp)
                                self.env.cr.execute(current_query)
                                self.env.cr.commit()


                            return {
                                'name': 'Importación exitosa',
                                'type': 'ir.actions.act_window',
                                'view_mode': 'form',
                                'res_model': 'payroll.payment.message.wizard',
                                'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Importación exitosa de pago de pensión alimenticia.'}).id,
                                'target': 'new'
                            }
                        else:
                            self.payroll_process_id.update_failed_file(content, 'alimony_payments', 'completed', self.env.user.id, self.filename)
                            return {
                                'name': 'Error en validación',
                                'type': 'ir.actions.act_window',
                                'view_mode': 'form',
                                'res_model': 'payroll.payment.message.wizard',
                                'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Se encontraron errores durante la validación del archivo de pago a beneficiarios de pensión alimenticia, corriga los errores e intente nuevamente.'}).id,
                                'target': 'new'
                            }

                    else:
                        return {
                            'name': 'Error en columnas de archivo',
                            'type': 'ir.actions.act_window',
                            'view_mode': 'form',
                            'res_model': 'payroll.payment.message.wizard',
                            'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Número de columnas del archivo de pago de pensión alimenticia, no coincide con las columnas esperadas.'}).id,
                            'target': 'new'
                        }

            #======================== Additional payments ================# 
            if self.type_of_movement == 'additional_payments':
                data = base64.decodestring(self.file)
                book = open_workbook(file_contents=data or b'')
                sheet = book.sheet_by_index(0)

                current_query = f'SELECT count(fpe.id) as total FROM employee_payroll_file fpe WHERE fpe.payroll_processing_id = {self.payroll_process_id.id}'
                self.env.cr.execute(current_query)
                postgres_employee_payroll_file_total = self.env.cr.fetchall()
                postgres_employee_payroll_file_total = int(postgres_employee_payroll_file_total[0][0])

                current_query = f'SELECT count(*) as total FROM deduction_line ld INNER JOIN employee_payroll_file fpe ON ld.payroll_id = fpe.id WHERE fpe.payroll_processing_id = {self.payroll_process_id.id}'
                self.env.cr.execute(current_query)
                postgres_employee_deduction_line_total = self.env.cr.fetchall()
                postgres_employee_deduction_line_total = int(postgres_employee_deduction_line_total[0][0])

                current_query = f'SELECT count(*) as total FROM additional_payments_line lpa INNER JOIN employee_payroll_file fpe ON lpa.payroll_id = fpe.id WHERE fpe.payroll_processing_id = {self.payroll_process_id.id}'
                self.env.cr.execute(current_query)
                postgres_employee_additional_payment_line_total = self.env.cr.fetchall()
                postgres_employee_additional_payment_line_total = int(postgres_employee_additional_payment_line_total[0][0])

                if postgres_employee_payroll_file_total == 0 or postgres_employee_deduction_line_total == 0:
                    return {
                        'name': 'Importación',
                        'type': 'ir.actions.act_window',
                        'view_mode': 'form',
                        'res_model': 'payroll.payment.message.wizard',
                        'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Error en importación de pagos de prestación adicionales, debe importar primero percepciones y deducciones.'}).id,
                        'target': 'new'
                    }
                elif postgres_employee_additional_payment_line_total > 0:
                    return {
                        'name': 'Importación',
                        'type': 'ir.actions.act_window',
                        'view_mode': 'form',
                        'res_model': 'payroll.payment.message.wizard',
                        'res_id': self.env['payroll.payment.message.wizard'].create({'message': f'Error en importación de pagos de prestación adicional, ya existen pagos adicionales cargados en la nomina actual({self.payroll_process_id.name}).'}).id,
                        'target': 'new'
                    }

                else:
                    additional_payment_header = ['rfc', 'program_code', 'net', 'detail']
                    file_path_db = tempfile.mkdtemp()
                    fortnight = self.payroll_process_id.fornight
                    year = self.payroll_process_id.fornight
                    additional_payment_table_name = f'additional_payment{fortnight}{year}'

                    db_payroll_path = f"{file_path_db}/payroll_{fortnight}{year}.db"
                    con = sqlite3.connect(db_payroll_path)
                    cur = con.cursor()


                    current_query = f"DROP TABLE IF EXISTS {additional_payment_table_name};"
                    cur.execute(current_query)
                    additional_payment_header_table_creation = [element + ' TEXT' for element in additional_payment_header]
                    additional_payment_header_table_creation = ','.join(additional_payment_header_table_creation)
                    current_query = f'CREATE TABLE {additional_payment_table_name}(id INTEGER PRIMARY KEY,{additional_payment_header_table_creation});'
                    cur.execute(current_query)


                    # To open the workbook
                    # Workbook object is created
                    wb_obj = openpyxl.load_workbook(filename=io.BytesIO(base64.b64decode(self.file)), read_only=True, data_only=True, keep_links=False)

                    # Get workbook active sheet object
                    # from the active attribute
                    sheet_obj = wb_obj.active

                    max_row = sheet_obj.max_row
                    max_column = sheet_obj.max_column
                    if max_column == len(additional_payment_header):
                        counter = 1
                        for i in sheet_obj.iter_rows(values_only=True, min_row=2):
                            rfc = str(i[0]).strip() if i[0] is not None else ''
                            program_code = str(i[1]).strip() if i[1] is not None else ''
                            net = str(i[2]).strip() if i[2] is not None else ''
                            detail = str(i[3]).strip() if i[3] is not None else ''
                            current_query = f"INSERT INTO {additional_payment_table_name} VALUES({counter}, TRIM(\'{rfc}\'), TRIM(\'{program_code}\'), TRIM(\'{net}\'), TRIM(\'{detail}\'));"
                            counter = counter + 1
                            cur.execute(current_query)
                    
                    
                        con.commit()

                        current_query = f"SELECT {','.join(additional_payment_header)} FROM {additional_payment_table_name};"
                        cur.execute(current_query)
                        lite_additional_payment_table_data_list = cur.fetchall()
                        lite_additional_payment_table_data_dict = {}
                        for lite_additional_payment_table_data_list_element in lite_additional_payment_table_data_list:
                            if lite_additional_payment_table_data_list_element[0] not in lite_additional_payment_table_data_dict:
                                lite_additional_payment_table_data_dict[lite_additional_payment_table_data_list_element[0]] = [(lite_additional_payment_table_data_list_element[1], lite_additional_payment_table_data_list_element[2], lite_additional_payment_table_data_list_element[3],)]
                            else:
                                lite_additional_payment_table_data_dict[lite_additional_payment_table_data_list_element[0]].append((lite_additional_payment_table_data_list_element[1], lite_additional_payment_table_data_list_element[2], lite_additional_payment_table_data_list_element[3],))

                        ######################################################################
                        ######################################################################
                        # Postgres employee perception dict, for additional payments
                        current_query = f'SELECT eh.rfc, fpe.id FROM employee_payroll_file fpe INNER JOIN hr_employee eh ON fpe.employee_id = eh.id WHERE fpe.payroll_processing_id = {self.payroll_process_id.id}'
                        self.env.cr.execute(current_query)
                        postgres_employee_rfc_perception_list = self.env.cr.fetchall()
                        postgres_employee_rfc_employee_payroll_file_dict = {}
                        for postgres_employee_rfc_employee_payroll_file_list_element in postgres_employee_rfc_perception_list:
                            postgres_employee_rfc_employee_payroll_file_dict[postgres_employee_rfc_employee_payroll_file_list_element[0]] = { 'employee_payroll_file_id' : postgres_employee_rfc_employee_payroll_file_list_element[1]}


                        # Postgres program codes, program code dict
                        current_query = f'SELECT cp.program_code, cp.id, ie.unam_account_id FROM program_code cp LEFT JOIN expenditure_item ie ON cp.item_id = ie.id WHERE cp.state = \'validated\';'
                        self.env.cr.execute(current_query)
                        postgres_employee_program_code_list = self.env.cr.fetchall()
                        postgres_employee_program_code_dict = {}
                        for postgres_employee_program_code_element in postgres_employee_program_code_list:
                            postgres_employee_program_code_dict[postgres_employee_program_code_element[0]] = {'id' : postgres_employee_program_code_element[1], 'unam_account_id' : postgres_employee_program_code_element[2] or ''}


                        # Postgres detail dict
                        postgres_selection_field_additional_payment_dict = {}
                        postgres_selection_field_additional_payment_dict['detail'] = {key : value for key, value in self.env['additional.payments.line'].fields_get([], {})['details']['selection']}
                        # Deatil empy in file, if space in record then it must be an error
                        postgres_selection_field_additional_payment_dict['detail'].update({'' : ''})

                        data_not_found_dict = {
                            'employee' : [],
                            'program_code' : [],
                            'detail' : [],
                        }

                        current_query = f'SELECT DISTINCT(rfc) FROM {additional_payment_table_name};'
                        cur.execute(current_query)
                        lite_additional_payment_employee_rfc_list = cur.fetchall()
                        for lite_additional_payment_employee_rfc_list_element in lite_additional_payment_employee_rfc_list:
                            if lite_additional_payment_employee_rfc_list_element[0] not in postgres_employee_rfc_employee_payroll_file_dict:
                                data_not_found_dict['employee'].append(lite_additional_payment_employee_rfc_list_element[0])


                        current_query = f'SELECT DISTINCT(program_code) FROM {additional_payment_table_name};'
                        cur.execute(current_query)
                        lite_additional_payment_program_code_list = cur.fetchall()
                        for lite_additional_payment_program_code_list_element in lite_additional_payment_program_code_list:
                            if lite_additional_payment_program_code_list_element[0] not in postgres_employee_program_code_dict:
                                data_not_found_dict['program_code'].append(lite_additional_payment_program_code_list_element[0])


                        current_query = f'SELECT DISTINCT(detail) FROM {additional_payment_table_name};'
                        cur.execute(current_query)
                        lite_additional_payment_detail_list = cur.fetchall()
                        for lite_additional_payment_detail_list_element in lite_additional_payment_detail_list:
                            if lite_additional_payment_detail_list_element[0] not in postgres_selection_field_additional_payment_dict['detail']:
                                data_not_found_dict['detail'].append(lite_additional_payment_detail_list_element[0])

                        current_query = f'SELECT rfc, SUM(net) FROM {additional_payment_table_name} GROUP BY rfc ORDER BY rfc;'
                        cur.execute(current_query)
                        lite_employee_rfc_group_list = cur.fetchall()
                        lite_employee_rfc_group_dict = {}

                        for lite_employee_rfc_group_list_element in lite_employee_rfc_group_list:
                            lite_employee_rfc_group_dict[lite_employee_rfc_group_list_element[0]] = {'total_additional_lending' : lite_employee_rfc_group_list_element[1]}

                        there_is_error = False
                        content = f'###################################################\nDATOS ERRÓNEOS PAGOS DE PRESTACIÓN ADICIONAL : {datetime.now()}\n##################################################\n\n\n'
                        if len(data_not_found_dict['employee']) > 0:
                            content = content  + f"\n################################################################\nEmpleados no encontrados(RFC) en la nómina actual para pagos de prestación adicional\n################################################################\n{data_not_found_dict['employee']}" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['program_code']) > 0:
                            content = content  + f"\n#######################################################\nCódigos programáticos no encontrados en sistema\n#######################################################\n{data_not_found_dict['program_code']}" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['detail']) > 0:
                            content = content  + f"\n########################################################\nDetalles no encontrados\n########################################################\n{data_not_found_dict['detail']}" + '\n\n\n'
                            there_is_error = True


                        if not there_is_error:

                            current_query = """
                                INSERT INTO "additional_payments_line"
                                ("id", "payroll_id", "details", "amount", 
                                "create_uid", "create_date", "write_uid", 
                                "write_date", "program_code_id")
                                VALUES
                            """

                            current_query_general_value = """
                                (nextval('additional_payments_line_id_seq'), {}, \'{}\', {}, 
                                {}, (now() at time zone 'UTC'), {}, 
                                (now() at time zone 'UTC'), {}),
                            """

                            value_tmp = ''
                            for lite_additional_payment_table_data_dict_element in lite_additional_payment_table_data_dict:
                                for lite_additional_payment_table_data_dict_internal in lite_additional_payment_table_data_dict[lite_additional_payment_table_data_dict_element]:
                                    value_tmp = value_tmp + current_query_general_value.format(postgres_employee_rfc_employee_payroll_file_dict[lite_additional_payment_table_data_dict_element]['employee_payroll_file_id'], lite_additional_payment_table_data_dict_internal[2], lite_additional_payment_table_data_dict_internal[1], 
                                        self.env.user.id, self.env.user.id, postgres_employee_program_code_dict[lite_additional_payment_table_data_dict_internal[0]]['id'])

                            if value_tmp:
                                current_query = current_query + value_tmp.strip().strip(',')
                                self.env.cr.execute(current_query)


                            current_query_general_value = """
                                ({}, {}),
                            """

                            current_query = """
                                UPDATE employee_payroll_file SET 
                                total_additional_lending = tmp_data.total_additional_lending
                                FROM ( VALUES {} ) AS tmp_data(id_element, total_additional_lending) WHERE id = tmp_data.id_element;
                            """

                            value_tmp = ''
                            for lite_employee_rfc_group_dict_element in lite_employee_rfc_group_dict:
                                value_tmp = value_tmp + current_query_general_value.format(postgres_employee_rfc_employee_payroll_file_dict[lite_employee_rfc_group_dict_element]['employee_payroll_file_id'], lite_employee_rfc_group_dict[lite_employee_rfc_group_dict_element]['total_additional_lending'])

                            if value_tmp:
                                value_tmp = value_tmp.strip().strip(',')
                                current_query = current_query.format(value_tmp)
                                self.env.cr.execute(current_query)
                                self.env.cr.commit()

                            return {
                                'name': 'Importación exitosa',
                                'type': 'ir.actions.act_window',
                                'view_mode': 'form',
                                'res_model': 'payroll.payment.message.wizard',
                                'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Importación exitosa de pagos de prestación adicional.'}).id,
                                'target': 'new'
                            }

                        else:
                            self.payroll_process_id.update_failed_file(content, 'additional_payments', 'completed', self.env.user.id, self.filename)
                            return {
                                'name': 'Error en validación',
                                'type': 'ir.actions.act_window',
                                'view_mode': 'form',
                                'res_model': 'payroll.payment.message.wizard',
                                'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Se encontraron errores durante la validación del archivo de pagos de prestación adicional, corriga los errores e intente nuevamente.'}).id,
                                'target': 'new'
                            }
                    else:
                        return {
                            'name': 'Error en columnas de archivo',
                            'type': 'ir.actions.act_window',
                            'view_mode': 'form',
                            'res_model': 'payroll.payment.message.wizard',
                            'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Número de columnas del archivo de prestación adicional, no coincide con las columnas esperadas.'}).id,
                            'target': 'new'
                        }


            #======================== additional_pension_payments ================# 
            if self.type_of_movement == 'additional_pension_payments':
                data = base64.decodestring(self.file)
                book = open_workbook(file_contents=data or b'')
                sheet = book.sheet_by_index(0)
                self.env.user.notify_success(message='Additional Pension Payments Process Is Start',
                                    title="Additional Pension Payments", sticky=True)
                

                current_query = f'SELECT count(fpe.id) as total FROM employee_payroll_file fpe WHERE fpe.payroll_processing_id = {self.payroll_process_id.id}'
                self.env.cr.execute(current_query)
                postgres_employee_payroll_file_total = self.env.cr.fetchall()
                postgres_employee_payroll_file_total = int(postgres_employee_payroll_file_total[0][0])

                current_query = f'SELECT count(*) as total FROM deduction_line ld INNER JOIN employee_payroll_file fpe ON ld.payroll_id = fpe.id WHERE fpe.payroll_processing_id = {self.payroll_process_id.id}'
                self.env.cr.execute(current_query)
                postgres_employee_deduction_line_total = self.env.cr.fetchall()
                postgres_employee_deduction_line_total = int(postgres_employee_deduction_line_total[0][0])

                current_query = f'SELECT count(*) as total FROM additional_pension_payments_line lppa INNER JOIN employee_payroll_file fpe ON lppa.payroll_id = fpe.id WHERE fpe.payroll_processing_id = {self.payroll_process_id.id}'
                self.env.cr.execute(current_query)
                postgres_employee_additional_pension_payments_line_total = self.env.cr.fetchall()
                postgres_employee_additional_pension_payments_line_total = int(postgres_employee_additional_pension_payments_line_total[0][0])

                current_query = f'SELECT count(*) as total FROM pension_payment_line lpp INNER JOIN employee_payroll_file fpe ON lpp.payroll_id = fpe.id WHERE fpe.payroll_processing_id = {self.payroll_process_id.id}'
                self.env.cr.execute(current_query)
                postgres_employee_pension_payment_line_total = self.env.cr.fetchall()
                postgres_employee_pension_payment_line_total = int(postgres_employee_pension_payment_line_total[0][0])


                if postgres_employee_payroll_file_total == 0 or postgres_employee_deduction_line_total == 0:
                    return {
                        'name': 'Importación',
                        'type': 'ir.actions.act_window',
                        'view_mode': 'form',
                        'res_model': 'payroll.payment.message.wizard',
                        'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Error en importación de pagos de pensión alimenticia adicionales, debe importar primero percepciones, deducciones y pagos de pensión alimenticia, posteriormente los pagos de pensión alimenticia adicional.'}).id,
                        'target': 'new'
                    }
                elif postgres_employee_additional_pension_payments_line_total > 0:
                    return {
                        'name': 'Importación',
                        'type': 'ir.actions.act_window',
                        'view_mode': 'form',
                        'res_model': 'payroll.payment.message.wizard',
                        'res_id': self.env['payroll.payment.message.wizard'].create({'message': f'Error en importación de pagos de pensión alimenticia adicional, ya existen pagos de pensión alimenticia adicional cargados en la nomina actual({self.payroll_process_id.name}).'}).id,
                        'target': 'new'
                    }
                elif postgres_employee_pension_payment_line_total == 0:
                    return {
                        'name': 'Importación',
                        'type': 'ir.actions.act_window',
                        'view_mode': 'form',
                        'res_model': 'payroll.payment.message.wizard',
                        'res_id': self.env['payroll.payment.message.wizard'].create({'message': f'Error en importación de pagos de pensión alimenticia adicional, no existen pagos de pensión alimenticia cargados en la nomina actual({self.payroll_process_id.name}), cargue primero los pagos de pensión alimenticia y posteriormente las pagos de pensión alimenticia adicional.'}).id,
                        'target': 'new'
                    }
                else:
                    additional_alimony_payment_header = ['rfc', 'beneficiary_name', 'net']
                    file_path_db = tempfile.mkdtemp()
                    fortnight = self.payroll_process_id.fornight
                    year = self.payroll_process_id.fornight
                    additional_alimony_payment_table_name = f'additional_payment{fortnight}{year}'

                    db_payroll_path = f"{file_path_db}/payroll_{fortnight}{year}.db"
                    con = sqlite3.connect(db_payroll_path)
                    cur = con.cursor()


                    current_query = f"DROP TABLE IF EXISTS {additional_alimony_payment_table_name};"
                    cur.execute(current_query)
                    additional_alimony_payment_header_table_creation = [element + ' TEXT' for element in additional_alimony_payment_header]
                    additional_alimony_payment_header_table_creation = ','.join(additional_alimony_payment_header_table_creation)
                    current_query = f'CREATE TABLE {additional_alimony_payment_table_name}(id INTEGER PRIMARY KEY,{additional_alimony_payment_header_table_creation});'
                    cur.execute(current_query)


                    # To open the workbook
                    # Workbook object is created
                    wb_obj = openpyxl.load_workbook(filename=io.BytesIO(base64.b64decode(self.file)), read_only=True, data_only=True, keep_links=False)

                    # Get workbook active sheet object
                    # from the active attribute
                    sheet_obj = wb_obj.active

                    max_row = sheet_obj.max_row
                    max_column = sheet_obj.max_column
                    if max_column == len(additional_alimony_payment_header):
                        counter = 1
                        for i in sheet_obj.iter_rows(values_only=True, min_row=2):
                            rfc = str(i[0]).strip() if i[0] is not None else ''
                            beneficiary_name = str(i[1]).strip() if i[1] is not None else ''
                            net = str(i[2]).strip() if i[2] is not None else ''
                            current_query = f"INSERT INTO {additional_alimony_payment_table_name} VALUES({counter}, TRIM(\'{rfc}\'), TRIM(\'{beneficiary_name}\'), TRIM(\'{net}\'));"
                            counter = counter + 1
                            cur.execute(current_query)
                    
                    
                        con.commit()
                        current_query = f"SELECT {','.join(additional_alimony_payment_header)} FROM {additional_alimony_payment_table_name};"
                        cur.execute(current_query)
                        lite_additional_alimony_payment_table_data_list = cur.fetchall()
                        lite_additional_alimony_payment_table_data_dict = {}
                        for lite_additional_alimony_payment_table_data_list_element in lite_additional_alimony_payment_table_data_list:
                            if lite_additional_alimony_payment_table_data_list_element[0] not in lite_additional_alimony_payment_table_data_dict:
                                lite_additional_alimony_payment_table_data_dict[lite_additional_alimony_payment_table_data_list_element[0]] = [(lite_additional_alimony_payment_table_data_list_element[1], lite_additional_alimony_payment_table_data_list_element[2],)]
                            else:
                                lite_additional_alimony_payment_table_data_dict[lite_additional_alimony_payment_table_data_list_element[0]].append((lite_additional_alimony_payment_table_data_list_element[1], lite_additional_alimony_payment_table_data_list_element[2],))


                        ######################################################################
                        ######################################################################
                        # Postgres employee perception dict, for additional alimony payments
                        #current_query = f'SELECT eh.rfc, fpe.id FROM employee_payroll_file fpe INNER JOIN hr_employee eh ON fpe.employee_id = eh.id WHERE fpe.payroll_processing_id = {self.payroll_process_id.id}'

                        current_query = f"""
                            SELECT eh.rfc, lpp.payroll_id, pr.name, lpp.partner_id FROM pension_payment_line lpp 
                            INNER JOIN employee_payroll_file fpe ON lpp.payroll_id = fpe.id
                            INNER JOIN hr_employee eh on fpe.employee_id  = eh.id
                            INNER JOIN res_partner pr on lpp.partner_id = pr.id
                            WHERE fpe.payroll_processing_id = {self.payroll_process_id.id}
                            group by eh.rfc, pr.name, lpp.partner_id, lpp.payroll_id
                        """

                        self.env.cr.execute(current_query)
                        postgres_employee_rfc_alimony_payment_list = self.env.cr.fetchall()
                        postgres_employee_rfc_employee_payroll_file_with_alimony_payment_dict = {}
                        for postgres_employee_rfc_alimony_payment_list_element in postgres_employee_rfc_alimony_payment_list:
                            if postgres_employee_rfc_alimony_payment_list_element[0] in postgres_employee_rfc_employee_payroll_file_with_alimony_payment_dict:
                                postgres_employee_rfc_employee_payroll_file_with_alimony_payment_dict[postgres_employee_rfc_alimony_payment_list_element[0]]['beneficiary'].update({ postgres_employee_rfc_alimony_payment_list_element[2] : postgres_employee_rfc_alimony_payment_list_element[3]})
                            else:
                                postgres_employee_rfc_employee_payroll_file_with_alimony_payment_dict[postgres_employee_rfc_alimony_payment_list_element[0]] = { 'employee_payroll_file_id' : postgres_employee_rfc_alimony_payment_list_element[1], 'beneficiary' : { postgres_employee_rfc_alimony_payment_list_element[2] : postgres_employee_rfc_alimony_payment_list_element[3]} }
                                


                        data_not_found_dict = {
                            'employee' : [],
                            'beneficiary_name' : [],
                        }

                        
                        lite_additional_alimony_payment_table_data_dict_tmp = copy.deepcopy(lite_additional_alimony_payment_table_data_dict)
                        for lite_additional_alimony_payment_table_data_dict_element in lite_additional_alimony_payment_table_data_dict:
                            if lite_additional_alimony_payment_table_data_dict_element not in postgres_employee_rfc_employee_payroll_file_with_alimony_payment_dict:
                                data_not_found_dict['employee'].append([(lite_additional_alimony_payment_table_data_dict_element, name) for name, net in [element for element in lite_additional_alimony_payment_table_data_dict[lite_additional_alimony_payment_table_data_dict_element]]])
                                lite_additional_alimony_payment_table_data_dict_tmp.pop(lite_additional_alimony_payment_table_data_dict_element)

                        for lite_additional_alimony_payment_table_data_dict_tmp_element in lite_additional_alimony_payment_table_data_dict_tmp:
                            for lite_additional_alimony_payment_table_data_dict_tmp_internal in lite_additional_alimony_payment_table_data_dict_tmp[lite_additional_alimony_payment_table_data_dict_tmp_element]:
                                if lite_additional_alimony_payment_table_data_dict_tmp_internal[0] not in postgres_employee_rfc_employee_payroll_file_with_alimony_payment_dict[lite_additional_alimony_payment_table_data_dict_tmp_element]['beneficiary']:
                                    data_not_found_dict['beneficiary_name'].append((lite_additional_alimony_payment_table_data_dict_tmp_element, lite_additional_alimony_payment_table_data_dict_tmp_internal[0]))

                        
                        lite_additional_alimony_payment_table_data_dict_tmp = {}

                        current_query = f'SELECT rfc, SUM(net) FROM {additional_alimony_payment_table_name} GROUP BY rfc ORDER BY rfc;'
                        cur.execute(current_query)
                        lite_employee_rfc_group_list = cur.fetchall()
                        lite_employee_rfc_group_dict = {}

                        for lite_employee_rfc_group_list_element in lite_employee_rfc_group_list:
                            lite_employee_rfc_group_dict[lite_employee_rfc_group_list_element[0]] = {'total_additional_alimony_payment' : lite_employee_rfc_group_list_element[1]}


                        there_is_error = False
                        content = f'###################################################\nDATOS ERRÓNEOS PAGOS DE PENSIÓN ALIMENTICIA ADICIONAL : {datetime.now()}\n##################################################\n\n\n'
                        if len(data_not_found_dict['employee']) > 0:
                            content = content  + f"\n################################################################\nEmpleados sin pago de pensión alimenticia asociado\n################################################################\n{data_not_found_dict['employee']}" + '\n\n\n'
                            there_is_error = True
                        if len(data_not_found_dict['beneficiary_name']) > 0:
                            content = content  + f"\n#######################################################\nBeneficiarios de pensión alimenticia sin pago de pensión alimenticia asociado\n#######################################################\n{data_not_found_dict['beneficiary_name']}" + '\n\n\n'
                            there_is_error = True

                        if not there_is_error:
                            # postgres_employee_rfc_employee_payroll_file_with_alimony_payment_dict
                            current_query = """
                                INSERT INTO "additional_pension_payments_line"
                                ("id", "payroll_id", "partner_id", "amount", 
                                "create_uid", "create_date", "write_uid", "write_date")
                                VALUES
                            """

                            current_query_general_value = """
                                (nextval('additional_pension_payments_line_id_seq'), {}, {}, \'{}\', 
                                {}, (now() at time zone 'UTC'), {}, (now() at time zone 'UTC')),
                            """

                            value_tmp = ''
                            for lite_additional_alimony_payment_table_data_dict_element in lite_additional_alimony_payment_table_data_dict:
                                for lite_additional_alimony_payment_table_data_dict_element_internal in lite_additional_alimony_payment_table_data_dict[lite_additional_alimony_payment_table_data_dict_element]:
                                    value_tmp = value_tmp + current_query_general_value.format(postgres_employee_rfc_employee_payroll_file_with_alimony_payment_dict[lite_additional_alimony_payment_table_data_dict_element]['employee_payroll_file_id'], postgres_employee_rfc_employee_payroll_file_with_alimony_payment_dict[lite_additional_alimony_payment_table_data_dict_element]['beneficiary'][lite_additional_alimony_payment_table_data_dict_element_internal[0]], lite_additional_alimony_payment_table_data_dict_element_internal[1],
                                        self.env.user.id, self.env.user.id)

                            if value_tmp:
                                current_query = current_query + value_tmp.strip().strip(',')
                                self.env.cr.execute(current_query)



                            current_query_general_value = """
                                ({}, {}),
                            """

                            current_query = """
                                UPDATE employee_payroll_file SET 
                                total_additional_alimony_payment = tmp_data.total_additional_alimony_payment
                                FROM ( VALUES {} ) AS tmp_data(id_element, total_additional_alimony_payment) WHERE id = tmp_data.id_element;
                            """

                            value_tmp = ''
                            for lite_employee_rfc_group_dict_element in lite_employee_rfc_group_dict:
                                value_tmp = value_tmp + current_query_general_value.format(postgres_employee_rfc_employee_payroll_file_with_alimony_payment_dict[lite_employee_rfc_group_dict_element]['employee_payroll_file_id'], lite_employee_rfc_group_dict[lite_employee_rfc_group_dict_element]['total_additional_alimony_payment'])

                            if value_tmp:
                                value_tmp = value_tmp.strip().strip(',')
                                current_query = current_query.format(value_tmp)
                                self.env.cr.execute(current_query)
                                self.env.cr.commit()

                            return {
                                'name': 'Importación exitosa',
                                'type': 'ir.actions.act_window',
                                'view_mode': 'form',
                                'res_model': 'payroll.payment.message.wizard',
                                'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Importación exitosa de pagos de pensión alimenticia adicional.'}).id,
                                'target': 'new'
                            }
                        else:
                            self.payroll_process_id.update_failed_file(content, 'additional_pension_payments', 'completed', self.env.user.id, self.filename)
                            return {
                                'name': 'Error en validación',
                                'type': 'ir.actions.act_window',
                                'view_mode': 'form',
                                'res_model': 'payroll.payment.message.wizard',
                                'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Se encontraron errores durante la validación del archivo de pagos de pensión alimenticia adicional, corriga los errores e intente nuevamente.'}).id,
                                'target': 'new'
                            }

                    else:
                        return {
                            'name': 'Error en columnas de archivo',
                            'type': 'ir.actions.act_window',
                            'view_mode': 'form',
                            'res_model': 'payroll.payment.message.wizard',
                            'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Número de columnas del archivo de pagos adicionales de pensión alimenticia, no coincide con las columnas esperadas.'}).id,
                            'target': 'new'
                        }