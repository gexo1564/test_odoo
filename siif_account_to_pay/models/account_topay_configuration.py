# -*- coding: utf-8 -*-

from email.policy import default
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
import logging
from lxml import etree
logger = logging.getLogger(__name__)

ACC_TOPAY_TYPE='account.topay.type'
class AccountToPayType(models.Model):
    _name = ACC_TOPAY_TYPE
    _description = 'Account Payable Type'

    name = fields.Char(string='Name', help='Account Payable Type Name', required=True)
    description = fields.Char(string='Description', help='Account Payable Type Description')
    prefix = fields.Char(string='Prefix', help='Payable Account Prefix')
    journal_id = journal_id = fields.Many2one('account.journal')
    is_papiit = fields.Boolean(string='Is PAPIIT?')
    is_obras = fields.Boolean(string='Is Works?')
    is_fondos = fields.Boolean(string='Is Funds?')
    is_allocated_budget_allowed = fields.Boolean(string='Allocated Budget', help='Account Payable Type is Allocated Budget')
    is_nonrecurring_income_allowed = fields.Boolean(string='Nonrecurring Income', help='Account Payable Type is Nonrecurring Income')
    specific_items_allowed_ids = fields.Many2many(string='Specific Items Allowed', comodel_name='expenditure.item', relation='account_payable_type_expenditure_item_allowed_relation', column1='account_payable_type_id', column2='expenditure_item_id', help='Specific Items Allowed for Account Payable Type')
    item_groups_allowed = fields.Char(string='Item Groups Allowed', help='Item Groups Allowed for Account Payable Type, Numbers separated by coma. ie.: 200,300')
    is_account_allowed = fields.Boolean(string='Accountant Account', help='Account Payable Type Allow Accountant Account')
    allowed_accounts_ids = fields.Many2many(string='Allowed Accounts', comodel_name='account.account', relation='account_payable_type_account_account_associated_relation', column1='account_payable_type_id', column2='account_account_id', help='Accounting Accounts Allowed for Account Payable Type')
    account_for_balance_on_account_payable_id = fields.Many2one(string='Account for Balance on Account Payable', comodel_name='account.account', help='Account for Balance on Account Payable')
    is_capitalized = fields.Boolean(string='Capitalized', help='Capitalized Account Payable Type')
    recording_accounting_period_start = fields.Date(string='Period Start', help='Recording Accounting Period Start for Account Payable Type')
    recording_accounting_period_end = fields.Date(string='Period End', help='Recording Accounting Period End for Account Payable Type')
    is_active = fields.Boolean(string='Active', help='Account Payable Type is Active/Deactive', default=True)

    allowed_users_cxp = fields.Many2many('res.users',  'permissions_account_to_type_rel', string ='Allowed users')

    sequence_id = fields.Many2one('ir.sequence', string='Entry Sequence', required=True, copy=False)

    #Constrain para verificar que el nombre sea único
    @api.constrains('name')
    def check_name(self):
        if self.env[ACC_TOPAY_TYPE].search_count([('name','=',self.name)])>1:
            raise ValidationError(_('The name must be unique.'))

    @api.constrains('prefix')
    def check_prefix(self):
        if self.env[ACC_TOPAY_TYPE].search_count([('prefix','=',self.prefix)])>1:
            raise ValidationError(_('The prefix must be unique.'))

    @api.onchange('item_groups_allowed')
    def _onchange_item_groups_allowed(self):
        for record in self:
            if record.item_groups_allowed:
                record.item_groups_allowed = record.item_groups_allowed.replace(' ', '').strip(',')
                item_groups = record.item_groups_allowed.split(',')
                for item_group in item_groups:
                    if not item_group.isnumeric() or int(item_group) % 100 != 0:
                        raise ValidationError(_('Item groups specified are not groups, please verify them. Groups example: 200,300'))

    @api.onchange('is_papiit')
    def _onchange_is_papiit(self):
        for record in self:
            if record.is_papiit:
                record.is_fondos = record.is_obras = False

    @api.onchange('is_fondos')
    def _onchange_is_fondos(self):
        for record in self:
            if record.is_fondos:
                record.is_papiit = record.is_obras = False

    @api.onchange('is_obras')
    def _onchange_is_obras(self):
        for record in self:
            if record.is_obras:
                record.is_papiit = record.is_fondos = False

    def name_get(self):
        return [(r.id, r.name) for r in self]

    @api.model
    def _create_sequence(self, vals, refund=False):
        """ Create new no_gap entry sequence for every new Journal"""
        prefix = vals.get('prefix', 'CXP')
        seq = {
            'name': _('%s Sequence') % prefix,
            'implementation': 'no_gap',
            'prefix': prefix + '/%(range_year)s/',
            'padding': 4,
            'number_increment': 1,
            'use_date_range': True,
        }
        if 'company_id' in vals:
            seq['company_id'] = vals['company_id']
        seq = self.env['ir.sequence'].create(seq)
        seq_date_range = seq._get_current_sequence()
        seq_date_range.number_next = vals.get('sequence_number_next', 1)
        return seq

    @api.model
    def create(self, vals):
        if not vals.get('prefix', None):
            raise ValidationError(_("The account payable prefix is required"))
        vals.update({'sequence_id': self.sudo()._create_sequence(vals).id})
        res = super(AccountToPayType, self).create(vals)
        return res

    def write(self, vals):
        prefix = vals.get('prefix', None)
        res = super(AccountToPayType, self).write(vals)
        if prefix:
            self.sequence_id.sudo().write({'prefix': prefix + '/%(range_year)s/'})
        return res

    def unlink(self):
        for rec in self:
            if self.env['account.topay'].sudo().search([('type_provision.id', '=', rec.id)]):
                raise UserError(_('You cannot delete payables types that are already associated with an account payable.'))
        return super(AccountToPayType, self).unlink()

    #Segmento para asignar un Tipo de cuenta PAPIIT
    @api.onchange('is_papiit')
    def _is_papiit(self):
        info_papiit =  self.env[ACC_TOPAY_TYPE].search_count([('name','=', 'PAPIIT')])
        if self.is_papiit:
            if info_papiit == 0:
                self.name='PAPIIT'
                self.prefix='PAPIIT'
            else:
                self.name=''
                self.prefix=''
        else:
            self.name=''
            self.prefix=''




class RejectionReason(models.Model):
    _name = 'rejection.reason'
    _description = 'Rejection Reason'

    name = fields.Char(string='Name', help='Rejection Reason Name', size=100, required=True)
    description = fields.Char(string='Description', help='Rejection Reason Description')