from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
from datetime import datetime
import pytz
import logging

# Define constant
INCOME_ADEQUACIES = 'income.adequacies'
INCOME_ADEQUACIES_LINES = 'income.adequacies.lines'
INCOME_BUDGETS = 'siif.budget.mgmt.income.budgets'
IR_SEQUENCE = 'ir.sequence'
ACCOUNT_FISCAL_YEAR = 'account.fiscal.year'
INCOME_BUDGETS_LINES = 'siif.budget.mgmt.income.budgets.lines'
INCOME_ADEQUACIES_FUNCTION = 'income.adequacies.function'

DATE_FORMAT = '%Y-%m-%d'
  
class IncomeAdequacies(models.Model):
    _name = 'income.adequacies'
    _description = 'Adecuaciones de Ingresos'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'folio'
    _order = "create_date desc"

    # Campos Kanban
    priority = fields.Integer('Priority', compute='_compute_priority', store=True)
    kanban_state = fields.Selection([
                                    ('normal', 'In Progress'),
                                    ('blocked', 'Blocked'),
                                    ('done', 'Ready for next stage')],
                                    'Kanban State', default='normal')
    color = fields.Integer('Color Index')
  
    folio = fields.Char(string='Folio', tracking=True)
    adequacies_lines_ids = fields.One2many(
        INCOME_ADEQUACIES_LINES, 'adequacies_id', string='Lineas de adecuación', tracking=True)
    sequence_tree = fields.Integer(string = 'Sequence', store = False)
    budget_id = fields.Many2one(INCOME_BUDGETS, required=True, string='Presupuesto', domain=lambda self: [('fiscal_year', '=', self.fiscal_year.id), ('state', '=', 'validated')] , tracking=True)
    journal_id = fields.Many2one('account.journal', string="Diario", readonly = True)
    journal_search = fields.Char(string="Diario relacionado", store=False)

    fiscal_year = fields.Many2one(comodel_name = "account.fiscal.year", string = "Año Fiscal", required=True, tracking=True)
    date_adequacie = fields.Date(string='Fecha', required=True, tracking=True)
    type = fields.Selection(
        [('adequacy','Adecuación'),('recandelize','Recalendarización')], default='adequacy', string='Tipo de movimiento', required=True, tracking=True)
    adequacie_type = fields.Selection(
        [('compensated', 'Compensación'), ('liquid', 'Liquidez')], default='compensated', string='Tipo de adecuación', required=True, tracking=True)
    origin_of_the_adequacy = fields.Selection(
        [('manual', 'Manual'), ('automatic', 'Automatico'),
        ], default='manual', string='Origen de adecuación', tracking=True)
    record_number = fields.Integer(string='Número de registros', compute='_get_count', tracking=True)
    imported_record_number = fields.Integer(string='Número de registros importados', compute='_get_count', tracking=True)
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('confirmed', 'Confirmado'),
        ('rejected', 'Rechazado'),
        ('accepted', 'Aceptado')
    ],default='draft', required=True, string='Estado', tracking=True)
    observation = fields.Text(string='Justificación del movimiento', tracking=True, required=True)
    rejection_reason = fields.Text(string='Motivo de rechazo', tracking=True)
    rejection_date = fields.Date(string='Fecha de rechazo', default=datetime.today(), tracking=True) 
    policy_description = fields.Char(string='Poliza Descripción', tracking=True)
    documentary_support = fields.Many2many("ir.attachment", relation="income_adequacies_documentary_rel", column1="document_id", column2="attachment_id", string="Agregar Soporte Documental")

    # Total increased and decreased fields
    total_decreased = fields.Float(
        string='Total Decreased Amount', compute="_compute_total_amounts", tracking=True)
    total_increased = fields.Float(
        string='Total Decreased Amount', compute="_compute_total_amounts", tracking=True)
    
    # Secuencia
    year_sequence = fields.Many2one(IR_SEQUENCE, string='Secuencia por Año')

    _sql_constraints = [
        ('folio_uniq', 'unique(folio)', 'El folio debe ser único')]

    # Estado de importación
    validation_status = fields.Boolean(string = "Estado de Validación")
    import_file = fields.Char(string = "Archivo Importado")
    total_rows = fields.Integer(string="Total Líneas", readonly = True, store = True)
    success_rows = fields.Integer(string='Total Líneas Exitosas', readonly = True, store=True, compute="_compute_success_rows")
    success_row_ids = fields.Text(string='Líneas Exitosas Ids', default="[]", copy=False)
    failed_rows = fields.Integer(string='Total Líneas Erróneas', readonly = True, store = True)
    budget_file = fields.Binary(string='Archivo Importado')
    filename = fields.Char(string='File name')
    failed_row_file = fields.Binary(string = "Archivo de Líneas Erróneas", store=True)
    failed_row_filename = fields.Char(string='File name', compute='_compute_failed_row_filename')

    # Lineas Asientos Contables
    
    line_accounting_entries = fields.One2many('account.move.line', 'income_adequacies_id', 'Asientos Contables')

    @api.depends('state')
    def _compute_priority(self):
        priority_order = {
            'draft': 1,
            'confirmed': 2,
            'rejected': 3,
            'accepted': 4
        }
        for record in self:
            record.priority = priority_order.get(record.state, 99)

    # Folio adecuación / recalendarización
    @api.model
    def create(self, vals):
        res = super(IncomeAdequacies, self).create(vals)
        # Extrae el nombre del año fiscal
        year_fiscal = self.env[ACCOUNT_FISCAL_YEAR].search([('id', '=', vals['fiscal_year'])])
        year = year_fiscal.name
        # Declara una variable vacia para guardar la secuencia del año fiscal
        fiscal_year = ''
        # Objeto de la tabla secuencias
        year_sequence = self.env[IR_SEQUENCE]
        # Verifica que haya un año fiscal seleccionado
        if year:
            # Busca que el nombre coincida con el correspondiente del año fiscal en la tabla de secuencias
            existing_sequence = self.env[IR_SEQUENCE].search([('name', 'like', f'Secuencia Folio de Adecuaciones de Ingresos {year}')])
            # Si existe asigna la secuencia a la variable fiscal_year
            if existing_sequence:
                # Obtiene el campo code de la secuencia existente
                fiscal_year = existing_sequence[0].code
            # Si no existe crea la nueva secuencia para el año fiscal seleccionado
            else:
                new_sequence = year_sequence.create({
                    'name': f'Secuencia Folio de Adecuaciones de Ingresos {year}',
                    'code': f'seq.income.adequacies.folio.{year}',
                    'padding': 6,
                    'implementation': 'no_gap',
                })
                # Obtiene el campo code de la secuencia creada
                fiscal_year = new_sequence.code
        # Se obtiene el siguiente número de la secuencia
        seq = self.env[IR_SEQUENCE].next_by_code(fiscal_year)
        if res.type == 'adequacy':
            res.folio = "AD" + "/" + str(year) + "/" + str(seq)
        elif res.type == 'recandelize':
            res.folio = "RE" + "/" + str(year) + "/" + str(seq)
        return res
    
    @api.onchange('fiscal_year')
    def onchange_fiscal_year(self):
        # Limpiar campos
        self.budget_id = False
        self.date_adequacie = False
        self.observation = False
        self.adequacies_lines_ids = False
        values = {}
        if self.fiscal_year:
            budgets = self.env[INCOME_BUDGETS].search([('fiscal_year', '=', self.fiscal_year.id), ('state', '=', 'validated')])
            if not budgets:
                warning = {
                    'title': _('No hay presupuestos validados'),
                   'message': _('No se han encontrado presupuestos validados para el año fiscal seleccionado'),
                }
                values.update({'warning': warning, 'domain': {'budget_id': [('id', 'in', budgets.ids)]}})
            else:
                values.update({'domain': {'budget_id': [('id', 'in', budgets.ids)]}})
        if not self.fiscal_year:
            budgets = self.env[INCOME_BUDGETS].search([('fiscal_year', '=', 'null')])
            values.update({'domain': {'budget_id': [('id', 'in', budgets.ids)]}})
        return values
       
    # Si el tipo de movimiento es recalendarización, el tipo de adecuación debe ser compensada
    @api.onchange('type')
    @api.constrains('type')
    def type_recandelize(self):
        if self.type == 'recandelize':
            self.adequacie_type = 'compensated'

    @api.onchange('adequacie_type')
    def onchange_type_adequacie(self):
        if self.adequacie_type == 'compensated':
            var = 'Presupuestos/D00002'
        elif self.adequacie_type == 'liquid':
            var = 'Presupuestos/D00003'
        self.search_journal(var)

    # Verificar que fea sea mayor o igual al inicio del ejercicio fiscal y menor o igual a la fecha actual
    @api.constrains('date_adequacie')
    def check_date_adequacie(self):
        if self.date_adequacie and self.fiscal_year:
            selected_date_adequacie = self.date_adequacie.strftime(DATE_FORMAT)
            tz = pytz.timezone('America/Mexico_City')
            today = datetime.now(tz).strftime(DATE_FORMAT)
            fiscal_year_start = self.fiscal_year.date_from.strftime(DATE_FORMAT)
            fiscal_year_end = self.fiscal_year.date_to.strftime(DATE_FORMAT)
            if selected_date_adequacie > today or selected_date_adequacie < fiscal_year_start or selected_date_adequacie > fiscal_year_end:
                raise ValidationError("La fecha de la adecuación debe estar dentro del rango del año fiscal actual y no debe ser mayor a la fecha actual")
        if not self.fiscal_year:
            raise ValidationError("Falta seleccionar el Año fiscal")

    def action_draft(self):
        self.ensure_one()
        self.state = 'draft'

    def action_confirmed(self):
        self.ensure_one()
        if self.failed_row_file:
            raise ValidationError(_("Incorrect lines in the adequacy load, check the import status tab."))

        if self.type == 'recandelize':
            self._verify_recandelize_cri()

        self._verify_cri_exist_in_income_budget()
        self._verify_income_adequacies_lines()        
        self.check_total_amounts()
        # If all validations are correct, the state is changed to confirmed
        self.state = 'confirmed'

    # Verify that the recalendarization has the same CRI
    def _verify_recandelize_cri(self):
        adequacies_lines = self.env[INCOME_ADEQUACIES_LINES].search([('adequacies_id', '=', self.id)])
        cri_list = []
        for recadelize in adequacies_lines:
            cri_list.append(recadelize.cri_id.id)
        if len(set(cri_list)) != 1:
            raise ValidationError(_("CRI must be the same for all lines to recalendarize."))

    # Verify that the CRI exists in the income budget
    def _verify_cri_exist_in_income_budget(self):
        adequacies_lines = self.env[INCOME_ADEQUACIES_LINES].search([('adequacies_id', '=', self.id)])
        selected_budget = self.budget_id.id
        for adequacy in adequacies_lines:
            cri_line_id = adequacy.cri_id.id
            search_budget_line = self.env[INCOME_BUDGETS_LINES].search([('my_id', '=', selected_budget), ('estimated_cri_id', '=', cri_line_id)])
            if not search_budget_line:
                raise ValidationError(_("CRI %s not found in selected budget.") % (adequacy.cri_id.name))

    # Verify that the income adequacies lines are correct
    def _verify_income_adequacies_lines(self):
        adequacies_lines = self.env[INCOME_ADEQUACIES_LINES].search([('adequacies_id', '=', self.id)])
        for adequacy in adequacies_lines:           
            if not adequacy.line_type:
                raise ValidationError(_("Missing movement type for line with CRI %s.") % (adequacy.cri_id.name))
            if adequacy.line_type == 'aumento' and not adequacy.month_to:
                raise ValidationError(_("Missing destination month for line with CRI %s.") % (adequacy.cri_id.name))
            if adequacy.line_type == 'aumento' and adequacy.increase_line <= 0:
                raise ValidationError(_("Value must be greater than 0 in increase line with CRI %s.") % (adequacy.cri_id.name))
            if adequacy.line_type == 'disminucion' and not adequacy.month_from:
                raise ValidationError(_("Missing origin month for line with CRI %s.") % (adequacy.cri_id.name))
            if adequacy.line_type == 'disminucion' and adequacy.decrease_line <= 0:
                raise ValidationError(_("Value must be greater than 0 in decrease line with CRI %s.") % (adequacy.cri_id.name))
    
    def action_accepted(self):
        self.ensure_one()
        if self.rejection_reason:
            raise ValidationError("El campo motivo de rechazo no esta vacio")
        else:
            self.state = 'accepted'
            self.validation_status = True
            income_adequacy_id = self.id
            income_budget_id = self.budget_id.id
            type_adequacy = self.type
            origin_type = 'adequacy'
            self.env[INCOME_ADEQUACIES_FUNCTION].search([]).income_adequacies_function(income_adequacy_id,income_budget_id,type_adequacy,origin_type)
        # Si el tipo de movimiento es adecuación, se genera el apunte contable
        if self.type == 'adequacy':
            if self.adequacie_type == 'compensated':
               var = 'Presupuestos/D00002'
            elif self.adequacie_type == 'liquid':
               var = 'Presupuestos/D00003'
            self.search_journal(var)
            self.accounting_entries()  
    
    def action_rejected(self):
        self.ensure_one()
        if not self.rejection_reason:
            raise ValidationError("Se debe colocar un motivo de rechazo")
        else:
            self.state = 'rejected'

    # Function calculate total imported and manual rows
    def _get_count(self):
        for record in self:
            record.record_number = len(record.adequacies_lines_ids)
            record.imported_record_number = len(
                record.adequacies_lines_ids.filtered(lambda l: l.imported == True))
            
    # Function to calculate total increase amount and total decrease amount
    def _compute_total_amounts(self):
        for adequacies in self:
            total_decreased = 0
            total_increased = 0
            for line in adequacies.adequacies_lines_ids:
                if line.line_type == 'disminucion':
                    total_decreased += line.decrease_line
                if line.line_type == 'aumento':
                    total_increased += line.increase_line
            adequacies.total_decreased = float(total_decreased)
            adequacies.total_increased = float(total_increased)

    # Función para validar que el total de disminuciones sea igual al total de aumentos cuando el tipo de movimiento es compensado
    def check_total_amounts(self):
        # Si el tipo de movimiento es compensado, se valida que el total de disminuciones sea igual al total de aumentos
        if self.adequacie_type == 'compensated':
            if self.total_decreased != self.total_increased:
                raise ValidationError("El total de disminuciones debe ser igual al total de aumentos")
            if self.total_decreased == 0 and self.total_increased == 0:
                raise ValidationError("El total de disminuciones o aumentos debe ser mayor a cero")
        elif self.adequacie_type == 'liquid':
            if self.total_decreased == 0 and self.total_increased == 0:
                raise ValidationError("El total de disminuciones o aumentos debe ser mayor a cero")

    def import_lines_button(self):
        return {
            'name': _('Importar Líneas'),
            'type': 'ir.actions.act_window',
            'res_model': 'import.income.adequacies.line',
            'view_mode': 'form',
            'view_type': 'form',
            'views': [(False, 'form')],
            'target': 'new',
            'context': {
                'default_father_adequacy' : self.id,
            },
            }

    # Asientos contables para adecuaciones
    def accounting_entries(self):
        move_obj = self.env['account.move']
        user = self.env.user
        company_id = user.company_id.id
        partner_id = user.partner_id.id
        date = datetime.today().date() 
        unam_move_val = {}
        self._verify_journal_exists(self.journal_id.id)

        # Income adequacies accounting entries for compensated
        if self.adequacie_type == 'compensated':
            for lines in self.adequacies_lines_ids:
                if lines.line_type == 'aumento':
                    income_adequacy_line = {
                        'id' : self.id,
                        'folio' :self.folio,
                        'amount' : lines[0].increase_line,
                        'cri': lines.cri_id.name,
                        'policy_description': self.policy_description
                    }
                    journal = {
                        'id': self.journal_id.id,
                        'income_run_debit_account_id': self.journal_id.income_run_debit_account_id.id,
                        'income_run_credit_account_id': self.journal_id.income_run_credit_account_id.id,
                        'conac_income_run_debit_account_id': self.journal_id.conac_income_run_debit_account_id.id,
                        'conac_income_run_credit_account_id': self.journal_id.conac_income_run_credit_account_id.id
                    }                   
                    unam_move_val = self._set_increase_income_adequacies_accounting_entries(income_adequacy_line, journal, company_id, partner_id, date)

                elif lines.line_type == 'disminucion':
                    income_adequacy_line = {
                        'id' : self.id,
                        'folio' :self.folio,
                        'amount' : lines[0].decrease_line,
                        'cri': lines.cri_id.name,
                        'policy_description': self.policy_description
                    }
                    journal = {
                        'id': self.journal_id.id,
                        'income_run_debit_account_id': self.journal_id.income_run_debit_account_id.id,
                        'income_run_credit_account_id': self.journal_id.income_run_credit_account_id.id,
                        'conac_income_run_debit_account_id': self.journal_id.conac_income_run_debit_account_id.id,
                        'conac_income_run_credit_account_id': self.journal_id.conac_income_run_credit_account_id.id
                    }                   
                    unam_move_val = self._set_decrease_income_adequacies_accounting_entries(income_adequacy_line, journal, company_id, partner_id, date)
                    
                unam_move = move_obj.create(unam_move_val)
                unam_move.action_post()

        # Income adequacies accounting entries for liquid
        elif self.adequacie_type == 'liquid':
            for lines in self.adequacies_lines_ids:
                if lines.line_type == 'aumento':
                    income_adequacy_line = {
                        'id' : self.id,
                        'folio' :self.folio,
                        'amount' : lines.increase_line,
                        'cri': lines.cri_id.name,
                        'policy_description': False
                    }
                    journal = {
                        'id': self.journal_id.id,
                        'income_run_debit_account_id': self.journal_id.income_run_debit_account_id.id,
                        'income_run_credit_account_id': self.journal_id.income_run_credit_account_id.id,
                        'conac_income_run_debit_account_id': self.journal_id.conac_income_run_debit_account_id.id,
                        'conac_income_run_credit_account_id': self.journal_id.conac_income_run_credit_account_id.id
                    }                
                    unam_move_val = self._set_increase_income_adequacies_accounting_entries(income_adequacy_line, journal, company_id, partner_id, date)

                elif lines.line_type == 'disminucion':
                    income_adequacy_line = {
                        'id' : self.id,
                        'folio' :self.folio,
                        'amount' : lines.decrease_line,
                        'cri': lines.cri_id.name,
                        'policy_description': False
                    }
                    journal = {
                        'id': self.journal_id.id,
                        'income_run_debit_account_id': self.journal_id.income_run_debit_account_id.id,
                        'income_run_credit_account_id': self.journal_id.income_run_credit_account_id.id,
                        'conac_income_run_debit_account_id': self.journal_id.conac_income_run_debit_account_id.id,
                        'conac_income_run_credit_account_id': self.journal_id.conac_income_run_credit_account_id.id
                    }                   
                    unam_move_val = self._set_decrease_income_adequacies_accounting_entries(income_adequacy_line, journal, company_id, partner_id, date) 

                unam_move = move_obj.create(unam_move_val)
                unam_move.action_post()   

    def _verify_journal_exists(self, journal_id):
        if not journal_id:
            raise ValueError(_("Journal not found, please contact the administrator to configure it"))

    def _verify_journal_accounts_configured(self, journal):
        # Verify that the journal has the accounts configured
        if not journal['income_run_debit_account_id'] or not journal['income_run_credit_account_id'] \
            or not journal['conac_income_run_debit_account_id'] or not journal['conac_income_run_credit_account_id']:
            raise ValidationError(_("Please configure UNAM and CONAC account in budget journal"))

    def _set_increase_income_adequacies_accounting_entries(self, income_adequacy_line, journal, company_id, partner_id, date):
        self._verify_journal_accounts_configured(journal)
        
        unam_move_val = {
            'ref': income_adequacy_line['folio'], 'income_adequacies_id': income_adequacy_line['id'], 'conac_move': True, 'type': 'entry',
            'date': date, 'journal_id': journal['id'], 'company_id': company_id,
            'line_ids': [(0, 0, {
                'account_id': journal['income_run_debit_account_id'],
                'coa_conac_id': journal['conac_income_run_debit_account_id'],
                'credit': income_adequacy_line['amount'],
                'partner_id': partner_id,
                'income_adequacies_id': income_adequacy_line['id'],
                'cri': income_adequacy_line['cri'],
                'name': income_adequacy_line['policy_description']
            }), 
            (0, 0, {
                'account_id': journal['income_run_credit_account_id'],
                'coa_conac_id': journal['conac_income_run_credit_account_id'],
                'debit': income_adequacy_line['amount'],
                'partner_id': partner_id,
                'income_adequacies_id':income_adequacy_line['id'],
                'cri': income_adequacy_line['cri'],
                'name': income_adequacy_line['policy_description']
            })]}
        return unam_move_val

    def _set_decrease_income_adequacies_accounting_entries(self, income_adequacy_line, journal, company_id, partner_id, date):
        self._verify_journal_accounts_configured(journal)
        
        unam_move_val = {
            'ref': income_adequacy_line['folio'], 'income_adequacies_id': income_adequacy_line['id'], 'conac_move': True, 'type': 'entry',
            'date': date, 'journal_id': journal['id'], 'company_id': company_id,
            'line_ids': [(0, 0, {
                'account_id': journal['income_run_debit_account_id'],
                'coa_conac_id': journal['conac_income_run_debit_account_id'],
                'debit': income_adequacy_line['amount'],
                'partner_id': partner_id,
                'income_adequacies_id': income_adequacy_line['id'],
                'cri': income_adequacy_line['cri'],
                'name': income_adequacy_line['policy_description']
            }), 
            (0, 0, {
                'account_id': journal['income_run_credit_account_id'],
                'coa_conac_id': journal['conac_income_run_credit_account_id'],
                'credit': income_adequacy_line['amount'],
                'partner_id': partner_id,
                'income_adequacies_id':income_adequacy_line['id'],
                'cri': income_adequacy_line['cri'],
                'name': income_adequacy_line['policy_description']
            })]}
        return unam_move_val

    # Definir nombre para el archivo de errores
    def _compute_failed_row_filename(self):
        for record in self:
            tz = pytz.timezone('America/Mexico_City')
            filename = f'Registros_erroneos_{record.folio}_{datetime.now(tz).strftime("%Y-%m-%d_%H:%M:%S")}.txt'
            record.failed_row_filename = filename

    # Verificar que no se elimine la adecuación si esta es estado Aceptado
    def unlink(self):
        if self.state == 'accepted':
            raise ValidationError(_(f" No se puede eliminar la adecuación {self.folio} porque está en estado ACEPTADO"))
        res = super(IncomeAdequacies, self).unlink()
        return res
    
    #- Método que modifica el css de la vista en Presupuesto de Ingresos
    x_css = fields.Html(string='CSS', sanitize=False, compute='_compute_css', store=False)
    @api.depends('state')
    def _compute_css(self):
        is_admin_user = self.env.user.has_group('base.group_user')
        is_adequacies_administrator = self.env.user.has_group('siif_budget_mgmt.group_budget_pi_adequacies_administrator')
        is_registration_adequacies = self.env.user.has_group('siif_budget_mgmt.group_budget_pi_registration_adequacies')
        is_confirmation_adequacies = self.env.user.has_group('siif_budget_mgmt.group_budget_pi_confirmation_adequacies')
        is_validation_adequacies = self.env.user.has_group('siif_budget_mgmt.group_budget_pi_validation_adequacies')

        for record in self:
            record.x_css = ""

            if is_admin_user:
                #Ocultar boton de editar en Adecuaciones de Ingresos en los status siguientes
                if record.state in ['accepted', 'rejected']:
                    record.x_css += """
                        <style>
                            .o_form_view .o_form_statusbar > .o_statusbar_status {
                                max-width:85%;
                            }
                            .o_form_button_edit {
                                display: none !important;
                            }
                        </style>
                    """

            if is_adequacies_administrator:
                #Ocultar boton de editar en Adecuaciones de Ingresos en los status siguientes
                if record.state in ['accepted', 'rejected']:
                    record.x_css += """
                        <style>
                            .o_form_view .o_form_statusbar > .o_statusbar_status {
                                max-width:85%;
                            }
                            .o_form_button_edit {
                                display: none !important;
                            }
                        </style>
                    """
            
            if is_registration_adequacies:
                #Ocultar boton de editar en Adecuaciones de Ingresos en los status siguientes
                if record.state in ['confirmed', 'accepted', 'rejected']:
                    record.x_css += """
                        <style>
                            .o_form_view .o_form_statusbar > .o_statusbar_status {
                                max-width:85%;
                            }
                            .o_form_button_edit {
                                display: none !important;
                            }
                        </style>
                    """
            
            if is_confirmation_adequacies:
                #Ocultar boton de editar en Adecuaciones de Ingresos en los status siguientes
                if record.state in ['draft', 'confirmed', 'accepted', 'rejected']:
                    record.x_css += """
                        <style>
                            .o_form_view .o_form_statusbar > .o_statusbar_status {
                                max-width:85%;
                            }
                            .o_form_button_edit {
                                display: none !important;
                            }
                        </style>
                    """
            
            if is_validation_adequacies:
                #Ocultar boton de editar en Adecuaciones de Ingresos en los status siguientes
                if record.state in ['draft', 'accepted', 'rejected']:
                    record.x_css += """
                        <style>
                            .o_form_view .o_form_statusbar > .o_statusbar_status {
                                max-width:85%;
                            }
                            .o_form_button_edit {
                                display: none !important;
                            }
                        </style>
                    """
                else:
                    record.x_css += """
                        <style>
                            .o_form_view .o_form_statusbar > .o_statusbar_status{
                                max-width:85%;
                            }
                        </style>
                    """

    def search_journal(self,type):
        search = self.env['siif.budget.mgmt.relation.journal.process'].search([('name', '=', type)])
        if not search:
            raise ValidationError(_(f" No se encontró el diario {type} solicite al administrador que lo modifique"))
        self.journal_search = search.journal_name
        self.journal_id = search.journal_name
                        
    # sobreescritura de la vista en la parte de los asientos contables
    date = fields.Date(string = "Fecha", store=False)
    company_id = fields.Many2one(comodel_name = "res.company", string = "Compañia", store=False)
    move_id = fields.Many2one(comodel_name = "account.move", string = "Asiento Contable", store=False)
    account_id = fields.Many2one(comodel_name = "account.account", string = "Cuenta", store=False)
    cri = fields.Char(string='CRI', store=False)
    debit = fields.Float(string = "Debe", store=False)
    credit = fields.Float(string = "Haber", store=False)

    # Función para rehacer las adecuaciones
    def redo_income_adequacies(self):
        for record in self:
            budget_id = record.budget_id
            # Objetos
            obj_adequacies = self.env[INCOME_ADEQUACIES]
            obj_logbook = self.env['siif.budget.mgmt.accounting.movements.logbook']
            obj_function = self.env[INCOME_ADEQUACIES_FUNCTION]

            # Busca todas las Adecuaciones que tengan estado Validado
            adequacies = obj_adequacies.search([('budget_id', '=', budget_id.id),('folio', '=', record.folio),('state', '=', 'accepted')])
            

            # Envia las adecuaciones una por una a la función que aplica las adecuaciones al presupuesto
            for adequacy in adequacies:
                # Verifica que la Adecuacion no exista en la Bitacora
                cve = obj_logbook.search([('cve', '=', adequacy.folio)])
                if not cve:
                    # Registra la adecuacion en el Presupuesto de ingresos y la Bitacora
                    adequacy_id = adequacy.id
                    budget_id = adequacy.budget_id.id
                    type_adequacy = adequacy.type
                    origin_type = 're-adequacy'
                    obj_function.search([]).income_adequacies_function(adequacy_id,budget_id,type_adequacy,origin_type)

class IncomeAdequaciesLines(models.Model):
    _name = 'income.adequacies.lines'
    _description = 'Income Adequacies Lines'

    adequacies_id = fields.Many2one(INCOME_ADEQUACIES, string='Adecuaciones', ondelete="cascade", index=True)
    cri_id = fields.Many2one('classifier.income.item', string='Clasificador por Rubro de Ingresos')
    imported = fields.Boolean(string = 'Importado', default = False)

    fiscal_year = fields.Many2one(string = 'Año fiscal', related = 'adequacies_id.fiscal_year', store = True)
    budget_id = fields.Many2one(string = 'Presupuesto', related = 'adequacies_id.budget_id', store = True)
    movement_type = fields.Selection(string = 'Tipo de movimiento', related = 'adequacies_id.type', store = True)
    adequacy_type = fields.Selection(string = 'Tipo de adecuación', related = 'adequacies_id.adequacie_type', store = True)
    adequacy_date = fields.Date(string = 'Fecha de adecuación', related = 'adequacies_id.date_adequacie', store = True)
    journal_id = fields.Many2one(string="Diario" , related = 'adequacies_id.journal_id', store = True)

    line_type = fields.Selection([('aumento', 'Aumento'), ('disminucion', 'Disminución')], string='Movimiento')
    month_from = fields.Selection([
        ('enero','Enero'),
        ('febrero','Febrero'),
        ('marzo','Marzo'),
        ('abril','Abril'),
        ('mayo','Mayo'),
        ('junio','Junio'),
        ('julio','Julio'),
        ('agosto','Agosto'),
        ('septiembre','Septiembre'),
        ('octubre','Octubre'),
        ('noviembre','Noviembre'),
        ('diciembre','Diciembre'),
        ],string='Mes origen')
    month_to = fields.Selection([
        ('enero','Enero'),
        ('febrero','Febrero'),
        ('marzo','Marzo'),
        ('abril','Abril'),
        ('mayo','Mayo'),
        ('junio','Junio'),
        ('julio','Julio'),
        ('agosto','Agosto'),
        ('septiembre','Septiembre'),
        ('octubre','Octubre'),
        ('noviembre','Noviembre'),
        ('diciembre','Diciembre'),
        ],string='Mes destino')
    increase_line = fields.Float(string='Aumento')
    decrease_line = fields.Float(string='Disminución')
    creation_type = fields.Selection([
        ('manual', 'Manual'),
        ('imported', 'Importado'),
        ('automatic', 'Automatico'),
        ],string='Tipo de creación',default='manual')
    date_affectation = fields.Date(string='Fecha de afectación')
    amount = fields.Float(string='Importe')

    display_type = fields.Selection([('line_section', "Section"), ('line_note', "Note")], default=False, help="Technical field for UX purpose.", store=False)
    sequence = fields.Integer(string = 'Sequence', store = False)

    # Función que detecta si type_line es aumento o disminucion y coloca en 0 el campo contrario
    @api.onchange('line_type')
    def onchange_line_type(self):
        if self.line_type == 'aumento':
            self.decrease_line = 0
            self.month_from = False
        if self.line_type == 'disminucion':
            self.increase_line = 0
            self.month_to = False

class AffetationIncomeBudget(models.Model):
    _name = 'affectation.income.budget'
    _description = 'Affectation Income Budget'

    # Función para modificar el presupuesto de ingresos
    def affectation_income_budget(self, type_affectation, cri_id, date_accrued, date_affectation, amount):
        logging.info(f"************ TIPO DE AFECTACIÓN 2: {type_affectation}")
        logging.info(f"************ CRI ID 2: {cri_id}")
        logging.info(f"************ FECHA ACCRUED 2: {date_accrued}")
        logging.info(f"************ FECHA DE AFECTACIÓN 2: {date_affectation}")
        logging.info(f"************ AMOUNT 2: {amount}")
        # Verifica si uno o más campos están vacíos
        if not cri_id or not date_affectation or not amount:
            raise ValidationError('Uno o más campos para la afectacion al presupuesto están vacíos')
        # Extrae el año de la fecha
        year = date_affectation.year
        # Extrae el mes de la fecha
        month = date_affectation.month

        # Diccionario mes y nombre del mes
        month_name = {
            1: 'enero',
            2: 'febrero',
            3: 'marzo',
            4: 'abril',
            5: 'mayo',
            6: 'junio',
            7: 'julio',
            8: 'agosto',
            9: 'septiembre',
            10: 'octubre',
            11: 'noviembre',
            12: 'diciembre',
        }   
        # Diccionario meses por ejecutar
        month_to_collect = {
            'enero': 'to_collect_january_field',
            'febrero': 'to_collect_february_field',
            'marzo': 'to_collect_march_field',
            'abril': 'to_collect_april_field',
            'mayo': 'to_collect_may_field',
            'junio': 'to_collect_june_field',
            'julio': 'to_collect_july_field',
            'agosto': 'to_collect_august_field',
            'septiembre': 'to_collect_september_field',
            'octubre': 'to_collect_october_field',
            'noviembre': 'to_collect_november_field',
            'diciembre': 'to_collect_december_field',
        }
        # Diccionario meses devenegados
        month_accrued = {
            'enero': 'accrued_january_field',
            'febrero': 'accrued_february_field',
            'marzo': 'accrued_march_field',
            'abril': 'accrued_april_field',
            'mayo': 'accrued_may_field',
            'junio': 'accrued_june_field',
            'julio': 'accrued_july_field',
            'agosto': 'accrued_august_field',
            'septiembre': 'accrued_september_field',
            'octubre': 'accrued_october_field',
            'noviembre': 'accrued_november_field',
            'diciembre': 'accrued_december_field',
        }
        # Diccionario meses recaudados
        month_collected = {
            'enero': 'collected_january_field',
            'febrero': 'collected_february_field',
            'marzo': 'collected_march_field',
            'abril': 'collected_april_field',
            'mayo': 'collected_may_field',
            'junio': 'collected_june_field',
            'julio': 'collected_july_field',
            'agosto': 'collected_august_field',
            'septiembre': 'collected_september_field',
            'octubre': 'collected_october_field',
            'noviembre': 'collected_november_field',
            'diciembre': 'collected_december_field',
        }
        # Objeto de la tabla año fiscal
        obj_fiscal_year = self.env[ACCOUNT_FISCAL_YEAR]
        # Busca el id del año fiscal
        fiscal_year_id = obj_fiscal_year.search([('name', '=', year)])
        if not fiscal_year_id:
            raise ValidationError(f'No existe el año fiscal {year}')
        else:
            # Objeto de la tabla de presupuesto de ingresos
            obj_income_budgets = self.env[INCOME_BUDGETS]
            budget_validated = obj_income_budgets.search([('fiscal_year', '=', fiscal_year_id.id),('state', '=', 'validated')])
            # Verifica que el año recibido tenga un presupuesto validado
            if budget_validated:
                # Objeto de la tabla de lineas de presupuesto de ingresos
                obj_income_budgets_lines = self.env[INCOME_BUDGETS_LINES]
                # Realiza la busqueda del presupuesto que corresponde
                budget_lines = obj_income_budgets_lines.search([('estimated_fiscal_year', '=', fiscal_year_id.id),('my_id', '=', budget_validated.id)])
                # Verifica que existan lineas de presupuesto
                if not budget_lines:
                    raise ValidationError(f'No se encontraron lineas de presupuesto para el año fiscal {year}')
                else:
                    # Verifica que el CRI exista
                    cri_name = self.env['classifier.income.item'].search([('id', '=', cri_id)])
                    if not cri_name:
                        raise ValidationError(f'El CRI con id {cri_id} no existe')
                    # Extrae la linea de presupuesto que corresponde al CRI seleccionado
                    budget_line_cri = obj_income_budgets_lines.search([('my_id', '=', budget_validated.id),('estimated_cri_id', '=', cri_id)])
                    if not budget_line_cri:
                        ####################################################
                        # If a budget line is not found, create a new one. #
                        ####################################################
                        new_budget_line_vals = {
                            'my_id': budget_validated.id,
                            'estimated_fiscal_year': fiscal_year_id.id,
                            'estimated_from_date': fiscal_year_id.date_from,
                            'estimated_to_date': fiscal_year_id.date_to, 
                            'estimated_cri_id': cri_id,
                        }
                        new_budget_line_cri = obj_income_budgets_lines.create(new_budget_line_vals)
                        ########################################################################
                        # Updates the lines_added_later field in the object obj_income_budgets #
                        ########################################################################
                        obj_income_budgets.browse(budget_validated.id).write({'lines_added_later': budget_validated.lines_added_later + 1})

                        if type_affectation == 'eje-dev': # Suma al Devengado y Resta al Por Ejecutar
                            # Busca el mes seleccionado en el diccionario
                            month_name_selected = month_name.get(month)
                            # Busca el mes por ejecutar seleccionado en el diccionario
                            month_to_collect_selected = month_to_collect.get(month_name_selected)
                            # Busca el mes devengado seleccionado en el diccionario
                            month_accrued_selected = month_accrued.get(month_name_selected)
                            # Suma al Devengado
                            setattr(new_budget_line_cri, month_accrued_selected, getattr(new_budget_line_cri, month_accrued_selected) + amount)
                            new_budget_line_cri.accrued_total += amount
                            # Resta al Por Ejecutar
                            setattr(new_budget_line_cri, month_to_collect_selected, getattr(new_budget_line_cri, month_to_collect_selected) - amount)
                            new_budget_line_cri.to_collect_total -= amount
                        elif type_affectation == 'dev-rec': # Resta al Devengado y Suma al Recaudado
                            # Busca el mes seleccionado en el diccionario
                            month_name_selected = month_name.get(month)
                            # Busca el mes por ejecutar seleccionado en el diccionario
                            if date_accrued:
                                month_accrued_selected = month_accrued.get(month_name.get(date_accrued.month))
                            else:
                                month_accrued_selected = month_accrued.get(month_name_selected)
                            # Busca el mes Recaudado seleccionado en el diccionario
                            month_collected_selected = month_collected.get(month_name_selected)
                            # Resta al Devengado
                            setattr(new_budget_line_cri, month_accrued_selected, getattr(new_budget_line_cri, month_accrued_selected) - amount)
                            new_budget_line_cri.accrued_total -= amount
                            # Suma al Recaudado
                            setattr(new_budget_line_cri, month_collected_selected, getattr(new_budget_line_cri, month_collected_selected) + amount)
                            new_budget_line_cri.collected_total += amount
                        elif type_affectation == 'eje-rec': # Suma al Recaudado y Resta al Por Ejecutar
                            # Busca el mes seleccionado en el diccionario
                            month_name_selected = month_name.get(month)
                            # Busca el mes recaudado seleccionado en el diccionario
                            month_collected_selected = month_collected.get(month_name_selected)
                            # Busca el mes devengado seleccionado en el diccionario
                            month_to_collect_selected = month_to_collect.get(month_name_selected)
                            # Suma al Recaudado
                            setattr(new_budget_line_cri, month_collected_selected, getattr(new_budget_line_cri, month_collected_selected) + amount)
                            new_budget_line_cri.collected_total += amount
                            # Resta al por ejecutar
                            setattr(new_budget_line_cri, month_to_collect_selected, getattr(new_budget_line_cri, month_to_collect_selected) - amount)
                            new_budget_line_cri.to_collect_total -= amount
                    else:

                        if type_affectation == 'eje-dev': # Suma al Devengado y Resta al Por Ejecutar
                            # Busca el mes seleccionado en el diccionario
                            month_name_selected = month_name.get(month)
                            # Busca el mes por ejecutar seleccionado en el diccionario
                            month_to_collect_selected = month_to_collect.get(month_name_selected)
                            # Busca el mes devengado seleccionado en el diccionario
                            month_accrued_selected = month_accrued.get(month_name_selected)
                            # Suma al Devengado
                            setattr(budget_line_cri, month_accrued_selected, getattr(budget_line_cri, month_accrued_selected) + amount)
                            budget_line_cri.accrued_total += amount
                            # Resta al Por Ejecutar
                            setattr(budget_line_cri, month_to_collect_selected, getattr(budget_line_cri, month_to_collect_selected) - amount)
                            budget_line_cri.to_collect_total -= amount
                        elif type_affectation == 'dev-rec': # Resta al Devengado y Suma al Recaudado
                            # Busca el mes seleccionado en el diccionario
                            month_name_selected = month_name.get(month)
                            # Busca el mes por ejecutar seleccionado en el diccionario
                            if date_accrued:
                                month_accrued_selected = month_accrued.get(month_name.get(date_accrued.month))
                            else:
                                month_accrued_selected = month_accrued.get(month_name_selected)
                            # Busca el mes Recaudado seleccionado en el diccionario
                            month_collected_selected = month_collected.get(month_name_selected)
                            # Resta al Devengado
                            setattr(budget_line_cri, month_accrued_selected, getattr(budget_line_cri, month_accrued_selected) - amount)
                            budget_line_cri.accrued_total -= amount
                            # Suma al Recaudado
                            setattr(budget_line_cri, month_collected_selected, getattr(budget_line_cri, month_collected_selected) + amount)
                            budget_line_cri.collected_total += amount
                        elif type_affectation == 'eje-rec': # Suma al Recaudado y Resta al Por Ejecutar
                            # Busca el mes seleccionado en el diccionario
                            month_name_selected = month_name.get(month)
                            # Busca el mes recaudado seleccionado en el diccionario
                            month_collected_selected = month_collected.get(month_name_selected)
                            # Busca el mes devengado seleccionado en el diccionario
                            month_to_collect_selected = month_to_collect.get(month_name_selected)
                            # Suma al Recaudado
                            setattr(budget_line_cri, month_collected_selected, getattr(budget_line_cri, month_collected_selected) + amount)
                            budget_line_cri.collected_total += amount
                            # Resta al por ejecutar
                            setattr(budget_line_cri, month_to_collect_selected, getattr(budget_line_cri, month_to_collect_selected) - amount)
                            budget_line_cri.to_collect_total -= amount

    def associate_cri_account(self,default_credit,default_debit):
        # Objeto de Relación Cuentas - CRI
        obj_cri = self.env['relation.account.cri']
         # Busca si el código de la cuenta contable del diario comienza con 4
        cri_account = obj_cri.search([('cuentas_id', '=', default_credit),('cuentas_id', 'like', '4%')])
        if not cri_account:
            cri_account = obj_cri.search([('cuentas_id', '=', default_debit),('cuentas_id', 'like', '4%')])
        # Retorna CRI asociado a la Cuenta Contable
        return cri_account.cri
    
    def associate_cri_account_code(self,code):
        # Objetos
        obj_account = self.env['account.account']
        obj_cri = self.env['relation.account.cri']
        # Busca el id de la cuenta
        account_id = obj_account.search([('code', '=', code),('code', 'like', '4%')])
        # Busca si el código de la cuenta contable del diario comienza con 4
        cri_account = obj_cri.search([('cuentas_id', '=', account_id.id)],limit=1)
        # Retorna CRI asociado a la Cuenta Contable
        return cri_account.cri
    
# Función separada para las adecuaciones al presupuesto de ingresos
class IncomeAdequaciesFunction(models.Model):
    _name = 'income.adequacies.function'
    _description = 'Income Adequacies Function'

    def _get_dict_month_name(self, month):
        month_name = {
            'enero': 'Enero',
            'febrero': 'Febrero',
            'marzo': 'Marzo',
            'abril': 'Abril',
            'mayo': 'Mayo',
            'junio': 'Junio',
            'julio': 'Julio',
            'agosto': 'Agosto',
            'septiembre': 'Septiembre',
            'octubre': 'Octubre',
            'noviembre': 'Noviembre',
            'diciembre': 'Diciembre',
        }
        return month_name.get(month)
    
    def _get_dict_month_number(self, month):
        month_number = {
            'enero': '1',
            'febrero': '2',
            'marzo': '3',
            'abril': '4',
            'mayo': '5',
            'junio': '6',
            'julio': '7',
            'agosto': '8',
            'septiembre': '9',
            'octubre': '10',
            'noviembre': '11',
            'diciembre': '12',
        }
        return month_number.get(month)
    
    def _get_dict_month_increase(self, month):
        month_increase = {
            'enero': 'increases_january_field',
            'febrero': 'increases_february_field',
            'marzo': 'increases_march_field',
            'abril': 'increases_april_field',
            'mayo': 'increases_may_field',
            'junio': 'increases_june_field',
            'julio': 'increases_july_field',
            'agosto': 'increases_august_field',
            'septiembre': 'increases_september_field',
            'octubre': 'increases_october_field',
            'noviembre': 'increases_november_field',
            'diciembre': 'increases_december_field',
        }
        return month_increase.get(month)
    
    def _get_dict_month_decrease(self, month):
        month_decrease = {
            'enero': 'decreases_january_field',
            'febrero': 'decreases_february_field',
            'marzo': 'decreases_march_field',
            'abril': 'decreases_april_field',
            'mayo': 'decreases_may_field',
            'junio': 'decreases_june_field',
            'julio': 'decreases_july_field',
            'agosto': 'decreases_august_field',
            'septiembre': 'decreases_september_field',
            'octubre': 'decreases_october_field',
            'noviembre': 'decreases_november_field',
            'diciembre': 'decreases_december_field',
        }
        return month_decrease.get(month)
    
    def _get_dict_month_modified(self, month):
        month_modified = {
            'enero': 'modified_january_field',
            'febrero': 'modified_february_field',
            'marzo': 'modified_march_field',
            'abril': 'modified_april_field',
            'mayo': 'modified_may_field',
            'junio': 'modified_june_field',
            'julio': 'modified_july_field',
            'agosto': 'modified_august_field',
            'septiembre': 'modified_september_field',
            'octubre': 'modified_october_field',
            'noviembre': 'modified_november_field',
            'diciembre': 'modified_december_field',
        }
        return month_modified.get(month)
    
    def _get_dict_month_to_collect(self, month):
        month_to_collect = {
            'enero': 'to_collect_january_field',
            'febrero': 'to_collect_february_field',
            'marzo': 'to_collect_march_field',
            'abril': 'to_collect_april_field',
            'mayo': 'to_collect_may_field',
            'junio': 'to_collect_june_field',
            'julio': 'to_collect_july_field',
            'agosto': 'to_collect_august_field',
            'septiembre': 'to_collect_september_field',
            'octubre': 'to_collect_october_field',
            'noviembre': 'to_collect_november_field',
            'diciembre': 'to_collect_december_field',
        }
        return month_to_collect.get(month)
    
    # Verify that the recalendarization has the same CRI
    def _verify_recandelize_cri(self, income_adequacy_id):
        adequacies_lines = self.env[INCOME_ADEQUACIES_LINES].search([('adequacies_id', '=', income_adequacy_id)])
        cri_list = []
        for recadelize in adequacies_lines:
            cri_list.append(recadelize.cri_id.id)
        if len(set(cri_list)) != 1:
            raise ValidationError(_("CRI must be the same for all lines to recalendarize."))
    
    # Function to modify the income budget
    def income_adequacies_function(self,income_adequacy_id,income_budget_id,type_adequacy,origin_type):
        adequacies_lines = self.env[INCOME_ADEQUACIES_LINES].search([('adequacies_id', '=', income_adequacy_id)])

        if type_adequacy == 'recandelize':
            self._verify_recandelize_cri(income_adequacy_id)

        for adequacy_line in adequacies_lines:
            adequacy_line_type = adequacy_line.line_type.upper()
            if adequacy_line_type == 'DISMINUCION':
                self._set_decrease_adequacy_to_income_budget(adequacy_line.id,income_adequacy_id,income_budget_id,type_adequacy,origin_type)
                        
            elif adequacy_line_type == 'AUMENTO':
                self._set_increase_adequacy_to_income_budget(adequacy_line.id,income_adequacy_id,income_budget_id,type_adequacy,origin_type)   

    def _set_increase_adequacy_to_income_budget(self,adequacy_line,income_adequacy_id,income_budget_id,type_adequacy,origin_type):
        adequacy = self.env[INCOME_ADEQUACIES_LINES].search([('id', '=', adequacy_line)])
        adequacy_parent = self.env[INCOME_ADEQUACIES].search([('id', '=', income_adequacy_id)])
        cri_line_id = adequacy.cri_id.id
        budget_line = self.env[INCOME_BUDGETS_LINES].search([('my_id', '=', income_budget_id),('estimated_cri_id', '=', cri_line_id)])

        # Get month to selected in adequacy
        month_to_field_selected = self._get_dict_month_increase(adequacy.month_to)
        month_to_modified_selected = self._get_dict_month_modified(adequacy.month_to)
        month_to_to_collect_selected = self._get_dict_month_to_collect(adequacy.month_to)
        
        if month_to_field_selected:
            '''
                Use the setattr function to set a value in the specified field of an object.
                The first argument of setattr is the object in which the value of the field will be set.
                The second argument is the name of the field that will be updated, 
                field_month_to is a variable that contains the name of the field suitable for the current month.
                The third argument is the value that will be set in the field. 
                In this case, the amount of the adequacy line is added to the current value of the field using getattr 
                to obtain the current value of the field and then add the amount of the adequacy line.
            '''
            # Modify the increase in the budget
            setattr(budget_line, month_to_field_selected, getattr(budget_line, month_to_field_selected) + adequacy.increase_line)
            budget_line.increases_total += adequacy.increase_line
            # Modify the modified budget
            setattr(budget_line, month_to_modified_selected, getattr(budget_line, month_to_modified_selected) + adequacy.increase_line)
            budget_line.modified_total += adequacy.increase_line
            # Modify the budget to collect
            setattr(budget_line, month_to_to_collect_selected, getattr(budget_line, month_to_to_collect_selected) + adequacy.increase_line)
            budget_line.to_collect_total += adequacy.increase_line
            # Save the adequacy logbook
            fiscal_year_id = adequacy.fiscal_year.id
            cri_id = cri_line_id
            proposed_date = adequacy.month_to
            movement = adequacy.line_type
            adequacy_folio = adequacy_parent.folio
            journal_id = adequacy_parent.journal_id.id
            update_date_record = datetime.today()
            amount = adequacy.increase_line
            update_date_income = datetime.today()
            self.env[INCOME_ADEQUACIES_FUNCTION].search([]).logbook_income_budget(fiscal_year_id,cri_id,proposed_date,type_adequacy,movement,origin_type,adequacy_folio,journal_id,update_date_record,amount,update_date_income)       
            month_number_name = self._get_dict_month_number(adequacy.month_to)
            fiscal_year_name = adequacy.fiscal_year.name
            adequacy.date_affectation = fiscal_year_name +'-'+month_number_name+'-01'
            adequacy.amount = adequacy.increase_line

    def _set_decrease_adequacy_to_income_budget(self,adequacy_line,income_adequacy_id,income_budget_id,type_adequacy,origin_type):
        adequacy = self.env[INCOME_ADEQUACIES_LINES].search([('id', '=', adequacy_line)])
        adequacy_parent = self.env[INCOME_ADEQUACIES].search([('id', '=', income_adequacy_id)])
        cri_line_id = adequacy.cri_id.id
        budget_line = self.env[INCOME_BUDGETS_LINES].search([('my_id', '=', income_budget_id),('estimated_cri_id', '=', cri_line_id)])

        # Busca el mes seleccionado en el diccionario
        month_from_field_selected = self._get_dict_month_decrease(adequacy.month_from)
        month_name_selected = self._get_dict_month_name(adequacy.month_from)
        month_modified_selected = self._get_dict_month_modified(adequacy.month_from)
        month_from_to_collect_selected = self._get_dict_month_to_collect(adequacy.month_from)
        if month_from_field_selected and month_name_selected and month_modified_selected and month_from_to_collect_selected:
            if ((adequacy.decrease_line > getattr(budget_line, month_modified_selected)) or (adequacy.decrease_line > getattr(budget_line, month_from_to_collect_selected))):
                raise ValidationError(_(f"El monto {adequacy.decrease_line} que desea disminuir en el mes de {month_name_selected} es mayor al que tiene el presupuesto"))
            else:
                # Modify the decrease in the budget
                setattr(budget_line, month_from_field_selected, getattr(budget_line, month_from_field_selected) + adequacy.decrease_line)
                budget_line.decreases_total += adequacy.decrease_line
                # Modify the modified budget
                setattr(budget_line, month_modified_selected, getattr(budget_line, month_modified_selected) - adequacy.decrease_line)
                budget_line.modified_total -= adequacy.decrease_line
                # Modify the budget to collect
                setattr(budget_line, month_from_to_collect_selected, getattr(budget_line, month_from_to_collect_selected) - adequacy.decrease_line)
                budget_line.to_collect_total -= adequacy.decrease_line
                # Save the adequacy logbook
                fiscal_year_id = adequacy.fiscal_year.id
                cri_id = cri_line_id
                proposed_date = adequacy.month_from
                movement = adequacy.line_type
                adequacy_folio = adequacy_parent.folio
                journal_id = adequacy_parent.journal_id.id
                update_date_record = adequacy.write_date.strftime(DATE_FORMAT)
                amount = adequacy.decrease_line
                update_date_income = budget_line.write_date.strftime(DATE_FORMAT)
                self.env[INCOME_ADEQUACIES_FUNCTION].search([]).logbook_income_budget(fiscal_year_id,cri_id,proposed_date,type_adequacy,movement,origin_type,adequacy_folio,journal_id,update_date_record,amount,update_date_income) 
                month_number_name = self._get_dict_month_number(adequacy.month_from)
                fiscal_year_name = adequacy.fiscal_year.name
                adequacy.date_affectation = fiscal_year_name +'-'+month_number_name+'-01'
                adequacy.amount = adequacy.decrease_line

    # Función para generar bitacora de adecuaciones y afectaciones
    def logbook_income_budget(self,fiscal_year_id,cri_id,proposed_date,type,movement,origin_type,name,journal_id,update_date_record,amount,update_date_income):
        # Objeto de la tabla de bitacora
        obj_logbook = self.env['siif.budget.mgmt.accounting.movements.logbook']
        logbook_line = obj_logbook.search([])
        # Variables para reemplazar por los valores correspondientes
        var_type = ''
        var_movement = ''
        # Objeto año fiscal
        obj_fiscal_year = self.env[ACCOUNT_FISCAL_YEAR]
        fiscal_year_name = obj_fiscal_year.search([('id', '=', fiscal_year_id)])
        # Si es una adecuación o recalendarización, valida y busca los datos
        if type == 'adequacy' or type == 'recandelize':
            if not fiscal_year_id or not movement or not origin_type or not name or not journal_id or not update_date_record or not amount or not update_date_income:
                raise ValidationError("Hacen falta datos para guardar la bitacora")
            # Guarda la bitacora de la adecuación
            if type == 'recandelize':
                var_type = 'rescheduling'
            else:
                var_type = 'adequacy'
            if movement == 'aumento':
                var_movement = 'increase'
            else:
                var_movement = 'decrease'
            month_names = {
            'enero': '1',
            'febrero': '2',
            'marzo': '3',
            'abril': '4',
            'mayo': '5',
            'junio': '6',
            'julio': '7',
            'agosto': '8',
            'septiembre': '9',
            'octubre': '10',
            'noviembre': '11',
            'diciembre': '12',
            }
            # Busca el nombre del mes que le corresponde al número de mes
            month_name_selected = month_names.get(proposed_date)
            # Forma el mes de afectación con formato AAAA/MM/DD
            var_propossed_date = fiscal_year_name.name +'-'+month_name_selected+'-01'
        else:
            var_type = type
            var_movement = movement
            var_propossed_date = proposed_date

        # Crea registro de bitacora
        logbook_line.create({
            'fiscal_year': fiscal_year_id,
            'cri_id': cri_id,
            'proposed_date': var_propossed_date,
            'movement_type': var_type,
            'movement': var_movement,
            'source_type': origin_type,
            'cve': name,
            'journal_applied': journal_id,
            'date_update': update_date_record,
            'policy_amount': amount,
            'budget_update_date': update_date_income,
        })
