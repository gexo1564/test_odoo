# -*- coding: utf-8 -*-
import logging
import unittest
from odoo.exceptions import UserError, ValidationError
from datetime import datetime
from odoo.tests import common, tagged
_logger = logging.getLogger(__name__)

@tagged('-at_install','post_install','relation_journal_process','siif_budget_mgmt')
class IncomeAdequaciesTest(common.TransactionCase):
	
    # ! Prepara entorno de pruebas
    def setUp(self):
        super(IncomeAdequaciesTest,self).setUp()
        self.model = self.env['siif.budget.mgmt.relation.journal.process']
        self.process = self.env['siif.budget.mgmt.relation.modules.process']
        self.journal_id = self.env['account.journal']

    # ! Test diario no existente
    def test_journal_id_not_exist(self):
        id_journal = "0"
        journal_id = self.journal_id.search([('id', '=', id_journal)])

        # ? Verifica que journal_id esté vacío, lo que indica que no se encontró el diario
        self.assertEqual(len(journal_id), 0)

        # ? Intenta obtener el año fiscal predeterminado en el modelo
        res = self.model.default_get([])

        # ? Verifica que el campo journal_id en res no coincida con el diario no existente
        self.assertNotEqual(res.get('journal_name'), str(journal_id))

        print('------------------------ DIARIO NO EXISTE ----------------------------- OK')

    # ! Test proceso no existente
    def test_process_id_not_exist(self):
        id_process = "0"
        process_id = self.process.search([('id', '=', id_process)])

        # ? Verifica que process_id esté vacío, lo que indica que no se encontró el proceso
        self.assertEqual(len(process_id), 0)

        # ? Intenta obtener el año fiscal predeterminado en el modelo
        res = self.model.default_get([])

        # ? Verifica que el campo process_name en res no coincida con el proceso no existente
        self.assertNotEqual(res.get('process_name'), str(process_id))

        print('------------------------ PROCESO NO EXISTE ----------------------------- OK')
