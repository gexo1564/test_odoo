from odoo import models, fields, api

class ResPartner(models.Model):

    _inherit = 'res.partner'

    contact_type = fields.Selection([('client', 'Client'),
                                     ('provider', 'Provider')], string="Contact Type")
    
    income_control = fields.Char('Control')

        # Seguridad facturacion digital
    is_group_income_fac_administrator_user = fields.Boolean(compute="_compute_is_group_income_fac_administrator_user")
    is_group_income_fac_finance_user = fields.Boolean(compute="_compute_is_group_income_fac_finance_user")
    is_group_income_fac_accounting_user = fields.Boolean(compute="_compute_is_group_income_fac_accounting_user")
    x_css_fac = fields.Html(string='CSS', sanitize=False, compute='_compute_css_fac', store=False)

    @api.depends('is_group_income_fac_administrator_user')
    def _compute_is_group_income_fac_administrator_user(self):
        self.is_group_income_fac_administrator_user = self.env.user.has_group('jt_income.group_income_fac_administrator_user')

    @api.depends('is_group_income_fac_finance_user')
    def _compute_is_group_income_fac_finance_user(self):
        self.is_group_income_fac_finance_user = self.env.user.has_group('jt_income.group_income_fac_finance_user')

    @api.depends('is_group_income_fac_accounting_user')
    def _compute_is_group_income_fac_accounting_user(self):
        self.is_group_income_fac_accounting_user = self.env.user.has_group('jt_income.group_income_fac_accounting_user')                  

    @api.depends('is_group_income_fac_administrator_user','is_group_income_fac_finance_user','is_group_income_fac_accounting_user')
    def _compute_css_fac(self):
 
        for record in self:
            record.x_css_fac = ""

            record.x_css_fac += """
                <style>
                    .o_form_view .o_form_statusbar > .o_statusbar_status{
                    max-width:85%;
                    }
                    button[name="613"] {
                        display: none !important;
                    }
                    button[name="open_action_followup"] {
                        display: none !important;
                    }
                    button[name="total_invoiced"] {
                        display: none !important;
                    }
                    button[name="614"] {
                        display: none !important;
                    }
                    button[name="open_partner_ledger"] {
                        display: none !important;
                    }   
                </style>
            """