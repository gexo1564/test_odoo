# -*- coding: utf-8 -*-
import logging
from odoo.tests import common
from odoo.exceptions import UserError, ValidationError

ACCOUNT_TOPAY_TYPE = 'account.topay.type'

class TestAccountToPayType(common.TransactionCase):

    def test_check_name(self):
        # Se cargan valores iniciales en la base de datos
        self.env[ACCOUNT_TOPAY_TYPE].create({
            'name': 'CXP_TEST_SAME_NAME',
            'prefix': 'p1'
        })
        # Se realiza la prueba de ejecutar un nombre duplicado
        with self.assertRaises(ValidationError):
            self.env[ACCOUNT_TOPAY_TYPE].create({
                'name': 'CXP_TEST_SAME_NAME',
                'prefix': 'p2'
            })

    def test_check_prefix(self):
        # Se cargan valores iniciales en la base de datos
        self.env[ACCOUNT_TOPAY_TYPE].create({
            'name': 'TEST1',
            'prefix': 'CXP_TEST_SAME_PREFIX'
        })
        # Se realiza la prueba de insertar un prefijo duplicado
        with self.assertRaises(ValidationError):
            self.env[ACCOUNT_TOPAY_TYPE].create({
                'name': 'TEST2',
                'prefix': 'CXP_TEST_SAME_PREFIX'
            })

    def test_create(self):
        """
        Escenario 1: Crear un tipo de cuenta por pagar sin indicar el prefijo
        """
        with self.assertRaises(ValidationError):
            self.env[ACCOUNT_TOPAY_TYPE].create({
                'name': 'TEST',
            })
        """
        Escenario 2: Validar la creación de la secuencia del tipo de cuenta por pagar
        """
        res = self.env[ACCOUNT_TOPAY_TYPE].create({
            'name': 'TEST',
            'prefix': 'test'
        })
        # Se realiza la busqueda de la secuencia para validar su creación
        self.assertEqual(res.sequence_id.prefix, 'test/%(range_year)s/')

    def test_write(self):
        # Se cargan valores iniciales en la base de datos
        res = self.env[ACCOUNT_TOPAY_TYPE].create({
            'name': 'TEST',
            'prefix': 'test'
        })
        # Se realiza la modificación del prefijo
        res.write({'prefix': 'new_prefix'})
        # Se realiza la busqueda de la secuencia para validar su creación
        self.assertEqual(res.sequence_id.prefix, 'new_prefix/%(range_year)s/')

    def test_unlink(self):
        # Se cargan valores iniciales en la base de datos
        account_type = self.env[ACCOUNT_TOPAY_TYPE].create({
            'name': 'TEST',
            'prefix': 'test'
        })
        # Se crea la cuanta por pagar asociada a type
        account_to_pay = self.env['account.topay'].create({
            'type_provision': account_type.id,
            'partner_id': 86166,
            "dependency_id": 145,
            "sub_dependency_id": 944,
            'state': 'draft'
        })
        """
        Escenario 1: Intentar borrar un tipo de cuenta que ya se asoció con una cuenta por pagar
        """
        with self.assertRaises(UserError):
            account_type.unlink()

        """
        Escenario 2: Eliminar un tipo de cuenta por pagar que no esta asociada a nínguna cuenta por pagar
        """
        account_to_pay.unlink()
        account_type.unlink()

    def test_onchange_item_groups_allowed(self):
        account_type = self.env[ACCOUNT_TOPAY_TYPE].create({
            'name': 'TEST',
            'prefix': 'test'
        })

        """
        Escenario 1: Intentar registrar un grupo erróneo con caracteres
        """
        with self.assertRaises(ValidationError):
            account_type.item_groups_allowed = 'Hola, error'
            account_type._onchange_item_groups_allowed()
        """
        Escenario 2: Intentar registrar grupos que no tiene 3 digitos
        """
        with self.assertRaises(ValidationError):
            account_type.item_groups_allowed = '10, 20'
            account_type._onchange_item_groups_allowed()

        """
        Escenario 3: Intentar registrar grupos que cumplen, y grupos que no
        """
        with self.assertRaises(ValidationError):
            account_type.item_groups_allowed = '100, 20'
            account_type._onchange_item_groups_allowed()

        """
        Escenario 4: Intentar grupos con formato invalido
        """
        with self.assertRaises(ValidationError):
            account_type.item_groups_allowed = '100,, 200'
            account_type._onchange_item_groups_allowed()

        """
        Escenario 5: Grupos validos
        """
        account_type.item_groups_allowed = '100, 200'
        account_type._onchange_item_groups_allowed()
