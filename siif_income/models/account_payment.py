from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
import logging
from datetime import date, datetime
logger = logging.getLogger(__name__)
import logging

model_move_line = 'account.move.line'
model_move = 'account.move'
model_payment = 'account.payment'
model_budget = 'affectation.income.budget'
model_subdep = 'sub.dependency'

class account_payment(models.Model):
    _inherit = "account.payment"

    type_of_revenue_collection = fields.Selection(selection_add=[('education_servs', 'Education Services'),
                                                    ('aspirants','Aspirants'),('exam_prep', 'Exam Preparation')])
    ticket_description = fields.Char("Ticket description")
    cfdi_serie = fields.Char("Serie de la factura")
    cfdi_folio = fields.Char("Folio de la factura")
    mn_amount = fields.Float("Monto/Total en moneda nacional")

    def post(self):
        result = super(account_payment,self).post()
        # Campo monto de afectación
        cri = False
        affectation_amount = 0
        # Se iteran cuando se selecciona por bloque Validar pago / cobro
        for values in self:
            # #Solicitudes de pago a proveedor
            if values.payment_request_type == 'supplier_payment' and values.payment_request_type != 'payroll_payment':
                #Dependencia y subdependencia en la cuenta por pagar de la póliza de pago
                move = self.env[model_move].sudo().search([('id','=',values.payment_request_id.id)])
                res_id = 'res.partner,' + str(values.partner_id.id)
                account_payable = self.env['ir.property'].sudo().search([('res_id', '=', res_id), ('name', '=', 'property_account_payable_id')])
                cuenta = account_payable.value_reference
                corta = str(cuenta).split(',')
                cxp = int(corta[1])
                move_line = self.env[model_move_line].sudo().search([('ref','=', values.communication),('account_id','=',cxp)])
                obj_line = self.env[model_move_line].sudo().search([('id', '=', move_line.id)])

                # Actualizar
                obj_line.update({
                        'dependency_id': move.dependancy_id.id,
                        'sub_dependency_id': move.sub_dependancy_id.id ,
                    })

                # Código programático póliza de pago
                pa = self.env['egress.keys'].search([('key','=','PA')])
                obj = self.env[model_move_line].search([('move_id','=', values.payment_request_id.id ),('program_code_id','!=', False),('exclude_from_invoice_tab', '=', False)])

                amount_currency = 0.0
                currency_id = False

            if values.payment_request_type == 'supplier_payment' and values.payment_request_type != 'payroll_payment':
                payment_lines = self.env[model_move_line].search([('move_id','=', values.payment_request_id.id ),('program_code_id','!=', False),('exclude_from_invoice_tab', '=', False)])
                for x in payment_lines:
                    if (values.invoice_ids.currency_id != values.invoice_ids.company_id.currency_id):
                        amount_currency = abs(x.price_total)
                        balance = values.invoice_ids.currency_id._convert(amount_currency, values.invoice_ids.company_currency_id, values.invoice_ids.company_id, values.invoice_ids.date_approval_request)
                        currency_id = values.invoice_ids.currency_id and values.invoice_ids.currency_id.id or False
                    else:
                        balance = abs(x.price_total)
                        amount_currency = 0.0
                        currency_id = False
                    values.move_line_ids = [
                        (0, 0, {
                            'account_id': x.journal_id.paid_credit_account_id and x.journal_id.paid_credit_account_id.id or False,
                            'coa_conac_id': x.journal_id.conac_paid_credit_account_id and x.journal_id.conac_paid_credit_account_id.id or False,
                            'credit': balance,
                            'exclude_from_invoice_tab': True,
                            'conac_move' : True,
                            'move_id' : move_line.move_id.id,
                            'amount_currency' : -amount_currency,
                            'currency_id' : currency_id,
                            'dependency_id' : x.program_code_id.dependency_id.id or False,
                            'sub_dependency_id' : x.program_code_id.sub_dependency_id.id or False,
                            'program_code_id' : x.program_code_id.id or False,
                            'partner_id':x.partner_id.id or False,
                            # 'name':f'Pago a proveedor: {provider} {date} {folio}'
                        }),
                        (0, 0, {
                            'account_id': x.journal_id.paid_debit_account_id and x.journal_id.paid_debit_account_id.id or False,
                            'coa_conac_id': x.journal_id.conac_paid_debit_account_id and x.journal_id.conac_paid_debit_account_id.id or False,
                            'debit': balance,
                            'exclude_from_invoice_tab': True,
                            'conac_move' : True,
                            'move_id' : move_line.move_id.id,
                            'amount_currency' : amount_currency,
                            'currency_id' : currency_id,
                            'dependency_id' :  x.program_code_id.dependency_id.id or False,
                            'sub_dependency_id' : x.program_code_id.sub_dependency_id.id or False,
                            'program_code_id' : x.program_code_id.id or False,
                            'partner_id': x.partner_id.id or False,
                            # 'name':f'Pago a proveedor: {provider} {date} {folio}'
                        })
                    ]

        for payment in self:
            ie_message = 'Please configure IE Accounts 100% Percentage'
            com_message = "Comisión de la factura"
            iva_message = "IVA de la comisión"
            income_invoices= payment.invoice_ids.filtered(lambda x:x.type_of_revenue_collection)
            if income_invoices:
                if payment.journal_id and not payment.journal_id.accrued_income_credit_account_id \
                    or not payment.journal_id.conac_accrued_income_credit_account_id \
                    or not payment.journal_id.accrued_income_debit_account_id \
                    or not payment.journal_id.conac_accrued_income_debit_account_id :
                    payment_acc_flag = False
                else:
                    payment_acc_flag = True

                if payment.journal_id and not payment.journal_id.recover_income_credit_account_id \
                    or not payment.journal_id.conac_recover_income_credit_account_id \
                    or not payment.journal_id.recover_income_debit_account_id \
                    or not payment.journal_id.conac_recover_income_debit_account_id :
                    payment_rec_flag = False
                else:
                    payment_rec_flag = True
                company_currency = self.company_id.currency_id                    
                if payment.move_line_ids:
                    move_id = payment.move_line_ids[0].move_id

                    for pay in income_invoices:
                        invoice_line = []
                        control=0
                        if pay.comission_id:
                            comission = pay.comission_id
                            comission_amount = pay.comission_id.comission_amount*1.16
                            comission_flag = True
                        else:
                            comission_flag = False  
                        type_captation = pay.type_of_revenue_collection
                        if payment.currency_id == company_currency:
                            total_payment = pay.amount_total
                            payment_amount = payment.amount
                            currency_id = False
                        else:
                            total_payment = self.currency_id._convert(pay.amount_total, company_currency, payment.company_id, payment.payment_date)
                            payment_amount = self.currency_id._convert(payment.amount, company_currency, payment.company_id, payment.payment_date)
                            currency_id = payment.currency_id.id

                        taxes_cfdi=0.0
                        if pay.type == 'out_invoice':
                            if pay.type_of_invoice=='cash_closing':
                                if payment.currency_id == company_currency:
                                    total_payment=payment.amount
                                    currency_id = False
                                else:
                                    total_payment = self.currency_id._convert(payment.amount, company_currency, payment.company_id, payment.payment_date)
                                    currency_id = payment.currency_id.id

                                for line in pay.income_invoice_line_ids.filtered(lambda x:x.name.rstrip() in payment.ticket_description.rstrip() and x.move_id.cfdi_serie==payment.cfdi_serie and x.move_id.folio_cfdi == payment.cfdi_folio):

                                    if line.account_ie_id and line.account_ie_id.ie_account_line_ids:
                                        total_line_pr = sum(x.percentage for x in line.account_ie_id.ie_account_line_ids)
                                        control_cent = 0
                                        residual_amount = 0
                                        if total_line_pr != 100:
                                            raise ValidationError(_(ie_message))
                                        if line.account_ie_id.base_amount>0.00:
                                            base_amount_payment = line.account_ie_id.base_amount
                                            aspirants_amount = 0.0
                                        if pay.type_of_revenue_collection=='billing':
                                            base_amount_payment = total_payment
                                            aspirants_flag = False
                                            comission_flag = False
                                        if not line.tax_ids:
                                            tax_flag=False
                                        else:
                                            pay_tax_id = line.tax_ids[0]

                                    #####Comisiones de la factura
                                        if control==0 and pay.comission_cfdi>0.0:
                                            line_vals = {
                                                         'account_id': pay.income_bank_journal_id.default_debit_account_id.id,
                                                         'coa_conac_id': pay.income_bank_journal_id.conac_accrued_income_credit_account_id and pay.income_bank_journal_id.conac_accrued_income_credit_account_id.id or False,
                                                         'credit': pay.comission_cfdi, 
                                                         'conac_move' : True,
                                                         'amount_currency' : 0.0,
                                                         #'currency_id' : currency_id,                                     
                                                         'move_id' : move_id.id,
                                                         'payment_id': payment.id,
                                                         'date_maturity': payment.payment_date,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'dependency_id':move_id.dependancy_id.id if move_id.income_bank_journal_id.default_debit_account_id.dep_subdep_flag else False,
                                                         'sub_dependency_id':move_id.sub_dependancy_id.id if move_id.income_bank_journal_id.default_debit_account_id.dep_subdep_flag else False,
                                                         'name':com_message,

                                                                                     
                                                        }
                                            invoice_line.append((0,0,line_vals,{'context':{'check_move_validity': False}}))

                                            line_vals = {
                                                         'account_id': pay.income_bank_journal_id.default_debit_account_id.id,
                                                         'coa_conac_id': pay.income_bank_journal_id.conac_accrued_income_credit_account_id and pay.income_bank_journal_id.conac_accrued_income_credit_account_id.id or False,
                                                         'credit': pay.iva_comission_cfdi, 
                                                         'conac_move' : True,
                                                         'amount_currency' : 0.0,
                                                         #'currency_id' : currency_id,                                     
                                                         'move_id' : move_id.id,
                                                         'payment_id': payment.id,
                                                         'date_maturity': payment.payment_date,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'dependency_id':move_id.dependancy_id.id if move_id.income_bank_journal_id.default_debit_account_id.dep_subdep_flag else False,
                                                         'sub_dependency_id':move_id.sub_dependancy_id.id if move_id.income_bank_journal_id.default_debit_account_id.dep_subdep_flag else False,
                                                         'name':iva_message,
                                                                                     
                                                        }
                                            invoice_line.append((0,0,line_vals,{'context':{'check_move_validity': False}}))
                                            control=control+1
                                            taxes_cfdi=pay.comission_cfdi+pay.iva_comission_cfdi

                                        if pay_tax_id.amount>0.0 and total_payment>0.2:
                                            per_amount_total = base_amount_payment/total_payment
                                            real_amount=line.price_subtotal*per_amount_total
                                            pay_tax_amount = real_amount*(pay_tax_id.amount/100)


                                            line_vals = {
                                                         'account_id': pay_tax_id.iva_transferred_account_id.id,
                                                         #'coa_conac_id': payment.journal_id.conac_accrued_income_credit_account_id and payment.journal_id.conac_accrued_income_credit_account_id.id or False,
                                                         'credit': pay_tax_amount, 
                                                         'conac_move' : False,
                                                         'amount_currency' : 0.0,
                                                         'currency_id' : currency_id,                                     
                                                         'move_id' : move_id.id,
                                                         'payment_id': payment.id,
                                                         'date_maturity': payment.payment_date,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'dependency_id':move_id.dependancy_id.id if pay_tax_id.iva_transferred_account_id.dep_subdep_flag else False,
                                                         'sub_dependency_id':move_id.sub_dependancy_id.id if pay_tax_id.iva_transferred_account_id.dep_subdep_flag else False,
                                                                                     
                                                        }
                                            invoice_line.append((0,0,line_vals,{'context':{'check_move_validity': False}}))
                                                        
                                            line_vals = {
                                                         'account_id': pay_tax_id.iva_to_move_account_id.id,
                                                         #'coa_conac_id': payment.journal_id.conac_accrued_income_debit_account_id and payment.journal_id.conac_accrued_income_debit_account_id.id or False,
                                                         'debit': pay_tax_amount,
                                                         'conac_move' : False,
                                                         'amount_currency' : 0.0,
                                                         'currency_id' : currency_id,                                     
                                                         'move_id' : move_id.id,
                                                         'payment_id': payment.id,
                                                         'date_maturity': payment.payment_date,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'dependency_id':move_id.dependancy_id.id if pay_tax_id.iva_to_move_account_id.dep_subdep_flag else False,
                                                         'sub_dependency_id':move_id.sub_dependancy_id.id if pay_tax_id.iva_to_move_account_id.dep_subdep_flag else False,
                                                         
                                                                                   
                                                        }
                                            invoice_line.append((0,0,line_vals,{'context':{'check_move_validity': False}}))

                                        if aspirants_flag:
                                            for account_line in line.account_ie_id.ie_account_line_ids.filtered(lambda x:x.percentage == 0.0):
                                                # Busqueda de CRI
                                                account_code = account_line.account_id.code
                                                cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                                line_vals = {
                                                                'account_id': account_line.account_id and account_line.account_id.id or False,
                                                                 #'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                                 'credit': aspirants_amount, 
                                                                 'conac_move' : False,
                                                                 'amount_currency' : 0.0,
                                                                 'currency_id' : currency_id,                                     
                                                                 'move_id' : move_id.id,
                                                                 'payment_id': payment.id,
                                                                 'date_maturity': payment.payment_date,
                                                                 'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                 'dependency_id':move_id.dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                                 'sub_dependency_id':move_id.sub_dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                                 'account_ie_id': line.account_ie_id and line.account_ie_id.id or False,
                                                                 'cri' : cri.name if cri else False
                                                            }
                                                invoice_line.append((0,0,line_vals))

                                                if(account_line.account_id.code[0]=='4' and account_line.account_id.budget_registry):
                                                    # Busqueda de CRI
                                                    account_code = account_line.account_id.code
                                                    cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                                    if payment_rec_flag and payment_acc_flag:
                                                        payment.move_line_ids = [(0, 0, {
                                                                                     'account_id': payment.journal_id.accrued_income_credit_account_id and payment.journal_id.accrued_income_credit_account_id.id or False,
                                                                                     'coa_conac_id': payment.journal_id.conac_accrued_income_credit_account_id and payment.journal_id.conac_accrued_income_credit_account_id.id or False,
                                                                                     'credit': aspirants_amount, 
                                                                                     'conac_move' : True,
                                                                                     'amount_currency' : 0.0,
                                                                                     'currency_id' : currency_id,                                     
                                                                                     'move_id' : move_id.id,
                                                                                     'payment_id': payment.id,
                                                                                    'date_maturity': payment.payment_date,
                                                                                    'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                    'cri' : cri.name if cri else False
                                                                                 }), 
                                                                        (0, 0, {
                                                                                     'account_id': payment.journal_id.accrued_income_debit_account_id and payment.journal_id.accrued_income_debit_account_id.id or False,
                                                                                     'coa_conac_id': payment.journal_id.conac_accrued_income_debit_account_id and payment.journal_id.conac_accrued_income_debit_account_id.id or False,
                                                                                     'debit': aspirants_amount,
                                                                                     'conac_move' : True,
                                                                                     'amount_currency' : 0.0,
                                                                                     'currency_id' : currency_id,                                     
                                                                                     'move_id' : move_id.id,
                                                                                     'payment_id': payment.id,
                                                                                     'date_maturity': payment.payment_date,
                                                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                     'cri' : cri.name if cri else False
                                                                                 })]

                                                        payment.move_line_ids = [(0, 0, {
                                                                                     'account_id': payment.journal_id.recover_income_credit_account_id and payment.journal_id.recover_income_credit_account_id.id or False,
                                                                                     'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                                                     'credit': aspirants_amount, 
                                                                                     'conac_move' : True,
                                                                                     'amount_currency' : 0.0,
                                                                                     'currency_id' : currency_id,                                     
                                                                                     'move_id' : move_id.id,
                                                                                     'payment_id': payment.id,
                                                                                    'date_maturity': payment.payment_date,
                                                                                    'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                    'cri' : cri.name if cri else False
                                                                                 }), 
                                                                        (0, 0, {
                                                                                     'account_id': payment.journal_id.recover_income_debit_account_id and payment.journal_id.recover_income_debit_account_id.id or False,
                                                                                     'coa_conac_id': payment.journal_id.conac_recover_income_debit_account_id and payment.journal_id.conac_recover_income_debit_account_id.id or False,
                                                                                     'debit': aspirants_amount,
                                                                                     'conac_move' : True,
                                                                                     'amount_currency' : 0.0,
                                                                                     'currency_id' : currency_id,                                     
                                                                                     'move_id' : move_id.id,
                                                                                     'payment_id': payment.id,
                                                                                     'date_maturity': payment.payment_date,
                                                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                     'cri' : cri.name if cri else False
                                                                                 })]
                                                        # Afectar el presupuesto de ingresos
                                                        if cri:
                                                            # Extraer campos
                                                            journal = payment.journal_id
                                                            affectation_amount = aspirants_amount
                                                            date_affectation = payment.payment_date
                                                            # Enviar a la funcion
                                                            self._income_budget_afectation(cri,journal,affectation_amount,date_affectation)
                                                        
                                        if comission_flag:
                                            base_amount_payment = base_amount_payment-comission_amount
                                            for comission_line in comission.comission_line_ids.filtered(lambda x:x.comission_percentage > 0):
                                                # Busqueda de CRI
                                                account_code = comission_line.account_id.code
                                                cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                                line_vals = {
                                                                'account_id': comission_line.account_id and comission_line.account_id.id or False,
                                                                 #'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                                 'credit': comission_amount, 
                                                                 'conac_move' : False,
                                                                 'amount_currency' : 0.0,
                                                                 'currency_id' : currency_id,                                     
                                                                 'move_id' : move_id.id,
                                                                 'payment_id': payment.id,
                                                                 'date_maturity': payment.payment_date,
                                                                 'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                 'dependency_id':move_id.dependancy_id.id if comission_line.account_id.dep_subdep_flag else False,
                                                                 'sub_dependency_id':move_id.sub_dependancy_id.id if comission_line.account_id.dep_subdep_flag else False,
                                                                 'account_ie_id': line.account_ie_id and line.account_ie_id.id or False,
                                                                 'cri' : cri.name if cri else False,
                                                            }
                                                invoice_line.append((0,0,line_vals))

                                                if(comission_line.account_id.code[0]=='4' and comission_line.account_id.budget_registry):
                                                    # Busqueda de CRI
                                                    account_code = comission_line.account_id.code
                                                    cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                                    if payment_rec_flag and payment_acc_flag:
                                                        payment.move_line_ids = [(0, 0, {
                                                                                     'account_id': payment.journal_id.accrued_income_credit_account_id and payment.journal_id.accrued_income_credit_account_id.id or False,
                                                                                     'coa_conac_id': payment.journal_id.conac_accrued_income_credit_account_id and payment.journal_id.conac_accrued_income_credit_account_id.id or False,
                                                                                     'credit': comission_amount, 
                                                                                     'conac_move' : True,
                                                                                     'amount_currency' : 0.0,
                                                                                     'currency_id' : currency_id,                                     
                                                                                     'move_id' : move_id.id,
                                                                                     'payment_id': payment.id,
                                                                                    'date_maturity': payment.payment_date,
                                                                                    'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                    'cri' : cri.name if cri else False
                                                                                 }), 
                                                                        (0, 0, {
                                                                                     'account_id': payment.journal_id.accrued_income_debit_account_id and payment.journal_id.accrued_income_debit_account_id.id or False,
                                                                                     'coa_conac_id': payment.journal_id.conac_accrued_income_debit_account_id and payment.journal_id.conac_accrued_income_debit_account_id.id or False,
                                                                                     'debit': comission_amount,
                                                                                     'conac_move' : True,
                                                                                     'amount_currency' : 0.0,
                                                                                     'currency_id' : currency_id,                                     
                                                                                     'move_id' : move_id.id,
                                                                                     'payment_id': payment.id,
                                                                                     'date_maturity': payment.payment_date,
                                                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                     'cri' : cri.name if cri else False
                                                                                 })]

                                                        payment.move_line_ids = [(0, 0, {
                                                                                     'account_id': payment.journal_id.recover_income_credit_account_id and payment.journal_id.recover_income_credit_account_id.id or False,
                                                                                     'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                                                     'credit': comission_amount, 
                                                                                     'conac_move' : True,
                                                                                     'amount_currency' : 0.0,
                                                                                     'currency_id' : currency_id,                                     
                                                                                     'move_id' : move_id.id,
                                                                                     'payment_id': payment.id,
                                                                                    'date_maturity': payment.payment_date,
                                                                                    'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                    'cri' : cri.name if cri else False
                                                                                 }), 
                                                                        (0, 0, {
                                                                                     'account_id': payment.journal_id.recover_income_debit_account_id and payment.journal_id.recover_income_debit_account_id.id or False,
                                                                                     'coa_conac_id': payment.journal_id.conac_recover_income_debit_account_id and payment.journal_id.conac_recover_income_debit_account_id.id or False,
                                                                                     'debit': comission_amount,
                                                                                     'conac_move' : True,
                                                                                     'amount_currency' : 0.0,
                                                                                     'currency_id' : currency_id,                                     
                                                                                     'move_id' : move_id.id,
                                                                                     'payment_id': payment.id,
                                                                                     'date_maturity': payment.payment_date,
                                                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                     'cri' : cri.name if cri else False
                                                                                 })]
                                                        # Afectar el presupuesto de ingresos
                                                        if cri:
                                                            # Extraer campos
                                                            journal = payment.journal_id
                                                            affectation_amount = comission_amount
                                                            date_affectation = payment.payment_date
                                                            # Enviar a la funcion
                                                            self._income_budget_afectation(cri,journal,affectation_amount,date_affectation)

                                        if taxes_cfdi>0.0:
                                            base_amount_payment=total_payment
                                            base_amount_payment=base_amount_payment-taxes_cfdi
                                        per_amount_total = base_amount_payment/total_payment
                                        real_amount=line.price_subtotal*per_amount_total
                                        residual_amount = real_amount

                                        for account_line in line.account_ie_id.ie_account_line_ids.filtered(lambda x:x.percentage > 0):

                                            line_count = len(line.account_ie_id.ie_account_line_ids.filtered(lambda x:x.percentage > 0))
                                            if real_amount < 0.2:
                                                price_per = real_amount

                                            else:
                                                price_per = round((real_amount*account_line.percentage)/100, 2)

                                            residual_amount = residual_amount-price_per

                                            if control_cent == line_count-1:
                                                if real_amount < 0.2:
                                                    price_per = round(price_per+residual_amount, 6)
                                                else:
                                                    price_per = round(price_per+residual_amount, 2)
                                            # Busqueda de CRI
                                            account_code = account_line.account_id.code
                                            cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                            line_vals = {
                                                            'account_id': account_line.account_id and account_line.account_id.id or False,
                                                             #'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                             'credit': price_per, 
                                                             'conac_move' : False,
                                                             'amount_currency' : 0.0,
                                                             'currency_id' : currency_id,                                     
                                                             'move_id' : move_id.id,
                                                             'payment_id': payment.id,
                                                             'date_maturity': payment.payment_date,
                                                             'partner_id': payment.partner_id.commercial_partner_id.id,
                                                             'dependency_id':move_id.dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                             'sub_dependency_id':move_id.sub_dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                             'account_ie_id': line.account_ie_id and line.account_ie_id.id or False,
                                                             'cri' : cri.name if cri else False
                                                        }
                                            invoice_line.append((0,0,line_vals,{'context':{'check_move_validity': False}}))
                                            if real_amount < 0.2:
                                                break

                                            if(account_line.account_id.code[0]=='2' and account_line.account_id.code[1]=='1' and account_line.account_id.code[2]=='3'):
                                                dep_213 = self.env['dependency']
                                                id_dep_213 = dep_213.search([('dependency','=', '764')])[0]
                                                sub_dep_213 = self.env[model_subdep]
                                                id_sub_dep_213 = sub_dep_213.search([('dependency_id','=', id_dep_213['id']),('sub_dependency','=','01')])[0] 
                                                line_vals = {
                                                            'account_id': account_line.account_id and account_line.account_id.id or False,
                                                             #'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                             'debit': price_per, 
                                                             'conac_move' : False,
                                                             'amount_currency' : 0.0,
                                                             'currency_id' : currency_id,                                     
                                                             'move_id' : move_id.id,
                                                             'payment_id': payment.id,
                                                             'date_maturity': payment.payment_date,
                                                             'partner_id': payment.partner_id.commercial_partner_id.id,
                                                             'dependency_id':move_id.dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                             'sub_dependency_id':move_id.sub_dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                             'account_ie_id': line.account_ie_id and line.account_ie_id.id or False,
                                                        }
                                                invoice_line.append((0,0,line_vals,{'context':{'check_move_validity': False}}))
                                                line_vals = {
                                                            'account_id': account_line.account_id and account_line.account_id.id or False,
                                                             #'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                             'credit': price_per, 
                                                             'conac_move' : False,
                                                             'amount_currency' : 0.0,
                                                             'currency_id' : currency_id,                                     
                                                             'move_id' : move_id.id,
                                                             'payment_id': payment.id,
                                                             'date_maturity': payment.payment_date,
                                                             'partner_id': payment.partner_id.commercial_partner_id.id,
                                                             'dependency_id': id_dep_213['id'],
                                                             'sub_dependency_id': id_sub_dep_213['id'],
                                                             'account_ie_id': line.account_ie_id and line.account_ie_id.id or False,
                                                        }
                                                invoice_line.append((0,0,line_vals,{'context':{'check_move_validity': False}}))


                                            if(account_line.account_id.code[0]=='4' and account_line.account_id.budget_registry):
                                                    # Busqueda de CRI
                                                    account_code = account_line.account_id.code
                                                    cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                                    if payment_rec_flag and payment_acc_flag:
                                                        payment.move_line_ids = [(0, 0, {
                                                                                     'account_id': payment.journal_id.accrued_income_credit_account_id and payment.journal_id.accrued_income_credit_account_id.id or False,
                                                                                     'coa_conac_id': payment.journal_id.conac_accrued_income_credit_account_id and payment.journal_id.conac_accrued_income_credit_account_id.id or False,
                                                                                     'credit': price_per, 
                                                                                     'conac_move' : True,
                                                                                     'amount_currency' : 0.0,
                                                                                     'currency_id' : currency_id,                                     
                                                                                     'move_id' : move_id.id,
                                                                                     'payment_id': payment.id,
                                                                                     'date_maturity': payment.payment_date,
                                                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                     'cri' : cri.name if cri else False
                                                                                 }), 
                                                                        (0, 0, {
                                                                                     'account_id': payment.journal_id.accrued_income_debit_account_id and payment.journal_id.accrued_income_debit_account_id.id or False,
                                                                                     'coa_conac_id': payment.journal_id.conac_accrued_income_debit_account_id and payment.journal_id.conac_accrued_income_debit_account_id.id or False,
                                                                                     'debit': price_per,
                                                                                     'conac_move' : True,
                                                                                     'amount_currency' : 0.0,
                                                                                     'currency_id' : currency_id,                                     
                                                                                     'move_id' : move_id.id,
                                                                                     'payment_id': payment.id,
                                                                                     'date_maturity': payment.payment_date,
                                                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                     'cri' : cri.name if cri else False
                                                                                 })]

                                                        payment.move_line_ids = [(0, 0, {
                                                                                     'account_id': payment.journal_id.recover_income_credit_account_id and payment.journal_id.recover_income_credit_account_id.id or False,
                                                                                     'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                                                     'credit': price_per, 
                                                                                     'conac_move' : True,
                                                                                     'amount_currency' : 0.0,
                                                                                     'currency_id' : currency_id,                                     
                                                                                     'move_id' : move_id.id,
                                                                                     'payment_id': payment.id,
                                                                                     'date_maturity': payment.payment_date,
                                                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                     'cri' : cri.name if cri else False
                                                                                 }), 
                                                                        (0, 0, {
                                                                                     'account_id': payment.journal_id.recover_income_debit_account_id and payment.journal_id.recover_income_debit_account_id.id or False,
                                                                                     'coa_conac_id': payment.journal_id.conac_recover_income_debit_account_id and payment.journal_id.conac_recover_income_debit_account_id.id or False,
                                                                                     'debit': price_per,
                                                                                     'conac_move' : True,
                                                                                     'amount_currency' : 0.0,
                                                                                     'currency_id' : currency_id,                                     
                                                                                     'move_id' : move_id.id,
                                                                                     'payment_id': payment.id,
                                                                                     'date_maturity': payment.payment_date,
                                                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                     'cri' : cri.name if cri else False
                                                                                 })]
                                                        # Afectar el presupuesto de ingresos
                                                        if cri:
                                                            # Extraer campos
                                                            journal = payment.journal_id
                                                            affectation_amount = price_per
                                                            date_affectation = payment.payment_date
                                                            # Enviar a la funcion
                                                            self._income_budget_afectation(cri,journal,affectation_amount,date_affectation)

                                            control_cent = control_cent+1


                                                        
                                if type_captation != 'deposit_cer':
                                    #logging.critical("Ticket: "+payment.ticket_description.split()[3])
                                    for line_inv in pay.line_ids:
                                        if len(line_inv.name.split())>3:
                                            if line_inv.name.split()[3]==payment.ticket_description.split()[3]:
                                                line_vals = {
                                                                     'account_id': line_inv.account_id and line_inv.account_id.id or False,
                                                                     #'coa_conac_id': payment.journal_id.conac_recover_income_debit_account_id and payment.journal_id.conac_recover_income_debit_account_id.id or False,
                                                                     'debit': line_inv.credit,
                                                                     'conac_move' : False,
                                                                     'amount_currency' : 0.0,
                                                                     'currency_id' : currency_id,                                     
                                                                     'move_id' : move_id.id,
                                                                     'payment_id': payment.id,
                                                                     'date_maturity': payment.payment_date,
                                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                     'dependency_id':move_id.dependancy_id.id if line_inv.account_id.dep_subdep_flag else False,
                                                                     'sub_dependency_id':move_id.sub_dependancy_id.id if line_inv.account_id.dep_subdep_flag else False,
                                                                   
                                                                 }
                                                invoice_line.append((0,0,line_vals,{'context':{'check_move_validity': False}}))
                                else:
                                    for line_inv in pay.line_ids:
                                        if line_inv.debit > 0:

                                            line_vals = {
                                                             'account_id': line_inv.account_id and line_inv.account_id.id or False,
                                                             #'coa_conac_id': payment.journal_id.conac_recover_income_debit_account_id and payment.journal_id.conac_recover_income_debit_account_id.id or False,
                                                             'credit': line_inv.debit,
                                                             'conac_move' : False,
                                                             'amount_currency' : 0.0,
                                                             'currency_id' : currency_id,                                     
                                                             'move_id' : move_id.id,
                                                             'payment_id': payment.id,
                                                             'date_maturity': payment.payment_date,
                                                             'partner_id': payment.partner_id.commercial_partner_id.id,
                                                             'dependency_id':move_id.dependancy_id.id if line_inv.account_id.dep_subdep_flag else False,
                                                             'sub_dependency_id':move_id.sub_dependancy_id.id if line_inv.account_id.dep_subdep_flag else False,
                                                           
                                                         }
                                            invoice_line.append((0,0,line_vals))
                                            
                                        else:
                                            
                                            line_vals = {
                                                             'account_id': line_inv.account_id and line_inv.account_id.id or False,
                                                             #'coa_conac_id': payment.journal_id.conac_recover_income_debit_account_id and payment.journal_id.conac_recover_income_debit_account_id.id or False,
                                                             'debit': line_inv.credit,
                                                             'conac_move' : False,
                                                             'amount_currency' : 0.0,
                                                             'currency_id' : currency_id,                                     
                                                             'move_id' : move_id.id,
                                                             'payment_id': payment.id,
                                                             'date_maturity': payment.payment_date,
                                                             'partner_id': payment.partner_id.commercial_partner_id.id,
                                                             'dependency_id':move_id.dependancy_id.id if line_inv.account_id.dep_subdep_flag else False,
                                                             'sub_dependency_id':move_id.sub_dependancy_id.id if line_inv.account_id.dep_subdep_flag else False,
                                                           
                                                         }
                                            invoice_line.append((0,0,line_vals))
                            else:
                                for line in pay.income_invoice_line_ids:
                                    if pay.l10n_mx_edi_payment_method_id.code=='17' and pay.dependancy_id.dependency=='417':
                                        continue
                                    if line.account_ie_id and line.account_ie_id.ie_account_line_ids:
                                        total_line_pr = sum(x.percentage for x in line.account_ie_id.ie_account_line_ids)
                                        control_cent = 0
                                        residual_amount = 0
                                        pay_tax_amount=0
                                        if total_line_pr != 100:
                                            raise ValidationError(_(ie_message))
                                        if line.account_ie_id.base_amount>0.00:
                                            base_amount_payment = line.account_ie_id.base_amount
                                        if pay.type_of_revenue_collection=='billing':
                                            base_amount_payment = payment_amount
                                            aspirants_flag = False
                                            comission_flag = False
                                        if not line.tax_ids:
                                                tax_flag=False
                                        else:
                                            pay_tax_id = line.tax_ids[0]

                                    #####Comisiones de la factura
                                        if control==0 and pay.comission_cfdi>0.0:
                                            line_vals = {
                                                         'account_id': pay.income_bank_journal_id.default_debit_account_id.id,
                                                         'coa_conac_id': pay.income_bank_journal_id.conac_accrued_income_credit_account_id and pay.income_bank_journal_id.conac_accrued_income_credit_account_id.id or False,
                                                         'credit': pay.comission_cfdi, 
                                                         'conac_move' : True,
                                                         'amount_currency' : 0.0,
                                                         #'currency_id' : currency_id,                                     
                                                         'move_id' : move_id.id,
                                                         'payment_id': payment.id,
                                                         'date_maturity': payment.payment_date,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'dependency_id':move_id.dependancy_id.id if move_id.income_bank_journal_id.default_debit_account_id.dep_subdep_flag else False,
                                                         'sub_dependency_id':move_id.sub_dependancy_id.id if move_id.income_bank_journal_id.default_debit_account_id.dep_subdep_flag else False,
                                                         'name':com_message,

                                                                                     
                                                        }
                                            invoice_line.append((0,0,line_vals,{'context':{'check_move_validity': False}}))

                                            line_vals = {
                                                         'account_id': pay.income_bank_journal_id.default_debit_account_id.id,
                                                         'coa_conac_id': pay.income_bank_journal_id.conac_accrued_income_credit_account_id and pay.income_bank_journal_id.conac_accrued_income_credit_account_id.id or False,
                                                         'credit': pay.iva_comission_cfdi, 
                                                         'conac_move' : True,
                                                         'amount_currency' : 0.0,
                                                         #'currency_id' : currency_id,                                     
                                                         'move_id' : move_id.id,
                                                         'payment_id': payment.id,
                                                         'date_maturity': payment.payment_date,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'dependency_id':move_id.dependancy_id.id if move_id.income_bank_journal_id.default_debit_account_id.dep_subdep_flag else False,
                                                         'sub_dependency_id':move_id.sub_dependancy_id.id if move_id.income_bank_journal_id.default_debit_account_id.dep_subdep_flag else False,
                                                         'name':iva_message,
                                                                                     
                                                        }
                                            invoice_line.append((0,0,line_vals,{'context':{'check_move_validity': False}}))
                                            control=control+1
                                            taxes_cfdi=pay.comission_cfdi+pay.iva_comission_cfdi

                                        if pay_tax_id.amount>0.0 and payment_amount>0.2:
                                            per_amount_total = base_amount_payment/total_payment
                                            real_amount=line.price_subtotal*per_amount_total
                                            pay_tax_amount = real_amount*(pay_tax_id.amount/100)


                                            line_vals = {
                                                         'account_id': pay_tax_id.iva_transferred_account_id.id,
                                                         #'coa_conac_id': payment.journal_id.conac_accrued_income_credit_account_id and payment.journal_id.conac_accrued_income_credit_account_id.id or False,
                                                         'credit': pay_tax_amount, 
                                                         'conac_move' : False,
                                                         'amount_currency' : 0.0,
                                                         'currency_id' : currency_id,                                     
                                                         'move_id' : move_id.id,
                                                         'payment_id': payment.id,
                                                         'date_maturity': payment.payment_date,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'dependency_id':move_id.dependancy_id.id if pay_tax_id.iva_transferred_account_id.dep_subdep_flag else False,
                                                         'sub_dependency_id':move_id.sub_dependancy_id.id if pay_tax_id.iva_transferred_account_id.dep_subdep_flag else False,
                                                                                     
                                                        }
                                            invoice_line.append((0,0,line_vals,{'context':{'check_move_validity': False}}))
                                                        
                                            line_vals = {
                                                         'account_id': pay_tax_id.iva_to_move_account_id.id,
                                                         #'coa_conac_id': payment.journal_id.conac_accrued_income_debit_account_id and payment.journal_id.conac_accrued_income_debit_account_id.id or False,
                                                         'debit': pay_tax_amount,
                                                         'conac_move' : False,
                                                         'amount_currency' : 0.0,
                                                         'currency_id' : currency_id,                                     
                                                         'move_id' : move_id.id,
                                                         'payment_id': payment.id,
                                                         'date_maturity': payment.payment_date,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'dependency_id':move_id.dependancy_id.id if pay_tax_id.iva_to_move_account_id.dep_subdep_flag else False,
                                                         'sub_dependency_id':move_id.sub_dependancy_id.id if pay_tax_id.iva_to_move_account_id.dep_subdep_flag else False,
                                                         
                                                                                   
                                                        }
                                            invoice_line.append((0,0,line_vals,{'context':{'check_move_validity': False}}))

                                        if aspirants_flag:
                                            for account_line in line.account_ie_id.ie_account_line_ids.filtered(lambda x:x.percentage == 0.0):
                                                # Busqueda de CRI
                                                account_code = account_line.account_id.code
                                                cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                                line_vals = {
                                                                'account_id': account_line.account_id and account_line.account_id.id or False,
                                                                 #'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                                 'credit': aspirants_amount, 
                                                                 'conac_move' : False,
                                                                 'amount_currency' : 0.0,
                                                                 'currency_id' : currency_id,                                     
                                                                 'move_id' : move_id.id,
                                                                 'payment_id': payment.id,
                                                                 'date_maturity': payment.payment_date,
                                                                 'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                 'dependency_id':move_id.dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                                 'sub_dependency_id':move_id.sub_dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                                 'account_ie_id': line.account_ie_id and line.account_ie_id.id or False,
                                                                 'cri' : cri.name if cri else False
                                                            }
                                                invoice_line.append((0,0,line_vals))

                                                if(account_line.account_id.code[0]=='4' and account_line.account_id.budget_registry):
                                                    # Busqueda de CRI
                                                    account_code = account_line.account_id.code
                                                    cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                                    if payment_rec_flag and payment_acc_flag:
                                                        payment.move_line_ids = [(0, 0, {
                                                                                     'account_id': payment.journal_id.accrued_income_credit_account_id and payment.journal_id.accrued_income_credit_account_id.id or False,
                                                                                     'coa_conac_id': payment.journal_id.conac_accrued_income_credit_account_id and payment.journal_id.conac_accrued_income_credit_account_id.id or False,
                                                                                     'credit': aspirants_amount, 
                                                                                     'conac_move' : True,
                                                                                     'amount_currency' : 0.0,
                                                                                     'currency_id' : currency_id,                                     
                                                                                     'move_id' : move_id.id,
                                                                                     'payment_id': payment.id,
                                                                                    'date_maturity': payment.payment_date,
                                                                                    'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                    'cri' : cri.name if cri else False
                                                                                 }), 
                                                                        (0, 0, {
                                                                                     'account_id': payment.journal_id.accrued_income_debit_account_id and payment.journal_id.accrued_income_debit_account_id.id or False,
                                                                                     'coa_conac_id': payment.journal_id.conac_accrued_income_debit_account_id and payment.journal_id.conac_accrued_income_debit_account_id.id or False,
                                                                                     'debit': aspirants_amount,
                                                                                     'conac_move' : True,
                                                                                     'amount_currency' : 0.0,
                                                                                     'currency_id' : currency_id,                                     
                                                                                     'move_id' : move_id.id,
                                                                                     'payment_id': payment.id,
                                                                                     'date_maturity': payment.payment_date,
                                                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                     'cri' : cri.name if cri else False
                                                                                 })]

                                                        payment.move_line_ids = [(0, 0, {
                                                                                     'account_id': payment.journal_id.recover_income_credit_account_id and payment.journal_id.recover_income_credit_account_id.id or False,
                                                                                     'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                                                     'credit': aspirants_amount, 
                                                                                     'conac_move' : True,
                                                                                     'amount_currency' : 0.0,
                                                                                     'currency_id' : currency_id,                                     
                                                                                     'move_id' : move_id.id,
                                                                                     'payment_id': payment.id,
                                                                                    'date_maturity': payment.payment_date,
                                                                                    'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                    'cri' : cri.name if cri else False
                                                                                 }), 
                                                                        (0, 0, {
                                                                                     'account_id': payment.journal_id.recover_income_debit_account_id and payment.journal_id.recover_income_debit_account_id.id or False,
                                                                                     'coa_conac_id': payment.journal_id.conac_recover_income_debit_account_id and payment.journal_id.conac_recover_income_debit_account_id.id or False,
                                                                                     'debit': aspirants_amount,
                                                                                     'conac_move' : True,
                                                                                     'amount_currency' : 0.0,
                                                                                     'currency_id' : currency_id,                                     
                                                                                     'move_id' : move_id.id,
                                                                                     'payment_id': payment.id,
                                                                                     'date_maturity': payment.payment_date,
                                                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                     'cri' : cri.name if cri else False
                                                                                 })]
                                                    # Afectar el presupuesto de ingresos
                                                    if cri:
                                                        # Extraer campos
                                                        journal = payment.journal_id
                                                        affectation_amount = aspirants_amount
                                                        date_affectation = payment.payment_date
                                                        # Enviar a la funcion
                                                        self._income_budget_afectation(cri,journal,affectation_amount,date_affectation)

                                        if comission_flag:
                                            base_amount_payment = base_amount_payment-comission_amount
                                            for comission_line in comission.comission_line_ids.filtered(lambda x:x.comission_percentage > 0):
                                                # Busqueda de CRI
                                                account_code = comission_line.account_id.code
                                                cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                                line_vals = {
                                                                'account_id': comission_line.account_id and comission_line.account_id.id or False,
                                                                 #'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                                 'credit': comission_amount, 
                                                                 'conac_move' : False,
                                                                 'amount_currency' : 0.0,
                                                                 'currency_id' : currency_id,                                     
                                                                 'move_id' : move_id.id,
                                                                 'payment_id': payment.id,
                                                                 'date_maturity': payment.payment_date,
                                                                 'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                 'dependency_id':move_id.dependancy_id.id if comission_line.account_id.dep_subdep_flag else False,
                                                                 'sub_dependency_id':move_id.sub_dependancy_id.id if comission_line.account_id.dep_subdep_flag else False,
                                                                 'account_ie_id': line.account_ie_id and line.account_ie_id.id or False,
                                                                 'cri': cri.name if cri else False
                                                            }
                                                invoice_line.append((0,0,line_vals))

                                                if(comission_line.account_id.code[0]=='4' and comission_line.account_id.budget_registry):
                                                    # Busqueda de CRI
                                                    account_code = comission_line.account_id.code
                                                    cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                                    if payment_rec_flag and payment_acc_flag:
                                                        payment.move_line_ids = [(0, 0, {
                                                                                     'account_id': payment.journal_id.accrued_income_credit_account_id and payment.journal_id.accrued_income_credit_account_id.id or False,
                                                                                     'coa_conac_id': payment.journal_id.conac_accrued_income_credit_account_id and payment.journal_id.conac_accrued_income_credit_account_id.id or False,
                                                                                     'credit': comission_amount, 
                                                                                     'conac_move' : True,
                                                                                     'amount_currency' : 0.0,
                                                                                     'currency_id' : currency_id,                                     
                                                                                     'move_id' : move_id.id,
                                                                                     'payment_id': payment.id,
                                                                                    'date_maturity': payment.payment_date,
                                                                                    'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                    'cri' : cri.name if cri else False
                                                                                 }), 
                                                                        (0, 0, {
                                                                                     'account_id': payment.journal_id.accrued_income_debit_account_id and payment.journal_id.accrued_income_debit_account_id.id or False,
                                                                                     'coa_conac_id': payment.journal_id.conac_accrued_income_debit_account_id and payment.journal_id.conac_accrued_income_debit_account_id.id or False,
                                                                                     'debit': comission_amount,
                                                                                     'conac_move' : True,
                                                                                     'amount_currency' : 0.0,
                                                                                     'currency_id' : currency_id,                                     
                                                                                     'move_id' : move_id.id,
                                                                                     'payment_id': payment.id,
                                                                                     'date_maturity': payment.payment_date,
                                                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                     'cri' : cri.name if cri else False
                                                                                 })]

                                                        payment.move_line_ids = [(0, 0, {
                                                                                     'account_id': payment.journal_id.recover_income_credit_account_id and payment.journal_id.recover_income_credit_account_id.id or False,
                                                                                     'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                                                     'credit': comission_amount, 
                                                                                     'conac_move' : True,
                                                                                     'amount_currency' : 0.0,
                                                                                     'currency_id' : currency_id,                                     
                                                                                     'move_id' : move_id.id,
                                                                                     'payment_id': payment.id,
                                                                                    'date_maturity': payment.payment_date,
                                                                                    'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                    'cri' : cri.name if cri else False
                                                                                 }), 
                                                                        (0, 0, {
                                                                                     'account_id': payment.journal_id.recover_income_debit_account_id and payment.journal_id.recover_income_debit_account_id.id or False,
                                                                                     'coa_conac_id': payment.journal_id.conac_recover_income_debit_account_id and payment.journal_id.conac_recover_income_debit_account_id.id or False,
                                                                                     'debit': comission_amount,
                                                                                     'conac_move' : True,
                                                                                     'amount_currency' : 0.0,
                                                                                     'currency_id' : currency_id,                                     
                                                                                     'move_id' : move_id.id,
                                                                                     'payment_id': payment.id,
                                                                                     'date_maturity': payment.payment_date,
                                                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                     'cri' : cri.name if cri else False
                                                                                 })]
                                                        # Afectar el presupuesto de ingresos
                                                        if cri:
                                                            # Extraer campos
                                                            journal = payment.journal_id
                                                            affectation_amount = comission_amount
                                                            date_affectation = payment.payment_date
                                                            # Enviar a la funcion
                                                            self._income_budget_afectation(cri,journal,affectation_amount,date_affectation)

                                        if taxes_cfdi>0.0:
                                            base_amount_payment=payment_amount
                                            base_amount_payment=base_amount_payment-taxes_cfdi
                                        base_amount_payment=base_amount_payment-pay_tax_amount
                                        per_amount_total = base_amount_payment/pay.amount_untaxed
                                        real_amount=round(line.price_subtotal*per_amount_total, 2)
                                        residual_amount = real_amount
 
                                        for account_line in line.account_ie_id.ie_account_line_ids.filtered(lambda x:x.percentage > 0):
                                            
                                            line_count = len(line.account_ie_id.ie_account_line_ids.filtered(lambda x:x.percentage > 0))
                                            if real_amount < 0.2:
                                                price_per = real_amount

                                            else:
                                                price_per = round((real_amount*account_line.percentage)/100, 2)
                                            residual_amount = residual_amount-price_per

                                            if control_cent == line_count-1:
                                                if real_amount < 0.2:
                                                    price_per = round(price_per+residual_amount, 6)
                                                else:
                                                    price_per = round(price_per+residual_amount, 2)
                                            # Busqueda de CRI
                                            account_code = account_line.account_id.code
                                            cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                            line_vals = {
                                                            'account_id': account_line.account_id and account_line.account_id.id or False,
                                                             #'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                             'credit': price_per, 
                                                             'conac_move' : False,
                                                             'amount_currency' : 0.0,
                                                             'currency_id' : currency_id,                                     
                                                             'move_id' : move_id.id,
                                                             'payment_id': payment.id,
                                                             'date_maturity': payment.payment_date,
                                                             'partner_id': payment.partner_id.commercial_partner_id.id,
                                                             'dependency_id':move_id.dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                             'sub_dependency_id':move_id.sub_dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                             'account_ie_id': line.account_ie_id and line.account_ie_id.id or False,
                                                             'cri' : cri.name if cri else False
                                                        }
                                            invoice_line.append((0,0,line_vals,{'context':{'check_move_validity': False}}))

                                            if real_amount < 0.2:
                                                break

                                            if(account_line.account_id.code[0]=='2' and account_line.account_id.code[1]=='1' and account_line.account_id.code[2]=='3'):
                                                dep_213 = self.env['dependency']
                                                id_dep_213 = dep_213.search([('dependency','=', '764')])[0]
                                                sub_dep_213 = self.env[model_subdep]
                                                id_sub_dep_213 = sub_dep_213.search([('dependency_id','=', id_dep_213['id']),('sub_dependency','=','01')])[0] 
                                                line_vals = {
                                                            'account_id': account_line.account_id and account_line.account_id.id or False,
                                                             #'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                             'debit': price_per, 
                                                             'conac_move' : False,
                                                             'amount_currency' : 0.0,
                                                             'currency_id' : currency_id,                                     
                                                             'move_id' : move_id.id,
                                                             'payment_id': payment.id,
                                                             'date_maturity': payment.payment_date,
                                                             'partner_id': payment.partner_id.commercial_partner_id.id,
                                                             'dependency_id':move_id.dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                             'sub_dependency_id':move_id.sub_dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                             'account_ie_id': line.account_ie_id and line.account_ie_id.id or False,
                                                        }
                                                invoice_line.append((0,0,line_vals,{'context':{'check_move_validity': False}}))
                                                line_vals = {
                                                            'account_id': account_line.account_id and account_line.account_id.id or False,
                                                             #'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                             'credit': price_per, 
                                                             'conac_move' : False,
                                                             'amount_currency' : 0.0,
                                                             'currency_id' : currency_id,                                     
                                                             'move_id' : move_id.id,
                                                             'payment_id': payment.id,
                                                             'date_maturity': payment.payment_date,
                                                             'partner_id': payment.partner_id.commercial_partner_id.id,
                                                             'dependency_id': id_dep_213['id'],
                                                             'sub_dependency_id': id_sub_dep_213['id'],
                                                             'account_ie_id': line.account_ie_id and line.account_ie_id.id or False,
                                                        }
                                                invoice_line.append((0,0,line_vals,{'context':{'check_move_validity': False}}))


                                            if(account_line.account_id.code[0]=='4' and account_line.account_id.budget_registry):
                                                    # Busqueda de CRI
                                                    account_code = account_line.account_id.code
                                                    cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                                    if payment_rec_flag and payment_acc_flag:
                                                        payment.move_line_ids = [(0, 0, {
                                                                                     'account_id': payment.journal_id.accrued_income_credit_account_id and payment.journal_id.accrued_income_credit_account_id.id or False,
                                                                                     'coa_conac_id': payment.journal_id.conac_accrued_income_credit_account_id and payment.journal_id.conac_accrued_income_credit_account_id.id or False,
                                                                                     'credit': price_per, 
                                                                                     'conac_move' : True,
                                                                                     'amount_currency' : 0.0,
                                                                                     'currency_id' : currency_id,                                     
                                                                                     'move_id' : move_id.id,
                                                                                     'payment_id': payment.id,
                                                                                     'date_maturity': payment.payment_date,
                                                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                     'cri' : cri.name if cri else False
                                                                                 }), 
                                                                        (0, 0, {
                                                                                     'account_id': payment.journal_id.accrued_income_debit_account_id and payment.journal_id.accrued_income_debit_account_id.id or False,
                                                                                     'coa_conac_id': payment.journal_id.conac_accrued_income_debit_account_id and payment.journal_id.conac_accrued_income_debit_account_id.id or False,
                                                                                     'debit': price_per,
                                                                                     'conac_move' : True,
                                                                                     'amount_currency' : 0.0,
                                                                                     'currency_id' : currency_id,                                     
                                                                                     'move_id' : move_id.id,
                                                                                     'payment_id': payment.id,
                                                                                     'date_maturity': payment.payment_date,
                                                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                     'cri' : cri.name if cri else False
                                                                                 })]

                                                        payment.move_line_ids = [(0, 0, {
                                                                                     'account_id': payment.journal_id.recover_income_credit_account_id and payment.journal_id.recover_income_credit_account_id.id or False,
                                                                                     'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                                                     'credit': price_per, 
                                                                                     'conac_move' : True,
                                                                                     'amount_currency' : 0.0,
                                                                                     'currency_id' : currency_id,                                     
                                                                                     'move_id' : move_id.id,
                                                                                     'payment_id': payment.id,
                                                                                     'date_maturity': payment.payment_date,
                                                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                     'cri' : cri.name if cri else False
                                                                                 }), 
                                                                        (0, 0, {
                                                                                     'account_id': payment.journal_id.recover_income_debit_account_id and payment.journal_id.recover_income_debit_account_id.id or False,
                                                                                     'coa_conac_id': payment.journal_id.conac_recover_income_debit_account_id and payment.journal_id.conac_recover_income_debit_account_id.id or False,
                                                                                     'debit': price_per,
                                                                                     'conac_move' : True,
                                                                                     'amount_currency' : 0.0,
                                                                                     'currency_id' : currency_id,                                     
                                                                                     'move_id' : move_id.id,
                                                                                     'payment_id': payment.id,
                                                                                     'date_maturity': payment.payment_date,
                                                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                     'cri' : cri.name if cri else False
                                                                                 })]
                                                        # Afectar el presupuesto de ingresos
                                                        if cri:
                                                            # Extraer campos
                                                            journal = payment.journal_id
                                                            affectation_amount = price_per
                                                            date_affectation = payment.payment_date
                                                            # Enviar a la funcion
                                                            self._income_budget_afectation(cri,journal,affectation_amount,date_affectation)
                                                            
                                            control_cent = control_cent+1

                                                        
                                if type_captation != 'deposit_cer':
                                    for line_inv in pay.invoice_line_ids:
                                        if pay.l10n_mx_edi_payment_method_id.code=='17' and pay.dependancy_id.dependency=='417':
                                            continue
                                        if payment.currency_id == company_currency:
                                            price_subtot = line_inv.price_subtotal
                                            currency_id = False
                                        else:
                                            price_subtot = self.currency_id._convert(line_inv.price_subtotal, company_currency, payment.company_id, payment.payment_date)
                                            currency_id = payment.currency_id.id
                                        per_amount_total = payment_amount/total_payment
                                        real_amount=price_subtot*per_amount_total
                                        line_vals = {
                                                             'account_id': line_inv.account_id and line_inv.account_id.id or False,
                                                             #'coa_conac_id': payment.journal_id.conac_recover_income_debit_account_id and payment.journal_id.conac_recover_income_debit_account_id.id or False,
                                                             'debit': real_amount,
                                                             'conac_move' : False,
                                                             'amount_currency' : 0.0,
                                                             'currency_id' : currency_id,                                     
                                                             'move_id' : move_id.id,
                                                             'payment_id': payment.id,
                                                             'date_maturity': payment.payment_date,
                                                             'partner_id': payment.partner_id.commercial_partner_id.id,
                                                             'dependency_id':move_id.dependancy_id.id if line_inv.account_id.dep_subdep_flag else False,
                                                             'sub_dependency_id':move_id.sub_dependancy_id.id if line_inv.account_id.dep_subdep_flag else False,
                                                           
                                                         }
                                        invoice_line.append((0,0,line_vals,{'context':{'check_move_validity': False}}))

                        ######## NOTAS DE CRÉDITO ########

                        elif pay.type == 'out_refund':
                            for line in pay.invoice_line_ids:
                                if line.account_ie_id and line.account_ie_id.ie_account_line_ids:
                                    total_line_pr = sum(x.percentage for x in line.account_ie_id.ie_account_line_ids)
                                    if total_line_pr != 100:
                                        raise ValidationError(_(ie_message))
                                    if line.account_ie_id.base_amount>0.00:
                                        base_amount_payment = line.account_ie_id.base_amount
                                    if pay.type_of_revenue_collection=='billing':
                                        base_amount_payment = payment.amount
                                        aspirants_flag = False
                                        comission_flag = False
                                    if not line.tax_ids:
                                            tax_flag=False
                                    else:
                                        pay_tax_id = line.tax_ids[0]

                                #####Comisiones de la factura
                                    if control==0 and pay.comission_cfdi>0.0:
                                        line_vals = {
                                                     'account_id': pay.income_bank_journal_id.default_debit_account_id.id,
                                                     'coa_conac_id': pay.income_bank_journal_id.conac_accrued_income_credit_account_id and pay.income_bank_journal_id.conac_accrued_income_credit_account_id.id or False,
                                                     'credit': pay.comission_cfdi, 
                                                     'conac_move' : True,
                                                     'amount_currency' : 0.0,
                                                     #'currency_id' : currency_id,                                     
                                                     'move_id' : move_id.id,
                                                     'payment_id': payment.id,
                                                     'date_maturity': payment.payment_date,
                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                     'dependency_id':move_id.dependancy_id.id if move_id.income_bank_journal_id.default_debit_account_id.dep_subdep_flag else False,
                                                     'sub_dependency_id':move_id.sub_dependancy_id.id if move_id.income_bank_journal_id.default_debit_account_id.dep_subdep_flag else False,
                                                     'name':com_message,

                                                                                 
                                                    }
                                        invoice_line.append((0,0,line_vals,{'context':{'check_move_validity': False}}))

                                        line_vals = {
                                                     'account_id': pay.income_bank_journal_id.default_debit_account_id.id,
                                                     'coa_conac_id': pay.income_bank_journal_id.conac_accrued_income_credit_account_id and pay.income_bank_journal_id.conac_accrued_income_credit_account_id.id or False,
                                                     'credit': pay.iva_comission_cfdi, 
                                                     'conac_move' : True,
                                                     'amount_currency' : 0.0,
                                                     #'currency_id' : currency_id,                                     
                                                     'move_id' : move_id.id,
                                                     'payment_id': payment.id,
                                                     'date_maturity': payment.payment_date,
                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                     'dependency_id':move_id.dependancy_id.id if move_id.income_bank_journal_id.default_debit_account_id.dep_subdep_flag else False,
                                                     'sub_dependency_id':move_id.sub_dependancy_id.id if move_id.income_bank_journal_id.default_debit_account_id.dep_subdep_flag else False,
                                                     'name':iva_message,
                                                                                 
                                                    }
                                        invoice_line.append((0,0,line_vals,{'context':{'check_move_validity': False}}))
                                        control=control+1
                                        taxes_cfdi=pay.comission_cfdi+pay.iva_comission_cfdi

                                    if pay_tax_id.amount>0.0:
                                        per_amount_total = base_amount_payment/total_payment
                                        real_amount=line.price_subtotal*per_amount_total
                                        pay_tax_amount = real_amount*(pay_tax_id.amount/100)


                                        line_vals = {
                                                     'account_id': pay_tax_id.iva_transferred_account_id.id,
                                                     #'coa_conac_id': payment.journal_id.conac_accrued_income_credit_account_id and payment.journal_id.conac_accrued_income_credit_account_id.id or False,
                                                     'debit': pay_tax_amount, 
                                                     'conac_move' : False,
                                                     'amount_currency' : 0.0,
                                                     'currency_id' : currency_id,                                     
                                                     'move_id' : move_id.id,
                                                     'payment_id': payment.id,
                                                     'date_maturity': payment.payment_date,
                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                     'dependency_id':move_id.dependancy_id.id if pay_tax_id.iva_transferred_account_id.dep_subdep_flag else False,
                                                     'sub_dependency_id':move_id.sub_dependancy_id.id if pay_tax_id.iva_transferred_account_id.dep_subdep_flag else False,
                                                                                 
                                                    }
                                        invoice_line.append((0,0,line_vals,{'context':{'check_move_validity': False}}))
                                                    
                                        line_vals = {
                                                     'account_id': pay_tax_id.iva_to_move_account_id.id,
                                                     #'coa_conac_id': payment.journal_id.conac_accrued_income_debit_account_id and payment.journal_id.conac_accrued_income_debit_account_id.id or False,
                                                     'credit': pay_tax_amount,
                                                     'conac_move' : False,
                                                     'amount_currency' : 0.0,
                                                     'currency_id' : currency_id,                                     
                                                     'move_id' : move_id.id,
                                                     'payment_id': payment.id,
                                                     'date_maturity': payment.payment_date,
                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                     'dependency_id':move_id.dependancy_id.id if pay_tax_id.iva_to_move_account_id.dep_subdep_flag else False,
                                                     'sub_dependency_id':move_id.sub_dependancy_id.id if pay_tax_id.iva_to_move_account_id.dep_subdep_flag else False,
                                                     
                                                                               
                                                    }
                                        invoice_line.append((0,0,line_vals,{'context':{'check_move_validity': False}}))

                                    if aspirants_flag:
                                        for account_line in line.account_ie_id.ie_account_line_ids.filtered(lambda x:x.percentage == 0.0):
                                            # Busqueda de CRI
                                            account_code = account_line.account_id.code
                                            cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                            line_vals = {
                                                            'account_id': account_line.account_id and account_line.account_id.id or False,
                                                             #'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                             'debit': aspirants_amount, 
                                                             'conac_move' : False,
                                                             'amount_currency' : 0.0,
                                                             'currency_id' : currency_id,                                     
                                                             'move_id' : move_id.id,
                                                             'payment_id': payment.id,
                                                             'date_maturity': payment.payment_date,
                                                             'partner_id': payment.partner_id.commercial_partner_id.id,
                                                             'dependency_id':move_id.dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                             'sub_dependency_id':move_id.sub_dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                             'account_ie_id': line.account_ie_id and line.account_ie_id.id or False,
                                                             'cri' : cri.name if cri else False
                                                        }
                                            invoice_line.append((0,0,line_vals))

                                            if(account_line.account_id.code[0]=='4' and account_line.account_id.budget_registry):
                                                # Busqueda de CRI
                                                account_code = account_line.account_id.code
                                                cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                                if payment_rec_flag and payment_acc_flag:
                                                    payment.move_line_ids = [(0, 0, {
                                                                                 'account_id': payment.journal_id.accrued_income_credit_account_id and payment.journal_id.accrued_income_credit_account_id.id or False,
                                                                                 'coa_conac_id': payment.journal_id.conac_accrued_income_credit_account_id and payment.journal_id.conac_accrued_income_credit_account_id.id or False,
                                                                                 'debit': aspirants_amount, 
                                                                                 'conac_move' : True,
                                                                                 'amount_currency' : 0.0,
                                                                                 'currency_id' : currency_id,                                     
                                                                                 'move_id' : move_id.id,
                                                                                 'payment_id': payment.id,
                                                                                'date_maturity': payment.payment_date,
                                                                                'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                'cri' : cri.name if cri else False
                                                                             }), 
                                                                    (0, 0, {
                                                                                 'account_id': payment.journal_id.accrued_income_debit_account_id and payment.journal_id.accrued_income_debit_account_id.id or False,
                                                                                 'coa_conac_id': payment.journal_id.conac_accrued_income_debit_account_id and payment.journal_id.conac_accrued_income_debit_account_id.id or False,
                                                                                 'credit': aspirants_amount,
                                                                                 'conac_move' : True,
                                                                                 'amount_currency' : 0.0,
                                                                                 'currency_id' : currency_id,                                     
                                                                                 'move_id' : move_id.id,
                                                                                 'payment_id': payment.id,
                                                                                 'date_maturity': payment.payment_date,
                                                                                 'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                 'cri' : cri.name if cri else False
                                                                             })]

                                                    payment.move_line_ids = [(0, 0, {
                                                                                 'account_id': payment.journal_id.recover_income_credit_account_id and payment.journal_id.recover_income_credit_account_id.id or False,
                                                                                 'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                                                 'debit': aspirants_amount, 
                                                                                 'conac_move' : True,
                                                                                 'amount_currency' : 0.0,
                                                                                 'currency_id' : currency_id,                                     
                                                                                 'move_id' : move_id.id,
                                                                                 'payment_id': payment.id,
                                                                                'date_maturity': payment.payment_date,
                                                                                'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                'cri' : cri.name if cri else False
                                                                             }), 
                                                                    (0, 0, {
                                                                                 'account_id': payment.journal_id.recover_income_debit_account_id and payment.journal_id.recover_income_debit_account_id.id or False,
                                                                                 'coa_conac_id': payment.journal_id.conac_recover_income_debit_account_id and payment.journal_id.conac_recover_income_debit_account_id.id or False,
                                                                                 'credit': aspirants_amount,
                                                                                 'conac_move' : True,
                                                                                 'amount_currency' : 0.0,
                                                                                 'currency_id' : currency_id,                                     
                                                                                 'move_id' : move_id.id,
                                                                                 'payment_id': payment.id,
                                                                                 'date_maturity': payment.payment_date,
                                                                                 'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                 'cri' : cri.name if cri else False
                                                                             })]
                                                # Afectar el presupuesto de ingresos
                                                if cri:
                                                    # Extraer campos
                                                    journal = payment.journal_id
                                                    affectation_amount = aspirants_amount
                                                    date_affectation = payment.payment_date
                                                    # Enviar a la funcion
                                                    self._income_budget_afectation(cri,journal,affectation_amount,date_affectation)

                                    if comission_flag:
                                        base_amount_payment = base_amount_payment-comission_amount
                                        for comission_line in comission.comission_line_ids.filtered(lambda x:x.comission_percentage > 0):
                                            # Busqueda de CRI
                                            account_code = comission_line.account_id.code
                                            cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                            line_vals = {
                                                            'account_id': comission_line.account_id and comission_line.account_id.id or False,
                                                             #'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                             'debit': comission_amount, 
                                                             'conac_move' : False,
                                                             'amount_currency' : 0.0,
                                                             'currency_id' : currency_id,                                     
                                                             'move_id' : move_id.id,
                                                             'payment_id': payment.id,
                                                             'date_maturity': payment.payment_date,
                                                             'partner_id': payment.partner_id.commercial_partner_id.id,
                                                             'dependency_id':move_id.dependancy_id.id if comission_line.account_id.dep_subdep_flag else False,
                                                             'sub_dependency_id':move_id.sub_dependancy_id.id if comission_line.account_id.dep_subdep_flag else False,
                                                             'account_ie_id': line.account_ie_id and line.account_ie_id.id or False,
                                                             'cri' : cri.name if cri else False
                                                        }
                                            invoice_line.append((0,0,line_vals))

                                            if(comission_line.account_id.code[0]=='4' and account_line.account_id.budget_registry):
                                                # Busqueda de CRI
                                                account_code = comission_line.account_id.code
                                                cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                                if payment_rec_flag and payment_acc_flag:
                                                    payment.move_line_ids = [(0, 0, {
                                                                                 'account_id': payment.journal_id.accrued_income_credit_account_id and payment.journal_id.accrued_income_credit_account_id.id or False,
                                                                                 'coa_conac_id': payment.journal_id.conac_accrued_income_credit_account_id and payment.journal_id.conac_accrued_income_credit_account_id.id or False,
                                                                                 'debit': comission_amount, 
                                                                                 'conac_move' : True,
                                                                                 'amount_currency' : 0.0,
                                                                                 'currency_id' : currency_id,                                     
                                                                                 'move_id' : move_id.id,
                                                                                 'payment_id': payment.id,
                                                                                'date_maturity': payment.payment_date,
                                                                                'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                'cri' : cri.name if cri else False
                                                                             }), 
                                                                    (0, 0, {
                                                                                 'account_id': payment.journal_id.accrued_income_debit_account_id and payment.journal_id.accrued_income_debit_account_id.id or False,
                                                                                 'coa_conac_id': payment.journal_id.conac_accrued_income_debit_account_id and payment.journal_id.conac_accrued_income_debit_account_id.id or False,
                                                                                 'credit': comission_amount,
                                                                                 'conac_move' : True,
                                                                                 'amount_currency' : 0.0,
                                                                                 'currency_id' : currency_id,                                     
                                                                                 'move_id' : move_id.id,
                                                                                 'payment_id': payment.id,
                                                                                 'date_maturity': payment.payment_date,
                                                                                 'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                 'cri' : cri.name if cri else False
                                                                             })]

                                                    payment.move_line_ids = [(0, 0, {
                                                                                 'account_id': payment.journal_id.recover_income_credit_account_id and payment.journal_id.recover_income_credit_account_id.id or False,
                                                                                 'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                                                 'debit': comission_amount, 
                                                                                 'conac_move' : True,
                                                                                 'amount_currency' : 0.0,
                                                                                 'currency_id' : currency_id,                                     
                                                                                 'move_id' : move_id.id,
                                                                                 'payment_id': payment.id,
                                                                                'date_maturity': payment.payment_date,
                                                                                'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                'cri' : cri.name if cri else False
                                                                             }), 
                                                                    (0, 0, {
                                                                                 'account_id': payment.journal_id.recover_income_debit_account_id and payment.journal_id.recover_income_debit_account_id.id or False,
                                                                                 'coa_conac_id': payment.journal_id.conac_recover_income_debit_account_id and payment.journal_id.conac_recover_income_debit_account_id.id or False,
                                                                                 'credit': comission_amount,
                                                                                 'conac_move' : True,
                                                                                 'amount_currency' : 0.0,
                                                                                 'currency_id' : currency_id,                                     
                                                                                 'move_id' : move_id.id,
                                                                                 'payment_id': payment.id,
                                                                                 'date_maturity': payment.payment_date,
                                                                                 'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                 'cri' : cri.name if cri else False
                                                                             })]
                                                # Afectar el presupuesto de ingresos
                                                if cri:
                                                    # Extraer campos
                                                    journal = payment.journal_id
                                                    affectation_amount = comission_amount
                                                    date_affectation = payment.payment_date
                                                    # Enviar a la funcion
                                                    self._income_budget_afectation(cri,journal,affectation_amount,date_affectation)

                                    for account_line in line.account_ie_id.ie_account_line_ids.filtered(lambda x:x.percentage > 0):
                                        if taxes_cfdi>0.0:
                                            base_amount_payment=payment.amount
                                            base_amount_payment=base_amount_payment-taxes_cfdi
                                        per_amount_total = base_amount_payment/total_payment
                                        real_amount=line.price_subtotal*per_amount_total
                                        price_per = (real_amount*account_line.percentage)/100
                                        # Busqueda de CRI
                                        account_code = account_line.account_id.code
                                        cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                        line_vals = {
                                                        'account_id': account_line.account_id and account_line.account_id.id or False,
                                                         #'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                         'debit': price_per, 
                                                         'conac_move' : False,
                                                         'amount_currency' : 0.0,
                                                         'currency_id' : currency_id,                                     
                                                         'move_id' : move_id.id,
                                                         'payment_id': payment.id,
                                                         'date_maturity': payment.payment_date,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'dependency_id':move_id.dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                         'sub_dependency_id':move_id.sub_dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                         'account_ie_id': line.account_ie_id and line.account_ie_id.id or False,
                                                         'cri' : cri.name if cri else False
                                                    }
                                        invoice_line.append((0,0,line_vals,{'context':{'check_move_validity': False}}))

                                        if(account_line.account_id.code[0]=='2' and account_line.account_id.code[1]=='1' and account_line.account_id.code[2]=='3'):
                                            # Busqueda de CRI
                                            account_code = account_line.account_id.code
                                            cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                            dep_213 = self.env['dependency']
                                            id_dep_213 = dep_213.search([('dependency','=', '764')])[0]
                                            sub_dep_213 = self.env[model_subdep]
                                            id_sub_dep_213 = sub_dep_213.search([('dependency_id','=', id_dep_213['id']),('sub_dependency','=','01')])[0] 
                                            line_vals = {
                                                        'account_id': account_line.account_id and account_line.account_id.id or False,
                                                         #'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                         'credit': price_per, 
                                                         'conac_move' : False,
                                                         'amount_currency' : 0.0,
                                                         'currency_id' : currency_id,                                     
                                                         'move_id' : move_id.id,
                                                         'payment_id': payment.id,
                                                         'date_maturity': payment.payment_date,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'dependency_id':move_id.dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                         'sub_dependency_id':move_id.sub_dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                         'account_ie_id': line.account_ie_id and line.account_ie_id.id or False,
                                                         'cri' : cri.name if cri else False
                                                    }
                                            invoice_line.append((0,0,line_vals,{'context':{'check_move_validity': False}}))
                                            line_vals = {
                                                        'account_id': account_line.account_id and account_line.account_id.id or False,
                                                         #'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                         'debit': price_per, 
                                                         'conac_move' : False,
                                                         'amount_currency' : 0.0,
                                                         'currency_id' : currency_id,                                     
                                                         'move_id' : move_id.id,
                                                         'payment_id': payment.id,
                                                         'date_maturity': payment.payment_date,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'dependency_id': id_dep_213['id'],
                                                         'sub_dependency_id': id_sub_dep_213['id'],
                                                         'account_ie_id': line.account_ie_id and line.account_ie_id.id or False,
                                                         'cri' : cri.name if cri else False
                                                    }
                                            invoice_line.append((0,0,line_vals,{'context':{'check_move_validity': False}}))


                                        if(account_line.account_id.code[0]=='4' and account_line.account_id.budget_registry):
                                                 # Busqueda de CRI
                                                account_code = account_line.account_id.code
                                                cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                                if payment_rec_flag and payment_acc_flag:
                                                    payment.move_line_ids = [(0, 0, {
                                                                                 'account_id': payment.journal_id.accrued_income_credit_account_id and payment.journal_id.accrued_income_credit_account_id.id or False,
                                                                                 'coa_conac_id': payment.journal_id.conac_accrued_income_credit_account_id and payment.journal_id.conac_accrued_income_credit_account_id.id or False,
                                                                                 'debit': price_per, 
                                                                                 'conac_move' : True,
                                                                                 'amount_currency' : 0.0,
                                                                                 'currency_id' : currency_id,                                     
                                                                                 'move_id' : move_id.id,
                                                                                 'payment_id': payment.id,
                                                                                'date_maturity': payment.payment_date,
                                                                                'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                'cri' : cri.name if cri else False
                                                                             }), 
                                                                    (0, 0, {
                                                                                 'account_id': payment.journal_id.accrued_income_debit_account_id and payment.journal_id.accrued_income_debit_account_id.id or False,
                                                                                 'coa_conac_id': payment.journal_id.conac_accrued_income_debit_account_id and payment.journal_id.conac_accrued_income_debit_account_id.id or False,
                                                                                 'credit': price_per,
                                                                                 'conac_move' : True,
                                                                                 'amount_currency' : 0.0,
                                                                                 'currency_id' : currency_id,                                     
                                                                                 'move_id' : move_id.id,
                                                                                 'payment_id': payment.id,
                                                                                 'date_maturity': payment.payment_date,
                                                                                 'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                 'cri' : cri.name if cri else False
                                                                             })]

                                                    payment.move_line_ids = [(0, 0, {
                                                                                 'account_id': payment.journal_id.recover_income_credit_account_id and payment.journal_id.recover_income_credit_account_id.id or False,
                                                                                 'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                                                 'debit': price_per, 
                                                                                 'conac_move' : True,
                                                                                 'amount_currency' : 0.0,
                                                                                 'currency_id' : currency_id,                                     
                                                                                 'move_id' : move_id.id,
                                                                                 'payment_id': payment.id,
                                                                                'date_maturity': payment.payment_date,
                                                                                'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                'cri' : cri.name if cri else False
                                                                             }), 
                                                                    (0, 0, {
                                                                                 'account_id': payment.journal_id.recover_income_debit_account_id and payment.journal_id.recover_income_debit_account_id.id or False,
                                                                                 'coa_conac_id': payment.journal_id.conac_recover_income_debit_account_id and payment.journal_id.conac_recover_income_debit_account_id.id or False,
                                                                                 'credit': price_per,
                                                                                 'conac_move' : True,
                                                                                 'amount_currency' : 0.0,
                                                                                 'currency_id' : currency_id,                                     
                                                                                 'move_id' : move_id.id,
                                                                                 'payment_id': payment.id,
                                                                                 'date_maturity': payment.payment_date,
                                                                                 'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                                 'cri' : cri.name if cri else False
                                                                             })]
                                                    # Afectar el presupuesto de ingresos
                                                    if cri:
                                                        # Extraer campos
                                                        journal = payment.journal_id
                                                        affectation_amount = price_per
                                                        date_affectation = payment.payment_date
                                                        # Enviar a la funcion
                                                        self._income_budget_afectation(cri,journal,affectation_amount,date_affectation)

                                                    
                            if type_captation != 'deposit_cer':
                                for line_inv in pay.invoice_line_ids:
                                    per_amount_total = payment.amount/total_payment
                                    real_amount=line_inv.price_subtotal*per_amount_total
                                    line_vals = {
                                                         'account_id': line_inv.account_id and line_inv.account_id.id or False,
                                                         #'coa_conac_id': payment.journal_id.conac_recover_income_debit_account_id and payment.journal_id.conac_recover_income_debit_account_id.id or False,
                                                         'credit': real_amount,
                                                         'conac_move' : False,
                                                         'amount_currency' : 0.0,
                                                         'currency_id' : currency_id,                                     
                                                         'move_id' : move_id.id,
                                                         'payment_id': payment.id,
                                                         'date_maturity': payment.payment_date,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'dependency_id':move_id.dependancy_id.id if line_inv.account_id.dep_subdep_flag else False,
                                                         'sub_dependency_id':move_id.sub_dependancy_id.id if line_inv.account_id.dep_subdep_flag else False,
                                                       
                                                     }
                                    invoice_line.append((0,0,line_vals,{'context':{'check_move_validity': False}}))
                            else:
                                for line_inv in pay.line_ids:
                                    if line_inv.debit > 0:

                                        line_vals = {
                                                         'account_id': line_inv.account_id and line_inv.account_id.id or False,
                                                         #'coa_conac_id': payment.journal_id.conac_recover_income_debit_account_id and payment.journal_id.conac_recover_income_debit_account_id.id or False,
                                                         'debit': line_inv.debit,
                                                         'conac_move' : False,
                                                         'amount_currency' : 0.0,
                                                         'currency_id' : currency_id,                                     
                                                         'move_id' : move_id.id,
                                                         'payment_id': payment.id,
                                                         'date_maturity': payment.payment_date,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'dependency_id':move_id.dependancy_id.id if line_inv.account_id.dep_subdep_flag else False,
                                                         'sub_dependency_id':move_id.sub_dependancy_id.id if line_inv.account_id.dep_subdep_flag else False,
                                                       
                                                     }
                                        invoice_line.append((0,0,line_vals))
                                        
                                    else:
                                        
                                        line_vals = {
                                                         'account_id': line_inv.account_id and line_inv.account_id.id or False,
                                                         #'coa_conac_id': payment.journal_id.conac_recover_income_debit_account_id and payment.journal_id.conac_recover_income_debit_account_id.id or False,
                                                         'credit': line_inv.credit,
                                                         'conac_move' : False,
                                                         'amount_currency' : 0.0,
                                                         'currency_id' : currency_id,                                     
                                                         'move_id' : move_id.id,
                                                         'payment_id': payment.id,
                                                         'date_maturity': payment.payment_date,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'dependency_id':move_id.dependancy_id.id if line_inv.account_id.dep_subdep_flag else False,
                                                         'sub_dependency_id':move_id.sub_dependancy_id.id if line_inv.account_id.dep_subdep_flag else False,
                                                       
                                                     }
                                        invoice_line.append((0,0,line_vals))

                    payment.with_context(check_move_validity=False).move_line_ids = invoice_line

                    for pay in income_invoices:
                        move_id.invoice_uuid = pay.invoice_uuid
                        move_id.cfdi_serie = pay.cfdi_serie
                        move_id.folio_cfdi = pay.folio_cfdi
                        move_id.ref = payment.bank_reference
                        move_id.concept = pay.name
                        pay.invoice_payment_ref = payment.communication
                        move_id.cfdi_status = pay.cfdi_status
                        move_id.type_of_revenue_collection = pay.type_of_revenue_collection
                        payment.cfdi_folio=pay.folio_cfdi
                        payment.cfdi_serie=pay.cfdi_serie
                        payment.l10n_mx_edi_origin=pay.invoice_uuid

                        for line in payment.with_context(check_move_validity=False).move_line_ids:
                            if line.account_id.dep_subdep_flag and (line.account_id.code[0:3]=='120' or line.account_id.code[0:3]=='210'):
                                line.dependency_id = line.move_id.dependancy_id.id
                                line.sub_dependency_id = line.move_id.sub_dependancy_id.id
                    
                    for line in payment.with_context(check_move_validity=False).move_line_ids:
                        if line.account_id == line.move_id.journal_id.default_debit_account_id:
                            line.ref = payment.bank_reference
                        if line.name == False or 'omisión' not in line.name: 
                            line.name = 'Pago cliente: '+line.payment_id.communication
                        line.folio_related = line.move_id.folio_cfdi
                        line.serie_related = line.move_id.cfdi_serie
                        line.uuid_related = line.move_id.invoice_uuid
                        if line.account_id.id == payment.journal_id.default_debit_account_id.id and payment.payment_type=='outbound':
                            for pay in income_invoices:
                                line.account_id = pay.journal_id.default_debit_account_id.id

        return result
   
    def _income_budget_afectation(self,cri,journal,affectation_amount,date_affectation):
        if cri and journal and affectation_amount and date_affectation:
            # Objetos
            obj_affetation = self.env[model_budget]
            obj_logbook = self.env['income.adequacies.function']
            obj_fiscal_year = self.env['account.fiscal.year']
            # Capos de  afectación al presupuesto
            type_affectation = 'eje-rec'
            date_accrued = False
            cri_id = cri.id
            # Mandar datos a la clase de afectación
            obj_affetation.search([]).affectation_income_budget(type_affectation,cri_id,date_accrued,date_affectation,affectation_amount)
            # Guarda la bitacora de la adecuación
            # Busca el id del año fiscal
            year = date_affectation.year
            fiscal_year_id = obj_fiscal_year.search([('name', '=', year)])
            type_cri = 'income'
            movement = 'for_exercising_collected'
            origin_type = 'income-cob'
            id_cri = self.name
            journal_id = journal.id
            update_date_record = datetime.today()
            update_date_income = datetime.today()
            obj_logbook.search([]).logbook_income_budget(fiscal_year_id.id,cri_id,date_affectation,type_cri,movement,origin_type,id_cri,journal_id,update_date_record,affectation_amount,update_date_income)
            
        

    def action_register_payment(self):
        res = super(account_payment,self).action_register_payment()
        if res:
            active_ids = self.env.context.get('active_ids')
            if len(active_ids) == 1:
                move_id = self.env[model_move].browse(active_ids)
                context = dict(res.get('context') or {})
                if move_id.invoice_date_conciliation:
                    date_invoice = move_id.invoice_date_conciliation
                elif move_id.type == 'out_refund':
                    date_invoice = date.today()
                else:
                    date_invoice = move_id.date
                
                context.update({'default_dependancy_id' : move_id.dependancy_id and move_id.dependancy_id.id or False,
                                                  'default_sub_dependancy_id' : move_id.sub_dependancy_id and move_id.sub_dependancy_id.id or False,
                                                  'default_l10n_mx_edi_payment_method_id' : move_id.l10n_mx_edi_payment_method_id and move_id.l10n_mx_edi_payment_method_id.id or False,
                                                  'default_sub_origin_resource_id' : move_id.sub_origin_resource_id and move_id.sub_origin_resource_id.id or False,
                                                  'default_type_of_revenue_collection' : move_id.type_of_revenue_collection,
                                                  'default_bank_reference' : move_id.ref,
                                                  'default_payment_date': date_invoice,
                                                  'default_amount': move_id.amount_total,
                                                  'default_payment_method_id': 1,
                                                  'default_communication': move_id.name,
                                                  'default_cfdi_serie': move_id.cfdi_serie,
                                                  'default_cfdi_folio': move_id.folio_cfdi,
                                                  })
                if move_id.income_bank_journal_id:
                    context.update({'default_journal_id':move_id.income_bank_journal_id.id})
                res.update({'context':context})    
        return res

class payment_register(models.TransientModel):
    
    _inherit = 'account.payment.register'
    
    
    def _prepare_payment_vals(self, invoices):
        res = super(payment_register,self)._prepare_payment_vals(invoices)
        if invoices and len(invoices)==1:
            move_id = invoices[0]
            if move_id.type_of_revenue_collection == 'billing':
                move_id.date = move_id.invoice_date
            elif move_id.type_of_revenue_collection:
                move_id.date = move_id.settlement_date
            elif move_id.type == 'out_refund':
                move_id.date = date.today()
            res.update({'dependancy_id' : move_id.dependancy_id and move_id.dependancy_id.id or False,
                                                  'sub_dependancy_id' : move_id.sub_dependancy_id and move_id.sub_dependancy_id.id or False,
                                                  'l10n_mx_edi_payment_method_id' : move_id.l10n_mx_edi_payment_method_id and move_id.l10n_mx_edi_payment_method_id.id or False,
                                                  'sub_origin_resource_id' : move_id.sub_origin_resource_id and move_id.sub_origin_resource_id.id or False,
                                                  'bank_reference' : move_id.ref,
                                                  'type_of_revenue_collection': move_id.type_of_revenue_collection,
                                                  'payment_date': move_id.invoice_date_conciliation,
                                                  'amount': move_id.amount_total,
                                                  'payment_method_id': 1,
                                                  'communication': move_id.name,
                                                  'cfdi_serie': move_id.cfdi_serie,
                                                  'cfdi_folio': move_id.folio_cfdi,
                                                  })
        return res
