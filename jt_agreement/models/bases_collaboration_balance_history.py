import calendar
from odoo import models, fields, api, _
from datetime import date, datetime
from odoo.exceptions import UserError, ValidationError

class CollaborationBalanceHistory(models.Model):
    _name = 'bases.collaboration.balance.history'
    _description = 'Collaboration Balance History'
    _rec_name = 'bases_collaboration_id'

    convention_no = fields.Char(related="bases_collaboration_id.convention_no",string='Convention Number')
    date = fields.Date(string='Date', required=True)
    balance = fields.Float(string='Balance', required=True)
    rate = fields.Float(string='Rate', required=True, digits=(12, 4))


    def create_balance_register(self, balance, rate, id, date):
        move = {
        'date': date,
        'balance': balance,
        'rate': rate,
        'bases_collaboration_id': id
        }

        self.create(move)

    # Obtiene el valor promedio de los saldos de los registros en el mes y año seleccionado
    def get_average_balance_history_in_month(self, month, year, bases_collaboration_id):
        # Realiza la busqueda en el rango de primer dia y ultimo dia del mes
        records = self.search([('bases_collaboration_id', '=', bases_collaboration_id), ('date', '>=', date(
            year, month, 1)), ('date', '<=', date(year, month, calendar.monthrange(year, month)[1]))])

        # Si no hay registro mandar mensaje de error
        # if len(records) == 0:
        #     raise UserError(
        #         _('"No se encontraron registros de saldos para la fecha seleccionada'))

        if records:
        # Sacamos el promedio de los saldos de los registros encontrados
            total_amount = 0
            for rec in records:
                total_amount += rec.balance
            average = total_amount / len(records)

            return average
        
        return False
    
