from odoo import models, fields

class CancelWizard(models.TransientModel):

    _name = 'cancel.wizard'

    model = fields.Char('Model')
    record_id = fields.Integer('Record Id')
    reason_for_cancellation = fields.Text('Reason For Rejection')

    def cancel(self):
        self.env[self.model].browse([self.record_id]).cancel(self.reason_for_cancellation)

def display_modal(self, title='Cancelar'):
    """
    Acción para obtener los motivos de rechazo
    """

    return {
        'name': title,
        'res_model': 'cancel.wizard',
        'view_mode': 'form',
        'view_id': self.env.ref('siif_supplier_payment.cancel_wizard_form').id,
        'context': {
            'default_model': self._name,
            'default_record_id': self.id,
        },
        'target': 'new',
        'type': 'ir.actions.act_window',
    }