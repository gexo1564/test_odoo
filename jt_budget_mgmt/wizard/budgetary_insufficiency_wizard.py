# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields
from datetime import datetime
import logging
class BudegtInsufficiencWiz(models.TransientModel):

    _name = 'budget.insufficien.wiz'
    _description = 'Budgetary Insufficienc'

    msg = fields.Text('Message')
    is_budget_suf = fields.Boolean(default=False)
    move_id = fields.Many2one('account.move','Move')
    move_ids = fields.Many2many('account.move', 'rel_wizard_budget_move', 'move_id', 'rel_wiz_move_id')
    insufficient_move_ids = fields.Many2many('account.move', 'rel_wizard_budget_insufficient_move', 'move_id', 'rel_wiz_move_id')
    
    def action_ok(self):
        move_str_msg_dict = self._context.get('move_str_msg_dict')
        if self.insufficient_move_ids and not self.move_id:
            for move in self.insufficient_move_ids:
                move.payment_state = 'rejected'
                move.reason_rejection = move_str_msg_dict.get(str(move.id))
            self.action_budget_allocation()
        else:
            self.move_id.payment_state = 'rejected'
            self.move_id.reason_rejection = self.msg

    # Este método decrementa el disponible en las tablas de presupuesto
    def decrease_available_amount(self):
        for line in self.move_id.invoice_line_ids:
            budget_line_links = []
            if line.program_code_id and line.price_total != 0:
                amount = 0
                if line.debit:
                    amount = line.debit + line.tax_price_cr
                else:
                    amount = line.credit + line.tax_price_cr

                # Se obtiene el trimestre correspondiente al mes actual
                b_month = self.move_id.invoice_date.month
                # Se ejecuta la actualizacion sobre la linea de presupuesto y se
                # obtinen las lineas de los trimestres afectados
                budget_lines = self.env['expenditure.budget.line'].update_expenditure_month(
                    line.program_code_id.id,
                    b_month,
                    amount
                )
                # Se almacena un registro de cada trimestre afectado
                for b_line in budget_lines:
                    budget_line_links.append(
                        (0, 0, {
                            'budget_line_id': b_line['budget_line_id'],
                            'account_move_line_id': line.id,
                            'amount': b_line['amount']
                        })
                    )
            line.budget_line_link_ids = budget_line_links

