from odoo import models, fields
import logging

class WizardForWarningsBudget(models.TransientModel):
    _name = 'wizard.for.warnings.budget'
    _description = 'Wizard for warnings'

    message = fields.Char('Message')