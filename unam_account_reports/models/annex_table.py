from odoo import api, fields, models, _


class AnnexTable(models.Model):
    _name = "annex.table"
    _description = 'Annexe (custom) Table'

    name = fields.Char()
    short_name = fields.Char(size=10)
    type = fields.Selection(selection=[('expense_per_program_code_item', 'Expense per Program Code Item'),
                                       ('unam_account_group', 'By groups of accounts'),
                                       ('expense_per_dependency', 'Expense per Dependency'),
                                       ('expense_per_provider', 'Expense per Provider'),
                                       ('reuse_annexes', 'Reuse Information from Other Annexes'),
                                       ('add_annexes', 'Add an Annex'),
                                       ('subtract_annexes', 'Subtract an Annex'), ])
    groupby1 = fields.Char(string='Section', help='Sets field that will be used as section for the custom table report')
    groupby2 = fields.Char(string='Row', help='Sets field that will be used as row(s) for the custom table report')
    groupby3 = fields.Char(string='Column',
                           help='Sets field that will be used as column(s) for the custom table report')
    has_row_grand_total = fields.Boolean('Add grand total per row')
    has_column_grand_total = fields.Boolean('Add grand total per column')
    show_first_ten_only = fields.Boolean('Show Only First 10')

    # -----------------------------------------------------------------------
    # 1. Expense per program code item
    # -----------------------------------------------------------------------
    item_ids = fields.Many2many(comodel_name='expenditure.item', string='Items')

    resource_origin_id = fields.Many2many(comodel_name='resource.origin', string='Resource Origin')

    # -----------------------------------------------------------------------
    # 2. Expense per dependency
    # -----------------------------------------------------------------------
    dependency_ids = fields.Many2many(comodel_name='dependency', string='Dependency')

    # -----------------------------------------------------------------------
    # 5. By groups of accounts
    # -----------------------------------------------------------------------
    unam_account_group_ids = fields.Many2many(comodel_name='unam.account.group', string='UNAM Account Groups')

    # -----------------------------------------------------------------------
    # 8. Reuse Information from Other Annexes
    # -----------------------------------------------------------------------
    annex_ids = fields.Many2many(comodel_name='account.financial.html.report.line',
                                 relation='annex_table_reuse_annexes_rel', string='Annexes',
                                 domain=[('annex_number', '!=', None)])

    # -----------------------------------------------------------------------
    # Add and subtract Annexes, tables 7A Y 7B
    # -----------------------------------------------------------------------
    added_annex_id = fields.Many2one(comodel_name='account.financial.html.report.line', string='Annexes',
                                     domain=[('annex_number', '!=', None)])

    subtracted_annex_id = fields.Many2one(comodel_name='account.financial.html.report.line', string='Annexes',
                                          domain=[('annex_number', '!=', None)])

    @api.model
    def get_specific_domain(self):
        self.ensure_one()
        domain = []
        if self.type in ['expense_per_program_code_item', 'expense_per_dependency',
                         'expense_per_provider']:
            expense_type_id = self.env.ref('account.data_account_type_expenses').id
            domain.extend([('account_id.user_type_id', '=', expense_type_id)])

        if self.type == 'expense_per_program_code_item':
            domain.extend([('expenditure_item_id', 'in', self.item_ids.ids),
                           ('resource_origin_id', 'in', self.resource_origin_id.ids)])

        if self.type == 'expense_per_dependency':
            domain.extend([('resource_origin_id', 'in', self.resource_origin_id.ids), 
                           ('expenditure_item_id', 'in', self.item_ids.ids)])
            if len(self.dependency_ids.ids) > 0:
                domain.extend([('program_code_dependency_id', 'in', self.dependency_ids.ids)]),

        if self.type == 'unam_account_group':
            domain.extend([('account_id', 'in', self.unam_account_group_ids.accounts_ids.ids)])

        if self.type == 'expense_per_provider':
            domain.extend([('expenditure_item_id', 'in', self.item_ids.ids)])

        return domain
