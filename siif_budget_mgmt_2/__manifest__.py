{
    'name': 'SIIF Budget Management',
    'summary': 'SIIF Budget is used to improve functional requirements through inherit',
    'version': '13.0.0.1.1',
    'category': 'Accounting',
    'author': 'Jupical Technologies Pvt. Ltd.',
    'maintainer': 'Jupical Technologies Pvt. Ltd.',
    'website': 'http://www.jupical.com',
    'license': 'AGPL-3',
    'depends': ['jt_budget_mgmt','jt_income','siif_account_design'],
    'data': [
        'views/invoice_inh.xml',
        'views/countability_equivalence.xml',
        'security/ir.model.access.csv',
        'views/exclude_program_code_adequacies.xml',

        'views/conversion_institutional_activity.xml',
        'views/conversion_resource_origin.xml',
        'views/conversion_expense_type.xml',
        'views/conversion_conpp.xml',
        'views/conversion_geographic_location.xml',
        'views/menus.xml',
    ],
    'demo': [

    ],
    'application': False,
    'installable': True,
    'auto_install': True,
}
