# -*- coding: utf-8 -*-
from distutils import log
from email.policy import default
import json
import logging
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError

REQUEST_INCOME_RECOGNITION_MODEL = 'request.income.recognition'
REQUEST_INCOME_RECOGNITION_WIZARD = 'request.income.recognition.wizard'
WITH_STEP_FORM_WIZARD = 'siif_income.wizard_with_step_form'
INCOME_COORDINATOR_USER = 'jt_income.group_income_project_coordinator_user'
ACTION_WINDOW = 'ir.actions.act_window'
class BankMovements(models.TransientModel):
    _name = REQUEST_INCOME_RECOGNITION_WIZARD


    state = fields.Selection(
        [
            ('step1', 'Invoice selection'),
            ('step2', 'Credit notes selection'),
            ('step3', 'Actions selections'),
            ('step4', 'Tickets selection'),
        ], default='step1',
    )
    dependency_id = fields.Many2one('dependency', string='Dependence', domain=lambda self: self.get_domain_dependency())
    sub_dependency_id = fields.Many2one('sub.dependency', "Subdependency")
    domain_sub_dependency = fields.Char(compute="_compute_domain_sub_dependency", store=False)
    bank_movements_ids = fields.Many2many(
        'bank.movements',
        'bank_movements_income_recognition_wizard_rel',
        'request_income_recognition_wizard_id',
        'bank_movements_id'
    )
    total_bank_movements = fields.Monetary(string='Total Deposits',compute='_compute_total_bank_movements',readonly=True)
    invoice_ids =  fields.Many2many(
        'account.move',
        'account_move_income_recognition_wizard_rel',
        'request_income_recognition_wizard_id',
        'account_move_id',
        string='Invoices',
    )
    total_invoices = fields.Monetary(string='Total Invoices',compute='_compute_total_invoices',readonly=True)
    currency_id = fields.Many2one('res.currency', default=lambda self: self.env.company.currency_id)
    action_ids = fields.Many2many(
        'income.recognition.actions',
        'request_income_recognition_wizard_rel',
        'request_income_recognition_wizard_id',
        'action_id',
        string='Actions',
    )
    dep_subdep_flag = fields.Boolean(default = False)
    reject_reason = fields.Text('Reason for rejection')
    message = fields.Char('Message')
    type = fields.Selection([
        ('recognized', 'Recognized'),
        ('partial_recognized', 'Partial recognized')
    ], "Type", default="")
    credit_note_ids = fields.Many2many(
        'account.move',
        'account_move_credit_note_wizard_rel',
        'request_income_recognition_wizard_id',
        'credit_note_id',
        string="Credit Notes",
        domain=[('type', '=', 'entry')]
    )
    total_credit_notes = fields.Monetary(string='Total Credit Notes',compute='_compute_total_credit_notes',readonly=True)
    tickets_ids = fields.Many2many(
        'tickets.store',
        'tickets_store_tickets_ids_wizard_rel',
        'request_income_recognition_wizard_id',
        'tickets_id',
        string="Tickets",
    )
    total_tickets = fields.Monetary(string='Total Tickets',compute='_compute_total_tickets',readonly=True)
    

    def get_domain_dependency(self):
        dep = self.env.user.dependency_income_ids.ids
        dep += [d.id for d in self.env.user.sub_dependency_income_ids.dependency_id]

        return [('id', 'in', dep)]
    

    @api.depends('bank_movements_ids', 'bank_movements_ids.amount_interest')
    def _compute_total_bank_movements(self):
        for record in self:
            record.total_bank_movements = sum(x.amount_interest for x in record.bank_movements_ids)


    @api.depends('invoice_ids')
    def _compute_domain_credit_notes(self):
        domain = [
            ('cfdi_serie', 'in', self.invoice_ids.mapped('cfdi_serie')),
            ('folio_cfdi', 'in', self.invoice_ids.mapped('folio_cfdi')),
            ('state', '=', 'draft'),
            ('credit_note_payment_state', '=', False),
            ('type', '=', 'entry'),
            ('type_of_registry_payment', '=', 'credit_note')
        ]

        self.domain_credit_notes = json.dumps(domain)

    domain_credit_notes = fields.Char(compute="_compute_domain_credit_notes", store=False)


    @api.depends('invoice_ids')
    def _compute_domain_tickets(self):
        domain = [
            ('serie', 'in', self.invoice_ids.mapped('cfdi_serie')),
            ('folio_cfd', 'in', self.invoice_ids.mapped('folio_cfdi')),
            ('ticket_store_state', '=', False)
        ]

        self.domain_tickets = json.dumps(domain)

    domain_tickets = fields.Char(compute="_compute_domain_tickets", store=False)

  

    @api.depends('dependency_id')
    def _compute_domain_sub_dependency(self):
        for record in self:
            domain = [('dependency_id', '=', self.dependency_id.id)]
            if self.dependency_id.id not in self.env.user.dependency_income_ids.ids:
                domain.append(('id', 'in', self.env.user.sub_dependency_income_ids.ids))
            domain.append(('sub_dependency', '!=', '00'))

            record.domain_sub_dependency = json.dumps(domain)

    domain_invoices = fields.Char(compute="_compute_domain_invoices", store=False)


    @api.depends('dependency_id', 'sub_dependency_id')
    def _compute_domain_invoices(self):
        domain = []
        default_domain = self._get_default_domain_invoices()

        is_group_income_admin_user = self.env.user.has_group('jt_income.group_income_admin_user')
        is_group_income_finance_user = self.env.user.has_group('jt_income.group_income_finance_user')
        is_group_income_project_coordinator_user = self.env.user.has_group(INCOME_COORDINATOR_USER)
        is_group_income_dependence_user = self.env.user.has_group('jt_income.group_income_dependence_user')

        if is_group_income_admin_user or is_group_income_finance_user or is_group_income_dependence_user:
            domain = default_domain 

        if is_group_income_project_coordinator_user:
            poi_conacyt_list = [move.payment_of_id.id for move in self.account_move if move.payment_of_id.description.upper().startswith("CONACYT")]
            default_domain.append(('payment_of_id.id', 'in', poi_conacyt_list))
            domain = default_domain

        self.domain_invoices = json.dumps(domain)


    def _get_currency_name(self):
        currency_name = self.currency_id.name
        currency_name_mov = [record.currency_related for record in self.bank_movements_ids][-1]
        if currency_name_mov:
            currency_name = currency_name_mov
        return currency_name


    def _get_default_domain_invoices(self):
        return [('dependancy_id', '=', self.dependency_id.id),
                ('sub_dependancy_id', '=', self.sub_dependency_id.id),
                ('type_of_revenue_collection', '=', 'billing'),
                ('invoice_payment_state', '=', 'not_paid'),
                ('is_recognized', '=', False),
                ('currency_id.name', '=', self._get_currency_name()),
                ('state', '=', 'posted'),
                ('type', '=', 'out_invoice')]
              
    
    def open_wizard(self, bank_movements):
        # Se valida que todas los depositos no se encuentren ya en proceso de reconocimiento
        rer_obj = self.env[REQUEST_INCOME_RECOGNITION_MODEL]

        request_names = rer_obj.search([('bank_movements_ids.id', 'in', bank_movements.ids)]).mapped('name')
        amounts = [f'${rec.amount_interest}' for rec in bank_movements if rec.type_movement in ('recognized', 'inprocess', 'reclassified')]

        if amounts:
            raise ValidationError(_('The following deposits have already been recognized or are in process or were reclassified in the request %s: %s') % (', '.join(request_names), ', '.join(amounts)))

        self = self.create({'bank_movements_ids': [(6, 0, bank_movements.ids)]})
        return self.select_invoice()


    def select_invoice(self):
        # Vacia todas las dependencias y subdependencias de las solicitudes previas
        income_actions = self.env['income.recognition.actions'].search([])

        for actions in income_actions:
             actions.dependency_id = None
             actions.sub_dependency_id = None  
  
        return {
            'type': ACTION_WINDOW,
            'res_model': REQUEST_INCOME_RECOGNITION_WIZARD,
            'name': _('Invoice selection'),
            'view_mode': 'form',
            'view_type': 'form',
            'views': [(self.env.ref(WITH_STEP_FORM_WIZARD).id, 'form')],
            'domain': [],
            'context': {
                'default_state': 'step1',
                'default_bank_movements_ids': [(6, 0, self.bank_movements_ids.ids)],
                'default_dependency_id': self.dependency_id.id or False,
                'default_sub_dependency_id' : self.sub_dependency_id.id or False,
                'default_invoice_ids': [(6, 0, self.invoice_ids.ids)],
            },
            'target': 'new',
        }


    def select_note(self):
        # Actualizar el banco a la factura
        self.update_bank_to_invoice()
              
        return {
            'type': ACTION_WINDOW,
            'res_model': REQUEST_INCOME_RECOGNITION_WIZARD,
            'name': _('Credit notes selection'),
            'view_mode': 'form',
            'view_type': 'form',
            'views': [(self.env.ref(WITH_STEP_FORM_WIZARD).id, 'form')],
            'domain': [],
            'context': {
                'default_state': 'step2',
                'default_bank_movements_ids': [(6, 0, self.bank_movements_ids.ids)],
                'default_dependency_id': self.dependency_id.id or False,
                'default_sub_dependency_id' : self.sub_dependency_id.id or False,
                'default_invoice_ids': [(6, 0, self.invoice_ids.ids)],
                'default_credit_note_ids': [(6, 0, self.credit_note_ids.ids)],
            },
            'target': 'new',
        }
    

    def update_bank_to_invoice(self):
        income_bank_journal_id = self.bank_movements_ids[-1].income_bank_journal_id.id
        self.invoice_ids.update({'income_bank_journal_id': income_bank_journal_id})
              

    def select_action(self):
        is_group_income_project_coordinator_user = self.env.user.has_group(INCOME_COORDINATOR_USER)
        is_group_income_dependence_user = self.env.user.has_group('jt_income.group_income_dependence_user')
        
        if is_group_income_project_coordinator_user or is_group_income_dependence_user:
            return self.select_ticket()
    
        return {
            'type': ACTION_WINDOW,
            'res_model': REQUEST_INCOME_RECOGNITION_WIZARD,
            'name': _('Actions selection'),
            'view_mode': 'form',
            'view_type': 'form',
            'views': [(self.env.ref(WITH_STEP_FORM_WIZARD).id, 'form')],
            'domain': [],
            'context': {
                'default_state': 'step3',
                'default_action_ids': [(6, 0, self.action_ids.ids)],
                'default_bank_movements_ids': [(6, 0, self.bank_movements_ids.ids)],
                'default_dependency_id': self.dependency_id.id or False,
                'default_sub_dependency_id' : self.sub_dependency_id.id or False,
                'default_invoice_ids': [(6, 0, self.invoice_ids.ids)],
                'default_credit_note_ids': [(6, 0, self.credit_note_ids.ids)],
                'default_tickets_ids': [(6, 0, self.tickets_ids.ids)],
            },
            'target': 'new',
        }


    def select_ticket(self):
       
        return {
            'type': ACTION_WINDOW,
            'res_model': REQUEST_INCOME_RECOGNITION_WIZARD,
            'name': _('Tickets selection'),
            'view_mode': 'form',
            'view_type': 'form',
            'views': [(self.env.ref(WITH_STEP_FORM_WIZARD).id, 'form')],
            'domain': [],
            'context': {
                'default_state': 'step4',
                'default_action_ids': [(6, 0, self.action_ids.ids)],
                'default_bank_movements_ids': [(6, 0, self.bank_movements_ids.ids)],
                'default_dependency_id': self.dependency_id.id or False,
                'default_sub_dependency_id' : self.sub_dependency_id.id or False,
                'default_invoice_ids': [(6, 0, self.invoice_ids.ids)],
                'default_credit_note_ids': [(6, 0, self.credit_note_ids.ids)],
                'default_tickets_ids': [(6, 0, self.tickets_ids.ids)],
            },
            'target': 'new',
        }    

    
    def create_request(self):
        # Validate the data before creating the request
        self.validate_request()

        # Update the state of the bank movements
        self.update_bank_movement_state('inprocess')

        # Check if there is a previous request for this deposit
        previous_request = self.in_previous_request()

        # Create a new request or update an existing one
        if previous_request:
            request = self.update_previous_request(previous_request, 'update')
        else:
            request = self.create_new_request()

        # Update the state of credit notes and tickets if applicable
        if request.has_credit_note():
            self.update_credit_note_state('in_request')
        if request.has_ticket():
            self.update_ticket_state('in_request')

        # Show a message to the user with information about the new or updated request
        return self.request_message_wizard(request.type_request, request)
    

    def create_new_request(self):
        type_request = 'new'

        vals = {
            'name': self.generate_name_sequence(),
            'type': self.type,
            'state': 'draft',
            'bank_movements_ids': [(6, 0, self.bank_movements_ids.ids)],
            'invoice_ids': [(6, 0, self.invoice_ids.ids)],
            'action_ids': [(6, 0, self.action_ids.ids)],
            'credit_note_ids': [(6, 0, self.credit_note_ids.ids)],
            'tickets_ids': [(6, 0, self.tickets_ids.ids)],
            'type_request': type_request,
        }

        is_group_income_project_coordinator_user = self.env.user.has_group(INCOME_COORDINATOR_USER)
        if is_group_income_project_coordinator_user:
            vals['group'] = 'income_project_coordinator'
        if self.has_invoice():
            dependency_id, sub_dependency_id = self.get_dependancy_subdependancy()
            vals['dependency_id'] = dependency_id
            vals['sub_dependency_id'] = sub_dependency_id

        return self.env[REQUEST_INCOME_RECOGNITION_MODEL].create(vals)


    def update_previous_request(self, previous_request, type_request):
            previous_request.write({
                'bank_movements_ids': [(4, val.id) for val in self.bank_movements_ids],
                'invoice_ids': [(4, val.id) for val in self.invoice_ids],
                'type_request': type_request,
            })
                
            return previous_request
  

    def has_invoice(self):
        if self.invoice_ids:
            return True


    def has_credit_note(self):
        if self.credit_note_ids:
            return True    


    def has_bank_movement(self):
        if self.bank_movements_ids:
            return True
        

    def has_action(self):
        if self.action_ids:
            return True
         

    def get_dependancy_subdependancy(self):
        if self.invoice_ids.dependancy_id.ids[-1]:
            dependency_id = self.invoice_ids.dependancy_id.ids[-1]
        if self.invoice_ids.sub_dependancy_id.ids[-1]:    
            sub_dependency_id = self.invoice_ids.sub_dependancy_id.ids[-1]

        return dependency_id, sub_dependency_id


    def in_previous_request(self):
        rer_obj = self.env[REQUEST_INCOME_RECOGNITION_MODEL]
        invoices = self.invoice_ids.ids
        previous_request = rer_obj.search([('state', '=', 'draft'), ('invoice_ids', 'in', invoices)], limit=1)

        return previous_request   


    def generate_name_sequence(self):
        name_seq = self.env['ir.sequence'].next_by_code('siif_income.request_income_recognition.sequence')
        if not name_seq:
            raise ValidationError(_('The sequence to generate the record name automatically has not been created.'))

        return name_seq    


    def update_bank_movement_state(self, new_state):
        for bank_movement in self.bank_movements_ids:
            bank_movement.type_movement = new_state


    def update_credit_note_state(self, new_state):
        for credit_note in self.credit_note_ids:
            credit_note.credit_note_payment_state = new_state


    def update_ticket_state(self, new_state):
        for ticket in self.tickets_ids:
            ticket.ticket_store_state = new_state    
         

    def validate_request(self):
        if not self.has_action():
            total_movements = self.total_bank_movements
            total_invoices = self.total_invoices

            if total_movements > total_invoices and not self.allow_account_configuration():
                raise ValidationError(_("The amount of the deposits is higher than the invoice."))

            if self.bank_movements_ids and self.invoice_ids:
                self.type = 'partial_recognized' if self.allow_account_configuration() else 'recognized'
                

    def allow_account_configuration(self):
        bank_movement_journals = self.bank_movements_ids.mapped('income_bank_journal_id')
        configuration_journals = self.env["configuration.journals"].search([
            ("is_active", "=", True),
            ("journal_id", "in", bank_movement_journals.ids),
        ])

        return bool(configuration_journals)


    @api.depends('invoice_ids', 'invoice_ids.amount_interest')
    def _compute_total_invoices(self):
        for record in self:
            record.total_invoices = sum(x.amount_interest for x in record.invoice_ids)
    

    @api.depends('credit_note_ids', 'credit_note_ids.amount_note')
    def _compute_total_credit_notes(self):
        for record in self:
            record.total_credit_notes = sum(x.amount_note for x in record.credit_note_ids)


    @api.depends('tickets_ids', 'tickets_ids.importe')
    def _compute_total_tickets(self):
        for record in self:
            record.total_tickets = sum(x.importe for x in record.tickets_ids)          


    @api.onchange('action_ids')
    def _onchangue_action_id(self):
        if len(self.action_ids) > 1:
            raise ValidationError(_("Only can add 1 action."))
                       
        for actions in self.action_ids:
            self.dep_subdep_flag = actions.accounting_account.dep_subdep_flag
    

    def reject(self):
        model = self._context.get('active_model')
        request_income_recognition_model = self.env[model].browse(self._context.get('active_id'))
        
        # Argumento de la razon de rechazo
        reject_reason = self.reject_reason
        request_income_recognition_model.reject(reject_reason)
        request_income_recognition_model.update_credit_note_state(None)


    def action_close(self):
        # Manda a la pantalla de solicitud
        return {
            "res_model": REQUEST_INCOME_RECOGNITION_MODEL,
            "type": "ir.actions.act_window",
            "view_mode": "form",
            "view_type": "form",
            "views": [(False, "form")],
            "view_id" : self.env.ref('siif_income.request_income_recognition_form').id,
            "res_id": self.env.context.get('default_request_id'),
            "target": "self",
            "context" : {
                "active_ids": None
            }
        } 
                
                
    @api.onchange('invoice_ids')
    def _onchangue_invoice_id(self):
        self.dependency_id = None
        self.sub_dependency_id = None                   


    def request_message_wizard(self, type, request):
        if type == 'new':
            message_content = _('Request %s created successfully.') % request.name
        elif type == 'update':
            message_content = _('Request %s updated successfully.') % request.name
            
            
        message_id = self.env[REQUEST_INCOME_RECOGNITION_WIZARD].create({'message': message_content})
            
        return {
            'name': _('Done'),
            'type': ACTION_WINDOW,
            'view_mode': 'form',
            'res_model': REQUEST_INCOME_RECOGNITION_WIZARD,
            'view_type': 'form',
            'views': [(self.env.ref("siif_income.successful_request_wizard_view_form").id, 'form')],
            'res_id': message_id.id,
            'target': 'new',
            'context': {
                'default_request_id': request.id,
            },
        }
    
                    
              
            