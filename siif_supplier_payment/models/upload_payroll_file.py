# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from datetime import datetime
from odoo.exceptions import UserError, ValidationError,Warning
import logging

class EmployeePayroll(models.Model):

    _inherit = 'employee.payroll.file'
        
    def get_payroll_payment_vals(self):
        invoice_line_vals = []
        journal = self.env.ref('jt_payroll_payment.payroll_payment_request_jour')
        for line in self.preception_line_ids:
            line_vals = self.get_invoice_line_vals(line)
            if line_vals:
                invoice_line_vals.append((0,0,line_vals))

        for line in self.additional_payments_line_ids:
            line_vals = self.get_invoice_line_vals(line)
            if line_vals:
                invoice_line_vals.append((0, 0, line_vals))

        for line in self.deduction_line_ids:
            line_vals = self.get_deduction_invoice_line_vals(line)
            if line_vals:
                invoice_line_vals.append((0,0,line_vals))
        is_payroll_payment_request = True
        is_pension_payment_request = False
#         if self.is_pension_payment_request:
#             is_payroll_payment_request = False
#             is_pension_payment_request = True
        is_check_payment_method = False
        bank_key_name = ''
        check_payment_method = self.env.ref('l10n_mx_edi.payment_method_cheque')
        if check_payment_method and self.l10n_mx_edi_payment_method_id and check_payment_method.id==self.l10n_mx_edi_payment_method_id.id:
            is_check_payment_method = True
            bank_key_name = self.bank_key_name
        partner_id = self.employee_id and self.employee_id.user_id and self.employee_id.user_id.partner_id and self.employee_id.user_id.partner_id.id or False 
        vals = {'payment_bank_id':self.bank_receiving_payment_id and self.bank_receiving_payment_id.id or False,
                'payment_bank_account_id': self.receiving_bank_acc_pay_id and self.receiving_bank_acc_pay_id.id or False,
                'payment_issuing_bank_id': self.payment_issuing_bank_id and self.payment_issuing_bank_id.id or False,
                'l10n_mx_edi_payment_method_id' : self.l10n_mx_edi_payment_method_id and self.l10n_mx_edi_payment_method_id.id or False,
                'partner_id' : partner_id,
                'is_payroll_payment_request':is_payroll_payment_request,
                'is_pension_payment_request' : is_pension_payment_request,
                'type' : 'in_invoice',
                'journal_id' : journal and journal.id or False,
                'invoice_date' : fields.Date.today(),
                'invoice_line_ids':invoice_line_vals,
                'fornight' : self.fornight,
                'payroll_request_type' : self.request_type,
                'deposite_number' : self.deposite_number,
                'check_number' : self.check_number,
                'bank_key' : self.bank_key,
                'pension_reference': self.reference,
                'period_start' : self.period_start,
                'period_end' : self.period_end,
                'is_check_payment_method':is_check_payment_method,
                'bank_key_name':bank_key_name,
                'dependancy_id':self.dependancy_id and self.dependancy_id.id or False,
                'sub_dependancy_id' : self.sub_dependancy_id and self.sub_dependancy_id.id or False,
                'payment_place_id' : self.payment_place_id and self.payment_place_id.id or False,
                'payroll_processing_id': self.payroll_processing_id,
                }

        logging.critical(vals)
        
        return vals