
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError



class AccountMoveManualPermission(models.Model):
    _name = 'account.move.manual.permission'
    _description = "Account Move Manual Permission"
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'res_users_id'



    res_users_id = fields.Many2one('res.users',string='user')
    email = fields.Char(string='Email', related='res_users_id.login', store=True)
    allowed_journals = fields.Many2many('account.journal',string='Allowed Journal')
    allowed_dependencies = fields.Many2many('dependency',string='Allowed Dependency')
    allowed_subdependencies = fields.Many2many('sub.dependency',string='Allowed Sub Dependency', domain="[('dependency_id','=',allowed_dependencies)]")

    tracking_allowed_journals = fields.Text(string='Allowed Journal', compute="_tracking_allowed_journals", store=True, tracking=True)
    tracking_allowed_dependencies = fields.Text(string='Allowed Dependency', compute="_tracking_allowed_dependencies", store=True, tracking=True)
    tracking_allowed_subdependencies = fields.Text(string='Allowed Sub Dependency', compute="_tracking_allowed_subdependencies", store=True, tracking=True)

    _sql_constraints = [
        ('user_uniq', 'unique(res_users_id)', _('User must be unique'))]

    @api.constrains('allowed_dependencies','allowed_subdependencies')
    def _check_subdependency_in_dependency(self):
        for record in self:
            for rec_subdependency in record.allowed_subdependencies:
                if rec_subdependency.dependency_id.id not in record.allowed_dependencies.ids:
                    raise ValidationError(_("Incorrect relationship between dependencies and subdependencies: Sub dependency %s with dependency %s, is not selected in the selected allowed dependencies") % (rec_subdependency.sub_dependency,rec_subdependency.dependency_id.dependency))

    @api.depends('allowed_journals')
    def _tracking_allowed_journals(self):
        for record in self:
            computed_value = ", ".join(record.allowed_journals.mapped('name'))
            record.tracking_allowed_journals = computed_value

    @api.depends('allowed_dependencies')
    def _tracking_allowed_dependencies(self):
        for record in self:
            computed_value = ", ".join(record.allowed_dependencies.mapped('dependency'))
            record.tracking_allowed_dependencies = computed_value

    @api.depends('allowed_subdependencies')
    def _tracking_allowed_subdependencies(self):
        for record in self:
            computed_value = ", ".join(record.allowed_subdependencies.mapped('sub_dependency'))
            record.tracking_allowed_subdependencies = computed_value