from odoo import fields, models


class Dependency(models.Model):
    _name = 'dependency'
    _inherit = ['dependency', 'excel.detailed.information']

    def _compute_excel_display_name(self):
        for record in self:
            record.excel_display_name = '%s - %s' % (record.dependency, record.description)
