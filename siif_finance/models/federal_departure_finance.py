# 1 : imports of python lib

# 2 : imports of odoo
import logging
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
# 3 : imports from odoo addons
from odoo.addons.siif_security.tools.utils import check_authorization, check_only_allowed_fields


class FederalDepartureFinance(models.Model):
    _name = 'federal.departure.finance'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'shcp.game']
    _description = 'Federal departure'


    conversion_key = fields.Char('Federal departure', track_visibility="onchange")
    conversion_key_desc = fields.Text(string="Description of Federal departure", track_visibility="onchange")
    start_date = fields.Date('Start Date', track_visibility="onchange")
    end_date = fields.Date('End Date', track_visibility="onchange")


    def unlink(self):
        for record in self:
            if self.env['calendar.assigned.amounts.lines'].search([('new_item_id', '=', record.id)]):
                raise ValidationError(_('You cannot delete a federal departure that is within the Schedule of Assigned Amounts'))
            elif self.env['control.amounts.received.line'].search([('new_item_id', '=', record.id)]):
                raise ValidationError(_('You cannot delete a federal departure that is within the Control of Amounts Received'))
            
        return super(FederalDepartureFinance, self).unlink()