from odoo import models, fields

class RejectWizard(models.TransientModel):

    _name = 'reject.wizard'

    model = fields.Char('Model')
    record_id = fields.Integer('Record Id')
    reason_for_rejection = fields.Text('Reason For Rejection')

    def reject(self):
        self.env[self.model].browse([self.record_id]).reject(self.reason_for_rejection)

def display_modal(self, title='Rechazar'):
    """
    Acción para obtener los motivos de rechazo
    """

    return {
        'name': title,
        'res_model': 'reject.wizard',
        'view_mode': 'form',
        'view_id': self.env.ref('siif_supplier_payment.reject_wizard_form').id,
        'context': {
            'default_model': self._name,
            'default_record_id': self.id,
        },
        'target': 'new',
        'type': 'ir.actions.act_window',
    }