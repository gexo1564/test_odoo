# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
from datetime import datetime
import time
import json
import logging

class SiifCodesPaypal(models.Model):
    _name = "siif.income.codes.paypal"
    _description = "Códigos PayPal"
    _rec_name = "codigo_paypal"

    codigo_paypal = fields.Char(string = "Código PayPal")
    description = fields.Char(string = "Descripción")
    used_by_paypal_check = fields.Boolean(striing = "Usado Por PayPal", default = "False")