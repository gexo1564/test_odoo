# -*- coding: utf-8 -*-
import logging
from odoo import models, fields, api, _
RES_USERS='res.users'
class ResUsers(models.Model):
    _inherit = RES_USERS
    all_dep_subdep_money_fund = fields.Boolean(string='All Dependencies selected')
    dependency_for_money_fund_ids = fields.Many2many(
        'dependency',
        'allowed_users_dep_money_fund_rel',
        string = "User Permissions for Dependencies"
    )

    sub_dependency_for_money_fund_ids = fields.Many2many(
        'sub.dependency',
        'allowed_users_sd_money_fund_rel',
        string = "User Permissions for Sub Dependencies"
    )

    len_perm_money_assigned = fields.Integer(compute='_get_size_permissions_money_fund',string="Permissions Assigned Money Fund",store=False)

    def _get_size_permissions_money_fund(self):
        for info in self:
            len_dep=len(info.dependency_ids)
            len_subdep=len(info.sub_dependency_ids)
            info.len_perm_money_assigned=len_dep+len_subdep

    #Asignación de Todos las Dependencias
    @api.onchange('all_dep_subdep_money_fund')
    def _all_deps_money_fund_selected(self):
        if (self.all_dep_subdep_money_fund==True):
            dependency_ids = self.env['dependency'].search([])
            for user in self:
                user.dependency_for_money_fund_ids=user.dependency_for_money_fund_ids=[(6, 0, dependency_ids.ids)]
            logging.critical(user.dependency_for_money_fund_ids)
        else:
            for user in self:
                if(len(user.dependency_for_money_fund_ids)>=1):
                    user.dependency_for_money_fund_ids=[(6, 0, [])]


class Dependency(models.Model):

    _inherit = 'dependency'

    allowed_users_money_funds = fields.Many2many(RES_USERS,  'allowed_users_dep_money_fund_rel', string ='Allowed users')

class SubDependency(models.Model):

    _inherit = 'sub.dependency'

    allowed_users_money_funds = fields.Many2many(RES_USERS,  'allowed_users_sd_money_fund_rel', string ='Allowed users')
