# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from num2words import num2words

from odoo import models, fields, api,_
from odoo.exceptions import UserError, ValidationError
import logging
from psycopg2 import sql
logger = logging.getLogger(__name__)

from odoo.tools.profiler import profile

from odoo.addons.siif_supplier_payment.tools.utils import Dictionary
from odoo.addons.siif_supplier_payment.tools.layouts import Context
from odoo.addons.siif_supplier_payment.tools.layouts.bancos import LayoutBBVASIT

"""Modelos"""
ACC_MOVE='account.move'
ACC_PAYMENT='account.payment'
CURRENCY_FUND='currency.funds'
CHEQUE_MX_EDI='l10n_mx_edi.payment_method_cheque'
PAYMENT_TRASNFER='l10n_mx_edi.payment_method_transferencia'
BATCH_MANAGEMENT='batch.management'
"""Variables"""
ACT_WINDOW='ir.actions.act_window'

"""Mensajes"""
MSG_ZERO="You can not register payment with 0 amount"
class AccountPayment(models.Model):

    _inherit = ACC_PAYMENT

    currency_availability = fields.Boolean('Currency availability')
    currency_funds_id = fields.Many2one(CURRENCY_FUND, 'Currency Funds')
    currency_application_request_id = fields.Many2one('application.request', 'Currency Application Request')
    # Documentación Soporte
    support_documentation = fields.Many2many('ir.attachment', string='Support Documentation')

    total_amount_words = fields.Char('Total Amount in Words', compute='_compute_total_amount_words', store=False)

    payment_issuance_method = fields.Selection(
        [('manual', 'Manual'),('automatic', 'Automatic'),('check', 'Check')],
        string='Record Type',
    )

    @api.depends('payment_request_id')
    def _compute_total_amount_words(self):
        for record in self:
            record.total_amount_words = num2words(record.payment_request_id.amount_total, lang='es')
    

    def create_journal_for_paid(self,invoice):
        
        if ((invoice.is_payment_request or invoice.is_project_payment) and (not invoice.is_different_payroll_request and not invoice.is_pension_payment_request and not invoice.is_payroll_payment_request)):
                logger.info("PAGO A PROVEEDOR")
        else:

            #==== he Bank Journal will be taken, corresponding to the “Paid” accounting moment ===#
            amount_total = sum(x.price_total for x in invoice.invoice_line_ids.filtered(lambda x:x.program_code_id))
            if invoice.currency_id != invoice.company_id.currency_id:
                amount_currency = abs(amount_total)
                balance = invoice.currency_id._convert(amount_currency, invoice.company_currency_id, invoice.company_id, invoice.date)
                currency_id = invoice.currency_id and invoice.currency_id.id or False
            else:
                balance = abs(amount_total)
                amount_currency = 0.0
                currency_id = False
                
            invoice.line_ids = [(0, 0, {
                                         'account_id': self.journal_id.paid_credit_account_id and self.journal_id.paid_credit_account_id.id or False,
                                         'coa_conac_id': self.journal_id.conac_paid_credit_account_id and self.journal_id.conac_paid_credit_account_id.id or False,
                                         'credit': balance, 
                                         'exclude_from_invoice_tab': True,
                                         'conac_move' : True,
                                         'amount_currency' : -amount_currency,
                                         'currency_id' : currency_id,                                     
                                         'partner_id':invoice.partner_id and invoice.partner_id.id or False,
                                     }), 
                            (0, 0, {
                                         'account_id': self.journal_id.paid_debit_account_id and self.journal_id.paid_debit_account_id.id or False,
                                         'coa_conac_id': self.journal_id.conac_paid_debit_account_id and self.journal_id.conac_paid_debit_account_id.id or False,
                                         'debit': balance,
                                         'exclude_from_invoice_tab': True,
                                         'conac_move' : True,
                                         'amount_currency' : amount_currency,
                                         'currency_id' : currency_id,
                                         'partner_id':invoice.partner_id and invoice.partner_id.id or False,                                     
                                     })]

    def _prepare_payment_moves(self):
        """
        Se sobre-escribe del método padre para modificar las descripciones de
        los apuntes contables
        """
        res = super()._prepare_payment_moves()
        p = 0
        for payment in self:
            if payment.payment_type == 'outbound' and payment.partner_type == 'supplier':
                for line in res[p]['line_ids']:
                    line[2]['name'] = "Pago a proveedor: %s %s %s" % (
                        self.partner_id.name or '',
                        self.payment_request_id.date.strftime('%d/%m/%Y') or '',
                        self.payment_request_id.folio or '',
                    )
            p += 1
        return res

    def action_register_payment(self):
        res = super(AccountPayment,self).action_register_payment()
        active_ids = self.env.context.get('active_ids')
        if not active_ids:
            return ''

        record_ids = self.env[ACC_MOVE].browse(active_ids)
        if not record_ids or any(invoice.payment_state != 'approved_payment' and (invoice.is_payroll_payment_request or invoice.is_different_payroll_request or invoice.is_pension_payment_request)  for invoice in record_ids):
            raise UserError(_("You can only register payment for  Approved for payment request"))

        if not record_ids or any(invoice.payment_state not in ('approved_payment','assigned_payment_method') and (invoice.is_project_payment or invoice.is_payment_request)  for invoice in record_ids):
            raise UserError(_("You can only register payment for  Approved for payment request"))

        # Validación para que solo se puedan programar solicitudes de pago con
        # el mismo tipo de método de pago
        payment_method = set([m.l10n_mx_edi_payment_method_id for m in record_ids])
        if len(payment_method) > 1:
            raise UserError("Solo se puede programar pagos de solicitudes con el mismo método de pago.")
        payment_method = list(payment_method)[0]
        # Flujo de programación de pagos con cheques:
        if payment_method == self.env.ref(CHEQUE_MX_EDI):
            return self.env[ACC_PAYMENT].schedule_payment_with_checks(record_ids.ids)

#         if not record_ids or any(invoice.state != 'posted' for invoice in record_ids):
#             raise UserError(_("You can only register payments for open invoices"))

        record_ids = record_ids.filtered(lambda x:(x.is_project_payment or x.is_payment_request or x.is_payroll_payment_request or x.is_different_payroll_request or x.is_pension_payment_request) and x.is_invoice(include_receipts=True))
        if record_ids:
#             payment_issuing_bank_id = record_ids.mapped('payment_issuing_bank_id')
#             if len(payment_issuing_bank_id) != 1 :
#                 raise UserError(_("You can not register payment for multiple Payment issuing Bank"))
            amount = 0
            for r in record_ids:
                amount1 = self._compute_payment_amount(r, r.currency_id, r.journal_id,fields.Date.today())
                amount += abs(amount1)

            if abs(amount) == 0 :
                raise UserError(_(MSG_ZERO))

            return {
                'name': _('Schedule Payment'),
                'res_model':'bank.balance.check',
                'view_mode': 'form',
                'view_id': self.env.ref('jt_supplier_payment.view_bank_balance_check').id,
                'context': {
                    'default_total_amount': abs(amount),
                    'default_total_request': len(record_ids),
                    'default_invoice_ids': [(6, 0, record_ids.ids)]
                },
                'target': 'new',
                'type': ACT_WINDOW,
            }
        else:
            return res

    #Acción para generar los fondeos manuales
    def action_register_payment_manual(self):
        active_ids = self.env.context.get('active_ids')
        if not active_ids:
            return ''

        record_ids = self.env[ACC_MOVE].browse(active_ids)

        # Validación para que solo se puedan programar solicitudes de pago con
        # el mismo tipo de método de pago
        payment_method = set(record_ids.mapped('l10n_mx_edi_payment_method_id'))
        if len(payment_method) > 1:
            raise UserError("Solo puede programar pagos de solicitudes con el mismo método de pago.")
        # Flujo de programación de pagos con cheques:
        if self.env.ref(CHEQUE_MX_EDI) in payment_method:
            return self.env[ACC_PAYMENT].schedule_payment_with_checks(record_ids.ids)
        # Flujo de programación de pago por transferencia
        elif self.env.ref(PAYMENT_TRASNFER) in payment_method:
            # Revisar que las solicitudes de pago estén
            #   - Aprobadas para pago
            #   - Método de emisión Manual
            #   - Si son de otro tipo de solicitud de account.move
            for record in record_ids:
                # Se valida que el tipo de pago sea manual.
                if record.payment_issuance_method != 'manual':
                    raise UserError("Esta acción solo permite el registro de Solicitudes de Pago con Método de Emisión de Pago Manuales.")
                # Se hacen las validaciones con el status aprobada para pago
                if record.payment_state == 'approved_payment':
                    # Si esta aprobada para pago y tiene lote se lanza error
                    if record.batch_folio != 0:
                        raise UserError("Esta acción solo permite el registro de Solicitudes de Pago con Método de Emisión de Pago y que están Aprobadas para pago.")
                # Cualquier otro estatus que no sea aprobada para pago y para tramite de pago
                else:
                    raise UserError("Esta acción solo permite el registro de Solicitudes de Pago con Método de Emisión de Pago y que están Aprobadas para pago.")
            record_ids = record_ids.filtered(lambda x:(x.is_project_payment or x.is_payment_request or x.is_payroll_payment_request or x.is_different_payroll_request or x.is_pension_payment_request) and x.is_invoice(include_receipts=True))
            if record_ids:
                amount = 0
                for r in record_ids:
                    amount1 = self._compute_payment_amount(r, r.currency_id, r.journal_id,fields.Date.today())
                    amount += abs(amount1)
                if abs(amount) == 0:
                    raise UserError(_(MSG_ZERO))
                return {
                    'name': 'Asignación de datos para pagos manuales',
                    'res_model':'bank.balance.check.manual',
                    'view_mode': 'form',
                    'view_id': self.env.ref('siif_supplier_payment.view_bank_balance_manual').id,
                    'context': {
                        'default_total_amount': abs(amount),
                        'default_total_request': len(record_ids),
                        'default_invoice_ids': [(6, 0, record_ids.ids)],
                        'default_folio': self.env[ACC_MOVE]._generate_next_batch_sheet_folio('supplier'),
                    },
                    'target': 'new',
                    'type': ACT_WINDOW,
                }

    def payment_processing_action(self):
        # Procesar pagos de tranferencias Electronicas
        lines_ok, lines_err = self.payments_with_electronic_transfers()
        # Procesar pagos de efectivo en moneda extranjeras
        lines_cash_ok, lines_cash_err = self.payments_with_foreign_currency_cash()
        elec_ok = True if lines_ok else False
        elec_err = True if lines_err else False
        cash_ok = True if lines_cash_ok else False
        cash_err = True if lines_cash_err else False
        message = None
        if not elec_ok and not elec_err and not elec_ok and not cash_err:
            message = '• No se encontraron Solicitudes de Pago automáticas para procesar.'
        elif (not elec_err and not cash_err) and (lines_ok or cash_ok):
            message = '• Los lotes de pago automáticos fueron generados exitosamente.'
        elif not elec_ok and elec_err and not cash_ok and cash_err:
            message = '• No fue posible generar los lotes de pago automáticos por insuficiencia de saldo.'
        else:
            message = '• Algunos lotes de pago automáticos no pudieron ser generados y/o validados, vea la sección de cuentas bancarias/fondos sin saldo disponible para ver el detalle.'
        # self.env.cr.rollback()
        return {
            'name': _('Procesamiento Automático de Pagos'),
            'type': ACT_WINDOW,
            'res_model': 'payment.processing.result',
            'view_mode': 'form',
            'view_type': 'form',
            'context': {
                'default_message': message,
                'default_payment_processing_result_line_ids': lines_ok,
                'default_payment_processing_result_error_line_ids': lines_err,
                'default_payment_processing_cash_result_line_ids': lines_cash_ok,
                'default_payment_processing_cash_result_error_line_ids': lines_cash_err,
            },
            'views': [(False, 'form')],
            'target': 'new'
        }

    def payments_with_electronic_transfers(self):
        """
        Método para la automatización de procesamiento de pagos y asignación de lotes
        de transferencias electronicas
        """

        def generate_groups_automatic():
            groups = []
            # Método de pago de tranferencia electronica
            payment_method_id = self.env.ref(PAYMENT_TRASNFER).id
            # Query para obtener todas las cuentas de donde se pagaran las solicitudes
            query_src_bank_accounts = """
                select distinct op.journal_id src_bank_accounts
                from (
                select * from account_move
                where type = 'in_invoice'
                    and is_payment_request = True
                    and l10n_mx_edi_payment_method_id = %(payment_method_id)s
                    and payment_state = 'approved_payment'
                    and (batch_folio = 0 or batch_folio is null)
                    and payment_issuance_method = 'automatic'
                ) solicitudes, operation_type op
                where solicitudes.operation_type_id = op.id
                order by op.journal_id;
            """
            params = {'payment_method_id': payment_method_id}
            # Query para obtener las solicitudes con el mismo banco destino y cuenta origen
            query_same_bank = """
                SELECT solicitudes.id
                FROM (
                select * from account_move
                where type = 'in_invoice'
                    and is_payment_request = True
                    and l10n_mx_edi_payment_method_id = %(payment_method_id)s
                    and payment_state = 'approved_payment'
                    and (batch_folio = 0 or batch_folio is null)
                    and payment_issuance_method = 'automatic'
                ) solicitudes, operation_type AS op
                WHERE solicitudes.operation_type_id = op.id
                    AND op.journal_id = %(journal_id)s
                    AND solicitudes.payment_bank_id = op.bank_id
            """
            # Query para obtener las solicitudes con diferente banco destino y cuenta origen
            query_other_bank = """
                SELECT solicitudes.id
                FROM (
                select * from account_move
                where type = 'in_invoice'
                    and is_payment_request = True
                    and l10n_mx_edi_payment_method_id = %(payment_method_id)s
                    and payment_state = 'approved_payment'
                    and (batch_folio = 0 or batch_folio is null)
                    and payment_issuance_method = 'automatic'
                ) solicitudes, operation_type AS op
                WHERE solicitudes.operation_type_id = op.id
                    AND op.journal_id = %(journal_id)s
                    AND solicitudes.payment_bank_id <> op.bank_id
            """
            # Se obtienen los ids de los bancos de emisión de las solicitudes de pago a procesar
            self.env.cr.execute(query_src_bank_accounts, params)
            for src_bank_accounts in self.env.cr.fetchall():
                # Diario de la cuenta bancaria
                journal_bank_id = self.env['account.journal'].browse(src_bank_accounts)
                banks_params = {
                    'payment_method_id': payment_method_id,
                    'journal_id': src_bank_accounts[0],
                }
                # Se obtiene las solicitudes con el mismo banco de emisión y recepción
                self.env.cr.execute(query_same_bank, banks_params)
                ids = [res[0] for res in self.env.cr.fetchall()]
                if ids:
                    groups.append(('same', 'automatic', journal_bank_id, ids))
                # groups += _generate_sub_groups('same', journal_bank_id, ids)
                # Se obtiene las solicitudes con diferente banco de emisión y recepción
                self.env.cr.execute(query_other_bank, banks_params)
                ids = [res[0] for res in self.env.cr.fetchall()]
                if ids:
                    groups.append(('other', 'automatic', journal_bank_id, ids))
            return groups

        # Query para obtener el monto total del lote
        query = """
            select sum(amount_total) from account_move where batch_folio = %s
        """
        lines_ok = []
        lines_err = []
        # Generar los grupos de pagos para las solicitudes automáticas
        for group in generate_groups_automatic():
            target_bank, payment_issuance_method, journal_bank_id, move_ids = group
            # Se asigna el folio al lote de pago
            folio = self.env[ACC_MOVE].generate_payment_batch('supplier', move_ids)
            lines_ok.append((0, 0, {
                'journal_id': journal_bank_id.id,
                'target_bank': target_bank,
                'payment_issuance_method': payment_issuance_method,
                'folio': folio
            }))
            # Creación de los pagos
            self.env['account.payment'].schedule_payment_with_bank_account(
                journal_bank_id,
                move_ids,
            )
            # Creación de los lotes de pago
            self.env.cr.execute(query, (folio,))
            amount_total = (self.env.cr.fetchone() or [None])[0]
            self.env[BATCH_MANAGEMENT].create({
                'journal_id': journal_bank_id.id,
                'folio': folio,
                'amount_total': amount_total,
                'number_of_payments': len(move_ids),
                'type': 'automatic'
            })

        return lines_ok, lines_err

    def payments_with_foreign_currency_cash(self):
        """
        Método para la automatización de procesamiento de pagos y asignación de lotes
        de pagos efectivo con moneda extrangeras
        """
        op_number = self.env['payment.parameters'].get_param('travel_expenses_op_type').op_number
        def generate_groups():
            groups = []
            # Método de pago de tranferencia electronica
            payment_method_id = self.env.ref('l10n_mx_edi.payment_method_efectivo').id
            # Subquery con el universo de solicitudes a procesar
            solicitudes = """(
                select * from account_move
                where type = 'in_invoice'
                    and is_payment_request = True
                    and l10n_mx_edi_payment_method_id = %s
                    and payment_state = 'approved_payment'
                    and (batch_folio = 0 or batch_folio is null)
                ) solicitudes
            """
            query_currency = f"""
                select distinct currency_id from {solicitudes}, operation_type op
                where solicitudes.operation_type_id = op.id
                    and op.op_number = %s
                    and currency_id != (select id from res_currency where name = 'MXN')
            """
            query_payment_request = f"""
                select solicitudes.id from {solicitudes}, operation_type op
                where solicitudes.operation_type_id = op.id
                    and op.op_number = %s
                    and currency_id = %s
            """
            self.env.cr.execute(query_currency, (payment_method_id, op_number))
            for currency in self.env.cr.fetchall():
                self.env.cr.execute(query_payment_request, (payment_method_id, op_number, currency[0]))
                ids = [res[0] for res in self.env.cr.fetchall()]
                if ids:
                    groups.append((currency, ids))
            return groups

        def check_balances(currency, move_ids):
            total_amount = self.get_total_payment_request(move_ids)
            return self.env[CURRENCY_FUND].check_balance(currency, total_amount), total_amount

        def register_payment(currency_id, move_ids):
            currency_funds = self.env[CURRENCY_FUND].search([('currency_id', '=', currency_id)])
            self.schedule_payment_foreign_currency_cash(currency_funds, move_ids)
            return currency_funds

        lines_ok = []
        lines_err = []
        # Generar los grupos de pagos
        for group in generate_groups():
            currency, move_ids = group
            # Se generan los lotes y obtiene el folio
            folio = self.env[ACC_MOVE].generate_payment_batch('supplier', move_ids)
            # Se registrar el pago
            currency_funds = register_payment(currency, move_ids)
            # Se agregan a la lista de lineas exitosas (siguiendo el flujo dado)
            lines_ok.append((0, 0, {
                'currency_funds_id': currency_funds.id,
                'folio': folio
            }))
            # Verificar el saldo de la cuenta
            ok, total_amount = check_balances(currency, move_ids)
            # Si hay saldo suficiente
            if ok:
                # Decrementar el saldo en el fondo
                self.env[CURRENCY_FUND].sustract_actual_fund(currency, total_amount, move_ids)
                # Colocar la bandera de disponible en los pagos en True
                payments = self.env[ACC_PAYMENT].search([('batch_folio', '=', folio)])
                payments.write({'currency_availability': True})
            # Si no hay saldo suficiente
            else:
                # Crear las solicitudes de compra de divisas
                request_id = self.env[CURRENCY_FUND].create_application_request(currency, total_amount, folio)
                # Colocar la bandera de disponible en los pagos en False
                # Asociar el id de la solicitud de compra de divisas con los pagos
                payments = self.env[ACC_PAYMENT].search([('batch_folio', '=', folio)])
                payments.write({
                    'currency_availability': False,
                    'currency_application_request_id': request_id.id
                })
                # Generar solicitud de compra
                # (currency, monto, move_ids)
                # Se agregan a las lineas de error
                # balance_available, minimum_balance, total_amount = res
                lines_err.append((0, 0, {
                    'currency_funds_id': currency_funds.id,
                    'currency_application_request_id': request_id.id,
                    'total_amount': total_amount,
                    'requested_amount': request_id.amount_funds,
                    'currency_id': currency[0],
                }))
        return lines_ok, lines_err

    def get_total_payment_request(self, move_ids):
        total_amount = 0
        for payment_request in self.env[ACC_MOVE].browse(move_ids):
            total_amount += abs(self.env[ACC_PAYMENT]._compute_payment_amount(
                payment_request,
                payment_request.currency_id,
                payment_request.journal_id,
                fields.Date.today()
            ))
        if abs(total_amount) == 0:
            raise UserError(_(MSG_ZERO))
        return total_amount

    def _schedule_payment(self, journal_id, move_ids, **kwargs):
        """
        Realiza la programación del Pago
        """
        # Se actualiza el status y la cuenta de emisión de pago de las solicitudes
        query_update_state = """
            update account_move set
                payment_state = 'for_payment_procedure',
                date_for_payment_procedure = now(),
                payment_issuing_bank_id = %s
            where id in %s
        """
        self.env.cr.execute(query_update_state, (journal_id.id, tuple(move_ids)))
        # Obtener el universo de pagos a crear
        query_rechazados = """
            select payment_request_id from account_payment
            where payment_request_id in %s
        """
        self.env.cr.execute(query_rechazados, (tuple(move_ids),))
        rejected_applications = [res[0] for res in self.env.cr.fetchall()]
        # Los pagos a crear son el universo - los que fueron rechazados
        payment_applications = list(set(move_ids) - set(rejected_applications))
        payment_ids = []
        if payment_applications:
            # Query para obtener los parametros para la creación de los pagos
            query_pagos = """
                select
                    m.partner_id partner_id,
                    p.password_beneficiary baneficiary_key,
                    m.amount_total amount,
                    m.payment_bank_account_id partner_bank_account_id,
                    m.l10n_mx_edi_payment_method_id,
                    m.dependancy_id,
                    m.sub_dependancy_id,
                    m.payment_bank_id payment_bank_id,
                    m.name communication,
                    m.batch_folio batch_folio,
                    m.folio folio,
                    m.folio bank_reference,
                    m.id payment_request_id,
                    m.currency_id currency_id,
                    m.check_folio_id,
                    m.payment_issuance_method
                from account_move m, res_partner p
                where m.partner_id = p.id
                    and m.id in %s
            """
            # Se obtienen los datos de los pagos
            self.env.cr.execute(query_pagos, (tuple(payment_applications),))
            payments = self.env.cr.dictfetchall()
            for payment in payments:
                payment.update({
                    'state': 'draft',
                    'journal_id': journal_id.id,
                    'payment_method_id': 1, # revisar
                    'payment_date': kwargs.get('payment_date', datetime.today()),
                    'partner_type': 'supplier',
                    'payment_type': 'outbound',
                    'payment_request_type': 'supplier_payment',
                    'l10n_mx_edi_sat_status': 'undefined',
                    'payment_bank_account_id': payment.get('partner_bank_account_id'),
                    'payment_state': 'draft',
                })
                # Si es una pago por cheque
                if payment.get('l10n_mx_edi_payment_method_id') == self.env.ref(CHEQUE_MX_EDI).id:
                    payment.pop('payment_bank_id')
                    payment.pop('payment_bank_account_id')
                for k, v in kwargs.items():
                    payment.update({k: v})
            # Creación de los pagos
            payment_ids = self.env[ACC_PAYMENT].insert_sql(payments)
            # Actualización de las referencias de la relación invoice_ids
            query_insert_inovice_ids = """
                insert into account_invoice_payment_rel(payment_id, invoice_id) values (%s, %s)
            """
            for i in range(len(payment_ids)):
                payment_id = payment_ids[i]
                move_id = payments[i].get('payment_request_id')
                self.env.cr.execute(query_insert_inovice_ids, (payment_id, move_id))
        # Actualizacion de los pagos rechazados
        for payment in self.search([('payment_request_id', 'in', rejected_applications)]):
            move_id = payment.payment_request_id
            # Actualiza la fecha de para tramite de pago
            move_id.date_for_payment_procedure = kwargs.get('payment_date', datetime.today())
            # Mensaje de notificación de programación de pagos
            move_id.message_post(body='Programación de pago')
            payment.update(self.get_payment_data(move_id, {
                'state': 'draft',
                'journal_id': journal_id.id
            }))
            payment_ids.append(payment.id)
        # Para todos los pagos, se valida el procedimiento de pago
        # for payment in self.browse(payment_ids):
        #     payment.action_validate_payment_procedure()
        # Registra el monto comprometido de la cuenta
        total_amount = self.get_total_payment_request(move_ids)
        journal_id.bank_account_id.add_expense(total_amount)

    def schedule_payment_with_bank_account(self, journal_bank_id, move_ids,  **kwargs):
        """
        Programa pagos que usan una cuenta bancaria
        """
        self._schedule_payment(journal_bank_id, move_ids, kwargs=kwargs)

    def schedule_payment_with_checks(self, move_ids):
        """
        Programa los pagos por cheques
        """
        # Valida que solo se puedan programar pagos cuyo status de cheque sea entregado
        query_check_status = """
            select count(*)
                from account_move m, check_log l
                where m.check_folio_id = l.id
                and l.status = 'Delivered'
                and m.id in %s
        """
        self.env.cr.execute(query_check_status, (tuple(move_ids),))
        res = (self.env.cr.fetchone() or [0])[0]
        if res != len(move_ids):
            raise UserError("Solo puede Programar solicitudes de pago cuyos cheques tengan el estado 'Entregado'.")
        # Obtiene la cuenta bancaria para registrar el pago
        query_check_account_bank = """
            select b.payment_issuing_bank_id, m.id
            from account_move m, check_payment_req c, payment_batch_supplier b
            where m.check_folio_id = c.check_folio_id
                and c.payment_batch_id = b.id
                and m.id in %s
        """
        self.env.cr.execute(query_check_account_bank, (tuple(move_ids),))
        # Se agrupan las solicitudes de pago por la cuenta bancaria
        account_payments = Dictionary()
        for journal_bank_id, move_id in self.env.cr.fetchall():
            journal_bank_id = self.env['account.journal'].browse([journal_bank_id])
            account_payments.add_list(journal_bank_id, move_id)
        # Por cada cuenta bancaria se registran los pagos
        lines_err = []
        for journal_bank_id, move_ids in account_payments.items():
            # Se programa el pago
            self._schedule_payment(journal_bank_id, move_ids)
        if lines_err:
            return {
                'name': _('Programación de Pagos de Cheques'),
                'type': ACT_WINDOW,
                'res_model': 'payment.processing.result',
                'view_mode': 'form',
                'view_type': 'form',
                'context': {
                    'default_message': "Algunos pagos no pudieron ser programados por insuficiencia de las cuentas bancarias.",
                    'default_payment_processing_result_error_line_ids': lines_err,
                },
                'views': [(False, 'form')],
                'target': 'new'
            }

    def create_payment_check_request(self, move_ids, journal_bank_id = None):
        """
        Crea los solicitudes de cheques para poder continuar el flujo en el módulo
        de control de Cheques
        """
        bank_id, bank_acc_id = None, None
        if journal_bank_id:
            bank_id = journal_bank_id.id
            bank_acc_id = journal_bank_id.bank_account_id.id if journal_bank_id.bank_account_id else False
        # Método de pago por cheques
        check_payment_method = self.env.ref(CHEQUE_MX_EDI)
        # Se agrupa en un diccionario las solicitudes de pago por su folio
        batch_data = Dictionary()
        for invoice in self.env[ACC_MOVE].browse(move_ids).filtered(lambda x:
            x.l10n_mx_edi_payment_method_id == check_payment_method
            and x.payment_state == 'approved_payment'
        ):
            batch_data.add_list(invoice.batch_folio, invoice)
        # Para cada folio de los pagos
        list_status=('Detained','Cancelled','Reissued','Withdrawn from circulation')
        for batch_folio, moves in batch_data.items():
            moves_list = []
            type_of_batch = False
            # Para cada solicitud de pago en el lote
            for move in moves:
                if move.is_payroll_payment_request:
                    type_of_batch = 'nominal'
                    if move.check_folio_id and move.check_folio_id.status not in list_status:
                        move.check_folio_id.status = 'Printed'
                        move.payment_state = 'assigned_payment_method'
                elif move.is_payment_request:
                    type_of_batch = 'supplier'
                elif move.is_different_payroll_request:
                    type_of_batch = 'nominal'
                elif move.is_project_payment:
                    type_of_batch = 'project'
                elif move.is_pension_payment_request:
                    type_of_batch = 'pension'
                    if move.check_folio_id and move.check_folio_id.status not in list_status:
                        move.check_folio_id.status = 'Printed'
                        move.payment_state = 'assigned_payment_method'
                moves_list.append(move)
            move_val_list = []
            checkbook_req_id = False
            for move in moves_list:
                check_folio_id = False
                if move.check_folio_id:
                    if move.check_folio_id.status not in list_status:
                        check_folio_id = move.check_folio_id.id
                        move.check_folio_id.check_amount = move.amount_total
                        checkbook_req_id = move.check_folio_id.checklist_id and move.check_folio_id.checklist_id.checkbook_req_id.id or False

                payment = self.env[ACC_PAYMENT].search([('payment_request_id', '=', move.id)], limit=1)
                move_val_list.append({
                    'payment_req_id': move.id,
                    'amount_to_pay': move.amount_total,
                    'payment_id': payment.id,
                    'check_folio_id':check_folio_id
                })
            dict_batch = {
                'batch_folio': batch_folio,
                'type_of_payment_method': 'checks',
                'type_of_batch':type_of_batch,
                'checkbook_req_id' : checkbook_req_id,
                'payment_req_ids': [(0, 0, val) for val in move_val_list]
            }
            if bank_id:
                dict_batch.update({'payment_issuing_bank_id': bank_id})
            if bank_acc_id:
                dict_batch.update({'payment_issuing_bank_acc_id': bank_acc_id})
            batch_id = self.env['payment.batch.supplier'].create(dict_batch)
            for move in moves_list:
                move.payment_check_batch_id = batch_id.id
            if batch_id.type_of_batch in ('nominal', 'pension'):
                batch_lines = batch_id.payment_req_ids.filtered(lambda x:x.check_folio_id and x.check_folio_id.status=='Printed')
                if batch_lines:
                    batch_id.create_check_printing_entry(batch_id, batch_lines)

    def schedule_payment_foreign_currency_cash(self, currency_funds_id, move_ids):
        """
        Programa los pagos con efectivo en moneda extrangera
        """
        if not currency_funds_id.journal_id:
            raise ValidationError("El fondo de divisas no tiene configurado su diario.")
        journal_id = currency_funds_id.journal_id
        self._schedule_payment(journal_id, move_ids, currency_funds_id=currency_funds_id.id)

    def get_payment_data(self, move, data):
        """
        Devuelve los datos de pago
        """
        # Fecha de pago
        payment_date = False
        # Si la solicitud tiene un plazo de pago
        if move.invoice_payment_term_id:
            result = move.invoice_payment_term_id.compute(move.amount_total, move.invoice_date, move.currency_id)
            if result:
                payment_date = result[0][0]
                if move.is_different_payroll_request or move.is_pension_payment_request:
                    if move.invoice_date and payment_date and isinstance(payment_date, str):
                        payment_date = datetime.strptime(payment_date, '%Y-%m-%d')
                        payment_date = payment_date + timedelta(days=-1)
                        non_business_day_ids = move.get_non_business_day(
                            move.invoice_date,
                            payment_date
                        )
                        if non_business_day_ids:
                            payment_date = payment_date + timedelta(days=1)
                            payment_date = move.get_patment_date(len(non_business_day_ids) - 1, payment_date)
        # Si la solicitud no tiene plazo de pago, pero tiene fecha de vencimiento
        elif not move.invoice_payment_term_id and move.invoice_date_due:
            payment_date = move.invoice_date_due
            if move.is_different_payroll_request or move.is_pension_payment_request:
                payment_date = move.get_patment_date(0, payment_date)
        # Si la solicitud tiene fecha de factura recibo
        elif move.invoice_date and not move.invoice_payment_term_id:
            payment_date = move.get_patment_date(30, move.invoice_date)
        payment_request_type = False
        if move.is_payroll_payment_request:
            payment_request_type = 'payroll_payment'
            payment_date = self.get_payroll_payment_date(move, payment_date)
        elif move.is_payment_request:
            payment_request_type = 'supplier_payment'
        elif move.is_different_payroll_request:
            payment_request_type = 'different_to_payroll'
        elif move.is_project_payment:
            payment_request_type = 'project_payment'
        elif move.is_pension_payment_request:
            payment_request_type = 'pension_payment'
        data.update({
            'payment_bank_id': move.payment_bank_id and move.payment_bank_id.id or False,
            'payment_bank_account_id': move.payment_bank_account_id and move.payment_bank_account_id.id or False,
            'payment_issuing_bank_acc_id': move.payment_issuing_bank_acc_id and move.payment_issuing_bank_acc_id.id or False,
            'batch_folio': move.batch_folio,
            'folio': move.folio,
            'payment_state': 'for_payment_procedure',
            'payment_request_id': move.id,
            'l10n_mx_edi_payment_method_id': move.l10n_mx_edi_payment_method_id and move.l10n_mx_edi_payment_method_id.id or False,
            'payment_request_type': payment_request_type,
            'fornight' : move.fornight,
            'payroll_request_type' : move.payroll_request_type,
            'dependancy_id': move.dependancy_id and move.dependancy_id.id or False,
            'sub_dependancy_id': move.sub_dependancy_id and move.sub_dependancy_id.id or False
        })
        if payment_date:
            data.update({'payment_date': payment_date})
        return data

    def create_journal_line_for_payment_procedure(self, invoice):
        """
        Crea los asientos contables para las solicitudes cuando se aprueban para pago
        """
        #===== for the accounting impact of the "Accrued" Budget====#
        if invoice.journal_id and not invoice.journal_id.accured_credit_account_id \
            or not invoice.journal_id.conac_accured_credit_account_id \
            or not invoice.journal_id.accured_debit_account_id \
            or not invoice.journal_id.conac_accured_debit_account_id :
            raise ValidationError("Please configure UNAM and CONAC Accrued account in payment request journal!")
        # Obtiene el monto total de las afectaciones a los códigos programáticos
        amount_total = sum(x.price_total for x in invoice.invoice_line_ids.filtered(lambda x:x.program_code_id))
        # Hace la conversión de la moneda a pesos
        if invoice.currency_id != invoice.company_id.currency_id:
            amount_currency = abs(amount_total)
            balance = invoice.currency_id._convert(amount_currency, invoice.company_currency_id, invoice.company_id, invoice.date)
            currency_id = invoice.currency_id and invoice.currency_id.id or False
        else:
            balance = abs(amount_total)
            amount_currency = 0.0
            currency_id = False
        # Hace los asientos del devengado solo si no es pago a provedores,
        # ya que este se hace en otro flujo
        if not (invoice.is_payment_request and (not invoice.is_different_payroll_request and not invoice.is_pension_payment_request and not invoice.is_payroll_payment_request)):
            # Lineas del DEVENGADO
            invoice.line_ids = [
                (0, 0, {
                    'account_id': invoice.journal_id.accured_credit_account_id and invoice.journal_id.accured_credit_account_id.id or False,
                    'coa_conac_id': invoice.journal_id.conac_accured_credit_account_id and invoice.journal_id.conac_accured_credit_account_id.id or False,
                    'credit': balance,
                    'exclude_from_invoice_tab': True,
                    'conac_move' : True,
                    'amount_currency' : -amount_currency,
                    'currency_id' : currency_id,
                    'partner_id':invoice.partner_id and invoice.partner_id.id or False,
                }),
                (0, 0, {
                    'account_id': invoice.journal_id.accured_debit_account_id and  invoice.journal_id.accured_debit_account_id.id or False,
                    'coa_conac_id': invoice.journal_id.conac_accured_debit_account_id and invoice.journal_id.conac_accured_debit_account_id.id or False,
                    'debit': balance,
                    'exclude_from_invoice_tab': True,
                    'conac_move' : True,
                    'amount_currency' : amount_currency,
                    'currency_id' : currency_id,
                    'partner_id':invoice.partner_id and invoice.partner_id.id or False,
                })
            ]
            #====== the Bank Journal, for the accounting impact of the "Exercised" Budget ======#
            if not self.journal_id.execercise_credit_account_id or not self.journal_id.conac_exe_credit_account_id \
                or not self.journal_id.execercise_debit_account_id or not self.journal_id.conac_exe_debit_account_id:
                raise ValidationError(
                    "Please configure UNAM and CONAC Exercised account in %s journal!" %
                    self.journal_id.name
                )
            # LINEAS DEL EJERCIDO
            invoice.line_ids = [
                (0, 0, {
                    'account_id': self.journal_id.execercise_credit_account_id and self.journal_id.execercise_credit_account_id.id or False,
                    'coa_conac_id': self.journal_id.conac_exe_credit_account_id and self.journal_id.conac_exe_credit_account_id.id or False,
                    'credit': balance,
                    'exclude_from_invoice_tab': True,
                    'conac_move' : True,
                    'amount_currency' : -amount_currency,
                    'currency_id' : currency_id,
                    'partner_id':invoice.partner_id and invoice.partner_id.id or False,
                }),
                (0, 0, {
                    'account_id': self.journal_id.execercise_debit_account_id and self.journal_id.execercise_debit_account_id.id or False,
                    'coa_conac_id': self.journal_id.conac_exe_debit_account_id and self.journal_id.conac_exe_debit_account_id.id or False,
                    'debit': balance,
                    'exclude_from_invoice_tab': True,
                    'conac_move' : True,
                    'amount_currency' : amount_currency,
                    'currency_id' : currency_id,
                    'partner_id':invoice.partner_id and invoice.partner_id.id or False,
                })
            ]

    def notify_currency_purchase(self, request, batch_folio):
        for payment in self.env[ACC_PAYMENT].search([('batch_folio', '=', batch_folio)]):
            payment.write({'currency_availability': True})
        self.env['mail.thread'].inbox_message(
            "Solicitud  de compra de divisas %s aceptada" % request.name,
            "Ya puede completar el proceso de pago de las solicitudes con lote %s" % batch_folio,
            [self.env.user]
        )

    def action_upload_layout(self):
        return {
                'name': _('Load Bank Layout'),
                'type': ACT_WINDOW,
                'res_model': 'load.bank.layout.supplier.payment',
                'view_mode': 'form',
                'view_type': 'form',
                'view_id': self.env.ref('jt_supplier_payment.view_load_bank_layout_supplier_payment_form').id,
                'views': [(False, 'form')],
                'context': {},
                'target': 'new',
            }

    def payments_with_electronic_transfers_from_layouts(self, journal_bank_id, layout_file):
        # Identificación de la cuenta bancaria
        bank_format = journal_bank_id.load_bank_format
        # Identificación del formato del layout
        layout_type = None
        if bank_format == "bbva_bancomer":
            layout_type = LayoutBBVASIT()
        else:
            return
        # Lectura y obtención de los ids de los recibos de nómina
        # logging.info("Lectura y obtención de los ids de los recibos de nómina ...")
        lines_ok, lines_err = Context(self.env, layout_type).load_layout(layout_file)
        # logging.info("Ok.")
        if lines_ok:
            # Obtención de la fecha
            payment_date = datetime.strptime(lines_ok[0].columns['fecha'], "%Y-%m-%d")
            # Obtención de los ids de las solicitudes de pago
            move_ids = [l.payment_id for l in lines_ok]
            # Generación del folio por lotes
            self.env[ACC_MOVE].generate_payment_batch('supplier', move_ids)
            # Creación de los objetos account.payment
            self._schedule_payment(journal_bank_id, move_ids, payment_date=payment_date)
            # Validación/ Rechazo de los pagos
            for line in lines_ok:
                # Flujo de Validación/Rechazo del pago
                if line.payment_status != None:
                    # Si el status del pago es verdadero, se debe validar
                    payment = self.search([('payment_request_id', '=', line.payment_id)])
                    if line.payment_status:
                        payment.post()
                    # Caso contrario se rechaza y se notifica el motivo de rechazo
                    else:
                        payment._reject_payment(line.error_message)
        return lines_err


    def post_payments_with_electronic_transfers_from_layouts(self, journal_bank_id, layout_file):
        # Identificación de la cuenta bancaria
        bank_format = journal_bank_id.load_bank_format
        # Identificación del formato del layout
        layout_type = None
        if bank_format == "bbva_bancomer":
            layout_type = LayoutBBVASIT()
        else:
            return
        # Lectura y obtención de los ids de los recibos de nómina
        # logging.info("Lectura y obtención de los ids de los recibos de nómina ...")
        lines_ok, lines_err = Context(self.env, layout_type).load_layout(layout_file)
        # logging.info("Ok.")
        if lines_ok:
            for line in lines_ok:
                # Flujo de Validación/Rechazo del pago
                if line.payment_status != None:
                    # Obtención de la fecha
                    payment_date = datetime.strptime(line.columns['fecha'], "%Y-%m-%d")
                    # Si el status del pago es verdadero, se debe validar
                    payment = self.search([('payment_request_id', '=', line.payment_id)])
                    if payment and payment.payment_state == 'for_payment_procedure':
                        if line.payment_status:
                            payment.write({'payment_date': payment_date})
                            payment.post()
                            # payment.write({'no_validate_payment': True})
                        # Caso contrario se rechaza y se notifica el motivo de rechazo
                        else:
                            payment.write({'payment_date': payment_date})
                            payment._reject_payment(line.error_message)
                    elif payment and payment.payment_state != 'for_payment_procedure':
                        line.error_message = "El pago %s tiene un estatus diferente a para trámite de pago." % payment.folio
                        lines_err.append(line)
                    else:
                        payment = self.env[ACC_MOVE].browse([line.payment_id])
                        line.error_message = "Pago %s no encontrado." % payment.folio
                        lines_err.append(line)
        return lines_err


    def _reject_payment(self, reason_for_rejection):
        for payment in self:
            if payment.state in ('cancelled','reconciled'):
                raise UserError(_("You can not reject Cancelled or Reconciled payment"))
            if payment.invoice_ids:
                payment.invoice_ids.button_draft()
                payment.invoice_ids.write({'payment_state': 'payment_not_applied','reason_rejection':reason_for_rejection})
                payment.invoice_ids.button_cancel()
            if payment.state == 'posted':
                payment.action_draft()
            payment.reason_for_rejection = reason_for_rejection
            payment.with_context(call_from_reject=True).cancel()
            payment.write({'payment_state': 'rejected'})


    def change_origin_account(self):
        for payment in self:
            if payment.state in ('cancelled','reconciled'):
                raise UserError(_("You can not reject Cancelled or Reconciled payment"))
            if payment.state == 'posted':
                payment.action_draft()
            payment.reason_for_rejection = 'Cambio de cuenta origen.'
            payment.with_context(call_from_reject=True).cancel()
            payment.write({'payment_state': 'rejected'})

    def action_confirm_src_bank(self):
        self.transfer_made_origin = True

    def action_confirm_dst_bank(self):
        self.confirmed_transfer = True
        resource_request_line = self.env['resource.request.line'].search([('payment_id', '=', self.id)])
        if self.payment_issuance_method == 'manual' and len(self.support_documentation) == 0:
            raise UserError("El soporte documental es requerido para transferencias de fondeo a lotes manuales.")
        if resource_request_line:
            resource_request_line.resource_request_id.update_number_of_transfers_validated()
        return self.post()

    def post(self):
        # Validación de soporte documental de pagos manuales con forma de pago
        # por transferecia
        payment_method_id = self.env.ref(PAYMENT_TRASNFER)
        for record in self:
            if record.payment_type == 'outbound' and record.payment_issuance_method == 'manual' and record.l10n_mx_edi_payment_method_id == payment_method_id and not record.support_documentation:
                raise UserError("El soporte documental es requerido para los pagos manuales.")
        result = super(AccountPayment,self).post()
        for record in self:
            # Si se trata de un pago o una transferencia se registra cancela el monto
            # comprometido
            if record.payment_type in ('outbound', 'transfer'):
                record.journal_id.bank_account_id.confirm_expense(record.amount)
        return result

    def cancel(self):
        result = super(AccountPayment,self).cancel()
        for record in self:
            if record.payment_request_type in ('supplier_payment', 'different_to_payroll', 'project_payment'):
                self.env[BATCH_MANAGEMENT].update_number_of_payments_rejected(record.batch_folio, record.amount)
            # Si se trata de un pago o una transferencia se registra cancela el monto
            # comprometido
            if record.payment_type in ('outbound', 'transfer'):
                record.journal_id.bank_account_id.cancel_expense(record.amount)
        return result

    @api.model
    def create(self, values):
        result = super().create(values)
        # Si se trata de un pago o una transferencia se registra el monto comprometido
        # por la cuenta
        if result.payment_type in ('outbound', 'transfer'):
            result.journal_id.bank_account_id.add_expense(result.amount)
        return result