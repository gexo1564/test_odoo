# -*- coding: utf-8 -*-
from datetime import datetime
import logging
from odoo.tests import common
from odoo.exceptions import UserError, ValidationError

ACCOUNT_TOPAY = 'account.topay'
EXPENDITURE_BUDGET_LINE = 'expenditure.budget.line'
PROGRAM_CODE = 'program.code'

DATE_FORMAT = "%Y-%m-%d"

class TestAccountToPay(common.TransactionCase):

    def setUp(self):
        super().setUp()
        specific_items = [(4, self.env['expenditure.item'].search([('item', '=', '711')]).id)]
        self.type_provision = self.env['account.topay.type'].create({
            'name': 'TEST',
            'prefix': 'test',
            'is_allocated_budget_allowed': True,
            'is_nonrecurring_income_allowed': True,
            'is_account_allowed': True,
            'specific_items_allowed_ids': specific_items,
            'item_groups_allowed': '300',
        })
        self.partner_id = self.env['res.partner'].search([])[0]
        self.dependency_id = self.env['dependency'].search([('dependency', '=', '212')])[0]
        self.sub_dependency_id = self.env['sub.dependency'].search([('dependency_id.id', '=', self.dependency_id.id)])[0]
        resource_origin = self.env['resource.origin'].search_read([])
        self.resource_origin = {o['key_origin']: o['id'] for o in resource_origin}
        self.program_code_pa_111_id = self.env[PROGRAM_CODE].search([('program_code', '=', '2022100141801111070025305E0101130101090000000000000000000000')])[0]
        self.program_code_pa_321_id = self.env[PROGRAM_CODE].search([('program_code', '=', '2022100141101321010025305E0101410101090000000000000000000000')])[0]
        self.program_code_ie_355_id = self.env[PROGRAM_CODE].search([('program_code', '=', '2022107533301355080225305E0101710201170000000000000000000000')])[0]
        self.program_code_ie_711_id = self.env[PROGRAM_CODE].search([('program_code', '=', '2022100141701711080125305E0103190301090000000000000000000000')])[0]
        self.program_code_xx_355_id = self.env[PROGRAM_CODE].search([('program_code', '=', '2022107633301355010425305E0101710201170000000000000000000000')])[0]
        self.program_code_xx_411_id = self.env[PROGRAM_CODE].search([('program_code', '=', '2022109552501411000325305E0102170101090000000000000000000000')])[0]
        self.account_ie = self.env['association.distribution.ie.accounts'].search([('ie_key', '=', '201')])
        self.account_id_dep = self.env['account.account'].search([('code', '=', '511.100.111')])
        self.account_id = self.env['account.account'].search([('code', '=', '511.200.271')])

    def test_unlink(self):
        """ Validación de restricción para eliminar cuentas por pagar. """
        account_to_pay = self.env[ACCOUNT_TOPAY].create({
            'type_provision' : self.type_provision.id,
            'partner_id': self.partner_id.id,
            'dependency_id': self.dependency_id.id,
            'sub_dependency_id': self.sub_dependency_id.id,
            'state': 'approved'
        })
        """
        Escenario 1: Eliminar una cuenta con estatus diferente al borrador
        """
        with self.assertRaises(UserError):
            account_to_pay.unlink()

        """
        Escenario 2: Eliminar una cuenta por pagar con estatus en borrador
        """
        account_to_pay.state = 'draft'
        account_to_pay.unlink()

    def test_check_date(self):
        """ Validación de restricción de las fechas permitidas para la creación cuentas por pagar. """
        account_to_pay = self.env[ACCOUNT_TOPAY].create({
            'type_provision' : self.type_provision.id,
            'partner_id': self.partner_id.id,
            'dependency_id': self.dependency_id.id,
            'sub_dependency_id': self.sub_dependency_id.id,
            'state': 'approved'
        })
        self.type_provision.recording_accounting_period_start = datetime.strptime("2022-01-01", DATE_FORMAT)
        self.type_provision.recording_accounting_period_end = datetime.strptime("2022-01-31", DATE_FORMAT)
        """
        Escenario 1: Fecha anterior al rango permitido
        """
        with self.assertRaises(ValidationError):
            account_to_pay.register_date = datetime.strptime("2021-12-31", DATE_FORMAT)

        """
        Escenario 2: Fecha posterior al rango permitido
        """
        with self.assertRaises(ValidationError):
            account_to_pay.register_date = datetime.strptime("2021-12-31", DATE_FORMAT)

        """
        Escenario 3: Fecha igual al inicio del rango permitido
        """
        account_to_pay.register_date = datetime.strptime("2022-01-01", DATE_FORMAT)
        account_to_pay._check_date()

        """
        Escenario 4: Fecha igual al fin del rango permitido
        """
        account_to_pay.register_date = datetime.strptime("2022-01-31", DATE_FORMAT)
        account_to_pay._check_date()

        """
        Escenario 5: Fecha en el rango permitido
        """
        account_to_pay.register_date = datetime.strptime("2022-01-15", DATE_FORMAT)
        account_to_pay._check_date()

    def test_check_origin_resource_lines(self):
        """ Validación en la creación de una cuenta por pagar. """
        account_to_pay = self.env[ACCOUNT_TOPAY].create({
            'type_provision' : self.type_provision.id,
            'partner_id': self.partner_id.id,
            'dependency_id': self.dependency_id.id,
            'sub_dependency_id': self.sub_dependency_id.id,
        })

        """
        Escenario 1: Origen del recurso no válido.
        """
        # Validación de un origen del recurso no permitido
        invoice_line_ids = [(0, 0, {
            'origin_resource_sel': self.resource_origin['02']
        })]
        with self.assertRaises(ValidationError):
            account_to_pay.write({'invoice_line_ids': invoice_line_ids})

        """
        Escenario 2: Validación de lineas con origen del recurso 00
        """
        # Validación de un origen del recurso 00 sin código programático
        account_to_pay.invoice_line_ids = None
        invoice_line_ids = [(0, 0, {
            'origin_resource_sel': self.resource_origin['00']
        })]
        with self.assertRaises(ValidationError):
            account_to_pay.write({'invoice_line_ids': invoice_line_ids})

        # Validación de un origen del recurso 00 con código programático de una partida no permitida
        account_to_pay.invoice_line_ids = None
        invoice_line_ids = [(0, 0, {
            'origin_resource_sel': self.resource_origin['00'],
            'program_code_id': self.program_code_pa_111_id.id
        })]
        with self.assertRaises(ValidationError):
            account_to_pay.write({'invoice_line_ids': invoice_line_ids})

        # Validación de un origen del recurso 00 con código programático con una partida permitida
        # sin cuenta contable
        account_to_pay.invoice_line_ids = None
        invoice_line_ids = [(0, 0, {
            'origin_resource_sel': self.resource_origin['00'],
            'program_code_id': self.program_code_ie_355_id.id
        })]
        with self.assertRaises(ValidationError):
            account_to_pay.write({'invoice_line_ids': invoice_line_ids})

        # Validación de un origen del recurso 00 con código programático con una partida permitida
        # con cuenta contable, pero origen del recurso erróneo.
        account_to_pay.invoice_line_ids = None
        invoice_line_ids = [(0, 0, {
            'origin_resource_sel': self.resource_origin['00'],
            'program_code_id': self.program_code_ie_355_id.id,
            'account_id': self.program_code_ie_355_id.item_id.unam_account_id.id,
        })]
        with self.assertRaises(ValidationError):
            account_to_pay.write({'invoice_line_ids': invoice_line_ids})

        # Validación de un origen del recurso 00 con código programático con una partida permitida
        # con cuenta contable, origen del recurso correcto y monto igual a cero
        account_to_pay.invoice_line_ids = None
        invoice_line_ids = [(0, 0, {
            'origin_resource_sel': self.resource_origin['00'],
            'program_code_id': self.program_code_pa_321_id.id,
            'account_id': self.program_code_pa_321_id.item_id.unam_account_id.id,
        })]
        with self.assertRaises(ValidationError):
            account_to_pay.write({'invoice_line_ids': invoice_line_ids})

        # Validación de un origen del recurso 00 con código programático con una partida permitida
        # con cuenta contable, origen del recurso correcto y monto mayor a cero
        account_to_pay.invoice_line_ids = None
        invoice_line_ids = [(0, 0, {
            'origin_resource_sel': self.resource_origin['00'],
            'program_code_id': self.program_code_pa_321_id.id,
            'account_id': self.program_code_pa_321_id.item_id.unam_account_id.id,
            'origin_money': 1000,
        })]
        account_to_pay.write({'invoice_line_ids': invoice_line_ids})

        # Validación de un origen del recurso 00 con código programático con una partida permitida
        # con cuenta contable, origen del recurso correcto y monto mayor a cero,
        # y dependencia / subdependencia
        account_to_pay.invoice_line_ids = None
        invoice_line_ids = [(0, 0, {
            'origin_resource_sel': self.resource_origin['00'],
            'program_code_id': self.program_code_pa_321_id.id,
            'account_id': self.program_code_pa_321_id.item_id.unam_account_id.id,
            'dependency_id': self.dependency_id.id,
            'sub_dependency_id': self.sub_dependency_id.id,
            'origin_money': 1000,
        })]
        account_to_pay.write({'invoice_line_ids': invoice_line_ids})

        """
        Escenario 3: Validación de lineas con origen del recurso 01
        """
        # Validación de un origen del recurso 01 sin código programático
        account_to_pay.invoice_line_ids = None
        invoice_line_ids = [(0, 0, {
            'origin_resource_sel': self.resource_origin['01']
        })]
        with self.assertRaises(ValidationError):
            account_to_pay.write({'invoice_line_ids': invoice_line_ids})

        # Validación de un origen del recurso 01 con código programático de una partida no permitida
        account_to_pay.invoice_line_ids = None
        invoice_line_ids = [(0, 0, {
            'origin_resource_sel': self.resource_origin['01'],
            'program_code_id': self.program_code_ie_711_id.id
        })]
        with self.assertRaises(ValidationError):
            account_to_pay.write({'invoice_line_ids': invoice_line_ids})

        # Validación de un origen del recurso 01 con código programático con una partida permitida
        # sin cuenta contable
        account_to_pay.invoice_line_ids = None
        invoice_line_ids = [(0, 0, {
            'origin_resource_sel': self.resource_origin['01'],
            'program_code_id': self.program_code_ie_355_id.id
        })]
        with self.assertRaises(ValidationError):
            account_to_pay.write({'invoice_line_ids': invoice_line_ids})

        # Validación de un origen del recurso 01 con código programático con una partida permitida
        # con cuenta contable, pero origen del recurso erróneo.
        account_to_pay.invoice_line_ids = None
        invoice_line_ids = [(0, 0, {
            'origin_resource_sel': self.resource_origin['01'],
            'program_code_id': self.program_code_pa_321_id.id,
            'account_id': self.program_code_pa_321_id.item_id.unam_account_id.id,
        })]
        with self.assertRaises(ValidationError):
            account_to_pay.write({'invoice_line_ids': invoice_line_ids})

        # Validación de un origen del recurso 01 con código programático con una partida permitida
        # con cuenta contable, origen del recurso correcto, sin cuenta de IE
        account_to_pay.invoice_line_ids = None
        invoice_line_ids = [(0, 0, {
            'origin_resource_sel': self.resource_origin['01'],
            'program_code_id': self.program_code_ie_711_id.id,
            'account_id': self.program_code_ie_711_id.item_id.unam_account_id.id,
        })]
        with self.assertRaises(ValidationError):
            account_to_pay.write({'invoice_line_ids': invoice_line_ids})

        # Validación de un origen del recurso 01 con código programático con una partida permitida
        # con cuenta contable, origen del recurso correcto y monto igual a cero
        account_to_pay.invoice_line_ids = None
        invoice_line_ids = [(0, 0, {
            'origin_resource_sel': self.resource_origin['01'],
            'program_code_id': self.program_code_ie_711_id.id,
            'account_id': self.program_code_ie_711_id.item_id.unam_account_id.id,
            'account_ie': self.account_ie.id,
        })]
        with self.assertRaises(ValidationError):
            account_to_pay.write({'invoice_line_ids': invoice_line_ids})

        # Validación de un origen del recurso 00 con código programático con una partida permitida
        # con cuenta contable, origen del recurso correcto y monto mayor a cero
        account_to_pay.invoice_line_ids = None
        invoice_line_ids = [(0, 0, {
            'origin_resource_sel': self.resource_origin['01'],
            'program_code_id': self.program_code_ie_711_id.id,
            'account_id': self.program_code_ie_711_id.item_id.unam_account_id.id,
            'account_ie': self.account_ie.id,
            'origin_money': 1000,
        })]
        account_to_pay.write({'invoice_line_ids': invoice_line_ids})

        # Validación de un origen del recurso 00 con código programático con una partida permitida
        # con cuenta contable, origen del recurso correcto y monto mayor a cero,
        # y dependencia / subdependencia
        account_to_pay.invoice_line_ids = None
        invoice_line_ids = [(0, 0, {
            'origin_resource_sel': self.resource_origin['01'],
            'program_code_id': self.program_code_ie_711_id.id,
            'account_id': self.program_code_ie_711_id.item_id.unam_account_id.id,
            'account_ie': self.account_ie.id,
            'dependency_id': self.dependency_id.id,
            'sub_dependency_id': self.sub_dependency_id.id,
            'origin_money': 1000,
        })]
        account_to_pay.write({'invoice_line_ids': invoice_line_ids})

        """
        Escenario 4: Validación de lineas con origen del recurso 05
        """
        # Validación de un origen del recurso 05 con código programático
        account_to_pay.invoice_line_ids = None
        invoice_line_ids = [(0, 0, {
            'origin_resource_sel': self.resource_origin['05'],
            'program_code_id': self.program_code_ie_711_id.id,
        })]
        with self.assertRaises(ValidationError):
            account_to_pay.write({'invoice_line_ids': invoice_line_ids})

        # Validación de un origen del recurso 05 con código programático
        # sin cuenta contable
        account_to_pay.invoice_line_ids = None
        invoice_line_ids = [(0, 0, {
            'origin_resource_sel': self.resource_origin['05']
        })]
        with self.assertRaises(ValidationError):
            account_to_pay.write({'invoice_line_ids': invoice_line_ids})

        # Validación de un origen del recurso 05 con código programático
        # con cuenta contable sin dependencia/subdependencia, sin el monto
        account_to_pay.invoice_line_ids = None
        invoice_line_ids = [(0, 0, {
            'origin_resource_sel': self.resource_origin['05'],
            'account_id': self.account_id.id
        })]
        with self.assertRaises(ValidationError):
            account_to_pay.write({'invoice_line_ids': invoice_line_ids})

        # Validación de un origen del recurso 05 con código programático
        # con cuenta contable sin dependencia/subdependencia, con el monto
        account_to_pay.invoice_line_ids = None
        invoice_line_ids = [(0, 0, {
            'origin_resource_sel': self.resource_origin['05'],
            'account_id': self.account_id.id,
            'origin_money': 1000,
        })]
        account_to_pay.write({'invoice_line_ids': invoice_line_ids})

        # Validación de un origen del recurso 05 con código programático
        # con cuenta contable con dependencia/subdependencia
        # Sin indicar la dep y sd
        account_to_pay.invoice_line_ids = None
        invoice_line_ids = [(0, 0, {
            'origin_resource_sel': self.resource_origin['05'],
            'account_id': self.account_id_dep.id
        })]
        with self.assertRaises(ValidationError):
            account_to_pay.write({'invoice_line_ids': invoice_line_ids})

        # Validación de un origen del recurso 05 con código programático
        # con cuenta contable con dependencia/subdependencia
        # Indicando la Dep, pero no la sd
        account_to_pay.invoice_line_ids = None
        invoice_line_ids = [(0, 0, {
            'origin_resource_sel': self.resource_origin['05'],
            'account_id': self.account_id_dep.id,
            'dependency_id': self.dependency_id.id,
        })]
        with self.assertRaises(ValidationError):
            account_to_pay.write({'invoice_line_ids': invoice_line_ids})

        # Validación de un origen del recurso 05 con código programático
        # con cuenta contable con dependencia/subdependencia
        # Indicando la Dep/SD, pero sin monto
        account_to_pay.invoice_line_ids = None
        invoice_line_ids = [(0, 0, {
            'origin_resource_sel': self.resource_origin['05'],
            'account_id': self.account_id_dep.id,
            'dependency_id': self.dependency_id.id,
            'sub_dependency_id': self.sub_dependency_id.id,
        })]
        with self.assertRaises(ValidationError):
            account_to_pay.write({'invoice_line_ids': invoice_line_ids})

        # Validación de un origen del recurso 05 con código programático
        # con cuenta contable con dependencia/subdependencia
        # Indicando la Dep/SD, y el monto
        account_to_pay.invoice_line_ids = None
        invoice_line_ids = [(0, 0, {
            'origin_resource_sel': self.resource_origin['05'],
            'account_id': self.account_id_dep.id,
            'dependency_id': self.dependency_id.id,
            'sub_dependency_id': self.sub_dependency_id.id,
            'origin_money': 1000,
        })]
        #with self.assertRaises(ValidationError):
        account_to_pay.write({'invoice_line_ids': invoice_line_ids})


    def test_validate_available_origin_resource(self):
        """ Validación de la obtención del disponible de una cuenta por pagar. """

        """ Escenario 1: Cuenta por pagar de PA sin disponible """
        account_to_pay = self.env[ACCOUNT_TOPAY].create({
            'type_provision' : self.type_provision.id,
            'partner_id': self.partner_id.id,
            'dependency_id': self.dependency_id.id,
            'sub_dependency_id': self.sub_dependency_id.id,
            'invoice_line_ids': [(0, 0, {
                'origin_resource_sel': self.resource_origin['00'],
                'program_code_id': self.program_code_pa_321_id.id,
                'account_id': self.program_code_pa_321_id.item_id.unam_account_id.id,
                'origin_money': self.env[EXPENDITURE_BUDGET_LINE].get_available_by_program_code(self.program_code_pa_321_id.id, 4) + 1,
                'mx_money': self.env[EXPENDITURE_BUDGET_LINE].get_available_by_program_code(self.program_code_pa_321_id.id, 4) + 1,
            })],
        })
        msgs = account_to_pay.validate_available_origin_resource()
        self.assertTrue(len(msgs) > 0)
        account_to_pay.action_validate()
        self.assertEqual(account_to_pay.state, 'rejected_by_insufficiency')

        """ Escenario 2: Cuenta por pagar de PA con disponible """
        account_to_pay = self.env[ACCOUNT_TOPAY].create({
            'type_provision' : self.type_provision.id,
            'partner_id': self.partner_id.id,
            'dependency_id': self.dependency_id.id,
            'sub_dependency_id': self.sub_dependency_id.id,
            'invoice_line_ids': [(0, 0, {
                'origin_resource_sel': self.resource_origin['00'],
                'program_code_id': self.program_code_pa_321_id.id,
                'account_id': self.program_code_pa_321_id.item_id.unam_account_id.id,
                'origin_money': self.env[EXPENDITURE_BUDGET_LINE].get_available_by_program_code(self.program_code_pa_321_id.id, 4),
                'mx_money': self.env[EXPENDITURE_BUDGET_LINE].get_available_by_program_code(self.program_code_pa_321_id.id, 4),
            })],
        })
        msgs = account_to_pay.validate_available_origin_resource()
        self.assertTrue(len(msgs) == 0)
        account_to_pay.action_validate()
        self.assertEqual(account_to_pay.state, 'published')

        """ Escenario 3: Cuenta por pagar de IE sin disponible """
        account_to_pay = self.env[ACCOUNT_TOPAY].create({
            'type_provision' : self.type_provision.id,
            'partner_id': self.partner_id.id,
            'dependency_id': self.dependency_id.id,
            'sub_dependency_id': self.sub_dependency_id.id,
            'invoice_line_ids': [(0, 0, {
                'origin_resource_sel': self.resource_origin['01'],
                'program_code_id': self.program_code_ie_711_id.id,
                'account_id': self.program_code_ie_711_id.item_id.unam_account_id.id,
                'account_ie': self.account_ie.id,
                'origin_money': 10000000000000000,
                'mx_money':     10000000000000000,
            })],
        })
        msgs = account_to_pay.validate_available_origin_resource()
        self.assertTrue(len(msgs) == 2)
        account_to_pay.action_validate()
        self.assertEqual(account_to_pay.state, 'rejected_by_insufficiency')

        """ Escenario 4: Cuenta por pagar de IE con disponible """
        account_to_pay = self.env[ACCOUNT_TOPAY].create({
            'type_provision' : self.type_provision.id,
            'partner_id': self.partner_id.id,
            'dependency_id': self.dependency_id.id,
            'sub_dependency_id': self.sub_dependency_id.id,
            'invoice_line_ids': [(0, 0, {
                'origin_resource_sel': self.resource_origin['01'],
                'program_code_id': self.program_code_ie_711_id.id,
                'account_id': self.program_code_ie_711_id.item_id.unam_account_id.id,
                'account_ie': self.account_ie.id,
                'origin_money': self.env[EXPENDITURE_BUDGET_LINE].get_available_by_program_code(self.program_code_ie_711_id.id, 4),
                'mx_money': self.env[EXPENDITURE_BUDGET_LINE].get_available_by_program_code(self.program_code_ie_711_id.id, 4),
            })],
        })
        msgs = account_to_pay.validate_available_origin_resource()
        self.assertTrue(len(msgs) == 0)
        account_to_pay.action_validate()
        self.assertEqual(account_to_pay.state, 'published')

        """ Escenario 5: Cuenta por pagar de CC sin disponible """
        account_to_pay = self.env[ACCOUNT_TOPAY].create({
            'type_provision' : self.type_provision.id,
            'partner_id': self.partner_id.id,
            'dependency_id': self.dependency_id.id,
            'sub_dependency_id': self.sub_dependency_id.id,
            'invoice_line_ids': [(0, 0, {
                'origin_resource_sel': self.resource_origin['05'],
                'account_id': self.account_id.id,
                'dependency_id': self.dependency_id.id,
                'sub_dependency_id': self.sub_dependency_id.id,
                'origin_money': 10000000000000000,
                'mx_money':     10000000000000000,
            })],
        })
        msgs = account_to_pay.validate_available_origin_resource()
        self.assertTrue(len(msgs) == 1)
        account_to_pay.action_validate()
        self.assertEqual(account_to_pay.state, 'rejected_by_insufficiency')

        """ Escenario 6: Cuenta por pagar de CC con disponible """
        account_to_pay = self.env[ACCOUNT_TOPAY].create({
            'type_provision' : self.type_provision.id,
            'partner_id': self.partner_id.id,
            'dependency_id': self.dependency_id.id,
            'sub_dependency_id': self.sub_dependency_id.id,
            'invoice_line_ids': [(0, 0, {
                'origin_resource_sel': self.resource_origin['05'],
                'account_id': self.account_id.id,
                'dependency_id': self.dependency_id.id,
                'sub_dependency_id': self.sub_dependency_id.id,
                'origin_money': 0.1,
                'mx_money':     0,
            })],
        })
        msgs = account_to_pay.validate_available_origin_resource()
        self.assertTrue(len(msgs) == 0)
        account_to_pay.action_validate()
        self.assertEqual(account_to_pay.state, 'published')