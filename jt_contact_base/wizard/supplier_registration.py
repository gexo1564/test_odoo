# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import base64
import csv
import io
import os
from datetime import datetime
from odoo import fields, models, _
import re

# CONSTANTS FOR DATE FORMAT
DATE_FORMAT_YMD = '%Y-%m-%d'
class SupplierRegistration(models.TransientModel):
    _name = 'supplier.registration'
    _description = 'Supplier Registration'

    
    journal_id = fields.Many2one('account.journal',string='Layout')
    
    fec_data = fields.Binary('FEC File', readonly=True, attachment=False)
    filename = fields.Char(string='Filename', size=256, readonly=True)
    file_error_data = fields.Binary('Error File', readonly=True, attachment=False)
    errfilename = fields.Char(string='ErrorFilename', size=256, readonly=True)

    def generate(self):
        partners = self.env['res.partner'].search([('id', 'in', self._context.get('active_ids'))])
        if self.journal_id.supplier_registration_layout == 'banamex':
            filename = 'Banamex.txt'
            name = 'Banamex'
            # Secuential(4 positions)
            contents = "0001"
            # Date(10 positions) and time(5 positions)
            contents += datetime.now().strftime('%d/%m/%Y%H:%M')
            # Client number(12 positions)
            contents += "000004198566"
            # Client name(36 positions)
            contents += "UNIVERSIDAD AUTONOMA DE MEXICO".ljust(36)
            # Agent number(12 positions)
            contents += '000000000000'
            # Agent name(36 positions)
            contents += ''.ljust(36)

            contents += "\r\n"
            for partner in partners:
                # Movement type
                contents += "A"
                # Bank(4 positions)
                if partner.bank_ids.bank_id.name.upper() == 'BANAMEX':
                    contents += '0000'
                else:
                    contents += partner.bank_ids.bank_id.l10n_mx_edi_code.zfill(4)
                # Bank account type
                contents += '00'
                # Account bank product
                contents += '0000'
                # Instrument
                contents += '00'
                # Bank branch(4 positions)
                if partner.bank_ids[:1].branch_number:
                    contents += str(partner.bank_ids[:1].branch_number).zfill(4)
                else:
                    contents += '0000'
                # Bank account number(20 positions)
                if partner.bank_ids:
                    contents += partner.bank_ids[:1].acc_number.zfill(20)
                else:
                    contents += ''.zfill(20)
                # Person type(2 positions)
                if partner.person_type:
                    if partner.person_type == 'physics':
                        contents += '01'
                    elif partner.person_type == 'moral':
                        contents += '02'
                else:
                    contents += '00'
                # Bank account beneficiary name(55 positions)
                partner_name = ''
                if partner.name:
                    partner_name = partner.name[:55]
                contents += partner_name.ljust(55)
                # Alias(20 positions)
                contents += partner_name[:20].ljust(20)
                # Currency(3 positions)
                contents += '001'
                # Bank account, max import(14 positions)
                contents += '99999999999999'
                # Period
                contents += "D"
                # Partner RFC(18 positions)
                partner_vat = ''
                if partner.vat:
                    partner_vat += partner.vat
                contents += partner_vat.ljust(18)
                # Operation type(2 positions)
                contents += '04'
                # Partner email(40 positions)
                if partner.email:
                    contents += partner.email[:40].ljust(40)
                else:
                    contents += ''.ljust(40)
                # Partner phone number(10 positions)
                if partner.phone:
                    contents += partner.phone[:10].ljust(10)
                else : 
                    contents += '9999999999'
                # Telephone number's company(2 positions)
                contents += ''.ljust(2)
                contents += "\r\n"

            gentextfile = base64.b64encode(bytes(contents, 'utf-8'))
            self.fec_data = gentextfile
            self.filename = filename

        if self.journal_id.supplier_registration_layout == 'bbva':
            filename = 'BBVA Bancomer SIT.txt'
            name = 'BBVA Bancomer SIT'
            contents = 'H'
            contents += "001480758"
            contents += datetime.now().date().strftime('%d-%m-%Y')
            #====== Group ======#
            high_rec = partners.filtered(lambda x:x.instruction == 'high')
            low_rec = partners.filtered(lambda x:x.instruction == 'low')
            changes_rec = partners.filtered(lambda x:x.instruction == 'change')
            if high_rec:
                if self.env.user.lang == 'es_MX':
                    contents += 'ALTAS'.ljust(10)
                else:
                    contents += 'HIGH'.ljust(10)
            elif low_rec:
                if self.env.user.lang == 'es_MX':
                    contents += 'BAJAS'.ljust(10)
                else:
                    contents += 'LOW'.ljust(10)
            elif changes_rec:
                if self.env.user.lang == 'es_MX':
                    contents += 'CAMBIOS'.ljust(10)
                else:
                    contents += 'CHANGES'.ljust(10)
            else:
                contents += ''.ljust(10)

            if high_rec:
                if self.env.user.lang == 'es_MX':
                    contents += 'ALTAPROVEEDOR'.ljust(20)
                else:
                    contents += 'HIGH SUPPLIER'.ljust(20)
            elif low_rec:
                if self.env.user.lang == 'es_MX':
                    contents += 'BAJAPROVEEDOR'.ljust(20)
                else:
                    contents += 'LOW SUPPLIER'.ljust(20)
            elif changes_rec:
                if self.env.user.lang == 'es_MX':
                    contents += 'CAMBIOPROVEEDOR'.ljust(20)
                else:
                    contents += 'CHANGES SUPPLIER'.ljust(20)
            else:
                contents += ''.ljust(20)
                
            #====== Response Code =====#
            contents += '00'
            #====== Description Code Replay =====#
            contents +=  ''.ljust(20)
            #====== Filler =====#
            contents +=  ''.ljust(564)
            contents += "\r\n"
            
            no_of_record_high = 0
            no_of_record_low = 0
            no_of_record_change = 0
            
            for partner in partners:
                contents += 'D'
                
                if partner.instruction and partner.instruction=='high':
                    contents += 'A'
                    no_of_record_high+=1
                elif partner.instruction and partner.instruction=='low':
                    contents += 'B'
                    no_of_record_low += 1 
                elif partner.instruction and partner.instruction=='change':
                    contents += 'C'
                    no_of_record_change += 1
                else:
                    contents += ' '

                if partner.password_beneficiary:
                    if len(partner.password_beneficiary) > 30:
                        contents += partner.password_beneficiary[:30]
                    else:
                        contents += partner.password_beneficiary.ljust(30)
                else:
                    contents += ''.ljust(30)
                contents += 'N    '
                contents += 'MXP'
                contents += '99'
                if partner.bank_ids and partner.bank_ids[:1].acc_number:
                    contents += partner.bank_ids[:1].acc_number.zfill(35)
                else:
                    contents += ''.zfill(35)
                contents += '   '
                contents += '  '
                contents += ''.ljust(35)
                contents += '    '    
                contents += ''.ljust(11)
                contents += ''.ljust(9)
                contents += ''.ljust(20)
                contents += ''.ljust(15)
                contents += ''.ljust(10)
                contents += datetime.now().date().strftime(DATE_FORMAT_YMD)    
                if partner.person_type and partner.person_type=='physics':
                    contents += 'F'
                    contents += ''.ljust(50)
                    contents += ''.ljust(10)
                elif partner.person_type and partner.person_type=='moral':
                    contents += 'M'
                    partner_name = partner.name
                    partner_name = partner_name.translate(str.maketrans('áéíóúäëïöüñÁÉÍÓÚÄËÏÖÜÑ', 'aeiouaeiou&AEIOUAEIOU&')) if partner_name else ''
                    partner_name = ''.join(i for i in partner_name if i not in ['!','“', '#', '/', '(', ')', ')', '=', '=', '?', '¡', '*', '}', '{', ']', '[', '.', ',', ';', ':', '¨', "'",'"', '´', '¨' ])
                    partner_name = partner_name.replace('No','&').ljust(50)
                    if len(partner.name) > 50:
                        contents += partner_name[:50]
                    else:
                        contents += partner_name.ljust(50)
                    contents += datetime.now().date().strftime(DATE_FORMAT_YMD)      
                    contents += ''.ljust(10)
                    
                if partner.person_type and partner.person_type == 'physics':
                    partner_name = partner.name
                    partner_name = partner_name.translate(str.maketrans('áéíóúäëïöüñÁÉÍÓÚÄËÏÖÜÑ', 'aeiouaeiou&AEIOUAEIOU&')) if partner_name else ''
                    partner_name = ''.join(i for i in partner_name if i not in ['!','“', '#', '/', '(', ')', ')', '=', '=', '?', '¡', '*', '}', '{', ']', '[', '.', ',', ';', ':', '¨', "'",'"', '´', '¨' ])
                    partner_name = partner_name.replace('No','&')
                    contents += partner_name[:60].ljust(60)
                    if partner.vat:
                        correct_birth_day = True
                        birth_day_date = str(partner.vat[4:6] + '-' + partner.vat[6:8] + '-' + partner.vat[8:10])
                        try:
                            correct_birth_day = bool(datetime.strptime(birth_day_date, DATE_FORMAT_YMD))
                        except:
                            correct_birth_day = False
                        contents += datetime.strptime(birth_day_date, DATE_FORMAT_YMD).strftime(DATE_FORMAT_YMD) if correct_birth_day else ''.ljust(10)
                    else:
                        contents += ''.ljust(10)
                else:
                    contents += ''.ljust(60)
                    contents += ''.ljust(10)

                if partner.vat:
                    if len(partner.vat) > 15:
                        contents += partner.vat[:14]
                    else:
                        contents += partner.vat.ljust(14)
                else:
                    contents += ''.ljust(14)                #===== Address=======#
                contents += ''.ljust(50)
                #===== Filler=======#
                contents += ''.ljust(8)
                #===== City=======#
                contents += ''.ljust(30)
                #===== Country=======#
                contents += ''.ljust(30)
                #===== Filler=======#
                contents += ''.ljust(35)
                #===== Key to company telephone=======#
                contents += ' '
                #===== Filler=======#
                contents += ''.ljust(39)
                
                #===== Confrimation Type=======#
                contents += '02'
                #======= Email =========#
                if partner.email:
                    if len(partner.email) > 50:
                        contents += partner.email[:50]
                    else:
                        contents += partner.email.ljust(50)
                else : 
                    contents += ''.ljust(50)
                #===== Code of replay =======#
                contents += '  '
                #=====Description of code=======#
                contents += ''.ljust(30)
                #===== Filler=======#
                contents += ''.ljust(8)
                contents += "\r\n"
            #====== Summary Data ===========#
            
            contents += 'T'
            #== Number of Records High ====#
            contents += str(no_of_record_high).zfill(10)
            #== Number of Records Low ====#
            contents += str(no_of_record_low).zfill(10)
            #== Number of Records Changes ====#
            contents += str(no_of_record_change).zfill(10)
            #== Number of Records rejected High ====#
            contents += '0000000000'
            #== Number of Records rejected Low ====#
            contents += '0000000000'
            #== Number of Records rejected Changes ====#
            contents += '0000000000'
            #=== Filler =====#
            contents += ''.ljust(575)
            contents += "\r\n"
            gentextfile = base64.b64encode(bytes(contents, 'utf-8'))
            self.fec_data = gentextfile
            self.filename = filename
#                 contents += "H000747289"
#                 contents += datetime.now().date().strftime('%Y-%m-%d')
#                 contents += "01"
#                 contents += "00                       "
#                 if partner.bank_ids[:1].acc_number:
#                     contents += partner.bank_ids[:1].acc_number.zfill(35)
#                 contents += "   "
#                 contents += "                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   "
#                 contents += "DAP"
#                 if partner.password_beneficiary:
#                     contents += partner.password_beneficiary
#                 contents += "PDA"
#                 contents += "00000000000000000000"
#                 contents += "000000000000000"
#                 contents += "               "
#                 contents += "                         "
#                 contents += "                                     "
#                 contents += "N          "
#                 contents += "                                  "
#                 contents += "                                       "
#                 contents += "                              "
#                 contents += "                                      "
#                 contents += "                            "
#                 if partner.currency_id:
#                     if partner.currency_id.name == 'MXN':
#                         contents += "MXN"
#                         if partner.bank_ids[:1].bic_swift:
#                             contents += partner.bank_ids[:1].bic_swift.zfill(11)
#                         
#                     if partner.currency_id.name == 'USD':
#                         contents += "USD"
#                         if partner.bank_ids[:1].aba:
#                             contents += partner.bank_ids[:1].aba.zfill(9)
#                     if partner.currency_id.name == 'EUR':
#                         contents += "EUR"
#                         if partner.bank_ids[:1].aba:
#                             contents += partner.bank_ids[:1].aba.zfill(9)
#                     if partner.currency_id.name == "CAD":
#                         contents += "CAD"
#                         if partner.bank_ids[:1].aba:
#                             contents += partner.bank_ids[:1].aba.zfill(9)
#                     if partner.currency_id.name == "CHF":
#                         contents += "CHF"
#                         if partner.bank_ids[:1].aba:
#                             contents += partner.bank_ids[:1].aba.zfill(9)
#                     if partner.currency_id.name == "SEK":
#                         contents += "SEK"
#                         if partner.bank_ids[:1].aba:
#                             contents += partner.bank_ids[:1].aba.zfill(9)
#                     if partner.currency_id.name == "GBP":
#                         contents += "GBP"
#                         if partner.bank_ids[:1].aba:
#                             contents += partner.bank_ids[:1].aba.zfill(9)
#                     if partner.currency_id.name == "JPY":
#                         contents += "JPY"
#                         if partner.bank_ids[:1].aba:
#                             contents += partner.bank_ids[:1].aba.zfill(9)
#                 contents += "FA"
#                 contents += "00000000000000001"
#                 if partner.email and len(partner.email) < 50:
#                     contents += partner.email.ljust(50)
#                 contents += "N000000000000"
#                 contents += "0001-01-010001-01-010001-01-01"
#                 contents += "N 0001-01-01700"
#                 contents += "                      "
#                 contents += "                              0001-01-01"
#                 contents += '\n\n'

        if self.journal_id.supplier_registration_layout == 'hsbc':
            name = 'HSBC'
            # with open('HSBC.csv', mode='w',newline='') as file:
            #     writer = csv.writer(file, delimiter=',',
            #                         quotechar='\"', quoting=csv.QUOTE_MINIMAL, lineterminator='\r\n')
            #     total_partner = len(partners)
            #     lst = []
            #     lst.append('AUTHBENEH')
            #     lst.append('ABCXXXXXXXX')
            #     date_today = 'MX-'+datetime.now().date().strftime('%d%m%Y')
            #     lst.append(date_today)
            #     file_sub_date = datetime.now().date().strftime('%Y:%m:%d')
            #     lst.append(file_sub_date)
            #     file_sub_time = datetime.now().time().strftime('%H:%M:%S')
            #     lst.append(file_sub_time)
            #     lst.append('             ')
            #     lst.append(total_partner+1)
            #     lst.append(total_partner)
            #     writer.writerow(tuple(lst))
            #
            #     for partner in partners:
            #         lst = []
            #         lst.append("BENEDET")
            #         if partner.password_beneficiary:
            #             lst.append(partner.password_beneficiary)
            #         else:
            #             lst.append("     ")
            #         lst.append(partner.name or '     ')
            #         lst.append('     ')
            #         lst.append('     ')
            #         lst.append('     ')
            #         lst.append('PPMX_MXOP')
            #         if partner.instruction and partner.instruction=='high':
            #             lst.append('A')
            #         elif partner.instruction and partner.instruction=='low':
            #             lst.append('D')
            #         else:
            #             lst.append(' ')
            #         lst.append('MX')
            #         if partner.bank_ids[:1].acc_number:
            #             lst.append(partner.bank_ids[:1].acc_number)
            #         else:
            #             lst.append(' ')
            #         lst.append('L')
            #         f_str= '021'
            #         lst.append(f_str)
            #         lst.append('BID')
            #         lst.append(' ')
            #         lst.append(' ')
            #         writer.writerow(tuple(lst))

            error_file_data = ''
            file_data = ''
            file_data +=str('AUTHBENEH')
            file_data += ','
            file_data +=str('ABCXXXXXXXX')
            file_data += ','
            total_partner = len(partners)
            date_today = 'MX-' + datetime.now().date().strftime('%d%m%Y')
            file_data +=str(date_today)
            file_data += ','
            file_sub_date = datetime.now().date().strftime('%Y:%m:%d')
            file_data +=str(file_sub_date)
            file_data += ','
            file_sub_time = datetime.now().time().strftime('%H:%M:%S')
            file_data +=str(file_sub_time)
            file_data += ','
            file_data +=str('             ')
            file_data += ','
            file_data +=str(total_partner + 1)
            file_data += ','
            file_data +=str(total_partner)
            file_data += ','
            file_data += "\r\n"

            for partner in partners:
                if partner.instruction: 
                    file_data +=str("BENEDET")
                    file_data += ','
                    if partner.password_beneficiary:
                        file_data +=str(partner.password_beneficiary)
                        file_data += ','
                    else:
                        file_data +=str("     ")
                        file_data += ','
                    file_data +=re.sub('\s\s+', ' ', partner.name.replace(',', ' '))
                    file_data += ','
                    file_data +=str('     ')
                    file_data += ','
                    file_data +=str('     ')
                    file_data += ','
                    file_data +=str('     ')
                    file_data += ','
                    file_data +=str('PPMX_MXOP')
                    file_data += ','
                    if partner.instruction=='high':
                        file_data +=str('A')
                        file_data += ','
                    elif partner.instruction=='low':
                        file_data +=str('D')
                        file_data += ','
                    elif partner.instruction=='change':
                        file_data +=str('C')
                        file_data += ','
                    file_data +=str('MX')
                    file_data += ','
                    if partner.bank_ids[:1].acc_number:
                        file_data +=str(partner.bank_ids[:1].acc_number)
                        file_data += ','
                    else:
                        file_data +=str(' ')
                        file_data += ','
                    file_data +=str('L')
                    file_data += ','
                    f_str= '021'
                    file_data +=str(f_str)
                    file_data += ','
                    file_data +=str('BID')
                    file_data += ','
                    file_data +=str(' ')
                    file_data += ','
                    file_data +=str(' ')
                    file_data += ','
                    file_data += "\r\n"
                else:
                    error_file_data += 'El contacto '+str(partner.name)+' no tiene asignado el valor: Instrucción ante Institución Bancaria. no esta asignado'
                    error_file_data += "\r\n"

            gentextfile = base64.b64encode(bytes(file_data,'utf-8'))

            if len(error_file_data)>0: 
                generrtext = base64.b64encode(bytes(error_file_data, 'utf-8'))
                self.write({
                    'fec_data': gentextfile,
                    'filename': 'HSBC.csv',
                    'file_error_data':generrtext,
                    'errfilename':'Errors.txt'
                })
            else:
                self.write({
                    'fec_data': gentextfile,
                    'filename': 'HSBC.csv',
                })

        if self.journal_id.supplier_registration_layout == 'santander':
            filename = 'Santander.txt'
            name = 'Santander'
            contents = ""
            for partner in partners:
                contents += "SANTAN"
                acc_number = ''
                if partner.bank_ids[:1].acc_number:
                    acc_number = partner.bank_ids[:1].acc_number
                contents += acc_number.zfill(20)
                partner_name = ''
                if partner.name:
                    partner_name = partner.name
                contents += partner_name[:40].ljust(40)
                contents += '\r\n'

            gentextfile = base64.b64encode(bytes(contents, 'utf-8'))
            self.fec_data = gentextfile
            self.filename = filename

        if self.journal_id.supplier_registration_layout != 'hsbc':
            file_data = base64.b64decode(open(os.path.dirname(
                os.path.abspath(__file__)) + "/newfile.txt", "rb").read())
            content = io.StringIO(file_data.decode("utf-8")).read()
            content += contents
            self.fec_data = base64.b64encode(bytes(content, 'utf-8'))
            self.filename = self.filename

            self.write({
                'fec_data': base64.b64encode(content.encode('utf-8')),
                'filename': self.filename,
            })
        return {
            'name': (_('Supplier Registration')),
            'type': 'ir.actions.act_window',
            'res_model': 'supplier.registration',
            'view_mode': 'form',
            'view_type': 'form',
            'view_id': self.env.ref('jt_contact_base.supplier_registration_form_view').id,
            'target': 'new',
            'res_id': self.id,
        }

        # return {
        #     'name': name,
        #     'type': 'ir.actions.act_url',
        #     'url': "web/content/?model=supplier.registration&id=" + str(self.id) + "&filename_field=filename&field=fec_data&download=true&filename=" + self.filename,
        #     'target': 'self',
        # }
