from odoo import models, fields, api, _
from odoo.exceptions import UserError, AccessError, ValidationError
import logging
from datetime import date
import datetime
from lxml import etree
import json
"""
Valores predeterminados
"""
CURRENCY_FUNDS='currency.funds'
APP_REQUEST='application.request'
ACCOUNT_MOVE='account.move'
RES_CURRENCY='res.currency'
class CurrencyFunds(models.Model):

	_name = "currency.funds"
	_description = 'Management of foreign exchange funds for cash per diems'
	_inherit = ['mail.thread', 'mail.activity.mixin']

	name = fields.Char(string='Name')
	associated_account = fields.Many2one('account.account','Associated Account', required=True,tracking=True)
	responsable_user_id = fields.Many2one('res.users','Responsable User',default=lambda self: self.env.user,tracking=True)
	journal_id = fields.Many2one('account.journal',string="Account Journal",required=True,tracking=True)
	currency_id = fields.Many2one(RES_CURRENCY,string='Currency',required=True,tracking=True)
	
	fund_limit = fields.Monetary("Fund Limit",currency_field='currency_id',default=0,tracking=True)

	actual_fund = fields.Monetary('Actual Fund',currency_field='currency_id',default=0,tracking=True)
	currency_reorder_point  = fields.Monetary("Currency Reorden Point",currency_field='currency_id',tracking=True)

	
	committed_fund = fields.Monetary("Committed Money",currency_field='currency_id', compute='_get_committed_money', default=0,tracking=True)
	has_committed_fund = fields.Boolean('Has committed',default=False)

	account_move_id = fields.One2many(ACCOUNT_MOVE, 'currency_funds_move_id', string='Payment Request')
	selected_account_move_id = fields.One2many(ACCOUNT_MOVE, compute="_set_visible_moves",string='Payment Request')
	all_account_move = fields.Integer('All account moves',compute='_count_moves',default=0)
	
	application_request_domain = fields.Char(compute="_compute_application_request", store=False)

	application_request = fields.One2many(APP_REQUEST,'currency_fund_id',string='Application Request Info', compute='_get_app_request_recived',store=False,tracking=True)
	sequence_app_request_id = fields.Many2one('ir.sequence', string='Entry Sequence', required=True, copy=False)

	_sql_constraints = [('currency_id_unique', 'unique(currency_id)', _('Currency: It cannot be selected an used currency.'))]

	@api.model
	def create(self, vals):
		#vals.update({'initial_fund':vals['actual_fund0']})
		moneda=self.env[RES_CURRENCY].search([('id','=',vals['currency_id'])])
		name="Fondo de Divisa ("+str(moneda.name)+")"

		vals.update({'name':name,'sequence_app_request_id':self.sudo()._create_sequence(vals).id})
		res = super(CurrencyFunds, self).create(vals)
		request={
				'name': 'Solicitud Inicial '+str(res.name),
				'currency_fund_id':res.id,
				'currency_id':res.currency_id,
				'required_date':date.today(),
				'amount_funds': res.fund_limit,
				'is_request_for_payment_request':False,
			}
		self.env[APP_REQUEST].create(request)
		return res

	def unlink(self):
		return super(CurrencyFunds, self).unlink()

	@api.model
	def _create_sequence(self, vals, refund=False):
		currency = vals.get('currency_id')
		info = self.env[RES_CURRENCY].search([('id','=',currency)]).name
		seq = {
			'name': _('%s Sequence') % info,
			'implementation': 'no_gap',
			'prefix': 'Solicitud ('+str(info)+') No. ',
			'padding': 6,
			'number_increment': 1,
		}
		seq = self.env['ir.sequence'].create(seq)
		seq_date_range = seq._get_current_sequence()
		seq_date_range.number_next = vals.get('sequence_number_next', 1)
		return seq

	#Revisión del monto
	@api.constrains('fund_limit')
	def _check_fund_limit(self):
		if self.fund_limit<=0:
			raise ValidationError(_('Fund Limit cant be 0 o negative'))

	#Revisión del monto
	@api.constrains('currency_reorder_point')
	def _check_currency_reorder_point(self):
		if self.currency_reorder_point<=0:
			raise ValidationError(_('Currency Reorder Point cant be 0 o negative'))
	
	def _compute_application_request(self):
		for rec in self:
			rec.application_request_domain = json.dumps([('status','=','recived')])

	def notify_state_currency_fund(self,currency,request):
		self.env['mail.thread'].inbox_message(
				"El monto del fondo %s está por debajo del punto de reorden" % currency.name,
				"Se generó la %s, revisa la pestaña de Solicitudes de compra de divisas." % request.name,
				[self.env.user]
			)
		
	
	@api.depends('application_request')
	def _get_app_request_recived(self):
		for rec in self:
			rec.application_request=self.env[APP_REQUEST].search([('currency_fund_id','=',rec.id),('status','=','recived')])

	#Función para generar solicitud de compra
	def set_new_application_request(self,currency,message):
		fund = self.env[CURRENCY_FUNDS].search([('currency_id','=',currency.id)])
		if fund.actual_fund<fund.currency_reorder_point:
			request={
				'currency_fund_id': fund.id,
				'currency_id': fund.currency_id.id,
				'required_date':date.today(),
				'amount_funds': round(fund.fund_limit-fund.actual_fund,2),
				'is_request_for_payment_request':False,
			}
			new_request=self.env[APP_REQUEST].create(request)
			if message:
				self.notify_state_currency_fund(fund,new_request)



	#Funcion que devuelve si el hay suficiencia en el fondo de divisas
	def check_balance(self,currency,funds):
		check=False
		fund = self.env[CURRENCY_FUNDS].search([('currency_id','=',currency)])
		
		if fund != False:
			if fund.actual_fund-funds>=0:
				check=True 
			else:
				check=False
		return check

	#Función para disminuir el monto actual del fondo, esto por parte de las solicitudes de pago
	def sustract_actual_fund(self,currency,funds,solicitudes_ids,message=True):
		fund = self.env[CURRENCY_FUNDS].search([('currency_id','=',currency)])
		if fund != False:
			fund.actual_fund=round(fund.actual_fund-funds,2)
			for sol in solicitudes_ids:
				fund.account_move_id = [(4,sol)]
			moneda=self.env[RES_CURRENCY].search([('id','=',currency)])
			self.set_new_application_request(moneda,message)
	
	def _count_moves(self):
		self.all_account_move = len(self.account_move_id)
	
	def _get_committed_money(self):    
		for rec in self:
			committed= self.env['money.commited'].search([('currency_fund','=',rec.id)]) 
			money=0
			for c in committed:
				money+=money+c.money
			rec.committed_fund=money
			if money!=0:
				rec.has_committed_fund=True
			else:
				rec.has_committed_fund=False            
	
	
	#Función para crear una solicitud de pago una vez que no haya suficiencia en el monto actual
	def create_application_request(self,currency,fondos,lote):
		fund = self.env[CURRENCY_FUNDS].search([('currency_id','=',currency)])
		money_app_request=abs(round(fund.actual_fund-fondos,2))
		money_committed=fund.actual_fund
		#Creación de la solicitud de compra de divisas
		fund.actual_fund=0
		request={
			'name': 'Solicitud Lote '+str(lote),
			'currency_fund_id':fund.id,
			'currency_id':fund.currency_id,
			'required_date':date.today(),
			'amount_funds': money_app_request,
			'is_request_for_payment_request':True,
			'batch':lote,
		}
		solicitud = self.env[APP_REQUEST].create(request)
		#Creación del registro de dinero comprometido
		committed={
			'folio':lote,
			'currency_fund':fund.id,
			'app_request':solicitud.id,
			'currency_id':solicitud.currency_id.id,
			'money':money_committed,
		}
		self.env['money.commited'].create(committed)
		return solicitud


	
	#Función de seguridad en Módulo de Finanzas
	@api.model
	def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
		res = super().fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
		doc = etree.XML(res['arch'])

		is_admin_finance = self.env.user.has_group('jt_payroll_payment.group_on_finance_admin')
		is_finance_user = self.env.user.has_group('jt_payroll_payment.group_admin_finance_user')
		is_currency_fund_user = self.env.user.has_group('jt_currency_purchase_req.group_currency_funds')

		"""
		Sección de Seguridad aplicado para:
			Módulo de Finanzas
			-Gestión de fondo de divisas
		"""

		"""Permisos Lista"""
		list_create=(is_admin_finance,is_finance_user,is_currency_fund_user)
		
		for node in doc.xpath("//" + view_type):
			if any(list_create):
				node.set('edit', '1')
				node.set('create', '1')
				node.set('delete', '1')
				node.set('import', '1')
				node.set('export_xlsx', '1')
				node.set('export', '1')
			else:
				node.set('edit', '0')
				node.set('create', '0')
				node.set('delete', '0')
				node.set('import', '0')
				node.set('export_xlsx', '0')
				node.set('export', '0')

		if 'form' in res['name']:
			"""
				Botones de la barra de estados "Borrador y Publicar"
					Botones para ver las solicitudes de pago
			"""
			button=['count_account_move']
			for node in doc.xpath("//button"):
				if (not any(list_create)) and (node.attrib['name'] in button):
					node.set('modifiers','{"invisible":true}')



		res['arch'] = etree.tostring(doc)

		return res
	
	def count_account_move(self):
		return {
			'name': 'Solicitudes de pago totales',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'views': [(self.env.ref("siif_account_to_pay.payment_req_account_topay_view").id, 'tree'), (self.env.ref("siif_supplier_payment.payment_requests_general").id, 'form')],
			'res_model': ACCOUNT_MOVE,
			'domain': [('currency_funds_move_id', '=', self.id)],
			'type': 'ir.actions.act_window',
			'context':{'payment_request': True,
                       'show_for_supplier_payment':True,
                       'default_type': 'in_invoice', 
                       'default_is_payment_request': 1,
                       'from_move': 1, 
                       'group_by': 'l10n_mx_edi_payment_method_id', 
                       'default_payment_issuance_method': 'manual'
            }
		}

	#Asignar los valores de las solicitudes de pago que se hicieron en el año actual
		
	@api.depends('account_move_id')
	def _set_visible_moves(self):
		year = datetime.datetime.now()
		start_year  = datetime.date(year.year, 1, 1)
		end_year    = datetime.date(year.year, 12, 31)
		for record in self:
			moves = record.account_move_id
			if len(moves)!=0:
				record.selected_account_move_id=moves.filtered(lambda l: l.date_approval_request >= start_year and l.date_approval_request <= end_year and l.payment_state=='paid')
			else:
				record.selected_account_move_id=False


