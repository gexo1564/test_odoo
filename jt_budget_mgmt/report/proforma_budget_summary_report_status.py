# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import datetime
import io
import logging
from odoo import models, fields, api, _
from odoo.tools.profiler import profile
from odoo.tools.misc import formatLang
from odoo.tools.misc import xlsxwriter
import json
import math

AGREEMENT_TYPE = 'agreement.type'
PROJECT_TYPE = 'project.type'
PROGRAM_MODEL = 'program'
SUB_PROGRAM_MODEL = 'sub.program'
DEPENDENCY_MODEL = 'dependency'
SUB_DEPENDENCY_MODEL = 'sub.dependency'
EXPENDITURE_ITEM_MODEL = 'expenditure.item'
DEPARTURE_GROUP_MODEL = 'departure.group'

DATE_FORMAT = '%Y-%m-%d'
MONETARY_FORMAT = '$#,##0.00'

class ProformaBudgetSummaryReport(models.AbstractModel):
    _name = "proforma.budget.summary.report.status"
    _inherit = "account.report"
    _description = "Proforma Budget Summary"

    filter_journals = None
    filter_multi_company = True
    filter_date = {'mode': 'range', 'filter': 'this_month'}
    filter_all_entries = None
    filter_comparison = None
    filter_journals = None
    filter_analytic = None
    filter_unfold_all = None
    filter_hierarchy = None
    filter_partner = None

    # Custom filters
    filter_line_pages = None
    filter_budget_control = None
    filter_program_code_section = None


    def _get_reports_buttons(self):
        return [
            {'name': _('Export (XLSX)'), 'sequence': 2, 'action': 'print_xlsx', 'file_export_type': _('XLSX')},
        ]


    def get_xlsx(self, options, response=None):
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        sheet = workbook.add_worksheet(self._get_report_name()[:31])

        date_default_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2, 'num_format': 'yyyy-mm-dd'})
        date_default_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': 'yyyy-mm-dd'})
        default_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2, 'num_format': MONETARY_FORMAT})
        default_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': MONETARY_FORMAT})
        title_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'bottom': 2, 'num_format': MONETARY_FORMAT})
        super_col_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'align': 'center'})
        level_0_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 13, 'bottom': 6, 'font_color': '#666666', 'num_format': MONETARY_FORMAT})
        level_1_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 13, 'bottom': 1, 'font_color': '#666666', 'num_format': MONETARY_FORMAT})
        level_2_col1_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 1, 'num_format': MONETARY_FORMAT})
        level_2_col1_total_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'num_format': MONETARY_FORMAT})
        level_2_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'num_format': MONETARY_FORMAT})
        level_3_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2, 'num_format': MONETARY_FORMAT})
        level_3_col1_total_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 1, 'num_format': MONETARY_FORMAT})
        level_3_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': MONETARY_FORMAT})

        #Set the first column width to 50
        sheet.set_column(0, 0, 50)
        super_columns = self._get_super_columns(options)
        sheet.write(0, 0,self._get_report_name(), title_style)
        y_offset = 1
        # Todo in master: Try to put this logic elsewhere
        x = super_columns.get('x_offset', 0)
        for super_col in super_columns.get('columns', []):
            cell_content = super_col.get('string', '').replace('<br/>', ' ').replace('&nbsp;', ' ')
            x_merge = super_columns.get('merge')
            if x_merge and x_merge > 1:
                sheet.merge_range(0, x, 0, x + (x_merge - 1), cell_content, super_col_style)
                x += x_merge
            else:
                sheet.write(0, x, cell_content, super_col_style)
                x += 1
        column_sizes = 0
        for row in self.get_header(options):
            x = 0
            for column in row:
                colspan = column.get('colspan', 1)
                header_label = column.get('name', '').replace('<br/>', ' ').replace('&nbsp;', ' ')
                if colspan == 1:
                    sheet.write(y_offset, x, header_label, title_style)
                else:
                    sheet.merge_range(y_offset, x, y_offset, x + colspan - 1, header_label, title_style)
                x += colspan
                column_sizes += 1
            y_offset += 1
        ctx = self._set_context(options)
        ctx.update({'no_format':True, 'print_mode':True, 'prefetch_fields': False})
        # deactivating the prefetching saves ~35% on get_lines running time
        lines = self.with_context(ctx)._get_lines(options)

        if options.get('hierarchy'):
            lines = self._create_hierarchy(lines, options)
        if options.get('selected_column'):
            lines = self._sort_lines(lines, options)

        #write all data rows
        for y in range(0, len(lines)):
            level = lines[y].get('level')
            if lines[y].get('caret_options'):
                style = level_3_style
                col1_style = level_3_col1_style
            elif level == 0:
                #This line was commented because of Proforma Budget Summary Report(XLSX) adds one empy row per each line with data
                #y_offset += 1
                style = level_0_style
                col1_style = style
            elif level == 1:
                style = level_1_style
                col1_style = style
            elif level == 2:
                style = level_2_style
                col1_style = 'total' in lines[y].get('class', '').split(' ') and level_2_col1_total_style or level_2_col1_style
            elif level == 3:
                style = level_3_style
                col1_style = 'total' in lines[y].get('class', '').split(' ') and level_3_col1_total_style or level_3_col1_style
            else:
                style = default_style
                col1_style = default_col1_style
 
            #write the first column, with a specific style to manage the indentation
            cell_type, cell_value = self._get_cell_type_value(lines[y])
            if cell_type == 'date':
                sheet.write_datetime(y + y_offset, 0, cell_value, date_default_col1_style)
            else:
                sheet.write(y + y_offset, 0, cell_value, col1_style)
 
            #write all the remaining cells
            for x in range(1, len(lines[y]['columns']) + 1):
                cell_type, cell_value = self._get_cell_type_value(lines[y]['columns'][x - 1])
                if cell_type == 'date':
                    sheet.write_datetime(y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, date_default_style)
                else:
                    sheet.write(y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, style)

        sheet.set_column(0, 0, 63)
        for i in range(1, column_sizes):
            sheet.set_column(i, i, 19)

        workbook.close()
        output.seek(0)
        generated_file = output.read()
        output.close()
        return generated_file

    # Set columns based on dynamic options
    def print_xlsx(self, options, extra_data):
        options['is_xlsx_report'] = True
        return {
                'type': 'ir_actions_account_report_download',
                'data': {'model': self.env.context.get('model'),
                         'options': json.dumps(options),
                         'output_format': 'xlsx',
                         'financial_id': self.env.context.get('id'),
                         }
                }
    
    def _get_columns_name(self, options):
        column_list = []
        column_list.append({'name': _("Program Code")})

        # Add program code structure fields
        for column in options['selected_program_fields']:
            column_list.append({'name': _(column)})

        for column in options['selected_budget_control']:
            column_list.append({'name': _(column)})
        return column_list

    def _format(self, value,figure_type):
        if self.env.context.get('no_format'):
            return value
        value['no_format_name'] = value['name']
        
        if figure_type == 'float':
            currency_id = self.env.company.currency_id
            if currency_id.is_zero(value['name']):
                # don't print -0.0 in reports
                value['name'] = abs(value['name'])
                value['class'] = 'number text-muted'
            value['name'] = formatLang(self.env, value['name'], currency_obj=currency_id)
            value['class'] = 'number'
            return value
        if figure_type == 'percents':
            value['name'] = str(round(value['name'] * 100, 1)) + '%'
            value['class'] = 'number'
            return value
        value['name'] = round(value['name'], 1)
        return value

    @api.model
    def _init_filter_line_pages(self, options, previous_options=None):
        options['line_pages'] = []
        options['is_xlsx_report'] = False
        start = datetime.strptime(
            str(options['date'].get('date_from')), DATE_FORMAT).date()
        end = datetime.strptime(
            options['date'].get('date_to'), DATE_FORMAT).date()

        budget_lines = self.env['expenditure.budget.line'].search(
            [('expenditure_budget_id.state', '=', 'validate'),('start_date', '>=', start), ('end_date', '<=', end)])
        program_codes = budget_lines.mapped('program_code_id')
        pages = math.ceil(len(program_codes) / 500)
        line_list = []
        for page in range(1, pages + 1):
            line_list.append(page)

        list_labels = self._context.get('lines_data', line_list)
        counter = 1

        if previous_options and previous_options.get('line_pages'):
            line_pages_map = dict((opt['id'], opt['selected']) for opt in previous_options['line_pages'] if opt['id'] != 'divider' and 'selected' in opt and opt['id'] not in previous_options['selected_line_pages'])
        else:
            line_pages_map = {}

        options['selected_line_pages'] = []
        for label in list_labels:
            options['line_pages'].append({
                'id': str(counter),
                'name': str(label),
                'code': str(label),
                'selected': line_pages_map.get(str(counter)),
            })
            if line_pages_map.get(str(counter)):
                options['selected_line_pages'].append(str(label))
            counter += 1



    @api.model
    def _init_filter_budget_control(self, options, previous_options=None):
        options['budget_control'] = []
        if self.env.user.lang == 'es_MX':
            list_labels = [ 'Autorizado', 'Total Asignado Anual', 'Asignado 1er Trimestre',
                           'Asignado 2do Trimestre', 'Asignado 3er Trimestre',
                           'Asignado 4to Trimestre', 'Modificado Anual',
                           'Por Ejercer', 'Comprometido', 'Devengado', 'Ejercido', 'Pagado', 'Disponible']
        else:
            list_labels = ['Expense Item', 'Authorized', 'Assigned Total Annual', 'Assigned 1st Trimester',
                           'Assigned 2nd Trimester', 'Assigned 3rd Trimester',
                           'Assigned 4th Trimester', 'Annual Modified',
                           'Per Exercise', 'Committed', 'Accrued', 'Exercised', 'Paid', 'Available']
        counter = 1

        if previous_options and previous_options.get('budget_control'):
            budget_control_map = dict((opt['id'], opt['selected'])
                                      for opt in previous_options['budget_control'] if
                                      opt['id'] != 'divider' and 'selected' in opt)
        else:
            budget_control_map = {}

        options['selected_budget_control'] = []
        for label in list_labels:
            options['budget_control'].append({
                'id': str(counter),
                'name': str(label),
                'code': str(label),
                'selected': budget_control_map.get(str(counter)),
            })
            if budget_control_map.get(str(counter)):
                options['selected_budget_control'].append(str(label))
            counter += 1


    @api.model
    def _init_filter_program_code_section(self, options, previous_options=None):
        budget_dependence_user = self.env.user.has_group('jt_budget_mgmt.group_dependence_user')
        user_dependencies = self.env.user.dependency_budget_ids.ids
        user_sub_dependencies = self.env.user.sub_dependency_budget_ids.ids
        user_programs = self.env.user.program_budget_ids.ids
        user_subprograms = self.env.user.subprogram_budget_ids.ids
        user_departure_groups = self.env.user.departure_group_budget_ids.ids
        proforma_budget_summary_report_obj = self.env['proforma.budget.summary.report']
        
        proforma_budget_summary_report_obj.apply_filter('report.program.fields', 'code_sections', 'selected_program_fields', 'name', options, previous_options)

        if budget_dependence_user:
            if user_programs:
                 options['selected_programs'] = []
            else:
               proforma_budget_summary_report_obj.apply_filter(PROGRAM_MODEL, 'section_program', 'selected_programs', 'key_unam', options, previous_options, user_programs)

            if user_subprograms: 
                options['selected_sub_programs'] = []
            else:
                proforma_budget_summary_report_obj.apply_filter(SUB_PROGRAM_MODEL, 'section_sub_program', 'selected_sub_programs', 'sub_program', options, previous_options, user_subprograms)
                       
            if user_dependencies:
                options['selected_dependency'] = []
            else:
                proforma_budget_summary_report_obj.apply_filter(DEPENDENCY_MODEL, 'section_dependency', 'selected_dependency', 'dependency', options, previous_options, user_dependencies)
                   
            if user_sub_dependencies:    
                options['selected_sub_dependency'] = []
            else:
                proforma_budget_summary_report_obj.apply_filter(SUB_DEPENDENCY_MODEL, 'section_sub_dependency', 'selected_sub_dependency', 'sub_dependency', options, previous_options, user_sub_dependencies)  

            if user_departure_groups:
                options['selected_items'] = []
                options['selected_departure_group'] = []
            else:    
                proforma_budget_summary_report_obj.apply_filter(EXPENDITURE_ITEM_MODEL, 'section_expense_item', 'selected_expense_item', 'item', options, previous_options, user_departure_groups)
                proforma_budget_summary_report_obj.apply_filter(DEPARTURE_GROUP_MODEL, 'section_departure_group', 'selected_departure_group', 'group', options, previous_options, user_departure_groups)

        else:
            proforma_budget_summary_report_obj.apply_filter(PROGRAM_MODEL, 'section_program', 'selected_programs', 'key_unam', options, previous_options, user_programs)
            proforma_budget_summary_report_obj.apply_filter(SUB_PROGRAM_MODEL, 'section_sub_program', 'selected_sub_programs', 'sub_program', options, previous_options, user_subprograms)
            proforma_budget_summary_report_obj.apply_filter(DEPENDENCY_MODEL, 'section_dependency', 'selected_dependency', 'dependency', options, previous_options, user_dependencies)
            proforma_budget_summary_report_obj.apply_filter(SUB_DEPENDENCY_MODEL, 'section_sub_dependency', 'selected_sub_dependency', 'sub_dependency', options, previous_options, user_sub_dependencies)
            proforma_budget_summary_report_obj.apply_filter(EXPENDITURE_ITEM_MODEL, 'section_expense_item', 'selected_items', 'item', options, previous_options, user_departure_groups)
            proforma_budget_summary_report_obj.apply_filter(DEPARTURE_GROUP_MODEL, 'section_departure_group', 'selected_departure_group', 'group', options, previous_options, user_departure_groups)

        # Origin Resource filter
        proforma_budget_summary_report_obj.apply_filter('resource.origin', 'section_or', 'selected_or', 'key_origin', options, previous_options)

        # Institutional Activity filter
        proforma_budget_summary_report_obj.apply_filter('institutional.activity', 'section_ai', 'selected_ai', 'number', options, previous_options)

        # Budget Program Conversion (CONPP) filter
        options['section_conpp'] = previous_options and previous_options.get(
            'section_conpp') or []
        conpp_ids = [int(acc) for acc in options['section_conpp']]
        selected_conpp = conpp_ids \
                        and proforma_budget_summary_report_obj.env['budget.program.conversion'].browse(conpp_ids) \
                        or proforma_budget_summary_report_obj.env['budget.program.conversion']
        options['selected_conpp'] = selected_conpp.mapped(
            'shcp').mapped('name')

        # SHCP Games (CONPA) filter
        proforma_budget_summary_report_obj.apply_filter('departure.conversion', 'section_conpa', 'selected_conpa','federal_part', options, previous_options)

        # Type of Expense (TG) filter
        proforma_budget_summary_report_obj.apply_filter('expense.type', 'section_expense', 'selected_expenses', 'key_expenditure_type', options, previous_options)

        # Geographic Location (UG) filter
        proforma_budget_summary_report_obj.apply_filter('geographic.location', 'section_ug', 'selected_ug', 'state_key', options, previous_options)

        # Wallet Key (CC) filter
        proforma_budget_summary_report_obj.apply_filter('key.wallet', 'section_wallet', 'selected_wallets', 'wallet_password', options, previous_options)

        # Project Type (TP) filter
        proforma_budget_summary_report_obj.apply_filter(PROJECT_TYPE, 'section_tp', 'selected_tp', 'project_type_identifier', options, previous_options)

        # Project Number filter
        proforma_budget_summary_report_obj.apply_filter(PROJECT_TYPE, 'section_pn', 'selected_pn', 'number', options, previous_options)

        # Stage filter
        proforma_budget_summary_report_obj.apply_filter('stage', 'section_stage', 'selected_stage', 'stage_identifier', options, previous_options)

        # Agreement Type filter
        proforma_budget_summary_report_obj.apply_filter(AGREEMENT_TYPE, 'section_agreement_type', 'selected_type', 'agreement_type', options, previous_options)

        # Agreement Number filter
        proforma_budget_summary_report_obj.apply_filter(AGREEMENT_TYPE, 'section_agreement_number', 'selected_agreement_number', 'number_agreement', options, previous_options)
               

    def check_item_range(self,item_range):
        key_i = int(item_range)
        if key_i >= 100 and key_i <= 199:
            return 1
        elif key_i >= 200 and key_i <= 299:
            return 2
        elif key_i >= 300 and key_i <= 399:
            return 3
        elif key_i >= 400 and key_i <= 499:
            return 4
        elif key_i >= 500 and key_i <= 599:
            return 5
        elif key_i >= 600 and key_i <= 699:
            return 6
        elif key_i >= 700 and key_i <= 799:
            return 7
        elif key_i >= 800 and key_i <= 899:
            return 8
        elif key_i >= 900 and key_i <= 999:
            return 9

    def all_lines_data(self,budget_line,options,lines,start,end):
        #print ("Start Time====",datetime.today())
        need_columns = []
        need_columns_with_format = []
        program_codes = budget_line.mapped('program_code_id')

        # Se forman las columnas a traer de la base de datos.
        col_query ="""
        select
            pc.program_code"""
        # Para optimizar la consulta se obtienen solo las tablas que deben hacer join
        from_query = """,
        program_code pc"""

        where_query = None
        if len(program_codes.ids) == 1:
            where_query = (f"""
        where budget.program_code_id = pc.id
            and pc.state = 'validated'
            and pc.id = {program_codes.ids[0]}""")
        else:
            where_query = (f"""
        where budget.program_code_id = pc.id
            and pc.state = 'validated'
            and pc.id in {tuple(program_codes.ids)}

        """)

        need_total = True
        # Program code struture view fields
        need_to_skip = 0
        for column in options['selected_program_fields']:
            if column in ('Year', 'Año'):
                need_columns.append('year')
                col_query += """,
            yc.name as year"""
                from_query += """,
                year_configuration yc"""
                where_query +="""
                and pc.year=yc.id"""
                need_to_skip += 1
            if column in ('Program', 'PR'):
                need_columns.append('program')
                col_query += """,
            pp.key_unam as program"""
                from_query += """,
                program pp"""
                where_query +="""
                and pc.program_id=pp.id"""
                need_to_skip += 1
            if column in ('Sub Program', 'SP'):
                need_columns.append('sub_program')
                col_query += """,
            sp.sub_program as sub_program"""
                from_query += """,
                sub_program sp"""
                where_query +="""
                and pc.sub_program_id=sp.id"""
                need_to_skip += 1
            if column in ('Dependency', 'DEP'):
                need_columns.append('dependency')
                col_query += """,
            dp.dependency as dependency"""
                from_query += """,
                dependency dp"""
                where_query +="""
                and pc.dependency_id=dp.id"""
                need_to_skip += 1
            if column in ('Sub Dependency', 'SD'):
                need_columns.append('sub_dependency')
                col_query += """,
            sdp.sub_dependency as sub_dependency"""
                from_query += """,
                sub_dependency sdp"""
                where_query +="""
                and pc.sub_dependency_id=sdp.id"""
                need_to_skip += 1
            if column in ('Expenditure Item', 'PAR'):
                need_columns.append('exp_name')
                col_query += """,
            expi.item as exp_name"""
                from_query += """,
                expenditure_item expi"""
                where_query +="""
                and pc.item_id=expi.id"""
                need_to_skip += 1
            if column in ('Check Digit', 'DV'):
                need_columns.append('check_digit')
                col_query += """,
            pc.check_digit as check_digit"""
                need_to_skip += 1
            if column in ('Source of Resource', 'OR'):
                need_columns.append('resource_origin_id')
                col_query += """,
            ro.key_origin as resource_origin_id"""
                from_query += """,
                resource_origin ro"""
                where_query +="""
                and pc.resource_origin_id=ro.id"""
                need_to_skip += 1
            if column in ('Institutional Activity', 'AI'):
                need_columns.append('institutional_activity_id')
                col_query += """,
            inac.number as institutional_activity_id"""
                from_query += """,
                institutional_activity inac"""
                where_query +="""
                and pc.institutional_activity_id=inac.id"""
                need_to_skip += 1
            if column in ('Conversion of Budgetary Program', 'CONPP'):
                need_columns.append('conversion_program')
                col_query += """,
            shcp.name as conversion_program"""
                from_query += """,
                shcp_code shcp,
                budget_program_conversion bpc"""
                where_query +="""
                and pc.budget_program_conversion_id=bpc.id and shcp.id=bpc.shcp"""
                need_to_skip += 1
            if column in ('SHCP items', 'CONPA'):
                need_columns.append('shcp_item')
                col_query += """,
            dc.federal_part as shcp_item"""
                from_query += """,
                departure_conversion as dc"""
                where_query +="""
                and pc.conversion_item_id=dc.id"""
                need_to_skip += 1
            if column in ('Type of Expenditure', 'TG'):
                need_columns.append('type_of_expenditure')
                col_query += """,
            et.key_expenditure_type as type_of_expenditure"""
                from_query += """,
                expense_type as et"""
                where_query +="""
                and pc.expense_type_id=et.id"""
                need_to_skip += 1
            if column in ('Geographic Location', 'UG'):
                need_columns.append('geographic_location')
                col_query += """,
            gl.state_key as geographic_location"""
                from_query += """,
                geographic_location as gl"""
                where_query +="""
                and pc.location_id=gl.id"""
                need_to_skip += 1
            if column in ('Wallet Key', 'CC'):
                need_columns.append('wallet_key')
                col_query += """,
            kw.wallet_password as wallet_key"""
                from_query += """,
                key_wallet as kw"""
                where_query +="""
                and pc.portfolio_id=kw.id"""
                need_to_skip += 1
            if column in ('Type of Project', 'TP'):
                need_columns.append('type_of_project')
                col_query += """,
            ptype.project_type_identifier as type_of_project"""
                from_query += """,
                project_type as ptype"""
                where_query +="""
                and pc.project_type_id=ptype.id"""
                need_to_skip += 1
            if column in ('Project Number', 'NP'):
                need_columns.append('project_number')
                col_query += """,
            projectp.number as project_number"""
                from_query += """,
                project_type as ptypen,
                project_project projectp"""
                where_query +="""
                and pc.project_type_id=ptypen.id and projectp.id=ptypen.project_id"""
                need_to_skip += 1
            if column in ('Stage', 'E'):
                need_columns.append('stage_identofier')
                col_query += """,
            si.stage_identifier as stage_identofier"""
                from_query += """,
                stage as si"""
                where_query +="""
                and pc.stage_id=si.id"""
                need_to_skip += 1
            if column in ('Type of Agreement', 'TC'):
                need_columns.append('type_of_agreement')
                col_query += """,
            atype.agreement_type as type_of_agreement"""
                from_query += """,
                agreement_type as atype"""
                where_query +="""
                and pc.agreement_type_id=atype.id"""
                need_to_skip += 1
            if column in ('Agreement Number', 'NC'):
                need_columns.append('number_of_agreement')
                col_query += """,
            atypen.number_agreement as number_of_agreement"""
                from_query += """,
                agreement_type as atypen"""
                where_query +="""
                and pc.agreement_type_id=atypen.id"""
                need_to_skip += 1


        for column in options['selected_budget_control']:
            if column in ('Partida de Gasto (PAR)', 'Expense Item'):
                need_columns.append('exp_item')
            elif column in ('Authorized', 'Autorizado'):
                need_columns_with_format.append('authorized')
                col_query += """,
            coalesce(authorized,0) authorized"""
            elif column in ('Assigned Total Annual', 'Total Asignado Anual'):
                need_columns_with_format.append('assigned')
                col_query += """,
            total_assigned assigned"""
            elif column in ('Annual Modified', 'Modificado Anual'):
                need_columns_with_format.append('annual_modified')
                col_query += """,
            case
                when authorized is null then coalesce(total_adequacies,0)
                else authorized + coalesce(total_adequacies,0)
            end annual_modified"""
            elif column in ('Assigned 1st Trimester', 'Asignado 1er Trimestre'):
                need_columns_with_format.append('assigned_1st')
                col_query += """,
            first_assigned assigned_1st"""
            elif column in ('Assigned 2nd Trimester', 'Asignado 2do Trimestre'):
                need_columns_with_format.append('assigned_2nd')
                col_query += """,
            second_assigned assigned_2nd"""
            elif column in ('Assigned 3rd Trimester', 'Asignado 3er Trimestre'):
                need_columns_with_format.append('assigned_3rd')
                col_query += """,
            third_assigned assigned_3rd"""
            elif column in ('Assigned 4th Trimester', 'Asignado 4to Trimestre'):
                need_columns_with_format.append('assigned_4th')
                col_query += """,
            fourth_assigned assigned_4th"""
            elif column in ('Per Exercise', 'Por Ejercer'):
                need_columns_with_format.append('per_exercise')
                col_query += """,
            case
                when authorized is null then
                    coalesce(total_adequacies,0) - coalesce(momentos.comprometido,0)
                    - coalesce(momentos.devengado,0) - coalesce(momentos.ejercido,0)
                    - coalesce(momentos.pagado, 0)
                else authorized + coalesce(total_adequacies,0) - coalesce(momentos.comprometido,0)
                    - coalesce(momentos.devengado,0) - coalesce(momentos.ejercido,0)
                    - coalesce(momentos.pagado, 0)
            end per_exercise"""
            elif column in ('Committed', 'Comprometido'):
                need_columns_with_format.append('committed')
                col_query += """,
            coalesce(momentos.comprometido,0) as committed"""
            elif column in ('Accrued', 'Devengado'):
                need_columns_with_format.append('accrued')
                col_query += """,
            coalesce(momentos.devengado,0) accrued"""
            elif column in ('Exercised', 'Ejercido'):
                need_columns_with_format.append('exercised')
                col_query += """,
            coalesce(momentos.ejercido,0) exercised"""
            elif column in ('Paid', 'Pagado'):
                need_columns_with_format.append('paid')
                col_query += """,
            coalesce(momentos.pagado, 0) paid"""
            elif column in ('Available', 'Disponible'):
                need_columns_with_format.append('available')
                col_query += """,
            case
                when authorized is null then
                    coalesce(total_adequacies,0) - coalesce(momentos.comprometido,0)
                    - coalesce(momentos.devengado,0) - coalesce(momentos.ejercido,0)
                    - coalesce(momentos.pagado, 0)
                else authorized + coalesce(total_adequacies,0) - coalesce(momentos.comprometido,0)
                    - coalesce(momentos.devengado,0) - coalesce(momentos.ejercido,0)
                    - coalesce(momentos.pagado, 0)
            end available"""

        year = end.year

        query_momentos = (f"""
        select coalesce(q2.program_code_id,  pagado.program_code_id) program_code_id,
        comprometido, devengado, ejercido, pagado
        from (
            select coalesce(q1.program_code_id, ejercido.program_code_id) program_code_id,
                comprometido, devengado, ejercido
            from (
                select coalesce(comprometido.program_code_id, devengado.program_code_id) program_code_id,
                    comprometido, devengado
                from (
                    select program_code_id, sum(l.mx_money - coalesce(l.amount_used,0)) as comprometido
                    from account_topay a, account_topay_line l
                    where a.id = l.account_topay_id
                    and a.state = 'approved'
                    and l.program_code_id is not null
                    and a.register_date between '{start}' and '{end}'
                    group by program_code_id
                ) comprometido
                full outer join (
                    select distinct ml.program_code_id, sum(ml.price_total) as devengado
                    from account_move am, account_move_line ml
                    where am.id = ml.move_id
                    and payment_state = 'approved_payment'
                    and is_payment_request = true and type = 'in_invoice'
                    and date_approval_request between '{start}' and '{end}'
                    and ml.exclude_from_invoice_tab = false
                    group by ml.program_code_id
                ) devengado
                on comprometido.program_code_id = devengado.program_code_id
            ) q1
            full outer join (
                select ml.program_code_id, sum(ml.price_total) as ejercido
                from account_move am, account_move_line ml
                where am.id = ml.move_id
                and payment_state = 'for_payment_procedure'
                and is_payment_request = true and type = 'in_invoice'
                and date_for_payment_procedure between '{start}' and '{end}'
                and ml.exclude_from_invoice_tab = false
                group by ml.program_code_id
            ) ejercido
            on q1.program_code_id = ejercido.program_code_id
        ) q2
        full outer join (
            select program_code_id, sum(pagado) as pagado
            from(
            select ml.program_code_id, sum(ml.price_total) as pagado
            from account_move m, account_move_line ml, account_payment p
            where m.id = ml.move_id
            and m.id = p.payment_request_id
            and m.payment_state = 'paid'
            and is_payment_request = true and type = 'in_invoice'
            and p.payment_date between '{start}' and '{end}'
            and ml.exclude_from_invoice_tab = false
            group by ml.program_code_id
            UNION ALL
            select ml.program_code_id, sum(ml.price_total) as pagado
            from account_move m, account_move_line ml
            where m.id = ml.move_id
            and ml.price_total > 0.0
            and m.payment_state = 'paid'
            and is_payment_request = true and type = 'entry'
            and m.date between '{start}' and '{end}'
            group by ml.program_code_id) as q1
            group by q1.program_code_id
        ) pagado
        on q2.program_code_id = pagado.program_code_id
        """)

        query = col_query + (f"""
        from (
            select coalesce(expansiones.program_code_id, reducciones.program_code_id) program_code_id,
            coalesce(total_expansion,0) - coalesce(total_reduction,0) total_adequacies
            from (
                select program_code_id, sum(expansion_amount) total_expansion
                from adequacies_lines_report
                where write_date between '{start}' and '{end}' and expansion_amount is not null
                group by program_code_id order by program_code_id
            ) expansiones
            full outer join (
                select program_code_id, sum(reduction_amount) total_reduction
                from adequacies_lines_report
                where write_date between '{start}' and '{end}' and reduction_amount is not null
                group by program_code_id order by program_code_id
            ) reducciones
            on expansiones.program_code_id = reducciones.program_code_id
        ) modificado
        full outer join (
            select autorizado.program_code_id,
                authorized,
                total_assigned,
                first_assigned,
                second_assigned,
                third_assigned,
                fourth_assigned
            from (
                select *,
                    coalesce(first_assigned,0) + coalesce(second_assigned,0) + coalesce(third_assigned, 0) + coalesce(fourth_assigned,0) total_assigned
                from crosstab('
                    select program_code_id, extract(quarter from start_date) quarter, coalesce(assigned,0)
                    from expenditure_budget_line budget, program_code, year_configuration year
                    where budget.program_code_id = program_code.id
                            and program_code.year = year.id
                            and to_date(name, ''yyyy'') between to_date(''{year}'', ''yyyy'') and to_date(''{year}'', ''yyyy'')
                            and budget_line_yearly_id is not null
                    order by program_code_id, quarter
                ', $$VALUES (1), (2), (3), (4)$$)
                as adequacies_lines_report(
                    program_code_id integer,
                    first_assigned numeric,
                    second_assigned numeric,
                    third_assigned numeric,
                    fourth_assigned numeric
                )
            ) asignado, (
                select program_code_id, authorized
                            from expenditure_budget_line budget, program_code, year_configuration year
                            where budget.program_code_id = program_code.id
                                    and program_code.year = year.id
                                    and to_date(name, 'yyyy') between to_date('{year}', 'yyyy') and to_date('{year}', 'yyyy')
                                    and budget_line_yearly_id is null
            ) autorizado
            where asignado.program_code_id = autorizado.program_code_id
        ) budget
        on modificado.program_code_id = budget.program_code_id
        full outer join ({query_momentos}) momentos
        on budget.program_code_id = momentos.program_code_id""") + from_query + where_query
        self.env.cr.execute(query, ())
        my_datas = self.env.cr.dictfetchall()
        total_dict = {}
        #======= get the data count =========#
        selected_line_pages = options['selected_line_pages']
        selected_my_data = []

        for s in selected_line_pages:
            s = int(s)
            start = (s - 1) * 500
            end = s * 500

            selected_my_data = selected_my_data + my_datas[start:end]

        if not selected_line_pages:
            selected_my_data = my_datas[:500]
        if 'is_xlsx_report' in options and options['is_xlsx_report'] == True:
            selected_my_data = my_datas
        for data in selected_my_data:
            columns_in = []
            for c in need_columns:
                columns_in.append({'name':data.get(c)})
            for c in need_columns_with_format:
                columns_in.append(self._format({'name': (data[c] if data.get(c) else 0.0)}, figure_type='float'))
                total_dict[c] = total_dict.get(c, 0.0) + (data[c] if data.get(c) else 0.0)

            lines.append({
                'id': data.get('id'),
                'name':data.get('program_code'),
                'columns': columns_in,
                'level': 2,
                'unfoldable': True,
                'unfolded': True,
            })

        if total_dict and need_total:
            main_cols = []
            for c in need_columns:
                main_cols.append({'name': '','float_name': ''})
            for c in need_columns_with_format:
                main_cols.append(self._format({'name': total_dict.get(c)},figure_type='float'))

            lines.append({
                    'id': 0,
                    'name': _('Total'),
                    'class': 'total',
                    'level': 2,
                    'columns': main_cols,
                })
        return lines,need_total, need_to_skip

    def _get_sum_trimster(self, all_b_lines, s_month, s_day, e_month, e_day):
        return sum(x.assigned if x.start_date.month == s_month and \
                                x.start_date.day == s_day and x.end_date.month == e_month and x.end_date.day == e_day \
                      else 0 for x in all_b_lines)

    @api.model
    def _get_lines(self, options, line_id=None):
        end = datetime.strptime(
            options['date'].get('date_to'), DATE_FORMAT).date()

        year = end.year
        start = datetime.strptime(
            str("%s/01/01" % (str(year))), '%Y/%m/%d').date()

        domain = [
            ('expenditure_budget_id.state', '=', 'validate'),
            ('program_code_id.year.name', '=', year),
        ]

        self.env['proforma.budget.summary.report'].options_selected_based_dependence_user(options)

        if 'selected_programs' in options and len(options['selected_programs']) > 0:
            domain.append(('program_code_id.program_id.key_unam', 'in', options['selected_programs']))
        if 'selected_sub_programs' in options and len(options['selected_sub_programs']) > 0:
            domain.append(('program_code_id.sub_program_id.sub_program', 'in', options['selected_sub_programs']))
        if 'selected_dependency' in options and len(options['selected_dependency']) > 0:
            domain.append(('program_code_id.dependency_id.dependency', 'in', options['selected_dependency']))
        if 'selected_sub_dependency' in options and len(options['selected_sub_dependency']) > 0:
            domain.append(('program_code_id.sub_dependency_id.sub_dependency', 'in', options['selected_sub_dependency']))
            
        if 'selected_expense_item' in options and len(options['selected_expense_item']) > 0:
            domain.append(('program_code_id.item_id.item', 'in', options['selected_expense_item']))
        if 'selected_or' in options and len(options['selected_or']) > 0:
            domain.append(('program_code_id.resource_origin_id.key_origin', 'in', options['selected_or']))
        if 'selected_ai' in options and len(options['selected_ai']):
            domain.append(('program_code_id.institutional_activity_id.number', 'in', options['selected_ai']))
        if 'selected_conpp' in options and len(options['selected_conpp']) > 0:
            domain.append(('program_code_id.budget_program_conversion_id.shcp.name', 'in', options['selected_conpp']))
        if 'selected_conpa' in options and len(options['selected_conpa']) > 0:
            domain.append(('program_code_id.conversion_item_id.federal_part', 'in', options['selected_conpa']))
        if 'selected_expenses' in options and len(options['selected_expenses']) > 0:
            domain.append(('program_code_id.expense_type_id.key_expenditure_type', 'in', options['selected_expenses']))
        if 'selected_ug' in options and len(options['selected_ug']) > 0:
            domain.append(('program_code_id.location_id.state_key', 'in', options['selected_ug']))
        if 'selected_wallets' in options and len(options['selected_wallets']) > 0:
            domain.append(('program_code_id.portfolio_id.wallet_password', 'in', options['selected_wallets']))
        if 'selected_tp' in options and len(options['selected_tp']) > 0:
            domain.append(('program_code_id.project_type_id.project_type_identifier', 'in', options['selected_tp']))
        if 'selected_pn' in options and len(options['selected_pn']) > 0:
            domain.append(('program_code_id.project_number', 'in', options['selected_pn']))
        if 'selected_stage' in options and len(options['selected_stage']) > 0:
            domain.append(('program_code_id.stage_id.stage_identifier', 'in', options['selected_stage']))
        if 'selected_type' in options and len(options['selected_type']) > 0:
            domain.append(('program_code_id.agreement_type_id.agreement_type', 'in', options['selected_type']))
        if 'selected_agreement_number' in options and len(options['selected_agreement_number']) > 0:
            domain.append(('program_code_id.number_agreement', 'in', options['selected_agreement_number']))
        if 'selected_departure_group' in options and len(options['selected_departure_group']) > 0:
            domain.append(('program_code_id.item_id.departure_group_id.group', 'in', options['selected_departure_group']))

        lines = []
        b_line_obj = self.env['expenditure.budget.line']
        budget_lines = b_line_obj.search(domain)

        #=================================== Start Haresh Test Code==============
        lines = []
        if budget_lines:
            lines, __, __ = self.all_lines_data(budget_lines,options,lines,start,end)
        return lines
        #================================= End Haresh Test Code ==========================

    def _get_report_name(self):
        context = self.env.context
        date_report = fields.Date.from_string(context['date_from']) if context.get(
            'date_from') else fields.date.today()
        return '%s_%s_Resumen_Presupuesto_Proforma_Estados' % (
            date_report.year,
            str(date_report.month).zfill(2))