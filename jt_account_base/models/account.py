# -*- coding: utf-8 -*-

from odoo.exceptions import UserError, ValidationError
from odoo import api, fields, models, _, tools


class AccountAccountType(models.Model):
    _inherit = "account.account.type"

    _sql_constraints = [('account_account_type_name_unique', 'unique(name)', _('Account Type: Name must be unique.'))]