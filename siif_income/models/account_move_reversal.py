# -*- coding: utf-8 -*-
import logging
from odoo import models, fields, api
from odoo.tools.translate import _
from odoo.exceptions import UserError
from odoo.exceptions import ValidationError
from datetime import datetime, timedelta

ACCOUNT_MOVE_MODEL = 'account.move'
AFFECTATION_INCOME_BUDGET_MODEL = 'affectation.income.budget'

class AccountMoveReversal(models.TransientModel):
    """
    Account move reversal wizard, it cancel an account move by reversing it.
    """
    _inherit = 'account.move.reversal'


    def _prepare_default_reversal(self, move, comission_date):
        
        # campos para la afectacion
        origin = False
        cri = False
        # Verificar el modelo activo
        name = move.name
        if name.startswith('CCD'):
            origin = 'income-cd'
            # Busqueda de CRI
            account_code = move.account_id.code
            cri = self.env[AFFECTATION_INCOME_BUDGET_MODEL].search([]).associate_cri_account_code(account_code)
            
        elif name.startswith('FDA'):
            origin = 'income-se'
            account_code = move.afectation_account_id.revenue_recognition_account_id.code
            cri = self.env[AFFECTATION_INCOME_BUDGET_MODEL].search([]).associate_cri_account_code(account_code)
            
        if cri:
            # Objetos
            obj_affetation = self.env[AFFECTATION_INCOME_BUDGET_MODEL]
            obj_logbook = self.env['income.adequacies.function']
            obj_fiscal_year = self.env['account.fiscal.year']
            # Afectación al presupuesto
            type_affectation = 'eje-rec'
            amount = (move.amount_interest * -1)
            date_affectation = move.date
            # Mandar datos a la clase de afectación
            cri_id = cri.id
            obj_affetation.search([]).affectation_income_budget(type_affectation,cri_id,date_affectation,amount)
            # Guarda la bitacora de la adecuación
            # Busca el id del año fiscal
            year = date_affectation.year
            fiscal_year_id = obj_fiscal_year.search([('name', '=', year)])
            type_cri = 'income'
            movement = 'for_exercising_collected'
            origin_type = origin
            id_cri = move.name
            journal_id = move.journal_id.id
            update_date_record = datetime.today()
            update_date_income = datetime.today()
            obj_logbook.search([]).logbook_income_budget(fiscal_year_id.id,cri_id,date_affectation,type_cri,movement,origin_type,id_cri,journal_id,update_date_record,amount,update_date_income)
        return {
            'ref': _('Reversal of: %s, %s') % (move.name, self.reason) if self.reason else _('Reversal of: %s') % (move.name),
            'date': comission_date or self.date or move.date,
            'invoice_date': move.is_invoice(include_receipts=True) and (self.date or move.date) or False,
            'journal_id': self.journal_id and self.journal_id.id or move.journal_id.id,
            'invoice_payment_term_id': None,
            'auto_post': True if self.date > fields.Date.context_today(self) else False,
            'invoice_user_id': move.invoice_user_id.id,
            'amount_interest': -move.amount_interest,
            'type_of_revenue_collection': move.type_of_revenue_collection,
            'cri': cri.name if cri else False,
        }

    def reverse_moves(self):
        
        active_model = self.env.context.get('active_model')
        moves = self.env[ACCOUNT_MOVE_MODEL].browse(self.env.context['active_ids']) if active_model == ACCOUNT_MOVE_MODEL else self.move_id
        comission_date = ''

        if active_model == 'comission.profit':
            allowed_groups = ['jt_payroll_payment.group_on_finance_admin','jt_payroll_payment.group_admin_finance_user','jt_payroll_payment.group_approval_user','jt_payroll_payment.group_register_user']

            if not any(self.env.user.has_group(group) for group in allowed_groups):
                raise UserError(_("You aren't allowed to perform this action."))
            
            moves, comission_date = self.return_account_move_date(active_model)
        original_move = moves
        
        # Create default values.
        default_values_list = []
        for move in moves:
            default_values_list.append(self._prepare_default_reversal(move, comission_date))
            move.income_status='reversed'

        batches = [
            [self.env[ACCOUNT_MOVE_MODEL], [], True],   # Moves to be cancelled by the reverses.
            [self.env[ACCOUNT_MOVE_MODEL], [], False],  # Others.
        ]
        for move, default_vals in zip(moves, default_values_list):
            is_auto_post = bool(default_vals.get('auto_post'))
            is_cancel_needed = not is_auto_post and self.refund_method in ('cancel', 'modify')
            batch_index = 0 if is_cancel_needed else 1
            batches[batch_index][0] |= move
            batches[batch_index][1].append(default_vals)

        # Handle reverse method.
        moves_to_redirect = self.env[ACCOUNT_MOVE_MODEL]
        for moves, default_values_list, is_cancel_needed in batches:
            new_moves = moves._reverse_moves(default_values_list, cancel=is_cancel_needed)

            if self.refund_method == 'modify':
                moves_vals_list = []
                for move in moves.with_context(include_business_fields=True):
                    moves_vals_list.append(move.copy_data({'date': self.date or move.date})[0])
                new_moves = self.env[ACCOUNT_MOVE_MODEL].create(moves_vals_list)
                
            moves_to_redirect |= new_moves
            moves_to_redirect.action_post()

        # Create bank movement
        self.create_bank_movement(original_move)
        
        # Create action.
        action = {
            'name': _('Reverse Moves'),
            'type': 'ir.actions.act_window',
            'res_model': ACCOUNT_MOVE_MODEL,
        }
        # Para saber de donde se esta haciendo la reversa y retornar su formulario correcto
        self.reverse_from(moves, action)

        if len(moves_to_redirect) == 1:
            action.update({
                'view_mode': 'form',
                'res_id': moves_to_redirect.id,
            })
        else:
            action.update({
                'view_mode': 'tree,form',
                'domain': [('id', 'in', moves_to_redirect.ids)],
            })
        
        return action
    

    def reverse_from(self, moves, action):
        # Si es desde facturas sera el formulario de notas de crédito
        if moves.type == 'out_invoice':
            action.update({
                'views': [(self.env.ref('jt_income.view_credit_notes_form_income').id, 'form')],
            })


    def create_bank_movement(self, moves):
        
        bank_movements_obj = self.env['bank.movements']
        are_amount_interest = {'bank_interest', 'education_servs', 'aspirants', 'exam_prep'}
        bank_movement_vals = {}
        register_id = ''

        for move in moves:
            type_of_revenue_collection = move.type_of_revenue_collection
            type_of_registry_payment = move.type_of_registry_payment
            if type_of_revenue_collection in are_amount_interest:
                amount_interest = move.amount_interest
                
                bank_movement_vals = {
                    'type_movement': 'unrecognized',
                    'income_bank_journal_id': move.income_bank_journal_id.id,
                    'currency_related': move.currency_id,
                    'movement_date': move.movement_date,
                    'amount_interest': amount_interest,
                    'ref': '',
                    'customer_ref': move.customer_ref,
                    'reference_plugin_1': move.reference_plugin,
                    'reference_plugin_2': move.reference_plugin_2,
                    'reference_plugin_3': move.reference_plugin_3,
                    'description_of_the_movement': move.description_of_the_movement,
                    'id_movto_bancario': move.income_id,
                    # 'charge_amount': '',
                    'folio': '',
                    'bank_branch': move.income_branch,
                    'settlement_date': move.settlement_date,
                    }
            
            elif self.is_comission(type_of_registry_payment):
                moves = self.return_move('comission.profit')
                bank_movement_vals = self.create_bank_movement_comission(moves)
                type_registry_spent_related = moves.type_registry_spent_related
                if self.is_authorized_budget(type_registry_spent_related):
                    self.return_available_program_code(moves)

            register_id = bank_movements_obj.create(bank_movement_vals)
           
        return register_id

    def create_bank_movement_comission(self, moves):
        
        for move in moves:
            bank_movement_vals = {
                'type_movement': 'unrecognized',
                'income_bank_journal_id': move.journal_id.id,
                'currency_related': move.currency_related,
                'movement_date': move.movement_date,
                # 'amount_interest': amount_interest,
                'ref': '',
                'customer_ref': move.customer_ref,
                'reference_plugin_1': move.reference_plugin,
                'reference_plugin_2': move.reference_plugin_2,
                'reference_plugin_3': move.reference_plugin_3,
                'description_of_the_movement': move.description,
                'id_movto_bancario': move.id_movto_ban,
                'charge_amount': move.amount,
                'folio': '',
                'bank_branch': move.bank_branch,
                'settlement_date': move.settlement_date,
            }

        return bank_movement_vals    

    def return_move(self, expected_model):
        moves = self.env[expected_model].browse(self.env.context['active_ids'])    

        return moves

    def return_account_move_date(self, expected_model):
        moves = self.env[expected_model].browse(self.env.context['active_ids'])
        moves.reversed = True
        movement_date = moves.movement_date
        moves = moves.mapped('move_line_ids.move_id')
        
        return moves, movement_date

    def is_comission(self, type_of_registry_payment):
        if type_of_registry_payment == 'comission':
            return True         

    def is_authorized_budget(self, type_registry_spent_related):
        if type_registry_spent_related == 'authorized_budget':
            return True 

    def return_available_program_code(self, moves):
        apportionment_line_ids = moves.mapped('apportionment_line_ids')
        movement_date = moves.movement_date
        program_codes=[]

        for record in apportionment_line_ids:
            program_codes.append((record.program_code_id.id, record.spent))

        self.env['expenditure.budget.line'].return_available(program_codes, movement_date)
        
