from odoo import models, fields, api, _
import logging
logger = logging.getLogger(__name__)


class MoneyFundsReqRejection(models.TransientModel):
	"""
		Wizard para rechazar una solicitud de Fondo Fijos
			Utilizado en:
				-Devoluciones de Fondo Fijo
				-Devoluciones de Solicitudes de Pago
	"""
	_name = 'money.fund.req.rejection'
	_description = 'Money Funds Request Rejection'
	reasons = fields.Text(string='Reasons')
	
	@api.model
	def create(self,vals):
		res = super(MoneyFundsReqRejection, self).create(vals)
		return res
	"""
		Función para asociar el motivo de rechazo y la descripción del rechazo de la solicitud de incremento y ministración
	 		Tipo Doc: Mandar rechazo de documentación
			Tipo Reject: Mandar rechazo de la solicitud
	"""
	def rejected(self):

		#Motivos de rechazo de Solicitudes de Fondo Fijo Asignación
		
		if self.env.context['type_request']=='adding':
			info = self.env['money.funds.request.increment'].search([('id','=',self.env.context['money_funds_req_info'])])
			if (self.env.context['type']=='doc'):
				info.write({'observations_doc':self.reasons,'state':self.env.context['next_state'],'is_published':False})
			elif (self.env.context['type']=='reject'):
				info.write({'reject_reasons':self.reasons,'state':self.env.context['next_state']})

		#Motivos de rechazo de Solicitudes de Fondo Fijo Ministración
		elif self.env.context['type_request']=='ministration':
			info = self.env['money.funds.request.ministration'].search([('id','=',self.env.context['money_funds_req_info'])])
			if (self.env.context['type']=='doc'):
				info.write({'observations_doc':self.reasons,'state':self.env.context['next_state'],'is_published':False})
			elif (self.env.context['type']=='reject'):
				info.write({'reject_reasons':self.reasons,'state':self.env.context['next_state']})

		#Motivos de rechazo de Solicitudes de Fondo Fijo Cancelación
		elif self.env.context['type_request']=='cancel':
			info = self.env['money.funds.cancel'].search([('id','=',self.env.context['money_funds_req_info'])])
			if (self.env.context['type']=='doc'):
				info.write({'observations_doc':self.reasons,'state':self.env.context['next_state'],'is_published':False})
			elif (self.env.context['type']=='reject'):
				info.write({'reject_reasons':self.reasons,'state':self.env.context['next_state']})
			
		self.env['mail.format.money.fund']._make_email_money_fund(self.env.context['type'],info)

		