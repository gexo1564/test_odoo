# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

from odoo.addons.siif_security.tools.utils import check_authorization, check_only_allowed_fields
from datetime import datetime
class Dependency(models.Model):

    _name = 'dependency'
    _description = 'Dependency'
    _rec_name = 'dependency'

    dependency = fields.Char(string='Dependency', size=3)
    description = fields.Text(string='Dependency description')
    sort_description = fields.Text(string='Dependency Sort Description')

    sub_dependency_ids = fields.One2many('sub.dependency', 'dependency_id', string='Sub Dependencies', copy=True, readonly=True)

    allowed_users_cxp = fields.Many2many('res.users',  'allowed_users_dep_cxp_rel', string ='Allowed users')

    _sql_constraints = [('dependency', 'unique(dependency, start_date, end_date)', 'The dependency must be unique.')]

    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')

    @api.constrains('start_date', 'end_date')
    def check_end_date(self):
        if not self.start_date:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("Configure la fecha inicial"))
            else:
                raise ValidationError(_("The start date of validity is required"))
        if self.end_date and self.end_date <= self.start_date:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("La fecha de fin de vigencia no puede ser menor o igual a la fecha de inicio de vigencia"))
            else:
                raise ValidationError(_("The effective end date cannot be less than or equal to the effective date"))

    
    def name_get(self):
        result = []
        for rec in self:
            name = rec.dependency or ''
            if self.env.context:
                if rec.description and (self.env.context.get('show_for_supplier_payment',False) or
                        self.env.context.get('show_for_agreement',False)):
                    name += ' ' + rec.description
                if rec.description and self.env.context.get('from_modification',False):
                    name = rec.description
            result.append((rec.id, name))
        return result
    
    @api.constrains('dependency')
    def _check_dependency(self):
        if not str(self.dependency).isnumeric():
            raise ValidationError(_('The Dependency must be numeric value'))

    def fill_zero(self, code):
        return str(code).zfill(3)

    @api.model
    def create(self, vals):
        if vals.get('dependency') and len(vals.get('dependency')) != 3:
            vals['dependency'] = self.fill_zero(vals.get('dependency'))
        return super(Dependency, self).create(vals)

    @check_authorization(
        'jt_budget_mgmt.group_budget_catalogs_admin_user',
        'jt_budget_mgmt.group_budget_admin_user'
    )
    @check_only_allowed_fields('description', 'sort_description', 'start_date', 'end_date')
    def write(self, vals):
        return super(Dependency, self).write(vals)

    @check_authorization(
        'jt_budget_mgmt.group_budget_catalogs_admin_user',
        'jt_budget_mgmt.group_budget_admin_user'
    )
    def unlink(self):
        for dependency in self:
            program_code = self.env['program.code'].search([('dependency_id', '=', dependency.id)], limit=1)
            if program_code:
                raise ValidationError(_('You can not delete dependency which are mapped with program code!'))
            sub_dependancy = self.env['sub.dependency'].search([('dependency_id', '=', dependency.id)], limit=1)
            if sub_dependancy:
                raise ValidationError(_('You can not delete Dependency which are mapped with Sub Dependancy!'))
        return super(Dependency, self).unlink()

    def validate_dependency(self, dependency_string):
        if len(str(dependency_string)) > 2:
            dependency_str = str(dependency_string).zfill(3)
            if dependency_str.isnumeric():
                dependency = self.search(
                    [('dependency', '=', dependency_str)], limit=1)
                if dependency:
                    return dependency
        return False
    
    # if not return value is not valid #
    def get_validity_dependency(self, dependency, today = datetime.now()):
        query = """
                SELECT id
                FROM dependency
                WHERE 
                    (
                        (start_date IS NULL AND end_date IS NULL AND dependency = %(dependency)s) OR
                        (start_date IS NOT NULL AND end_date IS NULL AND dependency = %(dependency)s AND start_date <= %(today)s) OR
                        (start_date IS NOT NULL AND end_date IS NOT NULL AND dependency = %(dependency)s AND start_date <= %(today)s AND end_date >= %(today)s)
                    )
                """
        params = {'dependency' : str(dependency),
                  'today': today}
        self.env.cr.execute(query, params)
        dependency_validity = self.env.cr.fetchall()

        return dependency_validity