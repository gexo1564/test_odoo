# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
from datetime import datetime, time
from collections import defaultdict
import time
import json
import logging
import pytz

######################
# Nombres de Modelos #
###################### 
ACCOUNT_MOVE = 'account.move'
ACCOUNT_MOVE_LINE_MODEL = 'account.move.line'
CLASSIFIER_INCOME_ITEM = 'classifier.income.item'
INCOME_ADEQUACIES_FUNCTION = 'income.adequacies.function'
INCOME_BUDGETS = 'siif.budget.mgmt.income.budgets'
INCOME_BUDGETS_LINES = 'siif.budget.mgmt.income.budgets.lines'
MODIFICATIONS_FEDERAL_SUBSIDY_LINES = 'modifications.federal.subsidy.lines'
RELATION_ACCOUNT_CRI = 'relation.account.cri'

##########
# Raises #
##########
CRI_NOT_FOUNT = "No se encontró el CRI vinculado a la cuenta contable del diario!"
MSG_CONFIGURE_UNAM_AND_CONAC_ACCOUNT_ES = "Por favor configure la cuenta UNAM y CONAC en diario!"
MSG_CONFIGURE_UNAM_AND_CONAC_ACCOUNT_EN = "Please configure UNAM and CONAC account in budget journal!"

###########
# Strings #
###########
STR_FORMAT_DATE = "%d-%m-%Y"


class ModificationsToTheFederalSubsidy(models.Model):
    _name = 'modifications.federal.subsidy'
    _description = 'Modifications to the Federal Subsidy'
    _rec_name = 'identificator'


    identificator = fields.Char(string='Identificador', tracking=True)
    fiscal_year = fields.Many2one("account.fiscal.year", string="Fiscal Year", tracking=True)
    calendar = fields.Many2one("calendar.assigned.amounts", string="Calendar", tracking=True)
    
    datetime_import = fields.Datetime(string='Date and Time of Import', default=datetime.today(), readonly=True, tracking=True)
    record_number = fields.Integer(string='Number of Records', readonly=True, tracking=True)
    imported_record_number = fields.Integer(string='Number of Records Imported', readonly=True, tracking=True)
    observation = fields.Text(string='Justification of the movement', tracking=True)
    documentary_support = fields.Many2many("ir.attachment", relation="modifications_federal_subsidy_rel", column1="document_id", column2="attachment_id", string="Add Documentary Support", tracking=True)

    modifications_lines_ids = fields.One2many(MODIFICATIONS_FEDERAL_SUBSIDY_LINES, 'modifications_id', string="Lines of Modifications to the Federal Subsidy")
    sequence = fields.Integer(string = 'Sequence', store = False)
    state = fields.Selection([('draft', 'Draft'),
                              ('approve', 'Approve'),
                             ], string="State", default='draft', track_visibility = "always")

    #################
    # Import Status #
    #################
    validation_status = fields.Boolean(string="Validation Status", readonly=True)
    imported_filename = fields.Char(string='Imported Filename')
    imported_file = fields.Binary(string='Imported File', readonly=True, tracking=True)
    total_rows = fields.Integer(string="Total Rows", readonly=True, store=True)
    success_rows = fields.Integer(string='Success Rows', readonly=True, store=True)
    failed_rows = fields.Integer(string='Failed Rows', readonly=True, store=True)
    failed_row_filename = fields.Char(string='Failed Row Filename', compute="_compute_failed_row_filename")
    failed_row_file = fields.Binary(string = "Failed Row File", readonly=True, store=True)

    date = fields.Date(string = "Fecha", store=False)
    company_id = fields.Many2one(comodel_name = "res.company", string = "Compañia", store=False)
    move_id = fields.Many2one(comodel_name = "account.move", string = "Asiento Contable", store=False)
    account_id = fields.Many2one(comodel_name = "account.account", string = "Cuenta", store=False)
    cri = fields.Char(string='CRI', store=False)
    debit = fields.Float(string = "Debe", store=False)
    credit = fields.Float(string = "Haber", store=False)
    ref = fields.Char(string="Referencia")

    #######################
    # Journal entry lines #
    #######################
    account_notes = fields.One2many('account.move.line', 'account_lines_ids', 'Accounting Notes')
    account_entries = fields.One2many(ACCOUNT_MOVE, 'account_lines_ids', 'Accounting Entries')

    journal_id = fields.Many2one('account.journal', string="Journal", readonly = True)
    journal_search = fields.Char(string="Related Journal", store=False)
                     

    def import_lines_button(self):
        return {
            'name': _('Importar Líneas'),
            'type': 'ir.actions.act_window',
            'res_model': 'import.modifications.federal.subsidy',
            'view_mode': 'form',
            'view_type': 'form',
            'views': [(False, 'form')],
            'context': {
                'default_modifications_federal_subsidy_id': self.id,
            },
            'target': 'new',
            }
    
    ##########################
    # define error file name #
    ##########################
    def _compute_failed_row_filename(self):
        for record in self:
            tz = pytz.timezone('America/Mexico_City')
            filename = f'Registros_erroneos-{record.identificator}-{datetime.now(tz).strftime("%Y-%m-%d_%H:%M:%S")}.txt'
            record.failed_row_filename = filename
    
    @api.model
    def create(self, vals):
        res = super(ModificationsToTheFederalSubsidy, self).create(vals)
        identifier = self.env['ir.sequence'].next_by_code('seq.modifications.federal.subsidy')

        res.identificator = identifier
        return res
    

    def action_draft(self):
        self.ensure_one()
        self.state = 'draft'

    def action_approve(self):
        self.ensure_one()

        self.check_duplicate_request_numbers()
        # if self.failed_rows > 0:
        #     raise ValidationError(_("No puede Aprobar el formulario ya que tiene %s línea(s) erronea(s), favor de revisar el archivo de líneas errones y corregir el archivo que desea cargar para que todas las líneas sean exitósas!" % (self.failed_rows)))
        # else:
        self.state = 'approve'
        self.validation_status = True
        
        self.find_duplicates_by_request_number()

        movement_type = ""
        movement_type_liquid = ""
        for data in self.modifications_lines_ids:
            if data.type_transaction == "cleared_adequacy":
                # logging.info("ADECUACION COMPENSADA")
                movement_type = data.type_transaction
                var = 'Finanzas/D00005'
                self.search_journal(var)
            elif data.type_transaction == 'liquid_adequacy':
                # logging.info("ADECUACION LIQUIDA")
                movement_type_liquid = data.type_transaction
            # elif data.type_transaction == 'recalendarization':
            #     movement_recalendarization = data.type_transaction
        if movement_type == 'cleared_adequacy': 
            self.accounting_entries()
        if movement_type_liquid == 'liquid_adequacy':
            var = 'Finanzas/D00006'
            self.search_journal(var) 
            self.accounting_entries_liquid()
            
        self.update_federal_subsidy_estimate()
        self.update_cri()

        
    def check_duplicate_request_numbers(self):
        count = 0
        for rec in self.modifications_lines_ids:
            count += 1
            duplicate_request_numbers = self.env[MODIFICATIONS_FEDERAL_SUBSIDY_LINES].search([
                ('cycle', '=', self.fiscal_year.id),
                ('request_number', '=', rec.request_number),
                ('state_related', '=', 'approve'),
            ])

            if duplicate_request_numbers:
                raise ValidationError(_("The Request Number %s is already registered before. Error in line %s.") % (rec.request_number, count)) 

    def agruped_modifications(self):
        request_data = defaultdict(lambda: {"folio": "", "amount": 0, "date": None, "today": None, "reference": ""})
        extension = 'AMPLIACIÓN'
        reduction = 'REDUCCION'

        for data in self.modifications_lines_ids:
            request_number = data.request_number
            to_datetime = datetime.strptime(data.date_application, STR_FORMAT_DATE)
            date = to_datetime
            reference = str(data.cycle.name) + str(data.branch_ord) + str(data.ur_ord) + str(data.request_number)

            amount_changes = {
                'january': data.january,
                'february': data.february,
                'march': data.march,
                'april': data.april,
                'may': data.may,
                'june': data.june,
                'july': data.july,
                'august': data.august,
                'september': data.september,
                'october': data.october,
                'november': data.november,
                'december': data.december,
            }

            for month, amount in amount_changes.items():
                if amount > 0:
                    if data.transaction == extension:
                        request_data[request_number]["amount"] += amount
                    elif data.transaction == reduction:
                        request_data[request_number]["amount"] -= amount

            if request_data[request_number]["folio"] == "":
                request_data[request_number].update({"folio": request_number, "date": date, "today": date, "reference": reference})

        return dict(request_data)
    

    ###############################################
    # Is responsible for finding the program code #
    ###############################################
    def _find_program_code(self, lines):
        return self.env['calendar.assigned.amounts.lines'].search([
            ('year', '=', lines.cycle.name), 
            ('branch', '=', lines.branch), 
            ('unit', '=', lines.unit), 
            ('purpose', '=', lines.purpose),
            ('function', '=', lines.function_int), 
            ('sub_function', '=', lines.sub_function), 
            ('program', '=', lines.program),
            ('institution_activity', '=', lines.institution_activity), 
            ('project_identification', '=', lines.project_identifier),
            ('project', '=', lines.project), 
            ('new_item_id', '=', lines.federal_item.conversion_key), 
            ('expense_type', '=', lines.expense_type),
            ('funding_source', '=', lines.funding_source), 
            ('federal', '=', lines.federative_entity), 
            ('key_wallet', '=', lines.key_wallet),
            ('state_related', '=', 'validate')
        ])
    
    def find_duplicates_by_request_number(self):
        ############################################################
        # Buscar registros relacionados con la modificación actual #
        ############################################################
        lines = self.env[MODIFICATIONS_FEDERAL_SUBSIDY_LINES].search([('modifications_id', '=', self.id)])
        ##########################################
        # Iterar sobre los registros encontrados #
        ##########################################
        for record in lines:
            if record.type_request == 'Calendarios':
                record.write({'type_transaction': 'recalendarization'})
            elif record.type_request == 'Claves Presupuestarias':
                record.write({'type_transaction': 'cleared_adequacy'})
            elif record.type_request == 'Otros R-23' or record.type_request == 'Traspaso entre Ramos':
                record.write({'type_transaction': 'liquid_adequacy'})

    ##################
    # Journal Search #
    ##################
    def search_journal(self,type_adequacy_var):
        search = self.env['siif.budget.mgmt.relation.journal.process'].search([('name', '=', type_adequacy_var)])
        if not search:
            raise ValidationError(_(f" No se encontró el diario {type_adequacy_var} solicite al administrador que lo modifique"))
        self.journal_search = search.journal_name
        self.journal_id = search.journal_name

    
    def create_accounting_entries(self, request_number):
        lines = self.modifications_lines_ids.filtered(lambda x: x.request_number == request_number and x.type_transaction == 'cleared_adequacy')
        if not lines:
            return

        #############################
        # debit and credit accounts #
        #############################
        debit_account_id = self.calendar.journal_id.default_debit_account_id.id
        credit_account_id = self.calendar.journal_id.default_credit_account_id.id
        journal = self.journal_id
                
        ##################
        # Account object #
        ##################
        obj_account = self.env[RELATION_ACCOUNT_CRI]
        ##################################################
        # Find if the journal account code starts with 4 #
        ##################################################
        cri_account = obj_account.search([('cuentas_id', '=', credit_account_id),('cuentas_id', 'like', '4%')])
        if not cri_account:
            cri_account = obj_account.search([('cuentas_id', '=', debit_account_id),('cuentas_id', 'like', '4%')])
            if not cri_account:
                raise ValidationError(_(CRI_NOT_FOUNT))
        ########################################################
        # Sends all the parameters to create the journal entry #
        ########################################################
        cri = self.env[CLASSIFIER_INCOME_ITEM].search([('id', '=', cri_account.cri.id)])

        company_id = self.env.user.company_id.id
        date = datetime.strptime(lines[0].date_application, STR_FORMAT_DATE)
        partner_id = self.env.user.partner_id.id
        account_lines_ids = self.id

        ##################################
        # sums the amounts of all months #
        ##################################
        debit_amount = 0
        credit_amount = 0
        expansion = ""
        reduction = ""
        move_vals = {}
        vals = []
        for line in lines:
            total_amount = sum(getattr(line, month) for month in ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'])
            if line.transaction == 'AMPLIACION':
                expansion = line.transaction
                debit_amount += total_amount
                reduction = None
            elif line.transaction == 'REDUCCION':
                expansion = None
                reduction = line.transaction
                credit_amount += total_amount
        if expansion == 'AMPLIACION' or reduction == 'REDUCCION':
            total_amount_signed = (debit_amount + credit_amount) / 2
            move_vals = {'ref': line.folio, 'account_lines_ids': account_lines_ids, 'conac_move': True, 'type': 'entry',
                         'date': date, 'journal_id': journal.id, 'company_id': company_id, 'amount_total_signed': total_amount_signed}

            vals.append({
                'account_id': journal.income_run_credit_account_id.id,
                'coa_conac_id': journal.conac_income_run_credit_account_id.id,
                'partner_id': partner_id,
                'account_lines_ids': self.id,
                'cri': cri.name,
                'price_unit': -credit_amount,
                'amount_residual': -credit_amount,
            })
            vals.append({
                'account_id': journal.income_run_debit_account_id.id,
                'coa_conac_id': journal.conac_income_run_debit_account_id.id,
                'partner_id': partner_id,
                'account_lines_ids': self.id,
                'cri': cri.name,
                'price_unit': debit_amount,
                'amount_residual': debit_amount,
            })
        
        # Creación de del asiento contable (account.move)
        move_id = self.env[ACCOUNT_MOVE].insert_sql([move_vals])[0]

        # Creación de los apuntes contables
        self.env[ACCOUNT_MOVE_LINE_MODEL].insert_sql(move_id, vals)

        # Se publican los asientos contables
        self.env[ACCOUNT_MOVE]._post(move_id, journal.id)


    def accounting_entries(self):
        for request_number in set(self.modifications_lines_ids.filtered(lambda x: x.type_transaction == 'cleared_adequacy').mapped('request_number')):
            self.create_accounting_entries(request_number)
    
    
    def create_liquid_adequacy_entries(self, request_number):
        lines = self.modifications_lines_ids.filtered(lambda x: x.request_number == request_number and x.type_transaction == 'liquid_adequacy')
        if not lines:
            return

        #############################
        # debit and credit accounts #
        #############################
        debit_account_id = self.calendar.journal_id.default_debit_account_id.id
        credit_account_id = self.calendar.journal_id.default_credit_account_id.id
        journal = self.journal_id

        ##################
        # Account object #
        ##################
        obj_account = self.env[RELATION_ACCOUNT_CRI]
        ##################################################
        # Find if the journal account code starts with 4 #
        ##################################################
        cri_account = obj_account.search([('cuentas_id', '=', credit_account_id),('cuentas_id', 'like', '4%')])
        if not cri_account:
            cri_account = obj_account.search([('cuentas_id', '=', debit_account_id),('cuentas_id', 'like', '4%')])
        if not cri_account:
            raise ValidationError(_(CRI_NOT_FOUNT))
        ########################################################
        # Sends all the parameters to create the journal entry #
        ########################################################
        cri = self.env[CLASSIFIER_INCOME_ITEM].search([('id', '=', cri_account.cri.id)])

        company_id = self.env.user.company_id.id
        date = datetime.strptime(lines[0].date_application, STR_FORMAT_DATE)
        partner_id = self.env.user.partner_id.id
        account_lines_ids = self.id

        ##################################
        # sums the amounts of all months #
        ##################################
        total_amount_signed = 0
        expansion = ""
        reduction = ""
        move_vals = {}
        vals = []
        for line in lines:
            total_amount = sum(getattr(line, month) for month in ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'])
            total_amount_signed += total_amount
            if line.transaction == 'AMPLIACION':
                expansion = line.transaction
                reduction = None
            elif line.transaction == 'REDUCCION':
                expansion = None
                reduction = line.transaction
        if expansion == 'AMPLIACION' or reduction == 'REDUCCION':
            
            move_vals = {'ref': line.folio, 'account_lines_ids': account_lines_ids, 'conac_move': True, 'type': 'entry',
                         'date': date, 'journal_id': journal.id, 'company_id': company_id, 'amount_total_signed': total_amount_signed}

            vals.append({
                'account_id': journal.income_run_credit_account_id.id,
                'coa_conac_id': journal.conac_income_run_credit_account_id.id,
                'partner_id': partner_id,
                'account_lines_ids': self.id,
                'cri': cri.name,
                'price_unit': -total_amount_signed,
                'amount_residual': -total_amount_signed,
            })
            vals.append({
                'account_id': journal.income_run_debit_account_id.id,
                'coa_conac_id': journal.conac_income_run_debit_account_id.id,
                'partner_id': partner_id,
                'account_lines_ids': self.id,
                'cri': cri.name,
                'price_unit': total_amount_signed,
                'amount_residual': total_amount_signed,
            })
        
        # Creación de del asiento contable (account.move)
        move_id = self.env[ACCOUNT_MOVE].insert_sql([move_vals])[0]

        # Creación de los apuntes contables
        self.env[ACCOUNT_MOVE_LINE_MODEL].insert_sql(move_id, vals)

        # Se publican los asientos contables
        self.env[ACCOUNT_MOVE]._post(move_id, journal.id)

    def accounting_entries_liquid(self):
        for request_number in set(self.modifications_lines_ids.filtered(lambda x: x.type_transaction == 'liquid_adequacy').mapped('request_number')):
            self.create_liquid_adequacy_entries(request_number)
    
    ##############################################################
    # This function sends to execute the following two functions #
    ##############################################################
    def update_federal_subsidy_estimate(self):
        modifications_lines = self.env[MODIFICATIONS_FEDERAL_SUBSIDY_LINES].search([('modifications_id', '=', self.id)])
        count = 0
        for lines in modifications_lines:
            program_code_search = self._find_program_code(lines)
            count += 1
            if program_code_search:
                self._update_monthly_values(program_code_search, lines, count)
    
    
    ##############################################################
    # Updates monthly values based on lines and transaction type #
    ##############################################################
    def _update_monthly_values(self, program_code_search, lines, count):
        months = [
                'january', 'february', 'march', 'april', 'may', 'june',
                'july', 'august', 'september', 'october', 'november', 'december'
        ]
        number = count
        error_msg = ''
        
        for month in months:
            modifications_field = f"modifications_{month}"
            modifications_month = getattr(program_code_search, modifications_field)
            modifications = round(modifications_month, 2)

            collect_field = f"collect_{month}"
            collect_amount = getattr(program_code_search, collect_field)
            collect = round(collect_amount, 2)

            values = getattr(lines, month)
            value = round(values, 2)

            current_program_value = getattr(program_code_search, month)

            amount_modifications_field = f"amount_modifications_{month}"

            if lines.transaction == 'AMPLIACION':
                if value > 0 and modifications == 0:
                    setattr(program_code_search, modifications_field, current_program_value + value)
                    setattr(program_code_search, collect_field, getattr(program_code_search, collect_field) + value)
                    setattr(program_code_search, amount_modifications_field, getattr(program_code_search, amount_modifications_field) + value)
                    ############################
                    # modify the annual amount #
                    ############################
                    program_code_search.modifications_accumulated_annual += value
                    program_code_search.modifications_annual_amount = (program_code_search.annual_amount + program_code_search.modifications_accumulated_annual)
                    
                if value > 0 and modifications > 0:
                    setattr(program_code_search, modifications_field, modifications_month + value)
                    setattr(program_code_search, collect_field, getattr(program_code_search, collect_field) + value)
                    setattr(program_code_search, amount_modifications_field, getattr(program_code_search, amount_modifications_field) + value)
                    ############################
                    # modify the annual amount #
                    ############################
                    program_code_search.modifications_accumulated_annual += value
                    program_code_search.modifications_annual_amount = (program_code_search.annual_amount + program_code_search.modifications_accumulated_annual)
            if lines.transaction == 'REDUCCION':
                if value > 0 and modifications == 0:
                    if (current_program_value - modifications) >= value and collect >= value:
                        setattr(program_code_search, modifications_field, current_program_value - value)
                        setattr(program_code_search, collect_field, getattr(program_code_search, collect_field) - value)
                        setattr(program_code_search, amount_modifications_field, getattr(program_code_search, amount_modifications_field) - value)
                        ############################
                        # modify the annual amount #
                        ############################
                        program_code_search.modifications_accumulated_annual -= value
                        program_code_search.modifications_annual_amount = (program_code_search.annual_amount + program_code_search.modifications_accumulated_annual)
                    elif (current_program_value - modifications) < value and collect < value:
                        ###########################################################
                        # Manda un mensaje de falta de suficiencia presupuestaria #
                        ###########################################################
                        mensaje = _("It is not possible to perform the movement due to budget insufficiency in %(month)s. You have: $%(current_program_value)s and you need: $%(value)s. Error in line %(number)s.") % {'month':_(month), 'current_program_value': current_program_value, 'value': value, 'number': number}
                        error_msg += mensaje
                        raise ValidationError(error_msg)
                elif value > 0 and modifications > 0:
                    if modifications >= value:
                        setattr(program_code_search, modifications_field, modifications - value)
                        setattr(program_code_search, collect_field, getattr(program_code_search, collect_field) - value)
                        setattr(program_code_search, amount_modifications_field, getattr(program_code_search, amount_modifications_field) - value)
                        ############################
                        # modify the annual amount #
                        ############################
                        program_code_search.modifications_accumulated_annual -= value
                        program_code_search.modifications_annual_amount = (program_code_search.annual_amount + program_code_search.modifications_accumulated_annual)
                    elif modifications < value:
                        ###########################################################
                        # Manda un mensaje de falta de suficiencia presupuestaria #
                        ###########################################################
                        mensaje = _("It is not possible to perform the movement due to budget insufficiency in %(month)s. You have: $%(modifications)s and you need: $%(value)s. Error in line %(number)s.") % {'month':_(month), 'modifications': modifications, 'value': value, 'number': number}
                        error_msg += mensaje
                        raise ValidationError(error_msg)

            
            

    
    def update_cri(self):
        debit_account_id = self.calendar.journal_id.default_debit_account_id.id
        credit_account_id = self.calendar.journal_id.default_credit_account_id.id

        cri_account = self._find_cri_account(credit_account_id) or self._find_cri_account(debit_account_id)
    
        cri = self._get_cri_by_account_id(cri_account)
    
        budget_validated = self._get_budget_by_cri_id(cri.id)
    
        self._update_budget_fields(budget_validated)
    
    def _find_cri_account(self, account_id):
        obj_account = self.env[RELATION_ACCOUNT_CRI]
        return obj_account.search([('cuentas_id', '=', account_id), ('cuentas_id', 'like', '4%')])
    
    def _get_cri_by_account_id(self, cri_account):
        return self.env[CLASSIFIER_INCOME_ITEM].search([('id', '=', cri_account.cri.id)])
    
    def _get_budget_by_cri_id(self, cri_id):
        return self.env[INCOME_BUDGETS_LINES].search([('estimated_cri_id', '=', cri_id)])
    
    def _update_budget_fields(self, budget_validated):
        month_fields = ['january', 'february', 'march', 'april', 'may', 'june',
                    'july', 'august', 'september', 'october', 'november', 'december']

        modifications_lines = self.env[MODIFICATIONS_FEDERAL_SUBSIDY_LINES].search([('modifications_id', '=', self.id)])

        for month_field in month_fields:
            for lines in modifications_lines:
                value = getattr(lines, month_field)
                if value > 0:
                    if lines.transaction == 'AMPLIACION':
                        self._update_ampliacion_fields(budget_validated, month_field, value)
                    elif lines.transaction == 'REDUCCION':
                        self._update_reduccion_fields(budget_validated, month_field, value)

        self._update_total_fields(budget_validated, month_fields)
    
    def _update_ampliacion_fields(self, budget_validated, month_field, value):
        setattr(budget_validated, f'to_collect_{month_field}_field', getattr(budget_validated, f'to_collect_{month_field}_field') + value)
        setattr(budget_validated, f'modified_{month_field}_field', getattr(budget_validated, f'modified_{month_field}_field') + value)
        setattr(budget_validated, f'increases_{month_field}_field', getattr(budget_validated, f'increases_{month_field}_field') + value)
    
    def _update_reduccion_fields(self, budget_validated, month_field, value):
        to_collect_field = getattr(budget_validated, f'to_collect_{month_field}_field')
        if to_collect_field >= value:
            setattr(budget_validated, f'to_collect_{month_field}_field', getattr(budget_validated, f'to_collect_{month_field}_field') - value)
            setattr(budget_validated, f'modified_{month_field}_field', getattr(budget_validated, f'modified_{month_field}_field') - value)
            setattr(budget_validated, f'decreases_{month_field}_field', getattr(budget_validated, f'decreases_{month_field}_field') + value)
    
    def _update_total_fields(self, budget_validated, month_fields):
        total_to_collect_sum = sum(getattr(budget_validated, f'to_collect_{month}_field') for month in month_fields)
        total_modified_sum = sum(getattr(budget_validated, f'modified_{month}_field') for month in month_fields)
        total_increases_sum = sum(getattr(budget_validated, f'increases_{month}_field') for month in month_fields)
        total_decreases_sum = sum(getattr(budget_validated, f'decreases_{month}_field') for month in month_fields)

        budget_validated.write({'to_collect_total': total_to_collect_sum,
                                'modified_total': total_modified_sum,
                                'increases_total': total_increases_sum,
                                'decreases_total': total_decreases_sum})

    def unlink(self):
        ########################################################
        # Verificar si algún registro está en estado 'approve' #
        ########################################################
        approve_records = self.filtered(lambda record: record.state == 'approve')

        if approve_records:
            raise ValidationError(_("No es posible realizar la eliminación de las modificaciones al subsidio que está seleccionando ya que se encuentra en estado Aprobado!!!"))
        res = super(ModificationsToTheFederalSubsidy, self - approve_records).unlink()
        return res



class ModificationsToTheFederalSubsidyLines(models.Model):
    _name = 'modifications.federal.subsidy.lines'
    _description = 'Lines of Modifications to the Federal Subsidy'

    modifications_id = fields.Many2one('modifications.federal.subsidy', string='Modifications to the Federal Subsidy', ondelete="cascade", index=True)
    display_type = fields.Selection([('line_section', "Section"), ('line_note', "Note")], default=False, help="Technical field for UX purpose.", store=False)
    sequence = fields.Integer(string = 'Sequence')

    folio = fields.Char(string="Folio", compute="_compute_folio")
    cycle = fields.Many2one('account.fiscal.year', string='Cycle')
    branch_ord = fields.Integer(string="Branch ORD")
    ur_ord = fields.Integer(string="UR ORD")
    request_number = fields.Integer(string="Request Number")
    type_request = fields.Char(string="Type Request")
    quarter = fields.Char(string="Quarter")
    type_movement = fields.Char(string="Type Movement")
    date_application = fields.Char(string="Date Application")
    branch = fields.Integer(string="Branch")
    unit = fields.Char(string="Unit")
    purpose = fields.Integer(string="Purpose")
    function_int = fields.Integer(string="Function")
    sub_function = fields.Integer(string="Subfunction")
    program = fields.Char(string="Program")
    institution_activity = fields.Integer(string="Institution Activity")
    project_identifier = fields.Char(string="Project Identifier")
    project = fields.Char(string="Project")
    federal_item = fields.Many2one('federal.departure.finance', string="Federal Item")
    expense_type = fields.Integer(string="Expense Type")
    funding_source = fields.Integer(string="Funding Source")
    federative_entity = fields.Integer(string="Fedrative Entity")
    key_wallet = fields.Char(strng="Key Wallet")
    transaction = fields.Char(string="Transaction")
    january = fields.Float(string = "January", default=0.0)
    february = fields.Float(string = "February", default=0.0)
    march = fields.Float(string = "March", default=0.0)
    april = fields.Float(string = "April", default=0.0)
    may = fields.Float(string = "May", default=0.0)
    june = fields.Float(string = "June", default=0.0)
    july = fields.Float(string = "July", default=0.0)
    august = fields.Float(string = "August", default=0.0)
    september = fields.Float(string = "September", default=0.0)
    october = fields.Float(string="October", default=0.0)
    november = fields.Float(string="November", default=0.0)
    december = fields.Float(string="December", default=0.0)
    type_transaction = fields.Selection([('cleared_adequacy', 'Cleared Adequacy'), ('liquid_adequacy', 'Liquid Adequacy'), ('recalendarization', 'Recalendarization')], default=False, string="Type Transaction")   

    load_type = fields.Selection([('manual', 'Manual'), ('automatic', 'Automatic')], default='automatic', string="Load Type")

    state_related = fields.Selection(related='modifications_id.state', string='State Related', store=True)

    ##########################################################
    # function to generate the folio by concatenating cycle, # 
    # branch ord, ur ord and request number                  #
    ##########################################################
    def _compute_folio(self):
        for record in self:
            record.folio = f"{record.cycle.name}{record.branch_ord}{record.ur_ord}{record.request_number}"
