# -*- coding: utf-8 -*-
# odoo
from odoo import models, fields, api, _
# imports from modules

STATES = [
    ('draft', 'BORRADOR'),
    ('registered','REGISTRADA'),
]

class HandbooksBIM(models.Model):
    _name = 'siif.check.handbooks'
    _description = "Manuales/tutoriales"

    state = fields.Selection(STATES, default=STATES[0][0], string='Estado del registro', readonly=True)
    name = fields.Char(string="Nombre de directorio")
    service_table = fields.Text(string="Mesa de servicios", help="Ponga el link de video")
    handbooks = fields.Many2many("ir.attachment", string="Añadir archivos")
    diagrams = fields.Many2many(comodel_name="ir.attachment", 
        relation="siif_check_handbooks_diagrams_rel", 
        column1="diagram_id",
        column2="attachment_id",
        string="Añadir archivos")

    documents = fields.Many2many(comodel_name="ir.attachment", 
        relation="siif_check_handbooks_documents_rel", 
        column1="document_id",
        column2="attachment_id",
        string="Añadir archivos")
    video_ids = fields.One2many(comodel_name = 'siif.check.video', string="Videos", inverse_name="handbook_siif_id", ondelete = "cascade"  )
    comments = fields.Text(string="Comentarios")

    
    @api.model
    def create(self, vals):
        vals['state'] =  'registered'
        result = super(HandbooksBIM, self).create(vals)
        return result

class HandbookVideoBASE(models.Model):
    # Private attributes
    _name="siif.check.video"
    name = fields.Char(String="Alias", required=True )
    video = fields.Text(string="Video", help="Ponga el link de video", required=True)
    handbook_siif_id = fields.Many2one( comodel_name= 'siif.check.handbooks',string='Manuales/tutoriales', ondelete = "cascade")