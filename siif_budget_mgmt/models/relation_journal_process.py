# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
import logging

# Define constant
RELATION_MODULES_PROCESS_MODULE = 'siif.budget.mgmt.relation.modules.process'

class RelationJournalProcess(models.Model):
    _name = 'siif.budget.mgmt.relation.journal.process'
    _description = 'Relación Diario - Proceso'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'name'
    _order = "name asc"

    name = fields.Char(string='Nombre', tracking=True, store=True, readonly=True)
    module_name = fields.Many2one('siif.budget.mgmt.relation.modules', string='Modulo', tracking=True, required=True)
    module_description = fields.Char(string="Descripción del modulo", store=True, compute="_compute_module_description")
    process_name = fields.Many2one(RELATION_MODULES_PROCESS_MODULE, string='Proceso', tracking=True, required=True)#, domain="[('module_name', '=', module_name)]"
    process_short_name = fields.Char(string="Nom corto proceso", store=True, compute="_compute_process_description")
    process_description = fields.Char(string="Descripción del proceso", store=True, compute="_compute_process_description")
    journal_name = fields.Many2one('account.journal', string="Diario", tracking=True, required=True)
    journal_description = fields.Char(string="Descripción del diario", store=True)
    accounting_affectation = fields.Boolean(string="Afec contable", tracking=True, default=False)
    expenditure_budget_affectation = fields.Boolean(string="Afec presup egresos", tracking=True, default=False)
    expenditure_budget_accured = fields.Boolean(string="Devengado")
    expenditure_budget_paid = fields.Boolean(string="Pagado")
    income_budget_affectation = fields.Boolean(string="Afec presup ingresos", tracking=True, default=False)
    income_budget_accured = fields.Boolean(string="Devengado")
    income_budget_collected = fields.Boolean(string="Recaudado")
    date_from = fields.Date(string="Fecha inicio vigencia", tracking=True)
    date_to = fields.Date(string="Fecha fin vigencia", tracking=True)
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('confirmed', 'Confirmado'),
    ],default='draft', required=True, string='Estado', tracking=True)

    # Folio Relacion
    @api.model
    def create(self, vals):
        res = super(RelationJournalProcess, self).create(vals)
        seq = self.env['ir.sequence'].next_by_code('seq.siif.budget.mgmt.relation.journal.process')
        res.name = res.module_name.name + "/" + str(seq)
        return res

    # Mostrar los procesos del modulo seleccionado
    @api.onchange('module_name')
    def onchange_module_name(self):
        # Limpiar campos
        self.process_name = False
        self.journal_name = False
        self.accounting_affectation = False
        self.budget_affectation = False
        self.income_budget_accurred = False
        self.income_budget_collected = False
        values = {}
        if self.module_name:
            process = self.env[RELATION_MODULES_PROCESS_MODULE].search([('module_name', '=', self.module_name.id)])
            if not process:
                warning = {
                    'title': _('No hay procesos'),
                   'message': _(f'No hay procesos registrados en el modulo {self.module_name.name}'),
                }
                values.update({'warning': warning, 'domain': {'process_name': [('id', 'in', process.ids)]}})
            else:
                values.update({'domain': {'process_name': [('id', 'in', process.ids)]}})
        if not self.module_name:
            process = self.env[RELATION_MODULES_PROCESS_MODULE].search([('module_name', '=', 'null')])
            values.update({'domain': {'process_name': [('id', 'in', process.ids)]}})
        return values

    @api.depends('process_name', 'journal_name')
    def _compute_name(self):
        for record in self:
            computed_name = ''
            if record.process_name:
                computed_name += str(record.process_name.name)
            record.name = computed_name

    # Se muestra la descripción de los campos seleccionados
    @api.onchange('process_name')
    def _get_process_description(self):
        self.process_description = self.process_name.description
        self.process_short_name = self.process_name.short_name_process
    @api.onchange('module_name')
    def _get_module_description(self):
        self.module_description = self.module_name.description

    # Guardar la descripción del modulo
    @api.depends('module_name')
    def _compute_module_description(self):
        for record in self:
            desc = ''
            if record.module_name:
                desc += str(record.module_name.description)
            record.module_description = desc

    # Guardar la descripción del proceso y el nombre corto
    @api.depends('process_name')
    def _compute_process_description(self):
        for record in self:
            desc = ''
            short = ''
            if record.process_name:
                desc += str(record.process_name.description)
                short += str(record.process_name.short_name_process)
            record.process_description = desc
            record.process_short_name = short
    
    # Pasar a estado confirmado
    def action_confirmed(self):
        self.ensure_one()
        self.state = 'confirmed'

    _sql_constraints = [('name', 'unique(name)', ('El nombre de la relación diario - proceso debe ser único'))]