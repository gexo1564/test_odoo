# -*- coding: utf-8 -*-
import json
import logging
from odoo import models, fields, api, _
from datetime import datetime
class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    previous_number = fields.Many2one(related='move_id.previous_number', store=False)
    document_type = fields.Selection(related='move_id.document_type', store=False)

    domain_account_to_pay_invoice = fields.Char(compute="_compute_domain_account_to_pay_invoice", store=False)

    domain_program_code = fields.Char(compute="_compute_domain_origin_resource", store=False)
    domain_account_id = fields.Char(compute="_compute_domain_origin_resource", store=False)
    domain_account_ie = fields.Char(compute="_compute_domain_origin_resource", store=False)

    egress_key = fields.Char(related='egress_key_id.key', store=False)
    domain_egress_key_id = fields.Char(compute="_compute_domain_egress_key_id", store=False)
    operation_type_id = fields.Many2one('operation.type', related='move_id.operation_type_id', store=False)
    move_dependency_id = fields.Many2one('dependency', related='move_id.dependancy_id', store=False)
    move_sub_dependency_id = fields.Many2one('sub.dependency', related='move_id.sub_dependancy_id', store=False)

    account_ie = fields.Many2one('association.distribution.ie.accounts', string='Account IE')
    account_move_returns_id = fields.Many2one('account.returns')

    exchange_rate_line = fields.Monetary(string='Exchange Rate Line', compute='_compute_exchange_rate')
    
    @api.onchange('currency_id')
    def _compute_exchange_rate(self):
        for rec in self:
            if rec.currency_id:
                rec.exchange_rate_line=(1/rec.currency_id.rate)
            else:
                rec.exchange_rate_line=1


    @api.depends('egress_key_id')
    def _compute_domain_account_to_pay_invoice(self):
        for record in self:
            dep = record.move_id.dependancy_id
            sd = record.move_id.sub_dependancy_id
            if record.egress_key_id and record.egress_key in ('CPN', 'CPE'):
                domain = []
                domain.append(('state', '=', 'approved'))
                domain.append(('previous_number', '!=', ''))
                domain.append(('available', '>', 0))
                domain.append(('document_type', '=', 'national' if record.egress_key_id.key == 'CPN' else 'foreign'))
                domain.append(('invoice_line_ids.dependency_id', '=', dep.id))
                domain.append(('invoice_line_ids.sub_dependency_id', '=', sd.id))
                record.domain_account_to_pay_invoice = json.dumps(domain)
            else:
                record.domain_account_to_pay_invoice = json.dumps([('id', '=', False)])

    @api.depends('operation_type_id', 'document_type', 'previous_number')
    def _compute_domain_egress_key_id(self):
        for record in self:
            domain = []
            if record.move_id and record.move_id.type != 'in_invoice':
                record.domain_egress_key_id = json.dumps([])
                continue
            if record.operation_type_id == self.env['payment.parameters'].get_param('ministration_op_type'):
                if record.previous_number:
                    domain.append('CPN' if record.document_type == 'national' else 'CPE')
                else:
                    domain.append(('id', '=', False))
            else:
                domain = ['PA', 'CC']
                if record.document_type == 'national':
                    if record.previous_number:
                        domain.append('CPN')
                    domain.append('IEMN')
                elif record.document_type == 'foreign':
                    if record.previous_number:
                        domain.append('CPE')
                    domain.append('IEME')
            record.domain_egress_key_id = json.dumps([('key', 'in', domain)])

    @api.onchange('previous_number')
    def _onchange_previous_number(self):
        for record in self:
            record.previous_number = record.move_id.previous_number

    @api.onchange('operation_type_id')
    def _onchange_operation_type_id(self):
        for record in self:
            record.operation_type_id = record.move_id.operation_type_id

    @api.onchange('document_type')
    def _onchange_document_type(self):
        for record in self:
            record.document_type = record.move_id.document_type

    @api.depends('egress_key_id', 'previous_number', 'operation_type_id', 'move_dependency_id', 'move_sub_dependency_id', 'program_code_id')
    def _compute_domain_origin_resource(self):
        for record in self:
            # Se valida que ya se haya seleccionado la clave de egreso.
            if not record.egress_key_id:
                record.domain_program_code = json.dumps([('id', '=', False)])
                record.domain_account_id = json.dumps([('id', '=', False)])
                record.domain_account_ie = json.dumps([('id', '=', False)])
                continue
            # Para cuentas por pagar, se muestran todos códigos programáticos registrados en la CXP.
            if record.egress_key_id.key in ('CPN', 'CPE'):
                if not record.previous_number:
                    record.domain_program_code = json.dumps([('id', '=', False)])
                    record.domain_account_id = json.dumps([('id', '=', False)])
                    record.domain_account_ie = json.dumps([('id', '=', False)])
                    continue
                # Dominio del código programático
                lines = record.previous_number.invoice_line_ids.filtered(lambda x:x.program_code_id)
                if lines and not record.program_code_id:
                    record.program_code_id = lines[0].program_code_id
                program_code = [('id', 'in', [l.program_code_id.id for l in lines])]
                record.domain_program_code = json.dumps(program_code)
                # Dominio de las cuentas de ie
                if record.program_code_id and record.program_code_id.resource_origin_id.key_origin == '01':
                    lines = record.previous_number.invoice_line_ids.filtered(lambda x:x.account_ie)
                    account_ie = [('id', 'in', [l.account_ie.id for l in lines])]
                    record.domain_account_ie = json.dumps(account_ie)
                else:
                    record.domain_account_ie = json.dumps([('id', '=', False)])
                continue
            # Para el resto de claves de egreso, se valida que la cabecera tenga
            # el tipo de operacion (para validar las partidas), la dependencia y la subdependencia.
            if not (record.operation_type_id and record.move_dependency_id and record.move_sub_dependency_id):
                record.domain_program_code = json.dumps([('id', '=', False)])
                record.domain_account_id = json.dumps([('id', '=', False)])
                record.domain_account_ie = json.dumps([('id', '=', False)])
                continue
            program_code = [('budget_id.state','=','validate')]
            program_code.append(('year.name', '=', str(datetime.now().year)))
            if record.egress_key_id.key == 'PA':
                program_code.append(('resource_origin_id.key_origin', '=', '00'))
                record.domain_account_id = json.dumps([('id', '=', False)])
                record.domain_account_ie = json.dumps([('id', '=', False)])
            elif record.egress_key_id.key in ('IEMN', 'IEME'):
                program_code.append(('resource_origin_id.key_origin', '=', '01'))
                record.domain_account_id = json.dumps([('id', '=', False)])
                record.domain_account_ie = json.dumps([])
            else:
                record.domain_program_code = json.dumps([('id', '=', False)])
                record.domain_account_id = json.dumps([])
                record.domain_account_ie = json.dumps([('id', '=', False)])
                continue
            # Filtro de la DEP/SD de la cabecera.
            program_code.append(('sub_dependency_id', '=', record.move_sub_dependency_id.id))
            program_code.append(('dependency_id', '=', record.move_dependency_id.id))
            # Filtro de las partidas según el tipo de operación.
            items = []
            if record.operation_type_id.specific_items_allowed_ids:
                items += record.operation_type_id.specific_items_allowed_ids.ids
            if record.operation_type_id.item_groups_allowed:
                item_groups = record.operation_type_id.item_groups_allowed.split(",")
                query = """
                    select id from expenditure_item where substring(item,1,1) in %s
                """
                self._cr.execute(query, (tuple([i[0] for i in item_groups]),))
                res = self.env.cr.fetchall()
                items += [r[0] for r in res]
            if items:
                program_code.append(('item_id', 'in', items))
            record.domain_program_code = json.dumps(program_code)

    @api.onchange('egress_key_id')
    def onchange_egress_key_id(self):
        self.program_code_id = None
        self.account_id = None
        self.account_ie = None
        self.dependency_id = self.sub_dependency_id = None

    @api.onchange('program_code_id')
    def onchange_program_code_id(self):
        if self.program_code_id and self.previous_number:
            self.account_id = self.previous_number.type_provision.account_for_balance_on_account_payable_id
            self.dependency_id = self.sub_dependency_id = None
            self.account_ie = None
