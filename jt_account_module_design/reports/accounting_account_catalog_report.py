# -*- coding: utf-8 -*-
##############################################################################
#
#    Author: UNAM
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import io
import base64
import lxml.html
from datetime import datetime

from odoo import models, api, _
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.tools import config, date_utils, get_lang
from odoo.tools.misc import formatLang
from odoo.tools.misc import xlsxwriter
from odoo.exceptions import UserError, ValidationError, UserError

class AccountingAccountCatalogReport(models.AbstractModel):
    _name = "accounting.account.catalog.report"
    _inherit = "account.coa.report"
    _description = "Accounting Account Catalog Report"

    filter_date = None
    filter_comparison = None
    filter_all_entries = None
    filter_journals = None
    filter_analytic = None
    filter_unfold_all = True
    filter_cash_basis = None
    filter_hierarchy = None
    filter_unposted_in_period = None
    MAX_LINES = None
    
    def _get_reports_buttons(self):
        return [
            {'name': _('Export to PDF'),
             'sequence': 1, 'action': 'print_pdf',
             'file_export_type': _('PDF'),
             },
            {'name': _('Export (XLSX)'),
             'sequence': 2,
             'action': 'print_xlsx',
             'file_export_type': _('XLSX')
             },
        ]

    def _get_templates(self):
        templates = super(
            AccountingAccountCatalogReport, self)._get_templates()
        templates[
            'main_table_header_template'] = 'account_reports.main_table_header'
        templates['main_template'] = 'account_reports.main_template'
        return templates

    def _get_columns_name(self, options):
        account_model_fields = self.env['account.account'].fields_get()
        columns = []
        for field in options['showing_fields']:
            columns.append({'name': (account_model_fields[field]['string'])})
        return columns

    def _get_lines(self, options, line_id=None):
        lines = []
        # Empty columns should be added for format purposes
        empty_columns = []
        for _ in range(len(options['showing_fields']) - 2):
            empty_columns.append({'name': ''})
        account_groups = self.env['account.group'].search([], order
                                                          = 'code_prefix asc')
        for group in account_groups:
            if not group.parent_id.id:
                parent_id = None
            else:
                parent_id = 'account_group_' + str(group.parent_id.id)
            lines.append({
                'id': 'account_group_' + str(group.id),
                'name': group.code_prefix,
                'level': group.parent_path.count('/'),
                'columns' : [{'name': group.name}] + empty_columns,
                'unfoldable': True,
                'unfolded': options['unfold_all'],
                'parent_id': parent_id
            })

        accounts = self.env['account.account'].search(
            [('group_id', '!=', None)]
        )
        for account in accounts:
            columns = []
            for field in options['showing_fields'][1:]:
                if isinstance(account[field], models.Model):
                    if not account[field]:
                        columns.append({'name': '-'})
                    else:
                        if field == 'coa_conac_id':
                            columns.append({'name': str(account[field].code)})
                        elif field == 'group_id':
                            columns.append(
                                {'name': str(account[field].code_prefix) +
                                 ' - ' + str(account[field].name)})
                        elif field == 'cog_conac_id':
                            columns.append(
                                {'name': str(account[field].heading) + ' - ' +
                                 str(account[field].name)})
                        elif field == 'revenue_recognition_account_id':
                            columns.append({'name': str(account[field].code)})
                        else:
                            columns.append({'name': str(account[field].name)})
                elif isinstance(account[field], bool):
                    if account[field]:
                        columns.append({'name': 'Si'})
                    elif field == 'description_revenue_recognition':
                        columns.append({'name': '-'})
                    else:
                        columns.append({'name': 'No'})
                else:
                    columns.append({'name': str(account[field])})
            lines.append({
                    'id': 'account_' + str(account.id),
                    'name': account.code,
                    'level': 6,
                    'columns' : columns,
                    'unfoldable': False,
                    'parent_id': 'account_group_' + str(account.group_id.id)
                })
        lines.sort(key = lambda line: line['name'])
        return lines

    def _get_report_name(self):
        return _("Accounting Account Catalog Report")

    def get_xlsx(self, options, response=None):
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        sheet = workbook.add_worksheet(self._get_report_name()[:31])
        
        title_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'bottom': 2})
        super_col_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'align': 'center'})
        currect_date_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'align': 'right'})
        currect_date_style.set_border(0)
        super_col_style.set_border(0)
        #Set the first column width to 50
        sheet.set_column(0, 0,40)
        sheet.set_column(0, 1,40)
        y_offset = 0
        col = 0
        
        sheet.merge_range(y_offset, col, 6, col, '',super_col_style)
        if self.env.user and self.env.user.company_id and self.env.user.company_id.header_logo:
            filename = 'logo.png'
            image_data = io.BytesIO(base64.standard_b64decode(self.env.user.company_id.header_logo))
            sheet.insert_image(0,0, filename, {'image_data': image_data,'x_offset':130,'y_offset':15,'x_scale':0.6,'y_scale':0.6})
        
        col += 1

        x_offset = 1

        currect_time_msg = "Fecha y hora de impresión: "
        currect_time_msg += datetime.today().strftime('%d/%m/%Y %H:%M')
        user_msg = " Reporte generado por: " + self.env.user.name
        sheet.merge_range(y_offset, x_offset, y_offset, x_offset+6, '', super_col_style)
        sheet.merge_range(y_offset+1, x_offset, y_offset+1, x_offset+6, 'UNIVERSIDAD NACIONAL AUTÓNOMA DE MÉXICO', super_col_style)
        sheet.merge_range(y_offset+2, x_offset, y_offset+2, x_offset+6, 'PATRONATO UNIVERSITARIO', super_col_style)
        sheet.merge_range(y_offset+3, x_offset, y_offset+3, x_offset+6, 'TESORERÍA', super_col_style)
        sheet.merge_range(y_offset+4, x_offset, y_offset+4, x_offset+6, self._get_report_name().upper(), super_col_style)
        sheet.merge_range(y_offset+5, x_offset, y_offset+5, x_offset+6, currect_time_msg, super_col_style)
        sheet.merge_range(y_offset+6, x_offset, y_offset+6, x_offset+6, user_msg, super_col_style)
        y_offset += 8
        for row in self.get_header(options):
            x = 0
            for column in row:
                colspan = column.get('colspan', 1)
                header_label = column.get('name', '').replace('<br/>', ' ').replace('&nbsp;', ' ')
                if colspan == 1:
                    sheet.write(y_offset, x, header_label, title_style)
                else:
                    sheet.merge_range(y_offset, x, y_offset, x + colspan - 1, header_label, title_style)
                x += colspan
            y_offset += 1
        ctx = self._set_context(options)
        ctx.update({'no_format':True, 'print_mode':True, 'prefetch_fields': False})
        # deactivating the prefetching saves ~35% on get_lines running time
        lines = self.with_context(ctx)._get_lines(options)

 
        if options.get('hierarchy'):
            lines = self._create_hierarchy(lines, options)
        if options.get('selected_column'):
            lines = self._sort_lines(lines, options)
 
        self._write_xlsx(lines, workbook, sheet, y_offset)
 
        workbook.close()
        output.seek(0)
        generated_file = output.read()
        output.close()
        return generated_file    

    def get_pdf(self, options, minimal_layout=False):
        # As the assets are generated during the same transaction as the rendering of the
        # templates calling them, there is a scenario where the assets are unreachable: when
        # you make a request to read the assets while the transaction creating them is not done.
        # Indeed, when you make an asset request, the controller has to read the `ir.attachment`
        # table.
        # This scenario happens when you want to print a PDF report for the first time, as the
        # assets are not in cache and must be generated. To workaround this issue, we manually
        # commit the writes in the `ir.attachment` table. It is done thanks to a key in the context.

        # fiels used to generate the report
        obj_ir_actions_report = self.env['ir.actions.report']
        str_web_min_lo = "web.minimal_layout"
        currect_time_msg = datetime.today().strftime('%d/%m/%Y %H:%M')
        header_user = self.env.user.name
        header_date = str(currect_time_msg)
        spec_paperformat_args = {}

        if not config['test_enable']:
            self = self.with_context(commit_assetsbundle=True)

        base_url = self.env['ir.config_parameter'].sudo().get_param(
            'report.url'
        ) or self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        rcontext = {
            'mode': 'print',
            'base_url': base_url,
            'company': self.env.company,
            'css': '',
            'o': self.env.user,
            'res_company': self.env.company,
            'header_date' : header_date,
            'header_user' : header_user,
        }

        body = self.env['ir.ui.view'].render_template(
            "account_reports.print_template",
            values=dict(rcontext),
        )
        body_html = self.with_context(print_mode=True).get_html(options)
        header = obj_ir_actions_report.render_template("jt_account_module_design.external_layout_accounting_account_catalog_report",
                values=rcontext)
        # Ensure that headers and footer are correctly encoded
        header = header.decode('utf-8')
        headers = header.encode()
        
        footer = obj_ir_actions_report.render_template("jt_account_module_design.external_layout_accounting_account_catalog_report_footer",
                values=rcontext)
        # Ensure that footers are correctly encoded
        footer = footer.decode('utf-8')
        footers = footer.encode()
        try:
            root = lxml.html.fromstring(header)
            match_klass = "//div[contains(concat(' ', normalize-space(@class), ' '), ' {} ')]"
            for node in root.xpath(match_klass.format('header')):
                headers = lxml.html.tostring(node)
                headers = obj_ir_actions_report.render_template(str_web_min_lo, values=dict(rcontext, subst=True, body=headers))
            root = lxml.html.fromstring(footer)
            for node in root.xpath(match_klass.format('footer')):
                footers = lxml.html.tostring(node)
                footers = obj_ir_actions_report.render_template(str_web_min_lo, values=dict(rcontext, subst=True, body=footers))
        except lxml.etree.XMLSyntaxError:
            headers = header.encode()
            footer = footer.encode()
        header = headers
        footer = footers

        body_html = footer + body_html
        body = body.replace(b'<body class="o_account_reports_body_print">',
                            b'<body class="o_account_reports_body_print">' +
                            body_html)

        landscape = True
        report = obj_ir_actions_report.search([('model','=','accounting.account.catalog.report')])
        if not report:
            raise UserError(_('No report found for this template!'))
        return report._run_wkhtmltopdf(
            [body],
            header=header, footer=footer,
            landscape=landscape,
            specific_paperformat_args=spec_paperformat_args
        )

    def get_html(self, options, line_id=None, additional_context=None):
        '''
        return the html value of report, or html value of unfolded line
        * if line_id is set, the template used will be the line_template
        otherwise it uses the main_template. Reason is for efficiency, when unfolding a line in the report
        we don't want to reload all lines, just get the one we unfolded.
        '''
		 # Groups that have access to the report
        list_groups = [
            'jt_account_base.group_account_admin_user',
            'jt_account_base.group_account_super_admin_user',
            'jt_account_base.group_account_guest_user',
            'jt_account_base.group_account_approval_user',
            'jt_account_base.group_account_register_user']
        
        self.env['account.module.security'].check_user_permissions(list_groups)
        
        # Check the security before updating the context to make sure the options are safe.
        self._check_report_security(options)

        # Prevent inconsistency between options and context.
        self = self.with_context(self._set_context(options))

        templates = self._get_templates()
        report_manager = self._get_report_manager(options)
        report = {}
        showing_fields = [
            'code',
            'name',
            'user_type_id',
            'tax_ids',
            'tag_ids',
            'group_id',
            'dep_subdep_flag',
            'is_payment_account',
            'is_income_account',
            'revenue_recognition_account_id',
            'description_revenue_recognition',
            'currency_id',
            'reconcile',
            'deprecated',
            'cog_conac_id',
            'coa_conac_id',
            'conac_name',
        ]
        options['showing_fields'] = showing_fields
        lines = self._get_lines(options, line_id=line_id)

        if options.get('hierarchy'):
            lines = self._create_hierarchy(lines, options)
        if options.get('selected_column'):
            lines = self._sort_lines(lines, options)

        footnotes_to_render = []
        if self.env.context.get('print_mode', False):
            # we are in print mode, so compute footnote number and include them
            # in lines values, otherwise, let the js compute the number
            # correctly as we don't know all the visible lines.
            footnotes = dict([(str(f.line), f) for f in report_manager.footnotes_ids])
            number = 0
            for line in lines:
                f = footnotes.get(str(line.get('id')))
                if f:
                    number += 1
                    line['footnote'] = str(number)
                    footnotes_to_render.append({'id': f.id, 'number': number, 'text': f.text})

        rcontext = {'report': report,
                    'lines': {'columns_header': self.get_header(options), 'lines': lines},
                    'options': {},
                    'context': self.env.context,
                    'model': self,
                }
        if additional_context and type(additional_context) == dict:
            rcontext.update(additional_context)
        if self.env.context.get('analytic_account_ids'):
            rcontext['options']['analytic_account_ids'] = [
                {'id': acc.id, 'name': acc.name} for acc in self.env.context['analytic_account_ids']
            ]

        render_template = templates.get('main_template', 'account_reports.main_template')
        if line_id is not None:
            render_template = templates.get('line_template', 'account_reports.line_template')
        html = self.env['ir.ui.view'].render_template(
            render_template,
            values=dict(rcontext),
        )
        if self.env.context.get('print_mode', False):
            for k,v in self._replace_class().items():
                html = html.replace(k, v)
            # append footnote as well
            html = html.replace(b'<div class="js_account_report_footnotes"></div>', self.get_html_footnotes(footnotes_to_render))
        return html
    
        
    def _write_xlsx(self, lines, workbook, sheet, y_offset):
        date_default_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2, 'num_format': 'yyyy-mm-dd'})
        date_default_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': 'yyyy-mm-dd'})
        default_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2})
        default_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'})
        level_0_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 13, 'bottom': 6, 'font_color': '#666666'})
        level_1_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 13, 'bottom': 1, 'font_color': '#666666'})
        level_2_col1_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 1})
        level_2_col1_total_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666'})
        level_2_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666'})
        level_3_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2})
        level_3_col1_total_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 1})
        level_3_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'})
        level_4_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 3})
        level_4_col1_total_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 2})
        level_4_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'})
        level_5_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 4})
        level_5_col1_total_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 3})
        level_5_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'})
        level_6_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 5})
        level_6_col1_total_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 4})
        level_6_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'})


        style_map = {
        0: (level_0_style, level_0_style, default_style),
        1: (level_1_style, level_1_style, default_style),
        2: (level_2_style, level_2_col1_style, level_2_col1_total_style),
        3: (level_3_style, level_3_col1_style, level_3_col1_total_style),
        4: (level_4_style, level_4_col1_style, level_4_col1_total_style),
        5: (level_5_style, level_5_col1_style, level_5_col1_total_style),
        6: (level_6_style, level_6_col1_style, level_6_col1_total_style),
            }
        

        for y in range(0, len(lines)):
            level = lines[y].get('level')
            style, col1_style, col1_total_style = style_map.get(level, (default_style, default_col1_style, default_col1_style))

            if lines[y].get('caret_options'):
                style = level_3_style
                col1_style = level_3_col1_style
            elif 'total' in lines[y].get('class', '').split(' '):
                col1_style = col1_total_style

            if level == 0:
                y_offset += 1

            # write the first column, with a specific style to manage the indentation
            cell_type, cell_value = self._get_cell_type_value(lines[y])
            if cell_type == 'date':
                sheet.write_datetime(y + y_offset, 0, cell_value, date_default_col1_style)
            else:
                sheet.write(y + y_offset, 0, cell_value, col1_style)

            # write all the remaining cells
            for x in range(1, len(lines[y]['columns']) + 1):
                cell_type, cell_value = self._get_cell_type_value(lines[y]['columns'][x - 1])
                if cell_type == 'date':
                    sheet.write_datetime(y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, date_default_style)
                else:
                    sheet.write(y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, style)

