from odoo import models, fields, api,_
from datetime import datetime, timedelta
from odoo.exceptions import ValidationError
from lxml import etree

class ComissionConcept(models.Model):

    _name = 'comission.concept'
    _description = "Comission concept"
    _rec_name = 'concept'

    concept = fields.Char('Concept')
    active =  fields.Boolean('Active')
    concept_direction =  fields.Selection([('egress', 'Egress'), ('income', 'Income')])

    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        res = super().fields_view_get(view_id=view_id,view_type=view_type,toolbar=toolbar,submenu=submenu)
        doc = etree.XML(res['arch'])
        is_group_approval_user = self.env.user.has_group('jt_payroll_payment.group_approval_user')
        is_group_consult_user = self.env.user.has_group('jt_payroll_payment.group_consult_user')

        if is_group_approval_user or is_group_consult_user:
            for node in doc.xpath("//" + view_type):
                node.set('create','0')
                node.set('delete','0')
                node.set('export','0')
                node.set('export_xlsx','0')

        if is_group_consult_user:
            for node in doc.xpath("//" + view_type):
                node.set('edit','0')

        res['arch'] = etree.tostring(doc)
        return res