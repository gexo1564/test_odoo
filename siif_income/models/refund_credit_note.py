# -*- coding: utf-8 -*-
import logging
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
from datetime import datetime
from lxml import etree

ACCOUNT_MOVE_MODEL = 'account.move'
ACCOUNT_MOVE_LINE_MODEL = 'account.move.line'
REASON_FOR_REJECTION_TEXT = 'Reason for rejection'
class RefundCreditNote(models.Model):
    _name = 'refund.credit.note'
    _description = 'Solicitud de devolución por Nota de Crédito'
    _inherit = ['mail.thread', 'mail.activity.mixin']


    name = fields.Char("Name", required=True, default="")
    state = fields.Selection(
        [   ('canceled', 'Canceled'),
            ('draft', 'Draft'),
            ('rejected', 'Rejected'),
            ('posted', 'Posted'),
            ('accepted', 'Accepted'),
            ('rejected_bank', 'Rejected Bank'),
            ('paid', 'Paid'),
        ], default='draft', track_visibility="always",
    )
    payment_method = fields.Selection(
        [
            ('transfer', 'Transfer'),
            ('check', 'Bank Check'),
        ], "Payment Method", default='',
    )
    customer_ids = fields.Many2many(comodel_name="res.partner",  string="Customers", domain=[
                                    ('contact_type', '=', 'client')])
    credit_note_ids = fields.Many2many(
        ACCOUNT_MOVE_MODEL,
        'account_move_refund_credit_note_rel',
        'refund_credit_note_id',
        'account_move_id',
        string="Credit Note",
        domain=[('type', '=', 'entry'),
                ('type_of_registry_payment', '=', 'credit_note')])
    bank = fields.Many2one('res.bank')
    bank_account = fields.Char('Bank Account')
    key_bank = fields.Char('Key Bank')

    line_ids = fields.One2many(
        ACCOUNT_MOVE_LINE_MODEL, 'refund_credit_note_id', string="Accounting Notes")
    documentary_support = fields.Many2many(
        "ir.attachment", string="Add documentary support")
    payment_voucher = fields.Binary('Payment Voucher')
    reject_reason = fields.Text(REASON_FOR_REJECTION_TEXT)
    bank_journal_id = fields.Many2one('account.journal', string="Credit Bank")
    line_ids = fields.One2many(
        ACCOUNT_MOVE_LINE_MODEL, 'refund_credit_note_id', string="Accounting Notes")
    rejected = fields.Boolean(string='Rejected', help='When the information that has been previously registered is incorrect.')
    rejected_bank = fields.Boolean(string='Rejected Bank', help='When the payment cannot be executed by the bank.')

    
    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        res = super().fields_view_get(view_id=view_id,
                                      view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])

        is_group_income_sdnc_administrator_user = self.env.user.has_group('jt_income.group_income_sdnc_administrator_user')
        is_group_income_sdnc_finance_user = self.env.user.has_group('jt_income.group_income_sdnc_finance_user')
        is_group_income_sdnc_accounting_user = self.env.user.has_group('jt_income.group_income_sdnc_accounting_user')
        
        if is_group_income_sdnc_administrator_user:
            for node in doc.xpath("//" + view_type):
                node.set('create', '0')
                node.set('export','0')

        elif is_group_income_sdnc_finance_user or is_group_income_sdnc_accounting_user:
            for node in doc.xpath("//" + view_type):
                node.set('create', '0')
                node.set('export','0')
                node.set('delete','0')
                     
        res['arch'] = etree.tostring(doc)

        return res    


    def update_state(self, new_state):
        for rec in self:
            rec.state = new_state


    def update_credit_note_state(self, new_state):
        for record in self.credit_note_ids:
            record.credit_note_payment_state = new_state        


    def post(self):
        if not self.documentary_support:
            raise UserError(_('Add supporting documentation to continue.'))
        self.reject_reason = None
        self.rejected = False
        self.rejected_bank = False

        self.update_state('posted')

    
    def reject_bank(self):

        self.update_state('rejected_bank')


    def accept(self):
        if not self.payment_voucher:
            raise UserError(_('Please add voucher payment to continue.'))

        if not self.bank_journal_id:
            raise UserError(_('Select a bank.'))    

        self.update_state('accepted')


    def pay(self):

        if self.accounting_action():
            self.update_credit_note_state('paid')
            self.update_state('paid')


    def accounting_action(self):
        move_id = ''
        today = datetime.today().date()
        partner_id = self.env.user.partner_id
        account_account = self.credit_note_ids[-1].journal_id.default_debit_account_id

        move_vals = {'ref': '', 'conac_move': False, 'date': today, 'journal_id': self.bank_journal_id.id, 'company_id': self.env.user.company_id.id,
                        'line_ids': [(0, 0, {
                            'name':	 account_account.name,    
                            'account_id': account_account.id,
                            'debit': self.credit_note_ids.amount_total,
                            'partner_id': partner_id.id,
                            'refund_credit_note_id': self.id,
                            }),
                            (0, 0, {
                            'name':	 account_account.name,
                            'account_id': self.bank_journal_id.default_credit_account_id.id,
                            'credit': self.credit_note_ids.amount_total,
                            'partner_id': partner_id.id,
                            'refund_credit_note_id': self.id,
                            }),
                    ]}
                    
        move_obj = self.env[ACCOUNT_MOVE_MODEL]
        move_id = move_obj.create(move_vals)
        move_id.action_post()

        return move_id    


    def reject_wizard(self):
        
        return {
            'name': _(REASON_FOR_REJECTION_TEXT),
            'type': 'ir.actions.act_window',
            'res_model': 'refund.credit.note.wizard',
            'view_type': 'form',
            'view_mode': 'form',
            'views': [(self.env.ref("siif_income.reject_refund_credit_note_wizard_view_form").id, 'form')],
            'context': {
            },
            'target': 'new'}
    

    def reject_paid(self):
        
        return {
            'name': _(REASON_FOR_REJECTION_TEXT),
            'type': 'ir.actions.act_window',
            'res_model': 'refund.credit.note.wizard',
            'view_type': 'form',
            'view_mode': 'form',
            'views': [(self.env.ref("siif_income.reject_paid_refund_credit_note_wizard_view_form").id, 'form')],
            'context': {
            },
            'target': 'new'}


    def cancel(self):
        # Libera la nota de credito
        self.update_credit_note_state('not_paid')
        self.update_state('canceled')


    def reject(self, reject_reason):
        self.reject_reason = reject_reason

        self.update_state('rejected')


    @api.onchange('customer_ids')
    def _onchangue_customer_id(self):
        if len(self.customer_ids) > 1:
                raise ValidationError(_("You can only add one customer."))       


    def unlink(self):
        # for record in self.credit_note_ids:
        #     if record.credit_note_payment_state == 'paid':
        #         raise UserError(_('The credit note on this request has already been paid.'))
        # Libera la nota de credito al suprimir registro
        self.update_credit_note_state('not_paid')
        res = super(RefundCreditNote, self).unlink()

        return res               


class AccountMoveLine(models.Model):

    _inherit = ACCOUNT_MOVE_LINE_MODEL


    refund_credit_note_id = fields.Many2one(
        'refund.credit.note', 'Refund Credit Note')


class AccountMove(models.Model):

    _inherit = ACCOUNT_MOVE_MODEL


    credit_note_payment_state = fields.Selection(selection=[
        ('not_paid', 'Not Paid'),
        ('in_request', 'In Request'),
        ('paid', 'Paid'),
        ('recognized', 'Recognized')
    ], string='Credit Note Status Request', readonly=True)
