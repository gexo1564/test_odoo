# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import re
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError

from odoo.addons.siif_security.tools.utils import check_authorization, check_only_allowed_fields

class SHCPCode(models.Model):

    _name = 'shcp.code'
    _description = 'SHCP Program Code'

    name = fields.Char("Code")
    desc = fields.Text(string='Description')

    _sql_constraints = [('shcp_code', 'unique(name, start_date, end_date)', 'The SHCP Program Code must be unique.')]

    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')

    @api.constrains('start_date', 'end_date')
    def check_end_date(self):
        if not self.start_date:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("Configure la fecha inicial"))
            else:
                raise ValidationError(_("The start date of validity is required"))
        if self.end_date and self.end_date <= self.start_date:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("La fecha de fin de vigencia no puede ser menor o igual a la fecha de inicio de vigencia"))
            else:
                raise ValidationError(_("The effective end date cannot be less than or equal to the effective date"))

    @api.constrains('name')
    def _check_name(self):
        if self.name:
            # To check size of the position is exact 2
            if len(self.name) != 4:
                raise ValidationError(_('The SHCP program value size must be four.'))
            if self.name and len(self.name) == 4:
                if not (re.match("[A-Z]{1}\d{3}", str(self.name).upper())):
                    raise UserError(
                        _('Please enter first digit as letter and last 3 digits as numbers for SHCP.'))

    @check_authorization(
        'jt_budget_mgmt.group_budget_catalogs_admin_user',
        'jt_budget_mgmt.group_budget_admin_user'
    )
    @check_only_allowed_fields('desc', 'start_date', 'end_date')
    def write(self, vals):
        return super(SHCPCode, self).write(vals)

    @check_authorization(
        'jt_budget_mgmt.group_budget_catalogs_admin_user',
        'jt_budget_mgmt.group_budget_admin_user'
    )
    def unlink(self):
        for code in self:
            conpp = self.env['budget.program.conversion'].search([('shcp', '=', code.id)], limit=1)
            if conpp:
                raise ValidationError(_('You can not delete SHCP Program Code which are mapped with'
                                        ' Budget Program Conversion (CONPP)!'))
        return super(SHCPCode, self).unlink()

    @api.model
    def fields_get(self, fields=None, attributes=None):
        no_selectable_fields = [ 'name', 'desc' ]
        res = super(SHCPCode, self).fields_get(fields, attributes=attributes)
        for key, value in res.items():
            if key in no_selectable_fields:
                value.update({'selectable': False})
        return res