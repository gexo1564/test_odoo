from odoo import models, fields, api, _

CHANNEL_NAME= 'Picking Validated'

class MailThread(models.AbstractModel):
    _inherit = ['mail.thread']

    def inbox_message(self, title, message, users):
        """
        Send user chat notification on picking validation.
        """
        # construct the message that is to be sent to the user
        message_text = f"""<strong>{title}</strong>
                        <p>{message}</p>
        """

        # odoo runbot
        odoobot_id = self.env['ir.model.data'].sudo().xmlid_to_res_id("base.partner_root")

        # find if a channel was opened for this user before
        channel = self.env['mail.channel'].sudo().search([
            ('name', '=', CHANNEL_NAME),
            ('channel_partner_ids', 'in', [u.partner_id.id for u in users])
        ],
            limit=1,
        )

        if not channel:
            # create a new channel
            channel = self.env['mail.channel'].with_context(mail_create_nosubscribe=True).sudo().create({
                'channel_partner_ids': [(4, u.partner_id.id) for u in users] + [ (4, odoobot_id)],
                'public': 'private',
                'channel_type': 'chat',
                'email_send': False,
                'name': CHANNEL_NAME,
                'display_name': CHANNEL_NAME,
            })

        # send a message to the related user
        channel.sudo().message_post(
            body=message_text,
            author_id=odoobot_id,
            message_type="comment",
            subtype="mail.mt_comment",
        )