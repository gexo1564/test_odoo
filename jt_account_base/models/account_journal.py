# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields, api, _
from odoo.exceptions import UserError
import re
from datetime import datetime

"""Variables Globales"""
ACC_JOURNAL='account.journal'
MIN_BALANCE_HISTORY='minimum.balance.history'
class AccountJournal(models.Model):
    _inherit = ACC_JOURNAL

    account_type = fields.Selection([('check', 'Checks'), ('card', 'Card'),
                                     ('master', 'Master Account'),
                                     ('key', 'Key Account')],
                                    string='Type of Account')
    branch_number = fields.Char('Branch Number', size=4)

    #Bank Accounts
    customer_number = fields.Char('Customer number')
    branch_office = fields.Char('Branch office')
    unam_account_name = fields.Char("UNAM Account Name")
    contract_number = fields.Char('Contract number')
    opening_date = fields.Date('Opening date')
    cancellation_date = fields.Date('Cancellation date')
    concept_id = fields.Many2one('bank.account.concept',string='Concept')
    has_checkbook = fields.Boolean(
        default=False, string='Do you have a checkbook?')
    checkbook_type = fields.Selection(
        [('normal', 'Normal'), ('massive', 'Massive')], string='Type of checkbook')
    checkbook_no = fields.Char("Checkbook No.")
    signatures = fields.Binary('Account Authorization Signatures')
    contract = fields.Binary('Attach contract')
    min_balance = fields.Monetary(string='Minimum Balance', tracking=True)
    min_balance_start_date = fields.Date('Start Date')
    min_balance_end_date = fields.Date('End Date')
    executive_ids = fields.One2many('executive.data', 'journal_id')
    clabe_account = fields.Char(related='bank_account_id.l10n_mx_edi_clabe',string='CLABE')
    update_history_ids = fields.One2many(MIN_BALANCE_HISTORY, 'journal_id')
    cost_per_check = fields.Monetary("Cost per check")
    
    is_federal_subsidy = fields.Boolean(string='Federal Subsidy',default=False,copy=False)
    is_productive_account = fields.Boolean(string='Productive Accounts',default=False,copy=False)
    is_investment_account = fields.Boolean(string='Investment',default=False,copy=False)
    is_interest_account = fields.Boolean(string='Interests',default=False,copy=False)
    
    

    @api.constrains('min_balance')
    def check_min_balance(self):
        if self.min_balance and self.min_balance < 0:
            raise UserError(_('The balance cannot be negative.'))

    @api.constrains('branch_number')
    def check_branch_number(self):
        if self.branch_number:
            pattern = "^\d{4}$"
            if not re.match(pattern, self.branch_number):
                raise UserError(_('The Branch Number should be of 4 digits.'))
        if self.branch_number and len(self.branch_number) != 4:
            raise UserError(_('The Branch Number should be of 4 digits.'))

    def create_update_history_record(self):
        for record in self:
            start_date = 0
            end_date = 0
            if record.min_balance_start_date:
                start_date = record.min_balance_start_date.day
            if record.min_balance_end_date:
                end_date = record.min_balance_end_date.day
             
            vals = {'start':start_date,'ends':end_date,'journal_id':record.id,'amount':record.min_balance,'update_date':datetime.today()}
            self.env[MIN_BALANCE_HISTORY].create(vals)
            
    @api.model
    def create(self,vals):
        #######################################################################
        # Parent block
        # Instead of calling the parent method: addons/account/models/account.py
        # its code is modified so as not to create the accounting accounts
        company_id = vals.get('company_id', self.env.company.id)
        if vals.get('type') in ('bank', 'cash'):
            # For convenience, the name can be inferred from account number
            vals['name'] = vals.get('name', vals.get('bank_acc_number'))

            # If no code provided, loop to find next available journal code
            vals['code'] = vals.get('code', self.get_next_bank_cash_default_code(vals['type'], company_id))
            if not vals['code']:
                raise UserError(_("Cannot generate an unused journal code. Please fill the 'Shortcode' field."))

            company = self.env['res.company'].browse(company_id)
            if vals['type'] == 'cash':
                vals['profit_account_id'] = vals.get('profit_account_id', company.default_cash_difference_income_account_id.id)
                vals['loss_account_id'] = vals.get('loss_account_id', company.default_cash_difference_expense_account_id.id)

        vals['refund_sequence'] = vals.get('refund_sequence', vals['type'] in ('sale', 'purchase'))

        # We just need to create the relevant sequences according to the chosen options
        vals['sequence_id'] = vals.get('sequence_id', self.sudo()._create_sequence(vals).id)
        if vals.get('type') in ('sale', 'purchase') and vals.get('refund_sequence') and not vals.get('refund_sequence_id'):
            vals.update({'refund_sequence_id': self.sudo()._create_sequence(vals, refund=True).id})
        journal = super(AccountJournal.__bases__[0], self.with_context(mail_create_nolog=True)).create(vals)
        if 'alias_name' in vals:
            journal._update_mail_alias(vals)

        # Create the bank_account_id if necessary
        if journal.type == 'bank' and not journal.bank_account_id and vals.get('bank_acc_number'):
            journal.set_bank_account(vals.get('bank_acc_number'), vals.get('bank_id'))
        #######################################################################
        if 'min_balance' in vals and vals.get('min_balance',0) != 0:
            journal.create_update_history_record()
        self._update_money_fund(journal)
        return journal

    def write(self,vals):
        res = super(AccountJournal,self).write(vals)
        if 'min_balance' in vals:
            self.create_update_history_record()
        return res
    
    #ACTUALIZAR FONDOS FIJOS
    def _update_money_fund(self,res):
        #Asignar cuenta a fondo fijo
        if res.account_open_request_id:
            money_fund=self.env['money.funds'].search([('request_account','=',res.account_open_request_id.id)])
            

            if money_fund:
                money_fund.bank_account_journal=res.id
                

    # 'branch_office', 'min_balance', 'currency_id',
    @api.onchange('bank_account_id')
    def auto_insert(self):
        self.branch_office = self.bank_account_id.branch_number
        self.min_balance = self.bank_account_id.minimum_balance

        moneda_value = self.bank_account_id.currency_id.name
        moneda = self.env['res.currency'].search([])
        for i in range(len(moneda)):
            if moneda[i].name == moneda_value:
                self.currency_id = moneda[i]
    
class ExecutiveData(models.Model):

    _name = 'executive.data'
    _description = "Executive Data"
    _rec_name = 'name'

    journal_id = fields.Many2one(ACC_JOURNAL)
    name = fields.Char('Name')
    position = fields.Char('Position')
    telephone = fields.Char('Telephone')
    address = fields.Char('Address')
    email = fields.Char('Email')
    recipient_executive = fields.Boolean("Recipient Executive")
    copied_executive = fields.Boolean("Copied Executive")
    
class MinimumBalanceHistory(models.Model):
    
    _name = MIN_BALANCE_HISTORY 
    _description = "Minimum Balance History"
        
    start = fields.Integer('Start')
    ends = fields.Integer('Ends')
    amount = fields.Monetary(string='Amount')
    update_date = fields.Date('Update Date')
    journal_id = fields.Many2one(ACC_JOURNAL,'Journal')
    currency_id = fields.Many2one(related='journal_id.currency_id')

