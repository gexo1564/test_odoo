# -*- coding: utf-8 -*-
import logging
from odoo import models, fields, api, _

class ResourceOrigin(models.Model):
    
    _inherit = 'resource.origin'
    
    def name_get(self):
        result = []
        for rec in self:
            name = rec.key_origin or ''
            if (rec.key_origin and self.env.context):
                #logging.critical("Origen del recurso")
                if rec.key_origin == '00':
                    description = _('Federal Subsidy')
                elif rec.key_origin == '01':
                    description = _('Extraordinary Income')
                elif rec.key_origin == '05':
                    description = _('Returns Reassignment PEF')
                else:
                    description = ''
                name= rec.key_origin + ' ' + description           
            result.append((rec.id, name))
        return result