# -*- coding: utf-8 -*-
import logging
from odoo import models, fields, api, _

class ResUsers(models.Model):
    _inherit = 'res.users'

    permission_type_of_account_payable = fields.Many2many(
      'account.topay.type',
      'permissions_account_to_type_rel',
      string = "User Permissions to Account Payable Type"
    )


    permission_upa = fields.Many2many('policy.keys','allowed_users_upa_cxp_rel', string="UPA Keys")
    all_upa_sel = fields.Boolean(string='All UPA selected')

    #Asignación de Todos las UPA
    @api.onchange('all_upa_sel')
    def _all_upa_selected(self):

        if (self.all_upa_sel==True):
            #logging.critical("Boton True")  
            upas_info = self.env['policy.keys'].search([])
            #logging.critical(type(dependency_ids))
            for user in self:
                user.permission_upa=upas_info
        else:
            #logging.critical("Boton FAlse") 
            for user in self:
                #logging.critical(len(user.dependency_ids))
                if(len(user.dependency_ids)>=1):
                    user.permission_upa=[(6, 0, [])]  