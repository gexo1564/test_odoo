# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2023 UNAM.
#    Author: SIIF UNAM
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


import io
import base64
import json
from datetime import datetime
import pytz
from psycopg2 import sql

from odoo import models, api, _
from odoo.tools.misc import xlsxwriter

# CONSTANTS FOR FORMATTING
DATE_FORMAT = '%Y-%m-%d'
CURRENCY_FORMAT = '#,##0'
TEXT_RIGHT_FORMAT = 'text-right text-dark'
TEXT_LEFT_FORMAT = 'text-left text-dark'

class IncomeExpensesInvestmentStatementReport(models.AbstractModel):

    _name = "income.expenses.and.investment.statement.report"
    _inherit = "account.report"
    _description = "Income, Expenses and Investment Statement Report"

    filter_date = {'mode': 'single', 'filter': 'this_year'}
    filter_all_entries = None
    filter_journals = None
    filter_unfold_all = None
    filter_cash_basis = None
    filter_hierarchy = None
    MAX_LINES = None
    filter_levels = None

    def _get_reports_buttons(self):
        return [
            {'name': _('Export (XLSX)'),
             'sequence': 1,
             'action': 'print_xlsx',
             'file_export_type': _('XLSX')
             },
        ]

    def print_xlsx(self, options, extra_data):
        options['is_xlsx_report'] = True
        return {
                'type': 'ir_actions_account_report_download',
                'data': {'model': self.env.context.get('model'),
                         'options': json.dumps(options),
                         'output_format': 'xlsx',
                         'financial_id': self.env.context.get('id'),
                         }
                }

    @api.model
    def _get_columns_name(self, options):
        columns = [
            {'name': _('ANNEXES'), 'class': TEXT_LEFT_FORMAT},
            {'name': _('CONCEPT'), 'class': TEXT_LEFT_FORMAT},
            {'name': _('PARTIAL of ')+options['date'].get('date_to'), 'class': TEXT_RIGHT_FORMAT},
            {'name': _('TOTAL of ')+options['date'].get('date_to'), 'class': TEXT_RIGHT_FORMAT},
            {'name': _('PARTIAL of ')+self.get_last_year_date(options['date'].get('date_to')), 'class': TEXT_RIGHT_FORMAT},
            {'name': _('TOTAL of ')+self.get_last_year_date(options['date'].get('date_to')), 'class': TEXT_RIGHT_FORMAT},
            {'name': _('PARTIAL'), 'class': TEXT_RIGHT_FORMAT},
            {'name': _('TOTAL VARIATION'), 'class': TEXT_RIGHT_FORMAT},
            {'name': _('%'), 'class': TEXT_RIGHT_FORMAT},  
        ]

        return columns

    @api.model
    def _get_lines(self, options, line_id=None):
        # Groups that have access to the report
        list_groups = [
            'jt_account_base.group_account_admin_user',
            'jt_account_base.group_account_super_admin_user',
            'jt_account_base.group_account_guest_user',
            'jt_account_base.group_account_approval_user']
        
        self.env['security.tools'].check_user_permissions(list_groups)         

        lines = []

        category_name = 'A'
        annexe_name = 'INGRESOS'
        first_column_name = category_name
        level = 0
        lines.append(self.get_category_dict_column(category_name, annexe_name, level, False, first_column_name))

        
        ## Subsidio Federal 
        category_name = 'B'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_federal_subsidy')
        parent_category_name = 'A'
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        subsidy_total_amount_selected_date, subsidy_total_amount_last_year, subsidy_total_amount_variation, subsidy_total_variation_percent = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        
        ## Total 
        concept_name = 'SUBSIDIO'
        parent_category_name = 'A'
        level = 0
        second_column_name = concept_name
        text_align = 'text-right'
        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, subsidy_total_amount_selected_date, subsidy_total_amount_last_year, subsidy_total_amount_variation, subsidy_total_variation_percent, level, second_column_name, text_align))
    
        ## Productos Financieros 
        category_name = 'C'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_financial_products')
        parent_category_name = 'A'
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        financial_products_total_amount_selected_date, financial_products_total_amount_last_year, financial_products_total_amount_variation, financial_products_total_variation_percent = self._add_accounts_to_lines(lines, options, annexe_name, category_name)  

        ## Ingresos Extroardinarios  
        category_name = 'D'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_extraordinary_income')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        extraordinary_income_total_amount_selected_date, extraordinary_income_total_amount_last_year, extraordinary_income_total_amount_variation,  extraordinary_total_variation_percent = self._add_accounts_to_lines(lines, options, annexe_name, category_name)    

        ## Ingresos por servicios de educación  
        category_name = 'E'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_education_services')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        educational_income_total_amount_selected_date, educational_income_total_amount_last_year, educational_income_total_amount_variation, educational_income_total_variation_percent  = self._add_accounts_to_lines(lines, options, annexe_name, category_name)   

        ## Otros ingresos
        category_name = 'F'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_other_income')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        other_income_total_amount_selected_date, other_income_total_amount_last_year, other_income_total_amount_variation, other_income_total_variation_percent = self._add_accounts_to_lines(lines, options, annexe_name, category_name)

        ## Total INGRESOS PROPIOS 
        concept_name = 'income_own'
        parent_category_name = 'A'
        own_income_total_amount_selected_date =  financial_products_total_amount_selected_date + extraordinary_income_total_amount_selected_date + educational_income_total_amount_selected_date + other_income_total_amount_selected_date
        own_income_total_amount_last_year = financial_products_total_amount_last_year + extraordinary_income_total_amount_last_year + educational_income_total_amount_last_year + other_income_total_amount_last_year
        own_income_total_amount_variation = financial_products_total_amount_variation + extraordinary_income_total_amount_variation + educational_income_total_amount_variation + other_income_total_amount_variation
        own_income_total_variation_percent = self.get_percent_variation(own_income_total_amount_selected_date, own_income_total_amount_last_year)
        level = 0
        second_column_name = 'INGRESOS PROPIOS'
        text_align = 'text-right'
        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, own_income_total_amount_selected_date, own_income_total_amount_last_year, own_income_total_amount_variation, own_income_total_variation_percent, level, second_column_name, text_align))
        
        ## TOTAL INGRESOS
        concept_name = 'total_income'
        parent_category_name = 'A'
        income_total_amount_selected_date = subsidy_total_amount_selected_date + financial_products_total_amount_selected_date + extraordinary_income_total_amount_selected_date + educational_income_total_amount_selected_date + other_income_total_amount_selected_date
        income_total_amount_last_year = subsidy_total_amount_last_year + financial_products_total_amount_last_year + extraordinary_income_total_amount_last_year + educational_income_total_amount_last_year + other_income_total_amount_last_year
        income_total_amount_variation = subsidy_total_amount_variation + financial_products_total_amount_variation + extraordinary_income_total_amount_variation + educational_income_total_amount_variation + other_income_total_amount_variation
        income_total_variation_percent = self.get_percent_variation(income_total_amount_selected_date, income_total_amount_last_year)
        level = 0
        second_column_name = 'TOTAL INGRESOS'
        text_align = 'text-right'
        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, income_total_amount_selected_date, income_total_amount_last_year, income_total_amount_variation, income_total_variation_percent, level, second_column_name, text_align))
        
        category_name = 'gastos_operacion'
        annexe_name = 'Gastos de operación'
        level = 0
        lines.append(self.get_category_dict_column(category_name, annexe_name, level))
        
        
        category_name = 'remuneraciones_personales'
        annexe_name = 'Remuneraciones Personales y Prestaciones'
        parent_category_name = 'gastos_operacion'
        level = 1
        lines.append(self.get_category_dict_column(category_name, annexe_name, level, parent_category_name))

        ## Remuneraciones al personal académico
        category_name = 'G'
        parent_category_name = 'remuneraciones_personales'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_remuneration_academic_personal')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        remuneration_academic_personal_total_amount_selected_date, remuneration_academic_personal_total_amount_last_year, remuneration_academic_personal_total_amount_variation, remuneration_academic_personal_total_variation_percent = self._add_accounts_to_lines(lines, options, annexe_name, category_name) 

        ## Remuneraciones al personal administrativo
        category_name = 'H'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_remuneration_academic_administrative')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        remuneration_academic_administrative_total_amount_selected_date, remuneration_academic_administrative_total_amount_last_year, remuneration_academic_administrative_total_amount_variation, remuneration_academic_administrative_total_variation_percent = self._add_accounts_to_lines(lines, options, annexe_name, category_name)

        ## Aguinaldo y prima vacacional
        category_name = 'I'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_bonus_vacation')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        bonus_vacation_total_amount_selected_date, bonus_vacation_total_amount_last_year, bonus_vacation_total_amount_variation, bonus_vacation_total_variation_percent = self._add_accounts_to_lines(lines, options, annexe_name, category_name) 

        ## Gratificaciones por separación y jubilación e indemnizaciones por defunción
        category_name = 'J'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_gratification_separation_retirement')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        gratification_separation_retirement_total_amount_selected_date, gratification_separation_retirement_amount_last_year, gratification_separation_retirement_total_amount_variation, gratification_separation_retirement_total_variation_percent = self._add_accounts_to_lines(lines, options, annexe_name, category_name)

        ## Cuotas de seguridad social
        category_name = 'K'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_social_security_fees')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        social_security_fees_total_amount_selected_date, social_security_fees_amount_last_year, social_security_fees_total_amount_variation, social_security_fees_total_variation_percent = self._add_accounts_to_lines(lines, options, annexe_name, category_name)

        ## Otras prestaciones sociales
        category_name = 'L'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_other_social_benefits')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        other_social_benefits_total_amount_selected_date, other_social_benefits_amount_last_year, other_social_benefits_total_amount_variation, other_social_benefits_total_variation_percent = self._add_accounts_to_lines(lines, options, annexe_name, category_name)

        ## TOTAL 
        concept_name = 'remunerations_total'
        remunerations_total_amount_selected_date = remuneration_academic_personal_total_amount_selected_date + remuneration_academic_administrative_total_amount_selected_date + bonus_vacation_total_amount_selected_date + gratification_separation_retirement_total_amount_selected_date + social_security_fees_total_amount_selected_date + other_social_benefits_total_amount_selected_date
        remunerations_total_amount_last_year = remuneration_academic_personal_total_amount_last_year + remuneration_academic_administrative_total_amount_last_year + bonus_vacation_total_amount_last_year + gratification_separation_retirement_amount_last_year + social_security_fees_amount_last_year + other_social_benefits_amount_last_year
        remunerations_total_amount_variation = remuneration_academic_personal_total_amount_variation + remuneration_academic_administrative_total_amount_variation + bonus_vacation_total_amount_variation + gratification_separation_retirement_total_amount_variation + social_security_fees_total_amount_variation + other_social_benefits_total_amount_variation
        remunerations_total_variation_percent = self.get_percent_variation(remunerations_total_amount_selected_date, remunerations_total_amount_last_year)
        level = 0
        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, remunerations_total_amount_selected_date, remunerations_total_amount_last_year, remunerations_total_amount_variation, remunerations_total_variation_percent, level))

        ## Becas
        category_name = 'M'
        parent_category_name = 'gastos_operacion'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_scolarship')
        level = 0
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        scolarship_total_amount_selected_date, scolarship_amount_last_year, scolarship_total_amount_variation, scolarship_total_variation_percent = self._add_accounts_to_lines(lines, options, annexe_name, category_name)

        ## Erogaciones para sedes en el extranjero
        category_name = 'N'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_foreign_expenses')
        level = 0
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        foreign_expenses_amount_selected_date, foreign_expenses_amount_last_year, foreign_expenses_amount_variation, foreign_expenses_total_variation_percent = self._add_accounts_to_lines(lines, options, annexe_name, category_name)

        category_name = 'services_articles_materials'
        annexe_name = 'Servicios, Artículos y Materiales de Consumo'
        level = 0
        lines.append(self.get_category_dict_column(category_name, annexe_name, level, parent_category_name))
        
        ## Materiales, refacciones, herramientas y accesorios didácticos y de investigación
        category_name = 'O'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_materials')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        materials_amount_selected_date, materials_amount_last_year, materials_amount_variation, materials_total_variation_percent = self._add_accounts_to_lines(lines, options, annexe_name, category_name) 
        
        ## Servicios y materiales de mantenimiento para edificios, equipos e instalaciones
        category_name = 'P'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_maintenance')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        maintenance_amount_selected_date, maintenance_amount_last_year, maintenance_amount_variation, maintenance_amount_variation_total_variation_percent  = self._add_accounts_to_lines(lines, options, annexe_name, category_name)

        ## Energía eléctrica y servicios de comunicación
        category_name = 'Q'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_electricity')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        electricity_amount_selected_date, electricity_amount_last_year, electricity_amount_variation, electricity_total_variation_percent = self._add_accounts_to_lines(lines, options, annexe_name, category_name)

        ## Trabajos de campo, prácticas escolares, paisajes y viáticos
        category_name = 'R'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_field_work')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        field_work_amount_selected_date, field_work_amount_last_year, field_work_amount_variation, field_work_total_variation_percent = self._add_accounts_to_lines(lines, options, annexe_name, category_name)

        ## Arrendamiento de muebles y equipo
        category_name = 'S'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_rent')
        level = 1
        unfolded = True
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level, unfolded))
        
        # Otros Servicios Comerciales
        category_name = 'commercial_services'
        parent_category_name = 'S'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_commercial_services')
        level = 1
        lines.append(self.get_parcial_annexe_dict_column(category_name, parent_category_name, annexe_name, level))
        commercial_services_amount_selected_date, commercial_services_amount_last_year, commercial_services_amount_variation, commercial_services_total_variation_percent = self._add_accounts_to_partial_lines(lines, options, annexe_name, category_name)

        # Licencias de Sistemas Informáticos
        category_name = 'statement_licenses'
        parent_category_name = 'S'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_licenses')
        level = 1
        lines.append(self.get_parcial_annexe_dict_column(category_name, parent_category_name, annexe_name, level))
        statement_licenses_amount_selected_date, statement_licenses_amount_last_year, statement_licenses_amount_variation, statement_licenses_total_variation_percent = self._add_accounts_to_partial_lines(lines, options, annexe_name, category_name)

        # Comisiones bancarias y financieras
        category_name = 'commissions'
        parent_category_name = 'S'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_commissions')
        level = 1
        lines.append(self.get_parcial_annexe_dict_column(category_name, parent_category_name, annexe_name, level))
        commissions_amount_selected_date, commissionsamount_last_year,ccommissionsamount_variation, ccommissions_amount_variation_percent = self._add_accounts_to_partial_lines(lines, options, annexe_name, category_name)

        # Rentas de Inmuebles y Equipo
        category_name = 'rent_equipment'
        parent_category_name = 'S'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_rent_equipment')
        level = 1
        lines.append(self.get_parcial_annexe_dict_column(category_name, parent_category_name, annexe_name, level))
        rent_equipment_amount_selected_date, rent_equipment_amount_last_year, rent_equipment_amount_variation, rent_equipment_total_variation_percent = self._add_accounts_to_partial_lines(lines, options, annexe_name, category_name)

        # Sumas de parciales
        partial_total_amount_selected_date = commercial_services_amount_selected_date + statement_licenses_amount_selected_date + commissions_amount_selected_date + rent_equipment_amount_selected_date
        partial_total_amount_last_year = commercial_services_amount_last_year + statement_licenses_amount_last_year + commissionsamount_last_year + rent_equipment_amount_last_year
        partial_total_amount_variation = commercial_services_amount_variation + statement_licenses_amount_variation + ccommissionsamount_variation + rent_equipment_amount_variation
        partial_total_variation_percent = self.get_percent_variation(partial_total_amount_selected_date, partial_total_amount_last_year)
        
        ## Ediciones de libros y revistas
        category_name = 'T'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_editions')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        editions_amount_selected_date, editions_amount_last_year, editions_amount_variation, editions_total_variation_percent = self._add_accounts_to_lines(lines, options, annexe_name, category_name)

        ## Cuotas de afiliación a sociedades e instituciones científicas, culturales y deportivas; simposios, congresos y seminarios
        category_name = 'U'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_membership_fees')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        membership_fees_amount_selected_date, membership_fees_amount_last_year, membership_fees_amount_variation, membership_fees_total_variation_percent  = self._add_accounts_to_lines(lines, options, annexe_name, category_name)

        ## Gastos y derechos de importación, seguros y fianzas
        category_name = 'V'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_importation_expenses')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        importation_expenses_amount_selected_date, importation_expenses_amount_last_year, importation_expenses_amount_variation, importation_expenses_total_variation_percent = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        
        ## Servicios por asesorías para operación de programas
        category_name = 'W'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_advisory_services')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        advisory_services_amount_selected_date, advisory_services_amount_last_year, advisory_services_amount_variation, advisory_services_total_variation_percent = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        
        ## Apoyo a programas de extensión y colaboración académica y científica
        category_name = 'X'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_support_programs')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        support_programs_amount_selected_date, support_programs_amount_last_year, support_programs_total_amount_variation, support_programs_total_variation_percent = self._add_accounts_to_lines(lines, options, annexe_name, category_name)

        ## TOTAL 
        concept_name = 'services_total'
        total_services_amount_selected_date = materials_amount_selected_date + maintenance_amount_selected_date + electricity_amount_selected_date  + field_work_amount_selected_date + partial_total_amount_selected_date + editions_amount_selected_date + membership_fees_amount_selected_date + importation_expenses_amount_selected_date + advisory_services_amount_selected_date + support_programs_amount_selected_date
        total_services_amount_last_year = materials_amount_last_year + maintenance_amount_last_year + electricity_amount_last_year + field_work_amount_last_year + partial_total_amount_last_year + editions_amount_last_year + membership_fees_amount_last_year + importation_expenses_amount_last_year + advisory_services_amount_last_year + support_programs_amount_last_year
        total_services_amount_variation = materials_amount_variation + maintenance_amount_variation + electricity_amount_variation + field_work_amount_variation + partial_total_amount_variation + editions_amount_variation + membership_fees_amount_variation + importation_expenses_amount_variation + advisory_services_amount_variation + support_programs_total_amount_variation
        total_services_amount_variation_percent = self.get_percent_variation(total_services_amount_selected_date, total_services_amount_last_year)
        level = 0
        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, total_services_amount_selected_date, total_services_amount_last_year, total_services_amount_variation, total_services_amount_variation_percent, level))
        
        ## TOTAL OPERACION
        concept_name = 'operating_costs'
        operating_costs_total_amount_selected_date = remunerations_total_amount_selected_date + scolarship_total_amount_selected_date + foreign_expenses_amount_selected_date + total_services_amount_selected_date
        operating_costs_total_amount_last_year = remunerations_total_amount_last_year + scolarship_amount_last_year + foreign_expenses_amount_last_year + total_services_amount_last_year
        operating_costs_total_amount_variation = remunerations_total_amount_variation + scolarship_total_amount_variation + foreign_expenses_amount_variation + total_services_amount_variation
        operating_costs_total_variation_percent = self.get_percent_variation(operating_costs_total_amount_selected_date, operating_costs_total_amount_last_year)

        level = 0
        second_column_name = 'GASTOS DE OPERACIÓN'
        text_align = 'text-right'
        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, operating_costs_total_amount_selected_date, operating_costs_total_amount_last_year, operating_costs_total_amount_variation, operating_costs_total_variation_percent, level, second_column_name, text_align))
        
        ## TOTAL REMANENTE
        concept_name = 'remaining_investment'
        remaining_investment_total_amount_selected_date = income_total_amount_selected_date - operating_costs_total_amount_selected_date
        remaining_investment_total_amount_last_year = income_total_amount_last_year - operating_costs_total_amount_last_year
        remaining_investment_total_amount_variation = income_total_amount_variation - operating_costs_total_amount_variation
        remaining_investment_total_variation_percent = self.get_percent_variation(remaining_investment_total_amount_selected_date, remaining_investment_total_amount_last_year)

        level = 0
        second_column_name = 'REMANENTE ANTES DE INVERSIONES'
        text_align = 'text-right'
        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, remaining_investment_total_amount_selected_date, remaining_investment_total_amount_last_year, remaining_investment_total_amount_variation, remaining_investment_total_variation_percent, level, second_column_name, text_align))
        

        category_name = 'properties_equipment'
        annexe_name = 'Inversiones en Propiedades y Equipo'
        level = 0
        lines.append(self.get_category_dict_column(category_name, annexe_name, level))
        
        ## Equipo, mobiliario e instrumentos científicos y didácticos
        category_name = 'Y'
        parent_category_name = 'properties_equipment'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_equipment')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        equipment_amount_selected_date, equipment_amount_last_year, equipment_amount_variation, equipment_total_variation_percent = self._add_accounts_to_lines(lines, options, annexe_name, category_name)

        ## Construcciones, rehabilitaciones y remodelación de inmuebles
        category_name = 'Z'
        parent_category_name = 'properties_equipment'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_construction')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        construction_amount_selected_date, construction_amount_last_year, construction_amount_variation, construction_total_variation_percent = self._add_accounts_to_lines(lines, options, annexe_name, category_name)

        ## Libros y revistas académicas y científicas
        category_name = 'AA'
        parent_category_name = 'properties_equipment'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_books')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        books_amount_selected_date, books_amount_last_year, books_amount_variation, books_total_variation_percent = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        
        ## TOTAL INVERSIONES
        concept_name = 'investments_propertie_equipment'
        investments_propertie_equipment_total_amount_selected_date = equipment_amount_selected_date + construction_amount_selected_date + books_amount_selected_date 
        investments_propertie_equipment_total_amount_last_year = equipment_amount_last_year + construction_amount_last_year + books_amount_last_year 
        investments_propertie_equipment_total_amount_variation = equipment_amount_variation + construction_amount_variation + books_amount_variation
        investments_propertie_equipment_total_variation_percent = self.get_percent_variation(investments_propertie_equipment_total_amount_selected_date, investments_propertie_equipment_total_amount_last_year)
        level = 0
        second_column_name = 'INVERSIONES'
        text_align = 'text-right'
        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, investments_propertie_equipment_total_amount_selected_date, investments_propertie_equipment_total_amount_last_year, investments_propertie_equipment_total_amount_variation, investments_propertie_equipment_total_variation_percent, level, second_column_name, text_align))
        
        ## TOTAL GASTOS DE OPERACIÓN E INVERSIONES
        concept_name = 'expenses_investments'
        expenses_investments_total_amount_selected_date = operating_costs_total_amount_selected_date + investments_propertie_equipment_total_amount_selected_date
        expenses_investments_total_amount_last_year = operating_costs_total_amount_last_year + investments_propertie_equipment_total_amount_last_year
        expenses_investments_total_amount_variation = operating_costs_total_amount_variation + investments_propertie_equipment_total_amount_variation
        expenses_investments_total_variation_percent = self.get_percent_variation(expenses_investments_total_amount_selected_date, expenses_investments_total_amount_last_year)
        level = 0
        second_column_name = 'TOTAL GASTOS DE OPERACIÓN E INVERSIONES'
        text_align = 'text-right'
        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, expenses_investments_total_amount_selected_date, expenses_investments_total_amount_last_year, expenses_investments_total_amount_variation, expenses_investments_total_variation_percent, level, second_column_name, text_align))

        ## TOTAL REMANENTE
        concept_name = 'profit'
        profit_total_amount_selected_date = income_total_amount_selected_date - expenses_investments_total_amount_selected_date
        profit_total_amount_last_year = income_total_amount_last_year - expenses_investments_total_amount_last_year
        profit_total_amount_variation = income_total_amount_variation - expenses_investments_total_amount_variation
        profit_total_variation_percent = self.get_percent_variation(profit_total_amount_selected_date, profit_total_amount_last_year)
        level = 0
        second_column_name = 'REMANENTE DEL EJERCICIO'
        text_align = 'text-right'
        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, profit_total_amount_selected_date, profit_total_amount_last_year, profit_total_amount_variation, profit_total_variation_percent, level, second_column_name, text_align))
        
        total_dict = {
            'B': [subsidy_total_amount_selected_date, subsidy_total_amount_last_year, subsidy_total_amount_variation, subsidy_total_variation_percent],
            'C': [financial_products_total_amount_selected_date, financial_products_total_amount_last_year, financial_products_total_amount_variation, financial_products_total_variation_percent],
            'D': [extraordinary_income_total_amount_selected_date, extraordinary_income_total_amount_last_year, extraordinary_income_total_amount_variation, extraordinary_total_variation_percent],
            'E': [educational_income_total_amount_selected_date, educational_income_total_amount_last_year, educational_income_total_amount_variation, educational_income_total_variation_percent],
            'F': [other_income_total_amount_selected_date, other_income_total_amount_last_year, other_income_total_amount_variation, other_income_total_variation_percent],
            'G': [remuneration_academic_personal_total_amount_selected_date, remuneration_academic_personal_total_amount_last_year, remuneration_academic_personal_total_amount_variation, remuneration_academic_personal_total_variation_percent],
            'H': [remuneration_academic_administrative_total_amount_selected_date, remuneration_academic_administrative_total_amount_last_year, remuneration_academic_administrative_total_amount_variation, remuneration_academic_administrative_total_variation_percent],
            'I': [bonus_vacation_total_amount_selected_date, bonus_vacation_total_amount_last_year, bonus_vacation_total_amount_variation, bonus_vacation_total_variation_percent],
            'J': [gratification_separation_retirement_total_amount_selected_date, gratification_separation_retirement_amount_last_year, gratification_separation_retirement_total_amount_variation, gratification_separation_retirement_total_variation_percent],
            'K': [social_security_fees_total_amount_selected_date, social_security_fees_amount_last_year, social_security_fees_total_amount_variation, social_security_fees_total_variation_percent],
            'L': [other_social_benefits_total_amount_selected_date, other_social_benefits_amount_last_year, other_social_benefits_total_amount_variation, other_social_benefits_total_variation_percent],
            'M': [scolarship_total_amount_selected_date, scolarship_amount_last_year, scolarship_total_amount_variation, scolarship_total_variation_percent],
            'N': [foreign_expenses_amount_selected_date, foreign_expenses_amount_last_year, foreign_expenses_amount_variation, foreign_expenses_total_variation_percent],
            'O': [materials_amount_selected_date, materials_amount_last_year, materials_amount_variation, materials_total_variation_percent],
            'P': [maintenance_amount_selected_date, maintenance_amount_last_year, maintenance_amount_variation, maintenance_amount_variation_total_variation_percent],
            'Q': [electricity_amount_selected_date, electricity_amount_last_year, electricity_amount_variation, electricity_total_variation_percent],
            'R': [field_work_amount_selected_date, field_work_amount_last_year, field_work_amount_variation, field_work_total_variation_percent],
            'S': [partial_total_amount_selected_date, partial_total_amount_last_year, partial_total_amount_variation, partial_total_variation_percent],
            'T': [editions_amount_selected_date, editions_amount_last_year, editions_amount_variation, editions_total_variation_percent],
            'U': [membership_fees_amount_selected_date, membership_fees_amount_last_year, membership_fees_amount_variation, membership_fees_total_variation_percent],
            'V': [importation_expenses_amount_selected_date, importation_expenses_amount_last_year, importation_expenses_amount_variation, importation_expenses_total_variation_percent],
            'W': [advisory_services_amount_selected_date, advisory_services_amount_last_year, advisory_services_amount_variation, advisory_services_total_variation_percent],
            'X': [support_programs_amount_selected_date, support_programs_amount_last_year, support_programs_total_amount_variation, support_programs_total_variation_percent],
            'Y': [equipment_amount_selected_date, equipment_amount_last_year, equipment_amount_variation, equipment_total_variation_percent],
            'Z': [construction_amount_selected_date, construction_amount_last_year, construction_amount_variation, construction_total_variation_percent],
            'AA': [books_amount_selected_date, books_amount_last_year, books_amount_variation, books_total_variation_percent],
        }

        self._update_total_for_category(lines, total_dict)

        partials_total_dict = {
            'commercial_services' : [commercial_services_amount_selected_date, commercial_services_amount_last_year, commercial_services_amount_variation, commercial_services_total_variation_percent], 
            'statement_licenses' : [statement_licenses_amount_selected_date, statement_licenses_amount_last_year, statement_licenses_amount_variation, statement_licenses_total_variation_percent],
            'commissions' : [commissions_amount_selected_date, commissionsamount_last_year, ccommissionsamount_variation, ccommissions_amount_variation_percent],
            'rent_equipment' : [rent_equipment_amount_selected_date, rent_equipment_amount_last_year, rent_equipment_amount_variation, rent_equipment_total_variation_percent],
        }

        self._update_total_for_partial_category(lines, partials_total_dict)
   
        return lines
        
    @api.model
    def _get_report_name(self):
        return _("Income, Expenses and Investment Statement Report Comparative Exercised")

    def get_month_name(self, month):
        
        month_names = {
            1: 'Enero',
            2: 'Febrero',
            3: 'Marzo',
            4: 'Abril',
            5: 'Mayo',
            6: 'Junio',
            7: 'Julio',
            8: 'Agosto',
            9: 'Septiembre',
            10: 'Octubre',
            11: 'Noviembre',
            12: 'Diciembre'
        }
        month_name = month_names.get(month, '')

        return month_name

    def get_xlsx(self, options, response=None):
        # Groups that have access to export report
        list_groups = [
            'jt_account_base.group_account_admin_user',
            'jt_account_base.group_account_super_admin_user',
            'jt_account_base.group_account_guest_user',
            'jt_account_base.group_account_approval_user']
        
        self.env['security.tools'].check_user_permissions(list_groups)  

        options['xlsx'] = True

        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        sheet = workbook.add_worksheet(self._get_report_name()[:31])

        title_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'bottom': 2, 'align': 'center'})
        super_col_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'align': 'center'})
        currect_date_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': False, 'align': 'right'})
        currect_date_style.set_border(0)
        super_col_style.set_border(0)
        # Set columns width
        sheet.set_column(0, 0, 25)
        sheet.set_column(1, 1, 80)
        sheet.set_column(2, 2, 20)
        sheet.set_column(3, 3, 20)
        sheet.set_column(4, 4, 20)
        sheet.set_column(5, 5, 20)
        sheet.set_column(6, 6, 20)
        sheet.set_column(7, 7, 20)
        sheet.set_column(8, 8, 20)
        
        y_offset = 0
        col = 0
        
        sheet.merge_range(y_offset, col, 7, col, '', super_col_style)
        if self.env.user and self.env.user.company_id and self.env.user.company_id.header_logo:
            filename = 'logo.png'
            image_data = io.BytesIO(base64.standard_b64decode(self.env.user.company_id.header_logo))
            sheet.insert_image(0,0, filename, {'image_data': image_data,'x_offset':60,'y_offset':15,'x_scale':0.6,'y_scale':0.6})
        
        col += 1
        x_offset = 1

        tz = pytz.timezone('America/Mexico_City')
        actual_date = datetime.now(tz)
        currect_time_msg = "Fecha y hora de impresión: "
        month_name = self.get_month_name(actual_date.month).upper()
        currect_time_msg += str(actual_date.day) + " de " + month_name + " del " + str(actual_date.year) + str(actual_date.strftime(" %I:%M:%S %p"))
        user_msg = " Reporte generado por: " + self.env.user.name
        sheet.merge_range(y_offset, x_offset, y_offset, x_offset+9, '', super_col_style)
        sheet.merge_range(y_offset+1, x_offset, y_offset+1, x_offset+7, 'UNIVERSIDAD NACIONAL AUTÓNOMA DE MÉXICO', super_col_style)
        sheet.merge_range(y_offset+2, x_offset, y_offset+2, x_offset+7, 'PATRONATO UNIVERSITARIO', super_col_style)
        sheet.merge_range(y_offset+3, x_offset, y_offset+3, x_offset+7, 'TESORERÍA', super_col_style)
        sheet.merge_range(y_offset+4, x_offset, y_offset+4, x_offset+7, self._get_report_name().upper(), super_col_style)
        sheet.merge_range(y_offset+5, x_offset, y_offset+5, x_offset+7, 'Cifras en Miles de Pesos', super_col_style)
        sheet.merge_range(y_offset+6, x_offset, y_offset+6, x_offset+7, currect_time_msg, super_col_style)
        sheet.merge_range(y_offset+7, x_offset, y_offset+7, x_offset+7, user_msg, super_col_style)
        y_offset += 10
        str_pre_ejer = 'Presupuesto Ejercido al '
        dict_extra_header = {
            '2': [str_pre_ejer+options['date'].get('date_to'),2],
            '4': [str_pre_ejer+self.get_last_year_date(options['date'].get('date_to')),2],
        }
        dict_column_name = {
            '2': 'Parcial',
            '3': 'Total',
            '4': 'Parcial',
            '5': 'Total'
        }
  
        y_offset = self._generate_header_rows(options, dict_extra_header, dict_column_name, sheet, y_offset, title_style)

        ctx = self._set_context(options)
        ctx.update({'no_format':True, 'print_mode':True, 'prefetch_fields': False})

        # deactivating the prefetching saves ~35% on get_lines running time
        lines = self.with_context(ctx)._get_lines(options)
        if options.get('hierarchy'):
            lines = self._create_hierarchy(lines, options)
        if options.get('selected_column'):
            lines = self._sort_lines(lines, options)            
        
        # # write all data rows
        row_y = 0
        self._write_xlsx(lines, workbook, sheet, y_offset, row_y)

        workbook.close()
        output.seek(0)
        generated_file = output.read()
        output.close()
        return generated_file    

    def _get_accounts_from_detailed_report_configuration(self, name):
        
        query = """
            select
                account_account_id
            from
                account_account_detailed_report_configuration_rel
            where
                detailed_report_configuration_id = (
                select
                    id
                from
                    detailed_report_configuration
                where
                    name = %(name)s)
        
        """
        params = {'name' : name}


        self.env.cr.execute(query, params)
        result = self.env.cr.fetchall()

        result_list = []

        for rec in result :
            result_list.append(rec[0])

        return result_list

    def _add_accounts_to_lines (self, lines, options, category_name, parent_name):
        accounts_list  =  self.env['account.account'].browse(self._get_accounts_from_detailed_report_configuration(category_name))

        total_amount_selected_date = 0
        total_amount_last_year = 0
        total_amount_variation = 0
        total_percent = 0

        for account in accounts_list:
            actual_date = options['date'].get('date_to')

            amount_selected_date = self._verify_account_type(account.code, self._get_account_total_amount(actual_date, account.id)) 
            amount_last_year = self._verify_account_type(account.code, self._get_account_total_amount(self.get_last_year_date(actual_date), account.id))
            amount_variation = amount_selected_date - amount_last_year
            percent_variation = self.get_percent_variation(self._get_rounded_formatted_value(amount_selected_date), self._get_rounded_formatted_value(amount_last_year))
    
            total_amount_selected_date += amount_selected_date
            total_amount_last_year += amount_last_year
            total_amount_variation += amount_variation
            

            lines.append(
                {'id': 'column_' + str(account.id),
                'name': '',
                'level': 3,
                'parent_id': parent_name,
                'columns' : [
                    {
                        'id': 'account_' + str(account.id),
                        'name': account.code,
                        'level': 3,
                        'columns' : [],
                        'unfoldable': False,
                    },
                    {},
                    {
                        'id': 'account_total_' + str(account.id),
                        'name': self._get_rounded_formatted_value(amount_selected_date), 'class': 'number', 'raw_value': self._get_rounded_formatted_value(amount_selected_date),
                        'level': 3,
                        'columns' : [],
                        'unfoldable': False,
                    },
                    {},
                    {
                        'id': 'account_total_last_year' + str(account.id),
                        'name': self._get_rounded_formatted_value(amount_last_year), 'class': 'number', 'raw_value': self._get_rounded_formatted_value(amount_last_year),
                        'level': 3,
                        'columns' : [],
                        'unfoldable': False,  
                    },
                    {},
                    {
                        'id': 'account_variation' + str(account.id),
                        'name': self._get_rounded_formatted_value(amount_variation),
                        'level': 3,
                        'columns' : [],
                        'unfoldable': False,  
                    },
                    {
                        'id': 'account_variation_percent' + str(account.id),
                        'name': percent_variation,
                        'level': 3,
                        'columns' : [],
                        'unfoldable': False,  
                        
                    }
                ]})

        total_percent = self.get_percent_variation(self._get_rounded_formatted_value(total_amount_selected_date), self._get_rounded_formatted_value(total_amount_last_year))
        return total_amount_selected_date, total_amount_last_year,  total_amount_variation,  total_percent

    def _get_dict_column(self, name, level):
        dict_column = {
            'id': 'category_' + name,
            'name': name,
            'level': level
        }
        return dict_column

    def _get_dict_number_format(self, name, level):
        dict_column = {
            'name': name , 'class': 'number',
            'level': level
        }
        return dict_column
    
    def _get_dict_number_rounded_thousnad_format(self, name, level):
        dict_column = {
            'name': self._get_rounded_formatted_value(name) , 
            'class': 'number',
            'level': level
        }
        return dict_column


    def _get_account_total_amount(self, date_to, account_id):     
        date_from = self.get_first_and_last_day_of_year(date_to)[0]
        query = sql.SQL("""
                select
                    COALESCE(SUM(balance), 0) as sumatoria_apuntes
                from
                    account_move_line
                where
                    account_id = {}
                    and date >= {}
                    and date <= {}
                    and parent_state = 'posted';
            """).format(
                sql.Literal(account_id),
                sql.Literal(date_from),
                sql.Literal(date_to)
                )
            
        self.env.cr.execute(query)
        result = self.env.cr.fetchall()
        if result:
            return result[0][0]
        else:
            return 0.0

    def get_first_and_last_day_of_year(self, date):
        date = datetime.strptime(date, DATE_FORMAT)
        first_day = datetime(date.year, 1, 1)
        last_day = datetime(date.year, 12, 31)
        return first_day, last_day
    
    def get_last_year_date(self, date):
        date = datetime.strptime(date, DATE_FORMAT)
        if  date.month == 2 and date.day == 29:
            date = date.replace(day=date.day - 1)
        last_year_date = date.replace(year=date.year - 1)
        last_year_date = last_year_date.strftime(DATE_FORMAT)

        return last_year_date
    
    def convert_negative_number_to_absolute_with_parentheses(self,number):
        if number < 0:
            return f"({abs(number)})"
        else:
            return str(number)
        
    def get_percent_variation(self, actual_amount, last_year_amount):
        if last_year_amount == 0:
            return 0
        else:
            percent = ((actual_amount - last_year_amount) * 100 / last_year_amount)
            return round(percent)
        
    def _verify_account_type(self, account_code,amount):
        if account_code.startswith('4'):
            amount = amount * -1
        return amount
    
    def _get_annexes_name(self, xml_id):
        annexe = self.env.ref(xml_id).name
        return annexe

    def _update_total_for_category(self, lines, dict_total):
        categorys = ['B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P','Q','R','S','T','U','V','W','X','Y','Z','AA']
        for line in lines:
            if line['id'] in categorys:
                line['columns'][2] = self._get_dict_number_rounded_thousnad_format(dict_total[line['id']][0], 2)
                line['columns'][4] = self._get_dict_number_rounded_thousnad_format(dict_total[line['id']][1], 2)
                line['columns'][6] = self._get_dict_number_rounded_thousnad_format(dict_total[line['id']][2], 2)
                line['columns'][7]['name'] = dict_total[line['id']][3]


    def _update_total_for_partial_category(self, lines, dict_total):
        categorys = ['commercial_services', 'statement_licenses', 'commissions', 'rent_equipment']
        for line in lines:
            if line['id'] in categorys:
                line['columns'][1] = self._get_dict_number_rounded_thousnad_format(dict_total[line['id']][0], 2)
                line['columns'][3] = self._get_dict_number_rounded_thousnad_format(dict_total[line['id']][1], 2)
                line['columns'][5] = self._get_dict_number_rounded_thousnad_format(dict_total[line['id']][2], 2)
                line['columns'][7]['name'] = dict_total[line['id']][3]

    def _get_rounded_formatted_value(self, value):
        rounded_value = round(value/1000)
        return rounded_value

    def _add_accounts_to_partial_lines(self, lines, options, category_name, parent_name):
        accounts_list  =  self.env['account.account'].browse(self._get_accounts_from_detailed_report_configuration(category_name))

        total_amount_selected_date = 0
        total_amount_last_year = 0
        total_amount_variation = 0
        total_percent = 0

        for account in accounts_list:
            actual_date = options['date'].get('date_to')

            amount_selected_date = self._verify_account_type(account.code, self._get_account_total_amount(actual_date, account.id)) 
            amount_last_year = self._verify_account_type(account.code, self._get_account_total_amount(self.get_last_year_date(actual_date), account.id))
            amount_variation = amount_selected_date - amount_last_year
            percent_variation = self.get_percent_variation(amount_selected_date, amount_last_year)
   
            total_amount_selected_date += amount_selected_date
            total_amount_last_year += amount_last_year
            total_amount_variation += amount_variation

            lines.append(
                {'id': 'column_' + str(account.id),
                'name': '',
                'level': 3,
                'parent_id': parent_name,
                'columns' : [
                    {
                        'id': 'account_' + str(account.id),
                        'name': account.code,
                        'level': 3,
                        'columns' : [],
                        'unfoldable': False,
                    },
                    {
                        'id': 'account_total_' + str(account.id),
                        'name': self._get_rounded_formatted_value(amount_selected_date), 'class': 'number', 'raw_value': self._get_rounded_formatted_value(amount_selected_date),
                        'level': 3,
                        'columns' : [],
                        'unfoldable': False,
                    },
                    {},
                    {
                        'id': 'account_total_last_year' + str(account.id),
                        'name': self._get_rounded_formatted_value(amount_last_year), 'class': 'number', 'raw_value': self._get_rounded_formatted_value(amount_last_year),
                        'level': 3,
                        'columns' : [],
                        'unfoldable': False,  
                    },
                    {},
                    {
                        'id': 'account_variation' + str(account.id),
                        'name': self._get_rounded_formatted_value(amount_variation),
                        'level': 3,
                        'columns' : [],
                        'unfoldable': False,  
                    },
                    {},
                    {
                        'id': 'account_variation_percent' + str(account.id),
                        'name': percent_variation,
                        'level': 3,
                        'columns' : [],
                        'unfoldable': False,  
                        
                    }

                ]})
            
        total_percent = self.get_percent_variation(total_amount_selected_date, total_amount_last_year)
        return total_amount_selected_date, total_amount_last_year,  total_amount_variation,  total_percent

    def get_annexe_dict_colum(self, category_name, parent_category_name, annexe_name, level, unfolded=False):
        dict_column = {
            'id': category_name,
            'name': category_name,
            'level': level,
            'columns' : [
                self._get_dict_column(annexe_name, level),
                {},
                self._get_dict_number_format(0, level),
                {},
                self._get_dict_number_format(0, level),
                {},
                self._get_dict_number_format(0, level),
                self._get_dict_number_format(0, level),
                 ],
            'unfoldable': True,
            'parent_id': parent_category_name,
            'unfolded': unfolded,
            }
        return dict_column
    
    def get_parcial_annexe_dict_column(self, category_name, parent_category_name, annexe_name, level):
        dict_parcial_column = {
            'id': category_name,
            'name': '',
            'level': level,
            'columns' : [
                self._get_dict_column(annexe_name, level),
                self._get_dict_number_format(0, level),
                {},
                self._get_dict_number_format(0, level),
                {},
                self._get_dict_number_format(0, level),
                {},
                self._get_dict_number_format(0, level),
                 ],
            'unfoldable': True,
            'parent_id': parent_category_name,
            }
        return dict_parcial_column

    def get_total_annexe_dict_column(self, concept_name, parent_category_name, total_amount_selected_date, total_amount_last_year, total_amount_variation, total_variation_percent, level, second_column_name='', text_align='text-left'):
        dict_total_column = {
            'id': 'total_' + concept_name,
            'name': '',
            'level': level,
            
            'columns' : [
                {
                    'id': 'total_amount' + concept_name,
                    'class': text_align,
                    'name': second_column_name,
                    'level': level,
                    'columns' : [],
                    'unfoldable': False,
                },
                {},
                {
                    'id': 'total_amount_selected_date' + concept_name,
                    'name': self._get_rounded_formatted_value(total_amount_selected_date), 'class': 'number',
                    'level': level,
                    'columns' : [],
                    'unfoldable': False,
                },
                {},
                {
                    'id': 'total_amount_last_year' + concept_name,
                    'name': self._get_rounded_formatted_value(total_amount_last_year), 'class': 'number',
                    'level': level,
                    'columns' : [],
                    'unfoldable': False,
                },
                {},
                {
                    'id': 'total_amount_variation' + concept_name,
                    'name': self._get_rounded_formatted_value(total_amount_variation),
                    'level': level,
                    'columns' : [],
                    'unfoldable': False,
                },
                {
                    'id': 'total_variation_percent' + concept_name,
                    'name': total_variation_percent,
                    'level': level,
                    'columns' : [],
                    'unfoldable': False,
                }
                 ],
            'unfoldable': False,
            'parent_id': parent_category_name,
            }
        return dict_total_column
    
    def get_category_dict_column(self, category_name, annexe_name, level, parent_id=False, first_column_name=''):
        dict_column = {
            'id': category_name,
            'name':  first_column_name,
            'level': level,
            'columns' : [
                self._get_dict_column(annexe_name, level),
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                 ],
            'unfoldable': True,
            'unfolded': True,
            'parent_id': parent_id
            }
        
        return dict_column




    def _write_xlsx(self, lines, workbook, sheet, y_offset, row_y):

        default_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2})
        default_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'})
        level_0_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'font_size': 13, 'bottom': 6, 'font_color': '#666666', 'num_format': CURRENCY_FORMAT})
        level_1_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': False, 'font_size': 13, 'bottom': 1, 'font_color': '#666666', 'num_format': CURRENCY_FORMAT})
        level_2_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': False, 'font_size': 12, 'font_color': '#666666', 'indent': 0})
        level_2_col1_total_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': False, 'font_size': 12, 'font_color': '#666666'})
        level_2_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': False, 'font_size': 12, 'font_color': '#666666', 'num_format': CURRENCY_FORMAT})
        level_3_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 0})
        level_3_col1_total_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': False, 'font_size': 12, 'font_color': '#666666', 'indent': 0})
        level_3_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': CURRENCY_FORMAT})
        level_4_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 0})
        level_4_col1_total_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': False, 'font_size': 12, 'font_color': '#666666', 'indent': 0})
        level_4_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': CURRENCY_FORMAT})
        level_5_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 0})
        level_5_col1_total_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': False, 'font_size': 12, 'font_color': '#666666', 'indent': 0})
        level_5_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': CURRENCY_FORMAT})



        style_map = {
            0: (level_0_style, level_0_style, default_style),
            1: (level_1_style, level_1_style, default_style),
            2: (level_2_style, level_2_col1_style, level_2_col1_total_style),
            3: (level_3_style, level_3_col1_style, level_3_col1_total_style),
            4: (level_4_style, level_4_col1_style, level_4_col1_total_style),
            5: (level_5_style, level_5_col1_style, level_5_col1_total_style),
        }


        for y in range(0, len(lines)):
            level = lines[y].get('level')
            style, col1_style, col1_total_style = style_map.get(level, (default_style, default_col1_style, default_col1_style))

            if lines[y].get('caret_options'):
                style = level_3_style
                col1_style = level_3_col1_style
            elif 'total' in lines[y].get('class', '').split(' '):
                col1_style = col1_total_style

            if lines[y]['level'] != 3:
                #write the first column, with a specific style to manage the indentation
                cell_value = self._get_cell_type_value(lines[y])[1]
                sheet.write(row_y + y_offset, 0, cell_value, col1_style)

                #write all the remaining cells
                for x in range(1, len(lines[y]['columns']) + 1):
                    cell_value = self._get_cell_type_value(lines[y]['columns'][x - 1])[1]
                    sheet.write(row_y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, style)
                row_y += 1

    def _generate_header_rows(self, options, dict_extra_header, dict_column_name, sheet, y_offset, title_style):
        for row in self.get_header(options):
            x = 0
            for column in row:
                colspan = column.get('colspan', 1)
                header_label = column.get('name', '').replace('<br/>', ' ').replace('&nbsp;', ' ')
                extra_header = dict_extra_header.get(str(x), [' ',0])
                column_header = dict_column_name.get(str(x), '')
                if extra_header[1] > 1:
                    sheet.merge_range(y_offset-1,x,y_offset-1,x + extra_header[1]-1,extra_header[0],title_style)

                if x >= 2 or x <= 5:
                    sheet.write(y_offset, x , column_header, title_style)

                if colspan == 1 and x >= 6:
                    sheet.write(y_offset, x, header_label, title_style)
                x += colspan
            y_offset += 1
        return y_offset