# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import base64
import io
import logging
import math
from datetime import datetime, timedelta
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError,UserError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import re
from odoo.tools.misc import ustr
import pytz

from odoo.tools.profiler import profile

DATE_FORMAT = '%Y-%m-%d'
EXPENDITURE_BUDGET = 'expenditure.budget'
EXPENDITURE_BUDGET_LINE = 'expenditure.budget.line'
PROGRAM_CODE = 'program.code'
YEAR_CONFIGURATION = 'year.configuration'

QUERY_CONTROL_LINE = """
update control_assigned_amounts_lines set
    available = coalesce(available,0)::numeric - %s::numeric,
    write_date = now()
where program_code_id = %s
and extract(quarter from start_date) = %s
returning 1;
"""
QUERY_EXPENDITURE_BUDGET_LINE = """
update expenditure_budget_line set
    available = coalesce(available,0)::numeric - %s::numeric,
    write_date = now()
where id = %s
"""

class ExpenditureBudget(models.Model):
    _name = EXPENDITURE_BUDGET
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Expenditure Budget'
    _rec_name = 'name'

    @api.depends('success_line_ids','success_line_ids.imported','success_line_ids.state','state')
    def _get_count(self):
        for record in self:
            record.record_number = len(record.success_line_ids)
            record.import_record_number = len(
                record.success_line_ids.filtered(lambda l: l.imported == True))

    # Fields For Header
    name = fields.Text(string='Budget name', required=True, tracking=True,
                       states={'validate': [('readonly', True)]})

    _sql_constraints = [
        ('uniq_budget_name', 'unique(name)',
         'The Budget name must be unique!'),
    ]

    user_id = fields.Many2one('res.users', string='Responsible',
                              default=lambda self: self.env.user, tracking=True,
                              states={'validate': [('readonly', True)]})

    # Date Periods
    from_date = fields.Date(string='From', states={
        'validate': [('readonly', True)]}, tracking=True)
    to_date = fields.Date(string='To', states={
        'validate': [('readonly', True)]}, tracking=True)
    
    fiscal_year = fields.Many2one('account.fiscal.year', string = 'Fiscal Year', compute='_compute_fiscal_year', store=True)
    file = fields.Binary(string='File')
    filename = fields.Char(string='File name')
    
    @api.depends('total_budget_validate','state','success_line_ids','success_line_ids.authorized')    
    def _compute_total_budget(self):
        for budget in self:
            if budget.state in ('validate', 'done'):
                budget.total_budget = budget.total_budget_validate
            else:
                budget.total_budget = sum(
                    budget.success_line_ids.mapped('authorized'))

    total_budget = fields.Float(
        string='Total budget', tracking=True, compute="_compute_total_budget",store=True)
    total_budget_validate = fields.Float(string='Total budget', copy=False)

    record_number = fields.Integer(
        string='Number of records', compute='_get_count',store=True)
    import_record_number = fields.Integer(
        string='Number of imported records', readonly=True, compute='_get_count',store=True)

    @api.depends('success_line_ids','success_line_ids.start_date','success_line_ids.end_date','success_line_ids.assigned','success_line_ids.state','state')
    def _compute_total_quarter_budget(self):
        for budget in self:
            total_quarter_budget = 0
            for line in budget.success_line_ids:
                if line.start_date and line.start_date.day == 1 and line.start_date.month == 1 and line.end_date and line.end_date.month == 3:
                    total_quarter_budget += line.assigned
            budget.total_quarter_budget = total_quarter_budget

    total_quarter_budget = fields.Float(
        string='Total 1st Quarter', tracking=True, compute="_compute_total_quarter_budget",store=True)
    journal_id = fields.Many2one('account.journal')
    move_line_ids = fields.One2many('account.move.line', 'budget_id', string="Journal Items")

    @api.depends('from_date', 'to_date')
    def _compute_fiscal_year(self):
        for record in self:
            if record.from_date and record.to_date:
                fiscal_year_obj = self.env['account.fiscal.year']
                fiscal_year = fiscal_year_obj.search([
                    ('date_from', '<=', record.from_date),
                    ('date_to', '>=', record.to_date)
                ])
                record.fiscal_year = fiscal_year and fiscal_year[0] or False

    @api.model
    def fields_get(self, fields=None, attributes=None):
        fields = super(ExpenditureBudget, self).fields_get(fields, attributes=attributes)
        for key, value in fields.items():
            value.update({'searchable': False, 'sortable': False})
        return fields

    @api.model
    def default_get(self, fields):
        res = super(ExpenditureBudget, self).default_get(fields)
        budget_app_jou = self.env.ref('jt_conac.budget_appr_jour')
        if budget_app_jou:
            res.update({'journal_id': budget_app_jou.id})
        return res

    @api.depends('line_ids','line_ids.state','success_line_ids','success_line_ids.state','state')
    def _get_imported_lines_count(self):
        for record in self:
            record.imported_lines_count = len(record.line_ids)
            record.success_lines_count = len(record.success_line_ids)

    @api.depends('success_line_ids', 'success_line_ids.assigned', 'success_line_ids.authorized',
                 'success_line_ids.available')
    def _compute_amt_total(self):
        """
        This function will count the total of all success rows
        :return:
        """
        amt_data = self.env[EXPENDITURE_BUDGET_LINE].read_group(domain=[('expenditure_budget_id', 'in',self.ids), ('state', '=', 'success'), ('assigned', '>', '0'), ('available', '>', '0')],
                                                                  fields=['expenditure_budget_id','assigned', 'authorized', 'available' ],
                                                                  groupby=['expenditure_budget_id'])
        
        mapped_data = dict([(m['expenditure_budget_id'][0], (m['assigned'], m['authorized'], m['available']))
        for m in amt_data])


        for budget in self:
            budget.assigned_total = mapped_data.get(budget.id, 0) and mapped_data[budget.id][0]
            budget.authorised_total = mapped_data.get(budget.id, 0) and mapped_data[budget.id][1]
            budget.available_total = mapped_data.get(budget.id, 0) and mapped_data[budget.id][2]
        
#         for budget in self:
#             assigned_total = 0
#             authorised_total = 0
#             available_total = 0
#             for line in budget.success_line_ids:
#                 assigned_total += line.assigned
#                 authorised_total += line.authorized
#                 available_total += line.available
# 
#             budget.assigned_total = assigned_total
#             budget.authorised_total = authorised_total
#             budget.available_total = available_total
# 

    assigned_total = fields.Float("Assigned Total", tracking=True, compute="_compute_amt_total", store=True)
    authorised_total = fields.Float("Authorised Total", tracking=True, compute="_compute_amt_total", store=True)
    available_total = fields.Float("Available Total", tracking=True, compute="_compute_amt_total", store=True)
    # Budget Lines
    line_ids = fields.One2many(
        EXPENDITURE_BUDGET_LINE, 'expenditure_budget_id',
        string='Expenditure Budget Lines', states={'validate': [('readonly', True)]},
        domain=[('state', '!=', 'success')])
    success_line_ids = fields.One2many(
        EXPENDITURE_BUDGET_LINE, 'expenditure_budget_id',
        string='Expenditure Budget Lines', domain=[('state', '=', 'success')])

    state = fields.Selection([
        ('draft', 'Draft'),
        ('previous', 'Previous'),
        ('confirm', 'Confirm'),
        ('validate', 'Validate'),
        ('done', 'Done')], default='draft', required=True, string='State', tracking=True)

    imported_lines_count = fields.Integer(
        string='Imported Lines', compute='_get_imported_lines_count',store=True)
    success_lines_count = fields.Integer(
        string='Success Lines', compute='_get_imported_lines_count',store=True)

    @api.depends('line_ids','line_ids.state','success_line_ids','success_line_ids.state','state')
    def _compute_total_rows(self):
        for budget in self:
            budget.draft_rows = self.env[EXPENDITURE_BUDGET_LINE].search_count(
                [('expenditure_budget_id', '=', budget.id), ('state', 'in', ['draft', 'manual'])])
            budget.failed_rows = self.env[EXPENDITURE_BUDGET_LINE].search_count(
                [('expenditure_budget_id', '=', budget.id), ('state', '=', 'fail')])
            budget.success_rows = self.env[EXPENDITURE_BUDGET_LINE].search_count(
                [('expenditure_budget_id', '=', budget.id), ('state', '=', 'success')])
            budget.total_rows = self.env[EXPENDITURE_BUDGET_LINE].search_count(
                [('expenditure_budget_id', '=', budget.id)])

    # Rows Progress Tracking Details
    cron_running = fields.Boolean(string='Running CRON?')
    import_status = fields.Selection([
        ('draft', 'Draft'),
        ('in_progress', 'In Progress'),
        ('done', 'Completed')], default='draft', copy=False)
    failed_row_file = fields.Binary(string='Failed Rows File')
    fialed_row_filename = fields.Char(
        string='File name', default=lambda self: _("Failed_Rows.txt"))
    draft_rows = fields.Integer(
        string='Failed Rows', compute="_compute_total_rows",store=True)
    failed_rows = fields.Integer(
        string='Failed Rows', compute="_compute_total_rows",store=True)
    success_rows = fields.Integer(
        string='Success Rows', compute="_compute_total_rows",store=True)
    total_rows = fields.Integer(
        string="Total Rows", compute="_compute_total_rows",store=True)

    is_validation_process_start = fields.Boolean(default=False)
    validation_process_start_user = fields.Many2one('res.users','Users')
     
    @api.constrains('from_date', 'to_date')
    def _check_dates(self):
        if self.from_date and self.to_date:
            if self.from_date > self.to_date:
                raise ValidationError("Please select correct date")
            if self.from_date.year != self.to_date.year:
                raise ValidationError("Start date and End date must be related to same year.")

    def import_lines(self):
        ctx = self.env.context.copy()
        if self._context.get('reimport'):
            ctx['reimport'] = True
        return {
            'name': _("Import Budget Lines"),
            'type': 'ir.actions.act_window',
            'res_model': 'import.line',
            'view_mode': 'form',
            'view_type': 'form',
            'views': [(False, 'form')],
            'target': 'new',
            'context': ctx,
        }

    def send_notification_msg(self, user, failed, successed):
        ch_obj = self.env['mail.channel']
        base_user = self.env.ref('base.user_root')
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        url = base_url + '/web#id=%s&view_type=form&model=expenditure.budget' % (self.id)
        body = (_("Budget Validation Process is Completed for \
                    <a href='%s' target='new'>%s</a>") % (url, self.name))
        body += (_("<ul><li>Total Successed Lines: %s </li>\
            <li>Total Failed Lines: %s </li></ul>") % (str(successed), str(failed)))
        if user:
            ch = ch_obj.sudo().search([('name', '=', str(base_user.name + ', ' + user.name)),
                                       ('channel_type', '=', 'chat')], limit=1)
            if not ch:
                ch = ch_obj.create({
                    'name': 'OdooBot, ' + user.name,
                    'public': 'private',
                    'channel_type': 'chat',
                    'channel_last_seen_partner_ids': [(0, 0, {'partner_id': user.partner_id.id,
                                                              'partner_email': user.partner_id.email}),
                                                      (0, 0, {'partner_id': base_user.partner_id.id,
                                                              'partner_email': base_user.partner_id.email})
                                                      ]
                })
            ch.message_post(attachment_ids=[], body=body, content_subtype='html',
                            message_type='comment', partner_ids=[], subtype='mail.mt_comment',
                            email_from=base_user.partner_id.email, author_id=base_user.partner_id.id)
        return True

    def check_year_exist(self, line):

        if len(str(line.year)) > 3:
            year_str = str(line.year)[:4]
            if year_str.isnumeric():
                year_obj = self.env[YEAR_CONFIGURATION].search_read([], fields=['id', 'name'])
                if not list(filter(lambda yr: yr['name'] == year_str, year_obj)):
                    self.env[YEAR_CONFIGURATION].create({'name': year_str}).id
        else:
            raise ValidationError('Invalid Year Format Of line one!')

    def get_line_vals_list(self,line):
        line_vals = [line.year, line.program, line.subprogram, line.dependency, line.subdependency, line.item,
                     line.dv, line.origin_resource, line.ai, line.conversion_program,
                     line.departure_conversion, line.expense_type, line.location, line.portfolio,
                     line.project_type, line.project_number, line.stage, line.agreement_type,
                     line.agreement_number, line.exercise_type]
        return line_vals

    def create_standarization_lines(self):
        data = base64.decodestring(self.file)
        #file = open(data)
        #lines = file.readlines()
        info = data.decode('utf-8')
        #.split('\\r\\n')
        #logging.critical("####Archivo"+str(info))
        lines = info.splitlines()

        for line in lines:

            line_split=line.split(",")

            program_code = self.env[PROGRAM_CODE].search([('program_code', '=', line_split[0])])
            
            if program_code:
                for x in range(4):
                    if x==0:
                        date_start = line_split[0][0:4]+'-01-01'
                        date_end = line_split[0][0:4]+'-03-31'
                        amount = float(line_split[1])
                    if x==1:
                        date_start = line_split[0][0:4]+'-04-01'
                        date_end = line_split[0][0:4]+'-06-30'
                        amount = float(line_split[2])
                    if x==2:
                        date_start = line_split[0][0:4]+'-07-01'
                        date_end = line_split[0][0:4]+'-09-30'
                        amount = float(line_split[3])
                    if x==3:
                        date_start = line_split[0][0:4]+'-10-01'
                        date_end = line_split[0][0:4]+'-12-31'
                        amount = float(line_split[4])
                    stand_vals = {
                            'expenditure_budget_id': self.id,
                            'program_code_id': program_code[0]['id'],
                            'start_date': date_start,
                            'end_date': date_end,
                            'authorized': amount,
                            'state': 'success'

                        }
                    stand_id = self.env[EXPENDITURE_BUDGET_LINE].create(stand_vals)

                



    def validate_and_add_budget_line(self):
        if len(self.line_ids.ids) > 0:
            failed_row = ""
            tz = pytz.timezone('Etc/GMT+6')

            budget_line_obj = self.env[EXPENDITURE_BUDGET_LINE]

            year_obj = self.env[YEAR_CONFIGURATION].search_read([], fields=['id', 'name'])
            year_dict = {y['name']: y['id'] for y in year_obj}

            program_obj = self.env['program'].search_read(
                [
                    '&',('program_key_id','!=',False),
                        '&', ('start_date', '<=', self.from_date),
                            '|', ('end_date', '=', False), ('end_date', '>=', self.from_date)

                ],
                fields=['id', 'key_unam']
            )
            program_dict = {p['key_unam']: p['id'] for p in program_obj}

            item_obj = self.env['expenditure.item'].search_read([], fields=['id', 'item', 'exercise_type'])
            item_dict = {i['item']: i['id'] for i in item_obj}

            dv_obj = self.env['verifying.digit']

            dependency_obj = self.env['dependency'].search_read(
                [
                    '&', ('start_date', '<=', self.from_date),
                            '|', ('end_date', '=', False), ('end_date', '>=', self.from_date)
                ], fields=['id', 'dependency'])

            dependency_dict = {d['dependency']: d['id'] for d in dependency_obj}

            sub_dependency_obj = self.env['sub.dependency'].search_read(
                [
                    '&', ('start_date', '<=', self.from_date),
                            '|', ('end_date', '=', False), ('end_date', '>=', self.from_date)
                ],
                fields=['id', 'dependency_id', 'sub_dependency']
            )
            sub_dependency_dict = {(d['dependency_id'][0], d['sub_dependency']): d['id'] for d in sub_dependency_obj}

            sub_program_obj = self.env['sub.program'].search_read(
                [
                    '&',('dependency_id', '!=', False),
                    '&',('sub_dependency_id', '!=', False),
                    '&',('unam_key_id', '!=', False),
                    '&',('geographic_location_id', '!=', False),
                        '&', ('start_date', '<=', self.from_date),
                            '|', ('end_date', '=', False), ('end_date', '>=', self.from_date)
                ],
                fields=['id', 'dependency_id', 'sub_dependency_id', 'unam_key_id', 'sub_program', 'geographic_location_id']
            )
            sub_program_dict = {(s['unam_key_id'][0], s['sub_program'], s['dependency_id'][0], s['sub_dependency_id'][0]) : s['id'] for s in sub_program_obj}

            origin_obj = self.env['resource.origin'].search_read(
                [
                    '&', ('start_date', '<=', self.from_date),
                            '|', ('end_date', '=', False), ('end_date', '>=', self.from_date)
                ], fields=['id', 'key_origin'])

            origin_dict = {o['key_origin']: o['id'] for o in origin_obj}

            activity_obj = self.env['institutional.activity'].search_read(
                [
                    '&', ('start_date', '<=', self.from_date),
                            '|', ('end_date', '=', False), ('end_date', '>=', self.from_date)
                ], fields=['id', 'number'])

            activity_dict = {a['number']: a['id'] for a in activity_obj}

            conpa_obj = self.env['departure.conversion'].search_read(
                [
                    '&', ('item_id', '!=', False),
                        '&', ('start_date', '<=', self.from_date),
                            '|', ('end_date', '=', False), ('end_date', '>=', self.from_date)
                ],
                fields=['id', 'item_id', 'federal_part']
            )
            conpa_dict = {(c['item_id'][0], c['federal_part']): c['id'] for c in conpa_obj}

            conpp_obj = self.env['budget.program.conversion'].search_read(
                [
                    '&', ('program_key_id', '!=', False),
                    '&', ('conversion_key_id', '!=', False),
                    '&', ('shcp', '!=', False),
                        '&', ('start_date', '<=', self.from_date),
                            '|', ('end_date', '=', False), ('end_date', '>=', self.from_date)
                ],
                fields=['id', 'program_key_id', 'conversion_key_id', 'shcp']
            )
            conpp_dict = {(c['program_key_id'][1], c['conversion_key_id'][1], c['shcp'][1]): c['id'] for c in conpp_obj}

            expense_type_obj = self.env['expense.type'].search_read(
                [
                    '&', ('start_date', '<=', self.from_date),
                        '|', ('end_date', '=', False), ('end_date', '>=', self.from_date)
                ], fields=['id', 'key_expenditure_type'])
            expense_type_dict = {e['key_expenditure_type']: e['id'] for e in expense_type_obj}

            geo_location_obj = self.env['geographic.location'].search_read(
                [
                    '&', ('start_date', '<=', self.from_date),
                        '|', ('end_date', '=', False), ('end_date', '>=', self.from_date)
                ], fields=['id', 'state_key'])
            geo_location_dict = {l['state_key']: l['id'] for l in geo_location_obj}

            wallet_obj = self.env['key.wallet'].search_read(
                [
                    '&', ('start_date', '<=', self.from_date),
                            '|', ('end_date', '=', False), ('end_date', '>=', self.from_date)
                ], fields=['id', 'wallet_password'])
            wallet_dict = {w['wallet_password']: w['id'] for w in wallet_obj}

            project_obj = self.env['project.project'].search_read(
                [('number', '!=', False)],
                fields=['id', 'number', 'custom_project_type_id', 'custom_stage_id']
            )
            project_dict = {(p['number'], p['custom_project_type_id'][1], p['custom_stage_id'][1]): p['id'] for p in project_obj}

            project_type_obj = self.env['project.type'].search_read([], fields=['id', 'project_id'])
            project_type_dict = {p['project_id'][0]: p['id'] for p in project_type_obj}

            stage_obj = self.env['stage'].search_read([], fields=['id', 'project_id'])
            stage_dict = {s['project_id'][0]: s['id'] for s in stage_obj}

            agreement_obj = self.env['agreement.type'].search_read(
                [],
                fields=['id', 'project_id', 'agreement_type', 'number_agreement']
            )
            agreement_dict = {(a['project_id'][0], a['agreement_type'], a['number_agreement']): a['id'] for a in agreement_obj}

            program_code_obj = self.env[PROGRAM_CODE]

            validated_rows = []
            while True:
                # Se particionan las lineas en sets de 500 registros para
                tmp_lines = budget_line_obj.search(
                    [
                        ('id','not in',validated_rows),
                        ('expenditure_budget_id','=',self.id),
                        ('state','!=','success')
                    ]
                )
                querys = []
                for line in tmp_lines:
                    # Para cada linea de presupuesto se realiza la validación y
                    # creación de los codigos programaticos
                    line_str = self.get_line_vals_list(line)
                    validated_rows.append(line.id)
                    if line.state == 'manual' or line.program_code_id:
                        # Validation Importe 1a Asignacion
                        try:
                            asigned_amount = float(line.assigned)
                            if asigned_amount < 0:
                                failed_row += str(line_str) + \
                                        _("------>> Assigned Amount should be greater than or 0!")
                                line.state = 'fail'
                                continue
                        except:
                            failed_row += str(line_str) + \
                                        _("------>> Invalid Asigned Amount Format")
                            line.state = 'fail'
                            continue

                        # Validation Authorized Amount
                        try:
                            authorized_amount = float(line.authorized)
                            if authorized_amount < 0:
                                failed_row += str(line_str) + \
                                        _("------>> Authorized Amount should be greater than 0!")
                                line.state = 'fail'
                                continue
                        except:
                            failed_row += str(line_str) + \
                                        _("------>> Invalid Authorized Amount Format")
                            line.state = 'fail'
                            continue
                        line.state = 'success'

                    if line.state in ['fail', 'draft']:

                        # Check Start and End Date
                        if not line.start_date:
                            failed_row += str(line_str) + \
                                        _("------>> Please Add Start Date\n")
                            line.state = 'fail'
                            continue

                        if not line.end_date:
                            failed_row += str(line_str) + \
                                        _("------>> Please Add End Date\n")
                            line.state = 'fail'
                            continue

                        if line.start_date and line.end_date \
                                and self.from_date.year != line.start_date.year \
                                or self.to_date.year != line.end_date.year:
                            failed_row += str(line_str) + \
                                        _("------>> Start date and End date must be related to same year of budget date.\n")
                            line.state = 'fail'
                            continue

                        # Validate year format
                        year = None
                        if len(str(line.year)) > 3:
                            year_str = str(line.year)[:4]
                            if year_str.isnumeric():
                                year = year_dict.get(year_str, None)
                                if not year and not self._context.get('from_adequacies'):
                                    year = self.env[YEAR_CONFIGURATION].create({'name': year_str}).id
                                    year_dict[year_str] = year
                        if not year:
                            failed_row += str(line_str) + \
                                        _("------>> Invalid Year Format\n")
                            line.state = 'fail'
                            continue

                        # Validate Program(PR)
                        program_str = str(line.program).zfill(2)
                        program = program_dict.get(program_str, None)
                        if not program:
                            failed_row += str(line_str) + \
                                        _("------>> Invalid Program(PR) Format, check the validity\n")
                            line.state = 'fail'
                            continue

                        # Validate Dependency
                        dependency_str = str(line.dependency).zfill(3)
                        dependency = dependency_dict.get(dependency_str, None)
                        if not dependency:
                            failed_row += str(line_str) + \
                                        _("------>> Invalid Dependency(DEP) Format, check the validity\n")
                            line.state = 'fail'
                            continue

                        # Validate Sub-Dependency
                        sub_dependency_str = str(line.subdependency).zfill(2)
                        sub_dependency = sub_dependency_dict.get((dependency, sub_dependency_str), None)
                        if not sub_dependency:
                            failed_row += str(line_str) + \
                                        _("------>> Invalid Sub Dependency(SUB-DEP) Format, check the validity\n")
                            line.state = 'fail'
                            continue

                        # Validate Sub-Program
                        sub_program_str = str(line.subprogram).zfill(2)
                        sub_program = sub_program_dict.get((program, sub_program_str, dependency, sub_dependency), None)
                        if not sub_program:
                            failed_row += str(line_str) + \
                                        _("------>> Invalid SubProgram(SP) Format, check the validity\n")
                            line.state = 'fail'
                            continue

                        # Validate Item
                        item_str = str(line.item).zfill(3)
                        item = item_dict.get(item_str, None)
                        if not item:
                            failed_row += str(line_str) + \
                                        _("------>> Invalid Expense Item(PAR) Format, check the validity\n")
                            line.state = 'fail'
                            continue

                        # Validate DV
                        dv = line.dv
                        if not dv:
                            failed_row += str(line_str) + \
                                        _("------>> Digito Verificador is not added!\n")
                            line.state = 'fail'
                            continue
                        dv_check = dv_obj.check_digit_from_values(
                            program_str,
                            sub_program_str,
                            dependency_str,
                            sub_dependency_str,
                            item_str
                        )
                        if dv != dv_check:
                            failed_row += str(line_str) + \
                                    _("------>> Digito Verificador is not matched! \n")
                            line.state = 'fail'
                            continue

                        # Validate Origin Of Resource
                        origin_resource_str = str(line.origin_resource).zfill(2)
                        origin_resource = origin_dict.get(origin_resource_str, None)
                        if not origin_resource:
                            failed_row += str(line_str) + \
                                        _("------>> Invalid Origin Of Resource(OR) Format, check the validity\n")
                            line.state = 'fail'
                            continue

                        # Validation Institutional Activity Number
                        institutional_activity_str = str(line.ai).zfill(5)
                        institutional_activity = activity_dict.get(institutional_activity_str, None)
                        if not institutional_activity:
                            failed_row += str(line_str) + \
                                        _("------>> Invalid Institutional Activity Number(AI) Format, check the validity\n")
                            line.state = 'fail'
                            continue

                        # Validation CONPA
                        conpa_str = str(line.departure_conversion).zfill(5)
                        conpa = conpa_dict.get((item, conpa_str), None)
                        if not conpa:
                            failed_row += str(line_str) + \
                                        _("------>> Invalid SHCP Games(CONPA) Format, check the validity\n")
                            line.state = 'fail'
                            continue

                        # Validation CONPP
                        conpp_str = str(line.conversion_program).zfill(4)
                        conpp = conpp_dict.get((program_str[0], conpa_str, conpp_str), None)
                        if not conpp:
                            failed_row += str(line_str) + \
                                        _("------>> Invalid Conversion Program SHCP(CONPP) Format, check the validity\n")
                            line.state = 'fail'
                            continue

                        # Validation Expense Type
                        expense_type_str = str(line.expense_type).zfill(2)
                        expense_type = expense_type_dict.get(expense_type_str, None)
                        if not expense_type:
                            failed_row += str(line_str) + \
                                        _("------>> Invalid Expense Type(TG) Format, check the validity\n")
                            line.state = 'fail'
                            continue

                        # Validation Geographic Location
                        geo_location_str = str(line.location).zfill(2)
                        geo_location = geo_location_dict.get(geo_location_str, None)
                        if not geo_location:
                            failed_row += str(line_str) + \
                                        _("------>> Invalid Geographic Location (UG) Format, check the validity\n")
                            line.state = 'fail'
                            continue

                        # Validation Wallet Key
                        wallet_key_str = str(line.portfolio).zfill(4)
                        wallet_key = wallet_dict.get(wallet_key_str, None)
                        if not wallet_key:
                            failed_row += str(line_str) + \
                                        _("------>> Invalid Wallet Key(CC) Format, check the validity\n")
                            line.state = 'fail'
                            continue

                        # Validate Project
                        project_type_custom_str = str(line.project_type).zfill(2)
                        project_number_custom_str = str(line.project_number).zfill(6)
                        custom_stage_str = str(line.stage).zfill(2)
                        project = project_dict.get((project_number_custom_str, project_type_custom_str, custom_stage_str), None)
                        if not project:
                            failed_row += str(line_str) + \
                                        _("------>> Invalid Project\n")
                            line.state = 'fail'
                            continue

                        # Validate Project Type
                        project_type = project_type_dict.get(project, None)
                        if not project_type:
                            failed_row += str(line_str) + \
                                        _("------>> Invalid Project Type(TP) Format\n")
                            line.state = 'fail'
                            continue

                        # Validate Stage
                        stage = stage_dict.get(project, None)
                        if not stage:
                            failed_row += str(line_str) + \
                                        _("------>> Invalid Stage(E) Format\n")
                            line.state = 'fail'
                            continue

                        # Validate Agreement Type
                        agreement_type_str = str(line.agreement_type).zfill(2)
                        agreement_number_str = str(line.agreement_number).zfill(6)
                        agreement_type = agreement_dict.get((project, agreement_type_str, agreement_number_str), None)
                        if not agreement_type:
                            failed_row += str(line_str) + \
                                        _("------>> Invalid Agreement Type(TC) Format\n")
                            line.state = 'fail'
                            continue

                        #Validate conversion_resource_origin
                        resource_origin_validation = program_code_obj.validate_conversion_origin_resource(item_str, sub_program_str, origin_resource_str, self.from_date)
                        if resource_origin_validation==False:
                            failed_row += str(line_str) + \
                                        _("------>> Origen del recurso inválido o no vigente de acuerdo a la regla OR-SP-PAR\n")
                            line.state = 'fail'
                            continue

                        #Validate validate_conversion_activity_institutional
                        ai_validation = program_code_obj.validate_conversion_activity_institutional(program_str, self.from_date)
                        if ai_validation==False:
                            failed_row += str(line_str) + \
                                        _("------>> Actividad Intitucional inválida o no vigente de acuerdo a la regla AI-PR\n")
                            line.state = 'fail'
                            continue

                        #Validate validate_conpp_conversion
                        conpp_validation = program_code_obj.validate_conpp_conversion(program_str, sub_program_str, dependency_str, sub_dependency_str, item_str, self.from_date)
                        if conpp_validation==False:
                            failed_row += str(line_str) + \
                                        _("------>> CONPP inválido o no vigente de acuerdo a la regla CONPP-PR-SP-PAR\n")
                            line.state = 'fail'
                            continue
                        #Validate validate_type_expense_conversion
                        tp_validation = program_code_obj.validate_type_expense_conversion(item_str, self.from_date)
                        if tp_validation==False:
                            failed_row += str(line_str) + \
                                        _("------>> Tipo de gasto inválido o no vigente de acuerdo a la regla TP-PAR\n")
                            line.state = 'fail'
                            continue

                        program_code_str = "".join([str(skey) for skey in line_str[:-1]])
                        program_code = program_code_obj.sudo().search([('program_code', '=', program_code_str)],limit=1)

                        if program_code and program_code.state == 'validated':
                            failed_row += str(line_str) + \
                                        _("------>> Duplicated Program Code Found! \n")
                            line.state = 'fail'
                            continue
                        if program_code and program_code.state == 'draft':
                            budget_line = self.env[EXPENDITURE_BUDGET_LINE].search(
                                [
                                    ('program_code_id', '=', program_code.id),
                                    ('start_date', '=', line.start_date),
                                    ('end_date', '=', line.end_date)
                                ], limit=1
                            )
                            if budget_line:
                                failed_row += str(line_str) + \
                                            _("------>> Program Code Already Linked With Budget Line With Selected Start/End Date! \n")
                                line.state = 'fail'
                                continue
                            else:
                                # Se asigna la referencia al código programático con la linea de presupuesto
                                budget_line_query = """
                                    UPDATE expenditure_budget_line SET
                                        program_code_id = %s,
                                        state = 'success'
                                    WHERE id = %s
                                    RETURNING 1
                                """
                                self.env.cr.execute(budget_line_query, (program_code.id, line.id))
                                if not self.env.cr.fetchone():
                                    raise ValidationError("Error al asignar el código programático %s a la línea de presupuesto" % program_code.program_code)
                        if not program_code:
                            program_vals = {
                                'budget': self.id,
                                'year': year,
                                'program_id': program,
                                'sub_program_id': sub_program,
                                'dependency_id': dependency,
                                'sub_dependency_id': sub_dependency,
                                'item_id': item,
                                'check_digit': dv,
                                'resource_origin_id': origin_resource,
                                'institutional_activity_id': institutional_activity,
                                'budget_program_conversion_id': conpp,
                                'conversion_item_id': conpa,
                                'expense_type_id': expense_type,
                                'location_id': geo_location,
                                'portfolio_id': wallet_key,
                                'project_type_id': project_type,
                                'stage_id': stage,
                                'agreement_type_id': agreement_type,
                                'state': 'draft',
                                'project_id' : project,
                                'program_code': program_code_str,
                            }
                            querys.append((program_code_obj.sql_params_program_code(program_vals), line.id))

                # Creación de los codigos programaticos:
                program_code_query = program_code_obj.sql_program_code()
                for q in querys:
                    # Se ejecuta la consulta SQL
                    self.env.cr.execute(program_code_query, q[0][0])
                    program_code_id = self.env.cr.fetchone()[0]
                    budget_line_query = ""
                    if not program_code_id:
                        failed_row += str(self.get_line_vals_list(line)) + \
                                        _("------>> Row Data Are Not Corrected or Duplicated Program Code Found! \n")
                        budget_line_query = """
                            UPDATE expenditure_budget_line SET state = 'fail'
                            WHERE id = %s
                            RETURNING 1
                        """
                        self.env.cr.execute(budget_line_query, (q[1]))
                    else:
                        budget_line_query = """
                            UPDATE expenditure_budget_line SET
                                program_code_id = %s,
                                state = 'success'
                            WHERE id = %s
                            RETURNING 1
                        """
                        self.env.cr.execute(budget_line_query, (program_code_id, q[1]))
                    res = self.env.cr.fetchone()[0]
                    if not res:
                        raise ValidationError("Error al validar las líneas")

                if not budget_line_obj.search([('id','not in',validated_rows),('expenditure_budget_id','=',self.id),
                                            ('state','!=','success')]):
                    break

            if not budget_line_obj.search([('expenditure_budget_id', '=', self.id),('state', '!=', 'success')]):
                self.state = 'previous'
            vals = {}
            if failed_row != "":
                content = "\n"
                content += _("...................Failed Rows ") + \
                        str(datetime.today()) + "...............\n"
                content += str(failed_row)
                failed_data = base64.b64encode(content.encode('utf-8'))
                vals['failed_row_file'] = failed_data
            else:
                vals['failed_row_file'] = None
            msg = (_("Budget Validation Process Ended at %s") % datetime.strftime(
                datetime.now(tz), DEFAULT_SERVER_DATETIME_FORMAT))
            self.env['mail.message'].create(
                {'model': EXPENDITURE_BUDGET, 'res_id': self.id,'body': msg}
            )
            self.write(vals)


    def remove_cron_records(self):
        crons = self.env['ir.cron'].sudo().search(
            [('model_id', '=', self.env.ref('jt_budget_mgmt.model_expenditure_budget').id)])
        for cron in crons:
            if cron.budget_id and not cron.budget_id.cron_running:
                try:
                    cron.sudo().unlink()
                except Exception as e:
                    raise ValidationError(e)

    def verify_data(self):
        total = sum(self.success_line_ids.mapped('authorized'))
        if total <= 0:
            raise ValidationError("Budget amount should be greater than 0")
        if len(self.success_line_ids.ids) == 0:
            raise ValidationError("Please correct failed rows")
        # if self.total_rows > 0 and self.success_rows != self.total_rows:
        #     raise ValidationError("Please correct failed rows")
        return True
    
    def start_again_previous_budget(self):
        if self.validation_process_start_user and self.validation_process_start_user.id != self.env.user.id:
            raise ValidationError("Budget Validation Process Is Started By Other Users!")
        self.is_validation_process_start = False
        self.validation_process_start_user = False
        self.previous_budget()
        
    def previous_budget(self):
        # Total CRON to create
        if self.state == 'previous':
            raise ValidationError("Budget already validated.Please reload the page!")
        # if self.is_validation_process_start:
        #     raise ValidationError("Budget Validation Process Is Started By Other Users!")
        # self.is_validation_process_start = True
        # self.validation_process_start_user = self.env.user.id
        # self.env.cr.commit()
        tz = pytz.timezone('Etc/GMT+6')
        try:
            msg = (_("Budget Validation Process Started at %s") % datetime.strftime(
                datetime.now(tz), DEFAULT_SERVER_DATETIME_FORMAT))
            self.env['mail.message'].create({'model': 'expenditure.budget', 'res_id': self.id,
                                            'body': msg})
            if self.success_rows != self.total_rows:
                self.validate_and_add_budget_line()
                total_lines = len(self.success_line_ids.filtered(
                    lambda l: l.state == 'success'))
    
                if total_lines == self.total_rows:
                    self.state = 'previous'
                    msg = (_("Budget Validation Process Ended at %s") % datetime.strftime(
                        datetime.now(tz), DEFAULT_SERVER_DATETIME_FORMAT))
                        
        except ValueError as e:
            self.is_validation_process_start = False
            self.validation_process_start_user = False
            self.env.cr.commit()            
            raise ValidationError(_("%s")% (ustr(e)))
                
        except ValidationError as e:
            self.is_validation_process_start = False
            self.validation_process_start_user = False
            self.env.cr.commit()            
            raise ValidationError(_("%s")% (ustr(e)))
            
        except UserError as e:
            self.is_validation_process_start = False
            self.validation_process_start_user = False
            self.env.cr.commit()            
            raise ValidationError(_("%s")% (ustr(e)))

        self.is_validation_process_start = False
        self.validation_process_start_user = False

    def confirm(self):
        self.verify_data()
        if self.file:
            self.create_standarization_lines()
        self.write({'state': 'confirm'})

    def approve(self):
        if self.state == 'validate':
            raise ValidationError("Budget already validated.Please reload the page!")
        self.write({'state': 'validate'})
        self.total_budget_validate = sum(self.success_line_ids.filtered(lambda x: x.start_date==x.expenditure_budget_id.from_date and x.end_date==x.expenditure_budget_id.to_date).mapped('authorized'))

        self.verify_data()
        if self.journal_id:
            move_obj = self.env['account.move']
            journal = self.journal_id
            today = datetime.today().date()
            budget_id = self.id
            user = self.env.user
            partner_id = user.partner_id.id
            amount = sum(self.success_line_ids.filtered(lambda x: x.start_date==x.expenditure_budget_id.from_date and x.end_date==x.expenditure_budget_id.to_date).mapped('authorized'))
            company_id = user.company_id.id
            if not journal.default_debit_account_id or not journal.default_credit_account_id \
                    or not journal.conac_debit_account_id or not journal.conac_credit_account_id:
                if self.env.user.lang == 'es_MX':
                    raise ValidationError(_("Por favor configure la cuenta UNAM y CONAC en diario!"))
                else:
                    raise ValidationError(_("Please configure UNAM and CONAC account in budget journal!"))
            unam_move_val = {'ref': self.name, 'budget_id': budget_id, 'conac_move': True, 'type': 'entry',
                             'date': today, 'journal_id': journal.id, 'company_id': company_id,
                             'line_ids': [(0, 0, {
                                 'account_id': journal.default_credit_account_id.id,
                                 'coa_conac_id': journal.conac_credit_account_id.id,
                                 'credit': amount, 'budget_id': budget_id,
                                 'partner_id': partner_id
                             }), (0, 0, {
                                 'account_id': journal.default_debit_account_id.id,
                                 'coa_conac_id': journal.conac_debit_account_id.id,
                                 'debit': amount, 'budget_id': budget_id,
                                 'partner_id': partner_id
                             })]}
            unam_move = move_obj.create(unam_move_val)
            unam_move.action_post()
        self.success_line_ids.mapped('program_code_id').write(
            {'state': 'validated', 'budget_id': self.id})

    def reject(self):
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'reject',
            'view_mode': 'form',
            'view_type': 'form',
            'views': [(False, 'form')],
            'target': 'new',
        }

    def unlink(self):
        if not self._context.get('from_wizard'):
            for budget in self:
                if budget.state not in ('draft', 'previous'):
                    if self.env.user.lang == 'es_MX':
                        raise ValidationError(
                            'No se puede borrar el presupuesto procesado!')
                    else:
                        raise ValidationError(
                            'You can not delete processed budget!')
        # Borrado de las líneas de presupuesto
        query = "delete from expenditure_budget_line where expenditure_budget_id = %s returning 1"
        self.env.cr.execute(query, (self.id,))
        # Borrado de los códigos programáticos
        query = "delete from program_code where budget_id = %s returning 1"
        self.env.cr.execute(query, (self.id,))
        return super(ExpenditureBudget, self).unlink()

    def show_imported_lines(self):
        action = self.env.ref(
            'jt_budget_mgmt.action_expenditure_budget_imported_line').read()[0]
        action['limit'] = 1000
        action['domain'] = [('id', 'in', self.line_ids.ids)]
        action['search_view_id'] = (self.env.ref(
            'jt_budget_mgmt.expenditure_budget_imported_line_search_view').id,)
        return action

    def show_success_lines(self):
        action = self.env.ref(
            'jt_budget_mgmt.action_expenditure_budget_success_line').read()[0]
        action['limit'] = 1000
        action['domain'] = [('id', 'in', self.success_line_ids.ids)]
        action['search_view_id'] = (self.env.ref(
            'jt_budget_mgmt.expenditure_budget_success_line_search_view').id,)
        return action


class ExpenditureBudgetLine(models.Model):
    _name = EXPENDITURE_BUDGET_LINE
    _description = 'Expenditure Budget Line'
    _rec_name = 'program_code_id'

    expenditure_budget_id = fields.Many2one(
        EXPENDITURE_BUDGET, string='Expenditure Budget', ondelete="cascade")

    start_date = fields.Date(
        string='Start date')
    end_date = fields.Date(
        string='End date')

    authorized = fields.Float(
        string='Authorized')
    assigned = fields.Float(
        string='Assigned')
    available = fields.Float(
        string='Available')
    currency_id = fields.Many2one(
        'res.currency', default=lambda self: self.env.company.currency_id)

    program_code_id = fields.Many2one(PROGRAM_CODE, string='Program code', index=True)
    program_id = fields.Many2one(
        'program', string='Program', related="program_code_id.program_id")
    dependency_id = fields.Many2one(
        'dependency', string='Dependency', related="program_code_id.dependency_id")
    sub_dependency_id = fields.Many2one(
        'sub.dependency', string='Sub-Dependency', related="program_code_id.sub_dependency_id")
    item_id = fields.Many2one(
        'expenditure.item', string='Item', related="program_code_id.item_id")
    departure_group_id = fields.Many2one('departure.group', string = 'Departure group', related = 'item_id.departure_group_id', store = True)

    imported = fields.Boolean()
    state = fields.Selection([('manual', 'Manual'), ('draft', 'Draft'), (
        'fail', 'Fail'), ('success', 'Success')], string='Status', default='manual')
    imported_sessional = fields.Boolean()

    # Fields for imported data
    year = fields.Char(string='Year')
    program = fields.Char(string='Program')
    subprogram = fields.Char(string='Sub-Program')
    dependency = fields.Char(string='Dependency')
    subdependency = fields.Char(string='Sub-Dependency')
    item = fields.Char(string='Expense Item')
    dv = fields.Char(string='Digit Verification')
    origin_resource = fields.Char(string='Origin Resource')
    ai = fields.Char(string='Institutional Activity')
    conversion_program = fields.Char(string='Conversion Program')
    departure_conversion = fields.Char(string='Federal Item')
    expense_type = fields.Char(string='Expense Type')
    location = fields.Char(string='State Code')
    portfolio = fields.Char(string='Key portfolio')
    project_type = fields.Char(string='Type of Project')
    project_number = fields.Char(string='Project Number')
    stage = fields.Char(string='Stage Identifier')
    agreement_type = fields.Char(string='Type of Agreement')
    agreement_number = fields.Char(string='Agreement number')
    exercise_type = fields.Char(string='Exercise type')
    cron_id = fields.Many2one('ir.cron', string="CRON ID")
    is_create_from_adequacies = fields.Boolean(string="Line Create From Adequacies",default=False,copy=False)
    new_adequacies_id = fields.Many2one('adequacies',copy=False)

    # Se agregan las siguientes columnas para calcular el presupuesto disponible
    # * Linea de presupuesto anual
    budget_line_yearly_id = fields.Many2one(EXPENDITURE_BUDGET_LINE, string="Yearly budget", required=False, index=True)
    # * Referencia a la tabla replica de los presupuestos
    #budget_available = fields.One2many("expenditure.budget.replica", "budget_line_yearly_id", string="Tests")

    @api.model
    def init(self):
        self._cr.execute("""
            create index if not exists program_code_budget_ix
            on expenditure_budget_line (program_code_id,expenditure_budget_id);
        """)
        self._cr.execute("""
            create extension if not exists tablefunc;
        """)

    _sql_constraints = [
        ('uniq_quarter', 'unique(program_code_id,start_date,end_date)',
        'The Program code must be unique per quarter!'),
    ]

    @api.model
    def fields_get(self, fields=None, attributes=None):
        no_selectable_fields = [ 'currency_id', 'cron_id', 'imported_sessional', 'imported', 'is_create_from_adequacies', 'new_adequacies_id',
                                'program_code_id', 'dependency_id', 'item_id', 'sub_dependency_id' , 'program_id']
        no_sortable_field = ['cron_id', 'location', 'imported_sessional', 'is_create_from_adequacies', 'currency_id', 'new_adequacies_id']
        res = super(ExpenditureBudgetLine, self).fields_get(fields, attributes=attributes)
        for key, value in res.items():
            if key in no_selectable_fields:
                value.update({'selectable': False})
        for key, value in res.items():
            if key in no_sortable_field:
                value.update({'sortable': False})
        return res

    @api.onchange('assigned')
    def onchange_assigned(self):
        if self.assigned:
            self.available = self.assigned

    def write(self, vals):
        if 'assigned' in vals:
            for line in self:
                line.available = vals.get('assigned', 0)
        return super(ExpenditureBudgetLine, self).write(vals)

    @api.model
    def create(self, vals):
        res = super(ExpenditureBudgetLine, self).create(vals)
        if res and res.assigned:
            res.available = res.assigned
        return res

    # ALTER TABLE expenditure_budget_line DROP CONSTRAINT expenditure_budget_line_uniq_program_code_id;

    def _get_available_for_quarter(self):
        """
        Devulve la dispobilidad en forma de lista ordenada de acuerdo al trimestre

        Args:

        Returns:
            [float]: Lista con los disponibles ordenados por trimestre
        """
        self._cr.execute("""
            select available, start_date
            from expenditure_budget_line
            where budget_line_yearly_id= %s
            order by start_date
        """, (self.id,))
        available = []
        for quarter in self._cr.dictfetchall():
            a = quarter.get('available', 0)
            if a:
                available.append(a)
            else:
                available.append(0)
        return available

    def get_current_quarter_available(self, quarter):
        """
        Devulve la dispobilidad acumulada para un trimestre dado

        Args:
        #budget = self.env['expenditure.budget.replica'].search([('budget_line_yearly_id', '=', self.id)])
            quarter (int): Número correspondiente al trimestre

        Returns:
            (int): Presupuesto acumulado al trimestre
        """
        available = 0
        quarter_availables = self._get_available_for_quarter()
        for i in range(quarter):
            if quarter_availables[i]:
                available += quarter_availables[i]
        return available

    def _month_to_quarter(self, month):
        """
        Devuelve el trimestre al que corresponde determinado mes del año

        Args:
            month (int): Mes del año

        Returns
            (int): Trimestre correspondite al mes
        """
        return [1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4][month-1]

    def _update_budget_line(self, adecuation_type, b_line, amount):
        """
        Actualiza la disponiblidad de presupuesto dada en funcíon de si es incremento
        o decremento para la linea dada, la tabla replica y las lineas de control.

        Args:
            adecuation_type (str): Tipo de adecuación: de incremento o decremento.
            b_line (object): Objeto de tipo expenditure.budget.line que corresponde
                al trimestre a modificar.
            amount (float): Cantidad a incrementar o decrementar en una linea.
        """
        if adecuation_type == "increase":
            b_line.available += amount
        else:
            b_line.available -= amount
        # Se actualiza la tabla replica
        b_quarter = self._month_to_quarter(b_line.start_date.month)
        # Se actualizan las lineas de control
        control_line = self.env['control.assigned.amounts.lines']
        control_line.update_control_line(b_line.available, self.program_code_id.id, b_quarter)

    def _update_available_accumulated_quarter(self, operation_type, program_code_id, date, amount, adecuation=None):
        """
        Actualiza la disponibilidad del presupuesto para un trimestre dado según
        el tipo de operacion (increase, decrease)

        Args:
            quarter (int): Trimestre a operar
            adecuation_type (str): Tipo de adecuación: de incremento o decremento
            amount (float): Cantidad a incrementar o decrementar en una linea
            adecuation (bool): Booleano que indica si se trata de una operacion
                de adecuacion (se realiza reduccion/ampliacion) o de un gasto
        """
        def validate_available(program_code_id, amount):
            query_program_code = """
                select l.id line_id, extract(quarter from start_date) quarter, coalesce(l.available,0.0) as available, l.write_date, l.budget_line_yearly_id
                from expenditure_budget_line l
                where budget_line_yearly_id is not null
                and l.program_code_id = %s
                order by quarter
            """
            self.env.cr.execute(query_program_code, (program_code_id,))
            res = self.env.cr.dictfetchall()
            # Validar suficiencia
            available = sum([res[q]['available'] for q in range(quarter)])
            if available < amount:
                program_code = self.env[PROGRAM_CODE].sudo().search([('id', '=', program_code_id)], limit=1)
                raise ValidationError("Insuficiencia Presupuestal\nCódigo programático: %s.\nEl monto requerido es de $%s y el monto disponible es de $%s." %(
                    program_code.program_code,
                    format(amount, ",.2f"),
                    format(available, ",.2f")
                ))
            return res

        quarter = self._month_to_quarter(date.month)
        querys = []
        query_adecuation = """
            INSERT INTO adequacies_lines_report(
                adequacies_id, program_code_id, quarter, expansion_amount, reduction_amount, currency_id,
                create_uid, write_uid, create_date, write_date
            )
            VALUES (
                %s, %s, %s, %s, %s, %s,
                %s, %s, %s, %s
            )
            returning 1;
        """
        if operation_type == 'decrease':
            res = validate_available(program_code_id, amount)
            tmp_amount = amount
            for q in range(quarter):
                # Si el trimestre tiene presupuesto disponible
                if res[q]['available'] > 0:
                    # Si el presupuesto es menor o igual a la cantidad a reducir
                    # se reduce todo el presupuesto de ese trimestre y se actualiza
                    # la linea correspondiente al trimestre
                    decrement = 0
                    if res[q]['available'] <= tmp_amount:
                        decrement = res[q]['available']
                    else:
                        decrement = tmp_amount
                    # Query para actualizar expenditure.budget.line
                    query_budget_line = "update expenditure_budget_line set available = coalesce(available,0)::numeric - %s::numeric, write_date = now() where id = %s and (write_date = %s or available >= %s) returning 1;"
                    querys.append([query_budget_line, (decrement, res[q]['line_id'], res[q]['write_date'], decrement)])
                    # Query para actualizar control.assigned.amounts.lines
                    querys.append([QUERY_CONTROL_LINE, (decrement, program_code_id, res[q]['quarter'])])
                    # Se actuliza el monto que falta por reducir.
                    tmp_amount -= decrement
                    if adecuation:
                        querys.append([query_adecuation, (
                            adecuation,
                            program_code_id,
                            q + 1,
                            0.0,
                            decrement,
                            self.env.company.currency_id.id,
                            self.env.user.id,
                            self.env.user.id,
                            date,
                            date,
                        )])
                # Cuando tmp_amount se hace cero se rompe el ciclo
                if tmp_amount == 0:
                    break
            # Se decrementa el monto reducido en el disponible anual
            budget_line_yearly = res[0]['budget_line_yearly_id']
            query_yearly = "update expenditure_budget_line set available = coalesce(available,0)::numeric - %s::numeric, write_date = now() where id = %s returning 1;"
            querys.append([query_yearly, (amount, budget_line_yearly)])
        elif operation_type == "increase":
            query_program_code = """
                select l.id line_id, budget_line_yearly_id
                from expenditure_budget_line l
                where budget_line_yearly_id is not null
                and l.program_code_id = %s
                and extract(quarter from start_date) = %s;
            """
            self.env.cr.execute(query_program_code, (program_code_id, quarter))
            quarter_line = self._cr.dictfetchall()[0]
            # Query para actualizar expenditure.budget.line
            query_budget_line = "update expenditure_budget_line set available = coalesce(available,0)::numeric + %s::numeric, write_date = now() where id = %s returning 1;"
            querys.append([query_budget_line, (amount, quarter_line['line_id'])])
            # Query para actualizar control.assigned.amounts.lines
            query_control_line = "update control_assigned_amounts_lines set available = coalesce(available,0)::numeric + %s::numeric, write_date = now() where program_code_id = %s and extract(quarter from start_date) = %s returning 1;"
            querys.append([query_control_line, (amount, program_code_id, quarter)])
            if adecuation:
                querys.append([query_adecuation, (
                    adecuation,
                    program_code_id,
                    quarter,
                    amount,
                    0.0,
                    self.env.company.currency_id.id,
                    self.env.user.id,
                    self.env.user.id,
                    date,
                    date,
                )])
            # Se incrementa el monto ampliado en el disponible anual
            query_budget_line_yearly = "update expenditure_budget_line set available = coalesce(available,0)::numeric + %s::numeric, write_date = now() where id = %s returning 1;"
            querys.append([query_budget_line_yearly, (amount, quarter_line['budget_line_yearly_id'])])
        # Ejecución de los update
        for query in querys:
            self.env.cr.execute(query[0], query[1])
            if not self.env.cr.fetchone():
                validate_available(program_code_id, amount)

    def update_adequacie(self, adecuation_type, program_code_id, date, amount, adecuation):
        """
        Actualiza la disponibilidad del presupuesto para un trimestre dado según
        el tipo de operacion (increase, decrease) de una adecuación.
        Actualiza control.assigned.amounts.lines.
        El método debe ser invocado desde una linea padre (linea con los datos anuales)

        Args:
            quarter (int): Trimestre a operar
            adecuation_type (str): Tipo de adecuación: de incremento o decremento
            amount (float): Cantidad a incrementar o decrementar en una linea
        """
        self._update_available_accumulated_quarter(adecuation_type, program_code_id, date, amount, adecuation)

    def update_expenditure_month(self, program_code_id, month, amount):
        """
        Actualiza la disponibilidad del presupuesto para un mes dado según
        el tipo de operacion (increase, decrease); utiliza el presupuesto acumulado,
        es decir, hace uso del presupuesto disponible de los meses anteriores.
        El método debe ser invocado desde una linea padre (linea con los datos anuales)

        Args:
            quarter (int): Trimestre a operar
            adecuation_type (str): Tipo de adecuación: de incremento o decremento
            amount (float): Cantidad a incrementar o decrementar en una linea

        Returns:
            [dict]: Arreglo de diccionarios que contiene la linea afectada
                    y el monto con que se afecto
                    ej. [{'budget_line_id': budget_line, 'amount': amount}]
        """
        b_quarter = self._month_to_quarter(month)
        # Se realiza un incremento la columna correspondiente al mes del gasto
        # para mantener actualizado y consistente el valor
        return self.update_available_program_code(program_code_id, amount, b_quarter)

    def cancel_expenditure_month(self, lines):
        """
        Cancela un gasto en el presupuesto, aumentando la disponibilidad del presupuesto
        para un mes dado según, y eliminado el gasto en la columna correspondiente
        al mes.
        El método debe ser invocado desde una linea padre (linea con los datos anuales)

        Args:
            lines ([dict]): Arreglo con las lineas afectadas,
                ej. [{'budget_line_id': budget_line, 'amount': amount}]
            month (int): Mes al que se le esta cancelando el gasto
            amount (float): Monto total del gasto cancelado
        """
        # Se realiza el incremento del disponible en las lineas afectadas para volver
        # al estado original
        querys = []
        total_amount = 0
        for b_line in lines:
            if b_line.budget_line_id and b_line.amount:
                # Se calcula el total
                total_amount += b_line.amount
                # Se calcula el trimestre de la linea
                b_quarter = self._month_to_quarter(b_line.budget_line_id.start_date.month)
                # Query para actualizar expenditure.budget.line
                query_budget_line = "update expenditure_budget_line set available = coalesce(available,0)::numeric + %s::numeric, write_date = now() where id = %s"
                querys.append([query_budget_line, (b_line.amount, b_line.budget_line_id.id)])
                # Query para actualizar control.assigned.amounts.lines
                query_control_line = "update control_assigned_amounts_lines set available = coalesce(available,0)::numeric + %s::numeric, write_date = now() where program_code_id = %s and extract(quarter from start_date) = %s"
                querys.append([query_control_line, (b_line.amount, b_line.budget_line_id.program_code_id.id, b_quarter)])
            else:
                raise ValidationError('Formato de línea de presupuesto inválido')
        # Query para actualizar expenditure.budget.line anual
        query_budget_line_yearly = "update expenditure_budget_line set available = coalesce(available,0)::numeric + %s::numeric, write_date = now() where id = %s"
        querys.append([query_budget_line_yearly, (total_amount, lines[0].budget_line_id.budget_line_yearly_id.id)])
        # Ejecución de los update
        for query in querys:
            self.env.cr.execute(query[0], query[1])

    def get_available_by_program_code(self, program_code_id, quarter):
        """
        Devulve la dispobilidad acumulada para un trimestre dado a partir del código programatico

        Args:
            program_code_id (int) : ID del código programatico a buscar
            quarter (int): Número correspondiente al trimestre

        Returns:
            (int): Presupuesto acumulado al trimestre
        """
        query = """
            select extract(quarter from start_date) quarter, coalesce(l.available,0.0) as available
            from expenditure_budget_line l
            where budget_line_yearly_id is not null
            and l.program_code_id = %s
            order by quarter
        """
        self.env.cr.execute(query, (program_code_id,))
        res = self.env.cr.dictfetchall()
        # Calculo de la suficiencia
        return sum([res[q]['available'] for q in range(quarter)])

    def reschedule(self, src_quarter, dst_quarter, amount):
        """
        Recalendariza el presupuesto de un trimestre asignandoselo a otro.
        El método debe ser invocado desde una linea padre (linea con los datos anuales)

        Args:
            src_quarer (int): Trimestre de origen del presupuesto
            dst_quarer (int): Trimestre de destino del presupuesto
            amount (float): Monto a recalendarizar
        """
        query = """
            select id from expenditure_budget_line
            where budget_line_yearly_id = %s and extract(quarter from start_date) = %s;
        """
        # Se obtiene la linea de origen
        self._cr.execute(query, (self.id, src_quarter))
        src_line_id = self._cr.dictfetchall()[0]
        src_line = self.env[EXPENDITURE_BUDGET_LINE].browse(src_line_id['id'])
        # Se obtiene la linea de destino
        self._cr.execute(query, (self.id, dst_quarter))
        src_line_id = self._cr.dictfetchall()[0]
        dst_line = self.env[EXPENDITURE_BUDGET_LINE].browse(src_line_id['id'])
        # Se valida que la linea de origen tenga el presupuesto necesario
        if src_line.available >= amount:
            # Se actualizan las lineas origen
            self._update_budget_line('decrease', src_line, amount)
            # Se actualizan las lineas destino
            self._update_budget_line('increase', dst_line, amount)
            return True, None
        return False, src_line.available

    def update_avalability_for_several_program_code(self, program_code_list, record_date=datetime.today()):

        """
        Disminuye el disponible de un codigo programatico utilizando el acumulado
        de los trimestres anteriores. Toma como base la fecha actual del servidor
        Odoo (python)

        Args:

        program_code_list ([(program_code_id, amount)]): Arreglo de tuplas con
        los registros de los código programaticos y el monto.
        Ej. [(program_code(1), 1000), (program_code(2), 1000)]

        """
        # Se obtiene el trimestre actual
        if not record_date:
            record_date=datetime.today()

        month = int(record_date.month)

        quarter = self._month_to_quarter(month)

        for line in program_code_list:

        # Se realiza la actualización tal como si fuera una adecuación de
        # reducción dado que se sigue la misma lógica.

            #self.env[EXPENDITURE_BUDGET_LINE].sudo().search([('program_code_id', '=', line[0]),('budget_line_yearly_id', '!=', '')],limit=1).update_adequacie('decrease', quarter, line[1])
            self.update_available_program_code(line[0],line[1],quarter)

    def update_available_program_code(self, program_code, amount, quarter = None):
        query_program_code = """
            select l.id line_id, extract(quarter from start_date) quarter, coalesce(l.available,0.0) as available, l.write_date, l.budget_line_yearly_id
            from expenditure_budget_line l
            where budget_line_yearly_id is not null
            and l.program_code_id = %s
            order by quarter
        """
        self.env.cr.execute(query_program_code, (program_code,))
        res = self.env.cr.dictfetchall()
        # Validar suficiencia
        available = sum([res[q]['available'] for q in range(quarter)])
        if available < amount:
            return False, available, []
        # Decrementar acumulados
        querys = []
        budget_lines = []
        tmp_amount = amount
        for q in range(quarter):
            # Si el trimestre tiene presupuesto disponible
            if res[q]['available'] > 0:
                # Si el presupuesto es menor o igual a la cantidad a reducir
                # se reduce todo el presupuesto de ese trimestre y se actualiza
                # la linea correspondiente al trimestre
                decrement = 0
                if res[q]['available'] <= tmp_amount:
                    decrement = res[q]['available']
                else:
                    decrement = tmp_amount
                # Query para actualizar expenditure.budget.line
                query_budget_line = "update expenditure_budget_line set available = coalesce(available,0)::numeric - %s::numeric, write_date = now() where id = %s and (write_date = %s or available >= %s)"
                querys.append([query_budget_line, (decrement, res[q]['line_id'], res[q]['write_date'], decrement)])
                # Query para actualizar control.assigned.amounts.lines
                querys.append([QUERY_CONTROL_LINE, (decrement, program_code, res[q]['quarter'])])
                # Array con los valores que fueron afectados
                budget_lines.append({'budget_line_id': res[q]['line_id'], 'amount': decrement})
                # Se actuliza el monto que falta por reducir.
                tmp_amount -= decrement
            # Cuando tmp_amount se hace cero se rompe el ciclo
            if tmp_amount == 0:
                break
        # Se decrementa el monto reducido en el disponible anual
        budget_line_yearly = res[0]['budget_line_yearly_id']
        querys.append([QUERY_EXPENDITURE_BUDGET_LINE, (amount, budget_line_yearly)])
        #logging.critical(querys)
        for query in querys:
            self.env.cr.execute(query[0], query[1])
            '''if not self._cr.fetchone():
                raise ValidationError("Error, el valor del disponible cambio")'''
        return True, None, budget_lines

    def return_available(self, lines, date=datetime.today(), name=''):
        """
        Devuelve el disponible del presupuesto al trimestre en que se realiza la
        cancelación.
        Args:
            lines ([dict]): Arreglo de tuplas con el id del código programático y el monto a regresar,
                ej. [(1, 1000), (2, 1000)]
            date (datetime): Fecha de devolución del gasto
        """
        # Mapea el trimestre al que se le devolverá el dinero
        quarter = self._month_to_quarter(date.month)
        query_budget_line = """
            update expenditure_budget_line set
                available = coalesce(available,0)::numeric + %s::numeric,
                write_date = now(),
                write_uid = %s
            where program_code_id = %s
            and (
                budget_line_yearly_id is null or (
                    extract(quarter from start_date) = %s and extract(quarter from end_date) = %s
                )
            )
        """
        query_control_line = """
            update control_assigned_amounts_lines set
                available = coalesce(available,0)::numeric + %s::numeric,
                write_date = now(),
                write_uid = %s
            where program_code_id = %s and extract(quarter from start_date) = %s
        """
        item_ie = self.env['payment.parameters'].get_param('item_ie')
        for program_code, amount in lines:
            program_code_id = self.env[PROGRAM_CODE].browse([program_code])
            if program_code_id.year.name != str(date.year):
                raise ValidationError("El año de la fecha de devolución no coincide con el año del código programático")
            # Actualización de las líneas de expenditure.buget.line
            self.env.cr.execute(query_budget_line, (amount, self.env.user.id, program_code, quarter, quarter))
            # Actualización de las líneas de control
            self.env.cr.execute(query_control_line, (amount, self.env.user.id, program_code, quarter))
            # Adecuación a la partida 711 para códigos de Ingreso Extraordinario
            if program_code_id.resource_origin_id.key_origin == '01':
                # Diario de las adecuaciones
                journal_id = self.env.ref('jt_conac.comp_adequacy_jour')
                lines_adeq = []
                # Código al que se le va a decrementar el disponible
                lines_adeq.append((0, 0, {
                    'program': program_code_id.id,
                    'line_type': 'decrease',
                    'amount': amount,
                    'decrease_type': amount,
                    'creation_type': 'automatic'
                }))
                # Código al que se le va a regresar el disponible
                program_code_id_711 = self.env[PROGRAM_CODE].search([
                    ('dependency_id', '=', program_code_id.dependency_id.id),
                    ('sub_dependency_id', '=', program_code_id.sub_dependency_id.id),
                    ('resource_origin_id.key_origin', '=', '01'),
                    ('item_id', '=', item_ie.id),
                    ('budget_id', '=', program_code_id.budget_id.id),
                ])
                lines_adeq.append((0, 0, {
                    'program': program_code_id_711.id,
                    'line_type': 'increase',
                    'amount': amount,
                    'increase_type': amount,
                    'creation_type': 'automatic'
                }))
                adequacie = {
                    'budget_id': program_code_id.budget_id.id,
                    'adaptation_type': 'compensated',
                    'state': 'confirmed',
                    'journal_id': journal_id and journal_id.id or False,
                    'date_of_budget_affected': date,
                    'adequacies_lines_ids': lines_adeq,
                    'is_dgpo_authorization': True,
                    'observation': 'Autorización partida %s' % item_ie.item,
                    'origin_of_the_adequacy': 'automatic',
                }
                adequacie = self.env['adequacies'].create(adequacie)
                adequacie.accept('Devolución de ingreso - gasto: [%s]' % name)

    def insert_sql(self, lines):
        def _get_column_stored_name():
            query = """
                select column_name from information_schema.columns
                where table_schema = 'public'
                and table_name   = 'expenditure_budget_line' and column_name <> 'id'
            """
            self.env.cr.execute(query, ())
            return [f'"{res[0]}"' for res in self.env.cr.fetchall()]

        def _get_insert_sql():
            columns = _get_column_stored_name()
            column = ", ".join(columns)
            values = ", ".join(["%s"]* len(columns))
            return f"insert into expenditure_budget_line ({column}) values({values}) returning id", columns


        def _get_insert_params(columns, vals):
            def update_vals(updates):
                for k, v in updates:
                    if k not in vals: vals.update({k: v})

            res = []
            # Pone los valores por defecto a los campos default o calculados de odoo
            updates = [
                ('authorized', 0.0),
                ('assigned', 0.0),
                ('available', 0.0),
                ('currency_id', self.env.company.currency_id.id),
                ('imported', False),
                ('create_uid', self.env.user.id),
                ('write_uid', self.env.user.id),
                ('create_date', 'now()'),
                ('write_date', 'now()'),
            ]
            update_vals(updates)
            for column in columns:
                res.append(vals.get(column[1:-1], None))
            return tuple(res)

        query, columns = _get_insert_sql()
        # Creación de las líneas anuales
        params = []
        for line in lines:
            year = line.get('year', datetime.today().year)
            line.update({
                'start_date': datetime.strptime("%s-01-01" % year, DATE_FORMAT),
                'end_date': datetime.strptime("%s-12-31" % year, DATE_FORMAT),
            })
            params.append(_get_insert_params(columns, line))
        ids = []
        for p in params:
            self.env.cr.execute(query, p)
            res = (self.env.cr.fetchone() or [None])[0]
            if not res:
                raise ValidationError("Error al insertar el registro expenditure.budget.line")
            ids.append(res)
        # Creación de las lineas trimestrales
        params = []
        # Fechas de inicio y fin de los trimestres
        months = [
            ('%s-01-01', '%s-03-31'),
            ('%s-04-01', '%s-06-30'),
            ('%s-07-01', '%s-09-30'),
            ('%s-10-01', '%s-12-31'),
        ]
        i = 0
        for line in lines:
            year = line.get('year', datetime.today().year)
            for start_date, end_date in months:
                line.update({
                    'budget_line_yearly_id': ids[i],
                    'start_date': datetime.strptime(start_date % year, DATE_FORMAT),
                    'end_date': datetime.strptime(end_date % year, DATE_FORMAT),
                })
                params.append(_get_insert_params(columns, line))
            i += 1
        for p in params:
            self.env.cr.execute(query, p)
            res = (self.env.cr.fetchone() or [None])[0]
            if not res:
                raise ValidationError("Error al insertar el registro expenditure.budget.line")
            ids.append(res)
        return ids

    def update_available_with_overdraft(self, program_code_list, affected_date=datetime.today(), quarter=None):
        """
        Método para actualizar el disponible sobregirando los códigos programáticos
        """
        querys = []
        budget_lines = []
        for line in program_code_list:
            program_code_id = line.get('program_code_id')
            amount = line.get('amount')
            # Se obtiene el trimestre que se va a afectar
            if not quarter:
                month = int(affected_date.month)
                quarter = self._month_to_quarter(month)
            # Se consulta la suficiencia
            query_program_code = """
            select l.id line_id, extract(quarter from start_date) quarter, coalesce(l.available,0.0) as available, l.write_date, l.budget_line_yearly_id
            from expenditure_budget_line l
            where budget_line_yearly_id is not null
            and l.program_code_id = %s
            order by quarter
            """
            self.env.cr.execute(query_program_code, (program_code_id,))
            res = self.env.cr.dictfetchall()
            # Decrementar acumulados
            tmp_amount = amount
            for q in range(quarter):
                # Si el trimestre tiene presupuesto disponible
                if res[q]['available'] > 0:
                    # Si el presupuesto es menor o igual a la cantidad a reducir
                    # se reduce todo el presupuesto de ese trimestre y se actualiza
                    # la linea correspondiente al trimestre
                    decrement = 0
                    if res[q]['available'] <= tmp_amount:
                        decrement = res[q]['available']
                    else:
                        decrement = tmp_amount
                    # Query para actualizar expenditure.budget.line
                    query_budget_line = "update expenditure_budget_line set available = coalesce(available,0)::numeric - %s::numeric, write_date = now() where id = %s and (write_date = %s or available >= %s)"
                    querys.append([query_budget_line, (decrement, res[q]['line_id'], res[q]['write_date'], decrement)])
                    # Query para actualizar control.assigned.amounts.lines
                    querys.append([QUERY_CONTROL_LINE, (decrement, program_code_id, res[q]['quarter'])])
                    # Array con los valores que fueron afectados
                    budget_lines.append({'budget_line_id': res[q]['line_id'], 'amount': decrement})
                    # Se actuliza el monto que falta por reducir.
                    tmp_amount -= decrement
                # Cuando tmp_amount se hace cero se rompe el ciclo
                if tmp_amount == 0:
                    break
            # Si el monto no se logro cubrir, se tiene que sobregirar el presupusto
            if tmp_amount > 0:
                # Query para actualizar expenditure.budget.line
                querys.append([QUERY_EXPENDITURE_BUDGET_LINE, (tmp_amount, res[q]['line_id'])])
                # Query para actualizar control.assigned.amounts.lines
                querys.append([QUERY_CONTROL_LINE, (tmp_amount, program_code_id, res[q]['quarter'])])
                # Array con los valores que fueron afectados
                budget_lines.append({'budget_line_id': res[q]['line_id'], 'amount': tmp_amount})
            # Se decrementa el monto reducido en el disponible anual
            budget_line_yearly = res[0]['budget_line_yearly_id']
            querys.append([QUERY_EXPENDITURE_BUDGET_LINE, (amount, budget_line_yearly)])
        # Se ejecutan las consultas en la base de datos
        for query in querys:
            self.env.cr.execute(query[0], query[1])
        # Se regresan las líneas de presupuesto afectadas
        return budget_lines
    

    def logged_user_domain(self):
        user = self.env.user
        res_users_model = self.env['res.users']
        domain = []
        domain_dependencies =  res_users_model.get_user_dependencies_domain_budget()
        domain_subdependencies =  res_users_model.get_user_subdependencies_domain_budget()
        domain_programs =  res_users_model.get_user_programs_domain_budget()
        domain_subprograms =  res_users_model.get_user_subprograms_domain_budget()

        if user.dependency_budget_ids:
            domain+= domain_dependencies
        if user.sub_dependency_budget_ids:
            domain+= domain_subdependencies    
        if user.program_budget_ids:
            domain += domain_programs
        if user.subprogram_budget_ids:
            domain += domain_subprograms

        return {
            'name': _('Budget lines'),
            "res_model": "expenditure.budget.line",
            "domain": domain,
            "type": "ir.actions.act_window",
            "view_mode": "tree,search",
            "views": [(self.env.ref('jt_budget_mgmt.expenditure_budget_line_tree').id, 'tree'),
                      (self.env.ref('jt_budget_mgmt.expenditure_budget_line_search').id, 'search')
                      ],
        }    
    