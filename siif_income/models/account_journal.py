from odoo import models, fields, api, _
from datetime import datetime, timedelta
from odoo.exceptions import ValidationError
import logging

model = 'account.account'
model_budget = 'affectation.income.budget'

class AccountJournal(models.Model):
    
    _inherit = "account.journal"

    interest_account_id = fields.Many2one(model, "Interest Account")
    number_movements_comission = fields.Integer("Movements whithout comission")
    aux_number_movements_comission = fields.Integer("Aux movements whithout comission")
    update_date_movements = fields.Date("Update date")
     

    @api.onchange('number_movements_comission')
    def set_aux_number_movements_comission(self):
        if not self.update_date_movements or self.update_date_movements=='':
            self.aux_number_movements_comission = self.number_movements_comission


class AccountTax(models.Model):
    _inherit = "account.tax"

    iva_to_move_account_id = fields.Many2one(model, "IVA to Move Account")
    iva_transferred_account_id = fields.Many2one(model, "IVA Transferred Account")



class AccountMove(models.Model):

    _inherit = 'account.move'

    comission_charged = fields.Selection([('no_charged', 'Comission not charged'), ('charged', 'Comission charged')])
    income_status = fields.Selection(selection_add=[('reversed','Reversed')])
    state = fields.Selection(selection_add=[('reversed','Reversed'), ('previous_exercise','Previous exercise'), ('error','Error'), ('rejected','Rejected'), ('disabled','Disabled'), ('inherit','Inherit'), ('inherit_substitution','Inherit substitution')])
    observations_web_service =  fields.Char('Observations web service')
    type_of_registry_payment = fields.Selection([('comission','Comission'), ('credit_note', 'Credit note')])
    is_in_cash_closing = fields.Boolean("Is in cash closing")
    short_code = fields.Char("Short code")


    @api.onchange('exercise','exercise_upa')
    def set_account_id(self):
        if self.exercise != '' and self.exercise_upa != '' and (int(self.exercise) > int(self.exercise_upa)):
            self.account_id = self.operation_key_id.previous_account_id.id


    def assign_name_education_servs(self, reference_plugin, reference_plugin_2, reference_plugin_3, description_of_the_movement):
        reference = reference_plugin or ''
        reference_2 = reference_plugin_2 or ''
        reference_3 = reference_plugin_3 or ''
        description = description_of_the_movement or ''             

        name = f'{reference} {reference_2} {reference_3} {description}'

        return name
 
    def approve(self):
        origin = False
        cri = False
        affectation_amount = 0
        for payment in self:
            if not payment.line_ids:
                if not payment.income_bank_journal_id:
                    if self.env.user.lang == 'es_MX':
                        raise ValidationError(_("Por favor seleccione un banco."))
                    else:
                        raise ValidationError(_("Please select a bank_interest."))
                else:
                    journal = payment.income_bank_journal_id
                    journal_pay = payment.journal_id
                    if payment.type_of_revenue_collection == 'bank_interest' and not journal.interest_account_id:
                        if self.env.user.lang == 'es_MX':
                            raise ValidationError(_("Por favor configure la cuenta de intereses del banco."))
                        else:
                            raise ValidationError(_("Please configure the interest bank account."))
                    if ((payment.type_of_revenue_collection == 'aspirants' or payment.type_of_revenue_collection == 'education_servs' or payment.type_of_revenue_collection == 'exam_prep') 
                        and not payment.afectation_account_id):
                        if self.env.user.lang == 'es_MX':
                            raise ValidationError(_("La cuenta de afectación no puede estar vacía."))
                        else:
                            raise ValidationError(_("The afectation account cannot be empty."))
                    if ((payment.type_of_revenue_collection == 'aspirants' or payment.type_of_revenue_collection == 'education_servs' or payment.type_of_revenue_collection == 'exam_prep' or payment.type_of_revenue_collection == 'deposit_cer') 
                        and payment.amount_interest <= 0.0):
                        if self.env.user.lang == 'es_MX':
                            raise ValidationError(_("El monto no puede ser menor o igual a cero."))
                        else:
                            raise ValidationError(_("The amount cannot be zero."))     

                    if payment.income_bank_journal_id and not payment.income_bank_journal_id.accrued_income_credit_account_id \
                        or not payment.income_bank_journal_id.conac_accrued_income_credit_account_id \
                        or not payment.income_bank_journal_id.accrued_income_debit_account_id \
                        or not payment.income_bank_journal_id.conac_accrued_income_debit_account_id :
                        payment_acc_flag = False
                    else:
                        payment_acc_flag = True

                    if payment.income_bank_journal_id and not payment.income_bank_journal_id.recover_income_credit_account_id \
                        or not payment.income_bank_journal_id.conac_recover_income_credit_account_id \
                        or not payment.income_bank_journal_id.recover_income_debit_account_id \
                        or not payment.income_bank_journal_id.conac_recover_income_debit_account_id :
                        payment_rec_flag = False
                    else:
                        payment_rec_flag = True
                    balance = self.amount_interest
                    counterpart_amount = 0
                    move_id = self.id
                    invoice_line = []

                    # Verifica si el tipo de cobro es intereses bancarios
                    if payment.type_of_revenue_collection == 'bank_interest':
                        payment.line_ids = [(0, 0, {
                                                             'account_id': payment.income_bank_journal_id.interest_account_id and payment.income_bank_journal_id.interest_account_id.id or False,
                                                             'credit': balance,
                                                             'amount_currency' : -counterpart_amount,                                     
                                                             'move_id' : move_id,
                                                             'partner_id': payment.partner_id.commercial_partner_id.id,
                                                             'dependency_id':payment.dependancy_id.id if payment.income_bank_journal_id.interest_account_id.dep_subdep_flag else False,
                                                             'sub_dependency_id':payment.sub_dependancy_id.id if payment.income_bank_journal_id.interest_account_id.dep_subdep_flag else False,
                                                             
                                                         }), 
                                                (0, 0, {
                                                             'account_id': payment.income_bank_journal_id.default_debit_account_id and payment.income_bank_journal_id.default_debit_account_id.id or False,
                                                             'coa_conac_id': payment.income_bank_journal_id.conac_debit_account_id and payment.income_bank_journal_id.conac_debit_account_id.id or False,
                                                             'debit': balance,
                                                             'conac_move' : True,
                                                             'amount_currency' : counterpart_amount,                                   
                                                             'move_id' : move_id,
                                                             'partner_id': payment.partner_id.commercial_partner_id.id,
                                                           
                                                         })]

                        payment.line_ids = [(0, 0, {
                                                             'account_id': payment.income_bank_journal_id.accrued_income_credit_account_id and payment.income_bank_journal_id.accrued_income_credit_account_id.id or False,
                                                             'coa_conac_id': payment.income_bank_journal_id.conac_accrued_income_credit_account_id and payment.income_bank_journal_id.conac_accrued_income_credit_account_id.id or False,
                                                             'credit': balance, 
                                                             'conac_move' : True,
                                                             'amount_currency' : -counterpart_amount,                                     
                                                             'move_id' : move_id,
                                                             'partner_id': payment.partner_id.commercial_partner_id.id,
                                                             
                                                         }), 
                                                (0, 0, {
                                                             'account_id': payment.income_bank_journal_id.accrued_income_debit_account_id and payment.income_bank_journal_id.accrued_income_debit_account_id.id or False,
                                                             'coa_conac_id': payment.income_bank_journal_id.conac_accrued_income_debit_account_id and payment.income_bank_journal_id.conac_accrued_income_debit_account_id.id or False,
                                                             'debit': balance,
                                                             'conac_move' : True,
                                                             'amount_currency' : counterpart_amount,                                   
                                                             'move_id' : move_id,
                                                             'partner_id': payment.partner_id.commercial_partner_id.id,
                                                           
                                                         })]

                        payment.line_ids = [(0, 0, {
                                                             'account_id': payment.income_bank_journal_id.recover_income_credit_account_id and payment.income_bank_journal_id.recover_income_credit_account_id.id or False,
                                                             'coa_conac_id': payment.income_bank_journal_id.conac_recover_income_credit_account_id and payment.income_bank_journal_id.conac_recover_income_credit_account_id.id or False,
                                                             'credit': balance, 
                                                             'conac_move' : True,
                                                             'amount_currency' : -counterpart_amount,                                    
                                                             'move_id' : move_id,
                                                             'partner_id': payment.partner_id.commercial_partner_id.id,
                                                             
                                                         }), 
                                                (0, 0, {
                                                             'account_id': payment.income_bank_journal_id.recover_income_debit_account_id and payment.income_bank_journal_id.recover_income_debit_account_id.id or False,
                                                             'coa_conac_id': payment.income_bank_journal_id.conac_recover_income_debit_account_id and payment.income_bank_journal_id.conac_recover_income_debit_account_id.id or False,
                                                             'debit': balance,
                                                             'conac_move' : True,
                                                             'amount_currency' : counterpart_amount,                                     
                                                             'move_id' : move_id,
                                                             'partner_id': payment.partner_id.commercial_partner_id.id,
                                                           
                                                         })]
                    # Termina función de intereses bancarios
                    # Verifica si el tipo de cobro es certificados de depósitos
                    elif payment.type_of_revenue_collection == 'deposit_cer':
                        origin = 'income-cd'
                        certificate_message = "Unable to validate the certificate."
                        if not payment.afectation_account_id and not payment.program_code_id and not payment.account_id and payment.record_type=='manual' and (payment.exercise_upa=='' or not payment.exercise_upa) and payment.operation_key_id.name!='00010030':
                            if self.env.user.lang == 'es_MX':
                                raise ValidationError(_("No se puede validar el certificado sino tiene cuenta de ingresos, código programático o cuenta contable."))
                            else:
                                raise ValidationError(_(certificate_message))

                        if payment.afectation_account_id and payment.program_code_id and payment.account_id and payment.record_type=='manual':
                            if self.env.user.lang == 'es_MX':
                                raise ValidationError(_("Solo se puede validar el certificado si tiene código programático y/o cuenta de ingresos o en su defecto cuenta contable."))
                            else:
                                raise ValidationError(_(certificate_message))
                        program_codes=[]

                        if payment.income_bank_journal_id and payment.income_bank_journal_id.default_debit_account_id:
                            line_vals = {
                                            
                                         'account_id': payment.income_bank_journal_id.default_debit_account_id and payment.income_bank_journal_id.default_debit_account_id.id or False,
                                         'coa_conac_id': payment.income_bank_journal_id.conac_debit_account_id and payment.income_bank_journal_id.conac_debit_account_id.id or False,
                                         'debit': payment.amount_interest,
                                         'conac_move' : True,
                                         'amount_currency' : counterpart_amount,                                   
                                         'move_id' : move_id,
                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                        }
                            invoice_line.append((0,0,line_vals))   

                            if payment.program_code_id and payment.program_code_id.resource_origin_id.key_origin=='00' and payment.exercise==payment.exercise_upa and not payment.afectation_account_id:

                                line_vals = {
                                            
                                         'account_id': payment.program_code_id.item_id.unam_account_id and payment.program_code_id.item_id.unam_account_id.id or False,
                                         'coa_conac_id': payment.program_code_id.item_id.cog_id and payment.program_code_id.item_id.cog_id.id or False,
                                         'credit': payment.amount_interest,
                                         'conac_move' : True,
                                         'amount_currency' : counterpart_amount,                                   
                                         'move_id' : move_id,
                                         'program_code_id': payment.program_code_id.id,
                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                         'dependency_id': payment.dependancy_id.id if payment.program_code_id.item_id.unam_account_id.dep_subdep_flag else False, 
                                         'sub_dependency_id': payment.sub_dependancy_id.id if payment.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                        }
                                invoice_line.append((0,0,line_vals)) 

                                line_vals = {
                                         'account_id': journal_pay.default_credit_account_id and journal_pay.default_credit_account_id.id or False,
                                         'coa_conac_id': journal_pay.conac_credit_account_id and journal_pay.conac_credit_account_id.id or False,
                                         'debit': payment.amount_interest, 
                                         'conac_move' : True,
                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                         'move_id' : move_id,
                                         'program_code_id': payment.program_code_id.id,
                                         'dependency_id': payment.dependancy_id.id if journal_pay.default_credit_account_id.dep_subdep_flag else False, 
                                         'sub_dependency_id': payment.sub_dependancy_id.id if journal_pay.default_credit_account_id.dep_subdep_flag else False,
                                        }
                                invoice_line.append((0,0,line_vals)) 

                                line_vals = {
                                         'account_id': journal_pay.default_debit_account_id and journal_pay.default_debit_account_id.id or False,
                                         'coa_conac_id': journal_pay.conac_debit_account_id and journal_pay.conac_debit_account_id.id or False,
                                         'credit': payment.amount_interest, 
                                         'conac_move' : True,
                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                         'move_id' : move_id,
                                         'program_code_id': payment.program_code_id.id,
                                         'dependency_id': payment.dependancy_id.id if journal_pay.default_debit_account_id.dep_subdep_flag else False, 
                                         'sub_dependency_id': payment.sub_dependancy_id.id if journal_pay.default_debit_account_id.dep_subdep_flag else False,
                                        }
                                invoice_line.append((0,0,line_vals))

                                line_vals = {
                                         'account_id': journal_pay.accured_credit_account_id and journal_pay.accured_credit_account_id.id or False,
                                         'coa_conac_id': journal_pay.conac_accured_credit_account_id and journal_pay.conac_accured_credit_account_id.id or False,
                                         'debit': payment.amount_interest, 
                                         'conac_move' : True,
                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                         'move_id' : move_id,
                                         'program_code_id': payment.program_code_id.id,
                                         'dependency_id': payment.dependancy_id.id if journal_pay.accured_credit_account_id.dep_subdep_flag else False, 
                                         'sub_dependency_id': payment.sub_dependancy_id.id if journal_pay.accured_credit_account_id.dep_subdep_flag else False,
                                        }
                                invoice_line.append((0,0,line_vals)) 

                                line_vals = {
                                         'account_id': journal_pay.accured_debit_account_id and journal_pay.accured_debit_account_id.id or False,
                                         'coa_conac_id': journal_pay.conac_accured_debit_account_id and journal_pay.conac_accured_debit_account_id.id or False,
                                         'credit': payment.amount_interest, 
                                         'conac_move' : True,
                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                         'move_id' : move_id,
                                         'program_code_id': payment.program_code_id.id,
                                         'dependency_id': payment.dependancy_id.id if journal_pay.accured_debit_account_id.dep_subdep_flag else False, 
                                         'sub_dependency_id': payment.sub_dependancy_id.id if journal_pay.accured_debit_account_id.dep_subdep_flag else False,

                                        }
                                invoice_line.append((0,0,line_vals)) 

                                line_vals = {
                                         'account_id': journal_pay.execercise_credit_account_id and journal_pay.execercise_credit_account_id.id or False,
                                         'coa_conac_id': journal_pay.conac_exe_credit_account_id and journal_pay.conac_exe_credit_account_id.id or False,
                                         'debit': payment.amount_interest, 
                                         'conac_move' : True,
                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                         'move_id' : move_id,
                                         'program_code_id': payment.program_code_id.id,
                                         'dependency_id': payment.dependancy_id.id if journal_pay.execercise_credit_account_id.dep_subdep_flag else False, 
                                         'sub_dependency_id': payment.sub_dependancy_id.id if journal_pay.execercise_credit_account_id.dep_subdep_flag else False,
                                        }
                                invoice_line.append((0,0,line_vals)) 

                                line_vals = {
                                         'account_id': journal_pay.execercise_debit_account_id and journal_pay.execercise_debit_account_id.id or False,
                                         'coa_conac_id': journal_pay.conac_exe_debit_account_id and journal_pay.conac_exe_debit_account_id.id or False,
                                         'credit': payment.amount_interest, 
                                         'conac_move' : True,
                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                         'move_id' : move_id,
                                         'program_code_id': payment.program_code_id.id,
                                         'dependency_id': payment.dependancy_id.id if journal_pay.execercise_debit_account_id.dep_subdep_flag else False, 
                                         'sub_dependency_id': payment.sub_dependancy_id.id if journal_pay.execercise_debit_account_id.dep_subdep_flag else False,
                                        }
                                invoice_line.append((0,0,line_vals)) 

                                line_vals = {
                                         'account_id': journal_pay.paid_credit_account_id and journal_pay.paid_credit_account_id.id or False,
                                         'coa_conac_id': journal_pay.conac_paid_credit_account_id and journal_pay.conac_paid_credit_account_id.id or False,
                                         'debit': payment.amount_interest, 
                                         'conac_move' : True,
                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                         'name': payment.ref+" "+payment.concept,
                                         'move_id' : move_id,
                                         'dependency_id': payment.dependancy_id.id if journal_pay.paid_credit_account_id.dep_subdep_flag else False, 
                                         'sub_dependency_id': payment.sub_dependancy_id.id if journal_pay.paid_credit_account_id.dep_subdep_flag else False,
                                         'program_code_id': payment.program_code_id.id,
                                        }
                                invoice_line.append((0,0,line_vals)) 

                                line_vals = {
                                         'account_id': journal_pay.paid_debit_account_id and journal_pay.paid_debit_account_id.id or False,
                                         'coa_conac_id': journal_pay.conac_paid_debit_account_id and journal_pay.conac_paid_debit_account_id.id or False,
                                         'credit': payment.amount_interest, 
                                         'conac_move' : True,
                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                         'move_id' : move_id,
                                         'dependency_id': payment.dependancy_id.id if journal_pay.paid_debit_account_id.dep_subdep_flag else False, 
                                         'sub_dependency_id': payment.sub_dependancy_id.id if journal_pay.paid_debit_account_id.dep_subdep_flag else False,
                                         'program_code_id': payment.program_code_id.id,
                                        }
                                invoice_line.append((0,0,line_vals))

                                program_codes.append((payment.program_code_id.id, payment.amount_interest))

                                payment.env['expenditure.budget.line'].return_available(program_codes, payment.movement_date)

                            #ejercicios anteriores
                            elif payment.exercise and payment.exercise_upa and payment.exercise!='' and payment.exercise_upa!=''  and payment.exercise!=payment.exercise_upa:
                                # Busqueda de CRI
                                account_code = payment.operation_key_id.previous_account_id.code
                                cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                line_vals = {
                                            
                                         'account_id': payment.operation_key_id.previous_account_id and payment.operation_key_id.previous_account_id.id or False,
                                         #'coa_conac_id': payment.program_code_id.item_id.cog_id and payment.program_code_id.item_id.cog_id.id or False,
                                         'credit': payment.amount_interest,
                                         'conac_move' : False,
                                         'amount_currency' : counterpart_amount,                                   
                                         'move_id' : move_id,
                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                         'dependency_id':payment.dependancy_id.id if payment.operation_key_id.previous_account_id.dep_subdep_flag else False,
                                         'sub_dependency_id':payment.sub_dependancy_id.id if payment.operation_key_id.previous_account_id.dep_subdep_flag else False,
                                         # Se busca el CRI de la cuenta, si tiene CRI se pega al Asiento contable
                                         'cri':cri.name if cri else False   
                                        }
                                invoice_line.append((0,0,line_vals))

                                if(payment.operation_key_id.previous_account_id and payment.operation_key_id.previous_account_id.code[0]=='4' and payment.operation_key_id.previous_account_id.budget_registry):
                                        # Busqueda de CRI
                                        account_code = payment.operation_key_id.previous_account_id.code
                                        cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                        if payment_rec_flag and payment_acc_flag:
                                            line_vals= {
                                                         'account_id': payment.income_bank_journal_id.accrued_income_credit_account_id and payment.income_bank_journal_id.accrued_income_credit_account_id.id or False,
                                                         'coa_conac_id': payment.income_bank_journal_id.conac_accrued_income_credit_account_id and payment.income_bank_journal_id.conac_accrued_income_credit_account_id.id or False,
                                                         'credit': payment.amount_interest, 
                                                         'conac_move' : True,
                                                         'amount_currency' : 0.0,
                                                         #'currency_id' : payment.currency_id.id,                                     
                                                         'move_id' : move_id,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                                         'cri':cri.name if cri else False             
                                                        }
                                            invoice_line.append((0,0,line_vals))
                                            line_vals = {
                                                         'account_id': payment.income_bank_journal_id.accrued_income_debit_account_id and payment.income_bank_journal_id.accrued_income_debit_account_id.id or False,
                                                         'coa_conac_id': payment.income_bank_journal_id.conac_accrued_income_debit_account_id and payment.income_bank_journal_id.conac_accrued_income_debit_account_id.id or False,
                                                         'debit': payment.amount_interest,
                                                         'conac_move' : True,
                                                         'amount_currency' : 0.0,
                                                         #'currency_id' : payment.currency_id.id,                                     
                                                         'move_id' : move_id,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                                         'cri':cri.name if cri else False             
                                                        }
                                            invoice_line.append((0,0,line_vals))

                                            line_vals = {
                                                         'account_id': payment.income_bank_journal_id.recover_income_credit_account_id and payment.income_bank_journal_id.recover_income_credit_account_id.id or False,
                                                         'coa_conac_id': payment.income_bank_journal_id.conac_recover_income_credit_account_id and payment.income_bank_journal_id.conac_recover_income_credit_account_id.id or False,
                                                         'credit': payment.amount_interest, 
                                                         'conac_move' : True,
                                                         'amount_currency' : 0.0,
                                                         #'currency_id' : payment.currency_id.id,                                     
                                                         'move_id' : move_id,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                                         'cri':cri.name if cri else False               
                                                                     }
                                            invoice_line.append((0,0,line_vals))
                                            line_vals = {
                                                         'account_id': payment.income_bank_journal_id.recover_income_debit_account_id and payment.income_bank_journal_id.recover_income_debit_account_id.id or False,
                                                         'coa_conac_id': payment.income_bank_journal_id.conac_recover_income_debit_account_id and payment.income_bank_journal_id.conac_recover_income_debit_account_id.id or False,
                                                         'debit':payment.amount_interest,
                                                         'conac_move' : True,
                                                         'amount_currency' : 0.0,
                                                         #'currency_id' : payment.currency_id.id,                                     
                                                         'move_id' : move_id,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                                         'cri':cri.name if cri else False              
                                                        }
                                            invoice_line.append((0,0,line_vals))
                                            affectation_amount = payment.amount_interest
                            #Cuentas de ingreso
                            elif payment.afectation_account_id and payment.program_code_id and payment.program_code_id.resource_origin_id.key_origin=='01' and payment.exercise==payment.exercise_upa:
                                   
                                
                                line_vals = {
                                            
                                         'account_id': payment.program_code_id.item_id.unam_account_id and payment.program_code_id.item_id.unam_account_id.id or False,
                                         'coa_conac_id': payment.program_code_id.item_id.cog_id and payment.program_code_id.item_id.cog_id.id or False,
                                         'credit': payment.amount_interest,
                                         'conac_move' : True,
                                         'amount_currency' : counterpart_amount,                                   
                                         'move_id' : move_id,
                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                         'dependency_id': payment.dependancy_id.id if payment.program_code_id.item_id.unam_account_id.dep_subdep_flag else False, 
                                         'sub_dependency_id': payment.sub_dependancy_id.id if payment.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
                                         'program_code_id': payment.program_code_id.id,
                                        }
                                invoice_line.append((0,0,line_vals)) 

                                line_vals = {
                                         'account_id': payment.journal_id.default_credit_account_id and payment.journal_id.default_credit_account_id.id or False,
                                         'coa_conac_id': payment.journal_id.conac_credit_account_id and payment.journal_id.conac_credit_account_id.id or False,
                                         'debit': payment.amount_interest, 
                                         'conac_move' : True,
                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                         'move_id' : move_id,
                                         'program_code_id': payment.program_code_id.id,
                                         'dependency_id': payment.dependancy_id.id if payment.journal_id.default_credit_account_id.dep_subdep_flag else False, 
                                         'sub_dependency_id': payment.sub_dependancy_id.id if payment.journal_id.default_credit_account_id.dep_subdep_flag else False,
                                        }
                                invoice_line.append((0,0,line_vals)) 

                                line_vals = {
                                         'account_id': payment.journal_id.default_debit_account_id and payment.journal_id.default_debit_account_id.id or False,
                                         'coa_conac_id': payment.journal_id.conac_debit_account_id and payment.journal_id.conac_debit_account_id.id or False,
                                         'credit': payment.amount_interest, 
                                         'conac_move' : True,
                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                         'name': payment.ref+" "+payment.concept,
                                         'move_id' : move_id,
                                         'program_code_id': payment.program_code_id.id,
                                         'dependency_id': payment.dependancy_id.id if payment.journal_id.default_debit_account_id.dep_subdep_flag else False, 
                                         'sub_dependency_id': payment.sub_dependancy_id.id if payment.journal_id.default_debit_account_id.dep_subdep_flag else False,
                                        }
                                invoice_line.append((0,0,line_vals))

                                line_vals = {
                                         'account_id': payment.journal_id.accured_credit_account_id and payment.journal_id.accured_credit_account_id.id or False,
                                         'coa_conac_id': payment.journal_id.conac_accured_credit_account_id and payment.journal_id.conac_accured_credit_account_id.id or False,
                                         'debit': payment.amount_interest, 
                                         'conac_move' : True,
                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                         'move_id' : move_id,
                                         'program_code_id': payment.program_code_id.id,
                                         'dependency_id': payment.dependancy_id.id if payment.journal_id.accured_credit_account_id.dep_subdep_flag else False, 
                                         'sub_dependency_id': payment.sub_dependancy_id.id if payment.journal_id.accured_credit_account_id.dep_subdep_flag else False,
                                        }
                                invoice_line.append((0,0,line_vals)) 

                                line_vals = {
                                         'account_id': payment.journal_id.accured_debit_account_id and payment.journal_id.accured_debit_account_id.id or False,
                                         'coa_conac_id': payment.journal_id.conac_accured_debit_account_id and payment.journal_id.conac_accured_debit_account_id.id or False,
                                         'credit': payment.amount_interest, 
                                         'conac_move' : True,
                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                         'move_id' : move_id,
                                         'program_code_id': payment.program_code_id.id,
                                         'dependency_id': payment.dependancy_id.id if payment.journal_id.accured_debit_account_id.dep_subdep_flag else False, 
                                         'sub_dependency_id': payment.sub_dependancy_id.id if payment.journal_id.accured_debit_account_id.dep_subdep_flag else False,
                                        }
                                invoice_line.append((0,0,line_vals)) 

                                line_vals = {
                                         'account_id': payment.journal_id.execercise_credit_account_id and payment.journal_id.execercise_credit_account_id.id or False,
                                         'coa_conac_id': payment.journal_id.conac_exe_credit_account_id and payment.journal_id.conac_exe_credit_account_id.id or False,
                                         'debit': payment.amount_interest, 
                                         'conac_move' : True,
                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                         'move_id' : move_id,
                                         'program_code_id': payment.program_code_id.id,
                                         'dependency_id': payment.dependancy_id.id if payment.journal_id.execercise_credit_account_id.dep_subdep_flag else False, 
                                         'sub_dependency_id': payment.sub_dependancy_id.id if payment.journal_id.execercise_credit_account_id.dep_subdep_flag else False,
                                        }
                                invoice_line.append((0,0,line_vals)) 

                                line_vals = {
                                         'account_id': payment.journal_id.execercise_debit_account_id and payment.journal_id.execercise_debit_account_id.id or False,
                                         'coa_conac_id': payment.journal_id.conac_exe_debit_account_id and payment.journal_id.conac_exe_debit_account_id.id or False,
                                         'credit': payment.amount_interest, 
                                         'conac_move' : True,
                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                         'move_id' : move_id,
                                         'program_code_id': payment.program_code_id.id,
                                         'dependency_id': payment.dependancy_id.id if payment.journal_id.execercise_debit_account_id.dep_subdep_flag else False, 
                                         'sub_dependency_id': payment.sub_dependancy_id.id if payment.journal_id.execercise_debit_account_id.dep_subdep_flag else False,
                                        }
                                invoice_line.append((0,0,line_vals)) 

                                line_vals = {
                                         'account_id': payment.journal_id.paid_credit_account_id and payment.journal_id.paid_credit_account_id.id or False,
                                         'coa_conac_id': payment.journal_id.conac_paid_credit_account_id and payment.journal_id.conac_paid_credit_account_id.id or False,
                                         'debit': payment.amount_interest, 
                                         'conac_move' : True,
                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                         'name': payment.ref+" "+payment.concept,
                                         'move_id' : move_id,
                                         'program_code_id': payment.program_code_id.id,
                                         'dependency_id': payment.dependancy_id.id if payment.journal_id.paid_credit_account_id.dep_subdep_flag else False, 
                                         'sub_dependency_id': payment.sub_dependancy_id.id if payment.journal_id.paid_credit_account_id.dep_subdep_flag else False,
                                        }
                                invoice_line.append((0,0,line_vals)) 

                                line_vals = {
                                         'account_id': payment.journal_id.paid_debit_account_id and payment.journal_id.paid_debit_account_id.id or False,
                                         'coa_conac_id': payment.journal_id.conac_paid_debit_account_id and payment.journal_id.conac_paid_debit_account_id.id or False,
                                         'credit': payment.amount_interest, 
                                         'conac_move' : True,
                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                         'move_id' : move_id,
                                         'program_code_id': payment.program_code_id.id,
                                         'dependency_id': payment.dependancy_id.id if payment.journal_id.paid_debit_account_id.dep_subdep_flag else False, 
                                         'sub_dependency_id': payment.sub_dependancy_id.id if payment.journal_id.paid_debit_account_id.dep_subdep_flag else False,
                                        }
                                invoice_line.append((0,0,line_vals))

                                for account_line in payment.afectation_account_id.ie_account_line_ids.filtered(lambda x:x.percentage > 0 and x.main_account==True):
                                    price_per = payment.amount_interest
                                    line_vals = {
                                                     'account_id': account_line.account_id and account_line.account_id.id or False,
                                                     #'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                     'credit': price_per, 
                                                     'conac_move' : False,
                                                     'amount_currency' : 0.0,
                                                     #'currency_id' : payment.currency_id.id,                                     
                                                     'move_id' : move_id,
                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                     'dependency_id':payment.dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                     'sub_dependency_id':payment.sub_dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                     'account_ie_id': payment.afectation_account_id and payment.afectation_account_id.id or False,
                                                     'name': payment.operation_key_id.description+" "+payment.concept,
                                                }
                                    invoice_line.append((0,0,line_vals))
                                # Busqueda de CRI
                                account_code = payment.afectation_account_id.revenue_recognition_account_id.code
                                cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                line_vals = {
                                                     'account_id': payment.afectation_account_id.revenue_recognition_account_id and payment.afectation_account_id.revenue_recognition_account_id.id or False,
                                                     #'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                     'debit': payment.amount_interest, 
                                                     'conac_move' : False,
                                                     'amount_currency' : 0.0,
                                                     #'currency_id' : payment.currency_id.id,                                     
                                                     'move_id' : move_id,
                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                     'dependency_id':payment.dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                     'sub_dependency_id':payment.sub_dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                     'account_ie_id': payment.afectation_account_id and payment.afectation_account_id.id or False,
                                                     'name': payment.operation_key_id.description+" "+payment.concept,
                                                     # Se busca el CRI de la cuenta, si tiene CRI se pega al Asiento contable
                                                     'cri' : cri.name if cri else False
                                                }
                                invoice_line.append((0,0,line_vals))

                                if(payment.afectation_account_id.revenue_recognition_account_id and payment.afectation_account_id.revenue_recognition_account_id.code[0]=='4' and payment.afectation_account_id.revenue_recognition_account_id.budget_registry):
                                        # Busqueda de CRI
                                        account_code = payment.afectation_account_id.revenue_recognition_account_id.code
                                        cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                        if payment_rec_flag and payment_acc_flag:
                                            line_vals= {
                                                         'account_id': payment.income_bank_journal_id.accrued_income_credit_account_id and payment.income_bank_journal_id.accrued_income_credit_account_id.id or False,
                                                         'coa_conac_id': payment.income_bank_journal_id.conac_accrued_income_credit_account_id and payment.income_bank_journal_id.conac_accrued_income_credit_account_id.id or False,
                                                         'debit': payment.amount_interest, 
                                                         'conac_move' : True,
                                                         'amount_currency' : 0.0,
                                                         #'currency_id' : payment.currency_id.id,                                     
                                                         'move_id' : move_id,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                                         'cri':cri.name if cri else False                
                                                        }
                                            invoice_line.append((0,0,line_vals))
                                            line_vals = {
                                                         'account_id': payment.income_bank_journal_id.accrued_income_debit_account_id and payment.income_bank_journal_id.accrued_income_debit_account_id.id or False,
                                                         'coa_conac_id': payment.income_bank_journal_id.conac_accrued_income_debit_account_id and payment.income_bank_journal_id.conac_accrued_income_debit_account_id.id or False,
                                                         'credit': payment.amount_interest,
                                                         'conac_move' : True,
                                                         'amount_currency' : 0.0,
                                                         #'currency_id' : payment.currency_id.id,                                     
                                                         'move_id' : move_id,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                                         'cri':cri.name if cri else False              
                                                        }
                                            invoice_line.append((0,0,line_vals))

                                            line_vals = {
                                                         'account_id': payment.income_bank_journal_id.recover_income_credit_account_id and payment.income_bank_journal_id.recover_income_credit_account_id.id or False,
                                                         'coa_conac_id': payment.income_bank_journal_id.conac_recover_income_credit_account_id and payment.income_bank_journal_id.conac_recover_income_credit_account_id.id or False,
                                                         'debit': payment.amount_interest, 
                                                         'conac_move' : True,
                                                         'amount_currency' : 0.0,
                                                         #'currency_id' : payment.currency_id.id,                                     
                                                         'move_id' : move_id,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                                         'cri':cri.name if cri else False                
                                                                     }
                                            invoice_line.append((0,0,line_vals))
                                            line_vals = {
                                                         'account_id': payment.income_bank_journal_id.recover_income_debit_account_id and payment.income_bank_journal_id.recover_income_debit_account_id.id or False,
                                                         'coa_conac_id': payment.income_bank_journal_id.conac_recover_income_debit_account_id and payment.income_bank_journal_id.conac_recover_income_debit_account_id.id or False,
                                                         'credit': payment.amount_interest,
                                                         'conac_move' : True,
                                                         'amount_currency' : 0.0,
                                                         #'currency_id' : payment.currency_id.id,                                     
                                                         'move_id' : move_id,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                                         'cri':cri.name if cri else False              
                                                        }
                                            invoice_line.append((0,0,line_vals))
                                            affectation_amount = payment.amount_interest
                                            
                                #program_codes.append((payment.program_code_id.id, payment.amount_interest))

                                #payment.env['expenditure.budget.line'].return_available(program_codes, payment.movement_date)


                            elif payment.account_id:
                                # Busqueda de CRI
                                account_code = payment.account_id.code
                                cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                line_vals = {
                                             'account_id': payment.account_id and payment.account_id.id or False,
                                             #'coa_conac_id': payment.income_bank_journal_id.conac_recover_income_debit_account_id and payment.income_bank_journal_id.conac_recover_income_debit_account_id.id or False,
                                             'credit': payment.amount_interest,
                                             'conac_move' : True,
                                             'amount_currency' : 0.0,
                                             #'currency_id' : payment.currency_id.id,                                     
                                             'move_id' : move_id,
                                             'partner_id': payment.partner_id.commercial_partner_id.id,
                                             'dependency_id':payment.dependancy_id.id if payment.account_id.dep_subdep_flag else False,
                                             'sub_dependency_id':payment.sub_dependancy_id.id if payment.account_id.dep_subdep_flag else False,
                                             'name': payment.operation_key_id.description+" "+payment.concept,
                                             # Se busca el CRI de la cuenta, si tiene CRI se pega al Asiento contable
                                             'cri' : cri.name if cri else False              
                                            }
                                invoice_line.append((0,0,line_vals))
                                if(payment.account_id and payment.account_id.code[0]=='4' and payment.account_id.budget_registry):
                                        # Busqueda de CRI
                                        account_code = payment.account_id.code
                                        cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                        if payment_rec_flag and payment_acc_flag:
                                            line_vals= {
                                                         'account_id': payment.income_bank_journal_id.accrued_income_credit_account_id and payment.income_bank_journal_id.accrued_income_credit_account_id.id or False,
                                                         'coa_conac_id': payment.income_bank_journal_id.conac_accrued_income_credit_account_id and payment.income_bank_journal_id.conac_accrued_income_credit_account_id.id or False,
                                                         'credit': payment.amount_interest, 
                                                         'conac_move' : True,
                                                         'amount_currency' : 0.0,
                                                         #'currency_id' : payment.currency_id.id,                                     
                                                         'move_id' : move_id,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                                         'cri':cri.name if cri else False
                                                        }
                                            invoice_line.append((0,0,line_vals))
                                            line_vals = {
                                                         'account_id': payment.income_bank_journal_id.accrued_income_debit_account_id and payment.income_bank_journal_id.accrued_income_debit_account_id.id or False,
                                                         'coa_conac_id': payment.income_bank_journal_id.conac_accrued_income_debit_account_id and payment.income_bank_journal_id.conac_accrued_income_debit_account_id.id or False,
                                                         'debit': payment.amount_interest,
                                                         'conac_move' : True,
                                                         'amount_currency' : 0.0,
                                                         #'currency_id' : payment.currency_id.id,                                     
                                                         'move_id' : move_id,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                                         'cri':cri.name if cri else False             
                                                        }
                                            invoice_line.append((0,0,line_vals))

                                            line_vals = {
                                                         'account_id': payment.income_bank_journal_id.recover_income_credit_account_id and payment.income_bank_journal_id.recover_income_credit_account_id.id or False,
                                                         'coa_conac_id': payment.income_bank_journal_id.conac_recover_income_credit_account_id and payment.income_bank_journal_id.conac_recover_income_credit_account_id.id or False,
                                                         'credit': payment.amount_interest, 
                                                         'conac_move' : True,
                                                         'amount_currency' : 0.0,
                                                         #'currency_id' : payment.currency_id.id,                                     
                                                         'move_id' : move_id,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                                         'cri':cri.name if cri else False               
                                                                     }
                                            invoice_line.append((0,0,line_vals))
                                            line_vals = {
                                                         'account_id': payment.income_bank_journal_id.recover_income_debit_account_id and payment.income_bank_journal_id.recover_income_debit_account_id.id or False,
                                                         'coa_conac_id': payment.income_bank_journal_id.conac_recover_income_debit_account_id and payment.income_bank_journal_id.conac_recover_income_debit_account_id.id or False,
                                                         'debit': payment.amount_interest,
                                                         'conac_move' : True,
                                                         'amount_currency' : 0.0,
                                                         #'currency_id' : payment.currency_id.id,                                     
                                                         'move_id' : move_id,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                                         'cri':cri.name if cri else False             
                                                        }
                                            invoice_line.append((0,0,line_vals))
                                            affectation_amount = payment.amount_interest
                            
                            #Certificados 30
                            elif payment.operation_key_id.name == '00010030':
                                if not payment.special_certificates_line_ids:
                                    if self.env.user.lang == 'es_MX':
                                        raise ValidationError(_("No se puede validar el certificado sino tiene el detalle de tienda UNAM"))
                                    else:
                                        raise ValidationError(_("Unable to validate the certificate."))
                                certificate_setting = self.env['certificate.settings'].search([('certificate_type', '=', payment.operation_key_id.id)])
                                for line_certificate in payment.special_certificates_line_ids:
                                    if line_certificate.dependency_id.dependency == '742' and line_certificate.sub_dependency_id.sub_dependency == '01':
                                        # Busqueda de CRI
                                        account_code = certificate_setting.dgpu_account_id.code
                                        cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                        line_vals = {
                                                 'account_id': certificate_setting.dgpu_account_id and certificate_setting.dgpu_account_id.id or False,
                                                 #'coa_conac_id': payment.income_bank_journal_id.conac_recover_income_debit_account_id and payment.income_bank_journal_id.conac_recover_income_debit_account_id.id or False,
                                                 'credit': line_certificate.amount,
                                                 'conac_move' : True,                                     
                                                 'move_id' : move_id,
                                                 'partner_id': payment.partner_id.commercial_partner_id.id,
                                                 'dependency_id':line_certificate.dependency_id.id if distribution_line.account_id.dep_subdep_flag else False,
                                                 'sub_dependency_id':line_certificate.sub_dependency_id.id if distribution_line.account_id.dep_subdep_flag else False,
                                                 'name': payment.operation_key_id.description+" "+payment.concept,
                                                 # Se busca el CRI de la cuenta, si tiene CRI se pega al Asiento contable
                                                 'cri': cri.name if cri else False                              
                                                }
                                        invoice_line.append((0,0,line_vals))
                                        if(certificate_setting.dgpu_account_id.code[0]=='4' and certificate_setting.dgpu_account_id.budget_registry):
                                            # Busqueda de CRI
                                            account_code = certificate_setting.dgpu_account_id.code
                                            cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                            line_vals= {
                                                         'account_id': payment.income_bank_journal_id.accrued_income_credit_account_id and payment.income_bank_journal_id.accrued_income_credit_account_id.id or False,
                                                         'coa_conac_id': payment.income_bank_journal_id.conac_accrued_income_credit_account_id and payment.income_bank_journal_id.conac_accrued_income_credit_account_id.id or False,
                                                         'credit': line_certificate.amount, 
                                                         'conac_move' : True,
                                                         'amount_currency' : 0.0,
                                                         #'currency_id' : payment.currency_id.id,                                     
                                                         'move_id' : move_id,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                                         'cri':cri.name if cri else False               
                                                        }
                                            invoice_line.append((0,0,line_vals))
                                            line_vals = {
                                                         'account_id': payment.income_bank_journal_id.accrued_income_debit_account_id and payment.income_bank_journal_id.accrued_income_debit_account_id.id or False,
                                                         'coa_conac_id': payment.income_bank_journal_id.conac_accrued_income_debit_account_id and payment.income_bank_journal_id.conac_accrued_income_debit_account_id.id or False,
                                                         'debit': line_certificate.amount,
                                                         'conac_move' : True,
                                                         'amount_currency' : 0.0,
                                                         #'currency_id' : payment.currency_id.id,                                     
                                                         'move_id' : move_id,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                                         'cri':cri.name if cri else False                              
                                                        }
                                            invoice_line.append((0,0,line_vals))

                                            line_vals = {
                                                         'account_id': payment.income_bank_journal_id.recover_income_credit_account_id and payment.income_bank_journal_id.recover_income_credit_account_id.id or False,
                                                         'coa_conac_id': payment.income_bank_journal_id.conac_recover_income_credit_account_id and payment.income_bank_journal_id.conac_recover_income_credit_account_id.id or False,
                                                         'credit': line_certificate.amount, 
                                                         'conac_move' : True,
                                                         'amount_currency' : 0.0,
                                                         #'currency_id' : payment.currency_id.id,                                     
                                                         'move_id' : move_id,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                                         'cri':cri.name if cri else False                               
                                                                     }
                                            invoice_line.append((0,0,line_vals))
                                            line_vals = {
                                                         'account_id': payment.income_bank_journal_id.recover_income_debit_account_id and payment.income_bank_journal_id.recover_income_debit_account_id.id or False,
                                                         'coa_conac_id': payment.income_bank_journal_id.conac_recover_income_debit_account_id and payment.income_bank_journal_id.conac_recover_income_debit_account_id.id or False,
                                                         'debit': line_certificate.amount,
                                                         'conac_move' : True,
                                                         'amount_currency' : 0.0,
                                                         #'currency_id' : payment.currency_id.id,                                     
                                                         'move_id' : move_id,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'name': payment.operation_key_id.description+" "+payment.concept,
                                                         'cri':cri.name if cri else False                              
                                                        }
                                            invoice_line.append((0,0,line_vals))
                                            affectation_amount = line_certificate.amount

                                    else:
                                        for distribution_line in certificate_setting.certificates_settings_line_ids.filtered(lambda x:x.percentage == 100.0):
                                            line_vals = {
                                                 'account_id': distribution_line.account_id and distribution_line.account_id.id or False,
                                                 #'coa_conac_id': payment.income_bank_journal_id.conac_recover_income_debit_account_id and payment.income_bank_journal_id.conac_recover_income_debit_account_id.id or False,
                                                 'credit': line_certificate.amount,
                                                 'conac_move' : True,                                     
                                                 'move_id' : move_id,
                                                 'partner_id': payment.partner_id.commercial_partner_id.id,
                                                 'dependency_id':line_certificate.dependency_id.id if distribution_line.account_id.dep_subdep_flag else False,
                                                 'sub_dependency_id':line_certificate.sub_dependency_id.id if distribution_line.account_id.dep_subdep_flag else False,
                                                 'name': payment.operation_key_id.description+" "+payment.concept,
                                                               
                                                }
                                            invoice_line.append((0,0,line_vals))

                                        for distribution_line in certificate_setting.certificates_settings_line_ids.filtered(lambda x:x.percentage != 100.0):
                                            price_per = (line_certificate.amount*distribution_line.percentage)/100 
                                            line_vals = {
                                                 'account_id': distribution_line.account_id and distribution_line.account_id.id or False,
                                                 #'coa_conac_id': payment.income_bank_journal_id.conac_recover_income_debit_account_id and payment.income_bank_journal_id.conac_recover_income_debit_account_id.id or False,
                                                 'debit': price_per,
                                                 'conac_move' : True,                                     
                                                 'move_id' : move_id,
                                                 'partner_id': payment.partner_id.commercial_partner_id.id,
                                                 'dependency_id':line_certificate.dependency_id.id if distribution_line.account_id.dep_subdep_flag else False,
                                                 'sub_dependency_id':line_certificate.sub_dependency_id.id if distribution_line.account_id.dep_subdep_flag else False,
                                                 'name': payment.operation_key_id.description+" "+payment.concept,
                                                               
                                                }
                                            invoice_line.append((0,0,line_vals))

                                            if distribution_line.bridge_account_id:
                                                price_bridge = (line_certificate.amount*distribution_line.percentage_bridge_account)/100
                                                if distribution_line.bridge_dependency_id:
                                                    dep = distribution_line.bridge_dependency_id.id
                                                    subdep = distribution_line.bridge_sub_dependency_id.id
                                                else:
                                                    dep = line_certificate.dependency_id.id
                                                    subdep = line_certificate.sub_dependency_id.id

                                                line_vals = {
                                                     'account_id': distribution_line.bridge_account_id and distribution_line.bridge_account_id.id or False,
                                                     #'coa_conac_id': payment.income_bank_journal_id.conac_recover_income_debit_account_id and payment.income_bank_journal_id.conac_recover_income_debit_account_id.id or False,
                                                     'credit': price_bridge,
                                                     'conac_move' : True,                                     
                                                     'move_id' : move_id,
                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                     'dependency_id': dep if distribution_line.bridge_account_id.dep_subdep_flag else False,
                                                     'sub_dependency_id': subdep if distribution_line.bridge_account_id.dep_subdep_flag else False,
                                                     'name': payment.operation_key_id.description+" "+payment.concept,
                                                                   
                                                    }
                                                invoice_line.append((0,0,line_vals))

                            
                            payment.line_ids = invoice_line



                    else:
                        #Comienza función de servicios de educación
                        origin = 'income-se'
                        
                        name = self.assign_name_education_servs(payment.reference_plugin, payment.reference_plugin_2, payment.reference_plugin_3, payment.description_of_the_movement)

                        if not journal.update_date_movements or journal.update_date_movements =='':  
                            journal.update_date_movements = payment.movement_date
                        if journal.aux_number_movements_comission <= 0:
                            if (journal.update_date_movements.strftime("%Y")<payment.movement_date.strftime("%Y")) or (journal.update_date_movements.strftime("%m")<payment.movement_date.strftime("%m")):
                                journal.aux_number_movements_comission = journal.number_movements_comission
                        journal.update_date_movements = payment.movement_date

                        if payment.comission_id and journal.aux_number_movements_comission==0 and payment.comission_id.comission_amount<payment.amount_interest:
                            comission = payment.comission_id
                            comission_amount = payment.comission_id.comission_amount*1.16
                            comission_amount_without_iva = comission.comission_amount
                            comission_amount_iva = comission_amount_without_iva*0.16
                            comission_flag = True
                            normal_process=True
                            payment.comission_charged='charged'
                        elif payment.comission_id and journal.aux_number_movements_comission==0 and payment.comission_id.comission_amount>payment.amount_interest:
                            comission_flag = False 
                            payment.comission_charged='charged'
                            normal_process=False
                        else:
                            comission_flag = False
                            normal_process=True 
                            payment.comission_charged='no_charged' 

                        if payment.afectation_account_id and payment.afectation_account_id.ie_account_line_ids:
                            total_line_pr = sum(x.percentage for x in payment.afectation_account_id.ie_account_line_ids)
                            if total_line_pr != 100:
                                raise ValidationError(_('Please configure IE Accounts 100% Percentage'))
                            if payment.afectation_account_id.base_amount>0.00:
                                base_amount_payment = payment.afectation_account_id.base_amount
                            else:
                                base_amount_payment = payment.amount_interest
                            if base_amount_payment<payment.amount_interest and payment.type_of_revenue_collection == 'aspirants':
                                aspirants_amount = payment.amount_interest-base_amount_payment
                                aspirants_flag = True
                            else:
                                base_amount_payment = payment.amount_interest
                                aspirants_flag = False

                            if payment.type_of_revenue_collection == 'aspirants':
                                if payment.afectation_account_id.base_amount>0.00:
                                    aspirants_amount = payment.afectation_account_id.base_amount
                                elif aspirants_amount<payment.amount_interest:
                                    base_amount_payment = payment.amount_interest-aspirants_amount
                                    aspirants_flag = True
                                else:
                                    base_amount_payment = payment.amount_interest
                                    aspirants_flag = False

                            if payment.income_bank_journal_id and payment.income_bank_journal_id.default_debit_account_id:
                                    # Busqueda de CRI
                                    account_code_debit = payment.income_bank_journal_id.default_debit_account_id.id
                                    account_code_credit = payment.income_bank_journal_id.default_credit_account_id.id
                                    cri = self.env[model_budget].search([]).associate_cri_account(account_code_debit,account_code_credit)
                                    
                                    line_vals = {
                                                 'name': name,   
                                                 'account_id': payment.income_bank_journal_id.default_debit_account_id and payment.income_bank_journal_id.default_debit_account_id.id or False,
                                                 'coa_conac_id': payment.income_bank_journal_id.conac_debit_account_id and payment.income_bank_journal_id.conac_debit_account_id.id or False,
                                                 'debit': payment.amount_interest,
                                                 'conac_move' : True,
                                                 'amount_currency' : counterpart_amount,                                   
                                                 'move_id' : move_id,
                                                 'partner_id': payment.partner_id.commercial_partner_id.id,
                                                 'cri' : cri.name if cri else False
                                                }
                                    invoice_line.append((0,0,line_vals))

                            if aspirants_flag and normal_process:
                                for account_line in payment.afectation_account_id.ie_account_line_ids.filtered(lambda x:x.percentage == 0.0):
                                    # Busqueda de CRI
                                    account_code = account_line.account_id.code
                                    cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                    # logging.info(f'Cuenta/account_line.account_id: {account_line.account_id.code.code} CRI: {cri.name}')
                                    price_per = (aspirants_amount*(100.00-account_line.percentage_bridge_account))/100
                                    price_bridge = (aspirants_amount*account_line.percentage_bridge_account)/100
                                    line_vals = {   'name': name,
                                                    'account_id': account_line.account_id and account_line.account_id.id or False,
                                                     #'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                     'credit': price_per, 
                                                     'conac_move' : False,
                                                     'amount_currency' : 0.0,
                                                     #'currency_id' : payment.currency_id.id,                                     
                                                     'move_id' : move_id,
                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                     'dependency_id':payment.dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                     'sub_dependency_id':payment.sub_dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                     'account_ie_id': payment.afectation_account_id and payment.afectation_account_id.id or False,
                                                     'cri': cri.name if cri else False
                                                }
                                    invoice_line.append((0,0,line_vals))

                                    if account_line.bridge_account_id and account_line.bridge_account_id.id:
                                        # Busqueda de CRI
                                        account_code = account_line.bridge_account_id.code
                                        cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                        
                                        line_vals = {   'name': name,
                                                        'account_id': account_line.bridge_account_id and account_line.bridge_account_id.id or False,
                                                         #'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                         'credit': price_bridge, 
                                                         'conac_move' : False,
                                                         'amount_currency' : 0.0,
                                                         #'currency_id' : payment.currency_id.id,                                     
                                                         'move_id' : move_id,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'dependency_id':payment.dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                         'sub_dependency_id':payment.sub_dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                         'cri': cri.name if cri else False
                                                    }
                                        invoice_line.append((0,0,line_vals))

                                    if(account_line.account_id.code[0]=='4' and account_line.account_id.budget_registry):
                                        # Busqueda de CRI
                                        account_code = account_line.account_id.code
                                        
                                        cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                        if payment_rec_flag and payment_acc_flag:
                                            line_vals= { 'name': name,   
                                                         'account_id': payment.income_bank_journal_id.accrued_income_credit_account_id and payment.income_bank_journal_id.accrued_income_credit_account_id.id or False,
                                                         'coa_conac_id': payment.income_bank_journal_id.conac_accrued_income_credit_account_id and payment.income_bank_journal_id.conac_accrued_income_credit_account_id.id or False,
                                                         'credit': price_per, 
                                                         'conac_move' : True,
                                                         'amount_currency' : 0.0,
                                                         #'currency_id' : payment.currency_id.id,                                     
                                                         'move_id' : move_id,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'cri':cri.name if cri else False               
                                                        }
                                            invoice_line.append((0,0,line_vals))
                                            line_vals = {'name': name,
                                                         'account_id': payment.income_bank_journal_id.accrued_income_debit_account_id and payment.income_bank_journal_id.accrued_income_debit_account_id.id or False,
                                                         'coa_conac_id': payment.income_bank_journal_id.conac_accrued_income_debit_account_id and payment.income_bank_journal_id.conac_accrued_income_debit_account_id.id or False,
                                                         'debit': price_per,
                                                         'conac_move' : True,
                                                         'amount_currency' : 0.0,
                                                         #'currency_id' : payment.currency_id.id,                                     
                                                         'move_id' : move_id,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'cri':cri.name if cri else False              
                                                        }
                                            invoice_line.append((0,0,line_vals))

                                            line_vals = {'name': name,
                                                         'account_id': payment.income_bank_journal_id.recover_income_credit_account_id and payment.income_bank_journal_id.recover_income_credit_account_id.id or False,
                                                         'coa_conac_id': payment.income_bank_journal_id.conac_recover_income_credit_account_id and payment.income_bank_journal_id.conac_recover_income_credit_account_id.id or False,
                                                         'credit': price_per, 
                                                         'conac_move' : True,
                                                         'amount_currency' : 0.0,
                                                         #'currency_id' : payment.currency_id.id,                                     
                                                         'move_id' : move_id,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'cri':cri.name if cri else False               
                                                                     }
                                            invoice_line.append((0,0,line_vals))
                                            line_vals = {'name': name,
                                                         'account_id': payment.income_bank_journal_id.recover_income_debit_account_id and payment.income_bank_journal_id.recover_income_debit_account_id.id or False,
                                                         'coa_conac_id': payment.income_bank_journal_id.conac_recover_income_debit_account_id and payment.income_bank_journal_id.conac_recover_income_debit_account_id.id or False,
                                                         'debit': price_per,
                                                         'conac_move' : True,
                                                         'amount_currency' : 0.0,
                                                         #'currency_id' : payment.currency_id.id,                                     
                                                         'move_id' : move_id,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'cri':cri.name if cri else False              
                                                        }
                                            invoice_line.append((0,0,line_vals))
                                            affectation_amount = self.amount_interest
                            if comission_flag and base_amount_payment>comission_amount and normal_process:
                                base_amount_payment = base_amount_payment-comission_amount
                                for comission_line in comission.comission_line_ids.filtered(lambda x:x.comission_percentage > 0):
                                    # Busqueda de CRI
                                    account_code = comission_line.account_id.code
                                    cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                    
                                    line_vals = {
                                                    'account_id': comission_line.account_id and comission_line.account_id.id or False,
                                                     #'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                     'credit': comission_amount_without_iva, 
                                                     'conac_move' : False,
                                                     'amount_currency' : 0.0,
                                                     #'currency_id' : payment.currency_id.id,
                                                     'name': 'Comisión cobrada',                                    
                                                     'move_id' : move_id,
                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                     'dependency_id':payment.dependancy_id.id if comission_line.account_id.dep_subdep_flag else False,
                                                     'sub_dependency_id':payment.sub_dependancy_id.id if comission_line.account_id.dep_subdep_flag else False,
                                                     'cri': cri.name if cri else False
                                                }
                                    invoice_line.append((0,0,line_vals))
                                    line_vals = {
                                                    'account_id': comission_line.account_id and comission_line.account_id.id or False,
                                                     #'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                     'credit': comission_amount_iva, 
                                                     'conac_move' : False,
                                                     'amount_currency' : 0.0,
                                                     #'currency_id' : payment.currency_id.id,
                                                     'name': 'IVA de la comisión',                                   
                                                     'move_id' : move_id,
                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                     'dependency_id':payment.dependancy_id.id if comission_line.account_id.dep_subdep_flag else False,
                                                     'sub_dependency_id':payment.sub_dependancy_id.id if comission_line.account_id.dep_subdep_flag else False,
                                                     'cri': cri.name if cri else False
                                                }
                                    invoice_line.append((0,0,line_vals))

                                    if(comission_line.account_id.code[0]=='4' and comission_line.account_id.budget_registry):
                                        # Busqueda de CRI
                                        account_code = comission_line.account_id.code
                                        cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                        if payment_rec_flag and payment_acc_flag:
                                            line_vals = {                'name': name,
                                                                         'account_id': payment.income_bank_journal_id.accrued_income_credit_account_id and payment.income_bank_journal_id.accrued_income_credit_account_id.id or False,
                                                                         'coa_conac_id': payment.income_bank_journal_id.conac_accrued_income_credit_account_id and payment.income_bank_journal_id.conac_accrued_income_credit_account_id.id or False,
                                                                         'credit': comission_amount, 
                                                                         'conac_move' : True,
                                                                         'amount_currency' : 0.0,
                                                                         #'currency_id' : payment.currency_id.id,                                     
                                                                         'move_id' : move_id,
                                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                         'cri':cri.name if cri else False
                                                                     }
                                            invoice_line.append((0,0,line_vals)) 
                                            line_vals = {                'name': name,
                                                                         'account_id': payment.income_bank_journal_id.accrued_income_debit_account_id and payment.income_bank_journal_id.accrued_income_debit_account_id.id or False,
                                                                         'coa_conac_id': payment.income_bank_journal_id.conac_accrued_income_debit_account_id and payment.income_bank_journal_id.conac_accrued_income_debit_account_id.id or False,
                                                                         'debit': comission_amount,
                                                                         'conac_move' : True,
                                                                         'amount_currency' : 0.0,
                                                                         #'currency_id' : payment.currency_id.id,                                     
                                                                         'move_id' : move_id,
                                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                         'cri':cri.name if cri else False
                                                                     }
                                            invoice_line.append((0,0,line_vals))

                                            line_vals = {                'name': name,
                                                                         'account_id': payment.income_bank_journal_id.recover_income_credit_account_id and payment.income_bank_journal_id.recover_income_credit_account_id.id or False,
                                                                         'coa_conac_id': payment.income_bank_journal_id.conac_recover_income_credit_account_id and payment.income_bank_journal_id.conac_recover_income_credit_account_id.id or False,
                                                                         'credit': comission_amount, 
                                                                         'conac_move' : True,
                                                                         'amount_currency' : 0.0,
                                                                         #'currency_id' : payment.currency_id.id,                                     
                                                                         'move_id' : move_id,
                                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                         'cri':cri.name if cri else False
                                                                     }
                                            invoice_line.append((0,0,line_vals)) 
                                            line_vals = {                'name': name,
                                                                         'account_id': payment.income_bank_journal_id.recover_income_debit_account_id and payment.income_bank_journal_id.recover_income_debit_account_id.id or False,
                                                                         'coa_conac_id': payment.income_bank_journal_id.conac_recover_income_debit_account_id and payment.income_bank_journal_id.conac_recover_income_debit_account_id.id or False,
                                                                         'debit': comission_amount,
                                                                         'conac_move' : True,
                                                                         'amount_currency' : 0.0,
                                                                         #'currency_id' : payment.currency_id.id,                                     
                                                                         'move_id' : move_id,
                                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                         'cri':cri.name if cri else False
                                                                     }
                                            invoice_line.append((0,0,line_vals))
                                            affectation_amount = self.amount_interest

                            if normal_process:
                                for account_line in payment.afectation_account_id.ie_account_line_ids.filtered(lambda x:x.percentage > 0):
                                    price_per = (base_amount_payment*account_line.percentage)/100 
                                    # Busqueda de CRI
                                    account_code = account_line.account_id.code
                                    if account_code[0]=='4':
                                        cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                    line_vals = {    'name': name,
                                                     'account_id': account_line.account_id and account_line.account_id.id or False,
                                                     #'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                     'credit': price_per, 
                                                     'conac_move' : False,
                                                     'amount_currency' : 0.0,
                                                     #'currency_id' : payment.currency_id.id,                                     
                                                     'move_id' : move_id,
                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                     'dependency_id':payment.dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                     'sub_dependency_id':payment.sub_dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                     'account_ie_id': payment.afectation_account_id and payment.afectation_account_id.id or False,
                                                }
                                    invoice_line.append((0,0,line_vals))

                                    if payment.type_of_revenue_collection == 'education_servs' and account_line.bridge_account_id and account_line.bridge_account_id.id:
                                        line_vals = {   'name': name,
                                                        'account_id': account_line.account_id and account_line.account_id.id or False,
                                                         #'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                         'debit': price_per, 
                                                         'conac_move' : False,
                                                         'amount_currency' : 0.0,
                                                         #'currency_id' : payment.currency_id.id,                                     
                                                         'move_id' : move_id,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'dependency_id':payment.dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                         'sub_dependency_id':payment.sub_dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                         'account_ie_id': payment.afectation_account_id and payment.afectation_account_id.id or False,
                                                    }
                                        invoice_line.append((0,0,line_vals))

                                        line_vals = {   'name': name,
                                                        'account_id': account_line.bridge_account_id and account_line.bridge_account_id.id or False,
                                                         #'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                         'credit': price_per, 
                                                         'conac_move' : False,
                                                         'amount_currency' : 0.0,
                                                         #'currency_id' : payment.currency_id.id,                                     
                                                         'move_id' : move_id,
                                                         'partner_id': payment.partner_id.commercial_partner_id.id,
                                                         'dependency_id':payment.dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                         'sub_dependency_id':payment.sub_dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                         'account_ie_id': payment.afectation_account_id and payment.afectation_account_id.id or False,
                                                    }
                                        invoice_line.append((0,0,line_vals))

                                    if(account_line.account_id.code[0]=='2' and account_line.account_id.code[1]=='1' and account_line.account_id.code[2]=='3'):
                                        dep_213 = self.env['dependency']
                                        id_dep_213 = dep_213.search([('dependency','=', '764')])[0]
                                        sub_dep_213 = self.env['sub.dependency']
                                        id_sub_dep_213 = sub_dep_213.search([('dependency_id','=', id_dep_213['id']),('sub_dependency','=','01')])[0]

                                        line_vals = {'name': name,
                                                     'account_id': account_line.account_id and account_line.account_id.id or False,
                                                     #'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                     'debit': price_per, 
                                                     'conac_move' : False,
                                                     'amount_currency' : 0.0,
                                                     #'currency_id' : payment.currency_id.id,                                     
                                                     'move_id' : move_id,
                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                     'dependency_id':payment.dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                     'sub_dependency_id':payment.sub_dependancy_id.id if account_line.account_id.dep_subdep_flag else False,
                                                     'account_ie_id': payment.afectation_account_id and payment.afectation_account_id.id or False,
                                                }
                                        invoice_line.append((0,0,line_vals))

                                        line_vals = {'name': name,
                                                     'account_id': account_line.account_id and account_line.account_id.id or False,
                                                     #'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                     'credit': price_per, 
                                                     'conac_move' : False,
                                                     'amount_currency' : 0.0,
                                                     #'currency_id' : payment.currency_id.id,                                     
                                                     'move_id' : move_id,
                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                     'dependency_id': id_dep_213['id'],
                                                     'sub_dependency_id': id_sub_dep_213['id'],
                                                     'account_ie_id': payment.afectation_account_id and payment.afectation_account_id.id or False,
                                                }
                                        invoice_line.append((0,0,line_vals))



                                    if(account_line.account_id.code[0]=='4' and account_line.account_id.budget_registry):
                                            # Busqueda de CRI
                                            account_code = account_line.account_id.code
                                            cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                            if payment_rec_flag and payment_acc_flag:
                                                line_vals = {                'name': name,
                                                                             'account_id': payment.income_bank_journal_id.accrued_income_credit_account_id and payment.income_bank_journal_id.accrued_income_credit_account_id.id or False,
                                                                             'coa_conac_id': payment.income_bank_journal_id.conac_accrued_income_credit_account_id and payment.income_bank_journal_id.conac_accrued_income_credit_account_id.id or False,
                                                                             'credit': price_per, 
                                                                             'conac_move' : True,
                                                                             'amount_currency' : 0.0,
                                                                             #'currency_id' : payment.currency_id.id,                                     
                                                                             'move_id' : move_id,
                                                                             'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                             'cri':cri.name if cri else False
                                                                         }
                                                invoice_line.append((0,0,line_vals))
                                                line_vals = {                'name': name,
                                                                             'account_id': payment.income_bank_journal_id.accrued_income_debit_account_id and payment.income_bank_journal_id.accrued_income_debit_account_id.id or False,
                                                                             'coa_conac_id': payment.income_bank_journal_id.conac_accrued_income_debit_account_id and payment.income_bank_journal_id.conac_accrued_income_debit_account_id.id or False,
                                                                             'debit': price_per,
                                                                             'conac_move' : True,
                                                                             'amount_currency' : 0.0,
                                                                             #'currency_id' : payment.currency_id.id,                                     
                                                                             'move_id' : move_id,
                                                                             'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                             'cri':cri.name if cri else False
                                                                         }
                                                invoice_line.append((0,0,line_vals))

                                                line_vals = {                'name': name,
                                                                             'account_id': payment.income_bank_journal_id.recover_income_credit_account_id and payment.income_bank_journal_id.recover_income_credit_account_id.id or False,
                                                                             'coa_conac_id': payment.income_bank_journal_id.conac_recover_income_credit_account_id and payment.income_bank_journal_id.conac_recover_income_credit_account_id.id or False,
                                                                             'credit': price_per, 
                                                                             'conac_move' : True,
                                                                             'amount_currency' : 0.0,
                                                                             #'currency_id' : payment.currency_id.id,                                     
                                                                             'move_id' : move_id,
                                                                            'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                             'cri':cri.name if cri else False
                                                                         }
                                                invoice_line.append((0,0,line_vals))
                                                line_vals = {                'name': name,
                                                                             'account_id': payment.income_bank_journal_id.recover_income_debit_account_id and payment.income_bank_journal_id.recover_income_debit_account_id.id or False,
                                                                             'coa_conac_id': payment.income_bank_journal_id.conac_recover_income_debit_account_id and payment.income_bank_journal_id.conac_recover_income_debit_account_id.id or False,
                                                                             'debit': price_per,
                                                                             'conac_move' : True,
                                                                             'amount_currency' : 0.0,
                                                                             #'currency_id' : payment.currency_id.id,                                     
                                                                             'move_id' : move_id,
                                                                             'partner_id': payment.partner_id.commercial_partner_id.id,
                                                                             'cri':cri.name if cri else False
                                                                         }
                                                invoice_line.append((0,0,line_vals))
                                                affectation_amount = self.amount_interest
                            else:
                                comission=payment.comission_id
                                for comission_line in comission.comission_line_ids.filtered(lambda x:x.comission_percentage > 0):
                                    # Busqueda de CRI
                                    account_code = comission_line.account_id.code
                                    cri = self.env[model_budget].search([]).associate_cri_account_code(account_code)
                                    line_vals = {
                                                    'account_id': comission_line.account_id and comission_line.account_id.id or False,
                                                     #'coa_conac_id': payment.journal_id.conac_recover_income_credit_account_id and payment.journal_id.conac_recover_income_credit_account_id.id or False,
                                                     'credit': payment.amount_interest, 
                                                     'conac_move' : False,
                                                     'amount_currency' : 0.0,
                                                     #'currency_id' : payment.currency_id.id,
                                                     'name': 'Comisión cobrada',                                    
                                                     'move_id' : move_id,
                                                     'partner_id': payment.partner_id.commercial_partner_id.id,
                                                     'dependency_id':payment.dependancy_id.id if comission_line.account_id.dep_subdep_flag else False,
                                                     'sub_dependency_id':payment.sub_dependancy_id.id if comission_line.account_id.dep_subdep_flag else False,
                                                     'cri': cri.name if cri else False
                                                }
                                    invoice_line.append((0,0,line_vals))

                            payment.with_context(check_move_validity=False).line_ids = invoice_line
                    date_affectation = payment.date
                    payment.action_post()
                    if cri:
                        # Objetos
                        obj_affetation = self.env[model_budget]
                        obj_logbook = self.env['income.adequacies.function']
                        obj_fiscal_year = self.env['account.fiscal.year']
                        # Afectación al presupuesto
                        type_affectation = 'eje-rec'
                        date_accrued = False
                        amount = affectation_amount
                        # Mandar datos a la clase de afectación
                        cri_id = cri.id
                        
                        obj_affetation.search([]).affectation_income_budget(type_affectation,cri_id,date_accrued,date_affectation,amount)
                        # Guarda la bitacora de la adecuación
                        # Busca el id del año fiscal
                        year = date_affectation.year
                        fiscal_year_id = obj_fiscal_year.search([('name', '=', year)])
                        type_cri = 'income'
                        movement = 'for_exercising_collected'
                        origin_type = origin
                        id_cri = self.name
                        journal_id = journal.id
                        update_date_record = datetime.today()
                        update_date_income = datetime.today()
                        obj_logbook.search([]).logbook_income_budget(fiscal_year_id.id,cri_id,date_affectation,type_cri,movement,origin_type,id_cri,journal_id,update_date_record,amount,update_date_income) 
                    
                    if journal.aux_number_movements_comission>0:
                        journal.aux_number_movements_comission = journal.aux_number_movements_comission-1
                    


            else:
                payment.action_post()