# -*- coding: utf-8 -*-
import json
import logging
from odoo import models, fields, api, _
from datetime import datetime
class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    # Campo de referencia de los asientos contables con la cuenta por pagar
    account_to_pay_id = fields.Many2one('account.topay')

    account_to_pay_invoice_id = fields.Many2one('account.topay', store=True)
class AccountToPayLinelinks(models.Model):

    _name = 'account.to.pay.line.links'

    account_to_pay_line_id = fields.Many2one('account.topay.line','Budget Line')
    account_move_id = fields.Many2one('account.move','Budget Line')
    amount = fields.Float('Amount')
