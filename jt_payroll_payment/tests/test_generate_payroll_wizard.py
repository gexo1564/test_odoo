from odoo.tests import TransactionCase
from odoo.tests import tagged

@tagged('jt_payroll_payment')
class GeneratePayrollWizard(TransactionCase):
    ## Nuevos Unit test ##
    def test_generate_query1(self):
        postgres_employee_bank_id_list = ["(1, 2) group by rb.l10n_mx_edi_code; SELECT id FROM productive_account where id = 1--",]        
        current_query = """SELECT rb.l10n_mx_edi_code, STRING_AGG(concat(cl.folio, ',', cl.id), ';') 
        FROM check_log cl INNER JOIN res_partner_bank rpb on cl.bank_account_id = rpb.id 
        INNER JOIN res_bank rb on rpb.bank_id = rb.id where cl.status IN 
        ('Available for printing') 
        AND cl.general_status = 'available' AND rb.id IN %(postgres_employee_bank_id_list)s group by rb.l10n_mx_edi_code"""
        
        params = {'postgres_employee_bank_id_list': tuple(postgres_employee_bank_id_list)}
        self.env.cr.execute(current_query, params)

        resultados = self.env.cr.fetchall()
        self.assertEqual(len(resultados), 0)
        print(F"PRUEBA DE QUERY SANITIZADO 1 generate_payroll_wizard\nRESULTADOS:{len(resultados)}")


    def test_generate_query2(self):
        postgres_alimony_bank_id_list = ["(1, 2) GROUP BY br.l10n_mx_edi_code; SELECT id FROM productive_account where id = 1--",]

        current_query = """SELECT rb.l10n_mx_edi_code, STRING_AGG(concat(cl.folio, ',', cl.id), ';') 
        FROM check_log cl INNER JOIN res_partner_bank rpb on cl.bank_account_id = rpb.id 
        INNER JOIN res_bank rb on rpb.bank_id = rb.id where cl.status IN 
        ('Available for printing') 
        AND cl.general_status = 'available' AND rb.id IN %(postgres_alimony_bank_id_list)s group by rb.l10n_mx_edi_code"""

        params = {'postgres_alimony_bank_id_list': tuple(postgres_alimony_bank_id_list)}
        self.env.cr.execute(current_query, params)
        
        resultados = self.env.cr.fetchall()
        self.assertEqual(len(resultados), 0)
        print(F"PRUEBA DE QUERY SANITIZADO 2 generate_payroll_wizard\nRESULTADOS:{len(resultados)}")