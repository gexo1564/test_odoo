# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
import re

from odoo.addons.siif_security.tools.utils import check_authorization, check_only_allowed_fields
from datetime import datetime
class BudgetProgramConversion(models.Model):

    _name = 'budget.program.conversion'
    _description = 'Budget Program Conversion'
    _rec_name = 'shcp'

    unam_key_id = fields.Many2one('program', string='P')
    unam_key_code = fields.Char(related='unam_key_id.key_unam')
    program_key_id = fields.Many2one('program.key','UNAM Function')

    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')
    
    desc = fields.Text(string='Description of key UNAM')
    shcp = fields.Many2one("shcp.code", string='Conversion of SHCP program')
    shcp_name = fields.Char(related='shcp.name')
    
    description = fields.Text(string='Description conversion of SHCP program')

    dep_con_id = fields.Many2one('departure.conversion','SI')
    
    conversion_key_id = fields.Many2one('shcp.game','Conversion with Item')
    federal_part = fields.Char(related='conversion_key_id.conversion_key')
    
    #federal_part = fields.Char(related='dep_con_id.federal_part')
    
    federal_part_desc = fields.Text(related='conversion_key_id.conversion_key_desc',string="Item SHCP Description")
    
    _sql_constraints = [('uniq_unam_key_id', 'unique(program_key_id,shcp,conversion_key_id, start_date, end_date)',
                         'The combination of UNAM function, Conversion with Item and Conversion of SHCP must be unique')]

    @api.constrains('start_date', 'end_date')
    def check_end_date(self):
        if not self.start_date:
            raise ValidationError(_("The start date of validity is required."))
        if self.end_date and self.end_date <= self.start_date:
            raise ValidationError(_("The effective end date cannot be less than or equal to the effective date."))

    @api.constrains('shcp')
    def _check_shcp(self):
        if self.shcp:
            # To check size of the position is exact 4
            if len(self.shcp.name) != 4:
                raise ValidationError(_('The Conversion of SHCP program value size must be four.'))
            if self.shcp.name and len(self.shcp.name) == 4:
                if not (re.match("[A-Z]{1}\d{3}", str(self.shcp.name).upper())):
                    raise UserError(
                        _('Please enter first digit as letter and last 3 digits as numbers for SHCP.'))
                    
                    
    @api.model
    def fields_get(self, fields=None, attributes=None):
        
        no_selectable_fields = [ 'unam_key_id', 'unam_key_code', 'shcp', 'desc', 'dep_con_id' , 'federal_part']
        
        no_sortable_field = [ 'unam_key_id' , 'dep_con_id']

        res = super(BudgetProgramConversion, self).fields_get(fields, attributes=attributes)
        
        for key, value in res.items():
            if key in no_selectable_fields:
                value.update({'selectable': False})
        
        for key, value in res.items():
            if key in no_sortable_field:
                value.update({'sortable': False})
                
        return res

    @api.onchange('unam_key_id')
    def _onchange_key_unam(self):
        if self.unam_key_id:
            self.desc = self.unam_key_id.desc_key_unam
        else:
            self.desc = False

    @api.onchange('shcp')
    def _onchange_shcp(self):
        if self.shcp:
            self.description = self.shcp.desc
        else:
            self.description = False

    @check_authorization(
        'jt_budget_mgmt.group_budget_catalogs_admin_user',
        'jt_budget_mgmt.group_budget_admin_user'
    )
    @check_only_allowed_fields('start_date', 'end_date')
    def write(self,vals):
        return super(BudgetProgramConversion,self).write(vals)

    @check_authorization(
        'jt_budget_mgmt.group_budget_catalogs_admin_user',
        'jt_budget_mgmt.group_budget_admin_user'
    )
    def unlink(self):
        for bpc in self:
            program_code = self.env['program.code'].search([('budget_program_conversion_id', '=', bpc.id)], limit=1)
            if program_code:
                raise ValidationError('No puedes eliminar una CONPP que este ligado a un Código Programático!')
        return super(BudgetProgramConversion, self).unlink()

    def validate_shcp(self, shcp_string, program,conversion_item_string):
        if len(str(shcp_string)) > 3:
            shcp_str = str(shcp_string)
            if len(shcp_str) == 4:
                if not (re.match("[A-Z]{1}\d{3}", str(shcp_str).upper())):
                    return False
                else:
                    shcp = self.search(
                        [('federal_part','=',conversion_item_string),('shcp.name', '=', shcp_str), ('program_key_id', '=', program.program_key_id.id)], limit=1)
                    if shcp:
                        return shcp
        return False

    # if not return value is not valid #
    def get_validity_conpp(self, conpa, conpp, today = datetime.now()):
        query = """
                SELECT bpc.id
                FROM budget_program_conversion bpc
                JOIN shcp_game sg on bpc.conversion_key_id = sg.id
                JOIN shcp_code sc on bpc.shcp = sc.id
                WHERE 
                    (
                        (bpc.start_date IS NULL AND bpc.end_date IS NULL AND sg.conversion_key  = %(conpa)s  AND sc.name  = %(conpp)s) OR
                        (bpc.start_date IS NOT NULL AND bpc.end_date IS NULL AND sg.conversion_key  = %(conpa)s  AND sc.name  = %(conpp)s AND bpc.start_date <= %(today)s) OR
                        (bpc.start_date IS NOT NULL AND bpc.end_date IS NOT NULL AND sg.conversion_key  = %(conpa)s  AND sc.name  = %(conpp)s AND bpc.start_date <= %(today)s AND bpc.end_date >= %(today)s)
                    )
                """
        params = {'conpa': str(conpa),
                  'conpp': str(conpp),
                  'today': today}
        
        self.env.cr.execute(query, params)
        conpp_validity = self.env.cr.fetchall()

        return conpp_validity