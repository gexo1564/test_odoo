

from odoo import models, fields

class ExcludeProgramCodeAdequacies(models.Model):
    _name = 'exclude.program.code.adequacies'

    initial_position = fields.Integer('Initial position')
    code_slice = fields.Char('String to exclude from program code to adequacies')

    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, str(record.id)))
        return res