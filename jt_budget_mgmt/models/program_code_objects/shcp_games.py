# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

from odoo.addons.siif_security.tools.utils import check_authorization, check_only_allowed_fields

class SHCPGame(models.Model):

    _name = 'shcp.game'
    _description = 'Conversion Key with Item'
    _rec_name = 'conversion_key'

    conversion_key = fields.Char('Conversion Key')
    conversion_key_desc = fields.Text(string="Description of Conversion Key")

    _sql_constraints = [('conversion_key', 'unique(conversion_key, start_date, end_date)', 'The  Conversion Key must be unique.')]

    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')

    @api.constrains('start_date', 'end_date')
    def check_end_date(self):
        if not self.start_date:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("Configure la fecha inicial"))
            else:
                raise ValidationError(_("The start date of validity is required"))
        if self.end_date and self.end_date <= self.start_date:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("La fecha de fin de vigencia no puede ser menor o igual a la fecha de inicio de vigencia"))
            else:
                raise ValidationError(_("The effective end date cannot be less than or equal to the effective date"))

    @api.constrains('conversion_key')
    def _check_name(self):
        if self.conversion_key:
            if not str(self.conversion_key).isnumeric():
                raise ValidationError(_('The Conversion Key must be numeric value'))
            if len(self.conversion_key) != 5:
                raise ValidationError(_('The Conversion Key size must be five.'))

    @check_authorization(
        'jt_budget_mgmt.group_budget_catalogs_admin_user',
        'jt_budget_mgmt.group_budget_admin_user'
    )
    @check_only_allowed_fields('conversion_key_desc', 'start_date', 'end_date')
    def write(self, vals):
        return super(SHCPGame, self).write(vals)

    @check_authorization(
        'jt_budget_mgmt.group_budget_catalogs_admin_user',
        'jt_budget_mgmt.group_budget_admin_user'
    )
    def unlink(self):
        for record in self:
            if self.env['departure.conversion'].search([('conversion_key_id', '=', record.id)]):
                raise ValidationError(_('You can not delete SHCP Game which are mapped with Departure Conversion (CONPA)!'))
        return super(SHCPGame, self).unlink()
