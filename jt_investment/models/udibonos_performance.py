import logging
from odoo import models, fields, api, _
from datetime import datetime
from odoo.exceptions import UserError, ValidationError
import pandas as pd
from pandas.tseries.offsets import BMonthEnd, BMonthBegin


class UdibonosPerformance(models.Model):
    _name = 'udibonos.performance'
    _description = "Rendimiento de Udibonos"

    date = fields.Date()
    description = fields.Char()
    previous_month_titles_value = fields.Float(digits=(8, 6))
    current_month_titles_value = fields.Float(digits=(8, 6))
    previous_month_nominal_value = fields.Float(digits=(8, 6))
    current_month_nominal_value = fields.Float(digits=(8, 6))
    difference = fields.Float(digits=(4, 6))
    performance = fields.Char()
    accomulated_difference = fields.Float(digits=(8, 6))

    supporting_documentation_files = fields.Many2many(
        "ir.attachment", string="Add Files", attachment=True)


    @api.constrains('previous_month_titles_value')
    def check_previous_month_titles_value(self):
        if self.previous_month_titles_value <= 0:
            raise UserError(
                _('Negative values or values equal to zero are not allowed'))

    @api.constrains('current_month_titles_value')
    def check_current_month_titles_value(self):
        if self.current_month_titles_value <= 0:
            raise UserError(
                _('Negative values or values equal to zero are not allowed'))