import io
from ..layouts import Layout, ResultLine
from datetime import date
class LayoutBBVASIT(Layout):

    def load_file(self, file_data):
        # io.StringIO(file_data.decode("utf-8"))
        self.file = io.StringIO(file_data.decode("ISO-8859-1")) #open(file, encoding="ISO-8859-1")

    def load_body(self):
        return self.file.readlines()[1:-1]

    def close_file(self):
        self.file.close()

    def read_line(self, line):
        folio =line[3:23] #Folio
        upa = folio[0:3]
        upa_doc = folio[3:5]
        folio_rec = folio[5:11]
        proveedor = line[23:53].strip()
        account = line[57:77]
        amount = float("%s.%s" % (line[444:457], line[458:459]))
        fecha = line[1356:1366]
        status = line[1324:1327]
        ok = "00" in status
        folio_unico = "%s/%s/%s/%s" % (str(date.today().year), upa, upa_doc, folio_rec)
        return ResultLine(ok, {'folio': folio_unico, 'rfc': proveedor, 'cuenta': account, 'amount': amount, 'fecha':fecha}, status)

    def get_ids_payment(self, lines: list):
        folio = tuple([l.columns.get('folio') for l in lines])
        # Se obtienen los ids que hacen match con el criterio de búsqueda
        query = """
            select folio, id, payment_state from account_move where folio in %s
        """
        self.env.cr.execute(query, (folio,))
        result_set = {r[0]: r[1:] for r in self.env.cr.fetchall()}
        lines_ok, lines_err = [], []
        for line in lines:
            folio = line.columns.get('folio')
            res = result_set.get(folio, None)
            if res:
                line.payment_id = res[0]
                lines_ok.append(line)
            else:
                line.payment_id = None
                line.error_message = "No se encontró una solicitud con folio %s" % folio
                lines_err.append(line)
        return lines_ok, lines_err