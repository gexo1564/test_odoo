# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.osv import expression
from odoo.tools.misc import formatLang


class AccountReport(models.AbstractModel):
    _inherit = 'account.report'

    special_reports = [
        'jt_account_module_design.trial.balance.base',
        'jt_account_module_design.stat.inc.exp.inv.report',
        'jt_account_module_design.financial.report',
        'jt_account_design.state.income.expenses',
        'jt_account_module_design.stat.inc.exp.inv.summary.report',
        'jt_account_module_design.miles.revenue.report',
        'jt_account_module_design.weight.income.report'
    ]

    def _get_options(self, previous_options=None):
        '''
        (Overridden) Set the option to allow unposted entries to show up in 
        reports to False by default if the report is one of the special reports
        '''
        options = super(AccountReport, self)._get_options(previous_options)
        if self._name in self.special_reports:
            options['all_entries'] = False
        return options

    def open_journal_items_unam(self, options, params):
        '''
        (New) When opening journal items from a report line that's a dependency
        or subdependency, add their appropriate extra domain filters to show 
        only the relevant journal items
        '''
        line_id = params.get('id')
        aml_id, subdep_id, dep_id, acc_id, __, __ = self.get_ids(line_id)
        params['id'] = str(acc_id)
        action = self.open_journal_items(options, params)

        extra = [
            ('dependency_id', '=', dep_id),
        ]
        if line_id.startswith('subdep'):
            extra.append(('sub_dependency_id', '=', subdep_id))

        action['domain'] = expression.AND([extra] + [action.get('domain', [])])
        return action

    @api.model
    def format_line(self, cols, children=None, extra_info=None, maj=None, con=None, acc=None, dep=None, subdep=None,
        aml=None):
        children = children or []
        extra_info = extra_info or {}
        '''
        (New) Returns a custom formatted line for an account report
        '''
        line_id = None
        parent_id = None
        name = None
        title_hover = None

        if aml is not None:
            line_type = 'aml'
        elif subdep is not None:
            line_type = 'subdep'
        elif dep is not None and subdep is None:
            line_type = 'dep'
        elif acc is not None and dep is None and subdep is None:
            line_type = 'account'
        elif con is not None and acc is None and dep is None and subdep is None:
            line_type = 'con'
        else:
            line_type = 'maj'

        if line_type == 'aml':
            line_id = 'aml' + self.str_id(aml) + self.str_id(subdep) + self.str_id(dep) + self.str_id(acc) + self.str_id(con) + self.str_id(maj)
            parent_id = 'subdep' + self.str_id(subdep) + self.str_id(dep) + self.str_id(acc) + self.str_id(con) + self.str_id(maj)
            name = str(aml.move_name)
            title_hover = aml.move_name
            level = 6
        elif line_type == 'subdep':
            line_id = 'subdep' + self.str_id(subdep) + self.str_id(dep) + self.str_id(acc) + self.str_id(con) + self.str_id(maj)
            parent_id = 'dep' + self.str_id(dep) + self.str_id(acc) + self.str_id(con) + self.str_id(maj)
            name = _('Other') if not subdep.id else (str(subdep.sub_dependency) + ' ' + str(subdep.description))
            title_hover = subdep.description
            level = 5
        elif line_type == 'dep':
            line_id =  'dep' + self.str_id(dep) + self.str_id(acc) + self.str_id(con) + self.str_id(maj)
            parent_id = 'account' + self.str_id(acc) + self.str_id(con) + self.str_id(maj)
            name = _('Other') if not dep.id else (str(dep.dependency) + ' ' + str(dep.description))
            title_hover = dep.description
            level = 4
        elif line_type == 'account':
            line_id =  'account' + self.str_id(acc) + self.str_id(con) + self.str_id(maj)
            parent_id = 'con' + self.str_id(con) + self.str_id(maj)
            name = str(acc.code) + ' ' + str(acc.name)
            title_hover = str(acc.code) + ' ' + str(acc.name)
            level = 3
        elif line_type == 'con':
            line_id = 'con' + self.str_id(con) + self.str_id(maj)
            parent_id = 'maj' + self.str_id(maj)
            name = con.concept
            title_hover = con.concept
            level = 2
        else:
            line_id = 'maj' + self.str_id(maj)
            name = maj.name
            title_hover = maj.name
            level = 1

        return {
            **{
                'id': line_id,
                'name': name,
                'title_hover': title_hover,
                'columns': cols,
                'level': level,
                'unfoldable': children,
                'parent_id': parent_id
            },
            **extra_info
        }
    
    def str_id(self, obj):
        '''
        (New) Returns the id of the object with an '_' prepended, or returns
        and empty string if the object doesn't exist
        '''
        if obj is None:
            return ''
        else:
            return '_' + (str(obj.id) if obj.id != False else 'None')

    def get_ids(self, line_id):
        '''
        (New) Returns a tuple of the account move line id, subdependency id,
        dependency id, account id, concept id, and major id in that order. If
        any of those ids don't exist will return None
        '''
        if not line_id:
            return tuple([None] * 6)

        line_type = ''
        if line_id.startswith('subdep_'):
            line_type = line_id[:7]
            line_id = line_id[7:]
        elif line_id.startswith('account_'):
            line_type = line_id[:8]
            line_id = line_id[8:]
        else:
            line_type = line_id[:4]
            line_id = line_id[4:]

        ids = list(map(lambda l: int(l) if l != 'None' else None, line_id.split('_')))

        if line_type == 'aml_':
            return tuple((ids + [None] * 6)[:6])
        elif line_type == 'subdep_':
            return tuple(([None] + ids + [None] * 6)[:6])
        elif line_type == 'dep_':
            return tuple(([None] * 2 + ids + [None] * 6)[:6])
        elif line_type == 'account_':
            return tuple(([None] * 3 + ids + [None] * 6)[:6])
        elif line_type == 'con_':
            return tuple(([None] * 4 + ids + [None] * 6)[:6])
        else:
            return tuple(([None] * 5 + ids + [None] * 6)[:6])


    def _format(self, value,figure_type=None):
        '''
        (New) Added this function to the base reports just to not have to add
        it to every report manually
        '''
        if self.env.context.get('no_format'):
            return value
        value['no_format_name'] = value['name']
        
        if figure_type == 'float':
            currency_id = self.env.company.currency_id
            if currency_id.is_zero(value['name']):
                # don't print -0.0 in reports
                value['name'] = abs(value['name'])
                value['class'] = 'number text-muted'
            value['name'] = formatLang(self.env, value['name'], currency_obj=currency_id)
            value['class'] = 'number'
            return value
        if figure_type == 'percents':
            value['name'] = str(round(value['name'] * 100, 1)) + '%'
            value['class'] = 'number'
            return value
        value['name'] = round(value['name'], 1)
        return value


    def open_document(self, options, params=None):
        '''
        (Overridden) Because we change how id works in account reports, if the
        id is from a custom report, replace it with the id of the report line
        it represents to make everything work as it should
        '''
        res_id = str(params.get('id'))

        if res_id.startswith(('aml', 'subdep', 'dep', 'account', 'con', 'maj')):
            aml_id, subdep_id, dep_id, acc_id, con_id, maj_id = self.get_ids(res_id)

            if aml_id:
                params['id'] = aml_id
            elif subdep_id:
                params['id'] = subdep_id
            elif dep_id:
                params['id'] = dep_id
            elif acc_id:
                params['id'] = acc_id
            elif con_id:
                params['id'] = con_id
            elif maj_id:
                params['id'] = maj_id

        return super(AccountReport, self).open_document(options, params)
