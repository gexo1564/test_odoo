# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields, api, _
import base64
import xlrd
from datetime import datetime
from odoo.modules.module import get_resource_path
from xlrd import open_workbook
from odoo.exceptions import UserError, ValidationError
from odoo.tools.misc import ustr
import logging
class ImportLine(models.TransientModel):

    _name = 'import.line'
    _description = 'Import Line'

    budget_name = fields.Text(string='Name of the budget')
    total_budget = fields.Float(string='Total budget')
    record_number = fields.Integer(string='Number of records')
    file = fields.Binary(string='File')
    filename = fields.Char(string='File name')
    dwnld_file = fields.Binary(string='Download File')
    dwnld_filename = fields.Char(string='Download File name')

    @api.model
    def default_get(self, fields):
        res = super(ImportLine, self).default_get(fields)
        budget = self.env['expenditure.budget'].browse(
            self._context.get('active_id'))
        if budget and budget.name:
            res['budget_name'] = budget.name
        return res

    def download_file(self):
        file_path = get_resource_path(
            'jt_budget_mgmt', 'static/file/import_line', 'import_line_sample.xlsx')
        file = False
        with open(file_path, 'rb') as file_date:                
            file = base64.b64encode(file_date.read())
        self.dwnld_filename = 'import_line.xlsx'
        self.dwnld_file = file
        return {
            'name': 'Download',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': False,
            'res_model': 'import.line',
            'domain': [],
            'type': 'ir.actions.act_window',
            'target': 'new',
            'res_id': self.id,
        }

    def import_line(self):
        budget = self.env['expenditure.budget'].browse(
            self._context.get('active_ids'))
        if not self.file:
            raise UserError(_('Please Upload File.'))
        if budget.name != self.budget_name:
            raise UserError(_('Budget name does not match.'))
        elif self.file:
            try:
                extension = self.filename.split(".")[-1]
                field_headers = ['year', 'program', 'subprogram', 'dependency', 'subdependency', 'item', 'dv',
                                 'origin_resource', 'ai', 'conversion_program', 'departure_conversion',
                                 'expense_type', 'location', 'portfolio', 'project_type', 'project_number',
                                 'stage', 'agreement_type', 'agreement_number', 'exercise_type',
                                 'authorized', 'start_date', 'end_date']
                if extension == 'xls' or extension =='xlsx':
                    data = base64.decodestring(self.file)
                    book = open_workbook(file_contents=data or b'')
                    sheet = book.sheet_by_index(0)

                    total_rows = self.record_number + 1
                    if sheet.nrows != total_rows:
                        raise UserError(
                            _('The number of imported lines is not equal to the number of records'))

                    headers = []
                    for rowx, row in enumerate(map(sheet.row, range(1)), 1):
                        for colx, cell in enumerate(row, 1):
                            headers.append(cell.value)

                    total_budget_amount = 0
                    result_vals = []
                    for rowx, row in enumerate(map(sheet.row, range(1, sheet.nrows)), 1):
                        result_dict = {
                            'imported': True,
                            'state': 'draft',
                        }
                        if budget:
                            if budget.from_date:
                                result_dict.update({'start_date':budget.from_date})
                            if budget.to_date:
                                result_dict.update({'end_date':budget.to_date})
                        counter = 0
                        for colx, cell in enumerate(row, 1):
                            value = cell.value
                            if field_headers[counter] not in ('authorized', 'assigned'):
                                if field_headers[counter] in ('year', 'dv') and type(value) is int or type(value) is float:
                                    value = int(cell.value)
                                else:
                                    value = str(cell.value)

                            if field_headers[counter] == 'start_date':
                                msg = ''
                                try:
                                    start_date = False
                                    if type(value) is str:
                                        start_date = datetime.strptime(str(value), '%m/%d/%Y').date()
                                    elif type(value) is int or type(value) is float:
                                        start_date = datetime(*xlrd.xldate_as_tuple(value, 0)).date()
                                    if start_date:
                                        if start_date.day !=1 or start_date.month != 1:
                                            if self.env.user.lang == 'es_MX':
                                                msg = "de fechas:Las fechas colocadas en el archivo no coinciden con las fechas colocadas en el Presupuesto 01/01/YYYY"
                                                raise ValidationError(_("de fechas:Las fechas colocadas en el archivo no coinciden con las fechas colocadas en el Presupuesto 01/01/YYYY"))
                                            else:
                                                raise ValidationError(_("Start Date Format must be 01/01/YYYY"))
                                        value = start_date
                                    else:
                                        value = False
                                except ValueError as e:
                                    raise ValidationError(_("Start Date Format Does Not match : %s")% (ustr(e)))
                                except ValidationError as e:
                                    if msg and self.env.user.lang == 'es_MX':
                                        raise ValidationError(_("de fechas:Las fechas colocadas en el archivo no coinciden con las fechas colocadas en el Presupuesto 01/01/YYYY"))
                                    else:
                                        raise ValidationError(_("Start Date Format Does Not match : %s")% (ustr(e)))
                                except UserError as e:
                                    raise ValidationError(_("Start Date Format Does Not match : %s")% (ustr(e)))            

                            if field_headers[counter] == 'end_date':
                                msg = ''
                                try:
                                    end_date = False
                                    if type(value) is str:
                                        end_date = datetime.strptime(str(value), '%m/%d/%Y').date()
                                    elif type(value) is int or type(value) is float:
                                        end_date = datetime(*xlrd.xldate_as_tuple(value, 0)).date()
                                    if end_date:
                                        if end_date.day != 31 or end_date.month != 12:
                                            if self.env.user.lang == 'es_MX':
                                                msg = 'de fechas:Las fechas colocadas en el archivo no coinciden con las fechas colocadas en el Presupuesto 31/12/YYYY'
                                                raise ValidationError(_("de fechas:Las fechas colocadas en el archivo no coinciden con las fechas colocadas en el Presupuesto 31/12/YYYY"))
                                            else:
                                                raise ValidationError(_("End Date Format must be 01/01/YYYY"))
                                        value = end_date
                                    else:
                                        value = False
                                except ValueError as e:
                                    raise ValidationError(_("End Date Format Does Not match : %s")% (ustr(e)))
                                except ValidationError as e:
                                    if msg and self.env.user.lang == 'es_MX':
                                        raise ValidationError(_("de fechas:Las fechas colocadas en el archivo no coinciden con las fechas colocadas en el Presupuesto 31/12/YYYY"))
                                    else:
                                        raise ValidationError(_("End Date Format Does Not match :%s")% (ustr(e)))
                                except UserError as e:
                                    raise ValidationError(_("End Date Format Does Not match : %s")% (ustr(e)))            
                            result_dict.update({field_headers[counter]: value})
                            counter += 1

                        if 'assigned' in result_dict:
                            amt = result_dict.get('assigned', 0)
                            if float(amt) < 0:
                                raise UserError(_("Assigned Amount should be greater than or 0!"))
                        if 'authorized' in result_dict:
                            amt = result_dict.get('authorized', 0)
                            if float(amt) < 0:
                                raise UserError(_("Authorized Amount should be greater than 0!"))
                            # Se agrega valor del disponible
                            amt = round(float(amt), 2)
                            result_dict.update({'available': amt})
                            total_budget_amount += amt
                        result_vals.append((0, 0, result_dict))
                else:

                    data = base64.decodestring(self.file)
                    #file = open(data)
                    #lines = file.readlines()
                    info = data.decode('utf-8')
                    #.split('\\r\\n')
                    #logging.critical("####Archivo"+str(info))
                    lines = info.splitlines()

                    budget.file = self.file

                    total_rows = self.record_number
                    if len(lines) != total_rows:
                        raise UserError(_('The number of imported lines is not equal to the number of records'))
                    result_vals = []
                    total_budget_amount=0

                    for line in lines:
                        list_result = []
                        result_dict = {
                            'imported': True,
                            'state': 'draft',
                        }
                        if len(line)>27:
                            line_split=line.split(",")

                            quarter_amount = line_split[5]
                            date_start = budget.from_date
                            date_end = budget.to_date

                            result_dict.update({field_headers[0]:line_split[0][0:4]})
                            result_dict.update({field_headers[1]:line_split[0][4:6]})
                            result_dict.update({field_headers[2]:line_split[0][6:8]})
                            result_dict.update({field_headers[3]:line_split[0][8:11]})
                            result_dict.update({field_headers[4]:line_split[0][11:13]})
                            result_dict.update({field_headers[5]:line_split[0][13:16]})
                            result_dict.update({field_headers[6]:line_split[0][16:18]})
                            result_dict.update({field_headers[7]:line_split[0][18:20]})
                            result_dict.update({field_headers[8]:line_split[0][20:25]})
                            result_dict.update({field_headers[9]:line_split[0][25:29]})
                            result_dict.update({field_headers[10]:line_split[0][29:34]})
                            result_dict.update({field_headers[11]:line_split[0][34:36]})
                            result_dict.update({field_headers[12]:line_split[0][36:38]})
                            result_dict.update({field_headers[13]:line_split[0][38:42]})
                            result_dict.update({field_headers[14]:line_split[0][42:44]})
                            result_dict.update({field_headers[15]:line_split[0][44:50]})
                            result_dict.update({field_headers[16]:line_split[0][50:52]})
                            result_dict.update({field_headers[17]:line_split[0][52:54]})
                            result_dict.update({field_headers[18]:line_split[0][54:60]})
                            result_dict.update({field_headers[19]:line_split[0][60:61]})
                            result_dict.update({field_headers[20]:float(quarter_amount)})
                            result_dict.update({field_headers[21]: date_start})
                            result_dict.update({field_headers[22]: date_end})
                            
                            result_vals.append((0, 0, result_dict))

                            total_budget_amount += float(line_split[5])

                if round(total_budget_amount, 2) != self.total_budget:
                    if self.env.user.lang=='es_MX':
                        raise UserError(
                            _('La suma de los montos Autorizados %s no es igual al total del presupuesto %s')%(round(total_budget_amount, 2),self.total_budget))
                    else:
                        raise UserError(
                            _('The sum of the Authorized amounts %s is not equal to the total of the budget %s')%(round(total_budget_amount, 2),self.total_budget))

                data = result_vals
                if budget:
                    if self._context.get('reimport'):
                        # Borrado de las líneas de presupuesto
                        query = "delete from expenditure_budget_line where expenditure_budget_id = %s returning 1"
                        self.env.cr.execute(query, (budget.id,))
                        # Borrado de los códigos programáticos
                        query = "delete from program_code where budget_id = %s returning 1"
                        self.env.cr.execute(query, (budget.id,))
                        # budget.line_ids.filtered(lambda l: l.state != 'manual').unlink()
                        # lines = budget.success_line_ids.filtered(lambda l: l.state != 'manual')
                        # # Se eliminan los códigos programáticos para no dejar códigos sin referencias
                        # for line in lines:
                        #     line.program_code_id.unlink()
                        # lines.unlink()
                        budget.write({'state': 'draft'})
                    try:
                        budget.write({
                            'import_status': 'in_progress',
                            'line_ids': data,
                        })
                        # Se inicializa la tabla replica con el id del registro anual
                        # budget.line_ids.replicate()
                    except ValueError as e:
                        if self.env.user.lang == 'es_MX':
                            raise ValidationError(_("La columna contiene valores incorrectos. Error: %s")% (ustr(e)))
                        else:
                            raise ValidationError(_("Column  contains incorrect values. Error: %s")% (ustr(e)))
                    except ValidationError as e:
                        if self.env.user.lang == 'es_MX':
                            raise ValidationError(_("La columna contiene valores incorrectos. Error: %s")% (ustr(e)))
                        else:
                            raise ValidationError(_("Column  contains incorrect values. Error: %s")% (ustr(e)))
                    except UserError as e:
                        if self.env.user.lang == 'es_MX':
                            raise ValidationError(_("La columna contiene valores incorrectos. Error: %s")% (ustr(e)))
                        else:
                            raise ValidationError(_("Column  contains incorrect values. Error: %s")% (ustr(e)))

            except ValueError as e:
                if self.env.user.lang == 'es_MX':
                    raise ValidationError(_("La columna contiene valores incorrectos. Error %s")% (ustr(e)))
                else:
                    raise ValidationError(_("Column  contains incorrect values. Error %s")% (ustr(e)))
            except ValidationError as e:
                if self.env.user.lang == 'es_MX':
                    raise ValidationError(_("La columna contiene valores incorrectos. Error: %s")% (ustr(e)))
                else:
                    raise ValidationError(_("Column  contains incorrect values. Error %s")% (ustr(e)))
            except UserError as e:
                if self.env.user.lang == 'es_MX':
                    raise ValidationError(_("La columna contiene valores incorrectos. Error: %s")% (ustr(e)))
                else:
                    raise ValidationError(_("Column  contains incorrect values. Error %s")% (ustr(e)))
            except Exception:
                raise UserError("Favor de verificar que su archivo tiene la estructura correcta y aceptada por el sistema")