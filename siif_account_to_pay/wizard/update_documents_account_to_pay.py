from odoo import models, fields, api, _
from odoo.exceptions import UserError
import base64
from xlrd import open_workbook
from datetime import datetime

class UpdateDocumentsAccountToPay(models.TransientModel):
    _name = 'update.documents.account.to.pay'
    _description = 'Update Documents Account To Pay'

    account_to_pay_id = fields.Many2one('account.topay', "Account to Pay")

    docs_files = fields.Many2many('ir.attachment', string="Add Document Files")

    def open_wizard(self, res_id = None, **kwargs):
        return {
            'name': _('Actualizar Documentación Soporte de la Cuenta por Pagar'),
            'type': 'ir.actions.act_window',
            'res_model': 'update.documents.account.to.pay',
            'view_mode': 'form',
            'view_type': 'form',
            'views': [(False, 'form')],
            'context': kwargs,
            'target': 'new',
            'res_id': res_id,
        }

    def action_update_documents(self):
        if not self.docs_files:
            raise UserError("Se requiere al menos un archivo de Documentación Soporte.")
        self.account_to_pay_id.docs_files = self.docs_files.ids