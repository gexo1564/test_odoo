# -*- coding: utf-8 -*-
from datetime import datetime
from odoo import api, fields, models, _


class WeightIncomeReport(models.AbstractModel):

    _inherit = "jt_account_module_design.miles.revenue.report"


    def _get_lines(self, options, line_id=None):
        lines = []
        start = datetime.strptime(str(options['date'].get('date_from')), '%Y-%m-%d').date()
        end = datetime.strptime(options['date'].get('date_to'), '%Y-%m-%d').date()
        posted_only = not options.get('all_entries')


        if options.get('all_entries') is False:
            move_state_domain = ('move_id.state', '=', 'posted')
        else:
            move_state_domain = ('move_id.state', '!=', 'cancel')

        domain = [('date', '<=', end),move_state_domain]
        
        concept_ids = self.env['miles.revenue'].search([])

        gt_total_balance1_inc = 0
        gt_total_balance2_inc = 0
        gt_total_balance3_inc = 0

        gt_total_balance1_exp = 0
        gt_total_balance2_exp = 0
        gt_total_balance3_exp = 0
        
        for con in concept_ids:
            account_dep_list = []
            total_balance1 = 0
            total_balance2 = 0
            total_balance3 = 0
            account_ids = con.account_ids
            for acc in account_ids:
                balance = sum(x.credit-x.debit for x in self.env['account.move.line'].search(
                    domain + [('account_id', '=', acc.id)])) / 1000
                adjustment = 0 / 1000
                adjusted = balance - adjustment
                
                total_balance1 += balance
                total_balance2 += adjustment
                total_balance3 += adjusted

                gt_total_balance1_inc += balance
                gt_total_balance2_inc += adjustment
                gt_total_balance3_inc += adjusted
                
                deps = self.env['dependency'].get_dependencies_for_account(acc,[
                    ('date', '>=', start),
                    ('date', '<=', end),
                    ('parent_state', '=', 'posted') if posted_only else ('parent_state', '!=', 'cancel')
                ])

                account_dep_list.append(self.format_line(children=deps, con=con, acc=acc,
                    cols=[
                        { 'name': acc.name },
                        self._format({'name': balance},figure_type='float'),
                        self._format({'name': adjustment},figure_type='float'),
                        self._format({'name': adjusted},figure_type='float'),
                    ] 
                ))

                ################################
                ####### DEP CALCULATIONS #######
                ################################
                for dep in deps:
                    dep_balance = sum(x.credit-x.debit for x in self.env['account.move.line'].search(domain + [
                        ('account_id', '=', acc.id),
                        ('dependency_id', '=', dep.id)
                    ])) / 1000
                    dep_adjustment = 0
                    dep_adjusted = dep_balance - dep_adjustment


                    subdeps = self.env['sub.dependency'].get_subdependencies_for_account(acc, dep, [
                        ('date', '>=', start),
                        ('date', '<=', end),
                        ('parent_state', '=', 'posted') if posted_only else ('parent_state', '!=', 'cancel')
                    ])

                    account_dep_list.append(self.format_line(con=con, acc=acc, dep=dep, children=subdeps,
                        cols=[
                            { 'name': dep.dependency },
                            self._format({'name': dep_balance},figure_type='float'),
                            self._format({'name': dep_adjustment},figure_type='float'),
                            self._format({'name': dep_adjusted},figure_type='float'),
                        ]
                    ))
                    for subdep in subdeps:
                        subdep_balance = sum(x.credit-x.debit for x in self.env['account.move.line'].search(domain + [
                            ('account_id', '=', acc.id),
                            ('dependency_id', '=', dep.id),
                            ('sub_dependency_id', '=', subdep.id)
                        ])) / 1000
                        subdep_adjustment = 0 / 1000
                        subdep_adjusted = subdep_balance - subdep_adjustment

                        account_dep_list.append(self.format_line(con=con, acc=acc, dep=dep, subdep=subdep, cols=[
                            { 'name': subdep.sub_dependency },
                            self._format({'name': subdep_balance},figure_type='float'),
                            self._format({'name': subdep_adjustment},figure_type='float'),
                            self._format({'name': subdep_adjusted},figure_type='float'),
                        ]))

            lines.append(self.format_line(con=con, children=lines, 
                cols=[
                    self._format({'name': total_balance1},figure_type='float'),
                    self._format({'name': total_balance2},figure_type='float'),
                    self._format({'name': total_balance3},figure_type='float'),
                ],
                extra_info = {
                    'colspan':2,
                    'class':'text-left'
                }
            ))
            lines += account_dep_list



        lines.append({
            'id': 'TOTAL REVENUE',
            'name': _('TOTAL REVENUE'),
            'columns': [{'name': ''},
                self._format({'name': gt_total_balance1_inc},figure_type='float'),
                self._format({'name': gt_total_balance2_inc},figure_type='float'),
                self._format({'name': gt_total_balance3_inc},figure_type='float'),
            ],
            'level': 1,
            'unfoldable': False,
            'unfolded': True,
        })
        user_type_id = self.env.ref('account.data_account_type_expenses').id
        account_ids = self.env['account.account'].search([('user_type_id','=',user_type_id)])
        exp_ids= self.env['account.move.line'].search(domain + [('account_id', 'in', account_ids.ids)])
        gt_total_balance1_exp = sum(x.debit - x.credit for x in exp_ids)
        gt_total_balance1_exp = abs(gt_total_balance1_exp)
        gt_total_balance1_exp = gt_total_balance1_exp/1000
        gt_total_balance2_exp = 0
        gt_total_balance3_exp = gt_total_balance1_exp
        
        lines.append({
            'id': 'TOTAL EGRESSES',
            'name': _('TOTAL EGRESSES'),
            'columns': [{'name': ''},
                        self._format({'name': gt_total_balance1_exp},figure_type='float'),
                        self._format({'name': gt_total_balance2_exp},figure_type='float'),
                        self._format({'name': gt_total_balance3_exp},figure_type='float'),
                        ],
            
            'level': 1,
            'unfoldable': False,
            'unfolded': True,
        })
        lines.append({
            'id': 'Remnants',
            'name': _('Remnants'),
            'columns': [{'name': ''},
                        self._format({'name': gt_total_balance1_inc - gt_total_balance1_exp},figure_type='float'),
                        self._format({'name': gt_total_balance2_inc - gt_total_balance2_exp},figure_type='float'),
                        self._format({'name': gt_total_balance3_inc - gt_total_balance3_exp},figure_type='float'),
                        ],
            
            'level': 1,
            'unfoldable': False,
            'unfolded': True,
        })

        return lines