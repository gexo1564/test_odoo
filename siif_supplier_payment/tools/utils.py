class Dictionary:
    class Empty():
        pass

    def __init__(self, size = 1):
        self.__dictionary = {}
        self.__size = size
        self.__empty = self.Empty()

    def add_list(self, key, value, pos = 0):
        if not self.contains(key):
            self.__dictionary[key] = [self.__empty] * self.__size
        self.__dictionary[key][pos] = self.get(key, [], pos)
        self.__dictionary[key][pos].append(value)

    def add_sum(self, key, value, pos = 0):
        if not self.contains(key):
            self.__dictionary[key] = [self.__empty] * self.__size
        self.__dictionary[key][pos] = round(self.get(key, 0.0, pos) + value,2)

    def subtract_value(self, key, value, pos = 0):
        if self.contains(key):
            self.__dictionary[key][pos] = self.get(key, pos) - value

    def get(self, key, default = 0, pos = 0):
        tmp = self.__dictionary.get(key, self.__empty)[pos]
        if isinstance(tmp, self.Empty):
            return default
        return tmp

    def contains(self, key):
        return key in self.__dictionary

    def items(self):
        items = self.__dictionary.items()
        if self.__size == 1:
            return [(key, value[0]) for key, value in items]
        return items