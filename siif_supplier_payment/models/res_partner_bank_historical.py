from odoo import models, fields, api,_
from odoo.exceptions import UserError, ValidationError

class ResPartnerBankHistorical(models.Model):
    _name = 'res.partner.bank.historical'
    _description = "Res Partner Bank Historical"

    res_partner_bank_id = fields.Many2one('res.partner.bank',string='Res Partner Bank Id')
    date = fields.Date("Datetime")
    balance = fields.Float("Balance", digits=(15, 2))

    @api.model
    def create(self, vals):
        count = self.env['res.partner.bank.historical'].search_count([('res_partner_bank_id','=',vals.get('res_partner_bank')),('date','=',vals.get('date'))])
        if count >= 1:
            raise ValidationError("No se puede asignar el historico con misma fecha.")
        else:
            cuenta = self.env['res.partner.bank'].search([('id','=',vals.get('res_partner_bank_id'))])
            cuenta.set_current_balance(vals.get('balance'))
            return super().create(vals)
