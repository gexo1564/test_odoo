# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
import base64
from odoo.modules.module import get_resource_path
from xlrd import open_workbook
from odoo.exceptions import UserError, ValidationError
import logging
from datetime import datetime, timedelta, date
import openpyxl
import io

FEDERAL_SUBSIDY_CALENDAR = 'calendar.assigned.amounts.lines'

class ImportModificationsFederalSubsidy(models.TransientModel):
    _name = 'import.modifications.federal.subsidy'
    _description = 'Import Modifications Federal Subsidy'

    modifications_federal_subsidy_id = fields.Many2one('modifications.federal.subsidy', 'Income Modifications Federal Subsidy', ondelete="cascade")
    record_number = fields.Integer(string='Record Number')
    filename = fields.Char(string='File name')
    imported_file = fields.Binary(string='File', store=True)
    lines_ok = fields.One2many('modifications.federal.subsidy.lines', 'modifications_id', 'Line Modifications')
    download_filename = fields.Char(string='Download File name')
    download_file = fields.Binary(string='Download File')

    def download(self):
        file_path = get_resource_path(
            'siif_finance', 'static/file/import_line', 'SIIF_Plantilla_Carga_de_Modificaciones_al_Subsidio_Federal.xls')
        file = False
        with open(file_path, 'rb') as file_date:
            file = base64.b64encode(file_date.read())
        self.download_filename = 'SIIF_Plantilla_Carga_de_Modificaciones_al_Subsidio_Federal.xls'
        self.download_file = file

        return {
            'name': 'Download Sample File',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': False,
            'res_model': 'import.modifications.federal.subsidy',
            'domain': [],
            'type': 'ir.actions.act_window',
            'target': 'new',
            'res_id': self.id,
        }
    
    @api.onchange('imported_file')
    def onchange_file(self):
        self.lines_err = None
        self.lines_ok = None
        self.file_errors = None
        self.modifications_federal_subsidy_id.imported_file = self.imported_file
        self.modifications_federal_subsidy_id.imported_filename = self.filename

    def import_line(self):

        lines_ok, lines_err, msg_err = [], [], []

        def set_line_error(sheet, row, msg):
            try:
                lines_err.append((0, 0, {
                    'cycle': sheet.cell_value(row, 0),
                    'branch_ord': sheet.cell_value(row, 1),
                    'ur_ord': sheet.cell_value(row, 2),
                    'request_number': sheet.cell_value(row, 3),
                    'type_request': sheet.cell_value(row, 4),
                    'quarter': sheet.cell_value(row, 5),
                    'type_movement': sheet.cell_value(row, 6),
                    'date_application': sheet.cell_value(row, 7),
                    'branch': sheet.cell_value(row, 8),
                    'unit': sheet.cell_value(row, 9),
                    'purpose': sheet.cell_value(row, 10),
                    'function_int': sheet.cell_value(row, 11),
                    'sub_function': sheet.cell_value(row, 12),
                    'program': sheet.cell_value(row, 13),
                    'institution_activity': sheet.cell_value(row, 14),
                    'project_identifier': sheet.cell_value(row, 15),
                    'project': sheet.cell_value(row, 16),
                    'federal_item': sheet.cell_value(row, 17),
                    'expense_type': sheet.cell_value(row, 18),
                    'funding_source': sheet.cell_value(row, 19),
                    'federative_entity': sheet.cell_value(row, 20),
                    'key_wallet': sheet.cell_value(row, 21),
                    'transaction': sheet.cell_value(row, 22),
                    'january': sheet.cell_value(row, 23),
                    'february': sheet.cell_value(row, 24),
                    'march': sheet.cell_value(row, 25),
                    'april': sheet.cell_value(row, 26),
                    'may': sheet.cell_value(row, 27),
                    'june': sheet.cell_value(row, 28),
                    'july': sheet.cell_value(row, 29),
                    'august': sheet.cell_value(row, 30),
                    'september': sheet.cell_value(row, 31),
                    'october': sheet.cell_value(row, 32),
                    'november': sheet.cell_value(row, 33),
                    'december': sheet.cell_value(row, 34),

                }))
            except IndexError:
                raise ValidationError(_("Please upload the correct file!"))

            line = ("[" + "'%s', "*34 + "%s]") % tuple([sheet.cell_value(row, i) for i in range(35)])
            msg_err.append("%s ---------->> %s" % (line, msg))

        if not self.imported_file:
            raise UserError(_('Please Upload File.'))
        try:
            data = base64.decodestring(self.imported_file)
            book = open_workbook(file_contents=data or b'')
        except UserError as e:
            raise UserError(e)
    
        sheet = book.sheet_by_index(0)

        total_rows = self.record_number + 6
        if sheet.nrows != total_rows:
            raise UserError(_('Number of records do not match with file.'))

        for row in range(6, sheet.nrows):
            try:
                cycle = sheet.cell_value(row, 0)
                branch_ord = sheet.cell_value(row, 1)
                ur_ord = sheet.cell_value(row, 2)
                request_number = sheet.cell_value(row, 3)
                type_request = sheet.cell_value(row, 4)
                quarter = sheet.cell_value(row, 5)
                type_movement = sheet.cell_value(row, 6)
                date_application = sheet.cell_value(row, 7)
                branch = sheet.cell_value(row, 8)
                unit = sheet.cell_value(row, 9)
                purpose = sheet.cell_value(row, 10)
                function_int = sheet.cell_value(row, 11)
                sub_function = sheet.cell_value(row, 12)
                program = sheet.cell_value(row, 13)
                institution_activity = sheet.cell_value(row, 14)
                project_identifier = sheet.cell_value(row, 15)
                project = sheet.cell_value(row, 16)
                federal_item = sheet.cell_value(row, 17)
                expense_type = sheet.cell_value(row, 18)
                funding_source = sheet.cell_value(row, 19)
                federative_entity = sheet.cell_value(row, 20)
                key_wallet = sheet.cell_value(row, 21)
                transaction = sheet.cell_value(row, 22)
                january = sheet.cell_value(row, 23)
                february = sheet.cell_value(row, 24)
                march = sheet.cell_value(row, 25)
                april = sheet.cell_value(row, 26)
                may = sheet.cell_value(row, 27)
                june = sheet.cell_value(row, 28)
                july = sheet.cell_value(row, 29)
                august = sheet.cell_value(row, 30)
                september = sheet.cell_value(row, 31)
                october = sheet.cell_value(row, 32)
                november = sheet.cell_value(row, 33)
                december = sheet.cell_value(row, 34)

            except IndexError:
                set_line_error(sheet, row, 'Favor de cargar el archivo correcto.')
                continue
            
            line = {}
            
            cycle_int = int(cycle)
            cycle_str = str(cycle_int)
            branch_ord_int = int(branch_ord)
            request_number_int = int(request_number)
            branch_int = int(branch)
            purpose_int = int(purpose)
            function_cal = int(function_int)
            sub_function_int = int(sub_function)
            program_int = int(program)
            institution_activity_int = int(institution_activity)
            project_int = int(project)
            federal_item_int = int(federal_item)
            expense_type_int = int(expense_type)
            funding_source_int = int(funding_source)
            federative_entity_int = int(federative_entity)

            # excel segment validations
            
            if not cycle:
                set_line_error(sheet, row, 'El archivo no cuenta con la información del campo Ciclo en la celda A%s' % (row + 1))
                continue
            else:
                fiscal_year = self.modifications_federal_subsidy_id.fiscal_year.name
                if cycle_str != fiscal_year:
                    set_line_error(sheet, row, 'El Ciclo del excel no coincide con el del formulario. Error en la celda A%s' % (row + 6))
                    continue
                fiscal_year = self.env['account.fiscal.year'].search([('name', '=', cycle_str)])
                if not fiscal_year:
                    set_line_error(sheet, row, 'No se encontró el Año Fiscal. Error en la celda A%s' % (row + 1))
                    continue
                else:
                    line.update({
                        'cycle': fiscal_year.id,
                    })
            
            if not branch_ord:
                set_line_error(sheet, row, 'El archivo no cuenta con la información del campo Ramo ORD en la celda B%s' % (row + 1))
                continue
            else:
                line.update({
                    'branch_ord': branch_ord,
                })

            if not ur_ord:
                set_line_error(sheet, row, 'El archivo no cuenta con la información del campo UR ORD en la celda C%s' % (row + 1))
                continue
            else:
                line.update({
                    'ur_ord': ur_ord,
                })

            if not request_number:
                set_line_error(sheet, row, 'El archivo no cuenta con la información del campo Solicitud Num en la celda D%s' % (row + 1))
                continue
            else:
                request = self.env['modifications.federal.subsidy.lines'].search([('request_number', '=', request_number), ('state_related', '=', 'approve')])
                if not request:
                    line.update({
                        'request_number': request_number,
                    })
                else:
                    set_line_error(sheet, row, 'El Número de Solicitud que desea requistrar de la celda D%s ya se encuentra registrado anteriormente' % (row + 1))
                    continue
            
            if not type_request:
                set_line_error(sheet, row, 'El archivo no cuenta con la información del campo Tipo de Solicitud en la celda E%s' % (row + 1))
                continue
            else:
                line.update({
                    'type_request': type_request,
                })
            
            if not quarter:
                set_line_error(sheet, row, 'El archivo no cuenta con la información del campo Trimestre en la celda F%s' % (row + 1))
                continue
            else:
                line.update({
                    'quarter': quarter,
                })

            if not type_movement:
                set_line_error(sheet, row, 'El archivo no cuenta con la información del campo Tipo de Movimiento en la celda G%s' % (row + 1))
                continue
            else:
                line.update({
                    'type_movement': type_movement,
                })

            if not date_application:
                set_line_error(sheet, row, 'El archivo no cuenta con la información del campo Fecha de Aplicación en la celda H%s' % (row + 1))
                continue
            else:
                line.update({
                    'date_application': date_application,
                })

            if not branch:
                set_line_error(sheet, row, 'El archivo no cuenta con la información del campo Ramo en la celda I%s' % (row + 1))
                continue
            else:
                validity_branch = self.env[FEDERAL_SUBSIDY_CALENDAR].search([('branch', '=', branch_int)])
                if not validity_branch:
                    set_line_error(sheet, row, 'El registro de Ramo en la celda I%s no se encuentra en ningún código programático en el calendario que desea modificar' % (row + 1))
                    continue
                else:
                    line.update({
                        'branch': branch,
                    })

            if not unit:
                set_line_error(sheet, row, 'El archivo no cuenta con la información del campo Unidad en la celda J%s' % (row + 1))
                continue
            else:
                validity_unit = self.env[FEDERAL_SUBSIDY_CALENDAR].search([('unit', '=', unit)])
                if not validity_unit:
                    set_line_error(sheet, row, 'El registro de Unidad en la celda J%s no se encuentre en nigún código programático en el calendario que desea modificar' % (row + 1))
                    continue
                else:
                    line.update({
                        'unit': unit,
                    })

            if not purpose:
                set_line_error(sheet, row, 'El archivo no cuenta con la información del campo Finalidad en la celda K%s' % (row + 1))
                continue
            else:
                validity_purpose = self.env[FEDERAL_SUBSIDY_CALENDAR].search([('purpose', '=', purpose_int)])
                if not validity_purpose:
                    set_line_error(sheet, row, 'El registro de Finalidad en la celda K%s no se encuentre en nigún código programático en el calendario que desea modificar' % (row + 1))
                    continue
                else:
                    line.update({
                        'purpose': purpose,
                    })
            
            if not function_int:
                set_line_error(sheet, row, 'El archivo no cuenta con la información del campo Función en la celda L%s' % (row + 1))
                continue
            else:
                validity_function = self.env[FEDERAL_SUBSIDY_CALENDAR].search([('function', '=', function_cal)])
                if not validity_function:
                    set_line_error(sheet, row, 'El registro de Función en la celda L%s no se encuentre en nigún código programático en el calendario que desea modificar' % (row + 1))
                    continue
                else:
                    line.update({
                        'function_int': function_cal,
                    })
            
            if not sub_function:
                set_line_error(sheet, row, 'El archivo no cuenta con la información del campo Subfunción en la celda M%s' % (row + 1))
                continue
            else:
                validity_sub_function = self.env[FEDERAL_SUBSIDY_CALENDAR].search([('sub_function', '=', sub_function_int)])
                if not validity_sub_function:
                    set_line_error(sheet, row, 'El registro de Subfunción en la celda M%s no se encuentre en nigún código programático en el calendario que desea modificar' % (row + 1))
                    continue
                else:
                    line.update({
                        'sub_function': sub_function,
                    })

            if program is None:
                set_line_error(sheet, row, 'El archivo no cuenta con la información del campo Programa en la celda N%s' % (row + 1))
                continue
            else:
                validity_program = self.env[FEDERAL_SUBSIDY_CALENDAR].search([('program', '=', program_int)])
                if not validity_program:
                    set_line_error(sheet, row, 'El registro de Programa en la celda N%s no se encuentra en ningún código programático en el calendario que desea modificar' % (row + 1))
                    continue
                else:                    
                    line.update({
                        'program': program_int,
                    })
            
            if not institution_activity:
                set_line_error(sheet, row, 'El archivo no cuenta con la información del campo Actividad Institucional en la celda O%s' % (row + 1))
                continue
            else:
                validity_institution_activity = self.env[FEDERAL_SUBSIDY_CALENDAR].search([('institution_activity', '=', institution_activity_int)])
                if not validity_institution_activity:
                    set_line_error(sheet, row, 'El registro de Actividad Institucional en la celda O%s no se encuentra en ningún código programático en el calendario que desea modificar' % (row + 1))
                    continue
                else:
                    line.update({
                        'institution_activity': institution_activity,
                    })
            
            if not project_identifier:
                set_line_error(sheet, row, 'El archivo no cuenta con la información del campo Identificador del Proyecto en la celda P%s' % (row + 1))
                continue
            else:
                validity_project_identifier =  self.env[FEDERAL_SUBSIDY_CALENDAR].search([('project_identification', '=', project_identifier)])
                if not validity_project_identifier:
                    set_line_error(sheet, row, 'El registro de Identificador de Proyecto en la celda P%s no se encuentra en ningún código programático en el calendario que desea modificar' % (row + 1))
                    continue
                else:
                    line.update({
                        'project_identifier': project_identifier,
                    })

            if not project:
                set_line_error(sheet, row, 'El archivo no cuenta con la información del campo Proyecto en la celda Q%s' % (row + 1))
                continue
            else:
                project_str = str(project_int).zfill(3)
                validity_project = self.env[FEDERAL_SUBSIDY_CALENDAR].search([('project', '=', project_str)])
                if not validity_project:
                    set_line_error(sheet, row, 'El registro de Proyecto en la celda Q%s no se encuentra en ningún código programático en el calendario que desea modificar' % (row + 1))
                    continue
                else:
                    line.update({
                        'project': project_str,
                    })
            
            if not federal_item:
                set_line_error(sheet, row, 'El archivo no cuenta con la información del campo Partida Federal en la celda R%s' % (row + 1))
                continue
            else:
                federal_departure_finance = self.env['federal.departure.finance'].search([('conversion_key', '=', federal_item_int)])
                if not federal_departure_finance:
                    set_line_error(sheet, row, 'El registro de Partida Federal en la celda R%s no se encuentra en el catálogo de Partida Federal en Finanzas' % (row + 1))
                    continue
                else:
                    validity_federal_item = self.env[FEDERAL_SUBSIDY_CALENDAR].search([('new_item_id', '=', federal_departure_finance.id)])
                    if not validity_federal_item:
                        set_line_error(sheet, row, 'El registro de Partida Federal en la celda R%s no se encuentra en ningún código programático en el calendario que desea modificar' % (row + 1))
                        continue
                    else: 
                        line.update({
                            'federal_item': federal_departure_finance.id,
                        })
            
            if not expense_type:
                set_line_error(sheet, row, 'El archivo no cuenta con la información del campo Tipo de Gasto en la celda S%s' % (row + 1))
                continue
            else:
                validity_expense_type = self.env[FEDERAL_SUBSIDY_CALENDAR].search([('expense_type', '=', expense_type_int)])
                if not validity_expense_type:
                    set_line_error(sheet, row, 'El registro de Tipo de Gasto en la celda S%s no se encuentra en ningún código programático en el calendario que desea modificar' % (row + 1))
                    continue
                else:
                    line.update({
                        'expense_type': expense_type_int,
                    })
            
            if not funding_source:
                set_line_error(sheet, row, 'El archivo no cuenta con la información del campo Fuente de Financiamiento en la celda T%s' % (row + 1))
                continue
            else:
                validity_funding_source = self.env[FEDERAL_SUBSIDY_CALENDAR].search([('funding_source', '=', funding_source_int)])
                if not validity_funding_source:
                    set_line_error(sheet, row, 'El registro de Fuente de Financiamiento en la celda T%s no se encuentra en ningún código programático en el calendario que desea modificar' % (row + 1))
                    continue
                else:
                    line.update({
                        'funding_source': funding_source_int,
                    })

            if not federative_entity:
                set_line_error(sheet, row, 'El archivo no cuenta con la información del campo Entidad Federativa en la celda U%s' % (row + 1))
                continue
            else:
                validity_federative_entity = self.env[FEDERAL_SUBSIDY_CALENDAR].search([('federal', '=', federative_entity_int)])
                if not validity_federative_entity:
                    set_line_error(sheet, row, 'El registro de Entidad Federativa en la celda U%s no se encuentra en ningún código programático en el calendario que desea modificar' % (row + 1))
                    continue
                else:
                    line.update({
                        'federative_entity': federative_entity_int,
                    })
            
            if key_wallet is None:
                set_line_error(sheet, row, 'El archivo no cuenta con la información del campo Clave Cartera en la celda V%s' % (row + 1))
                continue
            else:
                validity_key_wallet = self.env[FEDERAL_SUBSIDY_CALENDAR].search([('key_wallet', '=', key_wallet)])
                if not validity_key_wallet:
                    set_line_error(sheet, row, 'El registro de Clave Cartera en la celda V%s no se encuentra en ningún código programático en el calendario que desea modificar' % (row + 1))
                    continue
                else:
                    line.update({
                        'key_wallet': key_wallet,
                    })
            
            program_code_search = self.env[FEDERAL_SUBSIDY_CALENDAR].search([('year', '=', cycle_str), ('branch', '=', branch_int), ('unit', '=', unit), ('purpose', '=', purpose_int),
                                                                             ('function', '=', function_cal), ('sub_function', '=', sub_function_int), ('program', '=', program_int),
                                                                             ('institution_activity', '=', institution_activity_int), ('project_identification', '=', project_identifier),
                                                                             ('project', '=', project_str), ('new_item_id', '=', federal_departure_finance.id), ('expense_type', '=', expense_type_int),
                                                                             ('funding_source', '=', funding_source_int), ('federal', '=', federative_entity_int), ('key_wallet', '=', key_wallet)])
            if not program_code_search:
                set_line_error(sheet, row, 'La composición del código programático no coincide con los que están en el calendario de montos asignados del año %s' % (cycle_str))
                continue

            if not transaction:
                set_line_error(sheet, row, 'El archivo no cuenta con la información del campo Operación en la celda W%s' % (row + 1))
                continue
            else:
                line.update({
                    'transaction': transaction,
                })

            try:
                line.update({'january': float(january)})
            except:
                set_line_error(sheet, row, 'Formato del monto de Enero es invalido. Error en la celda X%s' % (row + 1))
                continue
            try:
                line.update({'february': float(february)})
            except:
                set_line_error(sheet, row, 'Formato del monto de Febrero es invalido. Error en la celda Y%s' % (row + 1))
                continue
            try:
                line.update({'march': float(march)})
            except:
                set_line_error(sheet, row, 'Formato del monto de Marzo es invalido. Error en la celda Z%s' % (row + 1))
                continue
            try:
                line.update({'april': float(april)})
            except:
                set_line_error(sheet, row, 'Formato del monto de Abril es invalido. Error en la celda AA%s' % (row + 1))
                continue
            try:
                line.update({'may': float(may)})
            except:
                set_line_error(sheet, row, 'Formato del monto de Mayo es invalido. Error en la celda AB%s' % (row + 1))
                continue
            try:
                line.update({'june': float(june)})
            except:
                set_line_error(sheet, row, 'Formato del monto de Junio es invalido. Error en la celda AC%s' % (row + 1))
                continue
            try:
                line.update({'july': float(july)})
            except:
                set_line_error(sheet, row, 'Formato del monto de Julio es invalido. Error en la celda AD%s' % (row + 1))
                continue
            try:
                line.update({'august': float(august)})
            except:
                set_line_error(sheet, row, 'Formato del monto de Agosto es invalido. Error en la celda AE%s' % (row + 1))
                continue
            try:
                line.update({'september': float(september)})
            except:
                set_line_error(sheet, row, 'Formato del monto de Septiembre es invalido. Error en la celda AF%s' % (row + 1))
                continue
            try:
                line.update({'october': float(october)})
            except:
                set_line_error(sheet, row, 'Formato del monto de Octubre es invalido. Error en la celda AG%s' % (row + 1))
                continue
            try:
                line.update({'november': float(november)})
            except:
                set_line_error(sheet, row, 'Formato del monto de Noviembre es invalido. Error en la celda AH%s' % (row + 1))
                continue
            try:
                line.update({'december': float(december)})
            except:
                set_line_error(sheet, row, 'Formato del monto de Diciembre es invalido. Error en la celda AI%s' % (row + 1))
                continue
                
            
            line.update({'folio': (str(cycle_int) + str(branch_ord_int) + str(ur_ord) + str(request_number_int))})
            line.update({'load_type': 'manual'})

            lines_ok.append((0, 0, line))


        if msg_err:
            content = "................... Registros erróneos " + str(datetime.today()) + "...................\r\n"
            content += "\r\n".join(msg_err)
            self.modifications_federal_subsidy_id.failed_row_file = base64.b64encode(content.encode('utf-8'))
        else:
            self.modifications_federal_subsidy_id.failed_row_file = None
        
        self.modifications_federal_subsidy_id.modifications_lines_ids = None
        
        self.modifications_federal_subsidy_id.modifications_lines_ids = lines_ok
        
        self.modifications_federal_subsidy_id.total_rows = self.record_number

        self.modifications_federal_subsidy_id.success_rows = len(self.modifications_federal_subsidy_id.modifications_lines_ids)
        
        self.modifications_federal_subsidy_id.record_number = self.record_number

        self.modifications_federal_subsidy_id.imported_record_number = len(self.modifications_federal_subsidy_id.modifications_lines_ids)

        self.modifications_federal_subsidy_id.failed_rows = (self.record_number - len(self.modifications_federal_subsidy_id.modifications_lines_ids))

        

    
