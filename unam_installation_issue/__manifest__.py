{
    'name': 'UNAM: Installation issue',
    'summary': '''
        Not able to install jt_budget_management
    ''',
    'description': '''
        Not able to install jt_budget_management
    ''',
    'license': 'OPL-1',
    'author': 'Odoo Ps',
    'website': 'https://www.odoo.com',
    'category': 'Development Services/Custom Development',
    'version': '1.1',
    'depends': [
        'account_reports',
        'account_accountant',
    ],
    'data': [
        # Security.
        'security/ir.model.access.csv',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
