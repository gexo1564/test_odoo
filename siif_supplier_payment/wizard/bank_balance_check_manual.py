from odoo import models, fields,_,api
from datetime import date

class BankBalanceCheck(models.TransientModel):

    _name = 'bank.balance.check.manual'
    _description = 'Bank Balance Check Manual'

    journal_id = fields.Many2one('account.journal','Bank Of Payment Issue')
    account_id = fields.Many2one('account.account','Bank Account')
    bank_account_id = fields.Many2one(related="journal_id.bank_account_id",string="Bank Account")
    total_amount = fields.Float('Total Amount')
    total_request = fields.Integer('Total Request')
    invoice_ids = fields.Many2many('account.move', 'account_invoice_payment_rel_bank_balance_manual', 'payment_id', 'invoice_id', string="Invoices", copy=False, readonly=True)
    folio = fields.Integer("Folio")
    request_date = fields.Date("Request Date",default=date.today())

    @api.onchange('journal_id')
    def onchange_jounal(self):
        if self.journal_id:
            self.account_id = self.journal_id.default_debit_account_id and self.journal_id.default_debit_account_id.id or False
        else:
            self.account_id = False

    def assign_manual_folio(self):
        move_ids = self.invoice_ids
        # Modificación de la cuenta origen
        for move_id in move_ids.filtered(lambda r: r.payment_state == 'for_payment_procedure'):
            # Rechaza el pago asociado
            payment_id = self.env['account.payment'].search([
                ('payment_request_id', '=', move_id.id),
                ('batch_folio', '=', move_id.batch_folio)
            ])
            if payment_id:
                payment_id.change_origin_account()
            # Actualiza el status de la solicitud de pago
            move_id.write({'payment_state': 'origin_account_change'})
        query = """
            update account_move set batch_folio = %s where id in %s
        """
        self.env.cr.execute(query, (self.folio, tuple(self.invoice_ids.ids)))
        # Programación de los pagos
        self.env['account.payment'].schedule_payment_with_bank_account(
            self.journal_id,
            move_ids.ids,
        )
        # Creación de los lotes
        self.env['batch.management'].create({
            'type': 'manual',
            'journal_id': self.journal_id.id,
            'folio': self.folio,
            'payment_schedule_date': self.request_date,
            'amount_total': self.total_amount,
            'number_of_payments': len(move_ids),
        })
