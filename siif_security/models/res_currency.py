from lxml import etree
from odoo import models, api

class AccountPayment(models.AbstractModel):
    _inherit = 'res.currency'

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):

        def hide_buttons(node, value):
            node.set('create', value)
            node.set('import', value)
            node.set('export_xlsx', value)
            node.set('delete', value)
            node.set('edit', value)

        res = super().fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])
        # Módulo de Ingresos
        if '.accounting.module' in res['name']:
            # Aplica para las siguientes vistas:
            #   - Configuracion:
            #       - Contabilidad
            #           - Monedas:
            if '.currency' in res['name']:
                is_admin_contabilidad = self.env.user.has_group('jt_account_base.group_account_admin_user')
                is_training_contabilidad = self.env.user.has_group('jt_account_base.group_account_training_user')
                # Oculta los botones y acciones para los usuarios que no son administradores
                # o de registro.
                if not (is_admin_contabilidad or is_training_contabilidad):
                    for node in doc.xpath("//" + view_type):
                        # Ocultar botones de las vistas Odoo
                        hide_buttons(node, '0')
                    # Desabilitar el toggle button (switch) de activo
                    if view_type == 'tree':
                        for node in doc.xpath("//field[@name='active']"):
                            node.set('modifiers', '{"readonly":true}')
        # Módulo de Conac
        elif '.conac' in res['name']:
            # Aplica para las siguientes vistas:
            #   - Configuracion:
            #       - Contabilidad
            #           - Monedas:
            if '.currency' in res['name']:
                is_admin_contabilidad = self.env.user.has_group('jt_account_base.group_account_admin_user')
                is_training_contabilidad = self.env.user.has_group('jt_account_base.group_account_training_user')
                # Oculta los botones y acciones para los usuarios que no son administradores
                # o de registro.
                if not (is_admin_contabilidad or is_training_contabilidad):
                    for node in doc.xpath("//" + view_type):
                        # Ocultar botones de las vistas Odoo
                        node.set('export', 'false')
                    # Desabilitar el toggle button (switch) de activo
                    if view_type == 'tree':
                        for node in doc.xpath("//field[@name='active']"):
                            node.set('modifiers', '{"readonly":true}')
        res['arch'] = etree.tostring(doc)
        return res