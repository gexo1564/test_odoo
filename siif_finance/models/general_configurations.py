from odoo import models, fields, api,_
from datetime import datetime, timedelta
from odoo.exceptions import UserError
from lxml import etree

class ExcludeProgramCode(models.Model):

     _name = 'exclude.program.code'
     _description = "Exclude program code"
     _rec_name = 'program_code_id'

     item_key_id = fields.Many2one('expenditure.item', "Expenditure Item")
     program_code_id = fields.Many2one('program.code',"Program Code")
     resource_origin_id = fields.Many2one('resource.origin', "Resource origin")
     year_related = fields.Char('Year related')
     item_related = fields.Integer('Item related')
     origin_related = fields.Integer(string="Origin id")

     @api.onchange('item_key_id')
     def _compute_domain(self):
          self.item_related = self.item_key_id.id
          self.year_related = datetime.today().year
          #self.origin_related = self.resource_origin_id
class AccountMovements(models.Model):

   _name = 'account.movements'
   _description = 'Account to unrecognized movements'
   _rec_name = 'account_id'

   account_id  = fields.Many2one('account.account', "Account")
   applications = fields.Selection([('divisas','Fondo de Divisas'),('otros','Otros')],string='Applications',required=True)
   
   @api.model
   def create(self, vals):
      if self.env['account.movements'].search_count([('applications','=',vals.get('applications'))])>=1:
         raise UserError('No se puede agregar otro registro para el uso o aplicación: '+dict(self._fields['applications'].selection).get(vals.get('applications')))
      else:
         return super(AccountMovements, self).create(vals)
   @api.model
   def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
     res = super().fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
     doc = etree.XML(res['arch'])

     is_admin_finance = self.env.user.has_group('jt_payroll_payment.group_on_finance_admin')
     is_finance_user = self.env.user.has_group('jt_payroll_payment.group_admin_finance_user')
     """
     Sección de Seguridad aplicado para:
          Módulo de Finanzas
          -Motivos de Rechazo de Solicitudes de compra
     """
     if is_admin_finance or is_finance_user:
          for node in doc.xpath("//" + view_type):
               node.set('edit', '1')
               node.set('create', '1')
               node.set('delete', '1')
               node.set('import', '1')
               node.set('export_xlsx', '1')
               node.set('export', '1')

     else:
          for node in doc.xpath("//" + view_type):
               node.set('edit', '0')
               node.set('create', '0')
               node.set('delete', '0')
               node.set('import', '0')
               node.set('export_xlsx', '0')
               node.set('export', '0')
          
          if res.get('toolbar') and res['toolbar'].get('action'):
               res['toolbar']['action'] = []


     res['arch'] = etree.tostring(doc)

     return res

class PaymentJournals(models.Model):

     _name = 'payment.journals'
     _description = 'Payment Journals'
     _rec_name = 'journal_id'

     journal_id = fields.Many2one('account.journal', string='Payment Journal')
     type_of_currency = fields.Selection(
          [('national', 'National Currency'), ('foreign', 'Foreign Currency')])

class ProvisionalAccountComission(models.Model):

   _name = 'provisional.account.comission'
   _description = 'Provisional account comission'
   _rec_name = 'account_id'

   account_id  = fields.Many2one('account.account', "Account")


class ExchangeRate(models.Model):

     _name = 'exchange.rate'
     _description = 'Exchange rate'
     _rec_name = 'badge_id'


     date = fields.Date("Date")
     currency_type = fields.Selection([('PESOS', 'PESOS'), ('DOLAR', 'DOLAR')])
     badge_id = fields.Many2one('badge', string='Badge')
     sale =  fields.Float('Sale')
     purchase = fields.Float('Purchase')

class Badge(models.Model):
     _name = 'badge'
     _description = 'Badge'
     _rec_name = 'siiof_name'


     siiof_name = fields.Char('SIIOF Name')
     currency_id = fields.Many2one('res.currency', string='Currency')


