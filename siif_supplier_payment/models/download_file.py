from odoo import http
from odoo.http import request
from odoo.addons.web.controllers.main import serialize_exception,content_disposition
import base64

class Binary(http.Controller):
    @http.route('/web/binary/download_bank_layout', type='http', auth="public")
    @serialize_exception
    def download_download_bank_layout(self, id, **kw):
        """ Download link for files stored as binary fields.
        :param str id: id of the record from batch.management
        :returns: :class:`werkzeug.wrappers.Response`
        """
        model = request.registry['batch.management']
        filecontent, filename = None, None
        # Busca el folio que tiene el id recibido
        batch_id = request.env['batch.management'].sudo().search([('id', '=', int(id))])
        # Obtiene el archivo y su nombre
        filecontent, filename = batch_id.download_layout_file()
        if not filecontent:
            return request.not_found()
        else:
            filecontent = base64.b64decode(filecontent)
            if not filename:
                filename = '%s_%s' % (model.replace('.', '_'), id)
            return request.make_response(filecontent,
                            [('Content-Type', 'application/octet-stream'),
                            ('Content-Disposition', content_disposition(filename))])