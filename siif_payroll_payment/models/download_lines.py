from odoo import http
from odoo.http import request
from odoo.addons.web.controllers.main import serialize_exception,content_disposition
import base64

class Binary(http.Controller):
    @http.route('/web/binary/download_payroll_request_lines', type='http', auth="public")
    @serialize_exception
    def download_payroll_request_lines(self, id, filename=None, **kw):
        """ Download link for files stored as binary fields.
        :param str model: name of the model to fetch the binary from
        :param str field: binary field
        :param str id: id of the record from which to fetch the binary
        :param str filename: field holding the file's name, if any
        :returns: :class:`werkzeug.wrappers.Response`
        """
        model = request.registry['account.move']
        # Dependiento del tipo de lineas se genera el archivo
        filecontent = None
        move_id = request.env['account.move'].sudo().search([('id', '=', int(id))])
        # Archivo de las Excel de la solicitud de Pago
        filecontent = move_id.get_payroll_payment_lines()
        if not filecontent:
            return request.not_found()
        else:
            filecontent = base64.b64decode(filecontent)
            if not filename:
                filename = '%s_%s' % (model.replace('.', '_'), id)
            return request.make_response(filecontent,
                            [('Content-Type', 'application/octet-stream'),
                            ('Content-Disposition', content_disposition(filename))])