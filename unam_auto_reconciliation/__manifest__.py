# -*- coding: utf-8 -*-
{
    'name': 'UNAM: Auto Reconcile By Reference Number',
    'summary': '''
        Automatically reconciles bank statements with account lines that
        have matching reference numbers
    ''',
    'description': '''
        MPV - TASK ID: 2893835
        Gives new option to account.reconcile.model models to allow invoices
        to be automatically reconcilled with bank statements if they have the
        same reference number on both sides. 
    ''',
    'license': 'OPL-1',
    'author': 'Odoo Inc',
    'website': 'https://www.odoo.com',
    'category': 'Development Services/Custom Development',
    'version': '13.0.1',
    'depends': [
        'account'
    ],
    'data': [
        'views/account_views.xml'
    ],
    'installable': True,
    'application': False,
    'auto_install': False
}