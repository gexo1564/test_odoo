import logging
from lxml import etree
from odoo import models, api

class AccountMove(models.AbstractModel):
    _inherit = 'account.move.line'

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):

        res = super().fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])

        # Validación de Usuarios
        is_admin_contabilidad = self.env.user.has_group('jt_account_base.group_account_admin_user')
        is_training_contabilidad = self.env.user.has_group('jt_account_base.group_account_training_user')
        is_register_contabilidad = self.env.user.has_group('jt_account_base.group_account_register_user')

        views_diarios = [
            "account.move.line.tree.grouped.sales.purchase",
            "account.move.line.tree.grouped.bank.cash",
            "account.move.line.tree.grouped.misc",
        ]

        # Módulo de Contabilidad
        if '.accounting' in res['name']:
            # Aplica para las siguientes:
            #   - Contabilidad:
            #       - Apuntes Contables
            if '.notes' in res['name']:
                if not (is_admin_contabilidad or is_training_contabilidad):
                    if not is_register_contabilidad:
                        for node in doc.xpath("//" + view_type):
                            # Ocultar botones de las vistas Odoo
                            node.set('create', '0')
                            node.set('import', '0')
                            node.set('delete', '0')
                            node.set('edit', '0')
                    else:
                        for node in doc.xpath("//" + view_type):
                            node.set('create', '0')
                            node.set('import', '0')
                            node.set('delete', '0')
                    if res.get('toolbar') and res['toolbar'].get('action'):
                        res['toolbar']['action'] = []
        # account.move.line.tree.grouped.sales.purchase
        if res['name'] in views_diarios:
            if not (is_admin_contabilidad or is_training_contabilidad):
                if res.get('toolbar'):
                    res['toolbar']['action'] = []

        # Facturación digital
        is_group_income_fac_administrator_user = self.env.user.has_group('jt_income.group_income_fac_administrator_user')
        is_group_income_fac_finance_user = self.env.user.has_group('jt_income.group_income_fac_finance_user')
        is_group_income_fac_accounting_user = self.env.user.has_group('jt_income.group_income_fac_accounting_user')
        
        if (is_group_income_fac_administrator_user or is_group_income_fac_finance_user or is_group_income_fac_accounting_user):

            for node in doc.xpath("//" + view_type):
                node.set('export', '0')
                node.set('delete', '0')
                

            actions_to_hide = [
                "Romper conciliación",
                "Conciliar",
                "Crear asiento de periodificación",
                "Crear entrada diferida",
                "Crear Activo",
            ]
            if res.get('toolbar'):
                if res['toolbar'].get('action'):
                    action_items = filter(lambda x: x['display_name'] not in actions_to_hide, 
					res['toolbar']['action'])
                    res['toolbar']['action'] = list(action_items)

        res['arch'] = etree.tostring(doc)
        return res