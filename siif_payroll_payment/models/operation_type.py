# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

class OperationType(models.Model):
    _inherit = 'operation.type'

    specific_items_allowed_ids = fields.Many2many(string='Specific Items Allowed', comodel_name='expenditure.item', relation='operation_type_expenditure_item_allowed_relation', column1='operation_type_id', column2='expenditure_item_id', help='Specific Items Allowed for Account Payable Type')
    item_groups_allowed = fields.Char(string='Item Groups Allowed', help='Item Groups Allowed for Account Payable Type, Numbers separated by coma. ie.: 200,300')


    @api.onchange('item_groups_allowed')
    def _onchange_item_groups_allowed(self):
        for record in self:
            if record.item_groups_allowed:
                record.item_groups_allowed = record.item_groups_allowed.replace(' ', '').strip(',')
                item_groups = record.item_groups_allowed.split(',')
                for item_group in item_groups:
                    if not item_group.isnumeric() or int(item_group) % 100 != 0:
                        raise ValidationError(_('Item groups specified are not groups, please verify them. Groups example: 200,300'))