# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields, api, _


class QuarterBudget(models.Model):

    _name = 'quarter.budget'
    _description = 'Quarter Budget'

    name = fields.Char(string='Quarter', translate=True, required=True)
    start_date = fields.Char(string='Start Date')
    end_date = fields.Char(string='End Date')

    _sql_constraints = [('quarter_budget_name_unique', 'unique(name)', _('Quarter budget: Name must be unique.'))]
    
    
    
    @api.model
    def fields_get(self, fields=None, attributes=None):
        
        no_selectable_fields = ['quarter_budget_id', 'start_date', 'end_date']
        
        no_sortable_field = ['id', 'start_date', 'end_date']

        res = super(QuarterBudget, self).fields_get()
        
        for key, value in res.items():
            if key in no_selectable_fields:
                value.update({'selectable': False})
        
        for key, value in res.items():
            if key in no_sortable_field:
                value.update({'sortable': False})
                
        return res