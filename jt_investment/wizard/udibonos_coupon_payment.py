import json
import logging
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
import datetime
import requests
from dateutil.relativedelta import relativedelta
class UDICouponPaymentWizard(models.TransientModel):
    _name = 'udi.coupon.payment.wizard'
    _description = 'Wizard para calcular el pago del cupon de los udibonos en el periodo requerido'

    id_udibono = fields.Integer()

    date_consult = fields.Date("Fecha que se Realiza el Movimiento", default=datetime.date.today())

    udi_value = fields.Float("Valor de la UDI",digits=(4, 9))

        
    @api.constrains('udi_value')
    def check_udi_value(self):
        if self.udi_value <= 0:
            raise UserError(_('Negative values or values equal to zero are not allowed'))


    @api.onchange('date_consult')
    def get_udi_value_on_date(self):
        today = self.date_consult
        token = self.env['ir.config_parameter'].get_param('banxico_token')
        url_banxico = self.env['ir.config_parameter'].get_param('banxico_api_url')
        url = f"{url_banxico}SP68257/datos/{today}/{today}?token={token}"
        response = requests.get(url,timeout=10)

        if response.status_code == 200:
            data = json.loads(response.content)
            if "datos" not in data["bmx"]["series"][0]:
                self.udi_value = 0
            else:
                udibono = data["bmx"]["series"][0]["datos"][0]["dato"]
                if udibono is not None:
                    self.udi_value = udibono
                else:
                    self.udi_value = 0
        else:
            self.udi_value = 0
          
    def coupon_payment(self):

        self.id_udibono = self.env.context.get('active_id')

        # objeto de tipo udibono para consultar el id del udibono actual   
        udibono = self.env['investment.udibonos'].search([('id', '=', self.id_udibono)])
        
        # Movement_date es la fecha que se usa de momento como test, se cambiara a la fecha del dia 
        udibono.movement_date = self.date_consult
        udibono.udi_value_end_month = self.udi_value

        # Verificar que la fecha no sea la misma que la de liquidación 
        if self.date_consult == udibono.expiry_date:
            raise UserError('Actualmente no es posible realizar esta acción. Por favor, realiza la liquidación')

        # Se llama a la funcion para realizar el calculo
        udibono.calculate_coupon_payment()

        return self.id_udibono