import logging
from odoo import models, fields, api, _
from datetime import datetime
from odoo.exceptions import UserError, ValidationError
import pandas as pd
from pandas.tseries.offsets import BMonthEnd, BMonthBegin


class AccruedInterest(models.Model):
    _name = 'accrued.interest'
    _description = "Accrued Interest"

    days_period_elapsed = fields.Integer("Days Period Elapsed")
    udi_value_first_day_month = fields.Float(
        "Value of the UDI on the first day of the month", digits=(4, 6))
    udi_value_end_month = fields.Float(
        "Value of the UDI at the end of the month", digits=(4, 6))
    start_month_nominal_value = fields.Float("Start Month Nominal Value",digits=(8, 6))
    end_month_nominal_value = fields.Float("End Month Nominal Value",digits=(8, 6))
    difference = fields.Float("Difference", digits=(4, 6))
    accrued_interest = fields.Float("Accrued Interest", digits=(6, 4))
    unrecorded_interest = fields.Float("Unrecorded Interest", digits=(4, 6))
    accrued_interest_to_date = fields.Float(
        "Accrued Interest to date", digits=(6, 4))
    performance = fields.Char("Performance")
    supporting_documentation_files = fields.Many2many(
        "ir.attachment", string="Add Files", attachment=True)
    date = fields.Date("Date")
    description = fields.Char("Description")
    investment_nominal_value = fields.Float(
        "Investment Nominal Value", digits=(6, 4))
    coupon_payment = fields.Float("Coupon Payment", digits=(6, 4))

    previous_month_titles_value = fields.Float(digits=(8, 6))
    current_month_titles_value = fields.Float(digits=(8, 6))
    accomulated_difference = fields.Float(digits=(8, 6))
    indicator = fields.Float("Indicator",digits=(8, 4))
    rate = fields.Float(digits=(8, 4))

    def _cron_interest_accrued_business_day_month(self):
        current_date = pd.Timestamp(datetime.today().date())

        # Inversiones a plazo
        self.env['term.investments'].interest_accrued()
        
        # Ultimo dia habil
        last_business_day_month = BMonthEnd().rollforward(current_date)
        if current_date == last_business_day_month:
            
            # Inversiones a plazo
            # self.env['term.investments'].interest_accrued_last_business_day_month()
            # UDIBONOS
            self.env['productive.account'].start_check_end_month()
        

        # Primer dia habil
        #first_day_of_the_month = BMonthBegin().rollforward(current_date)


    #     ## Fecha de pago del cupon
    #     udibonos_records = self.env['investment.udibonos'].search([])

    #    #Se realiza el recorrido de los registros
    #     for records in udibonos_records:
    #          #Si el registro esta en estado aprovado se realizan las acciones
    #         if records.state == 'active':
    #             logging.info("Evalua registro con fecha de vencimiento/pago")
    #             records.calculate_days_of_periot()


    def unlink(self):


        ## Validacion para no eliminar registros que ya estan vinculados a un asiento contable de cuentas productivas
        for rec in self:
            if rec.productive_account_id:
                obj = self.env['account.move.line'].search([('date','=',rec.date),('productive_account_id.id','=',rec.productive_account_id.id)])
                if obj: 
                    for records in obj:
                        accrued_interest = round(rec.accrued_interest,2)
                        if accrued_interest == records.debit or accrued_interest == records.credit:
                            raise UserError(_("No se puede eliminar el registro, ya que este ya esta vinculado a un asiento contable."))

        return super(AccruedInterest, self).unlink()
    
