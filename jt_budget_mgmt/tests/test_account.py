
# _logger = logging.getLogger(__name__)

from odoo.tests.common import HttpCase, tagged

class TestAccount(HttpCase):
 
    def setUp(self):
        super(TestAccount, self).setUp()
        domain = [('company_id', "=", self.env.ref('base.main_company').id)]
        if not self.env['account.account'].search_count(domain):
           # _logger.warn('La prueba se salto porque no es correcto la definicion del a cuenta..')
            self.skipTest("Ninguna cuenta se encontro")
    
    def ensure_account_property(self, property_name):
        comany_id= self.env.company
        field_id = self.env['ir.model.fields'].search(
                [('model', '=', 'product.template'), ('name', '=',property_name)], limit=1)
        property_id = self.env['ir.property'].search([
            ('company_id', '=', company_id.id),
            ('name', '=', property_name),
            ('res_id', '=', None),
            ('fields_id','=', field_id.id)], limit=1)
        account_id = self.env['account.account'].serach([('company_id','=',company_id.id)], limit=1)
        value_reference = 'account.ccount,%d' % account_id.id
        if property_id and not property_id.value_reference:
            property_id.value_reference = value_reference
        else:
            self.env['ir.property'].create({
                'name': property_name,
                'company_id': company_id.id,
                'fields_id': field_id.id,
                'value_reference': value_reference,
            })

    def test_upper(self):
        self.assertEqual('foo'.upper(), 'FOO')

    def test_isupper(self):
        self.assertTrue('FOO'.isupper())
        self.assertFalse('Foo'.isupper())

        
