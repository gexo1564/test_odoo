from odoo import models, fields, api,_
from lxml import etree

class AccountConversion(models.Model):

	_name="account.conversion"
	_description="Account conversion"
	_rec_name="account_id"

	account_id =  fields.Many2one("account.account", string="Account accountant")
	account =  fields.Char("Account")
	sub_account =  fields.Char("Subaccount")
	sub_sub_account = fields.Char("Subsubaccount")
 
class AccountConversion(models.AbstractModel):
	_inherit = 'account.conversion'
	@api.model
	def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
		res = super().fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
		doc = etree.XML(res['arch'])
	
		# Validación de Usuarios
		is_cd_user_finanzas = self.env.user.has_group('jt_income.group_income_cd_finance_user')
		is_cd_user_contabilidad = self.env.user.has_group('jt_income.group_income_cd_accounting_user')
		is_cd_admin = self.env.user.has_group('jt_income.group_income_cd_administrator')
		
		if (is_cd_user_finanzas or is_cd_user_contabilidad or is_cd_admin): 
			for node in doc.xpath("//" + view_type):
       #ocultar botones para grupos de certificado de depositos en el modulo de contabilidad 
				node.set('create', '0') 
				node.set('edit', '0')	
				node.set('delete', '0')
			res['arch'] = etree.tostring(doc) # Se actualiza la arquitectura de la vista
		return res
    

