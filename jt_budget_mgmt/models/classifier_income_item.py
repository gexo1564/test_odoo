#-*- coding: utf-8 -*-
import json
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

class ClassifierIncomeItem(models.Model):
    _name = 'classifier.income.item'
    _description = 'Clasificador por Rubro de Ingresos'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'name'
    _order = "name asc"

    name = fields.Text(string='Nombre', compute="_compute_cri", store=True, tracking=True)
    item = fields.Char(string='Rubro', required=True, tracking=True)
    type = fields.Char(string='Tipo', required=True, tracking=True)
    class_ = fields.Char(string='Clase', default='00', required=True, tracking=True)
    concept = fields.Char(string='Concepto', default='00', required=True, tracking=True)
    short_description = fields.Text(string='Descripción corta', required=True, tracking=True)
    large_description = fields.Text(string='Descripción larga', tracking=True)

    @api.depends('item', 'type', 'class_', 'concept')
    def _compute_cri(self):
        for record in self:
            classifier = ''
            if record.item:
                classifier += str(record.item)
            if record.type:
                classifier += str(record.type)
            if record.class_:
                classifier += str(record.class_)
            if record.concept:
                classifier += str(record.concept)
            record.name = classifier

    _sql_constraints = [('name', 'unique(name)', ('Clasificador por Rubro de ingresos debe ser único.'))]

    def name_get(self):
        result = []
        for rec in self:
            name = rec.name or ''
            if self.env.context:
                if rec.short_description and self.env.context.get('show_name_cri',False):
                    name += ' ' + rec.short_description
            result.append((rec.id, name))
        return result

    # To check that the size of the item is exactly 1 and is a numeric value
    @api.constrains('item')
    def _check_item(self):
        if len(str(self.item)) != 1:
            raise ValidationError(('El tamaño de item debe ser uno.'))
        if not str(self.item).isnumeric():
            raise ValidationError(_('El item debe ser un valor numerico.'))
        
    # To check that the size of the type is exactly 1 and is a numeric value
    @api.constrains('type')
    def _check_type(self):
        if len(str(self.type)) != 1:
            raise ValidationError(('El tamaño de tipo debe ser uno.'))
        if not str(self.type).isnumeric():
            raise ValidationError(_('El tipo debe ser un valor numerico.'))
        
    # To check that the size of the class_ is exactly 2 and is a numeric value
    @api.constrains('class_')
    def _check_class_(self):
        if len(str(self.class_)) != 2:
            raise ValidationError(('El tamaño de clase debe ser dos .'))
        if not str(self.class_).isnumeric():
            raise ValidationError(_('La clase debe ser un valor numerico.'))
        
    # To check that the size of the concept is exactly 2 and is a numeric value
    @api.constrains('concept')
    def _check_concept(self):
        if len(str(self.concept)) != 2:
            raise ValidationError(('El tamaño de concepto debe ser dos.'))
        if not str(self.concept).isnumeric():
            raise ValidationError(_('El concepto debe ser un valor numerico.'))