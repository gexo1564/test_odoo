# -*- coding: utf-8 -*-
import logging
import json
from lxml import etree
from odoo import models, fields, api, _
class AccountMove(models.Model):
    _inherit = 'account.move'    
    money_fund_move_id = fields.Many2one('money.funds.request.ministration')
class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    # Campo de referencia de los asientos contables con la cuenta por pagar
    #money_fund_request_incr_transfer_id = fields.Many2one('money.funds.request.increment')
    #money_fund_request_min_transfer_id = fields.Many2one('money.funds.request.ministration')
    money_fund_request_ret_transfer_id = fields.Many2one('money.funds.request.returns')
    money_fund_request_cancel_transfer_id = fields.Many2one('money.funds.cancel')
