from odoo import api, fields, models, _


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    group_id = fields.Many2one(comodel_name='account.group', related='account_id.group_id', store=True)

    # -----------------------------------------------------------------------
    # 1. Expense per program code item
    # -----------------------------------------------------------------------
    expenditure_item_id = fields.Many2one(related='program_code_id.item_id', store=True)

    resource_origin_id = fields.Many2one(related='program_code_id.resource_origin_id', store=True)

    # -----------------------------------------------------------------------
    # 2. Expenses per dependency
    # -----------------------------------------------------------------------
    program_code_dependency_id = fields.Many2one(related='program_code_id.dependency_id', string='Dependency', store=True)

    # -----------------------------------------------------------------------
    # 4. Expenses per provider
    # Fixes issue column reference 'partner_id' is ambiguous
    # -----------------------------------------------------------------------
    # provider_id = fields.Many2one(related='partner_id', string='Provider', store=False)

    # -----------------------------------------------------------------------
    # 5. By group of accounts
    # Fixes issue column reference 'account_id' is ambiguous
    # -----------------------------------------------------------------------
    # unam_account_id = fields.Many2one(related='account_id', string='UNAM Account', store=False)

