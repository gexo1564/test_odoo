# -*- coding: utf-8 -*-
# odoo
from odoo import models, fields, api, _
# imports from modules

IR_ATTACHMENT_MODEL = "ir.attachment"
ADD_FILES_TEXT = "Add files"
STATES = [
    ('draft', 'Draft'),
    ('registered','Registered'),
]

class PayHandbooks(models.Model):
    _name = 'siif.pay.handbooks'
    _description = "Manuals/Tutorials"

    state = fields.Selection(STATES, default=STATES[0][0], string='State', readonly=True)
    name = fields.Char(string="Directory Name")
    handbooks_ids = fields.Many2many(IR_ATTACHMENT_MODEL, string = ADD_FILES_TEXT)
    diagrams_ids = fields.Many2many(comodel_name=IR_ATTACHMENT_MODEL, 
        relation="siif_pay_handbooks_diagrams_rel", 
        column1="diagram_id",
        column2="attachment_id",
        string = ADD_FILES_TEXT)

    documents_ids = fields.Many2many(comodel_name=IR_ATTACHMENT_MODEL, 
        relation="siif_pay_handbooks_documents_rel", 
        column1="document_id",
        column2="attachment_id",
        string = ADD_FILES_TEXT)
    video_ids = fields.One2many(comodel_name = 'siif.pay.video', string="Videos", inverse_name="handbook_siif_id", ondelete = "cascade"  )
    comments = fields.Text(string="Comments")

    
    @api.model
    def create(self, vals):
        vals['state'] =  'registered'
        result = super(PayHandbooks, self).create(vals)
        return result

class HandbookVideoBASE(models.Model):
    # Private attributes
    _name="siif.pay.video"
    name = fields.Char(String="Alias", required=True )
    video = fields.Text(string="Video", help="Put the video link", required=True)
    handbook_siif_id = fields.Many2one( comodel_name= 'siif.pay.handbooks',string='Manuals/Tutorials', ondelete = "cascade")