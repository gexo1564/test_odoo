import logging
from odoo import fields, models, api, _
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.tools.misc import formatLang
from odoo.tools.misc import xlsxwriter
import io
import base64
from odoo.tools import config, date_utils, get_lang
import lxml.html
import logging
import math
import json

class ValidityReport(models.AbstractModel):
    _name = "validity.report"
    _inherit = "account.coa.report"
    _description = "Validity report"

    filter_date = None
    filter_comparison = None
    filter_all_entries = None
    filter_journals = None
    filter_analytic = None
    filter_unfold_all = None
    filter_cash_basis = None
    filter_hierarchy = None
    filter_unposted_in_period = None
    MAX_LINES = None

    filter_line_pages = None

    @api.model
    def _init_filter_line_pages(self, options, previous_options=None):
        options['line_pages'] = []
        options['is_xlsx_report'] = False
        start = str(datetime.now().year)+'-01-01'
        end = str(datetime.now().year)+'-12-31'

        budget_lines = self.env['program.code'].search(
            [('state', '=', 'validated'),('year.name', '=', str(datetime.now().year))])
        program_codes = budget_lines.mapped('id')
        pages = math.ceil(len(program_codes) / 500)
        line_list = []
        for page in range(1, pages + 1):
            line_list.append(page)

        list_labels = self._context.get('lines_data', line_list)
        counter = 1

        if previous_options and previous_options.get('line_pages'):
            line_pages_map = dict((opt['id'], opt['selected']) for opt in previous_options['line_pages'] if opt['id'] != 'divider' and 'selected' in opt and opt['id'] not in previous_options['selected_line_pages'])
        else:
            line_pages_map = {}

        options['selected_line_pages'] = []
        for label in list_labels:
            options['line_pages'].append({
                'id': str(counter),
                'name': str(label),
                'code': str(label),
                'selected': line_pages_map.get(str(counter)),
            })
            if line_pages_map.get(str(counter)):
                options['selected_line_pages'].append(str(label))
            counter += 1

    def _get_reports_buttons(self):
            return [
                    {'name': _('Export to PDF'), 'sequence': 1, 'action': 'print_pdf', 'file_export_type': _('PDF')},
                    {'name': _('Export (XLSX)'), 'sequence': 2, 'action': 'print_xlsx', 'file_export_type': _('XLSX')},
                ]

    #Set columns based on dynamic options
    def print_xlsx(self, options):
        options['is_xlsx_report'] = True
        return {
                'type': 'ir_actions_account_report_download',
                'data': {'model': self.env.context.get('model'),
                         'options': json.dumps(options),
                         'output_format': 'xlsx',
                         'financial_id': self.env.context.get('id'),
                         }
                }

    def _get_templates(self):
        templates = super(
            ValidityReport, self)._get_templates()
        templates[
            'main_table_header_template'] = 'account_reports.main_table_header'
        templates['main_template'] = 'account_reports.main_template'
        return templates

    def _get_columns_name(self, options):
        return [
            {'name': _('Código programático'), 'class':'text-justify'},
            {'name': _('PR'), 'class':'text-justify'},
            {'name': _('SP'), 'class':'text-justify'},
            {'name': _('DEP'), 'class':'text-justify'},
            {'name': _('SD'), 'class':'text-center'},
            {'name': _('PAR'), 'class':'text-center'},
            {'name': _('OR'), 'class':'text-center'},
            {'name': _('AI'), 'class':'text-center'},
            {'name': _('CONPP'), 'class':'text-center'},
            {'name': _('CONPA'), 'class':'text-center'},
            {'name': _('TG'), 'class':'text-center'},
            {'name': _('UG'), 'class':'text-center'},
            {'name': _('CC'), 'class':'text-center'},
        ]

    def _format(self, value,figure_type):
        if self.env.context.get('no_format'):
            return value
        value['no_format_name'] = value['name']
        
        if figure_type == 'float':
            currency_id = self.env.company.currency_id
            if currency_id.is_zero(value['name']):
                # don't print -0.0 in reports
                value['name'] = abs(value['name'])
                value['class'] = 'number text-muted'
            value['name'] = formatLang(self.env, value['name'], currency_obj=currency_id)
            value['class'] = 'number'
            return value
        if figure_type == 'percents':
            value['name'] = str(round(value['name'] * 100, 1)) + '%'
            value['class'] = 'number'
            return value
        value['name'] = round(value['name'], 1)
        return value['name']


    def _get_lines(self, options, line_id=None):
        lines = []
        year_date = datetime.now().year
        
        self.env.cr.execute(f'''select id, program_code, program_id, sub_program_id, dependency_id, sub_dependency_id, item_id, resource_origin_id, institutional_activity_id, 
                            budget_program_conversion_id, conversion_item_id, expense_type_id, location_id, portfolio_id 
                            from program_code where year = (select id from year_configuration where name = '{year_date}');''')

        program_data = self.env.cr.dictfetchall()

        #======= get the data count =========#
        selected_line_pages = options['selected_line_pages']
        selected_my_data = []

        for s in selected_line_pages:
            s = int(s)
            start = (s - 1) * 500
            end = s * 500

            selected_my_data = selected_my_data + program_data[start:end]

        if not selected_line_pages:
            selected_my_data = program_data[:500]
        if 'is_xlsx_report' in options and options['is_xlsx_report'] == True:
            selected_my_data = program_data

        for program_code in selected_my_data:
            #Se crean objetos de cada uno de los elementos del código programático
            program_id = self.env['program'].browse(program_code.get('program_id'))
            sub_program_id = self.env['sub.program'].browse(program_code.get('sub_program_id'))
            dependency_id = self.env['dependency'].browse(program_code.get('dependency_id'))
            sub_dependency_id = self.env['sub.dependency'].browse(program_code.get('sub_dependency_id'))
            item_id = self.env['expenditure.item'].browse(program_code.get('item_id'))
            resource_origin_id = self.env['resource.origin'].browse(program_code.get('resource_origin_id'))
            institutional_id = self.env['institutional.activity'].browse(program_code.get('institutional_activity_id'))
            budget_program_id = self.env['budget.program.conversion'].browse(program_code.get('budget_program_conversion_id'))
            conversion_item_id = self.env['departure.conversion'].browse(program_code.get('conversion_item_id'))
            expense_type_id = self.env['expense.type'].browse(program_code.get('expense_type_id'))
            location_id = self.env['geographic.location'].browse(program_code.get('location_id'))
            portfolio_id = self.env['key.wallet'].browse(program_code.get('portfolio_id'))
            
            #Variables para indicar la vigencia
            write_line = False
            actual_date = datetime.now().date()
            program_vigency = 'V'
            sub_program_vigency = 'V'
            dependency_vigency = 'V'
            sub_dependency_vigency = 'V'
            item_vigency = 'V'
            resource_origin_vigency = 'V'
            institutional_vigency = 'V'
            budget_program_vigency = 'V'
            conversion_item_vigency = 'V'
            expense_type_vigency = 'V'
            location_vigency = 'V'
            portfolio_vigency = 'V'


            #Validación del programa
            if program_id.end_date:
                if program_id.end_date<actual_date:
                    program_vigency = 'NV'
                    write_line = True

            #Validación del subprograma
            if sub_program_id.end_date:
                if sub_program_id.end_date<actual_date:
                    sub_program_vigency = 'NV'
                    write_line = True

            #Validación de la dependencia
            if dependency_id.end_date:
                if dependency_id.end_date<actual_date:
                    dependency_vigency = 'NV'
                    write_line = True

            #Validación de la subdependencia
            if sub_dependency_id.end_date:
                if sub_dependency_id.end_date<actual_date:
                    sub_dependency_vigency = 'NV'
                    write_line = True

            #Validación de la partida
            if item_id.end_date:
                if item_id.end_date<actual_date:
                    item_vigency = 'NV'
                    write_line = True

            #Validación del origen del recurso
            if resource_origin_id.end_date:
                if resource_origin_id.end_date<actual_date:
                    resource_origin_vigency = 'NV'
                    write_line = True

            #Validación de la actividad institucional
            if institutional_id.end_date:
                if institutional_id.end_date<actual_date:
                    institutional_vigency = 'NV'
                    write_line = True

            #Validación del CONPP
            if budget_program_id.end_date:
                if budget_program_id.end_date<actual_date:
                    budget_program_vigency = 'NV'
                    write_line = True

            #Validación del CONPA
            if conversion_item_id.end_date:
                if conversion_item_id.end_date<actual_date:
                    conversion_item_vigency = 'NV'
                    write_line = True

            #Validación del tipo de gasto
            if expense_type_id.end_date:
                if expense_type_id.end_date<actual_date:
                    expense_type_vigency = 'NV'
                    write_line = True

            #Validación de la UG
            if location_id.end_date:
                if location_id.end_date<actual_date:
                    location_vigency = 'NV'
                    write_line = True

            #Validación de la CC
            if portfolio_id.end_date:
                if portfolio_id.end_date<actual_date:
                    portfolio_vigency = 'NV'
                    write_line = True

            ### Se compone el arreglo de códigos que no están vigentes
            if write_line:
                lines.append({
                            'id': 'hierarchy_program_'+str(program_code.get('program_code')),
                            'name': program_code.get('program_code'),
                            'columns': [{'name': program_vigency},
                                        {'name': sub_program_vigency},
                                        {'name': dependency_vigency},
                                        {'name': sub_dependency_vigency},
                                        {'name': item_vigency},
                                        {'name': resource_origin_vigency},
                                        {'name': institutional_vigency},
                                        {'name': budget_program_vigency},
                                        {'name': conversion_item_vigency},
                                        {'name': expense_type_vigency},
                                        {'name': location_vigency},
                                        {'name': portfolio_vigency},
                                        ],
                            'level': 1,
                            'unfoldable': False,
                            'unfolded': False,
                            'class':'text-left'
                        })
                
        return lines

    def _get_report_name(self):
        return _("Reporte de vigencias")
    
    @api.model
    def _get_super_columns(self, options):
        date_cols = options.get('date') and [options['date']] or []
        date_cols += (options.get('comparison') or {}).get('periods', [])
        columns = reversed(date_cols)
        return {'columns': columns, 'x_offset': 1, 'merge': 5}
    
    def get_month_name(self,month):
        month_name = ''
        if month==1:
            month_name = 'Enero'
        elif month==2:
            month_name = 'Febrero'
        elif month==3:
            month_name = 'Marzo'
        elif month==4:
            month_name = 'Abril'
        elif month==5:
            month_name = 'Mayo'
        elif month==6:
            month_name = 'Junio'
        elif month==7:
            month_name = 'Julio'
        elif month==8:
            month_name = 'Agosto'
        elif month==9:
            month_name = 'Septiembre'
        elif month==10:
            month_name = 'Octubre'
        elif month==11:
            month_name = 'Noviembre'
        elif month==12:
            month_name = 'Diciembre'
            
        return month_name.lower()
    
    def get_header_year_list(self,options):
        start = datetime.strptime(
            str(options['date'].get('date_from')), '%Y-%m-%d').date()
        end = datetime.strptime(
            options['date'].get('date_to'), '%Y-%m-%d').date()
        
        str1 = ''
        if start.year == end.year:
            if start.month != end.month:
                str1 = self.get_month_name(start.month) + "-" +self.get_month_name(end.month) + " " + str(start.year)
            else:   
                str1 = self.get_month_name(start.month) + " " + str(start.year)
        else:
            str1 = self.get_month_name(start.month)+" "+ str(start.year) + "-" +self.get_month_name(end.month) + " " + str(end.year)
        return str1

    def is_float(self, num):
        try:
            float(num)
            return True
        except ValueError:
            return False

    def get_xlsx(self, options, response=None):
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        sheet = workbook.add_worksheet(self._get_report_name()[:31])
 
        date_default_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2, 'num_format': 'yyyy-mm-dd', 'text_wrap': True, 'valign': 'top'})
        date_default_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': 'yyyy-mm-dd', 'text_wrap': True, 'valign': 'top'})
        default_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2, 'text_wrap': True, 'valign': 'top'})
        default_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'text_wrap': True, 'valign': 'top'})
        title_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'bold': True, 'bottom': 2})
        super_col_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'bold': True, 'align': 'center', 'border': 2, 'valign': 'vcenter'})
        level_0_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'bottom': 6, 'font_color': '#666666', 'text_wrap': True, 'valign': 'top'})
        level_1_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'bottom': 1, 'font_color': '#666666', 'text_wrap': True, 'valign': 'top'})
        level_2_col1_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 1, 'text_wrap': True, 'valign': 'top'})
        level_2_col1_total_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'text_wrap': True, 'valign': 'top'})
        level_2_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'text_wrap': True, 'valign': 'top'})
        level_3_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2, 'text_wrap': True, 'valign': 'top'})
        level_3_col1_total_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 1, 'text_wrap': True, 'valign': 'top'})
        level_3_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'text_wrap': True, 'valign': 'top'})
        currect_date_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'align': 'right', 'text_wrap': True, 'valign': 'top'})
        currency_format = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 11, 'font_color': '#666666', 'num_format': '$#,##0.00'})

        sheet.set_column(0, 0, 60)
        sheet.set_column(1, 12,20)
        
        #Set the first column width to 50
#         sheet.set_column(0, 0,15)
#         sheet.set_column(0, 1,20)
#         sheet.set_column(0, 2,20)
#         sheet.set_column(0, 3,12)
#         sheet.set_column(0, 4,20)
        #sheet.set_row(0, 0,50)
        #sheet.col(0).width = 90 * 20
        super_columns = self._get_super_columns(options)
        #y_offset = bool(super_columns.get('columns')) and 1 or 0
        #sheet.write(y_offset, 0,'', title_style)
        y_offset = 0
        col = 0

        actual_date = datetime.today().strftime('%d/%m/%Y')
        
#         sheet.merge_range(y_offset, col, 6, col, '')
        if self.env.user and self.env.user.company_id and self.env.user.company_id.header_logo:
            filename = 'logo.png'
            image_data = io.BytesIO(base64.standard_b64decode(self.env.user.company_id.header_logo))
            sheet.insert_image(0,0, filename, {'image_data': image_data,'x_offset':8,'y_offset':3,'x_scale':0.63,'y_scale':0.63})

        header_title = '''DIRECCIÓN GENERAL DE PRESUPUESTO\nREPORTE DE VIGENCIAS\n\nFecha de consulta: %s'''%(actual_date)
        sheet.merge_range(y_offset, col, 5, col+12, header_title,super_col_style)
        y_offset += 6
#         col=1
#         currect_time_msg = "Fecha y hora de impresión: "
#         currect_time_msg += datetime.today().strftime('%d/%m/%Y %H:%M')
#         sheet.merge_range(y_offset, col, y_offset, col+17, currect_time_msg,currect_date_style)
#         y_offset += 1
        
        # Todo in master: Try to put this logic elsewhere
#         x = super_columns.get('x_offset', 0)
#         for super_col in super_columns.get('columns', []):
#             cell_content = super_col.get('string', '').replace('<br/>', ' ').replace('&nbsp;', ' ')
#             x_merge = super_columns.get('merge')
#             if x_merge and x_merge > 1:
#                 sheet.merge_range(0, x, 0, x + (x_merge - 1), cell_content, super_col_style)
#                 x += x_merge
#             else:
#                 sheet.write(0, x, cell_content, super_col_style)
#                 x += 1
        for row in self.get_header(options):
            x = 0
            for column in row:
                colspan = column.get('colspan', 1)
                header_label = column.get('name', '').replace('<br/>', ' ').replace('&nbsp;', ' ')
                if colspan == 1:
                    if x>4 and header_label!='Total':
                        sheet.write(y_offset, x, header_label, title_style)
                    else:
                        sheet.write(y_offset, x, header_label, title_style)
                else:
                    sheet.merge_range(y_offset, x, y_offset, x + colspan - 1, header_label, title_style)
                x += colspan
            y_offset += 1
        ctx = self._set_context(options)
        ctx.update({'no_format':True, 'print_mode':True, 'prefetch_fields': False})
        # deactivating the prefetching saves ~35% on get_lines running time
        lines = self.with_context(ctx)._get_lines(options)
 
        if options.get('hierarchy'):
            lines = self._create_hierarchy(lines, options)
        if options.get('selected_column'):
            lines = self._sort_lines(lines, options)
 
        #write all data rows
        for y in range(0, len(lines)):
            level = lines[y].get('level')
            if lines[y].get('caret_options'):
                style = level_3_style
                col1_style = level_3_col1_style
            elif level == 0:
                y_offset += 1
                style = level_0_style
                col1_style = style
            elif level == 1:
                style = level_1_style
                col1_style = style
            elif level == 2:
                style = level_2_style
                col1_style = 'total' in lines[y].get('class', '').split(' ') and level_2_col1_total_style or level_2_col1_style
            elif level == 3:
                style = level_3_style
                col1_style = 'total' in lines[y].get('class', '').split(' ') and level_3_col1_total_style or level_3_col1_style
            else:
                style = default_style
                col1_style = default_col1_style
 
            #write the first column, with a specific style to manage the indentation
            cell_type, cell_value = self._get_cell_type_value(lines[y])
            if cell_type == 'date':
                sheet.write_datetime(y + y_offset, 0, cell_value, date_default_col1_style)
            else:
                sheet.write(y + y_offset, 0, cell_value, col1_style)
 
            #write all the remaining cells
            for x in range(1, len(lines[y]['columns']) + 1):
                cell_type, cell_value = self._get_cell_type_value(lines[y]['columns'][x - 1])
                if cell_type == 'date':
                    sheet.write_datetime(y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, date_default_style)
                elif cell_type=='text' and self.is_float(cell_value) and x>4:
                    sheet.write(y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, currency_format)
                else:
                    sheet.write(y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, style)
 
        workbook.close()
        output.seek(0)
        generated_file = output.read()
        output.close()



        return generated_file    
        
    
    def get_pdf(self, options, minimal_layout=True):
        # As the assets are generated during the same transaction as the rendering of the
        # templates calling them, there is a scenario where the assets are unreachable: when
        # you make a request to read the assets while the transaction creating them is not done.
        # Indeed, when you make an asset request, the controller has to read the `ir.attachment`
        # table.
        # This scenario happens when you want to print a PDF report for the first time, as the
        # assets are not in cache and must be generated. To workaround this issue, we manually
        # commit the writes in the `ir.attachment` table. It is done thanks to a key in the context.
        minimal_layout = False

        actual_date = datetime.today().strftime('%d/%m/%Y')
        if not config['test_enable']:
            self = self.with_context(commit_assetsbundle=True)

        base_url = self.env['ir.config_parameter'].sudo().get_param('report.url') or self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        rcontext = {
            'mode': 'print',
            'base_url': base_url,
            'company': self.env.company,
        }

        body = self.env['ir.ui.view'].render_template(
            "account_reports.print_template",
            values=dict(rcontext),
        )
        body_html = self.with_context(print_mode=True).get_html(options)
        #logging.critical("BODY: "+str(body_html))
        body = body.replace(b'<body class="o_account_reports_body_print">', b'<body class="o_account_reports_body_print">' + body_html)
        
        if minimal_layout:
            header=''
            footer = self.env['ir.actions.report'].render_template("web.internal_layout", values=rcontext)
            spec_paperformat_args = {'data-report-margin-top': 10, 'data-report-header-spacing': 10}
            footer = self.env['ir.actions.report'].render_template("web.minimal_layout", values=dict(rcontext, subst=True, body=footer))
        else:
            rcontext.update({
                    'css': '',
                    'o': self.env.user,
                    'res_company': self.env.company,
                    'header_date_2': actual_date,
                })
#             header = self.env['ir.actions.report'].render_template("jt_income.pdf_external_layout_income_annual_report_new", values=rcontext)
#             
#             header = header.decode('utf-8') # Ensure that headers and footer are correctly encoded
#             current_filtered = self.get_header_year_list(options)
#             header.replace("REAL", "Test")
            image_data = io.BytesIO(base64.standard_b64decode(self.env.user.company_id.header_logo))
            '''header=<div class="header">
                        <div class="row">
                            <div class="col-12 text-center">
                                   <span style="font-size:16px;">DIRECCIÓN GENERAL DE FINANZAS</span><br/>
                                   <span style="font-size:14px;">COMISIONES BANCARIAS INSTITUCIONALES</span><br/>
                                   <span style="font-size:12px;">%s</span><br/>
                                   <span style="font-size:12px;">Fecha de consulta: %s</span><br/>
                            </div>
                        </div>
                    </div>
                %(header_date, actual_date)'''
            header = self.env['ir.actions.report'].render_template(
                "jt_budget_mgmt.validity_header_id", values=rcontext)
            header = header.decode('utf-8')

            headers = header.encode()
            '''footer = <div class="col-12 text-center">
                            <div class="footer">
                                <small>
                                    <span>Hoja  </span><span class="page"/> de <span class="topage"/>
                                </small>
                            </div>
                        </div>'''
            # parse header as new header contains header, body and footer
            footer = self.env['ir.actions.report'].render_template(
                "jt_budget_mgmt.validity_footer_id", values=rcontext)
            footer = footer.decode('utf-8')
            footers = footer.encode()
            try:
                root = lxml.html.fromstring(header)

                match_klass = "//div[contains(concat(' ', normalize-space(@class), ' '), ' {} ')]"

                for node in root.xpath(match_klass.format('header')):
                    headers = lxml.html.tostring(node)
                    headers = self.env['ir.actions.report'].render_template("web.minimal_layout", values=dict(rcontext, subst=True, body=headers))
                root = lxml.html.fromstring(footer)
                for node in root.xpath(match_klass.format('footer')):
                    footers = lxml.html.tostring(node)
                    footers = self.env['ir.actions.report'].render_template("web.minimal_layout", values=dict(rcontext, subst=True, body=footers))

            except lxml.etree.XMLSyntaxError:
                headers = header.encode()
                footers = footer.encode()
            header = headers
            footer = footers


        landscape = True
        if len(self.with_context(print_mode=True).get_header(options)[-1]) > 5:
            landscape = True

        reporte = self.env['ir.actions.report'].search([('model','=','validity.report')])

        return reporte._run_wkhtmltopdf(
            [body],
            header=header, footer=footer,
            landscape=landscape,
            specific_paperformat_args=None,
        )

    def get_html(self, options, line_id=None, additional_context=None):
        '''
        return the html value of report, or html value of unfolded line
        * if line_id is set, the template used will be the line_template
        otherwise it uses the main_template. Reason is for efficiency, when unfolding a line in the report
        we don't want to reload all lines, just get the one we unfolded.
        '''
        # Check the security before updating the context to make sure the options are safe.
        self._check_report_security(options)

        # Prevent inconsistency between options and context.
        self = self.with_context(self._set_context(options))

        templates = self._get_templates()
        report_manager = self._get_report_manager(options)
        report = {'name': self._get_report_name(),
                'summary': report_manager.summary,
                'company_name': self.env.company.name,}
        report = {}
        #options.get('date',{}).update({'string':''}) 
        lines = self._get_lines(options, line_id=line_id)

        if options.get('hierarchy'):
            lines = self._create_hierarchy(lines, options)
        if options.get('selected_column'):
            lines = self._sort_lines(lines, options)

        footnotes_to_render = []
        if self.env.context.get('print_mode', False):
            # we are in print mode, so compute footnote number and include them in lines values, otherwise, let the js compute the number correctly as
            # we don't know all the visible lines.
            footnotes = dict([(str(f.line), f) for f in report_manager.footnotes_ids])
            number = 0
            for line in lines:
                f = footnotes.get(str(line.get('id')))
                if f:
                    number += 1
                    line['footnote'] = str(number)
                    footnotes_to_render.append({'id': f.id, 'number': number, 'text': f.text})

        rcontext = {'report': report,
                    'lines': {'columns_header': self.get_header(options), 'lines': lines},
                    'options': {},
                    'context': self.env.context,
                    'model': self,
                }
        if additional_context and type(additional_context) == dict:
            rcontext.update(additional_context)
        if self.env.context.get('analytic_account_ids'):
            rcontext['options']['analytic_account_ids'] = [
                {'id': acc.id, 'name': acc.name} for acc in self.env.context['analytic_account_ids']
            ]

        render_template = templates.get('main_template', 'account_reports.main_template')
        if line_id is not None:
            render_template = templates.get('line_template', 'account_reports.line_template')
        html = self.env['ir.ui.view'].render_template(
            render_template,
            values=dict(rcontext),
        )
        if self.env.context.get('print_mode', False):
            for k,v in self._replace_class().items():
                html = html.replace(k, v)
            # append footnote as well
            html = html.replace(b'<div class="js_account_report_footnotes"></div>', self.get_html_footnotes(footnotes_to_render))
        return html