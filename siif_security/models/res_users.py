import logging
from odoo import models, fields, api
from lxml import etree


class ResPartner(models.Model):

    _inherit = 'res.users'

    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        res = super().fields_view_get(view_id=view_id,
                                      view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])

        if '.income' in res['name']:
            for node in doc.xpath("//" + view_type):
                node.set('export','0')
                #node.set('delete','0')
                
            actions_to_hide = [
				"Cambiar la contraseña",
				"Tareas asignadas",
			]
            if res.get('toolbar', {}).get('action'):
                action_items = filter(lambda x: x['display_name'] not in actions_to_hide, 
                res['toolbar']['action'])
                res['toolbar']['action'] = list(action_items)
         
            
        res['arch'] = etree.tostring(doc)

        return res
