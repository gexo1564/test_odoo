from odoo import models, fields, api,_
from odoo.exceptions import ValidationError

from datetime import datetime, timedelta

import logging
logger = logging.getLogger(__name__)

class AccountMove(models.Model):

    _inherit = 'account.move'
    payment_type = fields.Char(string='Payment Type')
    # Campos Globales para afectacion al presupuesto de ingresos
    cri = fields.Char(string='CRI', store="False")
    affectation_amount = fields.Float(string='Affectation Amount', store="False")

    #Función para el registro de las presupuestales de IEMN
    def presupuestales_iemn(self):
        
        res = self.invoice_line_ids.filtered(lambda x:x.egress_key_id.key in ('IEMN', 'IEME'))
        cri = False
        if not res:
            return
        for line in res:
            if not line.account_ie:
                raise ValidationError("La cuenta de ingresos extraordinarios es requerida.")
            account_id = line.account_ie.ie_account_line_ids.filtered(lambda x:x.main_account == True)
            if not account_id:
                raise ValidationError("La cuenta de ingresos extraordinarios no tiene configurada una cuenta principal.")
            account_ie = account_id.account_id
            if not account_ie.revenue_recognition_account_id:
                raise ValidationError("La cuenta de ingresos extraordinarios no tiene configurada una cuenta de reconocimiento de ingresos.")
            journal_id = self.journal_id
            if journal_id and (not journal_id.accrued_income_credit_account_id or not \
                journal_id.accrued_income_debit_account_id or not \
                journal_id.recover_income_credit_account_id or not \
                journal_id.recover_income_debit_account_id
            ):
                raise ValidationError("Configure las cuentas de reconocimiento de ingresos en el diario de la solicitud de pago")
            # Busqueda de CRI
            account_code = account_ie.revenue_recognition_account_id.code
            cri = self.env['affectation.income.budget'].search([]).associate_cri_account_code(account_code)
            self.line_ids = [
                # Pasivo Diferido
                (0, 0, {
                    # Cuentas 400
                    'account_id': account_ie.id,
                    'coa_conac_id': False,
                    'debit': line.balance,
                    'conac_move' : False,
                    'partner_id': self.partner_id.id,
                    'dependency_id': line.program_code_id.dependency_id.id if account_ie.dep_subdep_flag else False,
                    'sub_dependency_id': line.program_code_id.sub_dependency_id.id if account_ie.dep_subdep_flag else False,
                    'move_id': self.id,
                    'exclude_from_invoice_tab' : True,
                }),
                # Ingresos
                (0, 0, {
                    # Cuentas 400
                    'account_id': account_ie.revenue_recognition_account_id.id,
                    'coa_conac_id': False,
                    'credit': line.balance,
                    'conac_move' : False,
                    'partner_id': self.partner_id.id,
                    'dependency_id': line.program_code_id.dependency_id.id if account_ie.revenue_recognition_account_id.dep_subdep_flag else False,
                    'sub_dependency_id': line.program_code_id.sub_dependency_id.id if account_ie.revenue_recognition_account_id.dep_subdep_flag else False,
                    'move_id': self.id,
                    'exclude_from_invoice_tab' : True,
                    'cri' : cri.name if cri else False,
                }),
                # Cuentas de Ingresos
                (0, 0, {
                    # Cuentas 800
                    'account_id': journal_id.accrued_income_debit_account_id.id,
                    'coa_conac_id': journal_id.conac_accrued_income_debit_account_id and journal_id.conac_accrued_income_debit_account_id.id or False,
                    'debit': line.balance,
                    'conac_move' : False,
                    'partner_id': self.partner_id.id,
                    'dependency_id': line.program_code_id.dependency_id.id if journal_id.accrued_income_debit_account_id.dep_subdep_flag else False,
                    'sub_dependency_id': line.program_code_id.sub_dependency_id.id if journal_id.accrued_income_debit_account_id.dep_subdep_flag else False,
                    'move_id': self.id,
                    'exclude_from_invoice_tab' : True,
                    'cri' : cri.name if cri else False,
                }),
                (0, 0, {
                    # Cuentas 800
                    'account_id': journal_id.accrued_income_credit_account_id.id,
                    'coa_conac_id': journal_id.conac_accrued_income_credit_account_id and journal_id.conac_accrued_income_credit_account_id.id or False,
                    'credit': line.balance,
                    'conac_move' : False,
                    'partner_id': self.partner_id.id,
                    'dependency_id': line.program_code_id.dependency_id.id if journal_id.accrued_income_credit_account_id.dep_subdep_flag else False,
                    'sub_dependency_id': line.program_code_id.sub_dependency_id.id if journal_id.accrued_income_credit_account_id.dep_subdep_flag else False,
                    'move_id': self.id,
                    'exclude_from_invoice_tab' : True,
                    'cri' : cri.name if cri else False,
                }),
                (0, 0, {
                    # Cuentas 800
                    'account_id': journal_id.recover_income_debit_account_id.id,
                    'coa_conac_id': journal_id.conac_recover_income_debit_account_id and journal_id.conac_recover_income_debit_account_id.id or False,
                    'debit': line.balance,
                    'conac_move' : False,
                    'partner_id': self.partner_id.id,
                    'dependency_id': line.program_code_id.dependency_id.id if journal_id.recover_income_debit_account_id.dep_subdep_flag else False,
                    'sub_dependency_id': line.program_code_id.sub_dependency_id.id if journal_id.recover_income_debit_account_id.dep_subdep_flag else False,
                    'move_id': self.id,
                    'exclude_from_invoice_tab' : True,
                    'cri' : cri.name if cri else False,
                }),
                (0, 0, {
                    # Cuentas 800
                    'account_id': journal_id.recover_income_credit_account_id.id,
                    'coa_conac_id': journal_id.conac_recover_income_credit_account_id and journal_id.conac_recover_income_credit_account_id.id or False,
                    'credit': line.balance,
                    'conac_move' : False,
                    'partner_id': self.partner_id.id,
                    'dependency_id': line.program_code_id.dependency_id.id if journal_id.recover_income_credit_account_id.dep_subdep_flag else False,
                    'sub_dependency_id': line.program_code_id.sub_dependency_id.id if journal_id.recover_income_credit_account_id.dep_subdep_flag else False,
                    'move_id': self.id,
                    'exclude_from_invoice_tab' : True,
                    'cri' : cri.name if cri else False,
                }),
            ]
            if cri:
                date_affectation = self.date
                # Objetos
                obj_affetation = self.env['affectation.income.budget']
                obj_logbook = self.env['income.adequacies.function']
                obj_fiscal_year = self.env['account.fiscal.year']
                # Afectación al presupuesto
                type_affectation = 'eje-rec'
                date_accrued = False
                # Mandar datos a la clase de afectación
                amount = line.balance
                cri_id = cri.id
                
                obj_affetation.search([]).affectation_income_budget(type_affectation,cri_id,date_accrued,date_affectation,amount)
                # Guarda la bitacora de la adecuación
                # Busca el id del año fiscal
                year = date_affectation.year
                fiscal_year_id = obj_fiscal_year.search([('name', '=', year)])
                movement_type = 'income'
                movement = 'for_exercising_collected'
                origin_type = 'income-sp'
                movement_id = self.id
                journal_id = journal_id.id
                update_date_record = datetime.today()
                update_date_income = datetime.today()
                obj_logbook.search([]).logbook_income_budget(fiscal_year_id.id,cri_id,date_affectation,movement_type,movement,origin_type,movement_id,journal_id,update_date_record,amount,update_date_income) 

    @api.model
    def create(self, vals):
        res = super(AccountMove, self).create(vals)
        if res.dependancy_id and res.sub_dependancy_id:
            if res.sub_dependancy_id.dependency_id and res.sub_dependancy_id.dependency_id.id != res.dependancy_id.id:
                res.sub_dependancy_id = self.env['sub.dependency'].get_sub_dep_records(
                    res.sub_dependancy_id.sub_dependency, res.dependancy_id.id)

        # Registro Solicitudes de SIDIA
        upa_sidia = res.env['policy.keys'].search([('organization','=','SID')])
        if res.type == 'in_invoice' and res.upa_key.id == upa_sidia.id  and res.is_payment_request:
            res.update({'payment_state': 'approved_payment'})
        # else:
        #     # Se agrega Dependencia y Subdependencia a la cuenta por pagar
        #     if (res.type == 'in_invoice' and (not res.is_different_payroll_request and not res.is_pension_payment_request and not res.is_payroll_payment_request)):
        #         account_id = None
        #         # TODO Revisar si se modifica en el método _recompute_payment_terms_lines
        #         # if res.ministration_id:
        #         #     account_id = res.ministration_id.money_fund.debtor_account.id
        #         # else:
        #         res_id = 'res.partner,' + str(res.partner_id.id)
        #         cxp = res.env['ir.property'].sudo().search([('res_id', '=', res_id), ('name', '=', 'property_account_payable_id')])
        #         account_id = str(cxp.value_reference).split(',')[1]
        #         query = """
        #             select id from account_move_line
        #             where account_id = %s and move_id = %s
        #         """
        #         self.env.cr.execute(query, (account_id, res.id))
        #         resultado = [r[0] for r in self.env.cr.fetchall()]
        #         for line in self.env['account.move.line'].sudo().browse(resultado):
        #             line.update({
        #                 'dependency_id': vals['dependancy_id'],
        #                 'sub_dependency_id': vals['sub_dependancy_id'],
        #             })
        return res



    # Capitalización para solicitudes de pago
    def capitalizacion(self):
        uma = self.env['payment.parameters'].get_param('uma_capitalization')
        # Busca todas las líneas de egresos con codigo programático
        for line in self.invoice_line_ids.filtered(lambda x: x.program_code_id):
            if line.balance >= uma:

                # Buscamos la equivalencia que corresponde
                eq = self.env['siif_budget_mgmt_2.countability_equivalence'].search(
                    [('origin_resource', '=', line.program_code_id.resource_origin_id.id),
                     ('batch', '=', line.program_code_id.item_id.id)])
                if not eq:                
                    logger.info("\n/ / / / / / / / / / / / /  / / / / / /")
                    logger.info("No hay partida en el catálogo para hacer capitalización")
                    logger.info("\n/ / / / / / / / / / / / /  / / / / / /")
                else:
                    self.env['account.move.line'].with_context(check_move_validity=False).create({
                        'move_id': line.move_id.id,
                        'egress_key_id': line.egress_key_id.id,
                        'account_id': eq.account_fixed_asset.id,
                        'account_internal_type': 'other',
                        'exclude_from_invoice_tab': True,
                        'tax_exigible': False,
                        'dependency_id': line.dependency_id.id,
                        'sub_dependency_id': line.sub_dependency_id.id,
                        'quantity': 1,
                        'price_unit': line.balance,
                        'name': 'Capitalización',
                        'program_code_id': line.program_code_id.id
                    })

                    self.env['account.move.line'].with_context(check_move_validity=False).create({
                        'move_id': line.move_id.id,
                        'egress_key_id': line.egress_key_id.id,
                        'account_id': eq.account_heritage.id,
                        'account_internal_type': 'other',
                        'exclude_from_invoice_tab': True,
                        'tax_exigible': False,
                        'dependency_id': line.dependency_id.id,
                        'sub_dependency_id': line.sub_dependency_id.id,
                        'quantity': 1,
                        'price_unit': -line.balance,
                        'name': 'Capitalización',
                        'program_code_id': line.program_code_id.id
                    })

                    logger.info("Se realizó CAPITALIZACIÓN")

            else:
                logger.info("NO ES capitalizable")
                


    #Validación de suficiencia de IEMN
    def validate_iemn(self):
        check_iemn = False
        for line in self.invoice_line_ids.filtered(lambda x: x.egress_key_id.key in ('IEMN') and x.price_total > 0):
            self.env.cr.execute('''
            select
                coalesce(SUM(debit)) as debit,
                coalesce(SUM(credit)) as credit
            from
                account_move_line
            where
                account_id = %s
                and dependency_id = %s
                and sub_dependency_id = %s
                and parent_state = 'posted';
            ''', (line.account_id.id, line.dependency_id.id, line.sub_dependency_id.id))

            resultado = self.env.cr.dictfetchall()

            if (resultado[0]['credit'] is None or resultado[0]['debit'] is None):
                raise ValidationError(_(
                    "No hay saldo para la cuenta contable: %s  \n Dependencia: %s \n Subdependencia: %s") % (
                        line.account_id.code,
                        line.dependency_id.dependency,
                        line.sub_dependency_id.sub_dependency
                    )
                )

            debit = 0.0 if resultado[0]['debit'] is None else resultado[0]['debit']
            credit = 0.0 if resultado[0]['credit'] is None else resultado[0]['credit']

            saldo = credit - debit

            if (saldo >= line.price_total):
                check_iemn = False
            else:
                raise ValidationError(_(
                    "No hay suficiencia para IEMN \n Cuenta contable: %s  \n Dependencia: %s \n Subdependencia: %s \n\n Saldo de la cuenta: %s \n Monto egreso: %s") % (
                        line.account_id.code,
                        line.dependency_id.dependency,
                        line.sub_dependency_id.sub_dependency,
                        saldo,
                        line.price_total
                    )
                )

    def get_available_iemn(self, account_id, dependency_id, sub_dependency_id):
        self.env.cr.execute('''
            select
                coalesce(SUM(debit)) as debit,
                coalesce(SUM(credit)) as credit
            from
                account_move_line
            where
                account_id = %s
                and dependency_id = %s
                and sub_dependency_id = %s
                and parent_state = 'posted';
            ''', (account_id.id, dependency_id.id, sub_dependency_id.id)
        )
        res = self.env.cr.dictfetchall()
        debit = 0.0 if res[0]['debit'] is None else res[0]['debit']
        credit = 0.0 if res[0]['credit'] is None else res[0]['credit']
        return credit - debit

    def action_register(self):
        for move in self.filtered(lambda x: not x.is_different_payroll_request and not x.is_pension_payment_request):
            if not move.folio:
                move.generate_folio()
            if not move.commitment_date:
                today = datetime.today()
                current_date = today + timedelta(days=30)
                move.commitment_date = current_date
            if move.is_payment_request:
                line_pa = move.invoice_line_ids.filtered(lambda x: x.egress_key_id.key in ('PA'))
                if line_pa.filtered(lambda x: not x.program_code_id):
                    raise ValidationError("Hay egresos PA sin código programático")
        # Crea las líneas de ingresos extraordinarios
        # self.presupuestales_iemn()
        self.payment_state = 'registered'
        self.set_readonly_into_payment = True
        return True

    def _month_to_quarter(self, month):
        return [1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4][month-1]

    def _get_insufficiency_message(self, name, available, required):
        msg = ''
        if self.env.user.lang == 'es_MX':
            msg = 'Insuficiencia Presupuestal, {0} tiene un disponible de: ${1} y se requiere: ${2}\n\n'
        else:
            msg = 'Budgetary Insufficiency, {0} has an available: ${1} and it is required: ${2}\n\n'
        return msg.format(name, format(available, ",.2f"), format(required, ",.2f"))

    #Función para checar que los códigos presupuestales tienen suficiencia presupuestal
    def action_validate_budget(self):
            self.ensure_one()
            if self.is_payment_request or self.is_project_payment or self.is_payroll_payment_request:
                move = self.env['account.move']
                quarter = self._month_to_quarter(self.invoice_date.month)
                msgs = []
                lines_pa = []
                lines_ie = []
                lines_cc = []
                lines_cxp = []
                currency_id = self.currency_id
                currency_mx = self.env.user.company_id.currency_id
                for line in self.invoice_line_ids:
                    line_amount = abs(line.price_total)
                    # Conversión a moneda nacional
                    if currency_id != currency_mx:
                        line_amount = currency_id.compute(line_amount, currency_mx)
                    amount = round(line_amount, 2)
                    # Línea de Presupuesto Asignado
                    if line.egress_key_id.key == 'PA':
                        lines_pa.append((line.program_code_id, amount))
                    # Línea de Ingresos Extraordinarios
                    elif line.egress_key_id.key in ('IEMN', 'IEME'):
                        lines_ie.append((line.account_ie, line.program_code_id, amount))
                    # Línea de Ingresos Extraordinarios
                    elif line.egress_key_id.key == 'CC':
                        lines_cc.append((line.account_id, line.dependency_id, line.sub_dependency_id, amount))
                    # Línea de Cuenta por pagar
                    elif line.egress_key_id.key in ('CPN', 'CPE'):
                        lines_cxp.append((
                            self.previous_number,
                            line.program_code_id,
                            line.account_id,
                            line.account_ie,
                            line.dependency_id,
                            line.sub_dependency_id,
                            amount
                        ))
                # Validación de los disponibles de códigos de PA
                msgs += move.validate_get_available_program_code_pa(lines_pa, quarter)
                # Validación de los disponibles de códigos de IE
                msgs += move.validate_available_program_code_ie(lines_ie, quarter, self.invoice_date)
                # Validación de los disponibles de cuentas contables
                msgs += move.validate_available_account(lines_cc)
                # Validación de los disponibles de cuentas por pagar
                msgs += move.validate_available_account_to_pay(lines_cxp)

                return self._show_budget_message(msgs)

    def _show_budget_message(self, message_body):
        if message_body:
            sufficiency = False
            message_title = _('Budgetary Insufficiency')
        else:
            sufficiency = True
            message_title = message_body = _('Budgetary Sufficiency')
        return {
            'name': message_title,
            'type': 'ir.actions.act_window',
            'res_model': 'budget.insufficien.wiz',
            'view_mode': 'form',
            'view_type': 'form',
            'views': [(False, 'form')],
            'target': 'new',
            'context': {
                'default_msg': message_body,
                'default_move_id': self.id,
                'default_is_budget_suf': sufficiency
            }
        }

    #Obtener información del usuario
    def _get_partner_info_id(self,partner_id):
        return partner_id and partner_id.id or False
    #Obtener información del código programático
    def _get_program_code_id(self,program_code_id):
        return program_code_id and program_code_id.id or False
    #Obtener bandera si se permite DEP, SUB DEP
    def _check_dep_subdep_flag(self,dep_subdep,account_id):
        return dep_subdep.id if account_id.dep_subdep_flag else False

    def _check_credit_debit_account(self):
        if self.journal_id and (not self.journal_id.default_credit_account_id or not \
                self.journal_id.default_debit_account_id):
            raise ValidationError(_("Configure Default Debit and Credit Account in %s!" % \
                                    self.journal_id.name))
        
    def _compute_balance(self,amount_total):
        if self.currency_id != self.company_id.currency_id:
            amount_currency = abs(amount_total)
            balance = self.currency_id._convert(amount_currency, self.company_currency_id, self.company_id, self.date)
            currency_id = self.currency_id and self.currency_id.id or False
            # Si el tipo de moneda es diferente, se deben actualizar los débitos,
            # créditos y balances al nuevo tipo de cambio (ESTADO DE APROBACIÓN PARA PAGO)
            for line in self.line_ids:
                balance = self.currency_id._convert(line.price_total, self.company_currency_id, self.company_id, self.date)
                debit = abs(balance) if line.debit else 0.0
                credit = abs(balance) if line.credit else 0.0
                query = """
                    update account_move_line set balance = %s, debit = %s, credit = %s
                    where id = %s
                    returning 1
                """
                self.env.cr.execute(query, (balance, debit, credit, line.id))
                if not self.env.cr.fetchone():
                    raise ValidationError("Error al actualizar el tipo de cambio en los apuntes contables")
        else:
            balance = abs(amount_total)
            amount_currency = 0.0
            currency_id = False
        return currency_id,amount_currency,balance
        
    #Crea apuntes contables
    def create_journal_line_for_approved_payment(self):

        self._check_credit_debit_account()

        amount_total = sum(x.price_total for x in self.invoice_line_ids.filtered(lambda x: x.program_code_id))
        currency_id,amount_currency,balance=self._compute_balance(amount_total)
        balance = abs(round(balance, 2))
        if (self.is_payment_request or self.is_project_payment and (not self.is_different_payroll_request and not self.is_pension_payment_request and not self.is_payroll_payment_request)):
            # Creación de los asientos contables de IE
            
            self.presupuestales_iemn()
            for x in self.invoice_line_ids.filtered(lambda x: x.program_code_id and x.egress_key_id.key in ('PA', 'IEMN', 'IEME')):
                # COMPROMETIDO

                self.line_ids = [(0, 0, {
                    'account_id': self.journal_id.default_credit_account_id.id,
                    'coa_conac_id': self.journal_id.conac_credit_account_id.id,
                    'credit': x.balance,
                    'amount_currency': -amount_currency,
                    'exclude_from_invoice_tab': True,
                    'conac_move': True,
                    'currency_id': currency_id,
                    'partner_id': self._get_partner_info_id(self.partner_id),
                    'program_code_id': self._get_program_code_id(x.program_code_id),
                    #Dependencia y subdependencia - - -
                    'dependency_id': self._check_dep_subdep_flag(x.program_code_id.dependency_id,self.journal_id.default_credit_account_id),
                    'sub_dependency_id': self._check_dep_subdep_flag(x.program_code_id.sub_dependency_id,self.journal_id.default_credit_account_id),
                    'is_for_approved_payment': True,
                }),
                (0, 0, {
                    'account_id': self.journal_id.default_debit_account_id.id,
                    'coa_conac_id': self.journal_id.conac_debit_account_id.id,
                    'debit': x.balance,
                    'amount_currency': amount_currency,
                    'exclude_from_invoice_tab': True,
                    'conac_move': True,
                    'currency_id': currency_id,
                    'partner_id': self._get_partner_info_id(self.partner_id),
                    'program_code_id': self._get_program_code_id(x.program_code_id),
                    # Dependencia y subdependencia - - -
                    'dependency_id': self._check_dep_subdep_flag(x.program_code_id.dependency_id,self.journal_id.default_debit_account_id),
                    'sub_dependency_id': self._check_dep_subdep_flag(x.program_code_id.sub_dependency_id,self.journal_id.default_debit_account_id),
                    'is_for_approved_payment': True,
                })]
                # DEVENGADO
                
                self.line_ids = [(0, 0, {
                    'account_id': self.journal_id.accured_credit_account_id.id,
                    'coa_conac_id': self.journal_id.conac_accured_credit_account_id.id,
                    'credit': x.balance,
                    'amount_currency': -amount_currency,
                    'exclude_from_invoice_tab': True,
                    'conac_move': True,
                    'currency_id': currency_id,
                    'partner_id': self._get_partner_info_id(self.partner_id),
                    'program_code_id': self._get_program_code_id(x.program_code_id),
                    # Dependencia y subdependencia - - -
                    'dependency_id': self._check_dep_subdep_flag(x.program_code_id.dependency_id,self.journal_id.accured_credit_account_id),
                    'sub_dependency_id': self._check_dep_subdep_flag(x.program_code_id.sub_dependency_id,self.journal_id.accured_credit_account_id),
                    'is_for_approved_payment': True,
                }),
                (0, 0, {
                    'account_id': self.journal_id.accured_debit_account_id.id,
                    'coa_conac_id': self.journal_id.conac_accured_debit_account_id.id,
                    'debit': x.balance,
                    'amount_currency': amount_currency,
                    'exclude_from_invoice_tab': True,
                    'conac_move': True,
                    'currency_id': currency_id,
                    'partner_id': self._get_partner_info_id(self.partner_id),
                    'program_code_id': self._get_program_code_id(x.program_code_id),
                    # Dependencia y subdependencia - - -
                    'dependency_id': self._check_dep_subdep_flag(x.program_code_id.dependency_id,self.journal_id.accured_debit_account_id),
                    'sub_dependency_id': self._check_dep_subdep_flag(x.program_code_id.sub_dependency_id,self.journal_id.accured_debit_account_id),
                    'is_for_approved_payment': True,
                }),
                # EJERCIDO
                (0, 0, {
                    'account_id': self.journal_id.execercise_credit_account_id.id,
                    'coa_conac_id': self.journal_id.conac_exe_credit_account_id.id,
                    'credit': x.balance,
                    'amount_currency': -amount_currency,
                    'exclude_from_invoice_tab': True,
                    'conac_move': True,
                    'currency_id': currency_id,
                    'partner_id': self._get_partner_info_id(self.partner_id),
                    'program_code_id': self._get_program_code_id(x.program_code_id),
                    # Dependencia y subdependencia - - -
                    'dependency_id': self._check_dep_subdep_flag(x.program_code_id.dependency_id,self.journal_id.execercise_credit_account_id),
                    'sub_dependency_id': self._check_dep_subdep_flag(x.program_code_id.sub_dependency_id,self.journal_id.execercise_credit_account_id),
                    'is_for_approved_payment': True,
                }),
                (0, 0, {
                    'account_id': self.journal_id.execercise_debit_account_id.id,
                    'coa_conac_id': self.journal_id.conac_exe_debit_account_id.id,
                    'debit': x.balance,
                    'amount_currency': amount_currency,
                    'exclude_from_invoice_tab': True,
                    'conac_move': True,
                    'currency_id': currency_id,
                    'partner_id': self._get_partner_info_id(self.partner_id),
                    'program_code_id': self._get_program_code_id(x.program_code_id),
                    # Dependencia y subdependencia - - -
                    'dependency_id': self._check_dep_subdep_flag(x.program_code_id.dependency_id,self.journal_id.execercise_debit_account_id),
                    'sub_dependency_id': self._check_dep_subdep_flag(x.program_code_id.sub_dependency_id,self.journal_id.execercise_debit_account_id),
                    'is_for_approved_payment': True,
                })]
        self.conac_move = True

    def write(self,values):

        for value in self.line_ids:
            if value.debit == 0 and value.credit == 0:
                if self.env.user.lang == 'es_MX':
                    raise ValidationError('Al menos uno de los valores (debe/haber) debe ser mayor a 0.')
                else:
                    raise ValidationError('At least one of the values (debit/credit) must be greater than 0.')
        
        result = super(AccountMove,self).write(values)

        self.rewrite_logbook()
        return result

    def rewrite_logbook(self):
        for record in self:
            name = record.name
            if name != '/':
                id_loogbook = self.env['siif.budget.mgmt.accounting.movements.logbook'].search([('cve', '=', record.id)])
                for record in id_loogbook:
                    record.write({
                        'cve': name
                    })