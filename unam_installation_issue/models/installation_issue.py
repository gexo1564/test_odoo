from odoo import api, fields, models, _


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    program_code_id = fields.Many2one('program.code', index=True)


class ProgramCode(models.Model):
    _name = "program.code"
    _description = "to remove"

    resource_origin_id = fields.Many2one('resource.origin') # Key Origin resource
    desc_resource_origin = fields.Char(related="resource_origin_id.desc")

    item_id = fields.Many2one('expenditure.item')
    desc_item = fields.Text(related="item_id.description")

    dependency_id = fields.Many2one('dependency')
    desc_dependency = fields.Text(related="dependency_id.description")


class RessourceOrigin(models.Model):
    _name = 'resource.origin'
    _description = "to remove"

    desc = fields.Char()


class ExpenditureItem(models.Model):
    _name = 'expenditure.item'
    _description = "to remove"

    item = fields.Char()
    description = fields.Text()


class Dependency(models.Model):
    _name = 'dependency'
    _description = "to remove"

    description = fields.Text()
