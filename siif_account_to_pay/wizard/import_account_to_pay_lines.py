from odoo import models, fields, api, _
from odoo.exceptions import UserError
import base64
from xlrd import open_workbook
from datetime import datetime

IMPORT_ACCOUNT_TO_PAY_LINES = 'import.account.to.pay.lines'
PROGRAM_CODE = 'program.code'
class ImportAccountToPayLines(models.TransientModel):
    _name = IMPORT_ACCOUNT_TO_PAY_LINES
    _description = 'Import Account To Pay Lines'

    state = fields.Selection(
        [
            ('import', 'Import'),
            ('ok', 'Ok'),
            ('err', 'Ok'),
        ], default='import'
    )

    account_to_pay_id = fields.Many2one('account.topay', "Account to Pay")
    type_provision = fields.Many2one('account.topay.type', "Type of Provision")

    file = fields.Binary(string='File')
    filename = fields.Char(string='File name')
    file_errors = fields.Binary(string='File errors')
    file_errors_filename = fields.Char('File name', default="Registros_Erroneos.txt")

    lines_ok = fields.One2many('import.account.to.pay.lines.ok', 'import_account_to_pay_lines_id')
    lines_err = fields.One2many('import.account.to.pay.lines.err', 'import_account_to_pay_lines_id')

    @api.onchange('file')
    def onchange_file(self):
        self.state = 'import'
        self.lines_err = None
        self.lines_ok = None
        self.file_errors = None

    def open_wizard(self, res_id = None, **kwargs):
        return {
            'name': _('Importar Líneas de Cuentas por Pagar'),
            'type': 'ir.actions.act_window',
            'res_model': IMPORT_ACCOUNT_TO_PAY_LINES,
            'view_mode': 'form',
            'view_type': 'form',
            'views': [(False, 'form')],
            'context': kwargs,
            'target': 'new',
            'res_id': res_id,
        }

    def action_import_file(self):
        lines_ok, lines_err, msgs_err = [], [], []

        def set_line_error(sheet, row, msg):
            # Lineas de error
            lines_err.append((0, 0, {
                'resource_origin' : sheet.cell_value(row, 0),
                'program_code' : sheet.cell_value(row, 1),
                'account' : sheet.cell_value(row, 2),
                'account_ie' : sheet.cell_value(row, 3),
                'dependency' : sheet.cell_value(row, 4),
                'sub_dependency' : sheet.cell_value(row, 5),
                'amount' : sheet.cell_value(row, 6),
            }))
            # Mensaje de error
            line = ("[" + "'%s', "*6 + "%s]") % tuple([sheet.cell_value(row, i) for i in range(7)])
            msgs_err.append("%s ------->> %s" % (line, msg))

        if not self.file:
            raise UserError(_('Please Upload File.'))

        try:
            data = base64.decodestring(self.file)
            book = open_workbook(file_contents=data or b'')
        except UserError as e:
            raise UserError(e)

        sheet = book.sheet_by_index(0)

        res_obj = self.env['resource.origin'].search_read(
            [('key_origin', 'in', ('00', '01', '05'))],
            fields=['id', 'key_origin']
        )
        res_dic = {c['key_origin']: c['id'] for c in res_obj}

        for row in range(1, sheet.nrows):
            resource_origin = sheet.cell_value(row, 0)
            program_code = sheet.cell_value(row, 1)
            account = sheet.cell_value(row, 2)
            account_ie = sheet.cell_value(row, 3)
            dependency = sheet.cell_value(row, 4)
            sub_dependency = sheet.cell_value(row, 5)
            amount = sheet.cell_value(row, 6)

            line = {}
            # Obtención de los ids
            if resource_origin == '00':
                if len(program_code) != 60:
                    set_line_error(sheet, row, 'El código programático no cumple con la estructura.')
                    continue
                # Busca el código programático de PA
                program_code_id = self.env[PROGRAM_CODE].search([('program_code', '=', program_code)])
                if not program_code_id:
                    set_line_error(sheet, row, 'No se encontró el código programático.')
                    continue
                line.update({
                    'program_code_id': program_code_id.id,
                    'account_id': program_code_id.item_id.unam_account_id.id,
                    'dependency_id': program_code_id.dependency_id.id,
                    'sub_dependency_id': program_code_id.sub_dependency_id.id,
                })
            elif resource_origin == '01':
                if len(program_code) != 60:
                    set_line_error(sheet, row, 'El código programático no cumple con la estructura.')
                    continue
                # Busca o intenta crear si no existe el código programatico de IE
                program_code_id, err = self.env[PROGRAM_CODE].get_or_create_program_code_id(program_code)
                if err:
                    set_line_error(sheet, row, 'No fue posible crear el código programático, revise las reglas de presupuesto.')
                    continue
                program_code_id = self.env[PROGRAM_CODE].browse([program_code_id])
                line.update({
                    'program_code_id': program_code_id.id,
                    'account_id': program_code_id.item_id.unam_account_id.id,
                    'dependency_id': program_code_id.dependency_id.id,
                    'sub_dependency_id': program_code_id.sub_dependency_id.id,
                })
                # Buscar la cuenta conta de IE
                account_ie_id = self.env['association.distribution.ie.accounts'].search([
                    ('ie_key', '=', account_ie)
                ])
                if not account_ie_id:
                    set_line_error(sheet, row, 'No se encontró la cuenta de ingresos extraordinarios.')
                    continue
                line.update({'account_ie': account_ie_id.id})
            elif resource_origin == '05':
                # Buscar la cuenta contable
                account_id = self.env['account.account'].search([('code', '=', account)])
                if not account_id:
                    set_line_error(sheet, row, 'No se encontró la cuenta contable.')
                    continue
                line.update({'account_id': account_id.id})
                # Buscar la Dependencia
                dependency_id = self.env['dependency'].search([('dependency', '=', dependency)])
                if not dependency_id:
                    set_line_error(sheet, row, 'No se encontró la dependencia.')
                    continue
                line.update({'dependency_id': dependency_id.id})
                # Buscar la Subdependencia
                sub_dependency_id = self.env['sub.dependency'].search([
                    ('dependency_id', '=', dependency_id.id),
                    ('sub_dependency', '=', sub_dependency)
                ])
                if not sub_dependency_id:
                    set_line_error(sheet, row, 'No se encontró la sub dependencia.')
                    continue
                line.update({'sub_dependency_id': sub_dependency_id.id})
            else:
                set_line_error(sheet, row, 'Origen del recurso erroneo.')
                continue
            # Valida que el monto se pueda convertir a flotante
            try:
                line.update({'amount': float(amount)})
            except ValueError:
                set_line_error(sheet, row, 'Formato del monto en moneda origen es inválido.')
                continue
            # Asigna el id del origen del recurso
            line.update({'resource_origin_id': res_dic.get(resource_origin)})
            # Agrega la linea a la lista de lineas exitosas
            lines_ok.append((0, 0, line))
        if msgs_err:
            content = "................... Registros erróneos " + str(datetime.today()) + "...................\r\n"
            content += "\r\n".join(msgs_err)
            self.file_errors = base64.b64encode(content.encode('utf-8'))
            self.state = 'err'
        else:
            self.state = 'ok'
            self.file_errors = None
        self.lines_ok = None
        self.lines_ok = lines_ok
        self.lines_err = None
        self.lines_err = lines_err
        return self.open_wizard(res_id = self.id)

    def action_import_lines(self):
        lines = []
        for line in self.lines_ok:
            vals = {
                'origin_resource_sel': line.resource_origin_id.id,
                'program_code_id': line.program_code_id and line.program_code_id.id or False,
                'account_id': line.account_id and line.account_id.id or False,
                'account_ie': line.account_ie and line.account_ie.id or False,
                'dependency_id': line.dependency_id and line.dependency_id.id or False,
                'sub_dependency_id': line.sub_dependency_id and line.sub_dependency_id.id or False,
                'origin_money': line.amount,
            }
            lines.append((0, 0, vals))
        self.account_to_pay_id.invoice_line_ids = None
        self.account_to_pay_id.invoice_line_ids = lines
        

class ImportAccountToPayLinesOk(models.TransientModel):
    _name = 'import.account.to.pay.lines.ok'
    _description = 'Import Account To Pay Lines Ok'

    import_account_to_pay_lines_id = fields.Many2one(IMPORT_ACCOUNT_TO_PAY_LINES)

    resource_origin_id = fields.Many2one('resource.origin', 'Resource Origin')
    program_code_id = fields.Many2one(PROGRAM_CODE, 'Program Code')
    account_id = fields.Many2one('account.account', 'Account')
    account_ie = fields.Many2one('association.distribution.ie.accounts','Account IE')
    dependency_id = fields.Many2one('dependency', 'Dependency')
    sub_dependency_id = fields.Many2one('sub.dependency', 'Sub Dependency')
    amount = fields.Float('Amount')

class ImportAccountToPayLinesErr(models.TransientModel):
    _name = 'import.account.to.pay.lines.err'
    _description = 'Import Account To Pay Lines Error'

    import_account_to_pay_lines_id = fields.Many2one(IMPORT_ACCOUNT_TO_PAY_LINES)

    resource_origin = fields.Char('Resource Origin', size=2)
    program_code = fields.Char('Program Code', size=60)
    account = fields.Char('Account')
    account_ie = fields.Char('Account IE')
    dependency = fields.Char('Dependency')
    sub_dependency = fields.Char('Sub Dependency')
    amount = fields.Float('Amount')