from odoo import models, fields, api, _

class ResUsers(models.Model):

    _inherit = 'res.users'


    dependency_budget_ids = fields.Many2many('dependency', 'allowed_users_dep_budget_rel',
        string = "User Permissions for Dependencies")
    sub_dependency_budget_ids = fields.Many2many('sub.dependency', 'allowed_users_sd_budget_rel',
        string = "User Permissions for Sub Dependencies")
    all_dep_subdep_budget = fields.Boolean(string = 'All Dependencies selected')
    len_perm_assigned_budget = fields.Integer(compute = '_get_size_permissions_budget', string = "Permissions Assigned", store = False)

    program_budget_ids = fields.Many2many('program', 'allowed_users_program_budget_rel', string = "Programs")
    all_budget_programs_selected = fields.Boolean(string = 'All Programs selected')

    subprogram_budget_ids = fields.Many2many('sub.program', 'allowed_users_subprogram_budget_rel', string = "Subprograms")
    all_budget_subprograms_selected = fields.Boolean(string = 'All Subprograms selected')

    departure_group_budget_ids = fields.Many2many('departure.group', 'allowed_users_departure_group_budget_rel', string = "Departure groups")
    all_budget_departure_group_selected = fields.Boolean(string = 'All Departure groups selected')

    def _get_size_permissions_budget(self):
        for info in self:
            info.len_perm_assigned_budget = len(info.dependency_budget_ids) + len(info.sub_dependency_budget_ids)

    # Assing all dependencies
    @api.onchange('all_dep_subdep_budget')
    def _onchange_all_dep_subdep_budget(self):
        for user in self:
            if user.all_dep_subdep_budget:
                dependency_ids = self.env['dependency'].search([])
                user.dependency_budget_ids = [(6, 0, dependency_ids.ids)]
            else:
                user.dependency_budget_ids = [(5, )]

    # Assing all programs
    @api.onchange('all_budget_programs_selected')
    def _onchange_all_budget_programs_selected(self):
        for user in self:
            if self.all_budget_programs_selected:
                program_ids = self.env['program'].search([])
                user.program_budget_ids = [(6, 0, program_ids.ids)]
            else:
                user.program_budget_ids = [(5, )]

    # Assing all subprograms
    @api.onchange('all_budget_subprograms_selected')
    def _onchange_all_budget_subprograms_selected(self):
        for user in self:
            if user.all_budget_subprograms_selected:
                subprogram_ids = self.env['sub.program'].search([])
                user.subprogram_budget_ids = [(6, 0, subprogram_ids.ids)]
            else:
                user.subprogram_budget_ids = [(5, )]


    # Assing all departure groups
    @api.onchange('all_budget_departure_group_selected')
    def _onchange_all_budget_departure_group_selected(self):
        for user in self:
            if user.all_budget_departure_group_selected:
                departure_group_ids = self.env['departure.group'].search([])
                user.departure_group_budget_ids = [(6, 0, departure_group_ids.ids)]
            else:
                user.departure_group_budget_ids = [(5, )]
                                              

    def get_user_dependencies_subdependencies_domain_budget(self):
        user = self.env.user
        domain = [
            ('dependency_id', 'in', user.dependency_budget_ids.ids),
            ('sub_dependency_id', 'in', user.sub_dependency_budget_ids.ids)
        ]

        return domain
    

    def get_user_dependencies_domain_budget(self):
        user = self.env.user
        domain = [
            ('dependency_id', 'in', user.dependency_budget_ids.ids),
        
        ]

        return domain
    

    def get_user_subdependencies_domain_budget(self):
        user = self.env.user
        domain = [
            ('sub_dependency_id', 'in', user.sub_dependency_budget_ids.ids)
        ]

        return domain
    

    def get_user_programs_domain_budget(self):
        user = self.env.user
        domain = [
            ('program_id', 'in', user.program_budget_ids.ids),
        ]

        return domain


    def get_user_subprograms_domain_budget(self):
        user = self.env.user
        domain = [
            ('sub_program_id', 'in', user.subprogram_budget_ids.ids),
        ]

        return domain     