# -*- coding:utf-8 -*-

from odoo import models, fields, api, _

TYPE_BATCH = [
    ('capitalizable', 'Capitalizables'),
    ('control_economica', 'Económicas o consumibles')
]


class CountabilityEquivalence(models.Model):
    _name = 'siif_budget_mgmt_2.countability_equivalence'
    _description = "Intermediate table for account conversion"
    _rec_name = "batch"

    origin_resource = fields.Many2one(comodel_name='resource.origin', string="Clave de recurso")
    origin_resource_name = fields.Char(related='origin_resource.desc', string="Origen de recurso")
    batch = fields.Many2one(comodel_name="expenditure.item", string="Partida ID")
    type_batch = fields.Selection(TYPE_BATCH, default=TYPE_BATCH[0][0], string="Tipo de bien", tracking=True)
    account_fixed_asset = fields.Many2one(comodel_name="account.account", string="Cuenta activo fijo UNAM")
    account_heritage = fields.Many2one(comodel_name="account.account", string="Cuenta patrimonio")
    account_donation = fields.Many2one(comodel_name="account.account", string="Cuenta donación")
    account_donation_aptgo = fields.Many2one(comodel_name="account.account", string="Cuenta donación APTGO IE")

    _sql_constraints = [
        ('uniq_countability_equivalence_siif', 'unique(origin_resources,batch,type_batch)',
         'No se puede repetir origen de recurso/partida/tipo de bien'),
    ]


