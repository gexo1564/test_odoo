import base64
import copy
from datetime import datetime
import io
import locale
import re
import uuid
from collections import Counter

from odoo import models, fields, api, _
from odoo.modules.module import get_resource_path
from odoo.tools.misc import xlsxwriter
from odoo.tools import float_round

from .contants import TOTAL_LABEL, MIN_COLS, MAX_HEADER_COLS, NON_PERIOD_COLS, GENERAL_HEADER, COL_WIDTH_SMALL, COL_WIDTH_MEDIUM, COL_B_OFFSET, ANNEX_7

class AccountFinancialReportLine(models.Model):
    _inherit = "account.financial.html.report.line"
    _description = "Annexe - each line is an Annexe"

    annex_number = fields.Integer(copy=False)
    financial_situation_report_id = fields.Many2one('account.financial.situation.html.report', 'Financial Situation Report',
                                                    copy=False)
    account_group_ids = fields.Many2many('account.group')
    accounts_ids = fields.Many2many('account.account')
    table_ids = fields.Many2many('annex.table', 'annex_table_rel', 'annex_id', 'table_id')
    short_name = fields.Char(size=4)
    flip_sign = fields.Char(default=False)

    @api.onchange('account_group_ids')
    def _onchange_account_group_ids(self):
        for record in self:
            accounts = self.env['account.account'].search([('group_id', 'child_of', self.account_group_ids.ids)])
            record.accounts_ids = None
            record.accounts_ids = [(4, account.id, 0) for account in accounts]


class AccountFinancialSituation(models.Model):
    _name = "account.financial.situation.html.report"
    _inherit = "account.financial.html.report"
    _description = "Financial Situation report"

    line_ids = fields.One2many('account.financial.html.report.line', 'financial_situation_report_id', string='Lines')

    def get_header(self, options, is_global=False):
        header_options = options.copy()
        if options.get('for_xlsx'):
            if not options.get('add_evolutionary', False) and options.get('comparison'):
                header_options.pop('comparison')
        
        res = super().get_header(header_options)
        if options.get('for_xlsx'):
            new_res = [[
                {'name': 'Cuentas'},
            ]]
            if len(res[0]) == 2: # No comparison
                new_res[0].append(res[0][1]) # The only date period in res as res[0][0] contains an empty string
                new_res[0].append({'name': '--'})
            else:
                new_res[0].append(res[0][1])
                new_res[0].append(res[0][2])
            
            new_res[0].append({'name': '%'})
            new_res[0].append({'name': 'Evolutivo'})

            if not is_global:
                return new_res
            
        return res
    
    def _check_numeric(self, a):
        if type(a) == float or type(a) == int:
            return True
        return False

    def _get_line_evolutionary(self, lines, options):
        if len([options.get('date')] + (options.get('comparison')['periods'] or [])) == 2 and options.get(
                'add_evolutionary', False):
            for line in lines:
                if self._check_numeric(line['columns'][0]['name']) and self._check_numeric(line['columns'][1]['name']):
                    col1 = float(line['columns'][0]['name'].strip('$\xa0').replace(',', '')) if type(
                        line['columns'][0]['name']) == str else line['columns'][0]['name']
                    col2 = float(line['columns'][1]['name'].strip('$\xa0').replace(',', '')) if type(
                        line['columns'][1]['name']) == str else line['columns'][1]['name']
                    ev = abs(col1 - col2)
                    line['columns'].append({'name': ev})
        return lines

    def _get_lines(self, options, line_id, is_general=False):
        hidden_line = self.env.ref('unam_account_reports.account_financial_report_total_assets0_level1_2').id
        lines = super()._get_lines(options, line_id)

        if self.id == self.env.ref("unam_account_reports.account_financial_report_financial_situation").id:
            for line in lines:
                if line['id'] == hidden_line:
                    line['columns'] = [{'name' : '' } for _ in line['columns']]

        # For Annex 7, we need to check for the totals line that reflects balance = sum.balance
        if options.get('annex_code') == ANNEX_7 and len(lines) > 1:
            if lines[-1]['name'] == '':
                del lines[-1]
        elif options.get('add_annex') or options.get('subtract_annex') and len(lines) == 1:
            # Here we know we have the totals line where balance = IEO.balance + FID.balance + RGSPP.balance
            # We need to keep this for Annex 7
            if not options.get('is_total_line'):
                annex_id = options.get('add_annex') if options.get('add_annex') else options.get('subtract_annex')
                annex = self.env['account.financial.html.report.line'].browse([annex_id])
                lines[0]['name'] = annex.name
                lines[0]['keep_line'] = True
            else:
                lines[0]['name'] = 'Total'
        elif len(lines) == 1:
            lines[0]['name'] = 'Total'
        
        elif options.get('reuse_annex'):
            for line in lines[1:]:
                line['name'] = re.sub('\d{3}\.\d{3}\.\d{3}', '', line['name']).lstrip()
        
        self._hide_columns(line_id, lines, hidden_line)
        self._get_line_evolutionary(lines, options)
        return lines
    
    def _get_total_line_cols(self, cols):
        if len(cols) > 1:
            cols[2]['name'] = str((cols[0]['name'] - cols[1]['name']) / cols[1]['name']) + "%" if cols[1]['name'] > 0 else "n/a"

    def _hide_columns(self, line_id, lines, hidden_line):
        if line_id == hidden_line:
            for col in lines[0]['columns']:
                col['name'] = ''

    def _add_or_substract_line(self, line_id, lines, options, ctx, add_line, subtract_line):
        unfolded_lines = options.get('unfolded_lines')
        options['unfolded_lines'] = []
        options['unfold_all'] = False

        if line_id == self.env.ref('unam_account_reports.account_financial_report_total_assets0_level1_2').id:
            line = self.env.ref('unam_account_reports.account_financial_report_total_assets0_level1_2_0').id
            annex_total_line = self.with_context(ctx)._get_lines(options, line_id=line)
        else:
            annex_total_line = self.with_context(ctx)._get_lines(options, line_id=line_id)
            
        total_cols = annex_total_line[0]['columns']

        if add_line:
            lines.append({'name': _('ADD'), 'level': 0, 'columns': []})
            extra_line = self.with_context(ctx)._get_lines(options, line_id=add_line)
            lines.extend(extra_line)
            for index, col in enumerate(extra_line[0]['columns']):
                if type(col['name']) == float:
                    total_cols[index]['name'] += col['name']
        
        if subtract_line:
            lines.append({'name': _('SUBTRACT'), 'level': 0, 'columns': []})
            extra_line = self.with_context(ctx)._get_lines(options, line_id=subtract_line)
            lines.extend(extra_line)
            for index, col in enumerate(extra_line[0]['columns']):
                if type(col['name']) == float:
                    total_cols[index]['name'] -= col['name']

        self._get_total_line_cols(total_cols)
        total_line = {
            'name': 'Total', 
            'level': 2, 
            'class': '', 
            'columns': total_cols, 
            'unfoldable': True,
            'unfolded': False,
            'page_break': False}
        
        lines.append(total_line)
        
        options['unfolded_lines'] = unfolded_lines
        options['unfold_all'] = True

    def _get_report_manager(self, options):
        domain = [('report_name', '=', self._name)]
        domain = (domain + [('financial_report_id', '=', self.id)]) if 'id' in dir(self) else domain
        selected_companies = []
        if options.get('multi_company'):
            selected_companies = [c['id'] for c in options['multi_company'] if c.get('selected')]
        if len(selected_companies) == 1:
            domain += [('company_id', '=', selected_companies[0])]
        existing_manager = self.env['account.report.manager'].search(domain, limit=1)
        if not existing_manager:
            existing_manager = self.env['account.report.situation.manager'].search(domain, limit=1)
        if not existing_manager:
            existing_manager = self.env['account.report.situation.manager'].create({'report_name': self._name, 'company_id': selected_companies and selected_companies[0] or False, 'financial_report_id': self.id if 'id' in dir(self) else False})
        return existing_manager

    def write_generic_table_sheet(self, sheet, options, styles, line_id=False, line_values=False, add_line=None,
                                  subtract_line=None, reuse_lines=[], is_general=False):
        STARTING_ROW, STARTING_COL = 9, 1

        ctx = self._set_context(options)
        ctx.update({'no_format': True, 'print_mode': True,
                    'prefetch_fields': False})
        # deactivating the prefetching saves ~35% on get_lines running time
        lines = []
        if line_values and not line_id:
            lines = line_values
        elif not reuse_lines:
            unfolded_lines = options.get('unfolded_lines')
            if subtract_line:
                options['unfolded_lines'] = [line_id]
            lines = self._get_lines(options, line_id=line_id, is_general=is_general)
            if line_id == self.env.ref('unam_account_reports.account_financial_report_total_assets0_level1_2').id:
                options['unfolded_lines'] = []
                options['is_total_line'] = True
                lines.extend(self.with_context(ctx)._get_lines(options, line_id=self.env.ref('unam_account_reports.account_financial_report_total_assets0_level1_2_level2_4').id,))
                options.pop('is_total_line')
            if options.get('hierarchy'):
                lines = self.with_context(no_format=True)._create_hierarchy(lines, options)
            if options.get('selected_column'):
                lines = self._sort_lines(lines, options)
            options['unfolded_lines'] = unfolded_lines
            options['unfold_all'] = True

        if subtract_line or add_line:
            self._add_or_substract_line(line_id, lines, options, ctx, add_line, subtract_line)

        if reuse_lines:
            total_period_1 = 0
            total_period_2 = 0
            total_percent = 0
            total_evolutivo = 0
            has_second_period = False
            
            for line in reuse_lines:
                reused_lines = self.with_context(ctx)._get_lines(options, line_id=line)
                reused_lines.append({'name': '', 'columns': []})
                total_period_1 += reused_lines[0]['columns'][0]['name']
                
                if len(reused_lines[0]['columns']) > 1:
                    has_second_period = True
                    total_period_2 += reused_lines[0]['columns'][1]['name']
                    total_percent += float(reused_lines[0]['columns'][2]['name'].replace('%', ''))
                    total_evolutivo += reused_lines[0]['columns'][3]['name']

                lines.extend(reused_lines)
            
            if not has_second_period:
                lines.append({ 'name': 'Total', 'columns': [{ 'name': total_period_1 }] })
            
            else:
                lines.append({ 'name': 'Total', 'columns': [{ 'name': total_period_1 }, { 'name': total_period_2 }, { 'name': f'{total_percent}%' }, { 'name': total_evolutivo } ] })

        num_cols = len(lines[0]['columns']) if MIN_COLS < len(lines[0]['columns']) else MIN_COLS
        self.write_generic_header(sheet, styles, num_cols, options)
        self.write_specific_header(sheet, styles, num_cols, options)

        row, col = STARTING_ROW, STARTING_COL
        num_lines = len(lines)
        for line in lines:
            if re.search('^\d{1,2}\.\s{1}', line['name']):
                col += 1
                if line['level'] not in [0, 1] and (options.get('annex_code') != ANNEX_7 or line.get('keep_line', False) == True):
                    # Total row
                    total_row = row + num_lines - 1 if not reuse_lines and not line.get('keep_line') else row
                    if not reuse_lines or line.get('keep_line'):
                        if line.get('keep_line'):
                            number, name = line['name'].split('. ')
                            sheet.write(total_row, col - 1, number, styles.get('account_label_left'))
                            sheet.write(total_row, col, name, styles.get('account_label_left'))
                        else:
                            sheet.write(total_row, col, 'Total', styles.get('account_label_left'))
                    else:
                        name = line['name'].split('. ')[1]
                        sheet.write(total_row, col, name, styles.get('account_label_left'))
                    col += 1
                    if line['columns']:
                        if type(line['columns'][0]['name']) == float:
                            sheet.write(total_row, col, self.get_rounded_integer(line['columns'][0]['name']), styles.get('currency_bold'))
                    
                        col += 2
                        if len(line['columns']) > 1:
                            if type(line['columns'][1]['name']) == float:
                                sheet.write(total_row, col, self.get_rounded_integer(line['columns'][1]['name']), styles.get('currency_bold'))
                            col += 2
                            if line['columns'][2]['name'] == 'n/a':
                                sheet.write(total_row, col, '--', styles.get('account_label_centered'))
                            elif re.search('%', line['columns'][2]['name']):
                                sheet.write(total_row, col, self.get_rounded_percent(line['columns'][2]['name']), styles.get('account_label_right'))
                            if type(line['columns'][2]['name']) == float:
                                sheet.write(total_row, col, self.get_rounded_integer(line['columns'][2]['name']), styles.get('currency_bold'))
                            col += 1
                            if len(line['columns']) > 3 and type(line['columns'][3]['name']) == float:
                                sheet.write(total_row, col, self.get_rounded_integer(line['columns'][3]['name']), styles.get('currency_bold'))
                    col = STARTING_COL
                    if line.get('keep_line'):
                        row += 1
                    if not reuse_lines and not line.get('keep_line'):
                        continue

                elif line['level'] == 0 and len(line['columns']) > 1:
                    # Line columns contain text data, i.e. header
                    by_label = ''
                    if re.search('\s{1}-\s{1}', options.get('table_name')):
                        by_label = options.get('table_name').split(' - ')[1]
                    sheet.write(row, col, by_label, styles.get('account_label_left'))
                    col += 1
                    sheet.write(row, col, line['columns'][0]['name'], styles.get('account_label_centered'))
                    col += 2
                    sheet.write(row, col, line['columns'][1]['name'], styles.get('account_label_centered'))
                    col += 2
                    sheet.write(row, col, line['columns'][2]['name'], styles.get('account_label_centered'))
                    col += 1
                    sheet.write(row, col, line['columns'][3]['name'], styles.get('account_label_centered'))
                
                elif options['annex_code'] == ANNEX_7:
                    row -= 1 
            else:
                label_style = styles.get('account_label_left') if (line['name'] == 'Total' or not line['columns']) else styles.get('text_left')
                value_style = styles.get('currency_bold') if line['name'] == 'Total' else styles.get('currency')
                col += 1
                sheet.write(row, col, line['name'], label_style)
                col += 1
                # Columns list can contain a list of all empty strings, so need to check for falsy values for each column
                if line['columns']:
                    if type(line['columns'][0]['name']) == float:
                        sheet.write(row, col, self.get_rounded_integer(line['columns'][0]['name']), value_style)
                    col += 2
                    
                    if len(line['columns']) > 1:
                        if type(line['columns'][1]['name']) == float:
                            sheet.write(row, col, self.get_rounded_integer(line['columns'][1]['name']), value_style)
                        col += 2
                        
                        if len(line['columns']) > 2:
                            third_column = line['columns'][2]['name']
                            if third_column == 'n/a':
                                sheet.write(row, col, '--', styles.get('account_label_centered'))
                            
                            elif type(third_column) == str and re.search('%', line['columns'][2]['name']):
                                sheet.write(row, col, self.get_rounded_percent(line['columns'][2]['name']), value_style)
                            
                            elif type(third_column) == float:
                                sheet.write(row, col, self.get_rounded_integer(line['columns'][2]['name']), value_style)
                            col += 1
                            
                            if line['columns'][3]['name']:
                                sheet.write(row, col, self.get_rounded_integer(line['columns'][3]['name']), value_style)
                if re.search('Total', line['name']):
                    row += 1

            # Set positions
            row += 1
            col = STARTING_COL

    def write_specific_table_sheet(self, workbook, options, styles, annex, table, annexes):
        if table.type == 'reuse_annexes':
            annex_ids = annexes.filtered(lambda _annex: _annex.id in table.annex_ids.ids)
            sheet = workbook.add_worksheet("%s-%s" % (annex.short_name, table.short_name))
            self.write_generic_table_sheet(sheet, options, styles, line_id=False, reuse_lines=annex_ids.ids)
        
        elif table.type == 'add_annexes':
            sheet = workbook.add_worksheet(
                "%s-%s-%s" % (annex.short_name, table.short_name, table.added_annex_id.short_name))
            self.write_generic_table_sheet(sheet, options, styles, line_id=annex.id, add_line=table.added_annex_id.id)
        
        elif table.type == 'subtract_annexes':
            sheet = workbook.add_worksheet(
                "%s-%s-%s" % (annex.short_name, table.short_name, table.subtracted_annex_id.short_name))
            self.write_generic_table_sheet(sheet, options, styles, line_id=annex.id,
                                           subtract_line=table.subtracted_annex_id.id)
        
        else:
            self._write_specific_table(workbook, options, styles, annex, table)
    
    def _get_keys_array(self, value, origins):
        keys = list(value.keys()) 
        keys.extend([org for org in origins if org not in keys])
        return keys

    def _write_specific_table(self, workbook, options, styles, annex, table):
        # 1 groupby, we rely on standard groupby in html.report.line
        if table.groupby2 is None:
            groupby_report_line_id = annex.table_ids.filtered(
                lambda _config: _config == table).groupby_report_line_id
            self.write_generic_table_sheet(
                workbook.add_worksheet("%s - %s" % (annex.short_name, table.short_name)),
                options, styles, line_id=groupby_report_line_id.id)

        # 2 groupby, we query db
        comparison_table = [options.get('date')]
        
        res = {}
        columns = []
        origins = []
        i = 0
        column_excel_display_names = []

        if table.type not in ['expense_per_program_code_item', 'expense_per_dependency', 'expense_per_provider']:
            comparison_table += (options.get('comparison')['periods'] or [])
        else:
            origins = table.resource_origin_id.ids
        
        no_periods = len(comparison_table)
        for period in comparison_table:
            date_from = period.get('date_from', False)
            date_to = period.get('date_to', False) or period.get('date', False)
            date_from, date_to, strict_range = annex.with_context(date_from=date_from,
                                                                  date_to=date_to)._compute_date_range()
                                                                
            sql, where_params = self._build_annex_query(annex, table, date_from, date_to)
            aux_res, aux_columns = self._execute_query(sql, where_params, table)
            aux_columns.extend([org for org in origins if org not in aux_columns])

            if table.type == 'unam_account_group':
                for key, value in aux_res.items():
                    if not res.get(key):
                        res[key] = [{key: value} for _ in range(0, no_periods)]
                    res[key][i] = {key: value}
            elif not table.groupby1:
                for key, value in aux_res.items():
                    keys = self._get_keys_array(value, origins)
                    if not res.get(key):
                        res[key] = [{k: 0.0 for k in keys} for _ in range(0, no_periods)]
                    res[key][i] = value
            else:
                for section in aux_res:
                    if not res.get(section):
                        res[section] = {}
                    for key, value in aux_res[section].items():
                        keys = self._get_keys_array(value, origins)
                        if not res[section].get(key):
                            res[section][key] = [{k: 0.0 for k in keys} for _ in range(0, no_periods)]
                        res[section][key][i] = value
            
            names = self._get_excel_display_names(table.groupby3 if table.type != 'unam_account_group' else table.groupby2, aux_columns)
            if aux_columns:
                self._add_column_grand_totals(aux_columns, table)
            for col in aux_columns:
                if col not in columns:
                    columns.append(col)
                if not names.get(col, False):
                    names[col] = col
            column_excel_display_names.append(names)
            i += 1
        
        tab_sheets = []
        if table.groupby1:
            excel_display_names = self._get_excel_display_names(table.groupby1, list(res.keys()))
            for key, value in res.items():
                label = excel_display_names.get(key)
                options['groupby1_name'] = label
                options['skip_first_line'] = True
                section = '%s' % (label) if label else _('%s' % uuid.uuid1())
                tab_name = "%s-%s-%s" % (annex.short_name[:10], table.short_name[:10], section[:3])
                tab_sheets.append((tab_name, value,
                                {'name': section, 'level': 2,
                                    'columns': [{'name': ''} for _ in range(0, no_periods) for _ in columns ]}))
        else:
            tab_sheets.append((("%s-%s" % (annex.short_name, table.short_name)), res, None))

        
        for items in tab_sheets:
            row_values = [
                {'name': annex.display_name, 'level': 0,
                 'columns': [{'name': column_excel_display_names[p].get(column)} for p in range(0, no_periods) for
                             column in columns]}]
            sheet2 = workbook.add_worksheet(items[0])
            excel_display_names = self._get_excel_display_names(table.groupby2, list(items[1].keys()))
            if items[2]:
                row_values.append(items[2])
            values = items[1]
            self._add_row_values(values, annex, table, row_values, columns, excel_display_names, no_periods,
                                 add_evolutionary=options.get('add_evolutionary', False))
            self._add_row_grand_totals(table, values, row_values, columns, no_periods)
            
            if row_values[1]['columns'] and row_values[1]['columns'][0]['name'] == '': # Here with find the partida de gasto
                options['partida_de_gasto'] = row_values[1]['name']
                del row_values[1]
            
            self.write_generic_table_sheet(sheet2, options, styles, False, row_values)
            if options.get('partida_de_gasto'):
                options.pop('partida_de_gasto')
            if items[2]:
                row_values.pop()

    @api.model
    def _get_excel_display_names(self, field, ids):
        row_model = self.env['account.move.line']._fields[field].comodel_name
        others = False

        if 'others' in ids:
            ids.remove('others')
            others = True
            
        res =  {record.id: record.excel_display_name or record.id for record in
                self.env[row_model].search([('id', 'in', ids)])}
        
        if others:
            res['others'] = _('Otros')
        return res

    def _add_values(self, row_values, name, level, values, columns, table, row_labels=None, no_periods=1,
                    add_evolutionary=False, flip_sign=False):
        label = row_labels.get(name, '') if row_labels and name else name

        if table.type == 'unam_account_group':
            new_rows = {'name': label, 'level': level,
                    'columns': [{'name': - values[p][key] if flip_sign else values[p][key] for key in values[p].keys()} for p in range(0, no_periods)]}
            if no_periods == 2 and add_evolutionary:
                new_rows['columns'].extend(
                    [{'name': (values[0][key] - values[1][key]) / values[1][key] if values[1][key] != 0 else 'n/a'} for key in values[0].keys()])
                new_rows['columns'].extend(
                    [{'name': values[0][key] - values[1][key]} for key in values[0].keys()])
        else:
            new_rows = {'name': label, 'level': level,
                        'columns': [{'name': - values[p].get(column, 0.0) if flip_sign else values[p].get(column, 0.0)} for p in range(0, no_periods) for column in
                                    columns]}
        if no_periods == 2 and add_evolutionary:
            new_rows['columns'].extend(
                [{'name': values[0].get(col, False) - values[1].get(col, False)} for col in columns])

        row_values.append(new_rows)

    def get_aml_specific_domain(self, annex, table):
        if table.type in ['expense_per_program_code_item', 'expense_per_dependency',
                         'expense_per_provider', 'unam_account_group']:
            return table.get_specific_domain()
        
        return annex._get_aml_domain() + table.get_specific_domain()

    def _build_annex_query(self, annex, table, date_from, date_to):
        aml_obj = self.env['account.move.line']
        
        tables, where_clause, where_params = aml_obj._query_get(
            domain=self.get_aml_specific_domain(annex, table) + [('date', '>=', date_from), ('date', '<=', date_to)])

        select_query = ''
        group_query = ''

        if table.groupby1:
            select_query += '%s.%s as %s, ' % ('account_move_line', table.groupby1, table.groupby1)
            group_query += '%s.%s, ' % ('account_move_line', table.groupby1)
        if table.groupby2:
            select_query += '%s.%s as %s ' % ('account_move_line', table.groupby2, table.groupby2)
            group_query += '%s.%s ' % ('account_move_line', table.groupby2)
        if table.groupby3:
            select_query += ', %s.%s as %s ' % ('account_move_line', table.groupby3, table.groupby3)
            group_query += ', %s.%s' % ('account_move_line', table.groupby3)

        group_sql = "GROUP BY %s" % group_query if group_query else ''

        select, select_params = annex._query_get_select_sum(self._get_currency_table())
        where_params = select_params + where_params

        sql = "SELECT %s, %s FROM %s WHERE %s %s;" % (select_query, select, tables, where_clause, group_sql)
        return sql, where_params

    @api.model
    def _execute_query(self, sql, where_params, table):
        self.env.cr.execute(sql, where_params)
        res = {}
        columns = []
        rows = self.env.cr.dictfetchall()
        section = {}

        for row in rows:
            if table.type == 'unam_account_group':
                if row[table.groupby2] not in section:
                    section[row[table.groupby2]] = row['balance']
                else:
                    section[row[table.groupby2]] += row['balance']
            else:
                if row[table.groupby2] not in section:
                    section[row[table.groupby2]] = {}
                if row[table.groupby3] not in section[row[table.groupby2]]:
                    section[row[table.groupby2]][row[table.groupby3]] = row['balance']
                    if row[table.groupby3] not in columns:
                        columns.append(row[table.groupby3])
                else:
                    section[row[table.groupby2]][row[table.groupby3]] += row['balance']

        if table.groupby1:
            for row in rows:
                if row[table.groupby1] not in res:
                    res[row[table.groupby1]] = {}
                if row[table.groupby2] not in res[row[table.groupby1]]:
                    res[row[table.groupby1]][row[table.groupby2]] = {}
                if row[table.groupby3] not in res[row[table.groupby1]][row[table.groupby2]]:
                    res[row[table.groupby1]][row[table.groupby2]][row[table.groupby3]] = row['balance']
                else:
                    res[row[table.groupby1]][row[table.groupby2]][row[table.groupby3]] += row['balance']
        else:
            res = section

        if table.type in ['expense_per_dependency', 'expense_per_provider'] and table.show_first_ten_only:
            if table.groupby1:
                for key, values in res.items():
                    res[key] = self._get_biggest_ten(values, columns)
            else:
                res = self._get_biggest_ten(res, columns)

        return res, columns

    def _get_biggest_ten(self, res, columns):
        "Sort in descending order by sum of values"
        sorted_items = sorted(res.items(), key=lambda x: sum(x[1].values()), reverse=True)

        "Sum all other lines in one"
        last_line = dict(sum([Counter(i) for i in dict(sorted_items[10:]).values()], Counter()))

        new_res = dict(sorted_items[:10])
        new_res['others'] = last_line

        return new_res

    def _add_row_values(self, res, annex, table, row_values, columns, row_labels=None, no_periods=1, add_evolutionary=False): 
        if table.type == 'unam_account_group':
            for unam_account_group in table.unam_account_group_ids:
                new_res = {}
                row_values.append(
                    {'name': unam_account_group.name, 'level': 2,
                     'columns': [{'name': ''} for p in range(0, no_periods) for column in columns]})
                for key, value in res.items():
                    if key in unam_account_group.accounts_ids.ids:
                        new_res[key] = value
                self._write_row_values(new_res, row_values, row_labels, columns, table, no_periods, add_evolutionary, flip_sign=annex.flip_sign)
                self._add_row_grand_totals(table, new_res, row_values, columns, no_periods)
        else:
            self._write_row_values(res, row_values, row_labels, columns, table, no_periods)

    def _write_row_values(self, res, row_values, row_labels, columns, table, no_periods=1, add_evolutionary=False, flip_sign=False):
        for key, values in res.items():
            if table.has_row_grand_total:
                for val in values:
                    val[TOTAL_LABEL] = sum(val.values())
            self._add_values(row_values, key, 3, values, columns, table, row_labels, no_periods, add_evolutionary, flip_sign)

    def _add_column_grand_totals(self, columns, table):
        columns += [TOTAL_LABEL] if table.has_row_grand_total else []

    def _add_row_grand_totals(self, table, res, row_values, columns, no_periods, label=TOTAL_LABEL):
        if table.has_column_grand_total:
            grand_totals = []
            group_total = 0.0
            if table.type == 'unam_account_group':
                grand_totals = [{'name': 0.0} for _ in range(0, no_periods)]
            for row in res.values():
                if columns:
                    for period in range(0, len(row)):
                        grand_totals.append({})
                        for key, value in row[period].items():
                            if key not in grand_totals[period]:
                                grand_totals[period][key] = value
                            else:
                                grand_totals[period][key] += value
                elif table.type == 'unam_account_group':
                    for period in range(0, len(row)):
                        for key, value in row[period].items():
                            grand_totals[period]['name'] += value
            if grand_totals:
                self._add_values(row_values, label, 0, grand_totals, columns, table, no_periods=no_periods)

    def get_styles(self, workbook):
        UNAM_BLUE = '#1f497d'
        FONT = 'Arial'
        FONT_SIZE_REG  = 12
        FONT_SIZE_MED = 14
        FONT_SIZE_LG = 16
        base_styles = {
            'font': FONT,
            'font_size': FONT_SIZE_REG,
            'align': 'right',
        }
        generic_header_styles = copy.deepcopy(base_styles)
        generic_header_styles.update({
            'bg_color': UNAM_BLUE,
            'font_color': 'white',
            'bold': True,
        })
        
        styles = {
            'header_text_big': copy.deepcopy(generic_header_styles),
            'header_text_medium': copy.deepcopy(generic_header_styles),
            'table_header_medium': copy.deepcopy(generic_header_styles),
            'table_header_reg': copy.deepcopy(generic_header_styles),
            'table_header_right': copy.deepcopy(generic_header_styles),
            'table_header_left': copy.deepcopy(generic_header_styles),
            'blue_row': { 'bg_color': UNAM_BLUE },
            'account_label_left': copy.deepcopy(base_styles),
            'account_label_centered': copy.deepcopy(base_styles),
            'account_label_right': copy.deepcopy(base_styles),
            'inline_totals': copy.deepcopy(base_styles),
            'text_right': base_styles,
            'text_left': copy.deepcopy(base_styles),
            'currency': copy.deepcopy(base_styles),
            'currency_bold': copy.deepcopy(base_styles)
        }
        
        styles['header_text_big'].update({
                'font_size': FONT_SIZE_LG,
                'italic': True,
                'align': 'center',
                'valign': 'vcenter',
            })
        styles['header_text_medium'].update({
                'font_size': FONT_SIZE_MED,
                'italic': True,
                'align': 'center',
                'valign': 'vcenter',
            })
        styles['table_header_medium'].update({
                'font_size': FONT_SIZE_MED,
                'align': 'center',
                'valign': 'bottom',
            })
        styles['table_header_reg'].update({
                'font_size': FONT_SIZE_REG,
                'align': 'center',
                'valign': 'bottom',
                'text_wrap': True
            })
        styles['table_header_right'].update({
                'font_size': FONT_SIZE_REG,
                'align': 'right',
                'valign': 'bottom',
                'text_wrap': True,
                'num_format': '#,##0',
            })
        styles['table_header_left'].update({
            'bg_color': UNAM_BLUE,
            'font_color': 'white',
            'align': 'left',
            'bold': True,
        })
        styles['account_label_left'].update({ 'align': 'left', 'bold': True })
        styles['account_label_centered'].update({ 'align': 'center', 'bold': True })
        styles['account_label_right'].update({ 'align': 'right', 'bold': True })
        styles['inline_totals'].update({ 'top': 2, 'num_format': '#,##0' })
        styles['text_left'].update({ 'align': 'left' })
        styles['currency'].update({ 'num_format': '#,##0' })
        styles['currency_bold'].update({ 'num_format': '#,##0', 'bold': True })

        return { style: workbook.add_format(values) for style, values in styles.items() }

    
    def write_generic_header(self, sheet, styles, num_cols, options):
        # Call to _get_header returns a blank column for the first column
        columns = MIN_COLS if MIN_COLS > num_cols else num_cols
        sheet.set_row(0, 35)
        for row in range(GENERAL_HEADER):
            for col in range(columns):
                sheet.write_blank(row, col, '', styles.get('blue_row', None))
        
        image_path = get_resource_path('unam_account_reports', 'static', 'src', 'img', 'unam.png')
        sheet.insert_image('A1', image_path, {'x_scale': 0.75, 'y_scale': 0.75, 'x_offset': 10, 'y_offset': 10})

        # Account column
        sheet.set_column(2, 2, 50)

        # Small Columns
        sheet.set_column(1, 1, COL_WIDTH_SMALL)
        sheet.set_column(4, 4, COL_WIDTH_SMALL)
        sheet.set_column(6, 6, COL_WIDTH_SMALL)
        sheet.set_column(10, 10, COL_WIDTH_SMALL)
        
        # Value Columns
        sheet.set_column(3, 3, COL_WIDTH_MEDIUM)
        sheet.set_column(5, 5, COL_WIDTH_MEDIUM)
        sheet.set_column(7, 8, COL_WIDTH_MEDIUM)
        
        sheet.merge_range(0, 0, 0, num_cols - 1, 'UNIVERSIDAD NACIONAL AUTÓNOMA DE MÉXICO', styles.get('header_text_big'))
        sheet.merge_range(1, 0, 1, num_cols - 1, 'ESTADOS DE SITUACIÓN FINANCIERA', styles.get('header_text_big'))
        header_vals = self.get_header(options)[0]
        if len(header_vals) >= 5:
            if re.match('^De', header_vals[2]['name']):
                # Custom dates selected for comparison
                periods = header_vals[2]['name'].replace('\na', '').replace('De', ''). split(' ')
            else:
                periods = [header_vals[1]['name'], header_vals[2]['name']]

        elif re.search('^\d+\.\s{1}', header_vals[0]['name']):
            # Label for annex specific table
            title_row = 6
            number_and_name = header_vals[0]['name'].split('. ')
            sheet.merge_range(title_row, 1, title_row, num_cols - 1, number_and_name[1], styles.get('header_text_big'))
            periods = [fields.Date.today().year]

        else:
            # No additional periods selected for comparison
            periods = [header_vals[1]]
        
        if periods and len(periods) == 2:
            if re.search('[\/]', periods[0]):
                # Periods format comes from custom selection
                period1 = datetime.strptime(periods[0], "%d/%m/%Y").strftime("%w de %B %Y").title().replace('De', 'de')
                period2 = datetime.strptime(periods[1], "%d/%m/%Y").strftime("%w de %B %Y").title().replace('De', 'de')
            else:
                period1, period2 = periods[0], periods[1]
            if periods[1] != '--':
                sheet.merge_range(2, 0, 2, num_cols - 1, f'{period1} y {period2}', styles.get('header_text_big'))
            else:
                sheet.merge_range(2, 0, 2, num_cols - 1, f'{period1}', styles.get('header_text_big'))
        
        elif periods:
            if type(periods[0]) == str:
                period = periods[0]
            else:
                period = periods[0]['name']
            sheet.merge_range(2, 0, 2, num_cols - 1, f'{period}', styles.get('header_text_big'))
        
        sheet.merge_range(3, 0, 3, num_cols - 1, '(Miles de Pesos)', styles.get('header_text_big'))
        
    def write_specific_header(self, sheet, styles, num_cols, options, is_global=False):
        header_row, header_col = 7, 2
        header_vals = self.get_header(options)
        
        if is_global:
            sheet.set_column(11, 11, 50)
            sheet.set_column(12, 12, COL_WIDTH_MEDIUM)
            sheet.set_column(13, 13, COL_WIDTH_SMALL)
            sheet.set_column(14, 14, COL_WIDTH_MEDIUM)
            sheet.set_column(15, 15, COL_WIDTH_SMALL)
            sheet.set_column(16, 17, COL_WIDTH_MEDIUM)

            sheet.merge_range(5, 0, 5, 8, 'ACTIVO', styles.get('header_text_big'))
            sheet.merge_range(5, 9, 5, 17, 'PATRIMONIO, CRÉDITOS DIFERIDOS Y PASIVO', styles.get('header_text_big'))

            column_labels = header_vals[0][1:]
            num_periods = min(MAX_HEADER_COLS, len(column_labels))

            sheet.write(header_row, header_col, 'ANEXO', styles.get('header_text_big'))
            sheet.write(header_row, header_col + COL_B_OFFSET, 'ANEXO', styles.get('header_text_big'))
            header_col += 1
            for index in range(num_periods):
                # Here we are limiting to two columns in the periods available to report
                label = column_labels[index]
                sheet.write(header_row, header_col, label['name'], styles.get('header_text_big'))
                sheet.write(header_row, header_col + COL_B_OFFSET, label['name'], styles.get('header_text_big'))
                header_col += 2

                if num_periods < MAX_HEADER_COLS:
                    sheet.write(header_row, header_col, '--', styles.get('header_text_big'))
                    sheet.write(header_row, header_col + COL_B_OFFSET, '--', styles.get('header_text_big'))
                    header_col += 2
            
            sheet.write(header_row, header_col, 'VARIACIÓN IMPORTE', styles.get('header_text_big'))
            sheet.write(header_row, header_col + COL_B_OFFSET, 'VARIACIÓN IMPORTE', styles.get('header_text_big'))
            header_col += 1
            
            sheet.write(header_row, header_col, '%', styles.get('header_text_big'))
            sheet.write(header_row, header_col + COL_B_OFFSET, '%', styles.get('header_text_big'))
        
        else:
            column_labels = header_vals[0][1:] if header_vals[0][0]['name'] == '' else header_vals[0]

            # Get values from Options for Annex/Specific table
            annex_name = options.get('annex_name')
            sheet.merge_range(4, 0, 4, num_cols - 1, annex_name, styles.get('header_text_big'))

            if options.get('table_name'): # Specific Table
                sheet.merge_range(5, 0, 5, num_cols - 1, options.get('table_name'), styles.get('header_text_big'))
                if options.get('partida_de_gasto'):
                    sheet.merge_range(6, 0, 6, num_cols - 1, options['partida_de_gasto'], styles.get('header_text_big'))

            # Add the column labels
            if options.get('add_column_labels'):
                sheet.write(header_row, header_col, column_labels[0]['name'], styles.get('header_text_big'))
                header_col += 1
                sheet.write(header_row, header_col, column_labels[1]['name'], styles.get('header_text_big'))
                header_col += 2
                sheet.write(header_row, header_col, column_labels[2]['name'], styles.get('header_text_big'))
                header_col += 2
                sheet.write(header_row, header_col, column_labels[3]['name'], styles.get('header_text_big'))
                header_col += 1
                sheet.write(header_row, header_col, column_labels[4]['name'], styles.get('header_text_big'))
    
    def get_rounded_percent(self, percent):
        res = percent.replace('%', '')
        rounded_val = self.get_rounded_integer(float(res), is_percent=True)
        return f'{rounded_val}%'

    def get_rounded_integer(self, number, is_percent=False):
        if is_percent:
            new_val = float_round(number, precision_digits=0)
        else:
            new_val = float_round(number / 1000, precision_digits=0)
        
        if new_val < 0:
            return f"({abs(int(new_val))})"
        return int(new_val)

    def write_global_report(self, sheet, styles, options):
        lines = self._get_lines(options, False, True)
        table_width_cols = (len(lines[0]['columns']) + NON_PERIOD_COLS) * 2 # 2-column layout
        num_cols = table_width_cols if MIN_COLS < table_width_cols else MIN_COLS

        self.write_generic_header(sheet, styles, num_cols, options)
        self.write_specific_header(sheet, styles, num_cols, options, is_global=True)    

        # Report Body (tables)
        STARTING_ROW, STARTING_COL_A, STARTING_COL_B = 9, 1, 10
        FINAL_ROW_SIDE_A = 33
        current_row, current_col = STARTING_ROW, STARTING_COL_A
        side = 0
        section_end_indeces_a = [8, 9, 14]
        section_end_indeces_b = [29, 31]
        major_sections = ['ACTIVO', 'PATRIMONIO, CRÉDITOS DIFERIDOS Y PASIVO']

        for index, line in enumerate(lines):
            if current_row == FINAL_ROW_SIDE_A:
                current_row = STARTING_ROW
                current_col = STARTING_COL_B
                side = 1
            
            if line['level'] == 1 and not re.match('^\d', line['name']):
                sheet.merge_range(current_row, current_col + 1, current_row, current_col + 2, line['name'], styles.get('account_label_left'))
            
            elif (line['level'] == 1 or line['level'] == 2) and re.search('^\d{1,2}\.', line['name']):
                # Section Number, Account Label
                number, title = line['name'].split('. ')
                sheet.write(current_row, current_col, number, styles.get('account_label_left'))
                current_col += 1
                sheet.write(current_row, current_col, title, styles.get('text_left'))
                current_col += 1
                
                # Values
                if line['columns'][0]['name'] != '':
                    sheet.write(current_row, current_col, self.get_rounded_integer(line['columns'][0]['name']), styles.get('currency'))
                    current_col += 2
                    if len(line['columns']) > 1:
                        sheet.write(current_row, current_col, self.get_rounded_integer(line['columns'][1]['name']), styles.get('currency'))
                        current_col += 2
                        sheet.write(current_row, current_col, self.get_rounded_integer(line['columns'][3]['name']), styles.get('currency'))
                        current_col += 1
                        if line['columns'][2]['name'] == 'n/a':
                            sheet.write(current_row, current_col, '--', styles.get('text_right'))
                        else:
                            sheet.write(current_row, current_col, self.get_rounded_percent(line['columns'][2]['name']), styles.get('text_right'))
            
            elif line['level'] == 2 and line['name'] == '':
                # Indented line
                indented_col = current_col + 2
                sheet.write(current_row, indented_col, self.get_rounded_integer(line['columns'][0]['name']), styles.get('inline_totals'))
                indented_col += 2
                if len(line['columns']) > 1:
                    sheet.write(current_row, indented_col, self.get_rounded_integer(line['columns'][1]['name']), styles.get('inline_totals'))
                    indented_col += 2
                    sheet.write(current_row, indented_col, self.get_rounded_integer(line['columns'][3]['name']), styles.get('inline_totals'))
                    indented_col += 1
                    if line['columns'][2]['name'] == 'n/a':
                        sheet.write(current_row, indented_col, '--', styles.get('inline_totals'))
                    else:
                        sheet.write(current_row, indented_col, self.get_rounded_percent(line['columns'][2]['name']), styles.get('inline_totals'))

            elif line['name'] in major_sections:
                if line['name'] == 'ACTIVO':
                    row, col = 34, 1
                else:
                    row, col = 26, 10

                sheet.write_blank(row, col, '', styles.get('blue_row', None))
                col += 1
                sheet.write(row, col, line['name'], styles.get('table_header_left'))
                col += 1
                sheet.write(row, col, self.get_rounded_integer(line['columns'][0]['name']), styles.get('table_header_right'))
                col += 1
                sheet.write_blank(row, col, '', styles.get('blue_row', None))
                col += 1
                if len(line['columns']) > 1:
                    sheet.write(row, col, self.get_rounded_integer(line['columns'][1]['name']), styles.get('table_header_right'))
                    col += 1
                    sheet.write_blank(row, col, '', styles.get('blue_row', None))
                    col += 1
                    sheet.write(row, col, self.get_rounded_integer(line['columns'][3]['name']), styles.get('table_header_right'))
                    col += 1
                    sheet.write(row, col, self.get_rounded_percent(line['columns'][2]['name']), styles.get('table_header_right'))
            
            else:
                # level 2 with nothing prepended
                # Indented line
                if line['name']:
                    sheet.write(current_row, current_col + 1, line['name'], styles.get('text_left'))
                indented_col = current_col + 2
                sheet.write(current_row, indented_col, self.get_rounded_integer(line['columns'][0]['name']), styles.get('currency'))
                indented_col += 2
                if len(line['columns']) > 1:
                    sheet.write(current_row, indented_col, self.get_rounded_integer(line['columns'][1]['name']), styles.get('currency'))
                    indented_col += 2
                    sheet.write(current_row, indented_col, self.get_rounded_integer(line['columns'][3]['name']), styles.get('currency'))
                    indented_col += 1
                    if line['columns'][2]['name'] == 'n/a':
                        sheet.write(current_row, col, '--', styles.get('currency'))
                    else:
                        sheet.write(current_row, col, self.get_rounded_percent(line['columns'][2]['name']), styles.get('currency'))
            
            if (side == 0 and index in section_end_indeces_a) or (side == 1 and index in section_end_indeces_b):
                current_row += 2
            elif line['name'] not in major_sections:
                current_row += 1

            current_col = STARTING_COL_A if side == 0 else STARTING_COL_B

    def get_xlsx(self, options, response=None):
        # locale.setlocale(locale.LC_ALL, 'es_MX')
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {
            'in_memory': True,
            'strings_to_formulas': False,
        })
        styles = self.get_styles(workbook)

        #
        # Sheet 1 - general overview
        #
        options['unfolded_lines'] = []  # fold all
        options['unfold_all'] = False  # fold all
        options['add_evolutionary'] = True
        options['for_xlsx'] = True

        ctx = self._set_context(options)
        ctx.update({ 'no_format': True, 'print_mode': True,'prefetch_fields': False })

        sheet = workbook.add_worksheet(self._get_report_name()[:31])
        self.with_context(ctx).write_global_report(sheet, styles, options)

        #
        # Other sheets - looping over annexes
        #
        options['unfolded_lines'] = self.line_ids.children_ids.children_ids.ids
        options['unfold_all'] = True

        annexes = self.line_ids.children_ids.filtered(lambda l: l.annex_number) | self.line_ids.children_ids.children_ids.filtered(lambda l: l.annex_number)
        table_exception_types = [
            'unam_account_group', # By Groups of Accounts
            'reuse_annexes',     # Reuse information from Other Annexes
            'add_annexes',       # Add an Annex
            'subtract_annexes'   # Subtract an Annex
        ]
        for annex in annexes.sorted(key=lambda l: l.annex_number):
            sheet = workbook.add_worksheet(annex.name[:31])
            options['add_evolutionary'] = True
            options['sheet_name'] = False
            options['annex_name'] = annex.name
            options['add_column_labels'] = True
            options['annex_code'] = annex.code
            self.with_context(ctx).write_generic_table_sheet(sheet, options, styles, line_id=annex.id)
            #
            # Other sheets - looping over custom tables
            #
            for table in annex.table_ids:
                # Set options for tables
                options['sheet_name'] = '%s-%s' % (annex.name, table.name)
                options['table_name'] = table.name
                options['table_short_name'] = table.short_name
                options['add_evolutionary'] = table.type not in ['expense_per_program_code_item',
                                                                 'expense_per_dependency', 'expense_per_provider']
                options['add_column_labels'] = table.type in table_exception_types
                if table.type == 'add_annexes':
                    options['add_annex'] = table.added_annex_id.id
                elif table.type == 'subtract_annexes':
                    options['subtract_annex'] = table.subtracted_annex_id.id
                elif table.type == 'reuse_annexes':
                    options['reuse_annex'] = True
                
                self.with_context(ctx).write_specific_table_sheet(workbook, options, styles, annex, table, annexes)
                
                options.pop('table_name')
                options.pop('table_short_name')
                if options.get('add_annex'):
                    options.pop('add_annex')
                elif options.get('subtract_annex'):
                    options.pop('subtract_annex')
                elif options.get('reuse_annex'):
                    options.pop('reuse_annex')

        workbook.close()
        output.seek(0)
        generated_file = output.read()
        output.close()

        return generated_file


class AccountReportManager(models.Model):
    _name = 'account.report.situation.manager'
    _inherit = 'account.report.manager'

    _description = 'Manage Summary and Footnotes of Reports'

    financial_report_id = fields.Many2one('account.financial.situation.html.report')
