from odoo import models, fields, api, _
from datetime import date, datetime,timedelta
import calendar
from odoo.exceptions import UserError, ValidationError, UserError
import numpy as np
import pandas


import logging
_logger = logging.getLogger(__name__)

# CONSTANTS FOR MODELS
ACCOUNT_BALANCE_HISTORY_MODEL = 'account.balance.history'
ACCOUNT_JOURNAL_MODEL = 'account.journal'
ACCOUNT_MOVE_LINE_MODEL = 'account.move.line'
ACCRUED_INTEREST_MODEL = 'accrued.interest'
INVESTMENT_PERIOD_RATE_MODEL = 'investment.period.rate'
PRODUCTIVE_ACCOUNT_MODEL = 'productive.account'

# CONSTANTS FOR MESSAGES
END_MONTH_DATE_ERROR_MESSAGE = "The date is not the last day of the month."

RATE = [
    ('fija','Fija'),
    ('variable','Variable')
]

INDI = [
    ('tiie','TIIE'),
    ('cete','CETE')
]

STATES = [
    ('draft','Draft'),
    ('active','Active'),
]
name = 'Compra de la Inversión'
modelos_repetidos = ['account.move_']

class ProductiveAccounts(models.Model):

    _name = 'productive.account'
    _description = 'Productive Accounts'

    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char(string='First Number', required=True, copy=False, readonly=True, index=True, default='Nuevo')
    bank_account_id = fields.Many2one(ACCOUNT_JOURNAL_MODEL, string='Bank Account Associated with the Term', copy=False)
    journal_accrued_interest_id = fields.Many2one(ACCOUNT_JOURNAL_MODEL, string='Journal Accrued Interest')
    currency_id = fields.Many2one('res.currency', string='Currency', default=lambda self: self.env['res.currency'].search([('name', '=', 'MXN')]))
    invesment_date = fields.Date(string='Registration Date', default=False)
    rate_type = fields.Selection(RATE, string='Rate Type')
    fixed_rate = fields.Float(string='Fixed Rate', digits=(15,6))
    res_fixed_rate = fields.Float(string='Result Fixed Rate', digits=(15,4),readonly=True)
    indicad = fields.Selection(INDI, string='Indicator')
    surcharge = fields.Float(string='Surcharge', digits=(15,4))
    percentage = fields.Float(string='Percentage', digits=(15,4))
    sal_prom = fields.Float(string='Average Balance')                  #El campo permitira temportalmente, insertar el dato de manera manual, porque es necesario obtener un dato de SIOF y por ahora, no lo han proporcionado
    accrued_interest_line = fields.One2many(ACCRUED_INTEREST_MODEL,'productive_account_id',string="Accrued Interest")
    line_ids = fields.One2many(ACCOUNT_MOVE_LINE_MODEL, 'productive_account_id', string="Accounting Notes")
    status = fields.Selection(STATES, string='Status')
    next_first_day_month = fields.Date(string='Next Firts Day Month',default=False)
    months_elapsed = fields.Integer(string='Months Elapsed', default=1)
    frequency = fields.Selection([
        ('daily','Diaria'),
        ('monthly','Mensual')
    ], string='Frecuencia')
    calculation_type = fields.Selection([('surcharge', 'Surcharge'),
                                         ('percentage', 'Percentage'), ],
                                         'Calculation Type')
    bank_account_id_bank = fields.Char(String="Bank Productive Account", store=True, compute = "get_bank_account_id_bank")
    bank_account_id_account = fields.Char(String="Accout Productive Account", store=True, compute = "get_bank_account_id_account")


    init_date_test = fields.Date(string='Fecha de prueba inicio', default=False)
    finish_date_test = fields.Date(string='Fecha de prueba final', default=False)
    date_test = fields.Date(string='Fecha de prueba', default=False)

    @api.depends('bank_account_id')
    def get_bank_account_id_bank(self):
        self.bank_account_id_bank = self.bank_account_id.bank_id.name
        
    @api.depends('bank_account_id')
    def get_bank_account_id_account(self):
        self.bank_account_id_account = self.bank_account_id.bank_account_id.acc_number

    @api.constrains('percentage')
    def _check_percentage(self):
        if self.percentage < 0:
            raise ValidationError(_('El porcentaje no puede ser menor a 0'))
        if self.percentage > 100:
            raise ValidationError(_('La sobretasa no puede ser mayor a 100'))
        
    @api.constrains('surcharge')
    def _check_surcharge(self):
        if self.surcharge < 0:
            raise ValidationError(_('La sobretasa no puede ser menor a 0'))
        if self.surcharge > 1:
            raise ValidationError(_('La sobretasa no puede ser mayor a 1'))

    @api.constrains('fixed_rate')
    def _check_fixed_rate(self):
        if self.fixed_rate < 0:
            raise ValidationError(_('La tasa fija no puede ser menor a 0'))
        


    ## Un onchange para que cuando se elija rate_type en fija el campo indicad se ponga en blanco y se desactive
    @api.onchange('rate_type')
    def _onchange_rate_type(self):
        if self.rate_type == 'fija':
            self.indicad = False
            self.surcharge = False
            self.percentage = False
            self.calculation_type = False
        if self.rate_type == 'variable':
            self.fixed_rate = False
            self.calculation_type = False

    ## onchange para que cuando se elija calculation_type en surcharge el campo percentage se ponga en blanco y se desactive y viceversa
    @api.onchange('calculation_type')
    def _onchange_calculation_type(self):
        if self.calculation_type == 'surcharge':
            self.percentage = False
        if self.calculation_type == 'percentage':
            self.surcharge = False


    @api.model
    def create(self, vals):
        is_exist_proc_acc = self.env[PRODUCTIVE_ACCOUNT_MODEL].search([('bank_account_id','=',vals.get('bank_account_id')),('status','=','active')],limit=1)
        if is_exist_proc_acc:
            raise UserError(_('El diario ya esta cargado a otra Cuenta Productiva activa'))

        """Método que sobrescribe el create del objeto."""
        year = date.today().year
        if vals.get('name','Nuevo') == 'Nuevo':
            name_seq = self.env['ir.sequence'].next_by_code('prod.acc.sequence')
            code_of_journal = self.env[ACCOUNT_JOURNAL_MODEL].search([('id','=',vals.get('journal_accrued_interest_id'))])
            name = code_of_journal.code + '/'+ str(year) + '/' + str(name_seq)
            vals['name'] = name
            vals['status'] = 'draft' 
        return super(ProductiveAccounts, self).create(vals)


    def unlink(self):
        for rec in self:
            if rec.status not in ['draft']:
                raise UserError(_('You can delete only draft status data.'))
            accrued_records = rec.env[ACCRUED_INTEREST_MODEL].search([('productive_account_id', '=', rec.id)])
            for records in accrued_records:
                records.unlink()
        return super(ProductiveAccounts, self).unlink()
    
    def write(self, vals):
        is_exist_proc_acc = self.env[PRODUCTIVE_ACCOUNT_MODEL].search([('bank_account_id','=',vals.get('bank_account_id')),('status','=','active')],limit=1)
        if is_exist_proc_acc:
            raise UserError(_('El diario ya esta cargado a otra Cuenta Productiva activa'))
        return super(ProductiveAccounts, self).write(vals)

    # metodo para generar el apunte contrable de la compra de la inversion
    def pay_investment(self):
        partner_id = self.env.user.partner_id
        company_id = self.env.user.company_id
        account_move = self.env[modelos_repetidos[0]]

        move = {
            'ref':'',
            'conac_move':False,
            'date':self.invesment_date,
            'journal_id': self.accounting_account.id,
            'company_id':company_id.id,
            'line_ids':[
                (0,0,{
                'name':name,
                'account_id':self.bank_account_id.id,
                'debit':self.sal_prom,
                'partner_id':partner_id.id,
                'productive_account_id':self._origin.id
                }),
                (0,0,{
                'name':name,
                'account_id':self.accounting_account.default_credit_account_id.id,
                'credit':self.sal_prom,
                'partner_id':partner_id.id,
                'productive_account_id':self._origin.id
                })
            ]
        }

        move_id = account_move.create(move)
        move_id.action_post()

    # metodo para generar el apunte contrable del interes devengado
    def accounting_notes(self,end_day):
        partner_id = self.env.user.partner_id
        company_id = self.env.user.company_id
        account_move = self.env[modelos_repetidos[0]]
        accrued_interest = self.env[ACCRUED_INTEREST_MODEL].search([('date','=',end_day),('productive_account_id','=',self._origin.id)])

        move = {
            'ref':'',
            'conac_move':False,
            'date':end_day,
            'journal_id': self.journal_accrued_interest_id.id,
            'company_id':company_id.id,
            'line_ids':[
                (0,0,{
                'name':'Interes Devengado',
                'account_id':self.bank_account_id.default_debit_account_id.id,
                'debit':accrued_interest.accrued_interest,
                'partner_id':partner_id.id,
                'productive_account_id':self._origin.id
                }),
                (0,0,{
                'name':'Interes Devengado',
                'account_id':self.journal_accrued_interest_id.interest_account_id.id,
                'credit':accrued_interest.accrued_interest,
                'partner_id':partner_id.id,
                'productive_account_id':self._origin.id
                })
            ]
        }

        move_id = account_move.create(move)
        move_id.action_post()

    # metodo para generar el apunte contrable de la Venta de la inversion
    def buys_investment(self,end_day):
        partner_id = self.env.user.partner_id
        company_id = self.env.user.company_id
        account_move = self.env[modelos_repetidos[0]]

        move = {
            'ref':'',
            'conac_move':False,
            'date':self.invesment_date,
            'journal_id': self.accounting_account.id,
            'company_id':company_id.id,
            'line_ids':[
                (0,0,{
                'name':name,
                'account_id':self.bank_account_id.id,
                'debit':self.sal_prom,
                'partner_id':partner_id.id,
                'productive_account_id':self._origin.id
                }),
                (0,0,{
                'name':name,
                'account_id':self.accounting_account.default_credit_account_id.id,
                'credit':self.sal_prom,
                'partner_id':partner_id.id,
                'productive_account_id':self._origin.id
                })
            ]
        }

        move_id = account_move.create(move)
        move_id.action_post()

    # este metodo es el que enruta que metodo ejecutara dependiendo de que tipo de tasa tiene el registro
    def calculate_productive_accounts(self):
        date_calculation = self.date_test
        if self.env[ACCRUED_INTEREST_MODEL].search([('date','=',self.date_test),('productive_account_id.id','=',self.id)]):
            raise UserError(_('Ya existe un registro con esa fecha'))
        if self.status == 'active':
            if self.rate_type == 'fija':
                self.calcule_fixed_rate_ai(date_calculation)
            if self.rate_type == 'variable':
                if self.indicad == 'tiie':
                    self.calcule_tiie(date_calculation)
                elif self.indicad == 'cete':
                    self.calcule_cete(date_calculation)
            

    # metodo que obtiene la tasa (rate_days_28) que se indique en el mes correspondiente a la fecha del parametro y lo retorna.
    def get_rate_in_month(self,product_type,date_calculation):

        obj_investment_periot_rate = self.env[INVESTMENT_PERIOD_RATE_MODEL]
        if product_type == 'CETES':
            obj_investment_periot_rate.get_investment_product_rate_cete(date_calculation.month,date_calculation.year)
        if product_type == 'TIIE':
            obj_investment_periot_rate.get_investment_product_rate_tiie(date_calculation.month,date_calculation.year)

        # Se delimita el primer dia del mes y el ultimo dia del mes
        ini_day = date(date_calculation.year,date_calculation.month,1)
        end_day = date(date_calculation.year,date_calculation.month,calendar.monthrange(date_calculation.year,date_calculation.month)[1])

        # Se obtiene la cantidad de dias del mes de month y year que se le paso como parametro
        days = calendar.monthrange(date_calculation.year,date_calculation.month)[1]

        # Se obtiene los registros de tasas del periodo que se le paso como parametro en la fecha de calculo
        period_rates = obj_investment_periot_rate.search([('product_type','=',product_type),('rate_date','>=',ini_day),('rate_date','<=',end_day)])

        # Se crea un arreglo con las fechas del periodo que se le paso como parametro y se inicializa la tasa en 0.0000
        days_of_month = []
        for i in range(days):
            prom = {
                'fecha': ini_day + timedelta(days=i),
                'tasa':0.0000
            }
            days_of_month.append(prom)

        # se compara days_of_month con period_rates para pasarle las tasas de los dias que se registraron
        for i in range(len(days_of_month)):
            for j in period_rates:
                if days_of_month[i]['fecha'] == j.rate_date:
                    days_of_month[i]['tasa'] = j.rate_days_28

        # se compara days_of_month con period_rates para obtener las tasas de los dias que no se registraron, colocando la tasa del dia anterior registrado
        for i in range(len(days_of_month)):
            if days_of_month[i]['tasa'] == 0.0000:

                # para el caso del indice 0 del arreglo, se obtiene la tasa del dia anterior al primer dia del mes 
                if i == 0:
                    new_date_consult = days_of_month[i]['fecha'] - timedelta(days=1)
                    if product_type == 'CETES':
                        tasa = self.get_cete_today(new_date_consult)
                    if product_type == 'TIIE':
                        tasa = self.get_tiie_today(new_date_consult)
                    days_of_month[i]['tasa'] = tasa
                # caso contrario, se obtiene la tasa del dia anterior al dia actual
                else:
                    days_of_month[i]['tasa'] = days_of_month[i-1]['tasa']
        
        # Calculamos promedio
        suma = 0
        for i in range(len(days_of_month)):
            suma += days_of_month[i]['tasa']
        promedio_rate_days_28 = "{:.4f}".format(suma / days)
        
        return promedio_rate_days_28

    # este metodo solo asigna el estatus activado al registro
    # esto para ser reconocido en el CRON             
    def set_active(self):
        # Seguridad de la función
        if not self.env.user.has_group('jt_investment.group_investment_admin_user'):
            raise UserError(_("You don't have permission to perform this action."))
        
        # Comprobación de indicador TIIE o CETE para tasa variable
        if self.rate_type == 'variable' and not self.indicad:
            raise UserError(_("Debes seleccionar un indicador para la tasa variable"))
        
        if self.rate_type == 'fija':
            if not self.fixed_rate or self.fixed_rate <= 0:
                raise UserError(_("Debes ingresar una tasa fija valida"))
        if not self.bank_account_id_account:
            raise UserError(_("Tu diario no tiene una cuenta bancaria asociada"))
        
        self.status = 'active'
        
    '''Este metodo obtiene los dias del periodo, resibiendo un rango de dias (dia inicial y dia final) para obtener un dato entero con la tantidad de dias
    en la primera condicion, se hizo la resta directa porque en los documentos de ejemplo del usuario, el primer periodo tomaba los dias de 24hrs y los
    siguientes ,los cuenta como dias contados, ejemplo:
    dias de 24hrs   ----> del dia 01/01/23 al 02/01/23 transcurrio 1 dia
    dias contados   ----> del dia 01/01/23 al 02/01/23 transcurrio 2 dias'''
    def days_of_period(self,ini_day,end_day):
        if self.months_elapsed == 1:
            period = end_day - ini_day
            days_of_period = period.days
            return days_of_period
        else:
            period = end_day - (ini_day - timedelta(days=1))
            days_of_period = period.days
            return days_of_period

    # este metodo se usaba para retroceder dias si no eran habiles, ya no se uso porque el cron ya tiene programado esto
    def subtract_days(self,date):
        end_day_month = datetime.strptime(date,"%Y-%m-%d")
        end_day_month = end_day_month - timedelta(days=1)
        return end_day_month.strftime('%Y-%m-%d')

    # este metodo obtiene el ultimo dia natural del mes
    def get_day_end_month(self,day):
        end_month = calendar.monthrange(day.year,day.month)[1]
        end_day_month = date(day.year,day.month,end_month)
        return end_day_month


    
    #este metodo es el que se ejecuta en el cron en el documento jt_investment/models/accrued_interest
    def start_check_end_month(self):
        all_productive_accounts = self.env[PRODUCTIVE_ACCOUNT_MODEL].search([])
        for prod_acc in all_productive_accounts:
            prod_acc.is_firts_month_or_other()

    #este metodo sirve para asignar el primer dia del siguiente mes, valida si es un mes del mismo año o es nuevo año
    def set_next_first_day_month(self,end_day):
        self.months_elapsed += 1
        number_month = end_day.month
        if number_month < 12:
            self.next_first_day_month = date(date.today().year,(number_month+1),1)
        elif number_month == 12:
            number_month = 1
            self.next_first_day_month = date((date.today().year+1),(number_month),1)
        
    #este metodo valida si es el primer mes transcurrido del registro o es otro, esto porque, hay situaciones donde el primer registro empieza a mediados de mes
    # o casi finalizando
    def is_firts_month_or_other(self):
        if self.invesment_date.month == self.get_day_end_month(date.today()).month:
            self.start_productive_account(self.invesment_date,self.get_day_end_month(date.today()))
            self.accounting_notes(self.get_day_end_month(date.today()))
            self.set_next_first_day_month(self.get_day_end_month(date.today()))
        else:
            self.start_productive_account(self.next_first_day_month,self.get_day_end_month(self.next_first_day_month))
            self.accounting_notes(self.get_day_end_month(self.next_first_day_month))
            self.set_next_first_day_month(self.get_day_end_month(self.next_first_day_month))

    #Este metodo obtiene el calculo del caso de Tasa Fija
    def calcule_fixed_rate_ai(self,date_for_calculate):
        ini_day= date(date_for_calculate.year,date_for_calculate.month,1)
        end_day = date(date_for_calculate.year,date_for_calculate.month,calendar.monthrange(date_for_calculate.year,date_for_calculate.month)[1])
        
        # Verifica que la fecha sea el ultimo dia del mes
        current_date = pandas.Timestamp(date_for_calculate)
        if not current_date.is_month_end:
            raise UserError(_(END_MONTH_DATE_ERROR_MESSAGE))

        days = calendar.monthrange(date_for_calculate.year,date_for_calculate.month)[1]
        obj_account_balance_history = self.env[ACCOUNT_BALANCE_HISTORY_MODEL]
        balance = obj_account_balance_history.get_balance_history_in_month(date_for_calculate.month,date_for_calculate.year,self.bank_account_id.bank_account_id.acc_number)
        arr = []
        if ini_day.month == end_day.month:

            accrued_interest = balance * ((self.fixed_rate/360)*days)
            vals = {
                'productive_account_id':self._origin.id,
                'days_period_elapsed': days,
                'accrued_interest': accrued_interest,
                'date':date_for_calculate,
                'c_p_average_balance':balance
            }
            arr.append(vals)
            if not self.accrued_interest_line:
                self.accrued_interest_line = self.env[ACCRUED_INTEREST_MODEL].create(arr)
            else:
                self.accrued_interest_line += self.env[ACCRUED_INTEREST_MODEL].create(arr)
            
            #self.accounting_notes(date_for_calculate)

    #Este metodo obtiene el calculo del caso de Tasa Variable CETE
    def calcule_cete(self,reference_date):
        date_for_calculate = self.date_test

        obj_investment_periot_rate = self.env[INVESTMENT_PERIOD_RATE_MODEL]
        obj_investment_periot_rate.get_investment_product_rate_cete(date_for_calculate.month,date_for_calculate.year)

        obj_account_balance_history = self.env[ACCOUNT_BALANCE_HISTORY_MODEL]

        # Modificacion del indicador, dias transcurridos y saldos segun frecuencia
        if self.frequency=='daily':
            indicator = self.get_cete_today(date_for_calculate)
            days=1
            balance = obj_account_balance_history.get_balance_history_in_date(date_for_calculate,self.bank_account_id.bank_account_id.acc_number)

        if self.frequency=='monthly':
            # Verifica que la fecha sea el ultimo dia del mes
            current_date = pandas.Timestamp(date_for_calculate)
            if not current_date.is_month_end:
                raise UserError(_(END_MONTH_DATE_ERROR_MESSAGE))

            indicator = self.get_rate_in_month('CETES',date_for_calculate)

            days = calendar.monthrange(date_for_calculate.year,date_for_calculate.month)[1]
            balance = obj_account_balance_history.get_balance_history_in_month(date_for_calculate.month,date_for_calculate.year,self.bank_account_id.bank_account_id.acc_number)
        
        indicator = float(indicator)
        #si es Sobretasa
        if self.surcharge and not self.percentage:
            rate = indicator + self.surcharge
            accrued_interest = balance *((((indicator + self.surcharge)/100)/360)*days)
        #si es porcentaje
        elif self.percentage and not self.surcharge:
            rate = indicator * (self.percentage/100)
            accrued_interest = balance * ((((indicator * (self.percentage/100))/100)/360) * days)

        vals = {
            'productive_account_id':self._origin.id,
            'days_period_elapsed': days,
            'accrued_interest': accrued_interest,
            'date':reference_date,
            'c_p_average_balance':balance,
            'indicator':indicator,
            'rate':rate,
        }

        self.accrued_interest_line += self.env[ACCRUED_INTEREST_MODEL].create(vals)
        # self.accounting_notes(reference_date)

    #Este metodo obtiene el calculo del caso de Tasa Variable TIIE
    def calcule_tiie(self,reference_date):
        date_for_calculate = self.date_test

        obj_investment_periot_rate = self.env[INVESTMENT_PERIOD_RATE_MODEL]
        obj_investment_periot_rate.get_investment_product_rate_tiie(date_for_calculate.month,date_for_calculate.year)

        obj_account_balance_history = self.env[ACCOUNT_BALANCE_HISTORY_MODEL]

        # Modificacion del indicador, dias transcurridos y saldos segun frecuencia
        if self.frequency=='daily':
            indicator = self.get_tiie_today(date_for_calculate)
            days=1
            balance = obj_account_balance_history.get_balance_history_in_date(date_for_calculate,self.bank_account_id.bank_account_id.acc_number)

        if self.frequency=='monthly':
            # Verifica que la fecha sea el ultimo dia del mes
            current_date = pandas.Timestamp(date_for_calculate)
            if not current_date.is_month_end:
                raise UserError(_(END_MONTH_DATE_ERROR_MESSAGE))

            indicator = self.get_rate_in_month('TIIE',date_for_calculate)

            days = calendar.monthrange(date_for_calculate.year,date_for_calculate.month)[1]
            balance = obj_account_balance_history.get_balance_history_in_month(date_for_calculate.month,date_for_calculate.year,self.bank_account_id.bank_account_id.acc_number)
        
        indicator = float(indicator)
        #si es Sobretasa
        if self.calculation_type == "surcharge":
            rate = indicator + self.surcharge
            accrued_interest = balance *((((indicator + self.surcharge)/100)/360)*days)
        #si es porcentaje
        if self.calculation_type == "percentage":
            rate = indicator * (self.percentage/100)
            accrued_interest = balance * ((((indicator * (self.percentage/100))/100)/360) * days)

        vals = {
            'productive_account_id':self._origin.id,
            'days_period_elapsed': days,
            'accrued_interest': accrued_interest,
            'date':reference_date,
            'c_p_average_balance':balance,
            'indicator':indicator,
            'rate':rate,

        }

        self.accrued_interest_line += self.env[ACCRUED_INTEREST_MODEL].create(vals)
        # self.accounting_notes(reference_date)

    def get_tiie_today(self,date_consult):

        obj_investment_periot_rate = self.env[INVESTMENT_PERIOD_RATE_MODEL]
        obj_investment_periot_rate.get_investment_product_rate_tiie(date_consult.month,date_consult.year)


        tiie_value = obj_investment_periot_rate.search([('product_type','=','TIIE'),('rate_date','=',date_consult)])
        
        if not tiie_value:
            # Si no se encuentra el registro, buscar hacia atrás hasta encontrar uno
            search_date = date_consult - timedelta(days=1)
            while search_date <= date_consult:
                obj_investment_periot_rate = self.env[INVESTMENT_PERIOD_RATE_MODEL]
                obj_investment_periot_rate.get_investment_product_rate_tiie(search_date.month,search_date.year)
                tiie_value = obj_investment_periot_rate.search([('product_type','=','TIIE'),('rate_date','=',search_date)])
                if tiie_value:
                    break 
                 
                if search_date.year < 2010:
                    break

                search_date = search_date - timedelta(days=1)
        return tiie_value.rate_days_28

    def get_cete_today(self,date_consult):
        
        obj_investment_periot_rate = self.env[INVESTMENT_PERIOD_RATE_MODEL]
        obj_investment_periot_rate.get_investment_product_rate_cete(date_consult.month,date_consult.year)


        cete_value = obj_investment_periot_rate.search([('product_type','=','CETES'),('rate_date','=',date_consult)])
        ## SI no cuentra registro buscar en un dia anterior a la fecha date_consult
        if not cete_value:
            search_date = date_consult - timedelta(days=1)
            while search_date <= date_consult:
                obj_investment_periot_rate = self.env[INVESTMENT_PERIOD_RATE_MODEL]
                obj_investment_periot_rate.get_investment_product_rate_cete(search_date.month,search_date.year)
                cete_value = obj_investment_periot_rate.search([('product_type','=','CETES'),('rate_date','=',search_date)])
                if cete_value:
                    break 
                if search_date.year < 2010:
                    break
                search_date = search_date - timedelta(days=1)

        return cete_value.rate_days_28
    
    # Funcion que usa el Cron para calcular todas las cuentas productivas en estado activo
    def cron_calculate_productive_accounts(self):
        records = self.search([('status','=','active')])

        for rec in records:
            rec.calculate_productive_accounts()
  
    # funcion para retornar una vista que manda los saldos de account.balance.history relacionados con el campo bank_account_id
    def get_record_account_balance_history(self):
        return {
            'name': 'Histórico de saldos de la cuenta productiva',
            'view_mode': 'tree,form',
            'res_model': ACCOUNT_BALANCE_HISTORY_MODEL,
            'domain': [('account_number_id', '=', self.bank_account_id_account)],
            'type': 'ir.actions.act_window',
        }
    def generate_accounting_notes (self):
        #verifica que no exista otro registro con la misma fecha de calculo
        if self.env[ACCOUNT_MOVE_LINE_MODEL].search([('date','=',self.date_test),('productive_account_id','=',self.id)]):
            raise UserError(_("Ya existe un registro con esta fecha"))

        self.accounting_notes(self.date_test)
       

class ProductiveAccountsLine(models.Model):
    _inherit = 'accrued.interest'

    productive_account_id = fields.Many2one(PRODUCTIVE_ACCOUNT_MODEL)
    c_p_average_balance = fields.Float(string='Average balance')

class AccountMoveLine(models.Model):

    _inherit = 'account.move.line'

    productive_account_id = fields.Many2one(PRODUCTIVE_ACCOUNT_MODEL, 'Productive Account ID')