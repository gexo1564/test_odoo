from odoo import models, fields, api
from lxml import etree


class ResPartner(models.Model):

    _inherit = 'res.partner'

    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        res = super().fields_view_get(view_id=view_id,
                                      view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])

        is_group_income_fac_administrator_user = self.env.user.has_group('jt_income.group_income_fac_administrator_user')
        is_group_income_fac_finance_user = self.env.user.has_group('jt_income.group_income_fac_finance_user')
        is_group_income_fac_accounting_user = self.env.user.has_group('jt_income.group_income_fac_accounting_user')

        if (is_group_income_fac_administrator_user or is_group_income_fac_finance_user or is_group_income_fac_accounting_user):
            if 'toolbar' in res and 'action' in res['toolbar']:
                res['toolbar']['action'] = []

            for node in doc.xpath("//" + view_type):
                node.set('create', '0')
                node.set('import', '0')
                node.set('edit', '0')
                node.set('delete', '0')

            for node in doc.xpath("//field"):
                node.set('options','{"no_open":true}')   

        res['arch'] = etree.tostring(doc)

        return res
