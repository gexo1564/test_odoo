from lxml import etree
from odoo import models, api
import logging

class AccountPayment(models.AbstractModel):
	_inherit = 'account.payment'

	@api.model
	def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
		def hide_buttons(node, value):
			node.set('create', value)
			node.set('import', value)
			node.set('export_xlsx', value)
			node.set('delete', value)
			node.set('edit', value)
		STR_FIELD="//field"
		res = super().fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
		doc = etree.XML(res['arch'])
		is_admin = self.env.user.has_group('jt_income.group_income_admin_user')
		# Módulo de Ingresos
		logging.critical(res['name'])
		if '.income' in res['name']:
			# Aplica para la siguiente vista:
			#   - Ingresos:
			#       - Cobros
			if not is_admin:
				for node in doc.xpath("//" + view_type):
					# Ocultar botones de las vistas Odoo
					hide_buttons(node, '0')
				# Ocultar Combobox de Imprimir y Accion
				if res.get('toolbar'):
					res['toolbar']['action'] = []
					res['toolbar']['print'] = []
				
				for node in doc.xpath():
					node.set('options','{"no_open":true}')   		
		# Modulo de Contabilidad
		elif '.accounting.module' in res['name']:
			# Aplica para las siguientes vistas:
			#   - Clientes:
			#       - Pagos
			#   - Provedores:
			#       - Pagos
			if '.payment' in res['name']:
				is_admin_contabilidad = self.env.user.has_group('jt_account_base.group_account_admin_user')
				is_training_contabilidad = self.env.user.has_group('jt_account_base.group_account_training_user')
				is_register_contabilidad = self.env.user.has_group('jt_account_base.group_account_register_user')
				# Oculta los botones y acciones para los usuarios que no son administradores
				# o de registro.
				if not (is_admin_contabilidad or is_training_contabilidad or is_register_contabilidad):
					for node in doc.xpath("//" + view_type):
						# Ocultar botones de las vistas Odoo
						hide_buttons(node, '0')
					# Ocultar Combobox de Imprimir y Accion
					if res.get('toolbar'):
						res['toolbar']['action'] = []
						res['toolbar']['print'] = []
		#Módulo de Finanzas
		elif '.Finance' in res['name']:
			"""
			Aplica para las vistas:
				-Gestión de Pagos
					-Pagos
				-Gestión de Pagos de nómina
					-Pagos de nómina
					-Pagos de pensión
				-Gestión de Pagos diferentes a nómina
					-Pagos diferentes de nómina
				-UPA PAPPIT
					-Pagos
			"""
			#Finanzas
			is_admin_finance = self.env.user.has_group('jt_payroll_payment.group_on_finance_admin')
			is_finance_user = self.env.user.has_group('jt_payroll_payment.group_admin_finance_user')
			is_approval_user = self.env.user.has_group('jt_payroll_payment.group_approval_user')
			is_consult_user = self.env.user.has_group('jt_payroll_payment.group_consult_user')
			is_register_user = self.env.user.has_group('jt_payroll_payment.group_register_user')

			

			logging.critical(res['name'])
			#Ventanas de Gestión de Pagos/Pagos
			if 'form' not in res['name']:
				if ' Payment.Finance' in res['name']:
					#Permisos generales

					if is_admin_finance:
						for node in doc.xpath("//" + view_type):
							hide_buttons(node, '1')
					elif (is_finance_user or is_approval_user or is_consult_user or is_register_user):
						for node in doc.xpath("//" + view_type):
							hide_buttons(node, '0')
							node.set('export_xlsx', '1')


					#Acciones e impresiones
					if is_consult_user and res.get('toolbar'):
						res['toolbar']['action'] = []
						res['toolbar']['print'] = []
					elif is_approval_user and res.get('toolbar'):
						res['toolbar']['print'] = []
						action_actions=["Exportar","Suprimir","Validar pago / cobro","Generar Formato de Banco",
						"Cancelada","Pago Rechazado"]
						action_items = filter(
							lambda x: x['display_name'] in action_actions,
							res['toolbar']['action']
						)
						res['toolbar']['action'] = list(action_items)

					
								

				#Ventanas de Gestión de pagos de nómina/Pago de nómina
				elif 'Payroll.Payment.Finance' in res['name']:
					#Permisos generales
					if is_admin_finance:
						for node in doc.xpath("//" + view_type):
							hide_buttons(node, '1')
							node.set('import', '1')
							node.set('export', '1')
					elif is_finance_user:
						for node in doc.xpath("//" + view_type):
							hide_buttons(node, '0')
							node.set('edit', '1')
					elif is_approval_user or is_consult_user or is_register_user:
						for node in doc.xpath("//" + view_type):
							hide_buttons(node, '0')
							node.set('export_xlsx', '1')



				#Ventanas de Gestión de pagos de nómina/Pago de pensión
				elif 'Pension.Payment.Finance' in res['name']:
					if is_admin_finance:
						for node in doc.xpath("//" + view_type):
							hide_buttons(node, '1')
					elif is_finance_user:
						for node in doc.xpath("//" + view_type):
							hide_buttons(node, '0')
					elif is_approval_user or is_consult_user or is_register_user:
						for node in doc.xpath("//" + view_type):
							hide_buttons(node, '0')
							node.set('export_xlsx', '1')

				#Ventanas de Gestión de pagos diferentes a nómina/Pagos diferentes a nómina
				elif 'Diff.Payroll.Payment.Finance' in res['name']:
					if is_admin_finance:
						for node in doc.xpath("//" + view_type):
							hide_buttons(node, '1')
					elif is_finance_user or is_approval_user or is_register_user:
						for node in doc.xpath("//" + view_type):
							node.set('edit', '1')
							node.set('create', '0')
							node.set('delete', '1')
							node.set('import', '0')
							node.set('export_xlsx', '1')
					elif is_consult_user :
						for node in doc.xpath("//" + view_type):
							hide_buttons(node, '0')

				#Ventanas de UPA PAPIIT/Pagos
				elif 'PAPIIT.Payment.Finance' in res['name']:
					if is_admin_finance or is_finance_user:
						for node in doc.xpath("//" + view_type):
							hide_buttons(node, '1')
					elif is_approval_user or is_consult_user:
						for node in doc.xpath("//" + view_type):
							hide_buttons(node, '0')	
					elif is_register_user :
						for node in doc.xpath("//" + view_type):
							node.set('edit', '1')
							node.set('create', '1')
							node.set('delete', '1')
							node.set('import', '0')
							node.set('export_xlsx', '0')

			#Ocultar campos en Formularios
			elif 'form' in res['name']:
				invisible='{"invisible":true}'
				"""
				En el formulario se ocultaran los campos
					-Empresa
					-Folio de Solicitud
					-Banco de recepción de pago
					-Cuenta bancaria de recepcióoon de pago
				"""
				fields=["partner_id","payment_request_id","payment_bank_id","payment_bank_account_id",
				"l10n_mx_edi_payment_method_id"]
				buttons=['post','cancel','action_draft','action_validate_payment_procedure']
				#Acciones para el formulario
				if is_admin_finance or is_finance_user:
					for node in doc.xpath("//" + view_type):
						node.set('edit','1')
						if is_admin_finance:
							node.set('create','1')
						else:
							node.set('create','0')
				elif (is_approval_user or is_consult_user or is_register_user):
					for node in doc.xpath("//" + view_type):
						node.set('edit','0')
						node.set('create','0')

				#Acciones dentro de registro
				if (is_approval_user or is_consult_user or is_register_user):
					for node in doc.xpath("//button"):
						#logging.critical("boton "+node.attrib['name'])
						if (node.attrib['name'] in buttons):
							node.set('modifiers',invisible)
				if (is_approval_user or is_consult_user or is_register_user):
					for node in doc.xpath(STR_FIELD):
						if (node.attrib['name'] in fields):
							#logging.critical(node.attrib['modifiers'])
							node.set('modifiers',invisible)

				if is_admin_finance or is_finance_user:
					for node in doc.xpath("//" + view_type):
						node.set('delete','1')
						node.set('duplicate','1')
				else:
					for node in doc.xpath("//" + view_type):
						node.set('delete','0')
						node.set('duplicate','0')
		
		#Sección para las solicitudes de compra para fondo de divisas
		elif 'account.payment' in res['name']:
			logging.critical("Caso Solicitudes")
			is_admin_finance = self.env.user.has_group('jt_payroll_payment.group_on_finance_admin')
			is_finance_user = self.env.user.has_group('jt_payroll_payment.group_admin_finance_user')
			is_approve_user = self.env.user.has_group('jt_currency_purchase_req.group_request_approval')
			#Botones
			if not(is_admin_finance or is_finance_user or is_approve_user):
				buttons=['post','cancel']
				for node in doc.xpath("//button"):
					if (node.attrib['name'] in buttons):
						node.set('modifiers','{"invisible":true}')

			
		# Facturación digital
		is_group_income_fac_administrator_user = self.env.user.has_group('jt_income.group_income_fac_administrator_user')
		is_group_income_fac_finance_user = self.env.user.has_group('jt_income.group_income_fac_finance_user')
		is_group_income_fac_accounting_user = self.env.user.has_group('jt_income.group_income_fac_accounting_user')

		if (is_group_income_fac_administrator_user or is_group_income_fac_finance_user or is_group_income_fac_accounting_user):
			for node in doc.xpath("//" + view_type):
				node.set('create', '0')
				node.set('import', '0')
				node.set('edit','0')
				node.set('export','0')
				node.set('delete','0')

			prints_to_hide = [
					"Recibo de Cobro",
					"Recibo de Viáticos",
				]
			
			actions_to_hide = [
				"Validar pago / cobro",
				"Enviar recibo por correo electrónico",
				"Generar Formato de Banco",
				"Cobro Rechazado",
				"Cancelada",
				"Carga de Layout Bancario",
				"Reassign Budget",
				"Establecer fecha de cobro",
				"Pago Rechazado",
				"Establecer fecha de Pago",
				"Enviar ingresos por correo",
			]			

			if res.get('toolbar'):
					if res['toolbar'].get('print'):
						print_items = filter(lambda x: x['display_name'] not in prints_to_hide,
							res['toolbar']['print']
							)
						res['toolbar']['print'] = list(print_items)	

					if res['toolbar'].get('action'):
						action_items = filter(lambda x: x['display_name'] not in actions_to_hide, 
						res['toolbar']['action'])
						res['toolbar']['action'] = list(action_items)


		is_group_income_finance_user = self.env.user.has_group('jt_income.group_income_finance_user')
		is_group_income_project_coordinator_user = self.env.user.has_group('jt_income.group_income_project_coordinator_user')

		if is_group_income_finance_user or is_group_income_project_coordinator_user:
			
			for node in doc.xpath(STR_FIELD):
				node.set('options','{"no_open":true}')

			prints_to_hide = [
					"Recibo de Cobro",
					"Recibo de Viáticos",
			]

			actions_to_hide = [
				"Enviar recibo por correo electrónico",
			]

			if res.get('toolbar'):
					if res['toolbar'].get('print'):
						print_items = filter(lambda x: x['display_name'] not in prints_to_hide,
							res['toolbar']['print']
							)
						res['toolbar']['print'] = list(print_items)

					if res['toolbar'].get('action'):
						action_items = filter(lambda x: x['display_name'] not in actions_to_hide, 
						res['toolbar']['action'])
						res['toolbar']['action'] = list(action_items)								  
				
		res['arch'] = etree.tostring(doc)
		return res