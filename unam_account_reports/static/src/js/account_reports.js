odoo.define('unam_account_reports.account_report', function(require) {
    'use strict';
    
    const core = require('web.core');
    const AccountReportWidget = require('account_reports.account_report');

    const UNAMAccountReportWidget = AccountReportWidget.extend({
        render_searchview_buttons: function() {
            const $periods_input = this.$searchview_buttons.find('input[name="periods_number"]');
            _.each($periods_input, function(el) {
                el.disabled = true;
            });
            this._super.apply(this, arguments);
        }
    });

    core.action_registry.add('unam_account_report', UNAMAccountReportWidget);

    return UNAMAccountReportWidget;
});

