# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
import base64
from odoo.modules.module import get_resource_path
from xlrd import open_workbook
from odoo.exceptions import UserError, ValidationError
import logging
from datetime import datetime
import openpyxl
import io


class ImportBudgetsLine(models.TransientModel):

    _name = 'import.budgets.line'
    _description = 'Import Budgets Line'

    father_budget = fields.Many2one('siif.budget.mgmt.income.budgets', 'Income Budgets', ondelete="cascade") # Linea Nueva
    record_number = fields.Integer(string='Número de Registros')
    file = fields.Binary(string='Archivo', store=True)
    filename = fields.Char(string='File name')
    lines_ok = fields.One2many('siif.budget.mgmt.income.budgets.lines', 'my_id', 'Line Budgets') # Linea Nueva
    lines_ok_mirror = fields.One2many('siif.budget.mgmt.income.budget.mirror', 'my_id', 'Line Busgetas Mirror')
    download_file = fields.Binary(string='Descargar Archivo')
    download_filename = fields.Char(string='Download File name')

    def download(self):
        file_path = get_resource_path(
            'siif_budget_mgmt', 'static/file/import_line', 'SIIF_Plantilla_Presupuesto de Ingresos.xls')
        file = False
        with open(file_path, 'rb') as file_date:
            file = base64.b64encode(file_date.read())
        self.download_filename = 'SIIF_Plantilla_Presupuesto de Ingresos.xls'
        self.download_file = file

        return {
            'name': 'Download Sample File',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': False,
            'res_model': 'import.budgets.line',
            'domain': [],
            'type': 'ir.actions.act_window',
            'target': 'new',
            'res_id': self.id,
        }

    @api.onchange('file') # Linea Nueva
    def onchange_file(self): # Linea Nueva
        self.lines_err = None # Linea Nueva
        self.lines_ok = None # Linea Nueva
        self.lines_ok_mirror = None
        self.file_errors = None # Linea Nueva
        self.father_budget.budget_file = self.file
        self.father_budget.budget_file_filename = self.filename

    def import_line(self):

        lines_ok, lines_ok_mirror, lines_err, msgs_err = [], [], [], [] # Linea Nueva

        def set_line_error(sheet, row, msg): # Linea Nueva
            # Lineas de error
            lines_err.append((0, 0, { # Linea Nueva
                'estimated_fiscal_year': sheet.cell_value(row, 0),
                'estimated_cri_id': sheet.cell_value(row, 1), # Linea Nueva
                'estimated_january_field': sheet.cell_value(row, 2), # Linea Nueva
                'estimated_february_field': sheet.cell_value(row, 3), # Linea Nueva
                'estimated_march_field': sheet.cell_value(row, 4), # Linea Nueva
                'estimated_april_field': sheet.cell_value(row, 5), # Linea Nueva
                'estimated_may_field': sheet.cell_value(row, 6), # Linea Nueva
                'estimated_june_field': sheet.cell_value(row, 7), # Linea Nueva
                'estimated_july_field': sheet.cell_value(row, 8), # Linea Nueva
                'estimated_august_field': sheet.cell_value(row, 9), # Linea Nueva
                'estimated_september_field': sheet.cell_value(row, 10), # Linea Nueva
                'estimated_october_field': sheet.cell_value(row, 11), # Linea Nueva
                'estimated_november_field': sheet.cell_value(row, 12), # Linea Nueva
                'estimated_december_field': sheet.cell_value(row, 13), # Linea Nueva
                'estimated_total': sheet.cell_value(row, 14), # Linea Nueva

                'mirror_fiscal_year': sheet.cell_value(row, 0),
                'mirror_cri_id': sheet.cell_value(row, 1), # Linea Nueva
                'mirror_january_field': sheet.cell_value(row, 2), # Linea Nueva
                'mirror_february_field': sheet.cell_value(row, 3), # Linea Nueva
                'mirror_march_field': sheet.cell_value(row, 4), # Linea Nueva
                'mirror_april_field': sheet.cell_value(row, 5), # Linea Nueva
                'mirror_may_field': sheet.cell_value(row, 6), # Linea Nueva
                'mirror_june_field': sheet.cell_value(row, 7), # Linea Nueva
                'mirror_july_field': sheet.cell_value(row, 8), # Linea Nueva
                'mirror_august_field': sheet.cell_value(row, 9), # Linea Nueva
                'mirror_september_field': sheet.cell_value(row, 10), # Linea Nueva
                'mirror_october_field': sheet.cell_value(row, 11), # Linea Nueva
                'mirror_november_field': sheet.cell_value(row, 12), # Linea Nueva
                'mirror_december_field': sheet.cell_value(row, 13), # Linea Nueva
                'mirror_total': sheet.cell_value(row, 14), # Linea Nueva
            })) # Linea Nueva
            # Mensaje de error
            line = ("[" + "'%s', "*14 + "%s]") % tuple([sheet.cell_value(row, i) for i in range(15)]) # Linea Nueva
            msgs_err.append("%s ------->> %s" % (line, msg)) # Linea Nueva


        if not self.file:
            raise UserError(_('Please Upload File.'))
        try:
            data = base64.decodestring(self.file)
            book = open_workbook(file_contents=data or b'')
        except UserError as e:
            raise UserError(e)

        sheet = book.sheet_by_index(0)


        total_rows = self.record_number + 1
        if sheet.nrows != total_rows:
            raise UserError(_('Number of records do not match with file'))
        
        sum_total = 0

        for row in range(1, sheet.nrows):

            estimated_fiscal_year = sheet.cell_value(row, 0)
            estimated_cri_id = sheet.cell_value(row, 1) # Linea Nueva
            estimated_january_field = sheet.cell_value(row, 2)
            estimated_february_field = sheet.cell_value(row, 3)
            estimated_march_field = sheet.cell_value(row, 4)
            estimated_april_field = sheet.cell_value(row, 5)
            estimated_may_field = sheet.cell_value(row, 6)
            estimated_june_field = sheet.cell_value(row, 7)
            estimated_july_field = sheet.cell_value(row, 8)
            estimated_august_field = sheet.cell_value(row, 9)
            estimated_september_field = sheet.cell_value(row, 10)
            estimated_october_field = sheet.cell_value(row, 11)
            estimated_november_field = sheet.cell_value(row, 12)
            estimated_december_field = sheet.cell_value(row, 13)
            estimated_total = sheet.cell_value(row, 14)

            sum_total += estimated_total

            line = {} # Linea Nueva

            if estimated_fiscal_year != self.father_budget.fiscal_year.name:
                set_line_error(sheet, row, 'El año fiscal del excel no coincide con el del formulario. Error en la celda A%s' % (row + 1))
                continue
            fiscal_year_id = self.env['account.fiscal.year'].search([('name', '=', estimated_fiscal_year)])
            if not fiscal_year_id:
                set_line_error(sheet, row, 'No se encontró el Año Fiscal. Error en la celda A%s' % (row + 1))
                continue # Linea Nueva
            line.update({
                'estimated_fiscal_year': fiscal_year_id.id,
            })

            if len(estimated_cri_id) != 6:
                set_line_error(sheet, row, 'El Código CRI no cumple con la estructura. Error en la celda B%s' % (row + 1)) # Linea Nueva
                continue # Linea Nueva
            cri_id_id = self.env['classifier.income.item'].search([('name', '=', estimated_cri_id)]) # Linea Nueva
            if not cri_id_id: # Linea Nueva
                set_line_error(sheet, row, 'No se encontró el Código CRI. Error en la celda B%s' % (row + 1)) # Linea Nueva
                continue # Linea Nueva
            line.update({
                'estimated_cri_id': cri_id_id.id,
            })
            
            # Valida que los montos se pueda convertir a flotante
            try:
                line.update({'estimated_january_field': float(estimated_january_field)})
            except:
                set_line_error(sheet, row, 'Formato del monto de Enero es invalido. Error en la celda C%s' % (row + 1))
                continue
            try:
                line.update({'estimated_february_field': float(estimated_february_field)})
            except:
                set_line_error(sheet, row, 'Formato del monto de Febrero es invalido. Error en la celda D%s' % (row + 1))
                continue
            try:
                line.update({'estimated_march_field': float(estimated_march_field)})
            except:
                set_line_error(sheet, row, 'Formato del monto de Marzo es invalido. Error en la celda E%s' % (row + 1))
                continue
            try:
                line.update({'estimated_april_field': float(estimated_april_field)})
            except:
                set_line_error(sheet, row, 'Formato del monto de Abril es invalido. Error en la celda F%s' % (row + 1))
                continue
            try:
                line.update({'estimated_may_field': float(estimated_may_field)})
            except:
                set_line_error(sheet, row, 'Formato del monto de Mayo es invalido. Error en la celda G%s' % (row + 1))
                continue
            try:
                line.update({'estimated_june_field': float(estimated_june_field)})
            except:
                set_line_error(sheet, row, 'Formato del monto de Junio es invalido. Error en la celda H%s' % (row + 1))
                continue
            try:
                line.update({'estimated_july_field': float(estimated_july_field)})
            except:
                set_line_error(sheet, row, 'Formato del monto de Julio es invalido. Error en la celda I%s' % (row + 1))
                continue
            try:
                line.update({'estimated_august_field': float(estimated_august_field)})
            except:
                set_line_error(sheet, row, 'Formato del monto de Agosto es invalido. Error en la celda J%s' % (row + 1))
                continue
            try:
                line.update({'estimated_september_field': float(estimated_september_field)})
            except:
                set_line_error(sheet, row, 'Formato del monto de Septiembre es invalido. Error en la celda K%s' % (row + 1))
                continue
            try:
                line.update({'estimated_october_field': float(estimated_october_field)})
            except:
                set_line_error(sheet, row, 'Formato del monto de Octubre es invalido. Error en la celda L%s' % (row + 1))
                continue
            try:
                line.update({'estimated_november_field': float(estimated_november_field)})
            except:
                set_line_error(sheet, row, 'Formato del monto de Noviembre es invalido. Error en la celda M%s' % (row + 1))
                continue
            try:
                line.update({'estimated_december_field': float(estimated_december_field)})
            except:
                set_line_error(sheet, row, 'Formato del monto de Diciembre es invalido. Error en la celda N%s' % (row + 1))
                continue
            try:
                line.update({'estimated_total': float(estimated_total)})
            except:
                set_line_error(sheet, row, 'Formato del monto Total es invalido. Error en la celda O%s' % (row + 1))
                continue

            # Agrega la linea a la lista de lineas exitosas
            lines_ok.append((0, 0, line)) # Linea Nueva

        if msgs_err: # Linea Nueva
            content = "................... Registros erróneos " + str(datetime.today()) + "...................\r\n" # Linea Nueva
            content += "\r\n".join(msgs_err) # Linea Nueva
            self.father_budget.failed_row_file = base64.b64encode(content.encode('utf-8')) # Linea Nueva
        
        else: # Linea Nueva
            self.father_budget.failed_row_file = None

        self.father_budget.line_budgets = None
        self.father_budget.line_budgets = lines_ok 

        self.father_budget.total_rows = self.record_number
        
        self.father_budget.success_rows = len(self.father_budget.line_budgets)
        
        self.father_budget.import_record_number = len(self.father_budget.line_budgets)
        
        self.father_budget.failed_rows = (self.record_number - len(self.father_budget.line_budgets))
        
        self.father_budget.line_budgets.estimated_from_date = self.father_budget.from_date
        
        self.father_budget.line_budgets.estimated_to_date = self.father_budget.to_date

        if sum_total != self.father_budget.total_budget:
            raise ValidationError(_('La suma de los montos Totales no coincide con la del campo Presupuesto Total. \nFavor de verificar los montos.'))
    

        for row in range(1, sheet.nrows):


            mirror_fiscal_year = sheet.cell_value(row, 0)
            mirror_cri_id = sheet.cell_value(row, 1) # Linea Nueva
            mirror_january_field = sheet.cell_value(row, 2)
            mirror_february_field = sheet.cell_value(row, 3)
            mirror_march_field = sheet.cell_value(row, 4)
            mirror_april_field = sheet.cell_value(row, 5)
            mirror_may_field = sheet.cell_value(row, 6)
            mirror_june_field = sheet.cell_value(row, 7)
            mirror_july_field = sheet.cell_value(row, 8)
            mirror_august_field = sheet.cell_value(row, 9)
            mirror_september_field = sheet.cell_value(row, 10)
            mirror_october_field = sheet.cell_value(row, 11)
            mirror_november_field = sheet.cell_value(row, 12)
            mirror_december_field = sheet.cell_value(row, 13)
            mirror_total = sheet.cell_value(row, 14)

            sum_total += mirror_total

            line = {}

            cri_mirror_id = self.env['classifier.income.item'].search([('name', '=', mirror_cri_id)])

            line.update({
                'mirror_fiscal_year': fiscal_year_id.id,
                'mirror_cri_id': cri_mirror_id.id,
                'mirror_january_field': float(mirror_january_field),
                'mirror_february_field': float(mirror_february_field),
                'mirror_march_field': float(mirror_march_field),
                'mirror_april_field': float(mirror_april_field),
                'mirror_may_field': float(mirror_may_field),
                'mirror_june_field': float(mirror_june_field),
                'mirror_july_field': float(mirror_july_field),
                'mirror_august_field': float(mirror_august_field),
                'mirror_september_field': float(mirror_september_field),
                'mirror_october_field': float(mirror_october_field),
                'mirror_november_field': float(mirror_november_field),
                'mirror_december_field': float(mirror_december_field),
                'mirror_total': float(mirror_total),
            })

            # Agrega la linea a la lista de lineas exitosas
            lines_ok_mirror.append((0, 0, line))
        
        else:
            self.father_budget.failed_row_file = None

        self.father_budget.budget_mirror = None
        self.father_budget.budget_mirror = lines_ok_mirror

        self.father_budget.budget_mirror.mirror_from_date = self.father_budget.from_date

        self.father_budget.budget_mirror.mirror_to_date = self.father_budget.to_date
