from odoo import api, fields, models


class UnamAccountGroup(models.Model):
    _name = 'unam.account.group'
    _description = 'Groups account groups for custom tables of the financial report'

    name = fields.Char(string='Name')
    account_group_ids = fields.Many2many(comodel_name='account.group', string='Account Groups')
    accounts_ids = fields.Many2many('account.account')

    @api.onchange('account_group_ids')
    def _onchange_account_group_ids(self):
        for record in self:
            accounts = self.env['account.account'].search([('group_id', 'child_of', self.account_group_ids.ids)])
            record.accounts_ids = None
            record.accounts_ids = [(4, account.id, 0) for account in accounts]
