from odoo import models, fields, api, _
from odoo.exceptions import RedirectWarning, UserError, ValidationError, AccessError
from datetime import datetime, timedelta, date
import time
import logging
logger = logging.getLogger(__name__)


class ApplicationRequestRejection(models.TransientModel):
	
	_name = 'application.request.rejection'
	_description = 'Application Request Rejection'
	
	
	reason_rejection = fields.Many2one(comodel_name='application.request.rejection.reason', string='Reason for Rejection', help='Reason for Rejection')
	note = fields.Text(string='Notes')
	
	@api.model
	def create(self,vals):
		res = super(ApplicationRequestRejection, self).create(vals)
		return res


	#Función para asociar el motivo de rechazo y la descripción del rechazo con la cuenta por pagar actual
	def rejected(self):
		info = self.env['application.request'].search([('id','=',self.env.context['app_request_info'])])
		info.write({'reason_rejection':self.reason_rejection,'reason_rejection_desc': self.note,'status':self.env.context['next_state']})
		self.env['application.request'].notify_rejected_request(info)