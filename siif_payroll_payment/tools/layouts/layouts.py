from __future__ import annotations
from abc import ABC, abstractmethod
class ResultLine:
    def __init__(self, payment_status: bool, payment_type:str, columns:dict, error_message:str=""):
        self.payment_status = payment_status
        self.error_message = error_message
        self.payment_type = payment_type
        self.res_id = None
        self.columns = columns
class Context:
    _layout = None
    _file = None

    def __init__(self, env, state: Layout) -> None:
        self.transition_to(env, state)

    def transition_to(self, env, state: Layout):
        self._layout = state
        self._layout.env = env

    def read_layout_from_payroll_file(self, payroll_processing_id, file_data):
        lines = self._load_layout(file_data)
        return self.get_ids_search_employee_payroll_file(payroll_processing_id, lines)

    def read_layout_from_payment(self, batch_folio, file_data):
        lines = self._load_layout(file_data)
        return self.get_ids_search_account_payment(batch_folio, lines)

    def _load_layout(self, file_data):
        # Se abre el archivo
        self.load_file(file_data)
        # Se obtiene el body
        body = self.load_body()
        lines = []
        # Se procesan las líneas del body
        for line in body:
            # Se extraen los datos de la línea
            lines.append(self.read_line(line))
        # Se cierra el archivo
        self.close_file()
        return lines

    def load_file(self, file_data):
        return self._layout.load_file(file_data)

    def load_body(self):
        return self._layout.load_body()

    def read_line(self, line):
        return self._layout.read_line(line)

    def close_file(self):
        return self._layout.close_file()

    def get_query_search_employee_payroll_file(self):
        return self._layout.get_query_search_employee_payroll_file()

    def get_ids_search_employee_payroll_file(self, payroll_processing_id, lines):
        return self._layout.get_ids_search_employee_payroll_file(payroll_processing_id, lines)

    def get_ids_search_account_payment(self, batch_folio, lines):
        return self._layout.get_ids_search_account_payment(batch_folio, lines)

class Layout(ABC):
    @property
    def env(self):
        return self._env

    @env.setter
    def env(self, env) -> None:
        self._env = env

    @property
    def data_source_model_id(self):
        return self._data_source_model_id

    @data_source_model_id.setter
    def data_source_model_id(self, data_source_model_id: int) -> int:
        self._data_source_model_id = data_source_model_id

    @property
    def file(self):
        return self._file

    @file.setter
    def file(self, file) -> None:
        self._file = file

    @abstractmethod
    def load_file(self, file_data) -> None:
        pass

    @abstractmethod
    def load_body(self) -> None:
        pass

    @abstractmethod
    def read_line(self, line) -> ResultLine:
        pass

    @abstractmethod
    def close_file(self) -> None:
        pass

    @abstractmethod
    def get_ids_search_employee_payroll_file(self, payroll_processing_id: int, lines: list) -> tuple:
        pass

    @abstractmethod
    def get_ids_search_account_payment(self, batch_folio: str, lines: list) -> tuple:
        pass