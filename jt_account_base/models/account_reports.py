import logging
from odoo import models

GROUP_ACCOUNT_ADMIN = 'jt_account_base.group_account_admin_user'
GROUP_ACCOUNT_TRAINING = 'jt_account_base.group_account_training_user'

class ReportAccountFinancialReport(models.Model):
    _inherit = "account.financial.html.report"

    def _get_reports_buttons(self):
        buttons = super()._get_reports_buttons()
        is_admin_contabilidad = self.env.user.has_group(GROUP_ACCOUNT_ADMIN)
        is_training_contabilidad = self.env.user.has_group(GROUP_ACCOUNT_TRAINING)
        if not (is_admin_contabilidad or is_training_contabilidad):
            buttons = [b for b in buttons if b['name'] != 'Guardar']
        return buttons

class AccountCashFlowReport(models.AbstractModel):
    _inherit = 'account.cash.flow.report'

    def _get_reports_buttons(self):
        buttons = super()._get_reports_buttons()
        is_admin_contabilidad = self.env.user.has_group(GROUP_ACCOUNT_ADMIN)
        is_training_contabilidad = self.env.user.has_group(GROUP_ACCOUNT_TRAINING)
        if not (is_admin_contabilidad or is_training_contabilidad):
            buttons = [b for b in buttons if b['name'] != 'Guardar']
        return buttons

class ReportPartnerLedger(models.AbstractModel):
    _inherit = "account.partner.ledger"

    def _get_reports_buttons(self):
        buttons = super()._get_reports_buttons()
        is_admin_contabilidad = self.env.user.has_group(GROUP_ACCOUNT_ADMIN)
        is_training_contabilidad = self.env.user.has_group(GROUP_ACCOUNT_TRAINING)
        if not (is_admin_contabilidad or is_training_contabilidad):
            buttons = [b for b in buttons if b['name'] != 'Guardar']
        return buttons

class report_account_aged_receivable(models.AbstractModel):
    _inherit = "account.aged.receivable"

    def _get_reports_buttons(self):
        buttons = super()._get_reports_buttons()
        is_admin_contabilidad = self.env.user.has_group(GROUP_ACCOUNT_ADMIN)
        is_training_contabilidad = self.env.user.has_group(GROUP_ACCOUNT_TRAINING)
        if not (is_admin_contabilidad or is_training_contabilidad):
            buttons = [b for b in buttons if b['name'] != 'Guardar']
        return buttons

class report_account_aged_payable(models.AbstractModel):
    _inherit = "account.aged.payable"

    def _get_reports_buttons(self):
        buttons = super()._get_reports_buttons()
        is_admin_contabilidad = self.env.user.has_group(GROUP_ACCOUNT_ADMIN)
        is_training_contabilidad = self.env.user.has_group(GROUP_ACCOUNT_TRAINING)
        if not (is_admin_contabilidad or is_training_contabilidad):
            buttons = [b for b in buttons if b['name'] != 'Guardar']
        return buttons

class report_account(models.AbstractModel):
    _inherit = "account.report"

    def _get_reports_buttons(self):
        buttons = super()._get_reports_buttons()
        is_admin_contabilidad = self.env.user.has_group(GROUP_ACCOUNT_ADMIN)
        is_training_contabilidad = self.env.user.has_group(GROUP_ACCOUNT_TRAINING)
        if not (is_admin_contabilidad or is_training_contabilidad):
            buttons = [b for b in buttons if b['name'] != 'Guardar']
        return buttons


class AccountGenericTaxReport(models.AbstractModel):
    _inherit = "account.generic.tax.report"

    def _get_reports_buttons(self):
        buttons = super()._get_reports_buttons()
        is_admin_contabilidad = self.env.user.has_group(GROUP_ACCOUNT_ADMIN)
        is_training_contabilidad = self.env.user.has_group(GROUP_ACCOUNT_TRAINING)
        if not (is_admin_contabilidad or is_training_contabilidad):
            buttons = [b for b in buttons if b['name'] != 'Asiento contable de cierre ']
        return buttons