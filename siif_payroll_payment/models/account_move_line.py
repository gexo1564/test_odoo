# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from datetime import datetime, date

class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    def insert_sql(self, move_id,  lines, name=None):

        def _get_column_stored_name():
            query = """
                select column_name from information_schema.columns
                where table_schema = 'public'
                and table_name   = 'account_move_line' and column_name <> 'id'
            """
            self.env.cr.execute(query, ())
            return [f'"{res[0]}"' for res in self.env.cr.fetchall()]

        def _get_insert_sql():
            columns = _get_column_stored_name()
            column = ", ".join(columns)
            values = ", ".join(["%s"] * len(columns))
            return f"insert into account_move_line ({column}) values({values}) returning 1", columns

        def _get_insert_params(columns, vals):
            def update_vals(updates):
                for k, v in updates:
                    if k not in vals: vals.update({k: v})

            res = []
            account_id = self.env['account.account'].browse([vals.get('account_id')])
            updates = [
                ('move_name', '/'),
                ('date', date.today()),
                ('parent_state', 'draft'),
                ('company_id', self.env.user.company_id.id),
                ('account_internal_type', account_id.user_type_id.type),
                ('account_root_id', account_id.root_id.id),
                ('sequence', 10),
                ('dicount', 0),
                ('reconciled', False),
                ('blocked', False),
                ('tax_exigible', True),
                ('amount_residual', 0),
                ('amount_residual_currency', 0),
                ('is_for_approved_payment', False),
                ('other_amount', 0),
                ('tax_price_cr', 0),
                ('coa_conac_id', account_id.coa_conac_id.id),
                ('discount', 0),
                ('quantity', 1),
                ('create_date', 'now()'),
                ('write_date', 'now()'),
                ('create_uid', self.env.user.id),
                ('write_uid', self.env.user.id),
            ]
            if name:
                updates.append(('name', name))
            update_vals(updates)
            # Cálculo de los importes
            discount = vals.get('discount', 0)
            quantity = vals.get('quantity', 1)
            price_unit = round(vals.get('price_unit', 0.0), 4)
            price_unit_wo_discount = price_unit * (1 - (discount / 100.0))
            price_subtotal = quantity * price_unit_wo_discount
            # Se coloca el signo igual a 1 dado que se trata de un type in_voice (solicitudes de pago)
            sign = 1
            price_subtotal *= sign
            # Caso de solo una moneda
            vals.update({
                'amount_currency': 0.0,
                'debit': price_subtotal > 0.0 and price_subtotal or 0.0,
                'credit': price_subtotal < 0.0 and -price_subtotal or 0.0,
                'price_subtotal': price_subtotal,
                'price_total': price_subtotal,
                })
            balance = sign * (vals['debit'] - vals['credit'])
            vals.update({
                'price_unit': balance / (quantity or 1.0),
                'balance': balance,
            })
            if vals.get('program_code_id'):
                vals.update({
                    'is_programatic_code': True,
                    'is_ie_account': False,
                })
            if 'currency_id' not in vals:
                vals.update({'company_currency_id': self.env.company.currency_id.id})
            for column in columns:
                res.append(vals.get(column[1:-1], None))
            return tuple(res)

        query, columns = _get_insert_sql()
        params = []
        query_move = """
            select journal_id, partner_id from account_move where id = %s
        """
        self.env.cr.execute(query_move, (move_id,))
        res = self.env.cr.dictfetchone()
        for line in lines:
            line.update({
                'move_id': move_id,
                'journal_id': res['journal_id'],
                'partner_id': res['partner_id'],
            })
            params = _get_insert_params(columns, line)
            self.env.cr.execute(query, params)
            if not self.env.cr.fetchone():
                raise ValidationError("Error al insertar el registro account.move.line")