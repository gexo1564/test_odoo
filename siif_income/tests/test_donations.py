import logging
import unittest
from odoo.exceptions import UserError,ValidationError
from datetime import datetime
from odoo.tests import tagged

from odoo.tests import common
_logger = logging.getLogger(__name__)
msj_rep = [
            "Donativo Prueva 1"  #0
            ]

@tagged('-at_install','post_install','donations','siif_income')
class DonationsTest(common.TransactionCase):
    
    def setUp(self):
        super(DonationsTest,self).setUp()
        self.model = self.env['donations']

    def test_data_create(self):
        test_data_1 = {
                "type_donation_id": 1,
                "depen_trans": "744 Dirección General de Finanzas",
                "sub_depen_trans": "01 Dirección General de Finanzas",
                "emission_date": datetime(2022, 9, 23),
                "expiration_date": datetime(2022, 10, 23),
                "dependancy_id": 145,
                "sub_dependancy_id": 944,
                "benefactor": 86166,
                "benefactor_rfc": "AAMJ531224UZ7",
                "benefactor_email": "mail@mail.com",
                "number_files_referred": 2,
                "total_donation_amount": 600.0,
                "donation_amout_p_token": 300.0,
                "currency_id": 33,
                "agreement": "123561734",
                "excercise": "2022",
                "display_name": "",
                "cfdi_of_donation_pdf": False,
                "rdi": True,
                "name": "",
                "description_donation": msj_rep[0],
                "folio_don": False,
                "observations": False,
                "cfdi_of_donation_xml": False,
                "donation_status": "in_review",
                "is_later_payment": False,
                "documents_sopport_name": False,
                "cfdi_status_don": False,
                "uuid_don": False,
                "cfdi_deposit_token": False,
                "invoice_id": False,
                "series_don": False,
                "check": "1",
                "bank_check": False,
                "limit_value_uma": False,
                "deposit_token": False,
                "deposit_tokens_line_ids": [],
                "cfdi_of_donation_pdf_name": False,
                "fortnight": "10",
                "cfdi_date_don": False,
                "documents_sopport": [],
                "agreement_file": True,
                "name_type": "Donativo en Numerario",
                "letter_N_Considerations": True,
                "donation_concept": msj_rep[0],
                "cfdi_of_donation_xml_name": False,
                "tax_id_of_the_donor": True,
                "deduction_id": False,
            }
        
        test_data_2 = {
                "type_donation_id": 1,
                "depen_trans": "744 Dirección General de Finanzas",
                "sub_depen_trans": "01 Dirección General de Finanzas",
                "emission_date": datetime(2022, 9, 23),
                "expiration_date": datetime(2022, 10, 23),
                "dependancy_id": False,
                "sub_dependancy_id": False,
                "benefactor": 86166,
                "benefactor_rfc": "AAMJ531224UZ7",
                "benefactor_email": "12345",
                "number_files_referred": 0,
                "total_donation_amount": 0,
                "donation_amout_p_token": 0,
                "currency_id": 33,
                "agreement": "123561734",
                "excercise": "20",
                "display_name": "",
                "cfdi_of_donation_pdf": False,
                "rdi": False,
                "name": "",
                "description_donation": msj_rep[0],
                "folio_don": False,
                "observations": False,
                "cfdi_of_donation_xml": False,
                "donation_status": "in_review",
                "is_later_payment": False,
                "documents_sopport_name": False,
                "cfdi_status_don": False,
                "uuid_don": False,
                "cfdi_deposit_token": False,
                "invoice_id": False,
                "series_don": False,
                "check": "1",
                "bank_check": False,
                "limit_value_uma": False,
                "deposit_token": False,
                "deposit_tokens_line_ids": [],
                "cfdi_of_donation_pdf_name": False,
                "fortnight": "10",
                "cfdi_date_don": False,
                "documents_sopport": [],
                "agreement_file": False,
                "name_type": "Donativo en Numerario",
                "letter_N_Considerations": False,
                "donation_concept": msj_rep[0],
                "cfdi_of_donation_xml_name": False,
                "tax_id_of_the_donor": True,
                "deduction_id": False,
            }
        with self.assertRaises(ValidationError and UserError):
            self.env['donations'].create(test_data_2)
        print('------------------wrong data test---------------------------------OK')

        self.assertNotEquals(test_data_1,test_data_2,'Son Iguales')
        print('------------------NotEquals data test-----------------------------OK')


        print('-------------------------Test Data Create Result:-------------------------OK')

    def test_checks_fields(self):

        with self.assertRaises(UserError):
            self.env['donations'].create({"benefactor_email": '123'})
        print('------------wrong data test[benefactor_email=(string123)]----------OK')

        
        with self.assertRaises(UserError):
            self.env['donations'].create({"benefactor_email": 123})
        print('--------------------wrong data test[benefactor_email=123]----------OK')

        with self.assertRaises(ValidationError):
            self.env['donations'].create({"description_donation": False})
        print('--------------wrong data test[description_donation=False]----------OK')

        data_test_1 = self.env['donations'].create({
                                        "type_donation_id": 1,
                                        "tax_id_of_the_donor": False,
                                        "agreement_file": False,
                                        "rdi": False,
                                        "letter_N_Considerations": False,
                                        })
        with self.assertRaises(ValidationError):
            data_test_1._check_documents_support()
        print('---------------------wrong data test[checkboxcase1=Falses]----------OK')
           
        data_test_2 = data_test_1.create({
                                        "type_donation_id": 2,
                                        "tax_id_of_the_donor": False,
                                        "rdi": False,
                                        })
        with self.assertRaises(ValidationError):
            data_test_2._check_documents_support()
        print('---------------------wrong data test[checkbox-case2=Falses]----------OK')

        with self.assertRaises(ValidationError):
            self.env['donations'].create({"donation_concept": False})
        print('--------------------wrong data test[donation_concept=False]----------OK')

        with self.assertRaises(ValidationError):
            self.env['donations'].create({
                                        "dependancy_id": False,
                                        "sub_dependancy_id":False
                                        })
        print('-------wrong data test[dependency and sub-dependency=False]----------OK')

        with self.assertRaises(ValidationError):
            self.env['donations'].create({
                                        "expiration_date": datetime(2022, 9, 22),
                                        "emission_date": datetime(2022, 9, 23),
                                        })
        print('--------------wrong data test[expiration-date<emision-date]----------OK')

        with self.assertRaises(ValidationError):
            self.env['donations'].create({
                                        "type_donation_id": 1,
                                        "number_files_referred": 0,
                                        })
        print('-------------------wrong data test[number_files_referred<=0]----------OK')

        with self.assertRaises(ValidationError):
            self.env['donations'].create({
                                        "type_donation_id": 1,
                                        "is_later_payment":False,
                                        "donation_amout_p_token": 0.0,
                                        })
        print('-------------------wrong data test[donation_amout_p_token<=0]----------OK')

        test_data_3 = self.env['donations'].create({
                                        "type_donation_id": 3,
                                        "check": "Hola"
                                        })
        with self.assertRaises(ValidationError):
            test_data_3._check_check()
        print('-------------------wrong data test[check not is digit]----------OK')

        with self.assertRaises(ValidationError):
            self.env['donations'].create({
                                        "type_donation_id": 3,
                                        "bank_check": False,
                                        })
        print('-------------------wrong data test[banl_check = False]----------OK')

        with self.assertRaises(ValidationError):
            self.env['donations'].create({
                                            "excercise": "abcd"
                                        })
        print('-------------------wrong data test[excercise = abcd]----------OK')

        with self.assertRaises(ValidationError):
            self.env['donations'].create({
                                            "excercise": "0"
                                        })
        print('-------------------wrong data test[excercise = abcd]----------OK')
            

        print('-------------------------Test Check Validation Fields Result:-------------------------OK')
