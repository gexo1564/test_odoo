# -*- coding: utf-8 -*-

import logging
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError

REFUND_CREDIT_NOTE_WIZARD = 'refund.credit.note.wizard'
class RefundCreditNoteWizard(models.TransientModel):
    _name = REFUND_CREDIT_NOTE_WIZARD

    payment_method = fields.Selection(
        [
            ('transfer', 'Transfer'),
            ('check', 'Bank Check'),
        ], "Payment Method", default='',
    )
    customer_ids = fields.Many2many(comodel_name="res.partner",  string="Customers", domain=[
                                    ('contact_type', '=', 'client')])
    credit_note_ids = fields.Many2many(
        'account.move',
        'account_move_refund_credit_note_wizard_rel',
        'refund_credit_note_wizard_id',
        'account_move_id',
        string="Credit Note",
        domain=[('type', '=', 'entry'),
                ('type_of_registry_payment', '=', 'credit_note')])
    message_request = fields.Char('Message')
    reject_reason = fields.Text('Reason for rejection')
    rejected = fields.Boolean(string='Rejected', help='When the information that has been previously registered is incorrect.')
    rejected_bank = fields.Boolean(string='Rejected Bank', help='When the payment cannot be executed by the bank.')


    def open_wizard(self, credit_note):
        self.check_allowed_groups()

        if len(credit_note) > 1:        
            raise ValidationError(_('You can only select one credit note at a time.'))
        
        if credit_note.state == 'draft':
            raise ValidationError(_('Only published credit notes can be added.'))
    
        self.check_cfdi_charged(credit_note)

        if credit_note.credit_note_payment_state in ['in_request', 'paid']:
            related_requests = self.env['refund.credit.note'].search([('credit_note_ids', 'in', credit_note.ids)])
            if related_requests and related_requests[-1].name:
                raise ValidationError(_('The Credit Note is already in the request %s.') % related_requests[-1].name)   
     
        self = self.create({'credit_note_ids': [(6, 0, credit_note.ids)],})
        return self.select_beneficiary()


    def check_allowed_groups(self):
        allowed_groups = [
            'jt_income.group_income_admin_user',
            'jt_income.group_income_sdnc_administrator_user',
            'jt_income.group_income_sdnc_finance_user',
            'jt_income.group_income_sdnc_accounting_user'
        ]
            
        if not any(self.env.user.has_group(group) for group in allowed_groups):
            raise UserError(_("You aren't allowed to perform this action."))


    def check_cfdi_charged(self, credit_note):
        domain = [
            ('cfdi_serie', '=', credit_note.cfdi_serie),
            ('folio_cfdi', '=', credit_note.folio_cfdi),
            ('type', '=', 'out_invoice'),
            ('invoice_payment_state', '!=', 'paid')
        ]
        invoices = self.env['account.move'].search(domain)

        if invoices:
            raise ValidationError(_('The CFDI %s has not been charged.') % invoices.name)

         
    def select_beneficiary(self):

        return {
            'name': _('Select beneficiary'),
            'type': 'ir.actions.act_window',
            'res_model': REFUND_CREDIT_NOTE_WIZARD,
            'view_mode': 'form',
            'view_type': 'form',
            'views': [(self.env.ref("siif_income.refund_credit_note_wizard_view_form").id, 'form')],
            'domain': [],
            'context': {
                'default_credit_note_ids': [(6, 0, self.credit_note_ids.ids)],
            },
            'target': 'new',
        }


    def create_request(self):
        rcn_obj = self.env['refund.credit.note']

        name_seq = self.env['ir.sequence'].next_by_code('refund.credit.note.sequence')
        if not name_seq:
            raise ValidationError(_("The sequence to generate the record name automatically has not been created."))

        vals = {
            'name': name_seq,
            'state': 'draft',
            'payment_method': self.payment_method,
            'customer_ids': [(6, 0, self.customer_ids.ids)],
            'credit_note_ids': [(6, 0, self.credit_note_ids.ids)]
        }

        res_partner_id = self.customer_ids.id
        rpb_obj = self.env["res.partner.bank"].search([("partner_id", "=", res_partner_id)], limit=1)
        if rpb_obj:
            vals.update({
                'bank': rpb_obj.bank_id.id,
                'bank_account': rpb_obj.acc_number,
                'key_bank': rpb_obj.key_bank
            })

        request = rcn_obj.create(vals)
        request.update_credit_note_state('in_request')

        return self.request_message_created(request)


    def request_message_created(self, request):
        message_id = self.env[REFUND_CREDIT_NOTE_WIZARD].create({'message_request': _('Request %s created successfully.') % request.name})
        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': REFUND_CREDIT_NOTE_WIZARD,
            'view_type': 'form',
            'views': [(self.env.ref("siif_income.successful_refund_credit_note_wizard_view_form").id, 'form')],
            'res_id': message_id.id,
            'target': 'new',
            'context': {
                'default_request_id': request.id,
            },
        }


    def go_to_request(self):

        return {
            "type": "ir.actions.act_window",
            "view_mode": "form",
            "res_model": "refund.credit.note",
            "view_type": "form",
            "views": [(False, "form")],
            "view_id": self.env.ref('siif_income.refund_credit_note_view_form').id,
            "res_id": self.env.context.get('default_request_id'),
            "target": "self",
            "context": {
                "active_ids": None
            }
        }


    @api.constrains('customer_ids')  
    def _check_customer_ids(self):
        if not self.customer_ids:
            raise ValidationError(_("Please enter a customer to continue."))


    @api.onchange('customer_ids')
    def _onchangue_customer_id(self):
        if len(self.customer_ids) > 1:
                raise ValidationError(_("You can only add one customer."))        


    def reject(self):
        model = self._context.get('active_model')
        refund_credit_note_model = self.env[model].browse(self._context.get('active_id'))

        reject_reason = self.reject_reason
        refund_credit_note_model.reject(reject_reason)

    
    def reject_paid(self):
        model = self._context.get('active_model')
        refund_credit_note_model = self.env[model].browse(self._context.get('active_id'))
        refund_credit_note_model.rejected = self.rejected
        refund_credit_note_model.rejected_bank = self.rejected_bank
        refund_credit_note_model.update_state('rejected')


    @api.constrains('rejected', 'rejected_bank')
    def _check_rejected_boolean_values(self):
        for record in self:
            true_values = sum([1 for value in [record.rejected, record.rejected_bank] if value])
            if true_values > 1:
                raise ValidationError(_("You can only select one option."))
    
          
    
