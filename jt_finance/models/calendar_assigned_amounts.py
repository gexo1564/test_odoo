# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo.exceptions import ValidationError, UserError
from odoo import models, fields, api, _
from datetime import datetime
import base64
import io, logging

# Define Constant
CALENDAR_NAME = 'calendar.assigned.amounts'
CALENDAR_ASSIGNED_AMOUNTS_LINES = 'calendar.assigned.amounts.lines'
ACCOUNT_MOVE = 'account.move'
FEDERAL_DEPARTURE_FINANCE_MODEL = 'federal.departure.finance'

class CalendarAssignedAmounts(models.Model):

    _name = CALENDAR_NAME
    _description = 'Calendar of assigned amounts'
    _rec_name = 'date'

    def _get_amount(self):
        for record in self:
            record.amount_to_receive = sum(
                record.line_ids.mapped('annual_amount'))
            record.amount_received = sum(
                record.line_ids.mapped('annual_amount_received'))
            record.amount_pending = record.amount_to_receive - record.amount_received

    # Fields for control of amount received
    folio = fields.Char(string='Folio')
    budget_id = fields.Many2one('expenditure.budget', domain=[
                                ('state', '=', 'validate')])
    user_id = fields.Many2one('res.users', string='Made By',
                              default=lambda self: self.env.user, tracking=True, copy=False)
    file = fields.Binary(string='Seasonal File', copy=False)
    filename = fields.Char(string="File Name", copy=False)
    import_date = fields.Date(string='Import Date', copy=False,default=datetime.today())
    obs_cont_amount = fields.Text(string='Observations')
    total_rows = fields.Integer(string='Total Rows', copy=False)
    diff = fields.Text(string='Difference', copy=False)

    # Fields for Calender of assigned amounts
    date = fields.Date(string='Date')
    amount_to_receive = fields.Float(
        string='Amount to receive', compute='_get_amount')
    amount_received = fields.Float(
        string='Amount received', compute='_get_amount')
    amount_pending = fields.Float(
        string='Amount pending', compute='_get_amount')
    obs_calender_amount = fields.Text(string='Observations')
    fiscal_year = fields.Many2one(comodel_name = "account.fiscal.year", string = "Año Fiscal")

    @api.depends('date')
    def _compute_month_int(self):
        for record in self:
            record.month_int = 0
            if record.date:
                record.month_int = record.date.month

    month_int = fields.Integer(string='Month Integer', store=True, compute='_compute_month_int')

    line_ids = fields.One2many(CALENDAR_ASSIGNED_AMOUNTS_LINES,
                               'calendar_assigned_amount_id', string='Calendar of assigned amount lines',domain=[('state', 'in', ('success','manual'))])
    import_line_ids = fields.One2many(CALENDAR_ASSIGNED_AMOUNTS_LINES,
                               'calendar_assigned_amount_id', string='Imported Lines',domain=[('state', 'in', ('draft','fail'))])

    journal_id = fields.Many2one("account.journal", string="Journal")
    state = fields.Selection([('draft', 'Draft'), ('validate', 'Validated')], default='draft',string='Status')
    move_line_ids = fields.One2many('account.move.line', 'calender_id', string="Accounting Notes")
    move_line = fields.One2many(ACCOUNT_MOVE, 'calender_id', string="Accounting Entries")

    import_status = fields.Selection([
        ('draft', 'Draft'),
        ('in_progress', 'In Progress'),
        ('done', 'Completed')], default='draft', copy=False)

    def _compute_total_rows(self):
        for budget in self:
            budget.failed_rows = self.env[CALENDAR_ASSIGNED_AMOUNTS_LINES].search_count(
                [('calendar_assigned_amount_id', '=', budget.id), ('state', '=', 'fail')])
            budget.success_rows = self.env[CALENDAR_ASSIGNED_AMOUNTS_LINES].search_count(
                [('calendar_assigned_amount_id', '=', budget.id), ('state', '=', 'success')])
            budget.total_rows = self.env[CALENDAR_ASSIGNED_AMOUNTS_LINES].search_count(
                [('calendar_assigned_amount_id', '=', budget.id),('state', '!=', 'manual')])

    failed_rows = fields.Integer(
        string='Failed Rows', compute="_compute_total_rows")
    success_rows = fields.Integer(
        string='Success Rows', compute="_compute_total_rows")
    total_rows = fields.Integer(
        string="Total Rows", compute="_compute_total_rows")

    failed_row_file = fields.Binary(string='Failed Rows File')
    fialed_row_filename = fields.Char(
        string='File name', default=lambda self: _("Failed_Rows.txt"))    
    
    
    def unlink(self):
        for calendar in self:
            if calendar.state in ('validate'):
                if self.env.user.lang == 'es_MX':
                    raise ValidationError(_("No puedes eliminar un Registro del Subsidio Federal en estatus Validado."))
                else:
                    raise ValidationError('You cannot delete a Registration of Federal Subsidy in Validated status!')
        return super(CalendarAssignedAmounts, self).unlink()

    @api.model
    def default_get(self, fields):
        res = super(CalendarAssignedAmounts, self).default_get(fields)
        daily_income_jour = self.env.ref('jt_conac.daily_income_jour')
        if daily_income_jour:
            res.update({'journal_id': daily_income_jour.id})
        return res

    def validate_import_line(self):
        if len(self.import_line_ids.ids) > 0:

            counter = 0
            failed_row = ""
            failed_line_ids = []
            success_line_ids = []
            departure_conversion_obj = self.env['departure.conversion'].search_read([], fields=['id', 'federal_part'])
            for line in self.import_line_ids:
                counter += 1
                line_vals = [line.year, line.branch, line.unit, line.purpose, line.function,
                                 line.sub_function, line.program,line.institution_activity, line.project_identification, line.project, line.item_char,
                                 line.expense_type, line.funding_source, line.federal,line.key_wallet ,line.january, line.february,
                                 line.march, line.april, line.may, line.june,line.july,line.august,line.september,line.october,line.november,line.december,line.annual_amount]

                # Validate  Item
                item_id = False
                if line.item_char:
                    item_id = list(filter(lambda prog: prog['federal_part'] == line.item_char, departure_conversion_obj))
                    item_id = item_id[0]['id'] if item_id else False
                if not item_id:
                    failed_row += str(line_vals) + \
                                  "------>> Invalid SHCP Games(CONPA) Format\n"
                    failed_line_ids.append(line.id)
                    continue
                line.item_id = item_id
                success_line_ids.append(line.id)

            failed_lines = self.env[CALENDAR_ASSIGNED_AMOUNTS_LINES].search(
                [('calendar_assigned_amount_id', '=', self.id), ('id', 'in', failed_line_ids)])
            success_lines = self.env[CALENDAR_ASSIGNED_AMOUNTS_LINES].search(
                [('calendar_assigned_amount_id', '=', self.id), ('id', 'in', success_line_ids)])
            success_lines.write({'state': 'success'})
            for l in failed_lines:
                if l.state == 'draft':
                    l.state = 'fail'

            failed_data = False
            vals = {}
            if failed_row != "":
                content = ""
                if self.failed_row_file:
                    file_data = base64.b64decode(self.failed_row_file)
                    content += io.StringIO(file_data.decode("utf-8")).read()
                content += "\n"
                content += "...................Failed Rows " + \
                           str(datetime.today()) + "...............\n"
                content += str(failed_row)
                failed_data = base64.b64encode(content.encode('utf-8'))
                vals['failed_row_file'] = failed_data

            if vals.get('failed_row_file'):
                self.write(vals)
            if self.failed_rows == 0:
                self.import_status = 'done'

    def action_validate_calendar_assing(self):
        self.ensure_one()
        if self.total_rows != self.success_rows:
            raise ValidationError(_("Please validate all import lines!"))
        for line in self.line_ids:
            months_amounts = line.january + line.february+line.march+line.april+line.may+line.june+line.july+line.august+line.september+line.october+line.november+line.december
            if months_amounts != line.annual_amount:
                if self.env.user.lang == 'es_MX':
                    raise ValidationError(_("La cantidad anual ingresada no es correcta."))
                else:
                    raise ValidationError(_("The annual amount entered is not correct."))

        move_obj = self.env[ACCOUNT_MOVE]
        control_amount = self
        journal = control_amount.journal_id
        today = self.fiscal_year.date_from
        user = self.env.user
        partner_id = user.partner_id.id
        amount = sum(control_amount.line_ids.mapped('annual_amount'))
        company_id = user.company_id.id
        cri = '0'
        # Si no esta registrado el CRI, busca el código CRI vinculado a la cuenta contable del diario
        if not cri or cri == '0':
            # Extrae las cuentas contables del diario
            default_credit = journal.default_credit_account_id.id
            default_debit = journal.default_debit_account_id.id
            # Objeto de account account
            obj_account = self.env['relation.account.cri']
            # Busca si el código de la cuenta contable del diario comienza con 4
            cri_account = obj_account.search([('cuentas_id', '=', default_credit),('cuentas_id', 'like', '4%')])
            if not cri_account:
                cri_account = obj_account.search([('cuentas_id', '=', default_debit),('cuentas_id', 'like', '4%')])
                if not cri_account:
                    raise ValidationError(_("No se encontró el CRI vinculado a la cuenta contable del diario!"))
            # Envia todos los parametros para crear el asiento contable
            cri_name = self.env['classifier.income.item'].search([('id', '=', cri_account.cri.id)])
        if not journal.estimated_credit_account_id or not journal.estimated_debit_account_id \
                or not journal.conac_estimated_credit_account_id or not journal.conac_estimated_debit_account_id:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("Por favor configure estimada la cuenta UNAM y CONAC en diario!"))
            else:
                raise ValidationError(_("Please configure Estimated UNAM and CONAC account in journal!"))
        unam_move_val = {'ref': self.folio, 'calender_id': control_amount.id, 'conac_move': True,
                         'date': today, 'journal_id': journal.id, 'company_id': company_id,
                         'line_ids': [(0, 0, {
                             'account_id': journal.estimated_credit_account_id.id,
                             'coa_conac_id': journal.conac_estimated_credit_account_id.id,
                             'credit': amount, 'calender_id': control_amount.id,
                             'partner_id': partner_id,
                             'cri': cri_name.name
                         }), (0, 0, {
                             'account_id': journal.estimated_debit_account_id.id,
                             'coa_conac_id': journal.conac_estimated_debit_account_id.id,
                             'debit': amount, 'calender_id': control_amount.id,
                             'partner_id': partner_id,
                             'cri': cri_name.name
                         })]}
        unam_move = move_obj.create(unam_move_val)
        unam_move.action_post()

        #############################################################################################################
        # Función para agregar el subsidio a las líneas del presupuesto de ingresos si hay presupuesto dado de alta #
        # en estatus de validado, de lo contrarió no agrega la línea                                                #
        #############################################################################################################

        calendar_assigned_lines = self.env[CALENDAR_ASSIGNED_AMOUNTS_LINES].search([('calendar_assigned_amount_id', '=', self.id)])
        for data in calendar_assigned_lines:
            fiscal_year = self.env['account.fiscal.year'].search([('name', '=', data.year)])
        budget_lines = self.env['siif.budget.mgmt.income.budgets.lines'].search([('estimated_fiscal_year', '=', fiscal_year.id)])

        father_calendar = self.env[CALENDAR_NAME].search([('id', '=', calendar_assigned_lines.calendar_assigned_amount_id.id)])
        account_journal = self.env['account.journal'].search([('id', '=', father_calendar.journal_id.id)])

        account_account = self.env['account.account'].search([('id', '=', account_journal.default_credit_account_id.id)])

        account_cri = self.env['relation.account.cri'].search([('cuentas_id', '=', account_account.id)])


        if budget_lines.my_id.state == 'validated':
            if budget_lines.estimated_cri_id != account_cri.cri.id:
                sum_january = 0
                sum_february = 0
                sum_march = 0
                sum_april = 0
                sum_may = 0
                sum_june = 0
                sum_july = 0
                sum_august = 0
                sum_september = 0
                sum_october = 0
                sum_november = 0
                sum_december = 0
                sum_total = 0
                
                for rec in calendar_assigned_lines:
                    sum_january += rec.january
                    sum_february += rec.february
                    sum_march += rec.march
                    sum_april += rec.april
                    sum_may += rec.may
                    sum_june += rec.june
                    sum_july += rec.july
                    sum_august += rec.august
                    sum_september += rec.september
                    sum_october += rec.october
                    sum_november += rec.november
                    sum_december += rec.december
                    sum_total += rec.annual_amount
                lines_budget = []
                lines_budget.append((0,0,{
                    'my_id': budget_lines.my_id.id,
                    'estimated_fiscal_year': budget_lines.estimated_fiscal_year.id,
                    'estimated_from_date': budget_lines.estimated_fiscal_year.date_from,
                    'estimated_to_date': budget_lines.estimated_fiscal_year.date_to,
                    'estimated_cri_id': account_cri.cri.id,
                    'estimated_january_field': sum_january,
                    'estimated_february_field': sum_february,
                    'estimated_march_field': sum_march,
                    'estimated_april_field': sum_april,
                    'estimated_may_field': sum_may,
                    'estimated_june_field': sum_june,
                    'estimated_july_field': sum_july,
                    'estimated_august_field': sum_august,
                    'estimated_september_field': sum_september,
                    'estimated_october_field': sum_october,
                    'estimated_november_field': sum_november,
                    'estimated_december_field': sum_december,
                    'estimated_total': sum_total,

                    'to_collect_january_field': sum_january,
                    'to_collect_february_field': sum_february,
                    'to_collect_march_field': sum_march,
                    'to_collect_april_field': sum_april,
                    'to_collect_may_field': sum_may,
                    'to_collect_june_field': sum_june,
                    'to_collect_july_field': sum_july,
                    'to_collect_august_field': sum_august,
                    'to_collect_september_field': sum_september,
                    'to_collect_october_field': sum_october,
                    'to_collect_november_field': sum_november,
                    'to_collect_december_field': sum_december,
                    'to_collect_total': sum_total,

                    'modified_january_field': sum_january,
                    'modified_february_field': sum_february,
                    'modified_march_field': sum_march,
                    'modified_april_field': sum_april,
                    'modified_may_field': sum_may,
                    'modified_june_field': sum_june,
                    'modified_july_field': sum_july,
                    'modified_august_field': sum_august,
                    'modified_september_field': sum_september,
                    'modified_october_field': sum_october,
                    'modified_november_field': sum_november,
                    'modified_december_field': sum_december,
                    'modified_total': sum_total,

                }))
                self.write({'budget_lines': lines_budget})
            if budget_lines.my_id.state != 'validated' and rec.estimated_cri_id == account_cri.id:
                self.write({'budget_lines': False})
        self.collected()
        self.state = 'validate'

    def import_lines(self):
        return {
            'name': "Import and Validate File",
            'type': 'ir.actions.act_window',
            'res_model': 'import.calendar.assign.amount.lines',
            'view_mode': 'form',
            'view_type': 'form',
            'views': [(False, 'form')],
            'target': 'new',
        }

    company_id = fields.Many2one(comodel_name = "res.company", string = "Compañia", store=False)
    move_id = fields.Many2one(comodel_name = "account.move", string = "Asiento Contable", store=False)
    account_id = fields.Many2one(comodel_name = "account.account", string = "Cuenta", store=False)
    cri = fields.Char(string='CRI', store=False)
    debit = fields.Float(string = "Debe", store=False)
    credit = fields.Float(string = "Haber", store=False)

    def collected(self):
        months = self.env[CALENDAR_ASSIGNED_AMOUNTS_LINES].search([('calendar_assigned_amount_id', '=', self.id)])

        month_attributes = ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december']

        for month in months:
            for attr in month_attributes:
                if getattr(month, attr):
                    month.write({
                        f'collect_{attr}': getattr(month, attr)
                    })


class CalendarAssignedAmountsLines(models.Model):

    _name = CALENDAR_ASSIGNED_AMOUNTS_LINES
    _description = 'Calendar of assigned amounts lines'
    _rec_name = 'date'

    date = fields.Date(string='Deposit date')
    shcp_id = fields.Many2one(
        'shcp.code', string='Budgetary Program')
    month = fields.Selection([
        ('january', 'January'),
        ('february', 'February'),
        ('march', 'March'),
        ('april', 'April'),
        ('may', 'May'),
        ('june', 'June'),
        ('july', 'July'),
        ('august', 'August'),
        ('september', 'September'),
        ('october', 'October'),
        ('november', 'November'),
        ('december', 'December')], string='Month of the amount', default='january')

    bank_id = fields.Many2one('res.bank', string='Bank')
    bank_account_id = fields.Many2one(
        'res.partner.bank', string='Bank account', domain="['|', ('bank_id', '=', False), ('bank_id', '=', bank_id)]")
    observations = fields.Text(string='Observations')
    calendar_assigned_amount_id = fields.Many2one(
        CALENDAR_NAME, string='Calendar of assigned amount',ondelete='cascade')

    currency_id = fields.Many2one(
        'res.currency', default=lambda self: self.env.company.currency_id)

    @api.depends('project_identification','project')
    def get_budgetary_program(self):
        for line in self:
            budgetary_program = ''
            if line.project_identification:
                budgetary_program += line.project_identification
            if line.project:
                budgetary_program += line.project.zfill(3)
            line.budgetary_program = budgetary_program

    #==== New fields ======#

    is_imported = fields.Boolean('Imported',default=False)
    state = fields.Selection([('manual', 'Manual'), ('draft', 'Draft'), (
        'fail', 'Fail'), ('success', 'Success')], string='Status', default='manual')
    is_manual_line = fields.Boolean('Manual Add Line',default=False)

    year = fields.Char('Year',size=4)
    branch = fields.Integer('Branch')
    unit = fields.Char('Unit')
    purpose = fields.Integer('Purpose')
    function = fields.Integer('Function')
    sub_function = fields.Integer('Subfunction')
    program = fields.Char('Program')
    institution_activity = fields.Integer('Institution Activity')
    project_identification = fields.Char('Project Identification',size=1)
    project = fields.Char('Project',size=3)
    budgetary_program = fields.Char(string='Budgetary Program',compute='get_budgetary_program',store=True)
    item_id = fields.Many2one('departure.conversion','Expense Item')
    new_item_id = fields.Many2one(FEDERAL_DEPARTURE_FINANCE_MODEL,'Federal departure')
    item_char = fields.Char('Field_item',size=5)
    expense_type = fields.Integer('Expense Type')
    funding_source = fields.Char('Funding Source')
    federal = fields.Integer('Federal')
    key_wallet = fields.Char('Key Wallet')

    january = fields.Float(string='January')
    amount_modifications_january = fields.Float(satring='Amount Modifications January')
    modifications_january = fields.Float(string='Modifications January')
    amount_deposite_january = fields.Float(string='Amount deposited January')
    collect_january = fields.Float(string='Collect January')

    february = fields.Float(string='February')
    amount_modifications_february = fields.Float(satring='Amount Modifications February')
    modifications_february = fields.Float(string='Modifications February')
    amount_deposite_february = fields.Float(string='Amount deposited February')
    collect_february = fields.Float(string='Collect February')

    march = fields.Float(string='March')
    amount_modifications_march = fields.Float(satring='Amount Modifications March')
    modifications_march = fields.Float(string='Modifications March')
    amount_deposite_march = fields.Float(string='Amount deposited March')
    collect_march = fields.Float(string='Collect March')

    april = fields.Float(string='April')
    amount_modifications_april = fields.Float(satring='Amount Modifications April')
    modifications_april = fields.Float(string='Modifications April')
    amount_deposite_april = fields.Float(string='Amount deposited April')
    collect_april = fields.Float(string='Collect April')

    may = fields.Float(string='May')
    amount_modifications_may = fields.Float(satring='Amount Modifications May')
    modifications_may = fields.Float(string='Modifications May')
    amount_deposite_may = fields.Float(string='Amount deposited May')
    collect_may = fields.Float(string='Collect May')

    june = fields.Float(string='June')
    amount_modifications_june = fields.Float(satring='Amount Modifications June')
    modifications_june = fields.Float(string='Modifications June')
    amount_deposite_june = fields.Float(string='Amount deposited June')
    collect_june = fields.Float(string='Collect June')

    july = fields.Float(string='July')
    amount_modifications_july = fields.Float(satring='Amount Modifications July')
    modifications_july = fields.Float(string='Modifications July')
    amount_deposite_july = fields.Float(string='Amount deposited July')
    collect_july = fields.Float(string='Collect July')

    august = fields.Float(string='August')
    amount_modifications_august = fields.Float(satring='Amount Modifications August')
    modifications_august = fields.Float(string='Modifications August')
    amount_deposite_august = fields.Float(string='Amount deposited August')
    collect_august = fields.Float(string='Collect August')

    september = fields.Float(string='September')
    amount_modifications_september = fields.Float(satring='Amount Modifications September')
    modifications_september = fields.Float(string='Modifications September')
    amount_deposite_september = fields.Float(string='Amount deposited September')
    collect_september = fields.Float(string='Collect September')

    october = fields.Float(string='October')
    amount_modifications_october = fields.Float(satring='Amount Modifications October')
    modifications_october = fields.Float(string='Modifications October')
    amount_deposite_october = fields.Float(string='Amount deposited October')
    collect_october = fields.Float(string='Collect October')

    november = fields.Float(string='November')
    amount_modifications_november = fields.Float(satring='Amount Modifications November')
    modifications_november = fields.Float(string='Modifications November')
    amount_deposite_november = fields.Float(string='Amount deposited November')
    collect_november = fields.Float(string='Collect November')

    december = fields.Float(string='December')
    amount_modifications_december = fields.Float(satring='Amount Modifications December')
    modifications_december = fields.Float(string='Modifications December')
    amount_deposite_december = fields.Float(string='Amount deposited December')
    collect_december = fields.Float(string='Collect December')

    annual_amount = fields.Float(string='Annual Amount')
    modifications_accumulated_annual = fields.Float(string='Modifications Accumulated Annual')
    modifications_annual_amount = fields.Float(string="ModificationsAnnual Amount")

    state_related = fields.Selection(related='calendar_assigned_amount_id.state', string='State Related', store=True)

    @api.depends('amount_deposite_january','amount_deposite_february','amount_deposite_march',
                 'amount_deposite_april','amount_deposite_may','amount_deposite_june',
                 'amount_deposite_july','amount_deposite_august','amount_deposite_september',
                 'amount_deposite_october','amount_deposite_november','amount_deposite_december'
                 )
    def cal_annual_amount_received(self):
        for line in self:
            line.annual_amount_received = line.amount_deposite_january+line.amount_deposite_february+line.amount_deposite_march  \
                                          + line.amount_deposite_april + line.amount_deposite_may + line.amount_deposite_june \
                                          + line.amount_deposite_july + line.amount_deposite_august + line.amount_deposite_september \
                                          + line.amount_deposite_october + line.amount_deposite_november + line.amount_deposite_december

    annual_amount_received = fields.Float(string='Annual Amount Received',compute='cal_annual_amount_received',store=True,copy=False)

    @api.depends('new_item_id')
    def get_item_data(self):
        for line in self:
            if line.new_item_id and line.new_item_id.conversion_key:
                line.item_id_first = line.new_item_id.conversion_key[0]
                if len(line.new_item_id.conversion_key) > 1:
                    line.item_id_second = line.new_item_id.conversion_key[1]

    item_id_first = fields.Char('Item First',compute='get_item_data',store=True)
    item_id_second = fields.Char('Item Second',compute='get_item_data',store=True)

    @api.model
    def create(self, vals):
        vals = self._format_project_code(vals)
        duplicate_domain = self._build_duplicate_domain(vals)

        if self._has_duplicates(duplicate_domain):
            self._handle_duplicates(vals)
       
        self.validate_federal_departure_validity(vals['new_item_id'])

        return super(CalendarAssignedAmountsLines, self).create(vals)


    def validate_federal_departure_validity(self, federal_departure_id):
        conpa = self.env[FEDERAL_DEPARTURE_FINANCE_MODEL].browse(federal_departure_id)
        conpa_validity = self.get_conpa_validity_from_federal_departure_finance(conpa.conversion_key)
        if not conpa_validity:
            raise ValidationError(_("There aren't current dates for %s") % conpa.conversion_key)
     

    def get_conpa_validity_from_federal_departure_finance(self, conversion_key, today = datetime.now()):
        query = """
                SELECT conversion_key
                FROM federal_departure_finance
                WHERE 
                    (
                        (start_date IS NULL AND end_date IS NULL AND conversion_key = %(conversion_key)s) OR
                        (start_date IS NOT NULL AND end_date IS NULL AND conversion_key = %(conversion_key)s AND start_date <= %(today)s) OR
                        (start_date IS NOT NULL AND end_date IS NOT NULL AND conversion_key = %(conversion_key)s AND start_date <= %(today)s AND end_date >= %(today)s)
                    )
                """
        params = {'conversion_key' : conversion_key,
                  'today': today}
        self.env.cr.execute(query, params)
        conpa_validity = self.env.cr.fetchall()

        return conpa_validity

    def _format_project_code(self, vals):
        project_code = vals.get('project', '').zfill(3)
        vals['project'] = project_code
        return vals


    def _build_duplicate_domain(self, vals):

        duplicate_domain = [
            ('year', '=', vals['year']),
            ('branch', '=', vals['branch']),
            ('unit', '=', vals['unit']),
            ('purpose', '=', vals['purpose']),
            ('function', '=', vals['function']),
            ('sub_function', '=', vals['sub_function']),
            ('program', '=', vals['program']),
            ('institution_activity', '=', vals['institution_activity']),
            ('project_identification', '=', vals['project_identification']),
            ('project', '=', vals['project']),
            ('new_item_id', '=', vals['new_item_id']),
            ('expense_type', '=', vals['expense_type']),
            ('funding_source', '=', vals['funding_source']),
            ('federal', '=', vals['federal']),
            ('key_wallet', '=', vals['key_wallet']),
        ]

        return duplicate_domain


    def _has_duplicates(self, duplicate_domain):
        duplicate_count = self.env[CALENDAR_ASSIGNED_AMOUNTS_LINES].search_count(duplicate_domain)

        return duplicate_count


    def _handle_duplicates(self, vals):
        new_item_id = self.env[FEDERAL_DEPARTURE_FINANCE_MODEL].browse(vals['new_item_id'])
        error_message = (
            "La combinación de los siguientes campos ya existe: "
            "Año: {year}, Ramo: {branch}, Unidad: {unit}, "
            "Finalidad: {purpose}, Función: {function}, Subfunción: {sub_function}, "
            "Programa: {program}, ActividadInstitucional: {institution_activity}, "
            "Identificador de Proyecto: {project_identification}, Proyecto: {project}, "
            "Partida de Gasto: {new_item_id_value}, Tipo de Gasto: {expense_type}, "
            "Fuente de Financiamiento: {funding_source}, Entidad Federativa: {federal}, "
            "Clave Cartera: {key_wallet}"
        ).format(**vals, new_item_id_value = new_item_id.conversion_key)

        raise ValidationError(error_message)


    def write(self,vals):
        if vals.get('project',False):
            line_p = vals.get('project',False)
            line_p = str(line_p).zfill(3)
            vals.update({'project':line_p})
        return super(CalendarAssignedAmountsLines,self).write(vals)

class AccountMove(models.Model):

    _inherit = ACCOUNT_MOVE

    calender_id = fields.Many2one(CALENDAR_NAME)

class AccountMoveLine(models.Model):

    _inherit = 'account.move.line'

    calender_id = fields.Many2one(CALENDAR_NAME)
    cri = fields.Char(string = 'CRI')
