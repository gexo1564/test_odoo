# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

from odoo.addons.siif_security.tools.utils import check_authorization, check_only_allowed_fields
from datetime import datetime
class InstitutionalActivity(models.Model):

    _name = 'institutional.activity'
    _description = 'Institutional Activity'
    _rec_name = 'number'

    number = fields.Char(string='Institutional activity number', size=5)
    description = fields.Text(string='Description')

    _sql_constraints = [('number', 'unique(number, start_date, end_date)', 'The number must be unique.')]

    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')

    @api.constrains('start_date', 'end_date')
    def check_end_date(self):
        if not self.start_date:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("Configure la fecha inicial"))
            else:
                raise ValidationError(_("The start date of validity is required"))
        if self.end_date and self.end_date <= self.start_date:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("La fecha de fin de vigencia no puede ser menor o igual a la fecha de inicio de vigencia"))
            else:
                raise ValidationError(_("The effective end date cannot be less than or equal to the effective date"))

    @api.constrains('number')
    def _check_number(self):
        if not str(self.number).isnumeric():
            raise ValidationError(_('The Institutional activity number must be numeric value'))

    def fill_zero(self, code):
        return str(code).zfill(5)

    @api.model
    def create(self, vals):
        if vals.get('number') and len(vals.get('number')) != 5:
            vals['number'] = self.fill_zero(vals.get('number'))
        return super(InstitutionalActivity, self).create(vals)

    @check_authorization(
        'jt_budget_mgmt.group_budget_catalogs_admin_user',
        'jt_budget_mgmt.group_budget_admin_user'
    )
    @check_only_allowed_fields('description', 'start_date', 'end_date')
    def write(self, vals):
        return super(InstitutionalActivity, self).write(vals)

    @check_authorization(
        'jt_budget_mgmt.group_budget_catalogs_admin_user',
        'jt_budget_mgmt.group_budget_admin_user'
    )
    def unlink(self):
        for ai in self:
            program_code = self.env['program.code'].search([('institutional_activity_id', '=', ai.id)], limit=1)
            if program_code:
                raise ValidationError('No puedes eliminar una Actividad Institucional que este ligada a un Código Programático!')
        return super(InstitutionalActivity, self).unlink()

    def validate_institutional_activity(self, institutional_activity_string):
        if len(str(institutional_activity_string)) > 2:
            institutional_activity_str = str(
                institutional_activity_string).zfill(5)
            if institutional_activity_str.isnumeric():
                institutional_activity = self.search(
                    [('number', '=', institutional_activity_str)], limit=1)
                if institutional_activity:
                    return institutional_activity
        return False

    # if not return value is not valid #
    def get_validity_institutional_activity(self, institutional_activity, today = datetime.now()):
        query = """
                SELECT id
                FROM institutional_activity
                WHERE 
                    (
                        (start_date IS NULL AND end_date IS NULL AND number = %(institutional_activity)s) OR
                        (start_date IS NOT NULL AND end_date IS NULL AND number = %(institutional_activity)s AND start_date <= %(today)s) OR
                        (start_date IS NOT NULL AND end_date IS NOT NULL AND number = %(institutional_activity)s AND start_date <= %(today)s AND end_date >= %(today)s)
                    )
                """
        params = {'institutional_activity' : str(institutional_activity),
                  'today': today}
        self.env.cr.execute(query, params)
        institutional_activity_validity = self.env.cr.fetchall()

        return institutional_activity_validity