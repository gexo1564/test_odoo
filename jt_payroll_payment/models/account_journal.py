import logging
from lxml import etree
from odoo import models, fields, api

class AccountJournal(models.Model):

    _inherit = 'account.journal'

    payroll_bank_format = fields.Selection([('santander','SANTANDER'),
                                    ('santander_h2h', 'SANTANDER H2H'),
                                    ('hsbc','HSBC'),
                                    ('bbva_nomina','BBVA BANCOMER NOMINA'),
                                    ('bbva_232','BBVA BANCOMER DISPERSION 232'),
                                    ('banamex','Banamex'),
                                    ('scotiabank','SCOTIABANK'),
                                    ('banorte','BANORTE'),
                                    ('inbursa','INBURSA'),
                                    ],string="Layout generation for payroll payments")
    
    payroll_load_bank_format = fields.Selection([('santander','SANTANDER'),
                                        ('santander_h2h', 'SANTANDER H2H'),
                                        ('hsbc','HSBC'),
                                        ('bbva_nomina','BBVA BANCOMER NOMINA'),
                                        ('bbva_232','BBVA BANCOMER DISPERSIÓN 232'),
                                        ('banamex','BANAMEX'),
                                        ('scotiabank','SCOTIABANK'),
                                        ('banorte','BANORTE'),
                                        ('inbursa','INBURSA'),
                                        ],string="Load bank layout for payroll")

    payroll_beneficiaries_bank_format = fields.Selection([('BANORTE','BANORTE')],string="Registration of Payroll beneficiaries")

    support_documentation_line_ids = fields.One2many('bank.support.documentation','bank_journal_id','Bank support documentation')

class BankSupportDocumentation(models.Model):
    _name = 'bank.support.documentation'
    _description = "Bank support documentation"

    document = fields.Binary("Document")
    name =  fields.Char("Name")
    date = fields.Date("State account date")
    exercise =  fields.Char("Exercise")
    bank_journal_id = fields.Many2one('account.journal',string='Documentation',ondelete='cascade')
