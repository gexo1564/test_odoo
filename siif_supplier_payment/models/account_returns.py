import logging
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from datetime import date, datetime

from odoo.addons.siif_supplier_payment.tools.utils import Dictionary

"""
    Variables Globales
"""
ACC_RETURN='account.returns'
ACC_MOVE='account.move'
MAIL_THREAD='mail.thread'
IR_SEQ='ir.sequence'
PAYMENT_PARAMETERS='payment.parameters'
IR_ACTION='ir.actions.act_window'
class AccountReturns(models.Model):
    _name = ACC_RETURN
    _description = 'Account Returns'
    _inherit = [MAIL_THREAD, 'mail.activity.mixin']
    name = fields.Char(string="Name",tracking=True)
    def _default_year(self):
        return self.env['year.configuration'].search([('name', '=', date.today().year)], limit=1).id
    year = fields.Many2one('year.configuration',string="Exercise",default=_default_year,tracking=True)
    #Cuenta contable
    account_move_id = fields.Many2one(ACC_MOVE, string="Account Move Paid", domain=[('payment_state','in',('paid','with_returns')),('type','=','in_invoice')], required=True,tracking=True)
    #Dependencia y SUbdependencia
    dependency_id = fields.Many2one(string="Dependency",related='account_move_id.dependancy_id',store=True)
    sub_dependency_id = fields.Many2one(string="Subdependency",related='account_move_id.sub_dependancy_id',store=True)
    #Moneda
    currency_id = fields.Many2one('res.currency',string='Currency',required=True,related='account_move_id.currency_id')

    state = fields.Selection(
    [('draft','Draft'),
        ('published','Published'),
        ('reject_correction','Reject for Correction'),
        ('approved','Approved'),
        ('rejected','Rejected'),
        ('done','Done'),
        ('cancelled','Cancelled'),
    ],string='Status',default='draft',tracking=True)
    #Archivos de justificación
    justify_files = fields.Many2many("ir.attachment",'account_returns_justify_attachment_rel', string="Add Justify Files")
    support_files = fields.Many2many("ir.attachment",'account_returns_support_attachment_rel', string="Add Support Files")

    #Linas de devolución
    acc_return_lines = fields.One2many("account.returns.line", 'account_returns_line_ids', string="Return Lines",tracking=True)
    #Monto total para devolución
    amount_return = fields.Float(string="Amount Return")
    #Si tiene lineas de devolución
    has_return_lines = fields.Boolean(default=False)
    folio = fields.Char(string="Folio")

    #Firmas electrónicas Dependencia
    feu_signature_dep = fields.Many2one("sidia.university_signature", string="Firma del Usuario de la dependencia", store=True, tracking= True)
    qr_signature_dep = fields.Binary(related="feu_signature_dep.qr_image", string="Firma del Usuario de la dependencia", readonly=True)
    signature_name_dep = fields.Text(related="feu_signature_dep.signature_name", string="Nombre", readonly=True, store=True)
    curp_feu_dep = fields.Char(related="feu_signature_dep.curp", string="CURP", readonly=True , store=True)
    date_feu_dep = fields.Datetime(related="feu_signature_dep.create_date", string="Fecha firma", readonly=True)

    #Motivos de devolución
    info_return = fields.Char(compute='_get_info_account',store=False)
    reasons_for_return = fields.Text(string="Reasons for Return")
    nota = fields.Text(string="Concepto ",compute="_get_info_base")
    observations = fields.Text(string="Observations",tracking=True)

    #Usuario de registro
    user_id = fields.Many2one('res.users','User register',default=lambda self: self.env.user,tracking=True)

    #Referencia bancaria
    bank_ref = fields.Many2one('bank.reference',string='Bank Reference',tracking=True)

    acc_move_lines = fields.One2many("account.move.line", 'account_move_returns_id', string="Account Return Move",tracking=True)

    bank_move_id = fields.Many2one('bank.movements',string="Movimiento Bancario")

    certificate_type = fields.Many2one('deposit.certificate.type',string="Certificate Type", domain=[('account_return_catalog','=','True')],required=True)
    #==============================================================================
    #              Revisión de información
    #==============================================================================

    @api.constrains('justify_files')
    def _check_count_justify_files(self):
        if len(self.justify_files)==0:
            raise UserError("No se adjuntaron documentos de justificación, Agregalos a la solicitud.")

    @api.constrains('support_files')
    def _check_count_support_files(self):
        if len(self.support_files)==0:
            raise UserError("No se adjuntaron documentos soporte, Agregalos a la solicitud.")

    @api.depends('account_move_id')
    def _get_info_account(self):
        for rec in self:
            if rec.account_move_id:
                rec.info_return='DEV. DE LA ORDEN DE PAGO %s' % rec.account_move_id.folio
            else:
                rec.info_return=""
    @api.depends('info_return','reasons_for_return')
    def _get_info_base(self):
        for rec in self:
            if rec.info_return and rec.reasons_for_return:
                rec.nota=rec.info_return+" DEBIDO A "+str(rec.reasons_for_return).upper()
            else:
                rec.nota=""

    #==============================================================================
    #               Funciones de Creación
    #==============================================================================

    #Función de creación
    @api.model
    def create(self, vals):
        if vals.get('account_move_id'):
            vals.update({'name':self.env[IR_SEQ].next_by_code(ACC_RETURN)})
            res = super(AccountReturns, self).create(vals)
            return res
        else:
            raise UserError('No se ha seleccionado una solicitud de pago. Revisar.')

    #Bottat solicitudes de devolución
    def unlink(self):
        for rec in self:
            if rec.state not in ['draft']:
                raise UserError(_('You cannot delete an entry which has been not draft state.'))
            else:
                for line in rec.acc_return_lines:
                    line.unlink()
                return super(AccountReturns, self).unlink()
    #Obtener calculo de devolución total
    def get_returned_money(self,move_line):
        returned=0
        line_returns=self.env['account.returns.line'].search([('acc_move_line_id','=',move_line.id),('selected_line','=','True'),('account_returns_line_ids.state','not in',('rejected','cancelled'))])
        for line in line_returns:
            returned+=line.price_return
        return returned
    #Generar lineas de devolución por medio de las solicitudes
    def _get_return_lines(self):
        lines=[]
        for line in self.account_move_id.invoice_line_ids:
            vals={
                'egress_key_id':line.egress_key_id.id,
                'program_code_id':line.program_code_id.id or False,
                'account_id':line.account_id.id or False,
                'account_ie':line.account_ie.id or False,
                'dependency_id':line.dependency_id.id or False,
                'sub_dependency_id':line.sub_dependency_id.id or False,
                'quantity':line.quantity,
                'price_unit':line.price_unit,
                'price_subtotal':line.price_subtotal,
                'price_return':0,
                'returned_money':self.get_returned_money(line),
                'acc_move_line_id':line.id,
            }
            lines.append(vals)
        logging.critical(lines)
        return lines
    #======================================================================
    #                       Acciones Vista
    #======================================================================

    #Función para generar las lineas de devolución
    def action_generate_returns(self):
        return {
            'name':'Selección de lineas de devolución',
            'type': IR_ACTION,
            'res_model': 'select.return.lines',
            'view_mode': 'form',
            'context': {
                'id_return':self.id,
                'default_account_returns': self.id,
                'default_return_lines': self._get_return_lines(),
                'edition':False,
            },
            'target': 'new'
        }
    #Función para seleccionar las lineas de devolución
    def action_generate_returns_edit(self):
        return {
            'name':'Selección de lineas de devolución',
            'type': IR_ACTION,
            'res_model': 'select.return.lines',
            'view_mode': 'form',
            'context': {
                'id_return':self.id,
                'default_account_returns': self.id,
                'default_return_lines': self.acc_return_lines.ids,
                'edition':True,
            },
            'target': 'new'
        }
    #Función para publicar la solicitud
    def action_for_publish(self):
        if (self.qr_signature_dep!=False) or (self.env[PAYMENT_PARAMETERS].get_param('flag_feu')):
            if self.has_return_lines and self.state in ('draft','reject_correction'):
                self.state='published'
                self.account_move_id.payment_state='request_return'
            else:
                raise UserError('No se tiene las lineas seleccionadas para la devolución. Revisar')
        else:
            raise UserError('Se necesita registrar la firma electrónica para continuar el proceso. Favor de firmar.')
    #Función para aprobar la solicitud
    def action_for_approve(self):
        if self.state=='published':
            self.state='approved'
            self.env[ACC_RETURN].notify_request(self,'approve')
    #Función que lanza 
    def action_for_documentation(self):
        if self.state=='published':
            return {
            'name': _('Rechazo de solicitud de devolución'),
            'type': IR_ACTION,
            'res_model': 'account.return.rejection',
            'view_mode': 'form',
            'view_type': 'form',
            'context': {
                'acc_return_info': self.id,
                'type':'correction'
            },
            'views': [(self.env.ref("siif_supplier_payment.account_return_correction_form").id, 'form')],
            'target': 'new'
            }
    #Botón para rechazar
    def action_reject(self):
        if self.state=='published':
            return {
            'name': _('Rechazo de solicitud de devolución'),
            'type': IR_ACTION,
            'res_model': 'account.return.rejection',
            'view_mode': 'form',
            'view_type': 'form',
            'context': {
                'acc_return_info': self.id,
                'type':'rejection'
            },
            'views': [(self.env.ref("siif_supplier_payment.account_return_rejection_form").id, 'form')],
            'target': 'new'
            }
    #Botón para cancelar
    def action_for_cancel(self):
        if self.state=='reject_correction':
            self.state='cancelled'
            if self.env[ACC_RETURN].search_count([('account_move_id','=',self.account_move_id.id),('state','=','done')])==0:
                self.account_move_id.payment_state='paid'
            else:
                self.account_move_id.payment_state='with_returns'
            self.env[ACC_RETURN].notify_request(self,'cancel')
    #Generar referencia bancaria
    def generate_bank_ref(self):
        if self.state=='approved':
            if self.env[PAYMENT_PARAMETERS].get_param('last_exercise'):
                if self.folio==False:
                    self.folio=self.env[IR_SEQ].next_by_code('account.returns.bank.ref')
                return {
                'name': _('Generar Referencia Bancaria'),
                'type': IR_ACTION,
                'res_model': 'wiz.bank.reference',
                'view_mode': 'form',
                'view_type': 'form',
                'context': {
                    'model':'account_return',
                    'default_reference':self.env['bank.reference']._gen_short_ref(self.account_move_id.dependancy_id,
                                                                                self.account_move_id.sub_dependancy_id,
                                                                                self.folio,
                                                                                'money_return'),
                    'id_request': self.id,
                    'default_date': date.today(),
                    'default_amount':self.amount_return,
                },
                'views': [(False, 'form')],
                'target': 'new'
                }
    #Generar los nombres
    def _gen_name_return(self,folio):
        if self.env[PAYMENT_PARAMETERS].get_param('journal_return'):
            journal=self.env[PAYMENT_PARAMETERS].get_param('journal_return')
            name=journal.code+self.env[IR_SEQ].next_by_code('account.returns.publish')
            return name

    def _return_budget(self):
        lines = Dictionary()
        for line in self.acc_return_lines.filtered(lambda l: l.program_code_id and l.selected_line):
            # Monto a devolver
            lines.add_sum(line.program_code_id.id, line.price_return)
        # Devuelve el presupuesto disponible de la cuenta por pagar
        self.env['expenditure.budget.line'].return_available(lines.items(), datetime.today(), self.name)

    def _revert_account_lines(self,bank_move):
        # Se obtiene la poliza de los apuntes contables
        self.get_account_lines(bank_move)
       

    def return_paid(self,bank_move):
        # Devolución del presupuesto
        self._return_budget()
        # Reverción de los apuntes contables
        self._revert_account_lines(bank_move)
        self.account_move_id.payment_state='with_returns'
        self.state='done'
        self.bank_move_id=bank_move

    #====================================================================================
    #           Asientos contables de devoluciones
    #====================================================================================
    #Obtener información de cuenta
    def _get_account_info_id(self,account):
        return account and account.id or False
    
    def _check_dep_subdep_flag(self,dep_subdep,account_id):
        return dep_subdep.id if account_id.dep_subdep_flag else False
    #Asignar primera linea de asiento contable
    def _assign_first_line(self,lines,bank_move,move_id,nombre_desc):
        lines.append((0, 0, {
                'account_id': self._get_account_info_id(bank_move.income_bank_journal_id.default_debit_account_id),#Cuenta del banco
                'coa_conac_id': self._get_account_info_id(bank_move.income_bank_journal_id.conac_debit_account_id),#cuenta de conac debito
                'debit': self.amount_return,
                'conac_move' : True,
                'partner_id': self.account_move_id.partner_id.id,
                'dependency_id': self._check_dep_subdep_flag(move_id.dependancy_id, bank_move.income_bank_journal_id.default_debit_account_id),
                'sub_dependency_id': self._check_dep_subdep_flag(move_id.sub_dependancy_id,bank_move.income_bank_journal_id.default_debit_account_id),
                'exclude_from_invoice_tab':True,
                'account_move_returns_id':self.id,
                'name':nombre_desc,
            }))
    #Gastos de operacion
    def _assign_line_gastos_operacion(self,lines,amount,line,nombre_desc):
        lines.append((0, 0, {
            'account_id': self._get_account_info_id(line.program_code_id.item_id.unam_account_id),
            'coa_conac_id': self._get_account_info_id(line.program_code_id.item_id.cog_id),
            'credit': amount,
            'conac_move' : True,
            'partner_id': self.account_move_id.partner_id.id,
            'program_code_id': line.program_code_id.id,
            
            'exclude_from_invoice_tab':True,
            'account_move_returns_id':self.id,
            'name':nombre_desc,
            
            }))
    
    #Ṕresupuesto asignado
    def _assign_pa_lines(self,lines,line,journal,amount,nombre_desc):
        
        self._assign_line_gastos_operacion(lines,amount,line,nombre_desc)
            # Cuentas Presupuestales
        lines.append((0, 0, {
            'account_id': self._get_account_info_id(journal.default_credit_account_id),
            'coa_conac_id': self._get_account_info_id(journal.conac_credit_account_id),
            'debit': amount,
            'conac_move' : True,
            'partner_id': self.account_move_id.partner_id.id,
            'dependency_id':self._check_dep_subdep_flag(line.program_code_id.dependency_id,journal.default_credit_account_id),
            'sub_dependency_id':self._check_dep_subdep_flag(line.program_code_id.sub_dependency_id,journal.default_credit_account_id),
            'program_code_id': line.program_code_id.id,
            
            'exclude_from_invoice_tab':True,
            'account_move_returns_id':self.id,
            'name':nombre_desc,
            }))
        lines.append((0, 0, {
            'account_id': self._get_account_info_id(journal.default_debit_account_id),
            'coa_conac_id': self._get_account_info_id(journal.conac_debit_account_id),
            'credit': amount,
            'conac_move' : True,
            'partner_id': self.account_move_id.partner_id.id,
            'dependency_id':self._check_dep_subdep_flag(line.program_code_id.dependency_id,journal.default_debit_account_id),
            'sub_dependency_id':self._check_dep_subdep_flag(line.program_code_id.sub_dependency_id,journal.default_debit_account_id),
            'program_code_id': line.program_code_id.id,
            
            'exclude_from_invoice_tab':True,
            'account_move_returns_id':self.id,
            'name':nombre_desc,
            }))
        lines.append((0, 0, {
            'account_id': self._get_account_info_id(journal.accured_credit_account_id),
            'coa_conac_id': self._get_account_info_id(journal.conac_accured_credit_account_id),
            'debit': amount,
            'conac_move' : True,
            'partner_id': self.account_move_id.partner_id.id,
            'dependency_id':self._check_dep_subdep_flag(line.program_code_id.dependency_id,journal.accured_credit_account_id),
            'sub_dependency_id':self._check_dep_subdep_flag(line.program_code_id.sub_dependency_id,journal.accured_credit_account_id),
            'program_code_id': line.program_code_id.id,
            
            'exclude_from_invoice_tab':True,
            'account_move_returns_id':self.id,
            'name':nombre_desc,
            }))
        lines.append((0, 0, {
            'account_id': self._get_account_info_id(journal.accured_debit_account_id),
            'coa_conac_id': self._get_account_info_id(journal.conac_accured_debit_account_id),
            'credit': amount,
            'conac_move' : True,
            'partner_id': self.account_move_id.partner_id.id,
            'dependency_id':self._check_dep_subdep_flag(line.program_code_id.dependency_id,journal.accured_debit_account_id),
            'sub_dependency_id':self._check_dep_subdep_flag(line.program_code_id.sub_dependency_id,journal.accured_debit_account_id),
            'program_code_id': line.program_code_id.id,
            
            'exclude_from_invoice_tab':True,
            'account_move_returns_id':self.id,
            'name':nombre_desc,
            }))
        lines.append((0, 0, {
            'account_id': self._get_account_info_id(journal.execercise_credit_account_id),
            'coa_conac_id': self._get_account_info_id(journal.conac_exe_credit_account_id),
            'debit': amount,
            'conac_move' : True,
            'partner_id': self.account_move_id.partner_id.id,
            'dependency_id':self._check_dep_subdep_flag(line.program_code_id.dependency_id,journal.execercise_credit_account_id),
            'sub_dependency_id':self._check_dep_subdep_flag(line.program_code_id.sub_dependency_id,journal.execercise_credit_account_id),
            'program_code_id': line.program_code_id.id,
            
            'exclude_from_invoice_tab':True,
            'account_move_returns_id':self.id,
            'name':nombre_desc,
            }))
        lines.append((0, 0, {
            'account_id': self._get_account_info_id(journal.execercise_debit_account_id),
            'coa_conac_id': self._get_account_info_id(journal.conac_exe_debit_account_id),
            'credit': amount,
            'conac_move' : True,
            'partner_id': self.account_move_id.partner_id.id,
            'dependency_id':self._check_dep_subdep_flag(line.program_code_id.dependency_id,journal.execercise_debit_account_id),
            'sub_dependency_id':self._check_dep_subdep_flag(line.program_code_id.sub_dependency_id,journal.execercise_debit_account_id),
            'program_code_id': line.program_code_id.id,
            
            'exclude_from_invoice_tab':True,
            'account_move_returns_id':self.id,
            'name':nombre_desc,
            }))
    
    #Ingreso Extraordinario
    def _assign_ie_lines(self,lines,line,journal,amount,nombre_desc):
        account_ie = line.account_ie.ie_account_line_ids.filtered(lambda x:x.main_account == True).account_id
        self._assign_line_gastos_operacion(lines,amount,line,nombre_desc)
            # Pasivo Diferido
        lines.append((0, 0, {
            'account_id': self._get_account_info_id(account_ie),
            'coa_conac_id': False,
            'credit': amount,
            'conac_move' : False,
            'partner_id': self.account_move_id.partner_id.id,
            'dependency_id': self._check_dep_subdep_flag(line.program_code_id.dependency_id,account_ie),
            'sub_dependency_id': self._check_dep_subdep_flag(line.program_code_id.sub_dependency_id,account_ie),
            
            'exclude_from_invoice_tab':True,
            'account_move_returns_id':self.id,
            'name':nombre_desc,
            }))
            # Ingresos
        lines.append((0, 0, {
            'account_id': self._get_account_info_id(account_ie.revenue_recognition_account_id),
            'coa_conac_id': False,
            'debit': amount,
            'conac_move' : False,
            'partner_id': self.account_move_id.partner_id.id,
            'dependency_id':self._check_dep_subdep_flag(line.program_code_id.dependency_id,account_ie.revenue_recognition_account_id),
            'sub_dependency_id':self._check_dep_subdep_flag(line.program_code_id.sub_dependency_id,account_ie.revenue_recognition_account_id),
            
            'exclude_from_invoice_tab':True,
            'account_move_returns_id':self.id,
            'name':nombre_desc,
            }))
            # Cuentas de Ingresos
        lines.append((0, 0, {
            'account_id': self._get_account_info_id(journal.accrued_income_debit_account_id),
            'coa_conac_id': self._get_account_info_id(journal.conac_accrued_income_debit_account_id),
            'credit': amount,
            'conac_move' : True,
            'partner_id': self.account_move_id.partner_id.id,
            'dependency_id':self._check_dep_subdep_flag(line.program_code_id.dependency_id,journal.accrued_income_debit_account_id),
            'sub_dependency_id':self._check_dep_subdep_flag(line.program_code_id.sub_dependency_id,journal.accrued_income_debit_account_id),
            
            'exclude_from_invoice_tab':True,
            'account_move_returns_id':self.id,
            'name':nombre_desc,
            }))
        lines.append((0, 0, {
            'account_id': self._get_account_info_id(journal.accrued_income_credit_account_id),
            'coa_conac_id': self._get_account_info_id(journal.conac_accrued_income_credit_account_id),
            'debit': amount,
            'conac_move' : True,
            'partner_id': self.account_move_id.partner_id.id,
            'dependency_id':self._check_dep_subdep_flag(line.program_code_id.dependency_id,journal.accrued_income_credit_account_id),
            'sub_dependency_id':self._check_dep_subdep_flag(line.program_code_id.sub_dependency_id,journal.accrued_income_credit_account_id),
            
            'exclude_from_invoice_tab':True,
            'account_move_returns_id':self.id,
            'name':nombre_desc,
            }))
        lines.append((0, 0, {
            'account_id': self._get_account_info_id(journal.recover_income_debit_account_id),
            'coa_conac_id': self._get_account_info_id(journal.conac_recover_income_debit_account_id),
            'credit': amount,
            'conac_move' : True,
            'partner_id': self.account_move_id.partner_id.id,
            'dependency_id':self._check_dep_subdep_flag(line.program_code_id.dependency_id,journal.recover_income_debit_account_id),
            'sub_dependency_id':self._check_dep_subdep_flag(line.program_code_id.sub_dependency_id,journal.recover_income_debit_account_id),
            
            'exclude_from_invoice_tab':True,
            'account_move_returns_id':self.id,
            'name':nombre_desc,
            }))
        lines.append((0, 0, {
            'account_id': self._get_account_info_id(journal.recover_income_credit_account_id),
            'coa_conac_id': self._get_account_info_id(journal.conac_recover_income_credit_account_id),
            'debit': amount,
            'conac_move' : True,
            'partner_id': self.account_move_id.partner_id.id,
            'dependency_id':self._check_dep_subdep_flag(line.program_code_id.dependency_id,journal.recover_income_credit_account_id),
            'sub_dependency_id':self._check_dep_subdep_flag(line.program_code_id.sub_dependency_id,journal.recover_income_credit_account_id),
            
            'exclude_from_invoice_tab':True,
            'account_move_returns_id':self.id,
            'name':nombre_desc,
            }))
            # Cuentas Presupuestales
        lines.append((0, 0, {
            'account_id': self._get_account_info_id(journal.default_credit_account_id),
            'coa_conac_id': self._get_account_info_id(journal.conac_credit_account_id),
            'debit': amount,
            'conac_move' : True,
            'partner_id': self.account_move_id.partner_id.id,
            'dependency_id':self._check_dep_subdep_flag(line.program_code_id.dependency_id,journal.default_credit_account_id),
            'sub_dependency_id':self._check_dep_subdep_flag(line.program_code_id.sub_dependency_id,journal.default_credit_account_id),
            'program_code_id': line.program_code_id.id,
            
            'exclude_from_invoice_tab':True,
            'account_move_returns_id':self.id,
            'name':nombre_desc,
            }))
        lines.append((0, 0, {
            'account_id': self._get_account_info_id(journal.default_debit_account_id),
            'coa_conac_id': self._get_account_info_id(journal.conac_debit_account_id),
            'credit': amount,
            'conac_move' : True,
            'partner_id': self.account_move_id.partner_id.id,
            'dependency_id':self._check_dep_subdep_flag(line.program_code_id.dependency_id,journal.default_debit_account_id),
            'sub_dependency_id':self._check_dep_subdep_flag(line.program_code_id.sub_dependency_id,journal.default_debit_account_id),
            'program_code_id': line.program_code_id.id,
            
            'exclude_from_invoice_tab':True,
            'account_move_returns_id':self.id,
            'name':nombre_desc,
            }))
        lines.append((0, 0, {
            'account_id': self._get_account_info_id(journal.accured_credit_account_id),
            'coa_conac_id': self._get_account_info_id(journal.conac_accured_credit_account_id),
            'debit': amount,
            'conac_move' : True,
            'partner_id': self.account_move_id.partner_id.id,
            'dependency_id':self._check_dep_subdep_flag(line.program_code_id.dependency_id,journal.accured_credit_account_id),
            'sub_dependency_id':self._check_dep_subdep_flag(line.program_code_id.sub_dependency_id,journal.accured_credit_account_id),
            'program_code_id': line.program_code_id.id,
            
            'exclude_from_invoice_tab':True,
            'account_move_returns_id':self.id,
            'name':nombre_desc,
            }))
        lines.append((0, 0, {
            'account_id': self._get_account_info_id(journal.accured_debit_account_id),
            'coa_conac_id': self._get_account_info_id(journal.conac_accured_debit_account_id),
            'credit': amount,
            'conac_move' : True,
            'partner_id': self.account_move_id.partner_id.id,
            'dependency_id':self._check_dep_subdep_flag(line.program_code_id.dependency_id,journal.accured_debit_account_id),
            'sub_dependency_id':self._check_dep_subdep_flag(line.program_code_id.sub_dependency_id,journal.accured_debit_account_id),
            'program_code_id': line.program_code_id.id,
            
            'exclude_from_invoice_tab':True,
            'account_move_returns_id':self.id,
            'name':nombre_desc,
            }))
        lines.append((0, 0, {
            'account_id': self._get_account_info_id(journal.execercise_credit_account_id),
            'coa_conac_id': self._get_account_info_id(journal.conac_exe_credit_account_id),
            'debit': amount,
            'conac_move' : True,
            'partner_id': self.account_move_id.partner_id.id,
            'dependency_id':self._check_dep_subdep_flag(line.program_code_id.dependency_id,journal.execercise_credit_account_id),
            'sub_dependency_id':self._check_dep_subdep_flag(line.program_code_id.sub_dependency_id,journal.execercise_credit_account_id),
            'program_code_id': line.program_code_id.id,
            
            'exclude_from_invoice_tab':True,
            'account_move_returns_id':self.id,
            'name':nombre_desc,
            }))
        lines.append((0, 0, {
            'account_id': self._get_account_info_id(journal.execercise_debit_account_id),
            'coa_conac_id': self._get_account_info_id(journal.conac_exe_debit_account_id),
            'credit': amount,
            'conac_move' : True,
            'partner_id': self.account_move_id.partner_id.id,
            'dependency_id':self._check_dep_subdep_flag(line.program_code_id.dependency_id,journal.execercise_debit_account_id),
            'sub_dependency_id':self._check_dep_subdep_flag(line.program_code_id.sub_dependency_id,journal.execercise_debit_account_id),
            'program_code_id': line.program_code_id.id,
            
            'exclude_from_invoice_tab':True,
            'account_move_returns_id':self.id,
            'name':nombre_desc,
            }))
    
    #Cuenta contable
    def _assign_cc_lines(self,lines,line,amount,nombre_desc):
        lines.append((0, 0, {
            'account_id': self._get_account_info_id(line.account_id),
            'coa_conac_id': False,
            'credit': amount,
            'conac_move' : False,
            'partner_id': self.account_move_id.partner_id.id,
            'dependency_id': self._check_dep_subdep_flag(line.dependency_id,line.account_id),
            'sub_dependency_id': self._check_dep_subdep_flag(line.sub_dependency_id,line.account_id),
            
            'exclude_from_invoice_tab':True,
            'account_move_returns_id':self.id,
            'name':nombre_desc,
            }))
    #Cuenta por pagar, provisiones
    def _assign_cxp_lines(self,lines,line,amount,nombre_desc):
        move_id = self.account_move_id
        if line.program_code_id:
            line_cxp=move_id.previous_number.invoice_line_ids.filtered(lambda l:l.program_code_id==line.program_code_id)
            line_cxp[0].amount_used-=line.price_return
        else:
            line_cxp=move_id.previous_number.invoice_line_ids.filtered(lambda l:l.account_id==line.account_id)
            line_cxp[0].amount_used-=line.price_return
        move_id.previous_number.available+=line.price_return
        lines.append((0, 0, {
            'account_id': self._get_account_info_id(move_id.previous_number.type_provision.account_for_balance_on_account_payable_id),
            'coa_conac_id': False,
            'credit': amount,
            'conac_move' : False,
            'partner_id': self.account_move_id.partner_id.id,
            'dependency_id': self._check_dep_subdep_flag(move_id.dependancy_id,move_id.previous_number.type_provision.account_for_balance_on_account_payable_id),
            'sub_dependency_id': self._check_dep_subdep_flag(move_id.sub_dependancy_id,move_id.previous_number.type_provision.account_for_balance_on_account_payable_id),
            
            'exclude_from_invoice_tab':True,
            'account_move_returns_id':self.id,
            'name':nombre_desc,
        }))

    #Asignar asientos contables del ejercicio actual
    def _assign_actual_lines(self,lines,resource_origin,line,amount,nombre_desc):
        
        journal = self.account_move_id.journal_id
        # Linea de Presupuesto Asignado
        if resource_origin == 'PA':
            self._assign_pa_lines(lines,line,journal,amount,nombre_desc)
        # Línea de Ingresos Extraordinarios
        elif resource_origin in ('IEMN', 'IEME'):
            self._assign_ie_lines(lines,line,journal,amount,nombre_desc)
        # Linea de Cuenta Contable
        elif resource_origin == 'CC':
            self._assign_cc_lines(lines,line,amount,nombre_desc)
        #Cuentas por pagar
        elif resource_origin in ('CPN', 'CPE'):
            self._assign_cxp_lines(lines,line,amount,nombre_desc)


    #Asignar lines finales del asiento contable
    def _assign_last_lines(self,lines,journal,nombre_desc):
        for line in self.acc_return_lines.filtered(lambda l: l.selected_line and l.program_code_id):
            lines.append((0, 0, {
                'account_id': journal.paid_credit_account_id.id,
                'coa_conac_id': journal.conac_paid_credit_account_id.id,
                'debit': line.price_return,
                'conac_move' : True,
                'partner_id': self.account_move_id.partner_id.id,
                'dependency_id':self._check_dep_subdep_flag(line.program_code_id.dependency_id,journal.paid_credit_account_id),
                'sub_dependency_id':self._check_dep_subdep_flag(line.program_code_id.sub_dependency_id,journal.paid_credit_account_id),
                'program_code_id': line.program_code_id.id,
                'exclude_from_invoice_tab':True,
                'account_move_returns_id':self.id,
                'name':nombre_desc,
            }))
            lines.append((0, 0, {
                'account_id': journal.paid_debit_account_id.id,
                'coa_conac_id': journal.conac_paid_debit_account_id.id,
                'credit': line.price_return,
                'conac_move' : True,
                'partner_id': self.account_move_id.partner_id.id,
                'dependency_id':self._check_dep_subdep_flag(line.program_code_id.dependency_id,journal.paid_debit_account_id),
                'sub_dependency_id':self._check_dep_subdep_flag(line.program_code_id.sub_dependency_id,journal.paid_debit_account_id),
                'program_code_id': line.program_code_id.id,
                'exclude_from_invoice_tab':True,
                'account_move_returns_id':self.id,
                'name':nombre_desc,
            }))
    
    #Asignar asiento contable para devoluciones
    def _assign_last_exercise_lines(self,lines,bank_move,move_id,nombre_desc):
        lines.append((0, 0, {
                'account_id': bank_move.income_bank_journal_id.default_debit_account_id.id,
                'coa_conac_id': move_id.partner_id.property_account_payable_id.coa_conac_id and move_id.partner_id.property_account_payable_id.coa_conac_id.id or False,
                'debit': self.amount_return,
                'conac_move' : True,
                'partner_id': self.account_move_id.partner_id.id,
                'dependency_id': self._check_dep_subdep_flag(move_id.dependancy_id,bank_move.income_bank_journal_id.default_debit_account_id),
                'sub_dependency_id': self._check_dep_subdep_flag(move_id.sub_dependancy_id,bank_move.income_bank_journal_id.default_debit_account_id),
                'exclude_from_invoice_tab':True,
                'account_move_returns_id':self.id,
                'name':nombre_desc,
            }))
        ejercicio_anterior=self.env[PAYMENT_PARAMETERS].get_param('last_exercise')
        lines.append((0, 0, {
            'account_id': ejercicio_anterior.id,
            'coa_conac_id': move_id.partner_id.property_account_payable_id.coa_conac_id and move_id.partner_id.property_account_payable_id.coa_conac_id.id or False,
            'credit': self.amount_return,
            'conac_move' : True,
            'partner_id': self.account_move_id.partner_id.id,
            'dependency_id': self._check_dep_subdep_flag(move_id.dependancy_id,ejercicio_anterior),
            'sub_dependency_id': self._check_dep_subdep_flag(move_id.sub_dependancy_id,ejercicio_anterior),
            'exclude_from_invoice_tab':True,
            'account_move_returns_id':self.id,
            'name':nombre_desc,
        }))


    #Lineas de devolución
    def get_account_lines(self,bank_move):
        lines = []
        move_id = self.account_move_id
        journal = self.account_move_id.journal_id
        journal_ret = self.env[PAYMENT_PARAMETERS].get_param('journal_return')
        nombre_desc=self.name+' Referencia: '+self.bank_ref.reference+' Folio del Pago: '+self.account_move_id.folio
        if self.year.name==str(date.today().year):
            self._assign_first_line(lines,bank_move,move_id,nombre_desc)
            
            for line in self.acc_return_lines.filtered(lambda l: l.selected_line):
                amount = line.price_return
                resource_origin = line.egress_key_id.key
                self._assign_actual_lines(lines,resource_origin,line,amount,nombre_desc)

            self._assign_last_lines(lines,journal,nombre_desc)

            account_ids = set([a[2]['account_id'] for a in lines])
            account_sorted = {}
            cr = self._cr
            cr.execute("""select id from account_account where id in %s order by code""",(tuple(account_ids),))
            i=0   
            for f in cr.fetchall():
                account_sorted[f[0]]=i
                i+=1
            
            lines.sort(key=lambda l:account_sorted[l[2]['account_id']])
           
        else:
            #Lineas de ejercicios anteriores
            self._assign_last_exercise_lines(lines,bank_move,move_id,nombre_desc)
    

        unam_move_val = {
            'ref': self.name,
            'conac_move': True,
            'dependancy_id' : self.account_move_id.dependancy_id and self.account_move_id.dependancy_id.id or False,
            'sub_dependancy_id': self.account_move_id.sub_dependancy_id and self.account_move_id.sub_dependancy_id.id or False,
            'date': datetime.now(),
            'journal_id': journal_ret.id,
            'company_id': self.env.user.company_id.id,
            'line_ids': lines
        }
        # Creación de los apuntes contables
        move_obj = self.env[ACC_MOVE]
        unam_move = move_obj.create(unam_move_val).with_context(check_move_validity=False)
        unam_move.action_post()
    


    #Envio de notificaciones al usuario de registro
    def notify_request(self,request,type):
        #Envio de mensaje por rechazo por documentación
        mail_thread=self.env[MAIL_THREAD]
        if type=='doc':
            mail_thread.inbox_message(
                "Se rechazó para corrección la devolución de pago ID: %s " % request.name,
                "Ha sido rechazada para corregir la solicitud de devolución %s, con solicitud de pago con folio: %s \n Motivo de rechazo: %s. Favor de revisarlo" 
                % (request.name,request.account_move_id.folio,request.observations),
                [request.user_id])
        #Envio de mensaje por rechazo de la solicitud
        elif type == 'reject':
            mail_thread.inbox_message(
                "Se rechazó la devolución de pago ID: %s " % request.name,
                "Ha sido rechazada la solicitud de devolución %s, con solicitud de pago con folio: %s \n Motivo de rechazo: %s. Favor de revisarlo" 
                % (request.name,request.account_move_id.folio,request.observations),
                [request.user_id])
        #Envio de mensaje por cancelación de solicitud
        elif type=='cancel':
            mail_thread.inbox_message(
                "Se canceló la devolución de pago ID: %s" % request.name,
                "Ha sido cancelado la solicitud de devolución %s, con solicitud de pago con folio: %s \n Observaciones: %s. Favor de revisarlo" 
                % (request.name,request.account_move_id.folio,request.observations),
                [request.user_id])
        #Envio de mensaje por aprobación de solicitud de devolución
        elif type=="approve":
            mail_thread.inbox_message(
                "Se aprobó la devolución de pago ID: %s" % request.name,
                "Ha sido aprobada la solicitud de devolución %s, con solicitud de pago con folio: %s \n Se puede continuar con el proceso" 
                % (request.name,request.account_move_id.folio),
                [request.user_id])
