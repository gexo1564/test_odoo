# -*- coding: utf-8 -*-
import logging
from odoo import models, fields, api, _


class AccountMove(models.Model):
    _inherit = 'account.move'

    ### Campos para las notas de credito ###
    # Generales #
    exercise_year = fields.Integer('Exercise Year')
    serie_note = fields.Char('Serie Note')
    folio_note = fields.Integer('Folio Note')
    serie_csd = fields.Char('No. Serie CSD')

    voucher_effect = fields.Char('Voucher Effect')
    cfdi_use = fields.Char('CFDI Use')
    rate_fee = fields.Char('Rate or Fee')
    type_tax = fields.Char('Type of Tax')
    expedition_place = fields.Integer('Expedition Place')
    # folio_cfdi
    # dependendancy_id
    # sub_dependancy_id
    date_time_emission = fields.Char('Date Register')
    folio_uuid = fields.Char('UUID Fiscal Folio')
    origin = fields.Char('Origin')
    cfdi_version = fields.Char('CFDI Version')
    pac = fields.Char('Supplier Who Signs')
    status_conta_cfd = fields.Selection(selection=[
                                            ('A', 'COMPLETAR PÓLIZA'),
                                            ('C', 'CONCILIADO'),
                                            ('E', 'ERROR EN INFORMACIÓN CARGA EN BANCOS'),
                                            ('G', 'SIN CONCILIAR FACTURA GLOBAL'),
                                            ('K', 'PROCESO DE GENERACIÓN DE PÓLIZA'),
                                            ('P', 'CONCILIACIÓN PARCIAL'),
                                            ('S', 'SIN CONCILIAR FACTURA SUSTITUCIÓN'),
                                            ('W', 'PROCESO ESPECIAL DE REVISIÓN EN BANCOS'),
                                            ], string='CFDI Accounting Statement')
    date_status_conta_cfd = fields.Char('CFDI Accounting Status Date')
    policy_number = fields.Integer('Policy Number')
    policy_date_cfd = fields.Char('')
    policy_cancellation_number = fields.Integer('Policy Cancellation Number')
    policy_date_cfd_canc = fields.Char('')
    # observations
    status_note = fields.Selection(selection=[('C', 'EN CONFIRMACIÓN DE CANCELACIÓN'),
                                              ('D', 'DESACTIVADO'),
                                              ('E', 'ERROR'),
                                              ('G', 'GENERADO'),
                                              ('P', 'PENDIENTE DE GENERAR'),
                                              ('R', 'RECHAZADO'),
                                              ('S', 'SOLICITUD DE CANCELACIÓN'),
                                              ('T', 'ENVIADO AL SERVICIO DEL PAC'),
                                              ('X', 'PENDIENTE DE CANCELAR'),
                                              ('Z', 'CANCELADO'),
                                              ], string='Status Note')
    folio_cfs = fields.Char('Folio CFS')

    type_credit_note = fields.Selection(selection=[('DP', 'DEVOLUCIÓN PARCIAL'),
                                                   ('DS', 'DESCUENTO'),
                                                   ('DV', 'DEVOLUCIÓN TOTAL'),
                                                   ], string='Type Credit Note')
    
    conciliation_statement = fields.Selection(selection=[('DP', 'DEVOLUCIÓN PARCIAL'),
                                                         ('DS', 'DESCUENTO'),
                                                         ('DV', 'DEVOLUCIÓN TOTAL'),
                                                         ], string='Conciliation Statement')                                                                                         

    conciliation_statement_date = fields.Char('Conciliation Statement Date')
    
    cancellation_reason_code = fields.Selection(selection=[
        ('01', 'Comprobante emitido con errores con relación'),
        ('02', 'Comprobante emitido con errores sin relación'),
        ('03', 'No se llevó a cabo la operación'),
        ('04', 'Operación nominativa relacionada en una factura global'),
    ], string='Cancellation Reason Code')

    amount_note = fields.Monetary('Amount Note')
    payment_method_note = fields.Char('Payment Key', comodel_name='l10n_mx_edi.payment.method')

    # Receptor #
    # rfc
    receiver_name = fields.Char('Receiver')
    key_prod_serv = fields.Char('KeyProdServ')
    quantity = fields.Float('Quantity')
    key_unit = fields.Char('Key Unit')
    unit = fields.Char('Unit')
    description_detail = fields.Char('Description and Detail')
    unit_price = fields.Monetary('Unit Price')

    # Seccion Impuestos #
    iva = fields.Char('IVA')
    amount_total_iva = fields.Monetary('Amount IVA')
    base_iva = fields.Monetary('Base IVA')
    tax_code = fields.Integer('Tax Code')
    type_factor = fields.Char('Type Factor')
    discount = fields.Monetary('Discount')
    discount_amount = fields.Monetary('Discount Amount')
    amount_total = fields.Monetary('Amount Total')
    afectation_account_id = fields.Many2one('association.distribution.ie.accounts', "Afectation Account") 
    tax_cincom = fields.Monetary('')
    tax_seisp = fields.Monetary('')
    administrative_expenses = fields.Monetary('')
    payment_form_note = fields.Many2one(string="Payment Key CFDI",comodel_name='l10n_mx_edi.payment.method')
    
    # Sección documentos relacionados
    folio_uuid_related = fields.Char('Related UUID Fiscal Folio')
    relation_type = fields.Selection(selection=[
        ('01', 'Nota de crédito de los documentos relacionados'),
        ('02', 'Notas de Débito de los Documentos Relacionados'),
        ('03', 'Devolución de Mercancías sobre Facturas o Traslados Previos'),
        ('04', 'Sustitución de los CFDI Previos'),
        ('05', 'Traslados de Mercancías Facturados Previamente'),
        ('06', 'Factura Generada por los Traslados Previos'),
        ('07', 'CFDI por Aplicación de Anticipo'),
        ('08', 'Facturas Generadas por Pagos en Parcialidades'),
        ('09', 'Factura Generada por Pagos Diferidos'),
    ], string='Relation Type')