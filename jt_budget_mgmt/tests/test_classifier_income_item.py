# -*- coding: utf-8 -*-
import logging
import unittest
from odoo.exceptions import UserError, ValidationError
from datetime import datetime
from odoo.tests import common, tagged
_logger = logging.getLogger(__name__)

# Define constant
CLASSIFIER_INCOME_ITEM = 'classifier.income.item'

@tagged('-at_install','post_install','classifier_income_item','jt_budget_mgmt')
class ClassifierIncomeItemTest(common.TransactionCase):
	
    # Prepara entorno de pruebas
    def setUp(self):
        super(ClassifierIncomeItemTest,self).setUp()
        self.model = self.env[self.CLASSIFIER_INCOME_ITEM]

    # Test a los constraint de tamaño del catalogo de clasificador por rubro de ingresos
    def test_cri_constraint_size(self):
        obj_cri = self.env[self.CLASSIFIER_INCOME_ITEM]

        # Crear registro con un tamaño no valido
        with self.assertRaises(Exception):
            obj_cri.create({
                'item' : '123',
                'type' : '456',
                'class' : '789',
                'concept':'0'
            })
            print("----------[test_cri_constraint]-----[Tamaño invalido]-----[OK]----------")   

    # Test a los constraint de valor valido del catalogo de clasificador por rubro de ingresos
    def test_cri_constraint_value(self):
        obj_cri = self.env[self.CLASSIFIER_INCOME_ITEM]
               
        # Crear registro con valores invalidos
        with self.assertRaises(Exception):
            obj_cri.create({
                'item' : 'a',
                'type' : 'b',
                'class' : 'c',
                'concept':'d'
            })
            print("----------[test_cri_constraint]-----[Valores invalidos]-----[OK]----------")

    # Test al constraint de registro unico del catalogo de clasificador por rubro de ingresos
    def test_cri_exist(self):
        obj_cri = self.env[self.CLASSIFIER_INCOME_ITEM]

        # Crea registros de ejemplo para probar duplicados
        self.env[self.CLASSIFIER_INCOME_ITEM].create({
            'name': '999999',
            'cuentas_id': '9',
            'cri': '9',
            'start_date': '9',
            'end_date': '9'
        })

        # Crear registro duplicado
        with self.assertRaises(Exception) as e:
            obj_cri.create({
                'name': '999999',
                'cuentas_id': '9',
                'cri': '9',
                'start_date': '9',
                'end_date': '9'
            })
            print("----------[test_cri_constraint]-----[Registro duplicado]-----[OK]----------")
        # Verifica que la excepción se haya lanzado correctamente
        self.assertEqual(e.exception.name, 'Clasificador por Rubro de ingresos debe ser único.')
