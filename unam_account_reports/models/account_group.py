from odoo import api, fields, models, _


class AccountGroup(models.Model):
    _name = 'account.group'
    _inherit = ['account.group', 'excel.detailed.information']

    def _compute_excel_display_name(self):
        for record in self:
            record.excel_display_name = '%s - %s' % (record.code_prefix, record.name)
