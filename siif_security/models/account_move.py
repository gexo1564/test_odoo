from re import X
from lxml import etree
from odoo import models, api
from odoo.http import request
import logging
class AccountMove(models.AbstractModel):
	_inherit = 'account.move'

	@api.model
	def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
		def hide_buttons(node, value):
			node.set('create', value)
			node.set('import', value)
			node.set('export_xlsx', value)
			node.set('delete', value)
			node.set('edit', value)
			node.set('export', value)

		res = super().fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
		doc = etree.XML(res['arch'])
		
		is_cd_user_finanzas = self.env.user.has_group('jt_income.group_income_cd_d_finance_user')
		is_cd_user_contabilidad = self.env.user.has_group('jt_income.group_income_cd_accounting_user')
		is_cd_admin = self.env.user.has_group('jt_income.group_income_cd_administrator')
		is_account_register_user = self.env.user.has_group('jt_account_base.group_account_register_user')
		is_account_approval_user = self.env.user.has_group('jt_account_base.group_account_approval_user')
		
  		#Evitar los enlaces para los grupos de Certificados de Depósito
		if (is_cd_user_finanzas or is_cd_user_contabilidad or is_cd_admin):
			for node in doc.xpath("//field"):
				node.set('options','{"no_open":true}')
				
		# Módulo de Ingresos
		if '.income' in res['name']:
			# ADMIN DE INGRESOS
			is_group_income_admin_user = self.env.user.has_group('jt_income.group_income_admin_user')
			# Arbol de certificados de depositos  
			if 'account.dep_cert.tree.income' in res['name']:
				for node in doc.xpath("//" + view_type):
						node.set('create', '1')
			# Arbol de facturacion
			if 'account.invoice.tree.income' in res['name']:
				if not is_group_income_admin_user:
					for node in doc.xpath("//" + view_type):
						node.set('create', '0')
						node.set('delete', '0')
			# Arbol de intereses bancarios
			# if 'account.bank.interest.tree.income' in res['name']:
			# 	pass
		
			# Validación de Usuarios
			is_admin_ingresos = self.env.user.has_group('jt_income.group_income_admin_user')
			is_cd_user_finanzas = self.env.user.has_group('jt_income.group_income_cd_finance_user')
			is_cd_user_contabilidad = self.env.user.has_group('jt_income.group_income_cd_accounting_user')
			is_cd_admin = self.env.user.has_group('jt_income.group_income_cd_administrator')
			
			# Ocultar sub menu print en Certificados de Depósito
			if (is_cd_user_finanzas or is_cd_user_contabilidad or is_cd_admin):
				items_to_hide = [
					"Facturas sin pago",
					"Facturas",
					"Facturas originales",
					"Facturas si cobro",
					"Informe contra recibo",
					"Solicitud Proveedor",
					"Papeleta de Deposito Bancario",
					"FORMATO DE APLICACIÓN 20%",
					"FORMATO DE CONDONACIÓN",
					"FORMATO DE AVISO CAMBIO FORMA DE COBRO A TRANSFERENCIA",
					"2° FORMATO DE APLICACIÓN 20%",
					"FORMATO DE CONDONACIÓN 20%",
					"Solicitud Viáticos",
					"Solicitud Prácticas Escolares",
					"Solicitud Trabajos de Campo",
					"Solicitud de Intercambio",
					"FORMATO DE NOTIFICACIÓN DE CHEQUE DEVUELTO",
					"Solicitud reembolso a terceros",
					"Papeleta de Entrega de Cheques"
				]
				if 'toolbar' in res and 'print' in res:
					print_items = filter(
						lambda x: x['display_name'] not in items_to_hide,
						res['toolbar']['print']
					)
					res['toolbar']['print'] = list(print_items)

			# Ocultar sub menu accion en Certificados de Depósito
			if (is_cd_user_finanzas or is_cd_user_contabilidad or is_cd_admin):
				items_to_hide = [
					"Registrar Cobro/Pago",
					"Invertir",
					"Generar un enlace de pago"
				]
				if is_cd_user_contabilidad:
					items_to_hide.append('Suprimir'),
					items_to_hide.append('Enviar e imprimir'),
					items_to_hide.append('Compartir')
				if is_cd_user_finanzas:
					for node in doc.xpath("//" + view_type):
						node.set('delete','0')
						# node.set('create','0')
				if 'toolbar' in res and 'action' in res:
					action_items = filter(
						lambda x: x['display_name'] not in items_to_hide,
						res['toolbar']['action']
					)
					res['toolbar']['action'] = list(action_items)

			if not (is_admin_ingresos or is_cd_user_finanzas or is_cd_admin): #Evitar ocultar los botones para esos grupos
				# Aplica las siguientes vistas:
				#   - Ingresos:
				#       - Facturacion Digital
				#       - Certificados de Deposito
				#       - Intereses Bancarios
				#       - Servicios de Educación
				#       - Notas de Credito
				for node in doc.xpath("//" + view_type):
					# Ocultar botones de las vistas Odoo
					hide_buttons(node, '0')
					# Mostrar botones para CD - usuario de contabilidad 
					if is_cd_user_contabilidad :
						node.set('edit', '1')
						node.set('export_xlsx', '1')
						node.set('export', '1')

					
							
					
					if not (is_cd_user_finanzas or is_cd_user_contabilidad or is_cd_admin):	#Evitar ocultar los botones accion e imprimir para los grupos de certificados de deposito				
						if view_type == 'form':
							
							# Facturación digital
							# Permite ver las pólizas
							is_group_income_fac_administrator_user = self.env.user.has_group('jt_income.group_income_fac_administrator_user')
							is_group_income_fac_finance_user = self.env.user.has_group('jt_income.group_income_fac_finance_user')
							is_group_income_fac_accounting_user = self.env.user.has_group('jt_income.group_income_fac_accounting_user')
							if is_group_income_fac_administrator_user or is_group_income_fac_finance_user or is_group_income_fac_accounting_user:
								prints_to_show = ["Póliza de diario",
												   "Póliza de diario CONAC",
												]
								if res.get('toolbar'):				
									if res['toolbar'].get('print'):
										print_items = filter(lambda x: x['display_name'] in prints_to_show,
										res['toolbar']['print']
										)
										res['toolbar']['print'] = list(print_items)

							# Solicitud de devolución por nota de crédito
							is_group_income_sdnc_administrator_user = self.env.user.has_group('jt_income.group_income_sdnc_administrator_user')
							is_group_income_sdnc_finance_user = self.env.user.has_group('jt_income.group_income_sdnc_finance_user')
							is_group_income_sdnc_accounting_user = self.env.user.has_group('jt_income.group_income_sdnc_accounting_user')

							if (is_group_income_sdnc_administrator_user or is_group_income_sdnc_finance_user or is_group_income_sdnc_accounting_user):
								prints_to_show = ["Póliza de diario",

								]
								actions_to_show = ["Devolución por nota de crédito",
								]
								if res.get('toolbar'):
									if res['toolbar'].get('print'):
										print_items = filter(lambda x: x['display_name'] in prints_to_show,
										res['toolbar']['print']
										)
										res['toolbar']['print'] = list(print_items)

									if res['toolbar'].get('action'):
										action_items = filter(lambda x: x['display_name'] in actions_to_show,
										res['toolbar']['action']
										)
										res['toolbar']['action'] = list(action_items)

							#! Se ocultan todas las opciones de impresion en el formulario			
							else:
								# Ocultar submenus Print
								# Ocultar Combobox de Imprimir y Accion
								if res.get('toolbar'):
									if res['toolbar'].get('action'):
										res['toolbar']['action'] = []
									if res['toolbar'].get('print'):
										res['toolbar']['print'] = []

			# Facturación digital
			is_group_income_fac_administrator_user = self.env.user.has_group('jt_income.group_income_fac_administrator_user')
			is_group_income_fac_finance_user = self.env.user.has_group('jt_income.group_income_fac_finance_user')
			is_group_income_fac_accounting_user = self.env.user.has_group('jt_income.group_income_fac_accounting_user')
		
			if (is_group_income_fac_administrator_user or is_group_income_fac_finance_user or is_group_income_fac_accounting_user):
				
				prints_to_hide = [
					"Facturas",
					"Facturas originales",
					"Facturas si cobro",
					"Solicitud Proveedor",
					"Papeleta de Deposito Bancario",
					"FORMATO DE APLICACIÓN 20%",
					"FORMATO DE CONDONACIÓN",
					"FORMATO DE AVISO CAMBIO FORMA DE COBRO A TRANSFERENCIA",
					"2° FORMATO DE APLICACIÓN 20%",
					"FORMATO DE CONDONACIÓN 20%",
					"FORMATO DE NOTIFICACIÓN DE CHEQUE DEVUELTO",
					"Solicitud Viáticos",
					"Solicitud Prácticas Escolares",
					"Solicitud Trabajos de Campo",
					"Solicitud reembolso a terceros",
					"Solicitud de Intercambio",
					"Papeleta de Entrega de Cheques",
				]

				actions_to_hide = [
					"Registrar Cobro/Pago",
					"Invertir",
					"Generar un enlace de pago",
				]

				if res.get('toolbar'):
					if res['toolbar'].get('print'):
						print_items = filter(lambda x: x['display_name'] not in prints_to_hide,
							res['toolbar']['print']
							)
						res['toolbar']['print'] = list(print_items)	

					if res['toolbar'].get('action'):
						action_items = filter(lambda x: x['display_name'] not in actions_to_hide, 
						res['toolbar']['action']
						)
						res['toolbar']['action'] = list(action_items)	

							
				for node in doc.xpath("//field"):
					node.set('options','{"no_open":true}')

					
			if (is_group_income_fac_accounting_user or is_group_income_fac_administrator_user):
				actions_to_hide = [
					"Enviar e imprimir"
				]

				if res.get('toolbar'):
					if res['toolbar'].get('action'):
						action_items = filter(lambda x: x['display_name'] not in actions_to_hide,
						res['toolbar']['action']
						)
						res['toolbar']['action'] = list(action_items)

		# Módulo de Contabilidad
		elif '.accounting.module' in res['name'] or '.conac' in res['name']:
			# Validación de Usuarios
			is_super_contabilidad = self.env.user.has_group('jt_account_base.group_account_super_admin_user')
			is_admin_contabilidad = self.env.user.has_group('jt_account_base.group_account_admin_user')
			is_training_contabilidad = self.env.user.has_group('jt_account_base.group_account_training_user')
			is_register_contabilidad = self.env.user.has_group('jt_account_base.group_account_register_user')
			# Aplica para las siguientes:
			#   - Clientes:
			#       - Facturas
			#       - Facturas Rectificadas
			#       - Recibos
			#   - Provedores:
			#       - Facturas
			#       - Notas de Credito
			#       - Recibos
			if '.invoice' in res['name']:
				if not is_super_contabilidad:
					if not (is_admin_contabilidad or is_training_contabilidad):
						for node in doc.xpath("//" + view_type):
							node.set('create', '0')
							node.set('import', '0')
							node.set('edit', '0')
					for node in doc.xpath("//" + view_type):
						node.set('delete', '0')
						node.set('duplicate', '0')
					if res.get('toolbar'):
						if res['toolbar'].get('action'):
							res['toolbar']['action'] = []
						if res['toolbar'].get('print'):
							print_items = filter(
								lambda x: x['display_name']  == "Póliza de diario",
								res['toolbar']['print']
							)
							res['toolbar']['print'] = list(print_items)

			# Aplica para las siguientes vistas:
			#   - Contabilidad:
			#       - Asientos Contables
			elif 'account.move' in res['name']:
				# Oculta los botones y acciones para los usuarios que no son administradores
				# o de registro.
				if not (is_admin_contabilidad or is_training_contabilidad or is_register_contabilidad):
					for node in doc.xpath("//" + view_type):
						# Ocultar botones de las vistas Odoo
						node.set('create', 'false')
						node.set('import', 'false')
						node.set('delete', 'false')
						node.set('edit', 'false')
						node.set('export', 'false')
				# Ocultar los items de print excepto Poliza de diario para los
				# usuarios que no son administrador:
				if not is_super_contabilidad:
					if res.get('toolbar') and res['toolbar'].get('print'):
						print_items = filter(
							lambda x: x['display_name']  == "Póliza de diario",
							res['toolbar']['print']
						)
						res['toolbar']['print'] = list(print_items)
				# Ocultar los items de accion para los usuarios que no son
				# administrador o registro:
				#   - Suprimir
				#   - Duplicar
				#   - Compartir
				if res.get('toolbar') and res['toolbar'].get('action'):
					if not (is_admin_contabilidad or is_training_contabilidad or is_register_contabilidad):
						# Items a ocultar
						items_to_hide = ["Suprimir", "Duplicar", "Compartir"]
						action_items = filter(
							lambda x: x['display_name'] not in items_to_hide,
							res['toolbar']['action']
						)
						res['toolbar']['action'] = list(action_items)
					if not (is_admin_contabilidad or is_training_contabilidad):
						items_to_hide = [
							"Cambiar a reembolso/Nota de crédito",
							"Generar un enlace de pago",
							"Actualizar estado en el PAC",
							"Revertir cancelación del CFDI",
							"Cancelar el método de Cobro/Pagos",
						]
						action_items = filter(
							lambda x: x['display_name'] not in items_to_hide,
							res['toolbar']['action']
						)
						res['toolbar']['action'] = list(action_items)
		elif '.finance' in res['name']:
			"""
				Aplica para las ventanas del módulo de finanzas:
					-Gestión de pagos
						-Programación de pago
					-Gestión de pagos de nómina
						-Programación de pago de nómina
						-Programa de pago de pensiones
					-Gestión de pagos diferentes a nómina
						-Programación de pagos diferentes a nómina
					-UPA PAPIIT
						-Programación de Pagos
			"""
			# logging.critical(res['name'])
			is_admin_finance = self.env.user.has_group('jt_payroll_payment.group_on_finance_admin')
			is_finance_user = self.env.user.has_group('jt_payroll_payment.group_admin_finance_user')
			is_approval_user = self.env.user.has_group('jt_payroll_payment.group_approval_user')
			is_consult_user = self.env.user.has_group('jt_payroll_payment.group_consult_user')
			is_register_user = self.env.user.has_group('jt_payroll_payment.group_register_user')

			#Vistas de Programación de Pagos
			if '.gestion' in res['name']:
				logging.critical(res)
				#Vista de lista
				if '.tree' in res['name']:
					if is_admin_finance:
						for node in doc.xpath("//" + view_type):
							node.set('edit', '1')
							node.set('create', '1')
							node.set('delete', '1')
							node.set('import', '1')
							node.set('export_xlsx', '1')
					elif (is_finance_user or is_approval_user or is_consult_user or is_register_user):
						for node in doc.xpath("//" + view_type):
							node.set('edit', '0')
							node.set('create', '0')
							node.set('delete', '0')
							node.set('import', '0')
							node.set('export_xlsx', '1')
					if (is_consult_user or is_approval_user):
						for node in doc.xpath("//" + view_type):
							node.set('export', '0')
							node.set('delete','0')
					else:
						for node in doc.xpath("//" + view_type):
							node.set('export', '1')
							node.set('delete','1')


					actions_to_hide=[
					"Invertir","Cambiar a reembolso/Nota de crédito",
					"Validar asientos","Actualizar estado en el PAC",
					"Revertir cancelación del CFDI"
					]
					#Ocultar impresiones
					if not (is_admin_finance or is_finance_user):
						res['toolbar']['print'] = []
					#Ocultar impresiones
					if is_register_user:
						action_items = filter(
							lambda x: x['display_name'] not in actions_to_hide,
							res['toolbar']['action']
						)
						res['toolbar']['action'] = list(action_items)
					elif is_consult_user or is_approval_user:
						res['toolbar']['action'] = []

				#Vista de formulario
				elif '.form' in res['name']:
					#Ocultar impresiones
					if not (is_admin_finance or is_finance_user):
						res['toolbar']['print'] = []

					#Ocultar Acciones
					if not is_finance_user:
						res['toolbar']['action'] = []

					#Ocultar botones
					value=''
					if is_admin_finance or is_finance_user:
						value='1'
					else:
						value='0'
					for node in doc.xpath("//" + view_type):
						node.set('edit',value)
						node.set('delete',value)


					#Ocultar Campos
					fields_read=[
						"partner_id",
						"dependancy_id","sub_dependancy_id",
						"l10n_mx_edi_payment_method_id",
						"currency_id","journal_id",
						"upa_key", "upa_document_type",
						"operation_type_id","payment_bank_id",
						"payment_bank_account_id",
						"payment_issuing_bank_id",
						"payment_issuing_bank_acc_id",
						"deposite_number","check_folio_id",
						
					]
					fields_hide=["layout_scholarship_data","layout_scholarship_filename"]

					if (is_finance_user or is_approval_user or is_consult_user or is_register_user):
						for node in doc.xpath("//field"):
							if (node.attrib['name'] in fields_read):
								#logging.critical(node.attrib['modifiers'])
								node.set('options','{"no_open":true}')

					if (is_finance_user or is_approval_user or is_consult_user or is_register_user):
						for node in doc.xpath("//field"):
							if (node.attrib['name'] in fields_hide):
								#logging.critical(node.attrib['modifiers'])
								node.set('modifiers','{"invisible":true}')

					if (is_finance_user or is_approval_user or is_consult_user or is_register_user):
						for node in doc.xpath("//button"):
							if (node.attrib['name'] in ['import_lines']):
								#logging.critical(node.attrib['modifiers'])
								node.set('modifiers','{"invisible":true}')
								



			#Programación de pago de nómina
			elif '.nominas' in res['name']:
				#Vista de lista
				if 'tree.' in res['name']:
					if is_admin_finance:
						for node in doc.xpath("//" + view_type):
							node.set('edit', '1')
							node.set('create', '1')
							node.set('delete', '1')
							node.set('import', '1')
							node.set('export_xlsx', '1')
					elif is_finance_user:
						for node in doc.xpath("//" + view_type):
							node.set('edit', '1')
							node.set('create', '0')
							node.set('delete', '0')
							node.set('import', '0')
							node.set('export_xlsx', '0')
					elif is_approval_user or is_consult_user or is_register_user:
						for node in doc.xpath("//" + view_type):
							node.set('edit', '0')
							node.set('create', '0')
							node.set('delete', '0')
							node.set('import', '0')
							node.set('export_xlsx', '1')
							node.set('export','0')
					#Ocultar impresiones y acciones en la vista de listas
					if not (is_admin_finance or is_finance_user):
						res['toolbar']['print'] = []
						res['toolbar']['action'] =[]


					
				#Vista de formulario
				elif 'form.' in res['name']:
					if is_admin_finance or is_finance_user:
						value='1'
					else:
						value='0'
					for node in doc.xpath("//" + view_type):
						node.set('edit',value)
						node.set('delete',value)

					if not (is_admin_finance or is_finance_user):
						res['toolbar']['print'] = []
						res['toolbar']['action'] =[]	


					#Ocultar Campos
					fields_hide=[
						"payroll_register_user_id",
						"payroll_send_user_id"
					]
					fields_read=[
						"partner_id",
						"dependancy_id","sub_dependancy_id",
						"l10n_mx_edi_payment_method_id",
						"journal_id",
						"payment_place_id",
						"payment_bank_id",
						"payment_bank_account_id",
						"payment_issuing_bank_id",
						"payment_issuing_bank_acc_id",
		
					]

					if (is_finance_user or is_approval_user or is_consult_user or is_register_user):
						for node in doc.xpath("//field"):
							if (node.attrib['name'] in fields_hide):
								#logging.critical(node.attrib['modifiers'])
								node.set('modifiers','{"invisible":true}')

					#Evitar los enlaces
					if (is_finance_user or is_approval_user or is_consult_user or is_register_user):
						for node in doc.xpath("//field"):
							if (node.attrib['name'] in fields_read):
								#logging.critical(node.attrib['modifiers'])
								node.set('options','{"no_open":true}')




			#Programa de pago de pensiones
			elif '.pension' in res['name']:
				#Vista de lista
				if '.tree' in res['name']:
					if is_admin_finance:
						for node in doc.xpath("//" + view_type):
							node.set('edit', '1')
							node.set('create', '1')
							node.set('delete', '1')
							node.set('import', '1')
							node.set('export_xlsx', '1')
					elif is_finance_user:
						for node in doc.xpath("//" + view_type):
							node.set('edit', '0')
							node.set('create', '0')
							node.set('delete', '0')
							node.set('import', '0')
							node.set('export_xlsx', '0')
					elif is_approval_user or is_consult_user or is_register_user:
						for node in doc.xpath("//" + view_type):
							node.set('edit', '0')
							node.set('create', '0')
							node.set('delete', '0')
							node.set('import', '0')
							node.set('export_xlsx', '1')
							node.set('export','0')
					if not (is_admin_finance or is_finance_user):
						res['toolbar']['print'] = []
						res['toolbar']['action'] =[]

				#Vista de formulario
				elif '.form' in res['name']:
					if is_admin_finance or is_finance_user:
						value='1'
					else:
						value='0'
					for node in doc.xpath("//" + view_type):
						node.set('edit',value)
						node.set('delete',value)

					if not (is_admin_finance or is_finance_user):
						res['toolbar']['print'] = []
						res['toolbar']['action'] =[]	


					
					fields_read=[
						"partner_id",
						"dependancy_id","sub_dependancy_id",
						"l10n_mx_edi_payment_method_id",
						"currency_id",
						"journal_id",
						"payment_bank_id",
						"payment_bank_account_id",
						"payment_issuing_bank_id",
						"payment_issuing_bank_acc_id",
						"check_folio_id",
		
					]
					#Evitar los enlaces
					if (is_finance_user or is_approval_user or is_consult_user or is_register_user):
						for node in doc.xpath("//field"):
							if (node.attrib['name'] in fields_read):
								#logging.critical(node.attrib['modifiers'])
								node.set('options','{"no_open":true}')




			#Programación de pagos diferentes a nómina
			elif '.different' in res['name']:
				#Vista de lista
				if '.tree' in res['name']:
					if is_admin_finance:
						for node in doc.xpath("//" + view_type):
							node.set('edit', '1')
							node.set('create', '1')
							node.set('delete', '1')
							node.set('import', '1')
							node.set('export_xlsx', '1')
					elif is_finance_user or is_approval_user or is_register_user:
						for node in doc.xpath("//" + view_type):
							node.set('edit', '1')
							node.set('create', '0')
							node.set('delete', '1')
							node.set('import', '0')
							node.set('export_xlsx', '1')
					elif is_consult_user :
						for node in doc.xpath("//" + view_type):
							node.set('edit', '0')
							node.set('create', '0')
							node.set('delete', '0')
							node.set('import', '0')
							node.set('export_xlsx', '0')
							node.set('export','0')
					if not (is_admin_finance or is_finance_user):
						res['toolbar']['print'] = []
						res['toolbar']['action'] =[]
				#Vista de formulario
				elif '.form' in res['name']:
					if is_admin_finance or is_finance_user:
						value='1'
					else:
						value='0'
					for node in doc.xpath("//" + view_type):
						node.set('edit',value)
						node.set('delete',value)

					if not (is_admin_finance or is_finance_user):
						res['toolbar']['print'] = []
						res['toolbar']['action'] =[]	


					
					fields_read=[
						"partner_id",
						"dependancy_id","sub_dependancy_id",
						"l10n_mx_edi_payment_method_id",
						"currency_id",
						"journal_id",
						"payroll_register_user_id",
						"payment_bank_id",
						"payment_bank_account_id",
						"payment_issuing_bank_id",
						"payment_issuing_bank_acc_id",
						"administrative_secretary_id",
						"upa_key",
						"upa_document_type",
		
					]
					#Evitar los enlaces
					if (is_finance_user or is_approval_user or is_consult_user or is_register_user):
						for node in doc.xpath("//field"):
							if (node.attrib['name'] in fields_read):
								#logging.critical(node.attrib['modifiers'])
								node.set('options','{"no_open":true}')



			#Programación de Pagos UPA PAPIIT
			elif 'project.payment' in res['name']:
				#Vista de lista
				if '.tree' in res['name']:
					if is_admin_finance or is_finance_user:
						for node in doc.xpath("//" + view_type):
							node.set('edit', '1')
							node.set('create', '1')
							node.set('delete', '1')
							node.set('import', '1')
							node.set('export_xlsx', '1')
					elif is_approval_user or is_consult_user:
						for node in doc.xpath("//" + view_type):
							node.set('edit', '0')
							node.set('create', '0')
							node.set('delete', '0')
							node.set('import', '0')
							node.set('export_xlsx', '0')
					elif is_register_user :
						for node in doc.xpath("//" + view_type):
							node.set('edit', '1')
							node.set('create', '1')
							node.set('delete', '1')
							node.set('import', '0')
							node.set('export_xlsx', '0')
							node.set('export','0')
				#Vista de formulario
				elif '.form' in res['name']:
					if is_admin_finance or is_finance_user:
						value='1'
					else:
						value='0'
					for node in doc.xpath("//" + view_type):
						node.set('edit',value)
						node.set('delete',value)

					if not (is_admin_finance or is_finance_user):
						res['toolbar']['print'] = []
						res['toolbar']['action'] =[]	


					
					fields_read=[
						"partner_id",
						"dependancy_id","sub_dependancy_id",
						"l10n_mx_edi_payment_method_id",
						"currency_id",
						"journal_id",
						"payroll_register_user_id",
						"payment_bank_id",
						"payment_bank_account_id",
						"payment_issuing_bank_id",
						"payment_issuing_bank_acc_id",
						"administrative_secretary_id",
						"upa_key",
						"upa_document_type",
		
					]
					#Evitar los enlaces
					if (is_finance_user or is_approval_user or is_consult_user or is_register_user):
						for node in doc.xpath("//field"):
							if (node.attrib['name'] in fields_read):
								#logging.critical(node.attrib['modifiers'])
								node.set('options','{"no_open":true}')

		# Facturación digital
		is_group_income_fac_administrator_user = self.env.user.has_group('jt_income.group_income_fac_administrator_user')
		is_group_income_fac_finance_user = self.env.user.has_group('jt_income.group_income_fac_finance_user')
		is_group_income_fac_accounting_user = self.env.user.has_group('jt_income.group_income_fac_accounting_user')

		if (is_group_income_fac_administrator_user or is_group_income_fac_finance_user or is_group_income_fac_accounting_user):

			for node in doc.xpath("//" + view_type):
				node.set('export','1')
				node.set('edit','0')

			for node in doc.xpath("//field"):
					node.set('options','{"no_open":true}')	

			prints_to_hide = [
					"Facturas",
					"Facturas originales",
					"Facturas si cobro",
					"Solicitud Proveedor",
					"Papeleta de Deposito Bancario",
					"FORMATO DE APLICACIÓN 20%",
					"FORMATO DE CONDONACIÓN",
					"FORMATO DE AVISO CAMBIO FORMA DE COBRO A TRANSFERENCIA",
					"2° FORMATO DE APLICACIÓN 20%",
					"FORMATO DE CONDONACIÓN 20%",
					"FORMATO DE NOTIFICACIÓN DE CHEQUE DEVUELTO",
					"Solicitud Viáticos",
					"Solicitud Prácticas Escolares",
					"Solicitud Trabajos de Campo",
					"Solicitud reembolso a terceros",
					"Solicitud de Intercambio",
					"Papeleta de Entrega de Cheques",
			]
			
			actions_to_hide = [
				"Registrar Cobro/Pago",
				"Enviar e imprimir",
				#"Devolución por nota de crédito",
				"Invertir",
				"Compartir",
				"Generar un enlace de pago",

				"Cambiar a reembolso/Nota de crédito",
				"Validar asientos",
				"Actualizar estado en el PAC",
				"Revertir cancelación del CFDI",
				"Validación Presupuestal",
				"Generar hoja de lote",
				"Confirmar Pago Diferente de Nómina",
				"Confirmar pago de pensión",
				"Registro",
				"Cambio de Fecha",
				"Cancelar el método de Cobro/Pagos",
			]			

			if res.get('toolbar'):
				if res['toolbar'].get('print'):
					print_items = filter(lambda x: x['display_name'] not in prints_to_hide,
						res['toolbar']['print']
						)
					res['toolbar']['print'] = list(print_items)	

				if res['toolbar'].get('action'):
					action_items = filter(lambda x: x['display_name'] not in actions_to_hide, 
					res['toolbar']['action'])
					res['toolbar']['action'] = list(action_items)

		# Solicitud de devolución por nota de crédito
		is_group_income_sdnc_administrator_user = self.env.user.has_group('jt_income.group_income_sdnc_administrator_user')
		is_group_income_sdnc_finance_user = self.env.user.has_group('jt_income.group_income_sdnc_finance_user')
		is_group_income_sdnc_accounting_user = self.env.user.has_group('jt_income.group_income_sdnc_accounting_user')

		if (is_group_income_sdnc_administrator_user or is_group_income_sdnc_finance_user or is_group_income_sdnc_accounting_user):
			
			for node in doc.xpath("//" + view_type):
					node.set('export','0')
					node.set('delete','0')

			for node in doc.xpath("//field"):
				node.set('options','{"no_open":true}')
				
			prints_to_hide = ["Facturas originales",
				"Facturas si cobro",
				"Solicitud Proveedor",
				"Papeleta de Deposito Bancario",
				"FORMATO DE APLICACIÓN 20%",
				"FORMATO DE CONDONACIÓN",
				"FORMATO DE AVISO CAMBIO FORMA DE COBRO A TRANSFERENCIA",
				"2° FORMATO DE APLICACIÓN 20%",
				"FORMATO DE CONDONACIÓN 20%",
				"FORMATO DE NOTIFICACIÓN DE CHEQUE DEVUELTO",
				"Solicitud de Certificado de Depósito",
				"Solicitud Viáticos",
				"Solicitud Prácticas Escolares",
				"Solicitud Trabajos de Campo",
				"Solicitud reembolso a terceros",
				"Solicitud de Intercambio",
				"Papeleta de Entrega de Cheques",
				"Póliza de diario CONAC",
				]
			actions_to_hide = ["Registrar Cobro/Pago",
				"Invertir",
				"Cambiar a reembolso/Nota de crédito",
			]
			if res.get('toolbar'):
				if res['toolbar'].get('print'):
					print_items = filter(lambda x: x['display_name'] not in prints_to_hide,
					res['toolbar']['print']
					)
					res['toolbar']['print'] = list(print_items)

				if res['toolbar'].get('action'):
					action_items = filter(lambda x: x['display_name'] not in actions_to_hide,
					res['toolbar']['action']
					)
					res['toolbar']['action'] = list(action_items)					
			
		# Intereses bancarios
		is_group_income_bi_create_user = self.env.user.has_group('jt_income.group_income_bi_create_user') 
		is_group_income_bi_guest_user = self.env.user.has_group('jt_income.group_income_bi_guest_user')
		if is_group_income_bi_guest_user or is_group_income_bi_create_user:
			prints_to_hide = [
				"Facturas",
				"Facturas originales",
				"Facturas si cobro",
				"Solicitud Proveedor",
				"Papeleta de Deposito Bancario",
				"FORMATO DE APLICACIÓN 20%",
				"FORMATO DE CONDONACIÓN",
				"FORMATO DE AVISO CAMBIO FORMA DE COBRO A TRANSFERENCIA",
				"2° FORMATO DE APLICACIÓN 20%",
				"FORMATO DE CONDONACIÓN 20%",
				"FORMATO DE NOTIFICACIÓN DE CHEQUE DEVUELTO",
				"Solicitud Viáticos",
				"Solicitud Prácticas Escolares",
				"Solicitud Trabajos de Campo",
				"Solicitud reembolso a terceros",
				"Solicitud de Intercambio",
				"Papeleta de Entrega de Cheques",
				"Solicitud de Certificado de Depósito",
				#"Póliza de diario",
				"Póliza de diario CONAC",
				]
			actions_to_hide = [
				"Registrar Cobro/Pago",
				"Invertir",
				#"Devolución por nota de crédito",
				"Enviar e imprimir",
			]

			if res.get('toolbar'):
				if res['toolbar'].get('print'):
					print_items = filter(lambda x: x['display_name'] not in prints_to_hide,
					res['toolbar']['print']
					)
					res['toolbar']['print'] = list(print_items)

				if res['toolbar'].get('action'):
					action_items = filter(lambda x: x['display_name'] not in actions_to_hide,
					res['toolbar']['action']
					)
					res['toolbar']['action'] = list(action_items)
			
			for node in doc.xpath("//field"):
				node.set('options','{"no_open":true}')

		
		if is_group_income_bi_create_user:
			if 'account.bank.interest.tree.income' in res['name']:
				for node in doc.xpath("//" + view_type):
					node.set('create', '1')
					node.set('export_xlsx', '1')

			if 'account.bank.interest.form.income' in res['name']:
				for node in doc.xpath("//" + view_type):
					node.set('create', '1')
					node.set('duplicate', '0')

				for node in doc.xpath("//field"):
					node.set('options','{"no_open":true}')

		if is_group_income_bi_guest_user:
			if 'account.bank.interest.tree.income' in res['name']:
				for node in doc.xpath("//" + view_type):
					node.set('export_xlsx', '1')

			if 'account.bank.interest.form.income' in res['name']:
				for node in doc.xpath("//" + view_type):
					pass

				for node in doc.xpath("//field"):
					node.set('options','{"no_open":true}')			
					
		
			if 'account.invoice.income.form.view.income' in res['name']:
				for node in doc.xpath("//" + view_type):
					# Ocultar botones de las vistas Odoo
					node.set('create', '1')

		# Formularios
		# Form certificados depositos
		if 'account.dep_cert.form.income' in res['name']:
			for node in doc.xpath("//" + view_type):
				node.set('edit', '1')
		# Form facturacion
		if 'account.invoice.income.form.view.income' in res['name']:
			if not is_group_income_admin_user:
				for node in doc.xpath("//" + view_type):
					# Ocultar botones de las vistas Odoo
					node.set('create', '0')
					node.set('edit', '0')
					node.set('delete', '0')
	 
		# Asientos contables manuales
		if 'account.move.manual.accounting.record.form.view.inherit' in res['name']:
			if is_account_approval_user:
				for node in doc.xpath("//" + view_type):
                    # Ocultar botones de las vistas Odoo
					node.set('create', '0')
					node.set('edit', '1')
					node.set('delete', '0')
			elif is_account_register_user:
				for node in doc.xpath("//" + view_type):
                    # Ocultar botones de las vistas Odoo
					node.set('create', '1')
					node.set('edit', '1')
					node.set('delete', '0')

		res['arch'] = etree.tostring(doc)
		return res