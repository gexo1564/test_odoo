# -*- coding: utf-8 -*-
from datetime import datetime
import logging
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from lxml import etree

class ConversionResourceOrigin(models.Model):

    _name = 'conversion.resource.origin'

    resource_origin = fields.Many2one("resource.origin", string='Resource Origin')
    resource_origin_name = fields.Char(related='resource_origin.desc')

    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')

    selector_sp = fields.Selection(
        [
            ('all', 'All'),
            ('except', 'Except'),
            ('only', 'Only')
        ],
        default='all', required=True, string='Selector Sub Programs'
    )
    subprogram_ids = fields.Many2many('base.sub.program', 'conversion_resource_origin_sub_program_rel', string='Sub Programs')

    selector_item = fields.Selection(
        [
            ('all', 'All'),
            ('except', 'Except'),
            ('only', 'Only')
        ],
        default='all', required=True, string='Selector Items'
    )
    item_ids = fields.Many2many('expenditure.item', 'conversion_resource_origin_item_rel', string='Items')

    
    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super().fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])
   
        is_group_budget_approval_user = self.env.user.has_group('jt_budget_mgmt.group_budget_approval_user')
        is_group_budget_guest_user = self.env.user.has_group('jt_budget_mgmt.group_budget_guest_user')
        is_group_budget_confirmation_user = self.env.user.has_group('jt_budget_mgmt.group_budget_confirmation_user')
        is_group_budget_register_user = self.env.user.has_group('jt_budget_mgmt.group_budget_register_user')
        
        if (is_group_budget_approval_user or is_group_budget_guest_user or is_group_budget_confirmation_user or is_group_budget_register_user): 
            for node in doc.xpath("//" + view_type):
                node.set('export', '0')
                node.set('create', '0') 
                node.set('edit', '0')
                node.set('delete', '0')

            for node in doc.xpath("//field"):
                node.set('options','{"no_open":true}')
                    
                
        res['arch'] = etree.tostring(doc) 
        return res

    @api.constrains('start_date', 'end_date')
    def check_end_date(self):
        if not self.start_date:
            raise ValidationError(_("The start date of validity is required."))
        if self.end_date and self.end_date <= self.start_date:
            raise ValidationError(_("The effective end date cannot be less than or equal to the effective date."))

    @api.model
    def create(self, vals):
        domain = []
        if vals.get('resource_origin',None):
            domain.append(('resource_origin', '=', vals.get('resource_origin',None)))
        if vals.get('start_date',None):
            domain.append(('start_date', '=', vals.get('start_date',None)))
        if vals.get('end_date',None):
            domain.append(('end_date', '=', vals.get('end_date',None)))
        if vals.get('selector_sp',None):
            domain.append(('selector_sp', '=', vals.get('selector_sp',None)))
        subprogram_ids = vals.get('subprogram_ids',None)
        if subprogram_ids and subprogram_ids[0][2]:
            domain.append(('subprogram_ids', 'in', subprogram_ids[0][2]))
        if vals.get('selector_item',None):
            domain.append(('selector_item', '=', vals.get('selector_item',None)))
        item_ids = vals.get('item_ids',None)
        if item_ids and item_ids[0][2]:
            domain.append(('item_ids', 'in', item_ids[0][2]))
        if  self.env['conversion.resource.origin'].search(domain):
            raise ValidationError("La Regla del Origen del Recurso ya existe.")
        return super().create(vals)

    @api.onchange("selector_sp")
    def _onchange_selector_sp(self):
        if self.selector_sp == 'all':
            self.write({'subprogram_ids': [(5,)]})

    @api.onchange("selector_item")
    def _onchange_selector_item(self):
        if self.selector_item == 'all':
            self.write({'item_ids': [(5,)]})

    def name_get(self):
        return [(rec.id, rec.resource_origin.key_origin) for rec in self]

    @api.model
    def get_resource_origin(self, subprogram, item, date=datetime.now()):
        query = f"""
        select ro.key_origin resource_origin from conversion_resource_origin c, resource_origin ro
        where resource_origin = ro.id and (selector_sp = 'all' or (
                selector_sp = 'only' and exists (
                select 1 from conversion_resource_origin_sub_program_rel r, base_sub_program p where conversion_resource_origin_id = c.id and base_sub_program_id = p.id and p.sub_program = '{subprogram}'
            )) or (selector_sp = 'except' and not exists (
                select 1 from conversion_resource_origin_sub_program_rel r, base_sub_program p where conversion_resource_origin_id = c.id and base_sub_program_id = p.id and p.sub_program = '{subprogram}'
            ))
        ) and (selector_item = 'all' or (
                selector_item = 'only' and exists (
                select 1 from conversion_resource_origin_item_rel r, expenditure_item i where conversion_resource_origin_id = c.id and expenditure_item_id = i.id and i.item = '{item}'
            )) or (selector_item = 'except' and not exists (
                select 1 from conversion_resource_origin_item_rel r, expenditure_item i where conversion_resource_origin_id = c.id and expenditure_item_id = i.id and i.item = '{item}'
            ))
        ) and c.start_date <= '{date}' and (c.end_date is null or c.end_date >= '{date}')
        """
        self._cr.execute(query, ())
        res = self.env.cr.dictfetchall()
        return res
