# -*- coding: utf-8 -*-
from datetime import datetime
import logging
from odoo.tests import common
from odoo.exceptions import ValidationError, ValidationError

DATE_FORMAT = "%Y-%m-%d"

class TestConversionCONPP(common.TransactionCase):

    def setUp(self):
        super().setUp()
        self.conpp = self.env['shcp.code'].create({'name': 'T123'})

    def test_check_end_date(self):
        """ Validación de las fechas de inicio y fin de vigencia. """
        catalogo = self.env['conversion.conpp'].create({
            'conpp': self.conpp.id,
        })
        """
        Escenario 1: Regla sin fecha de inicio
        """
        with self.assertRaises(ValidationError):
            catalogo.check_end_date()

        """
        Escenario 2: Regla con fecha de inicio, pero sin fecha de fin
        """
        catalogo.start_date = datetime.strptime("2022-01-01", DATE_FORMAT)

        """
        Escenario 3: Regla con fecha de inicio, y fecha fin menor a la fecha inicio
        """
        with self.assertRaises(ValidationError):
            catalogo.end_date = datetime.strptime("2021-12-31", DATE_FORMAT)

        """
        Escenario 4: Regla con fecha de inicio, y fecha fin igual a la fecha inicio
        """
        with self.assertRaises(ValidationError):
            catalogo.end_date = datetime.strptime("2022-01-01", DATE_FORMAT)

        """
        Escenario 5: Regla con fecha de inicio, y fecha fin igual a la fecha inicio
        """
        catalogo.end_date = datetime.strptime("2022-01-31", DATE_FORMAT)

