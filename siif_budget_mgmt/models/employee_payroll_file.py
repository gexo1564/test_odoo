# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class AdditionalPaymentsLine(models.Model):

    _inherit = 'additional.payments.line'

    account_id = fields.Many2one(related='program_code_id.item_id.unam_account_id', string='Accounting Account')