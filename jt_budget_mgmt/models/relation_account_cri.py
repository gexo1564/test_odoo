#-*- coding: utf-8 -*-
import json
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
import logging

class RelationAccountCri(models.Model):
    _name = 'relation.account.cri'
    _description = 'Relación CC - CRI'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'name'
    _order = "name asc"

    name = fields.Text(string='Nombre', compute="_compute_relation_cc_cri", store=True, tracking=True)
    cuentas_id = fields.Many2one('account.account', string='Cuentas Contables', required=True, tracking=True, store=True)
    domain_cuentas_id = fields.Char(compute="compute_domain_cuentas", store=False)
    cri = fields.Many2one('classifier.income.item', string='Clasificador por Rubro de Ingresos', required=True, tracking=True)
    start_date = fields.Date(string="Fecha Inicio Vigencia", required=True, tracking=True)
    end_date = fields.Date(string="Fecha Fin Vigencia", tracking=True)
    user_id = fields.Many2one('res.users', string='Responsable',default=lambda self: self.env.user, tracking=True)
    budget_registry = fields.Boolean(string="Registro presupuestal", store = True, tracking=True)

    @api.depends('cuentas_id', 'cri', 'start_date')
    def _compute_relation_cc_cri(self):
        for record in self:
            relation = ''
            if record.cri:
                relation += str(record.cri.name)
                relation += "/"
            if record.cuentas_id:
                relation += str(record.cuentas_id.code)
            record.name = relation

    # Guardar la descripción del proceso y el nombre corto
    @api.constrains('cuentas_id')
    def _search_budget_registry(self):
        obj_account = self.env['account.account']
        for record in self:
            registry = obj_account.search([('id', '=', record.cuentas_id.id), ('budget_registry', '=', True)])
            
            if registry:
                self.budget_registry = True
            else:
                self.budget_registry = False

    _sql_constraints = [('name', 'unique(name)', ('La relación cuenta contable y Clasificador por Rubro de Ingresos debe ser única.'))]

    # To verify that end date is greater than start date
    @api.constrains('start_date', 'end_date')
    def _check_dates(self):
        if self.start_date and self.end_date and self.start_date > self.end_date:
            raise ValidationError(_("The end date of validity must be greater than the start date of validity."))
            
    @api.depends('cuentas_id')
    def compute_domain_cuentas(self):
        for rec in self:
            domain = [('internal_group', '=', 'income'),('budget_registry', '=', True)]
            if rec.cuentas_id:
                domain.append(('id', '=', rec.cuentas_id.id))
            rec.domain_cuentas_id = json.dumps(domain)