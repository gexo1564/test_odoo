# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields,_
from odoo.exceptions import UserError, ValidationError
from odoo.tools.profiler import profile
import logging

from odoo.addons.siif_supplier_payment.tools.utils import Dictionary

ACCOUNT_MOVE = 'account.move'
GENERATE_BATCH_SHEET = 'generate.batch.sheet'

class GenerateBatchSheet(models.TransientModel):

    _name = GENERATE_BATCH_SHEET
    _description = 'Generate Batch Sheet'

    batch_line_ids = fields.One2many('generate.batch.sheet.line','batch_sheet_id','Folio')
    payment_type = fields.Selection([('supplier','Suppier'),('payroll','Payroll'),('different_payroll','Different Payroll'),('project_payment','Project Payment'),('pension_payment','Pension Payment')],string="Payent Type") 

    def action_generate_batch(self):
        # Se obtienen los ids activos que selecciono el usuario
        active_ids = self.env.context.get('active_ids')
        if not active_ids:
            return ''
        # Se obtienen los objetos account.move de las solicitudes de pago
        move_ids = self.env[ACCOUNT_MOVE].browse(active_ids)
        # Diccionario para almacenar el folio que se asignara por tipo de solicitud y no incrementar dos veces la misma secuencia.
        folios = {}
        line_data = []
        for move_id in move_ids:
            # Obtiene el valor actual del folio de la solicitud de pago
            line_batch_folio = move_id.batch_folio
            if move_id.invoice_sequence_number_next_prefix and  move_id.invoice_sequence_number_next:
                name = move_id.invoice_sequence_number_next_prefix + move_id.invoice_sequence_number_next
            else:
                name = move_id.name
                # Obtiene el tipo de pago que se esta procesando
            payment_type = move_id.get_payment_type()
            # Si no esta difinido el folio actual, genera el siguiente valor de la
            # secuencia
            if not line_batch_folio:
                # Se actualiza o consulta el folio del tipo de pago en el caché
                if not folios.get(payment_type, False):
                    folios[payment_type] = move_id._generate_next_batch_sheet_folio(payment_type)
                line_batch_folio = folios.get(payment_type)
            line_data.append((0,0,{
                'account_move_id': move_id.id,
                'check_batch_folio': move_id.batch_folio,
                'batch_folio': line_batch_folio,
                'name': name,
            }))
        return {
            'name': _('Generate Batch Sheet'),
            'res_model': GENERATE_BATCH_SHEET,
            'view_mode': 'form',
            'view_id': self.env.ref('jt_supplier_payment.view_generate_batch_sheet').id,
            'context': {
                'default_batch_line_ids': line_data,
                'default_payment_type': payment_type,
            },
            'target': 'new',
            'type': 'ir.actions.act_window',
        }

    def update_batch_folio(self):
        # Solo actualiza las solicitudes a las que se les asignó por primera vez
        # su folio por lotes (folio distinto de cero).
        folios = Dictionary()
        for line in self.batch_line_ids:
            if line.check_batch_folio == 0:
                folios.add_list(line.batch_folio, line.account_move_id.id)

        for folio, move_ids in folios.items():
            self.env[ACCOUNT_MOVE]._update_batch_folio(folio, move_ids)

class GenerateBatchSheetLine(models.TransientModel):

    _name = 'generate.batch.sheet.line'

    account_move_id = fields.Many2one(ACCOUNT_MOVE,'Invoice')
    check_batch_folio = fields.Integer('Check Batch Folio')
    batch_folio = fields.Integer('Batch Folio')
    name = fields.Char('Name')
    batch_sheet_id = fields.Many2one(GENERATE_BATCH_SHEET)
