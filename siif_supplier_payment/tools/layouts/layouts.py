from __future__ import annotations
from abc import ABC, abstractmethod
class ResultLine:
    def __init__(self, payment_status: bool, columns:dict, error_message:str=""):
        self.payment_status = payment_status
        self.error_message = error_message
        self.payment_id = None
        self.columns = columns
class Context:
    _layout = None
    _file = None

    def __init__(self, env, state: Layout,) -> None:
        self.transition_to(env, state)

    def transition_to(self, env, state: Layout):
        self._layout = state
        self._layout.env = env

    def load_layout(self, file_data):
        # Se abre el archivo
        self.load_file(file_data)
        # Se obtiene el body
        body = self.load_body()
        lines = []
        # Se procesan las líneas del body
        for line in body:
            # Se extraen los datos de la línea
            lines.append(self.read_line(line))
        # Se cierra el archivo
        self.close_file()
        return self.get_ids_payment(lines)

    def load_file(self, file_data):
        return self._layout.load_file(file_data)

    def load_body(self):
        return self._layout.load_body()

    def read_line(self, line):
        return self._layout.read_line(line)

    def close_file(self):
        return self._layout.close_file()

    def get_query_search_employee_payroll_file(self):
        return self._layout.get_query_search_employee_payroll_file()

    def get_ids_payment(self, lines):
        return self._layout.get_ids_payment(lines)

class Layout(ABC):
    @property
    def env(self):
        return self._env

    @env.setter
    def env(self, env) -> None:
        self._env = env

    @property
    def payroll_processing_id(self):
        return self._payroll_processing_id

    @payroll_processing_id.setter
    def payroll_processing_id(self, payroll_processing_id: int) -> int:
        self._payroll_processing_id = payroll_processing_id

    @property
    def file(self):
        return self._file

    @file.setter
    def file(self, file) -> None:
        self._file = file

    @abstractmethod
    def load_file(self, file_data) -> None:
        pass

    @abstractmethod
    def load_body(self, file) -> None:
        pass

    @abstractmethod
    def read_line(self, line) -> ResultLine:
        pass

    @abstractmethod
    def close_file(self) -> None:
        pass

    @abstractmethod
    def get_ids_payment(self, lines: list) -> tuple:
        pass