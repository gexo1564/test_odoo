from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
import logging
from datetime import date,datetime
import json
import dateutil.parser
from lxml import etree

"""Modelos"""
ACC_MOVE='account.move'
ACC_TOPAY='account.topay'
MONEY_FUND_REQ='money.funds.request'
MONEY_FUND_CONF='money.funds.conf'
MONEY_FUND_REQ_MIN='money.funds.request.ministration'
MONEY_FUND_REQ_RETURN='money.funds.request.returns'
MONEY_FUND_REQ_REJECT='money.fund.req.rejection'
REQ_OPEN_FINANCE='request.open.balance.finance'
BANK_REF_CONF='bank.reference.conf'
SIDIA_SIGNATURE='sidia.university_signature'
IR_ATTACH='ir.attachment'
IR_SEQ='ir.sequence'
BANK_REF='bank.reference'
"""Configuracion"""
XPATH_FIELD='//field'
XPATH_OPEN_POS='{"no_open":true}'
XPATH_OPEN_NEG='{"no_open":false}'

"""Mensajes"""
ACT_WINDOW='ir.actions.act_window'
LIMIT_AMOUNT_MSG='Amount Limit cant be negative or 0.'
FIRMA='Firma del Usuario de la dependencia'
ADMIN_FORM='Add Administrative Forms'
WITH_OBSERVE='With Observations'
CORRECTION='Rejected for Correction'
PUBLISHED='Is published?'
SIGNED='Fecha firma'

"""Grupos"""
SIIF_ADMIN_USER='siif_currency_purchase_req.admin_user'
SIIF_UPA_USER='siif_currency_purchase_req.upa_user'
SIIF_DEP_USER='siif_currency_purchase_req.dep_user'
SIIF_CONTA='siif_currency_purchase_req.contability_user'
SIIF_FINANCE='siif_currency_purchase_req.finance_user'
SIIF_FINANCE_DIR='siif_currency_purchase_req.finance_director'
SIIF_BUDGET='siif_currency_purchase_req.budget_control_director'

class MoneyFundsRequest(models.Model):
    #Base solicitudes de Fondo Fijo
    _name = "money.funds.request"
    _description = 'Management of foreign exchange funds for cash per diems'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    domain_money_fund = fields.Char(compute="_compute_money_fund", store=False)
    money_fund = fields.Many2one('money.funds',string="Money Fund",store=True)
    name = fields.Char(string='Name')

    def get_domain_dependency(self):
        dep = self.env.user.dependency_for_money_fund_ids.ids
        dep += [d.id for d in self.env.user.sub_dependency_for_money_fund_ids.dependency_id]
        return json.dumps([('id', 'in', dep)])

    dependency_id = fields.Many2one('dependency',string="Dependency",required=True,domain=get_domain_dependency)
    sub_dependency_id = fields.Many2one('sub.dependency', string="Subdependency",required=True)
    domain_sub_dependency = fields.Char(compute="_compute_domain_sub_dependency", store=False)

    #Campos relacionados

    currency_id = fields.Many2one('res.currency',string='Currency',required=True,related='money_fund.currency_id')
    actual_fund = fields.Monetary(related='money_fund.import_fund',currency_field='currency_id',string="Actual Fund")
    has_funds = fields.Boolean(string="Funds?")
    min_request_id = fields.One2many(MONEY_FUND_REQ_MIN, compute="_compute_min_request",string='Ministrations')
    

    
    
    
    #Observaciones
    observations = fields.Text(string="Observations") 
    observations_doc = fields.Text(string="Observations Doc")
    reject_reasons = fields.Text(string="Rejected Reasons")

    #Solicitud de Transferencia Vinculada
    transfer_request = fields.Many2one(REQ_OPEN_FINANCE,string='Transfer request',tracking=True)
    transfer_acc_move_lines = fields.One2many('account.move.line', compute="_get_transfer_moves",tracking=True)

    

    
    


    #==============================================================
    #----------------------Condiciones del cambio
    #==============================================================
    @api.depends('dependency_id')
    def _compute_domain_sub_dependency(self):
        for record in self:
            domain = [('dependency_id', '=', record.dependency_id.id)]
            if record.dependency_id.id not in self.env.user.dependency_for_money_fund_ids.ids:
                domain.append(('id', 'in', self.env.user.sub_dependency_for_money_fund_ids.ids))
            record.domain_sub_dependency = json.dumps(domain)
    
    @api.depends('dependency_id','sub_dependency_id')
    def _compute_money_fund(self):
        for record in self:
            if record.dependency_id!=None and record.sub_dependency_id!=None:
                record.domain_money_fund = json.dumps([('dependency_id','=',record.dependency_id.id),('sub_dependency_id','=',record.sub_dependency_id.id),('state','=','on_use')])
            else:
                record.domain_money_fund = json.dumps([()])

    @api.onchange('dependency_id')
    def _onchange_subdependency(self):
        self.sub_dependency_id=''

    @api.onchange('money_fund')
    def _compute_has_fund(self):
        for rec in self:
            if rec.money_fund.id==False:
                rec.has_funds=False
            else:
                rec.has_funds=True


    #==========================================================
    #----------------Funciones
    #==========================================================
    #Función la cual asigna la lista de ministraciones pendientes para observaciones
    def _get_min_info(self,rec):
        msg="Listado de ministraciones dependientes"
        count=1
        for min in rec:
            msg+="\n"+str(count)+") Ministración: "+str(min.name)+" Monto pendiente: $"+str(min.amount_request)+". Fecha de compromiso: "+str(min.date_request.strftime("%d/%m/%Y"))
            if str(min.status_time)=='on_time':
                msg+=" Estado: En tiempo."
            elif str(min.status_time)=='delay':
                msg+=" Estado: Retrasada."
        return msg
    
    def update_amount_fund(self,rec,type):
        if type=='inc':
            rec.money_fund.import_fund += rec.amount_request
        elif type in ('min','dev'):
            rec.money_fund.import_fund -= rec.amount_request
    


    
            
class MoneyFundsRequestIncrement(models.Model):
    #Solicitudes de fondo fijo Incremento
    _name = 'money.funds.request.increment'
    _inherit = "money.funds.request"

    state = fields.Selection(selection=[
            ('draft', 'Draft'),
            ('published', 'Published'),
            ('with_observations', WITH_OBSERVE),
            ('revised', 'Revised'),
            ('reject_correction', CORRECTION),
            ('approved', 'Approved'),
            ('rejected', 'Rejected'),
            ('accepted_dep', 'Accepted by Dependency'),
            ('authorized_dfg', 'Authorized by DFG'),
            ('with_funds', 'With Funds'),
            ('cancelled','Cancelled')
        ], string='Status',default='draft')
        #Campos a Ingresar
    move_type = fields.Selection(selection=[
        ('adding','Adding'),
        ('ministration','Ministration')
    ],string="Move Type", compute="_compute_move_type")
    amount_request = fields.Monetary("Fund",currency_field='currency_id',default=0)
    is_published = fields.Boolean(string=PUBLISHED,default=False)
    is_first_request = fields.Boolean('Is First Request?',default=False)

    justify_files = fields.Many2many(IR_ATTACH,'money_funds_request_increment_attachment_rel', string=ADMIN_FORM)
    

    #Firmas electrónicas Dependencia
    feu_signature_dep = fields.Many2one(SIDIA_SIGNATURE, string=FIRMA, store=True, tracking= True)
    qr_signature_dep = fields.Binary(related="feu_signature_dep.qr_image", string=FIRMA, readonly=True)
    signature_name_dep = fields.Text(related="feu_signature_dep.signature_name", string="Nombre", readonly=True, store=True)
    curp_feu_dep = fields.Char(related="feu_signature_dep.curp", string="CURP", readonly=True , store=True)
    date_feu_dep = fields.Datetime(related="feu_signature_dep.create_date", string=SIGNED, readonly=True)


    #Firmas electrónicas DGF
    feu_signature_dgf = fields.Many2one(SIDIA_SIGNATURE, string="Firma del Director General de finanzas", store=True, tracking= True)
    qr_signature_dgf = fields.Binary(related="feu_signature_dgf.qr_image", string=FIRMA, readonly=True)
    signature_name_dgf = fields.Text(related="feu_signature_dgf.signature_name", string="Nombre", readonly=True, store=True)
    curp_feu_dgf = fields.Char(related="feu_signature_dgf.curp", string="CURP", readonly=True , store=True)
    date_feu_dgf = fields.Datetime(related="feu_signature_dgf.create_date", string=SIGNED, readonly=True)


    @api.model
    def create(self, vals):
        if vals.get('has_funds')==True:
            res = super(MoneyFundsRequestIncrement, self).create(vals)
            if not res.is_first_request:
                res.name = self.env[IR_SEQ].next_by_code('money.funds.request.increment')
            else:
                res.name = "Solicitud Inicial Incremento "+res.money_fund.name
            return res
        else:
            raise UserError(_('It cant be an assign request if doesnt have a currency fund.'))
        
    def unlink(self):
        if self.state not in ['draft']:
            raise UserError(_('You cannot delete an increment request which has been not draft state.'))
        return super(MoneyFundsRequestIncrement, self).unlink()
    #==============================================================
    #-----------------------Revisar datos
    #==============================================================
    @api.depends('dependency_id','sub_dependency_id')
    def _compute_money_fund(self):
        for record in self:
            if record.dependency_id!=None and record.sub_dependency_id!=None:
                if not record.is_first_request:
                    record.domain_money_fund = json.dumps([('dependency_id','=',record.dependency_id.id),('sub_dependency_id','=',record.sub_dependency_id.id),('state','=','on_use')])
                else:
                    record.domain_money_fund = json.dumps([()])
            else:
                record.domain_money_fund = json.dumps([()])
    @api.constrains('amount_request')
    def _check_amount_request(self):
        if self.has_funds and not self.is_first_request and self.amount_request<=0:
            raise ValidationError(_(LIMIT_AMOUNT_MSG))
            
    
            
    @api.onchange('has_funds')
    def _compute_min_request(self):
        if self.has_funds:
            self.min_request_id = self.env[MONEY_FUND_REQ_MIN].search([('money_fund','=',self.money_fund.id),('state','=','deposited'),('verified_complete','=',False)])
        else:
            self.min_request_id = False

    @api.onchange('has_funds')
    def _compute_move_type(self):
        for rec in self:
            if rec.has_funds:
                rec.move_type='adding'
            else:
                rec.move_type=False
    #==============================================================
    #-----------------Acciones de Vista
    #==============================================================
    #Botón para devolver la solicitud a Borrador
    def action_for_draft(self):
        self.state='draft'

    def action_for_publish(self):
        if self.has_funds:
            if self.state in ('draft','reject_correction'):
                if self.amount_request<=0:
                    raise ValidationError(_(LIMIT_AMOUNT_MSG))
                else:
                    if len(self.justify_files)>0:
                        self.state='published'
                    else:
                        raise ValidationError(_('Justify Doc doesnt attach, check.'))
        else:
            raise UserError(_('The request cant be published if it doesnt have a currency fund.'))

    # Botón para revisar la solicitud, se manda a Publicada si no se tiene 
    # otras ministraciones.
    # SI hay más ministraciones, se anda a Con Obsevaciones
    def action_for_observations(self):
        logging.critical(self.money_fund.id)
        ministraciones = self.env[MONEY_FUND_REQ_MIN].search([('money_fund','=',self.money_fund.id),('state','=','deposited'),('verified_complete','=',False)])
        if self.has_funds and self.state=='published':
            if len(ministraciones)>0:
                self.observations=self._check_funds_info(self)+self._get_min_info(ministraciones)+self._check_mount_info(self)
                self.state='with_observations'
                self.is_published=True
            else:
                self.observations=self._check_funds_info(self)+self._check_mount_info(self)
                self.state='published'
                self.is_published=True


    #Acción para asignar que está revisada
    def action_for_revised(self):
        if self.state in ('with_observations','published'):
            self.state='revised'
            


    #Acción para rechazar por documentación, esto se mandará la ventana de rechazo
    def action_for_reject_doc(self):
        if self.state in ('with_observations','published'):
            return {
            'name': _('Rechazo de solicitud de Ministraciones (Documentación)'),
            'type': ACT_WINDOW,
            'res_model': MONEY_FUND_REQ_REJECT,
            'view_mode': 'form',
            'views': [(self.env.ref("siif_currency_purchase_req.application_request_rejection_doc_form").id, 'form')],
            'context': {
                'type_request':self.move_type,
                'money_funds_req_info': self.id,
                'next_state':'reject_correction',
                'type':'doc',
                'request':self,
            },
            'target': 'new'
            }

    #Acción para aprobar
    def action_for_approve(self):
        if self.state=='revised':
            self.state='approved'
            self.name=self.money_fund.inc_seq.next_by_id()
            if self.is_first_request:
                self._generar_cuenta()

    #Acción para rechazar, se enviará la ventana para asignar el motivo de rechazo
    def action_for_reject(self):
        if self.state=='revised':
            return {
            'name': _('Rechazo de solicitud de '),
            'type': ACT_WINDOW,
            'res_model': MONEY_FUND_REQ_REJECT,
            'view_mode': 'form',
            'context': {
                'type_request':self.move_type,
                'money_funds_req_info': self.id,
                'next_state':'rejected',
                'type':'reject'
            },
            'views': [(self.env.ref("siif_currency_purchase_req.application_request_rejection_form").id, 'form')],
            'target': 'new'
            }

    #Acción para aprobar por la dependencia
    def action_for_accept_dep(self):
        money_fund = self.money_fund
        if self.state=='approved' and not self.is_first_request:
            self.state='accepted_dep'
        elif self.state == 'approved' and self.is_first_request:
            if not money_fund.account_journal:
                raise ValidationError(f'Falta asignar el Diario asociado al fondo fijo "{money_fund.name}". Favor de asignarlo.')
            elif not money_fund.bank_account_journal:
                raise ValidationError(f'Falta asignar la Cuenta bancaria asociada al fondo fijo "{money_fund.name}". Favor de asignarlo o continuar con la solicitud de apertura de cuenta.')    
            elif not (self.qr_signature_dep or money_fund.flag_firma):
                raise ValidationError('Se necesita registrar la firma antes de pasar al estado Aceptada por la dependencia. Favor de Firmar.')        
            else:
                self.state='accepted_dep'
    #Acción para autorizar de dfg
    def action_for_autho_dfg(self):
        if self.state=='accepted_dep':
            if self.qr_signature_dgf!=False or self.money_fund.flag_firma:
                self.state='authorized_dfg'
                self.action_for_gen_req_open_balance(self)
            else:
                raise ValidationError('Se necesita registrar la firma antes de pasar al estado Autorizado por la DGF. Favor de Firmar.')
    def action_for_cancel(self):
        if self.state=='reject_correction':
            self.state='cancelled'


    #Función para obtener la lista de lineas contables generadas por la solicitud de transferencia
    @api.depends('transfer_request')
    def _get_transfer_moves(self):
        if len(self.transfer_request.payment_ids.move_line_ids)!=0:
            self.transfer_acc_move_lines=self.transfer_request.payment_ids.move_line_ids
        else:
            self.transfer_acc_move_lines=False
    

    #==============================================================
    #-----------------Funciones del proceso
    #==============================================================
    #Acción para aprobar la solicitud y se genera la solicitud de transferencia
    def action_for_gen_req_open_balance(self,rec):
        if rec.state=='authorized_dfg':
            request={
            'state':'draft',
            'desti_bank_account_id':rec.money_fund.bank_account_journal.id,
            'desti_account':rec.money_fund.bank_account_journal.bank_account_id.id,
            'currency_id':rec.currency_id.id,
            'amount':rec.amount_request,
            'dependency_id':rec.dependency_id.id,
            'sub_dependency_id':rec.sub_dependency_id.id,
            'prepared_by_user_id':rec.env.user.id,
            'user_id':rec.env.user.id,
            'trasnfer_request':'finances',
            'date_required':date.today(),
            'from_money_fund':'add',
            }
            rec.transfer_request = self.env[REQ_OPEN_FINANCE].create(request)

    #Revisar información de fondos Fijos
    def _check_funds_info(self,rec):
        msg=""
        funds = rec.money_fund
        if len(funds)>0:
            msg="La dependencia cuenta con fondos fijos adicionales al solicitado: Sí\n"
        else:
            msg="La dependencia cuenta con fondos fijos adicionales al solicitado: No\n"

        if self.money_fund.state=='on_use':
            msg+="La dependencia cuenta con un fondo fijo con un monto total asignado de $"+str(funds.import_fund)+"\n"
        return msg
    #Función para detectar si la solicitud supera el fondo definido.
    def _check_mount_info(self,rec):
        msg=""
        if rec.amount_request>=self.env[MONEY_FUND_CONF].get_param('top'):    
            msg = "La dependencia solicita la cantidad de $"+str(rec.amount_request)+" mayor al tope definido de los fondos ($"+str(self.env[MONEY_FUND_CONF].get_param('top'))+")\n"
        return msg

    def _generar_cuenta(self):
        vals={
            'invoice':self.name,
            'bank_id':self.money_fund.bank_id.id,
            'ministrations_amount':self.amount_request,
            'move_type':'account open',

        }
        self.money_fund.request_account=self.env['request.accounts'].create(vals)

    def _get_info_request(self,type):
        tipo="<p><b>Tipo de Solicitud: </b>Solicitud de Asignación</p>"
        modelo="<p> <b>Fondo Fijo: </b>"+self.money_fund.name+"</p>"
        nombre="<p> <b>Identificador: </b>"+str(self.name)+"</p>"
        if type=='doc':
            dato="<p> <b>Observación de la documentación: </b>"+self.observations_doc+"</p>"
        elif type=='reject':
            dato="<p> <b>Observación de la documentación: </b>"+self.reject_reasons+"</p>"
        return tipo+modelo+nombre+dato
    
    
    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        res = super().fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])

        user = self.env.user
        is_admin = user.has_group(SIIF_ADMIN_USER)
        is_conta = user.has_group(SIIF_CONTA)
        is_dep_user = user.has_group(SIIF_DEP_USER)
        is_finance = user.has_group(SIIF_FINANCE)
        is_upa_user = user.has_group(SIIF_UPA_USER)
        is_director = user.has_group(SIIF_FINANCE_DIR)
        is_budget = user.has_group(SIIF_BUDGET)

        for node in doc.xpath("//" + view_type):
            if is_admin or is_dep_user:
                node.set('create', '1')
                node.set('import', '1')
                node.set('edit', '1')
                node.set('delete', '1')
                node.set('export_xlsx', '1')
                node.set('export', '1')
                node.set('duplicate', '1')
            else:
                node.set('create', '0')   
                node.set('edit', '0')
                node.set('create', '0')
                node.set('import', '0')
                node.set('delete', '0')
                node.set('export_xlsx', '0')
                node.set('export', '0')
                node.set('duplicate', '0')
            
            if is_upa_user:
                node.set('edit', '1')

        if (not (is_admin or is_conta or is_finance or is_director or is_budget or is_dep_user)) and ('toolbar' in res):
            res['toolbar']['print'] = []
            res['toolbar']['action'] = []

        for node in doc.xpath(XPATH_FIELD):
            node.set('options', XPATH_OPEN_POS if not (is_admin or is_dep_user or is_upa_user or is_director) else XPATH_OPEN_NEG)

        res['arch'] = etree.tostring(doc)

        return res

class MoneyFundsReqMinistration(models.Model):
    #Solicitudes de fondo fijo Ministraciones
    _name = 'money.funds.request.ministration'
    _inherit = "money.funds.request"
    
    state = fields.Selection(selection=[
            ('draft', 'Draft'),
            ('published', 'Published'),
            ('with_observations', WITH_OBSERVE),
            ('revised', 'Revised'),
            ('reject_correction', CORRECTION),
            ('approved', 'Approved'),
            ('rejected','Rejected'),
            ('deposited', 'Deposited')
        ], string='State',default='draft')
    status_time = fields.Selection(selection=[
        ('on_time','On Time'),
        ('delay','Delay'),
    ], string='Status Time',compute='_get_status_time')
        #Campos a Ingresar
    move_type = fields.Selection(selection=[
        ('adding','Adding'),
        ('ministration','Ministration')
    ],string="Move Type", compute="_compute_move_type")
    amount_request = fields.Monetary("Fund",currency_field='currency_id',default=0)  
    date_request = fields.Date('Date Receipt',default=date.today())
    trade_doc = fields.Binary(string="Trade Doc")
    trade_doc_name = fields.Char(string='Trade Doc')
    recived_doc = fields.Binary(string="Recived Doc")
    recived_doc_name = fields.Char(string='Recived Doc')
    justify_min_doc = fields.Binary(string="Justify Doc")
    justify_min_doc_name = fields.Char(string='Justify Doc')
    cover_doc = fields.Binary(string="Cover Doc")
    cover_doc_name = fields.Char(string='Cover Doc')

    is_published = fields.Boolean(string=PUBLISHED,default=False)

    account_topay_id = fields.Many2one(ACC_TOPAY,string='Associated Account to Pay')
    amount_request_ver = fields.Monetary("Fund Verificated",currency_field='currency_id',default=0)  
    verified_complete = fields.Boolean(string="Is verified?",default=False)

    num_account_moves = fields.Integer('All account moves',compute='_count_moves',default=0)
    num_return_moves = fields.Integer('All devolutions',compute='_count_return_moves',default=0)
    account_moves_ids = fields.One2many(ACC_MOVE, 'money_fund_move_id', string='Account Move Verification')
    
    min_account_id = fields.Many2one('account.account',string="Ministration Account",required=True)
    
    #Firmas electrónicas Dependencia
    feu_signature_dep = fields.Many2one(SIDIA_SIGNATURE, string=FIRMA, store=True, tracking= True)
    qr_signature_dep = fields.Binary(related="feu_signature_dep.qr_image", string=FIRMA, readonly=True)
    signature_name_dep = fields.Text(related="feu_signature_dep.signature_name", string="Nombre", readonly=True, store=True)
    curp_feu_dep = fields.Char(related="feu_signature_dep.curp", string="CURP", readonly=True , store=True)
    date_feu_dep = fields.Datetime(related="feu_signature_dep.create_date", string=SIGNED, readonly=True)
    
    #fechas
    date_approved = fields.Date('Date Approved')
    date_authorized = fields.Date('Date Authorized')
    justify_info = fields.Text('Justify Info')
    @api.model
    def create(self, vals):
        if vals.get('has_funds')==True:
            res = super(MoneyFundsReqMinistration, self).create(vals)
            res.name = self.env[IR_SEQ].next_by_code(MONEY_FUND_REQ_MIN)
            return res
        else:
            raise UserError(_('It cant be a ministration request if doesnt have a currency fund.'))
        
    def unlink(self):
        if self.state not in ['draft']:
            raise UserError(_('You cannot delete a ministration which has been not draft state.'))
        return super(MoneyFundsReqMinistration, self).unlink()
        
    #==============================================================
    #-----------------------Revisar datos
    #==============================================================
    @api.onchange('money_fund')
    def _assign_money_fund(self):
        self.min_account_id=self.env[MONEY_FUND_CONF].get_param('account_ministration').id

    #Checar si el monto de la ministración es correcto
    @api.constrains('amount_request')
    def _check_amount_request(self):
        if self.has_funds:
            if self.amount_request<=0:
                raise ValidationError(_(LIMIT_AMOUNT_MSG))
            else:
                if self.amount_request>self.money_fund.import_fund:
                    raise ValidationError('El monto de la ministración es mayor al dinero asignado del fondo fijo. Corregir.')
                
    #Revisar si las ministraciones estan en estado de comprobación: En tiempo o retrasado            
    @api.onchange('date_request')
    def _get_status_time(self):
        for rec in self:
            if rec.write_date and rec.date_request:
                write_date = dateutil.parser.parse(str(rec.write_date)).date()
                if write_date <= rec.date_request and date.today()<=rec.date_request:
                    rec.status_time = 'on_time'
                else:
                    rec.status_time = 'delay'
            else:
                rec.status_time = None

    """Revisar si se asignaron los archivos necesarios
            -Solicitud de ministración
            -Recibo de ministración
            -Justificación de ministración
            -Carátula del estado de la cuenta
    """
    @api.constrains('trade_doc')
    def _check_trade_doc(self):
        if self.has_funds and self.trade_doc==False:
            raise ValidationError('El oficio de solicitud de ministración no se agregó a la solicitud. Revisar.')

    @api.constrains('recived_doc')
    def _check_recived_doc(self):
        if self.has_funds and self.recived_doc==False:
            raise ValidationError('El recibo de la ministración no se agregó a la solicitud. Revisar.')
            
    @api.constrains('justify_min_doc')
    def _check_justify_min_doc(self):
        if self.has_funds and self.justify_min_doc==False:
            raise ValidationError('El documento de justificación de la ministración no se agregó a la solicitud. Revisar.')
            
    @api.constrains('cover_doc')
    def _check_cover_doc(self):
        if self.has_funds and self.cover_doc==False:
            raise ValidationError('La carátula del estado de la cuenta del fondo fijo no se agregó a la solicitud. Revisar.')
    

    #Contar solicitudes asociadas
    def _count_moves(self):
        self.num_account_moves = len(self.account_moves_ids)

    #Contar devoluciones asociadas
    def _count_return_moves(self):
        self.num_return_moves = len(self.env[MONEY_FUND_REQ_RETURN].search([('ministration_id','=',self.id),('state','=','recived_deposit')]))
    
    """
        Obtener las ministraciones asociadas al fondo fijo de la nueva ministración para ver si hay
        aún ministraciones pendientes de comprobar
    """
    @api.onchange('has_funds','money_fund')
    def _compute_min_request(self):
        if self.has_funds:
            if self.state!='deposited':
                self.min_request_id = self.env[MONEY_FUND_REQ_MIN].search([('money_fund','=',self.money_fund.id),('state','=','deposited'),('verified_complete','=',False)])
            else:
                self.min_request_id = self.env[MONEY_FUND_REQ_MIN].search([('id','!=',self.id),('money_fund','=',self.money_fund.id),('state','=','deposited'),('verified_complete','=',False)])
        else:
            self.min_request_id = False
        
    @api.onchange('has_funds')
    def _compute_move_type(self):
        for rec in self:
            if rec.has_funds:
                rec.move_type='ministration'
            else:
                rec.move_type=False
    #==============================================================
    #-----------------Acciones de Vista
    #==============================================================

    #Botón para devolver la solicitud a Borrador
    def action_for_draft(self):
        self.state='draft'

    #Botón para devolver la solicitud a Publicada
    def action_for_publish(self):
        if self.has_funds and self.state in ('draft','reject_correction'):
            if self.min_account_id!=False:
                if self.qr_signature_dep!=False or self.money_fund.flag_firma:
                    self.state='published'
                else:
                    raise ValidationError('Se necesita registrar la firma antes de pasar al estado Publicada por la dependencia. Favor de Firmar.')
            else:
                raise ValidationError('Se necesita registrar la Cuenta de la ministración. Favor de asignar.')
    
    # Botón para revisar la solicitud, se manda a Publicada si no se tiene 
    # otras ministraciones.
    # SI hay más ministraciones, se anda a Con Obsevaciones
    def action_for_observations(self):
        ministraciones = self.env[MONEY_FUND_REQ_MIN].search([('money_fund','=',self.money_fund.ids),('id','!=',self.id),('state','=','deposited'),('verified_complete','=',False)])
        if self.has_funds and self.state=='published':
            if len(ministraciones)>0:
                self.observations=self._get_min_info(ministraciones)
                self.state='with_observations'
                self.is_published=True
            else:
                self.state='published'
                self.is_published=True
            
    #Mandar a revisada la solicitud
    def action_for_revised(self):
        if self.state in ('with_observations','published'):
            self.state='revised'
    #Mandar la ventana para asignar la corrección de los documentos
    def action_for_reject_doc(self):
        if self.state in ('with_observations','published'):
            return {
            'name': _('Rechazo de solicitud de Ministraciones (Documentación)'),
            'type': ACT_WINDOW,
            'res_model': MONEY_FUND_REQ_REJECT,
            'view_mode': 'form',
            'view_type': 'form',
            'context': {
                'type_request':self.move_type,
                'money_funds_req_info': self.id,
                'next_state':'reject_correction',
                'type':'doc'
            },
            'views': [(False, 'form')],
            'target': 'new'
            }
    #Acción para aprobar la solicitud y se genera la solicitud de transferencia
    def action_for_approve(self):
        if self.state=='revised':
            ministration_journal=self.env[MONEY_FUND_CONF].get_param('journal_ministration')
            self.date_approved=date.today()
            self.state='approved'
            
            request={
            'state':'draft',
            'desti_bank_account_id':ministration_journal.id,
            'desti_account':ministration_journal.bank_account_id.id,
            'currency_id':self.currency_id.id,
            'amount':self.amount_request,
            'dependency_id':self.dependency_id.id,
            'sub_dependency_id':self.sub_dependency_id.id,
            'prepared_by_user_id':self.env.user.id,
            'user_id':self.env.user.id,
            'trasnfer_request':'finances',
            'date_required':date.today(),
            'from_money_fund':'min',
            }
            self.transfer_request = self.env[REQ_OPEN_FINANCE].create(request)
    
    #Mandar la ventana para asignar los motivos de rechazo
    def action_for_reject(self):
        if self.state=='revised':
            return {
            'name': _('Rechazo de solicitud de Ministraciones'),
            'type': ACT_WINDOW,
            'res_model': MONEY_FUND_REQ_REJECT,
            'view_mode': 'form',
            'view_type': 'form',
            'context': {
                'type_request':self.move_type,
                'money_funds_req_info': self.id,
                'next_state':'rejected',
                'type':'reject'
            },
            'views': [(False, 'form')],
            'target': 'new'
            }

    #==============================================================
    #----------------------Funciones
    #==============================================================
    

    #Función para obtener la lista de lineas contables generadas por la solicitud de transferencia
    @api.depends('transfer_request')
    def _get_transfer_moves(self):
        if len(self.transfer_request.payment_ids.move_line_ids)!=0:
            self.transfer_acc_move_lines=self.transfer_request.payment_ids.move_line_ids
            if(self.state=='approved'):
                self.state = 'deposited'
                self.date_authorized=date.today()
                self.name = self.money_fund.min_seq.next_by_id()
        else:
            self.transfer_acc_move_lines=False

    def _create_acc_topay(self,rec):
        request={
            'state':'draft',
            'secretary_admin':rec.money_fund.name_secretary,
            'secretary_admin_email':rec.money_fund.email_secretary,
            'dependency_id':rec.dependency_id.id,
            'sub_dependency_id':rec.sub_dependency_id.id,
            'date_receipt':date.today(),
            'invoice_date':date.today(),
            'register_date':date.today(),
            'is_for_ministration': True,
        }
        account_topay = self.env[ACC_TOPAY].create(request)
        return account_topay.id
    
    #Función para obtener las solicitudes de pago de comprobación
    def get_acc_moves(self, id, monto):
        ac_move = self.env[ACC_MOVE].search([('id','=',id)])
        if ac_move !=False:
            cxp = self.env[ACC_TOPAY].search([('id','=',ac_move.previous_number.id)])
            ministration = self.env[MONEY_FUND_REQ_MIN].search([('account_topay_id','=',cxp.id)])
            ministration.account_moves_ids = [(4,id)]
            
            ministration.write({
                'amount_request_ver':ministration.amount_request_ver+monto,
            })
            if ministration.amount_request_ver==ministration.amount_request:
                ministration.write({'verified_complete':True})
            else:
                ministration.write({'verified_complete':False})
    #Función para mostrar las solicitudes de pago de comprobación
    def check_account_move_info(self):
        return {
            'name': 'Solicitudes de comprobación',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'views': [(self.env.ref("siif_account_to_pay.payment_req_account_topay_view").id, 'tree'), (self.env.ref("siif_supplier_payment.payment_requests_general").id, 'form')],
            'res_model': ACC_MOVE,
            'domain': [('id', 'in', self.account_moves_ids.ids)],
            'type': ACT_WINDOW,
            'context':{'payment_request': True,
                       'show_for_supplier_payment':True,
                       'default_type': 'in_invoice', 
                       'default_is_payment_request': 1,
                       'from_move': 1, 
                       'group_by': 'l10n_mx_edi_payment_method_id', 
                       'default_payment_issuance_method': 'manual'
            }
        }
     #Función para mostrar las solicitudes de pago de devoluciones
    def check_return_move_info(self):
        return {
            'name': 'Devoluciones',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': MONEY_FUND_REQ_RETURN,
            'domain': [('ministration_id', '=', self.id),('state','=','recived_deposit')],
            'type': ACT_WINDOW,
        }
    def _get_info_request(self,type):
        tipo="<p><b>Tipo de Solicitud: </b>Solicitud de Ministración</p>"
        modelo="<p><b>Fondo Fijo: </b>"+self.money_fund.name+"</p>"
        nombre="<p><b>Identificador: </b>"+str(self.name)+"</p>"
        if type=='doc':
            dato="<p> <b> Observación de la documentación: </b>"+self.observations_doc+"</p>"
        elif type=='reject':
            dato="<p> <b> Observación de la documentación: </b>"+self.reject_reasons+"</p>"
        return tipo+modelo+nombre+dato

    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        res = super().fields_view_get(view_id=view_id,
                                      view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])

        is_admin = self.env.user.has_group(SIIF_ADMIN_USER)
        is_conta = self.env.user.has_group(SIIF_CONTA)
        is_dep_user = self.env.user.has_group(SIIF_DEP_USER)
        is_finance = self.env.user.has_group(SIIF_FINANCE)
        is_upa_user = self.env.user.has_group(SIIF_UPA_USER)
        is_director = self.env.user.has_group(SIIF_FINANCE_DIR)
        is_budget = self.env.user.has_group(SIIF_BUDGET)

        #Vista para el FONDO FIJO
        #   Gestión del fondo fijo

        
        for node in doc.xpath("//" + view_type):
            if (is_admin or is_dep_user):
                node.set('create', '1')
                node.set('import', '1')
                node.set('edit', '1')
                node.set('delete', '1')
                node.set('export_xlsx', '1')
                node.set('export', '1')
                node.set('duplicate', '1')
            else:    
                node.set('create', '0')
                node.set('import', '0')
                node.set('edit', '0')
                node.set('delete', '0')
                node.set('export_xlsx', '0')
                node.set('export', '0')
                node.set('duplicate', '0')

            if (is_upa_user):
                node.set('edit', '1')
                    
        if (not (is_admin or is_conta or is_finance or is_director or is_budget or is_dep_user)) and ('toolbar' in res):
            res['toolbar']['print'] = []
            res['toolbar']['action'] = []
    
        for node in doc.xpath(XPATH_FIELD):
            node.set('options', XPATH_OPEN_POS if not (is_admin or is_dep_user or is_upa_user or is_director) else XPATH_OPEN_NEG)

        res['arch'] = etree.tostring(doc)

        return res
            
class MoneyFundsReqReturns(models.Model):
    #Solicitudes de fondo fijo Ministraciones
    _name = 'money.funds.request.returns'
    _inherit = "money.funds.request"
    
    state = fields.Selection(selection=[
            ('draft', 'Draft'),
            ('published', 'Published'),
            ('recived_deposit', 'Recived Deposit'),
            ('return_confirm', 'Return Confirmed')
        ], string='State',default='draft')

    amount_request = fields.Monetary("Fund",currency_field='currency_id',default=0)  
    withdrawal_date = fields.Date('Withdrawal Date')
    interest_date = fields.Date('Interest Date')
    excedent_date = fields.Date('Excedent Date')
    from_other_exercise = fields.Boolean('Is from other exercise', default=False)
    return_type = fields.Selection([
        ('partial','Resources Partial Returns'),
        ('interest','Interests Returns'),
        ('excedent','Excedent Returns'),
        ('refund','Refund'),
        ('ministration','Ministration')
    ],string="Return Type")  
    #amount
    reasons = fields.Text(string="Refund Reasons")

    
    deposit_files = fields.Many2many(IR_ATTACH,'money_funds_request_return_attachment_rel', string=ADMIN_FORM)

    bank_ref = fields.Many2one(BANK_REF,string='Bank Reference')
    acc_move_lines = fields.One2many("account.move.line", 'money_fund_request_ret_transfer_id', string="Money Fund Payment",tracking=True)
    
    domain_ministration_id = fields.Char(compute='_get_domain_ministration',store=False)
    ministration_id = fields.Many2one(MONEY_FUND_REQ_MIN,string="Ministration")
    
    #Campos para las referencias bancarias
    folio  =  fields.Char(string="Folio")
    bank_move_id  =  fields.Many2one('bank.movements',string="Movimiento Bancario")


    
    @api.model
    def create(self, vals):
        
        if vals.get('has_funds')==True:
            res = super(MoneyFundsReqReturns, self).create(vals)
            res.name = self.env[IR_SEQ].next_by_code(MONEY_FUND_REQ_RETURN)
            return res
        else:
            raise UserError(_('It cant be a return request if doesnt have a currency fund.'))

    def unlink(self):
        if self.state not in ['draft']:
            raise UserError(_('You cannot delete a return request which has been not draft state.'))
        return super(MoneyFundsReqReturns, self).unlink()

    #==============================================================
    #-----------------------Revisar datos
    #==============================================================
    @api.constrains('amount_request')
    def _check_amount_request(self):
        if self.has_funds and self.amount_request <= 0:
            raise ValidationError('El monto asignado es menor o igual a 0, favor de asignar un valor válido.')
        
        if self.has_funds and self.amount_request > 0:
            if self.return_type != 'ministration' and self.amount_request > self.money_fund.import_fund:
                raise ValidationError('El monto asignado es mayor al del fondo fijo.')

            if self.return_type == 'ministration' and self.amount_request > (self.ministration_id.amount_request - self.ministration_id.amount_request_ver):
                raise ValidationError('El monto asignado es mayor al monto de la ministración.')

    #Revisar si se seleccionó un tipo de devolución
    @api.constrains('return_type')
    def _check_return_type(self):
        if self.return_type==False:
            raise ValidationError('Se necesita seleccionar un tipo de devolución para continuar')
    
    #Revisar si en la devolución de la ministración se seleccionó
    @api.constrains('ministration_id')
    def _check_ministration(self):
        if self.has_has_funds and self.return_type=='ministration' and self.ministration_id==False:
            raise ValidationError('No se tiene asignado la ministración para la Devolución de remanentes de ministración. Favor de seleccionarla')

    #Revisión de fechas requeridas para una devolución
    @api.constrains('withdrawal_date','interest_date','excedent_date')
    def _check_date(self):
        # Revisa si es válida la fecha según el tipo de devolución
        if self.return_type in ('partial', 'refund', 'ministration'):
            date_attr = self.withdrawal_date
            date_type = "Fecha de Devolución"

        elif self.return_type == 'interest':
            date_attr = self.interest_date
            date_type = "Fecha de Devolución por Intereses"

        elif self.return_type == 'excedent':
            date_attr = self.excedent_date
            date_type = "Fecha de Devolución por Excedente"

        if not date_attr:
            raise UserError(f"No se registró la {date_type}. Favor de asignar.")

        if date_attr.year != date.today().year:
            raise ValidationError(f"Solamente se puede asignar la {date_type} del año en curso.")


    #Se obtienen las ministraciones por comprobar y ya tengan cuenta por pagar
    @api.onchange('money_fund')
    def _get_domain_ministration(self):
        for record in self:
            if record.dependency_id!=None and record.sub_dependency_id!=None and record.money_fund!=None:
                self.domain_ministration_id=json.dumps([('money_fund','=',record.money_fund.id),('verified_complete','=', False),('state','=','deposited'),('account_topay_id.state','=','approved')])

    #Se checa si se tiene una ministración valida para la solicitud de devolución
    @api.onchange('return_type')
    def _check_ministration(self):
        if self.return_type!='ministration':
            self.ministration_id=False

    #Se asignará la fecha de hoy dependiendo del tipo de devolución
    @api.onchange('return_type')
    def _change_dates(self):
        if self.return_type in ('partial','ministration','refund'):
            self.withdrawal_date=date.today()
            self.interest_date=False
            self.excedent_date=False
        elif self.return_type=='interest':
            self.withdrawal_date=False
            self.interest_date=date.today()
            self.excedent_date=False
        elif self.return_type=='excedent':
            self.withdrawal_date=False
            self.interest_date=False
            self.excedent_date=date.today()

    #Al asignar la ministración se asigna de manera directa el monto que aun falta por comprobar
    @api.onchange('ministration_id')
    def _change_amount_request(self):
        if self.return_type=='ministration' and self.ministration_id:
            self.amount_request=self.ministration_id.account_topay_id.available

   
        
    #==============================================================
    #--------------------Acciones
    #==============================================================
    #Botón para publicar la solicitud de cancelación
    def action_for_publish(self):
        if self.state=='draft':
            if len(self.deposit_files)!=0:
                self.state='published'
                self.name = self.money_fund.dev_seq.next_by_id()
                self.folio = self.env[IR_SEQ].next_by_code(BANK_REF)
            else:
                raise ValidationError('No se tienen asignados los archivos de la devolución. Favor de asignarlos.')

    #Generar la referencia bancaria 
    def generate_bank_ref(self):
        if self.state=='published':
            return {
            'name': _('Generar Referencia Bancaria'),
            'type': ACT_WINDOW,
            'res_model': 'wiz.bank.reference',
            'view_mode': 'form',
            'view_type': 'form',
            'context': {
                'model':'returns',
                'id_request': self.id,
                'default_reference':self.env[BANK_REF]._gen_short_ref(self.dependency_id,self.sub_dependency_id,self.folio,'money_return'),
                'default_date': date.today(),
                'default_amount':self.amount_request,
            },
            'views': [(False, 'form')],
            'target': 'new'
            }

    #Acción para aprobar la solicitud y se genera la solicitud de transferencia
    def action_for_approve(self):
        if self.state=='recived_deposit':
            request={
            'state':'draft',
            'desti_bank_account_id':self.money_fund.bank_account_journal.id,
            'desti_account':self.money_fund.bank_account_journal.bank_account_id.id,
            'currency_id':self.currency_id.id,
            'amount':self.amount_request,
            'dependency_id':self.dependency_id.id,
            'sub_dependency_id':self.sub_dependency_id.id,
            'prepared_by_user_id':self.env.user.id,
            'user_id':self.env.user.id,
            'trasnfer_request':'finances',
            'date_required':date.today(),
            }
            self.transfer_request = self.env[REQ_OPEN_FINANCE].create(request)



    #Función para obtener la lista de lineas contables generadas por la solicitud de transferencia
    @api.depends('transfer_request')
    def _get_transfer_moves(self):
        if len(self.transfer_request.payment_ids.move_line_ids)!=0:
            self.transfer_acc_move_lines=self.transfer_request.payment_ids.move_line_ids
            if(self.state=='recived_deposit'):
                self.state = 'return_confirm'
        else:
            self.transfer_acc_move_lines=False

    #Revisar si existe el cri
    def _get_cri_info(self,account):
        if ('411' in account.code) or ('410' in account.code):
            cri_account = self.env['relation.account.cri'].search([('cuentas_id','=',account.id)])
            if cri_account:
                return True,cri_account
            else:
                return False,False
        else:
            return False,False

    #Lineas del CRI para las devoluciones
    def _generate_account_lines_cri(self,money_fund_req,type):
        
        lines=[]
        amount=money_fund_req.amount_request
        partner_id = self.env.user.partner_id.id
        journal = money_fund_req.money_fund.account_journal
        account_dgf=self.env[BANK_REF_CONF].get_param('account')
        #================================================================================================
        #Devoluciones por Intereses
        #================================================================================================
        if type=='interest':
            #Obtener fecha propuesta y fecha de afectación
            proposed_date = money_fund_req.interest_date
            date_affectation = money_fund_req.interest_date
            #Linea del banco
            lines.append((0, 0, {
                'date':date.today(),
                'name':'Banco '+money_fund_req._get_name_for_lines(),
                'account_id': account_dgf.id,
                'debit':money_fund_req.amount_request,
            }))

            #Intereses actuales
            if not money_fund_req.from_other_exercise:
                #Obtención del nombre de la devolución
                name_ref='Devolución por interes %s' % money_fund_req.name,
                interest = self.env[MONEY_FUND_CONF].get_param('account_return_interest')
                #Linea de la cuenta productiva de interes
                lines.append((0, 0, {
                    'date':date.today(),
                    'name':'Intereses Cuentas Productivas '+money_fund_req._get_name_for_lines(),
                    'account_id': interest.id,
                    'credit':money_fund_req.amount_request,
                }))
                #Asignación de valores para bitácora de movimientos contables
                origin_type = 'income-dev-int'
                account=interest

            #Intereses ejercicios anteriores
            else:
                #Obtención del nombre de la devolución
                name_ref='Devolución por interes (Ejercicio Anterior) %s' % money_fund_req.name,
                interest_last_exercise = self.env[MONEY_FUND_CONF].get_param('account_return')
                #Linea de cuenta productiva de interes de ejercicio anterior
                lines.append((0, 0, {
                    'date':date.today(),
                    'name':'Recuperaciones Ejercicios Anteriores '+money_fund_req._get_name_for_lines(),
                    'account_id': interest_last_exercise.id,
                    'credit':money_fund_req.amount_request,
                }))
                #Asignación de valores para la bitácora movimientos contables
                origin_type = 'income-dev-int-ant'
                account=interest_last_exercise
            #Se revisa si existe un CRI asociado a la cuenta 
            ok,cri_name= self._get_cri_info(account)
        

        #================================================================================================
        #Devolución por excedente
        #================================================================================================
        elif type=='excedent':
            #Obtener fecha propuesta y fecha de afectación
            proposed_date = money_fund_req.excedent_date
            date_affectation = money_fund_req.excedent_date
            
            lines.append((0, 0, {
                'date':date.today(),
                'name':'Bancos '+money_fund_req._get_name_for_lines(),
                'account_id': account_dgf.id,
                'debit':money_fund_req.amount_request,
            }))
            #Excedentes actuales
            if not money_fund_req.from_other_exercise:
                #Obtención de la devolución
                name_ref='Devolución de excedente %s' % money_fund_req.name,
                excedent = self.env[MONEY_FUND_CONF].get_param('account_return_excedent')
                lines.append((0, 0, {
                    'date':date.today(),
                    'name':'Otros Aprovechamientos '+money_fund_req._get_name_for_lines(),
                    'account_id': excedent.id,
                    'credit':money_fund_req.amount_request,
                }))
                #Asignación de valores para bitácora de movimientos contables
                origin_type = 'income-dev-exc'
                account=excedent
            #Excedentes de ejercicios anteriores
            else:
                name_ref='Devolución de excedente (Ejercicio Anterior) %s' % money_fund_req.name,
                excedent_last_exercise = self.env[MONEY_FUND_CONF].get_param('account_return')
                #Linea de cuenta productiva de interes de ejercicio anterior
                lines.append((0, 0, {
                    'date':date.today(),
                    'name':'Recuperación Ejercicios anteriores '+money_fund_req._get_name_for_lines(),
                    'account_id': excedent_last_exercise.id,
                    'credit':money_fund_req.amount_request,
                }))
                #Linea de la cuenta productiva de interes
                origin_type = 'income-dev-exc-ant'
                account=excedent_last_exercise
            #Se revisa si existe un CRI asociado a la cuenta 
            ok,cri_name= self._get_cri_info(account)

        #================================================================================================
        #Asientos contables generados por el uso de las cuentas 411
        #================================================================================================
        if ok:
            #Apunte contable (814 y 813) Recaudado
            lines.append((0, 0, {
                'name': 'RECAUDADO '+money_fund_req._get_name_for_lines(),
                'account_id': journal.recover_income_credit_account_id.id,
                'coa_conac_id': journal.conac_recover_income_credit_account_id.id,
                'credit': amount,
                'partner_id': partner_id,
                'cri': cri_name.cri.name
                }))
            lines.append((0, 0, {
                'name': 'DEVENGADO '+money_fund_req._get_name_for_lines(),
                'account_id': journal.recover_income_debit_account_id.id,
                'coa_conac_id': journal.conac_recover_income_debit_account_id.id,
                'debit': amount, 
                'partner_id': partner_id,
                'cri': cri_name.cri.name
                }))
            # Apuntes contables (811 y 813) Ganado
            lines.append((0, 0, {
                'name': 'DEVENGADO ' +money_fund_req._get_name_for_lines(),
                'account_id': journal.accrued_income_credit_account_id.id, 
                'coa_conac_id': journal.conac_accrued_income_credit_account_id.id,
                'credit': amount, 
                'partner_id': partner_id,
                'cri': cri_name.cri.name
                }))
            lines.append((0, 0, {
                'name': 'POR EJECUTAR '+money_fund_req._get_name_for_lines(),
                'account_id': journal.accrued_income_debit_account_id.id,
                'coa_conac_id': journal.conac_accrued_income_debit_account_id.id,
                'debit': amount,
                'partner_id': partner_id,
                'cri': cri_name.cri.name
                }))

            #Creación de la poliza
            move_val = {
                'ref': name_ref,
                'conac_move': False,
                'date': datetime.now(),
                'journal_id': journal.id,
                'company_id': self.env.user.company_id.id,
                'line_ids': lines,
                }
                                        
            #Creación del asiento       
            unam_move = self.env[ACC_MOVE].create(move_val)
            unam_move.action_post()
            money_fund_req.acc_move_lines=unam_move.line_ids
            self._make_returns_actions(money_fund_req)


            # Afectación al presupuesto
            type_affectation = 'eje-rec'
            date_accrued = False
            # Mandar datos a la clase de afectación
            cri_id = cri_name.cri.id
            self.env['affectation.income.budget'].search([]).affectation_income_budget(type_affectation,cri_id,date_accrued,date_affectation,amount)
            obj_logbook = self.env['income.adequacies.function']
           
            # Guarda la bitacora de la adecuación
            # Objeto de la tabla año fiscal
            obj_fiscal_year = self.env['account.fiscal.year']
            # Busca el id del año fiscal
            fiscal_year_id = obj_fiscal_year.search([('name', '=',date.today().year)])
            
            type_income = 'income'
            movement = 'for_exercising_collected'
            id_account = unam_move.name
            journal_id = journal.id
            update_date_record = datetime.today()
            update_date_income = datetime.today()
            obj_logbook.search([]).logbook_income_budget(fiscal_year_id.id,cri_id,proposed_date,type_income,movement,origin_type,id_account,journal_id,update_date_record,amount,update_date_income) 
        else:
            raise UserError("No se encontró el CRI asociado a la cuenta %s" % account.code)



    #Obtener nombre para las lineas
    def _get_name_for_lines(self):
        return '%s %s' % (self.name,self.money_fund.name)

    #Generación de asientos contables para devoluciones
    def _generate_returns_account_moves(self,money_fund_return):
        lines=[]
        money_fund_info=money_fund_return.money_fund
        account_dgf=self.env[BANK_REF_CONF].get_param('account')
        #===============================================================
        #Asientos contables devoluciones parciales
        #===============================================================
        if money_fund_return.return_type=='partial':
            logging.critical("Parciales")
            lines.append((0, 0, {
                'date':date.today(),
                'name':'Banco M.N. '+money_fund_return._get_name_for_lines(),
                'account_id': account_dgf.id,
                'debit':money_fund_return.amount_request,
            }))
            lines.append((0, 0, {
                'date':date.today(),
                'name':'Banco M.N. ' +money_fund_return._get_name_for_lines(),
                'account_id': money_fund_info.bank_account_journal.default_credit_account_id.id,
                'credit':money_fund_return.amount_request,
            }))
            #Creación de la poliza
            move_val = {
                'ref': "Devolución parcial de recursos "+money_fund_return.money_fund.name,
                'conac_move': False,
                'date': datetime.now(),
                'journal_id': money_fund_info.account_journal.id,
                'company_id': self.env.user.company_id.id,
                'line_ids': lines,
                }
            # Creación de los apuntes contables
            move = self.env[ACC_MOVE].create(move_val)
            move.action_post()
            money_fund_return.acc_move_lines=move.line_ids
            self._make_returns_actions(money_fund_return)
            
        #===============================================================
        #Asientos contables devoluciones por intereses
        #===============================================================
        elif money_fund_return.return_type=='interest':
            self._generate_account_lines_cri(money_fund_return,'interest')
            
        #===============================================================
        #Asientos contables devoluciones por excedente
        #===============================================================
        elif money_fund_return.return_type=='excedent':
            self._generate_account_lines_cri(money_fund_return,'excedent')
            
        #===============================================================
        #Asientos contables reintegro al fondo fijo
        #===============================================================
        elif money_fund_return.return_type=='refund':
            lines.append((0, 0, {
                'date':date.today(),
                'name':'Bancos '+money_fund_return._get_name_for_lines(),
                'account_id': account_dgf.id,
                'debit':money_fund_return.amount_request,
            }))
            lines.append((0, 0, {
                'date':date.today(),
                'name':'Fondos Fijos controlados DGF '+money_fund_return._get_name_for_lines(),
                'account_id': money_fund_info.bank_account_journal.default_credit_account_id.id,
                'credit':money_fund_return.amount_request,
            }))
            #Creación de Poliza
            move_val = {
                'ref': "Reintegro al Fondo Fijo "+money_fund_return.money_fund.name,
                'conac_move': False,
                'date': datetime.now(),
                'journal_id': money_fund_info.account_journal.id,
                'company_id': self.env.user.company_id.id,
                'line_ids': lines,
                }
            # Creación de los apuntes contables
            move = self.env[ACC_MOVE].create(move_val)
            move.action_post()
            money_fund_return.acc_move_lines=move.line_ids
            self._make_returns_actions(money_fund_return)
        #===============================================================
        #Asientos contables Devolución de remanentes de ministración especifica
        #===============================================================
        elif money_fund_return.return_type=='ministration':

            lines.append((0, 0, {
                'date':date.today(),
                'name':'Bancos M.N. '+money_fund_return._get_name_for_lines(),
                'account_id': account_dgf.id,
                'debit':money_fund_return.amount_request,
            }))
            lines.append((0, 0, {
                'date':date.today(),
                'name':'Ministración Específica de Recursos '+money_fund_return._get_name_for_lines(),
                'account_id': money_fund_return.ministration_id.min_account_id.id,
                'credit':money_fund_return.amount_request,
            }))

            move_val = {
                'ref': "Cancelación de Cuentas por pagar de ministración "+money_fund_return.money_fund.name,
                'conac_move': False,
                'date': datetime.now(),
                'journal_id': money_fund_info.account_journal.id,
                'company_id': self.env.user.company_id.id,
                'line_ids': lines,
                }
        
            # Creación de los apuntes contables
            move = self.env[ACC_MOVE].create(move_val)
            move.action_post()
            money_fund_return.acc_move_lines=move.line_ids
            self._make_returns_actions(money_fund_return)


    #Función para actualizar la ministración asciada
    def _update_ministration(self):
        self.ministration_id.verified_complete=True
        self.ministration_id.amount_request_ver+=self.amount_request

    
    

    #Función para acciones varias de devoluciones
    def _make_returns_actions(self,money_fund_return):
        type_return=money_fund_return.return_type
        money_fund_info=money_fund_return.money_fund
        #Acción para disminuir el monto del importe del fondo fijo
        if type_return =='partial':
            money_fund_info.import_fund=money_fund_info.import_fund-money_fund_return.amount_request
        #Actualización de la cuenta por pagar asociada y de a ministración
        elif type_return =='ministration':
            money_fund_return.ministration_id.account_topay_id.cancel_used_account()
            money_fund_return._update_ministration()

        #Acción para crear la trasnferencia bancaria para el reintegro al fondo fijo
        elif type_return=='refund':
            
            request={
            'state':'draft',
            'desti_bank_account_id':money_fund_return.money_fund.bank_account_journal.id,
            'desti_account':money_fund_return.money_fund.bank_account_journal.bank_account_id.id,
            'currency_id':money_fund_return.currency_id.id,
            'amount':money_fund_return.amount_request,
            'dependency_id':money_fund_return.dependency_id.id,
            'sub_dependency_id':money_fund_return.sub_dependency_id.id,
            'prepared_by_user_id':money_fund_return.env.user.id,
            'user_id':money_fund_return.env.user.id,
            'trasnfer_request':'finances',
            'date_required':date.today(),
            'from_money_fund':'refund',
            }
            money_fund_return.transfer_request = self.env[REQ_OPEN_FINANCE].create(request)
        #Actualiza el estado a Devolución realizada si no es por reintegro.
        if type_return!='refund':
            money_fund_return.state='recived_deposit'


    #=======================================================================
    #Permisos de vista
    #=======================================================================
    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        res = super().fields_view_get(view_id=view_id,
                                      view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])

        is_admin = self.env.user.has_group(SIIF_ADMIN_USER)
        is_dep_user = self.env.user.has_group(SIIF_DEP_USER)

        #Vista para el FONDO FIJO
        #   Gestión del fondo fijo

        
        for node in doc.xpath("//" + view_type):
            if (is_admin or is_dep_user):
                node.set('create', '1')
                node.set('import', '1')
                node.set('edit', '1')
                node.set('delete', '1')
                node.set('export_xlsx', '1')
                node.set('export', '1')
                node.set('duplicate', '1')
            else:    
                node.set('create', '0')
                node.set('edit', '0')
                node.set('create', '0')
                node.set('import', '0')
                
                node.set('delete', '0')
                node.set('export_xlsx', '0')
                node.set('export', '0')
                node.set('duplicate', '0')


        if not(is_admin or is_dep_user) and 'toolbar' in res:
            res['toolbar']['print'] = []
            res['toolbar']['action'] = []

        for node in doc.xpath(XPATH_FIELD):
            if not(is_admin or is_dep_user):
                node.set('options',XPATH_OPEN_POS)
            else:
                node.set('options',XPATH_OPEN_NEG)
                

        res['arch'] = etree.tostring(doc)

        return res
    
    
    
        

class MoneyFundsCancelation(models.Model):
    #Solicitudes de fondo fijo Incremento
    _name = 'money.funds.cancel'
    _inherit = "money.funds.request"
    state = fields.Selection(selection=[
            ('draft', 'Draft'),
            ('published', 'Published'),
            ('with_observations', WITH_OBSERVE),
            ('revised','Revised'),
            ('rejected_correction', CORRECTION),
            ('approved', 'Approved'),
            ('rejected','Rejected'),
            ('recived_deposit', 'Recived Deposit'),
            ('cancelled', 'Cancelled'),
        ], string='State',default='draft')
    is_published = fields.Boolean(string=PUBLISHED,default=False)
    no_cancel_files = fields.Integer(string="Cancel Doc Number")
    cancel_files = fields.Many2many(IR_ATTACH,'money_funds_cancel_attachment_rel', string=ADMIN_FORM)
    folio = fields.Char(string="Folio")
    bank_ref = fields.Many2one(BANK_REF,string='Bank Reference')
    acc_move_lines = fields.One2many("account.move.line", 'money_fund_request_cancel_transfer_id', string="Money Fund Payment",tracking=True)
    bank_move_id = fields.Many2one('bank.movements',string="Movimiento Bancario")
    @api.model
    def create(self, vals):
        if vals.get('has_funds')==True:
            res = super(MoneyFundsCancelation, self).create(vals)
            res.name= 'Cancelación FF-'+str(res.money_fund.dependency_id.dependency)+'-'+str(res.money_fund.sub_dependency_id.sub_dependency)
            return res
        else:
            raise UserError(_('It cant be a cancel request if doesnt have a currency fund.'))
    def unlink(self):
        if self.state not in ['draft']:
            raise UserError(_('You cannot delete a cancel request which has been not draft state.'))
        return super(MoneyFundsCancelation, self).unlink()
    #==============================================================
    #-----------------Acciones de la vista
    #==============================================================
    #Botón para devolver la solicitud a Borrador
    def action_for_draft(self):
        self.state='draft'

    #Botón para publicar la solicitud de cancelación
    def action_for_publish(self):
        if self.state=='draft' or self.state=='rejected_correction':
            if self.has_funds:
                if len(self.cancel_files)!=0:
                    self.state='published'
                else:
                    raise UserError('No se han añadido los archivos para la solicitud de cancelación. Revisar.')
        
    #Revisa si el fondo fijo tiene ministraciones pendientes por comprobar
    @api.onchange('has_funds','money_fund')
    def _compute_min_request(self):
        if self.has_funds:
            self.min_request_id = self.env[MONEY_FUND_REQ_MIN].search([('money_fund','=',self.money_fund.id),('state','=','deposited'),('verified_complete','=',False)])
        else:
            self.min_request_id = False

    #Función que revisa si hay algun detalle con el fondo fijo o con las ministraciones pendientes
    def action_for_observations(self):
        ministraciones = self.env[MONEY_FUND_REQ_MIN].search([('money_fund','=',self.money_fund.ids),('state','=','deposited'),('verified_complete','=',False)])
        if self.has_funds and self.state=='published':
            if len(ministraciones)>0:
                self.observations=self._get_min_info(ministraciones)
                self.state='with_observations'
                self.is_published=True
            else:
                self.state='published'
                self.is_published=True

    #Mandar a revisada la solicitud
    def action_for_revised(self):
        if self.state in ('with_observations','published'):
            self.state='revised'
    #Mandar la ventana para asignar la corrección de los documentos
    def action_for_reject_doc(self):
        if self.state in ('with_observations','published'):
            return {
            'name': _('Rechazo de solicitud de cancelación (Documentación)'),
            'type': ACT_WINDOW,
            'res_model': MONEY_FUND_REQ_REJECT,
            'view_mode': 'form',
            'view_type': 'form',
            'context': {
                'type_request':'cancel',
                'money_funds_req_info': self.id,
                'next_state':'rejected_correction',
                'type':'doc'
            },
            'views': [(self.env.ref("siif_currency_purchase_req.application_request_rejection_doc_form").id, 'form')],
            'target': 'new'
            }

    #Acción para aprobar la solicitud y se genera la solicitud de transferencia
    def action_for_approve(self):
        if self.state=='revised':
            self.state='approved'
            self.folio = self.env[IR_SEQ].next_by_code(BANK_REF)

    #Mandar la ventana para asignar los motivos de rechazo
    def action_for_reject(self):
        if self.state=='revised':
            return {
            'name': _('Rechazo de solicitud de Cancelación'),
            'type': ACT_WINDOW,
            'res_model': MONEY_FUND_REQ_REJECT,
            'view_mode': 'form',
            'view_type': 'form',
            'context': {
                'type_request':'cancel',
                'money_funds_req_info': self.id,
                'next_state':'rejected',
                'type':'reject'
            },
            'views': [(self.env.ref("siif_currency_purchase_req.application_request_rejection_form").id, 'form')],
            'target': 'new'
            }

    #Acción para cancelar la solicitud, asi para cancelar el fondo fijo
    def action_for_cancel(self):
        if self.state=='recived_deposit':
            ministrations = self.env[MONEY_FUND_REQ_MIN].search([('money_fund','=',self.money_fund.id),('state','=','deposited'),('verified_complete','=',False)])
            if len(ministrations)==0:
                self.state='cancelled'
                self.money_fund.import_fund=0
            else:
                raise ValidationError(_('To cancel de currency funds, the associated ministrations have to be verificated.'))

    #==============================================================
    #----------------------Funciones
    #==============================================================
    

    #Función para obtener la lista de lineas contables generadas por la solicitud de transferencia
    @api.depends('transfer_request')
    def _get_transfer_moves(self):
        if len(self.transfer_request.payment_ids.move_line_ids)!=0:
            self.transfer_acc_move_lines=self.transfer_request.payment_ids.move_line_ids
            if(self.state=='approved'):
                self.state = 'deposited'
                self.name = self.money_fund.min_seq.next_by_id()
        else:
            self.transfer_acc_move_lines=False

    #Generar la referencia bancaria con el dinero faltante del fondo
    def generate_bank_reference(self):
        if self.state=='approved':
            return {
            'name': _('Generar Referencia Bancaria'),
            'type': ACT_WINDOW,
            'res_model': 'wiz.bank.reference',
            'view_mode': 'form',
            'view_type': 'form',
            'context': {
                'model':'cancel',
                'id_request': self.id,
                'default_reference':self.env[BANK_REF]._gen_short_ref(self.dependency_id,self.sub_dependency_id,self.folio,'money_return'),
                'default_date': date.today(),
                'default_amount':self.money_fund.import_fund,
            },
            'views': [(False, 'form')],
            'target': 'new'
            }
    
    #Cancelación del fondo fijo
    @api.onchange('state')
    def _check_state(self):
        if self.state=='recived_deposit':
            self.money_fund.state='cancelled'


    #Función para asignar las lineas contables de SOlicitud de cancelación
    def _gen_account_moves(self,money_fund_cancel):
        lines=[]
        money_fund_info=money_fund_cancel.money_fund
        account_dgf=self.env[BANK_REF_CONF].get_param('account')
        lines.append((0, 0, {
            'date':date.today(),
            'name':'Devolución del Fondo Fijo',
            'account_id': money_fund_info.debtor_account.id,
            'dependency_id':money_fund_cancel.dependency_id.id,
            'sub_dependency_id':money_fund_cancel.sub_dependency_id.id,
            'credit':money_fund_info.import_fund,
        }))
        lines.append((0, 0, {
            'date':date.today(),
            'name':'Devolución del Fondo Fijo',
            'account_id': account_dgf.id,
            'debit':money_fund_info.import_fund,
        }))

        move_val = {
            'ref': "Cancelación del "+money_fund_cancel.money_fund.name,
            'conac_move': False,
            'date': datetime.now(),
            'journal_id': money_fund_info.account_journal.id,
            'company_id': self.env.user.company_id.id,
            'line_ids': lines,
            }
        
        # Creación de los apuntes contables
        move = self.env[ACC_MOVE].create(move_val)
        move.action_post()
        money_fund_cancel.acc_move_lines=move.line_ids

    def _get_info_request(self,type):
        tipo="<p><b>Tipo de Solicitud: </b>Solicitud de Cancelación</p>"
        modelo="<p><b>Fondo Fijo: </b>"+self.money_fund.name+"</p>"
        nombre="<p><b>Identificador: </b>"+str(self.name)+"</p>"
        if type=='doc':
            dato="<p><b>Observación de la documentación: </b>"+self.observations_doc+"</p>"
        elif type=='reject':
            dato="<p><b>Observación de la documentación: </b>"+self.reject_reasons+"</p>"
        return tipo+modelo+nombre+dato

    #Revisión de permisos de fondo fijo
    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        res = super().fields_view_get(view_id=view_id,
                                      view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])

        is_admin = self.env.user.has_group(SIIF_ADMIN_USER)
        is_upa_user = self.env.user.has_group(SIIF_UPA_USER)
        is_dep_user = self.env.user.has_group(SIIF_DEP_USER)
        #Vista para el FONDO FIJO
        #   Gestión del fondo fijo

        
        for node in doc.xpath("//" + view_type):
            if (is_admin or is_dep_user):
                node.set('create', '1')
                node.set('import', '1')
                node.set('edit', '1')
                node.set('delete', '1')
                node.set('export_xlsx', '1')
                node.set('export', '1')
                node.set('duplicate', '1')
            else:    
                node.set('edit', '0')
                node.set('create', '0')
                node.set('import', '0')
                node.set('edit', '0')
                node.set('delete', '0')
                node.set('export_xlsx', '0')
                node.set('export', '0')
                node.set('duplicate', '0')
            if (is_dep_user):
                node.set('edit', '1')
        

        if not (is_admin or is_dep_user) and 'toolbar' in res:
            res['toolbar']['print'] = []
            res['toolbar']['action'] = []
        for node in doc.xpath(XPATH_FIELD):
            node.set('options', XPATH_OPEN_POS if not (is_admin or is_dep_user or is_upa_user) else XPATH_OPEN_NEG)
        res['arch'] = etree.tostring(doc)

        return res
    