import logging
import unittest
from odoo.exceptions import UserError,ValidationError
from datetime import datetime
from odoo.tests import tagged


from odoo.tests import common
_logger = logging.getLogger(__name__)

@tagged('-at_install','post_install', 'income_budgets','siif_budget_mgmt')
class IncomeBudgetTest(common.TransactionCase):

    def setUp(self):
        super(IncomeBudgetTest,self).setUp()
        self.model = self.env['siif.budget.mgmt.income.budgets']
        self.journal = self.env['siif.budget.mgmt.relation.journal.process']
        self.module_process = self.env['siif.budget.mgmt.relation.modules.process']
        self.modules = self.env['siif.budget.mgmt.relation.modules']
        self.journal_id = self.env['account.journal']
        self.name = self.env['account.fiscal.year']
        self.year_sequence = self.env['ir.sequence']

    
    def test_default_get_with_existing_journal(self):

        id_journal = "Presupuesto de Ingresos (PDI02)"
        journal_id = self.journal_id.browse([id_journal])
        journal = self.journal.search([('journal_name', '=', journal_id.id)])

        res = self.model.default_get([])

        self.assertEqual(res['journal_id'], journal.journal_name.id)
        print(f'------------------Equals data test-----------------------------OK')

    def test_default_get_with_non_existing_journal(self):
        id_journal = "Presupuesto 1"
        journal_id = self.journal_id.browse([id_journal])
        journal = self.journal.search([('journal_name', '=', journal_id.id)])

        res = self.model.default_get([])

        self.assertNotEqual(res['journal_id'], journal.journal_name.id)
        print(f'------------------------Not Equals data test-----------------------------OK')

    def test_create_income_budget(self):
        vals = {
            'fiscal_year': 2,
        }

        # Llamar al método create() en el modelo IncomeBudget
        income_budget = self.env['siif.budget.mgmt.income.budgets'].create(vals)
        
        # Verificar que el presupuesto de ingresos se creó correctamente
        self.assertNotEqual(income_budget.name, "Presupuesto Ingresos/2022/002")

        print(f'---------Not equals data test----------OK')
 