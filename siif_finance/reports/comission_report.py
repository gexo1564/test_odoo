import logging
from odoo import fields, models, api, _
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.tools.misc import formatLang
from odoo.tools.misc import xlsxwriter
import io
import base64
from odoo.tools import config, date_utils, get_lang
import lxml.html
import logging

class ComissionReport(models.AbstractModel):
    _name = "comission.report"
    _inherit = "account.coa.report"
    _description = "Comission report"

    filter_date = {'mode': 'range', 'filter': 'this_year'}
    filter_comparison = None
    filter_all_entries = None
    filter_journals = None
    filter_analytic = None
    filter_unfold_all = None
    filter_cash_basis = None
    filter_hierarchy = None
    filter_unposted_in_period = None
    MAX_LINES = None

    filter_dates = 0

    def _get_reports_buttons(self):
        is_group_approval_user = self.env.user.has_group('jt_payroll_payment.group_approval_user')

        if is_group_approval_user:
            return[]
        else:
            return [
                {'name': _('Export to PDF'), 'sequence': 1, 'action': 'print_pdf', 'file_export_type': _('PDF')},
                {'name': _('Export (XLSX)'), 'sequence': 2, 'action': 'print_xlsx', 'file_export_type': _('XLSX')},
                ]

    def _get_templates(self):
        templates = super(
            ComissionReport, self)._get_templates()
        templates[
            'main_table_header_template'] = 'account_reports.main_table_header'
        templates['main_template'] = 'account_reports.main_template'
        return templates

    def _get_columns_name(self, options):
        return [
            {'name': _('Dirección'), 'class':'text-justify'},
            {'name': _('Concepto de la comisión'), 'class':'text-justify'},
            {'name': _('Banco'), 'class':'text-justify'},
            {'name': _('Cuenta Bancaria'), 'class':'text-justify'},
            {'name': _('Comisión'), 'class':'text-center'},
            {'name': _('ENE'), 'class':'text-center'},
            {'name': _('FEB'), 'class':'text-center'},
            {'name': _('MAR'), 'class':'text-center'},
            {'name': _('ABR'), 'class':'text-center'},
            {'name': _('MAY'), 'class':'text-center'},
            {'name': _('JUN'), 'class':'text-center'},
            {'name': _('JUL'), 'class':'text-center'},
            {'name': _('AGO'), 'class':'text-center'},
            {'name': _('SEP'), 'class':'text-center'},
            {'name': _('OCT'), 'class':'text-center'},
            {'name': _('NOV'), 'class':'text-center'},
            {'name': _('DIC'), 'class':'text-center'},
            {'name': _('Total'), 'class':'text-center'},
        ]

    def _format(self, value,figure_type):
        if self.env.context.get('no_format'):
            return value
        value['no_format_name'] = value['name']
        
        if figure_type == 'float':
            currency_id = self.env.company.currency_id
            if currency_id.is_zero(value['name']):
                # don't print -0.0 in reports
                value['name'] = abs(value['name'])
                value['class'] = 'number text-muted'
            value['name'] = formatLang(self.env, value['name'], currency_obj=currency_id)
            value['class'] = 'number'
            return value
        if figure_type == 'percents':
            value['name'] = str(round(value['name'] * 100, 1)) + '%'
            value['class'] = 'number'
            return value
        value['name'] = round(value['name'], 1)
        return value['name']


    def _get_lines(self, options, line_id=None):
        lines = []
        start = datetime.strptime(
            str(options['date'].get('date_from')), '%Y-%m-%d').date()
        end = datetime.strptime(
            options['date'].get('date_to'), '%Y-%m-%d').date()

        #### Se obtiene el filtro de fecha####
        date_filtro = options.get('dates', 0)
        
        if date_filtro == 0:
            where_date = "and movement_date>="+"'"+str(start)+"'"+" and movement_date<="+"'"+str(end)+"'"
        else:
            where_date = "and settlement_date>="+"'"+str(start)+"'"+" and settlement_date<="+"'"+str(end)+"'"

        #logging.critical("Where: "+str(where_date))

        total_1 = 0
        total_2 = 0
        total_3 = 0
        total_4 = 0
        total_5 = 0
        total_6 = 0
        total_7 = 0
        total_8 = 0
        total_9 = 0
        total_10 = 0
        total_11 = 0
        total_12 = 0
        total_13 = 0

        total_final_1 = 0
        total_final_2 = 0
        total_final_3 = 0
        total_final_4 = 0
        total_final_5 = 0
        total_final_6 = 0 
        total_final_7 = 0
        total_final_8 = 0
        total_final_9 = 0
        total_final_10 = 0 
        total_final_11 = 0 
        total_final_12 = 0 
        total_final_13 = 0

        self.env.cr.execute('''select distinct concept_direction from comission_concept;''')

        direction_data = self.env.cr.fetchall()
        
        for direction in direction_data:
            if direction[0]=='egress':
                new_direction = 'EGRESOS'
            else:
                new_direction = 'INGRESOS'

            total_direction_1 = 0
            total_direction_2 = 0
            total_direction_3 = 0
            total_direction_4 = 0
            total_direction_5 = 0
            total_direction_6 = 0
            total_direction_7 = 0
            total_direction_8 = 0
            total_direction_9 = 0
            total_direction_10 = 0
            total_direction_11 = 0
            total_direction_12 = 0
            total_direction_13 = 0

            lines.append({
                        'id': 'hierarchy_direction_'+str(direction[0]),
                        'name': new_direction,
                        'columns': [{'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    ],
                        'level': 0,
                        'unfoldable': True,
                        'unfolded': False,
                        'class':'text-left'
                    }) 
            self.env.cr.execute(f"""select id, concept from comission_concept where concept_direction = '{direction[0]}' order by concept;""")
            concept_data = self.env.cr.fetchall()
            for concept in concept_data:
                total_concept_1 = 0
                total_concept_2 = 0
                total_concept_3 = 0
                total_concept_4 = 0
                total_concept_5 = 0
                total_concept_6 = 0
                total_concept_7 = 0
                total_concept_8 = 0
                total_concept_9 = 0
                total_concept_10 = 0
                total_concept_11 = 0
                total_concept_12 = 0
                total_concept_13 = 0
                lines.append({
                        'id': 'hierarchy_concept_'+str(concept[0]),
                        'name': '',
                        'columns': [{'name': concept[1],'class':'text-justify'},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    ],
                        'level': 1,
                        'unfoldable': True,
                        'unfolded': True,
                        'parent_id': 'hierarchy_direction_'+str(direction[0]),
                        'class':'text-left'
                    })

                self.env.cr.execute(f"""select distinct bank from comission_profit where concept_related = {concept[0]} {where_date} and status = 'approved' and reversed is null order by bank;""")
                bank_data = self.env.cr.fetchall()
                for bank in bank_data:
                    total_bank_1 = 0
                    total_bank_2 = 0
                    total_bank_3 = 0
                    total_bank_4 = 0
                    total_bank_5 = 0
                    total_bank_6 = 0
                    total_bank_7 = 0
                    total_bank_8 = 0
                    total_bank_9 = 0
                    total_bank_10 = 0
                    total_bank_11 = 0
                    total_bank_12 = 0
                    total_bank_13 = 0
                    bank_id = self.env['res.bank'].browse(bank[0])
                    lines.append({
                        'id': 'hierarchy_bank_'+str(bank[0])+str(concept[0]),
                        'name': '',
                        'columns': [{'name': ''},
                                    {'name': bank_id and bank_id.name or '','class':'text-justify'},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    ],
                        'level': 2,
                        'unfoldable': True,
                        'unfolded': True,
                        'parent_id': 'hierarchy_concept_'+str(concept[0]),
                        'class':'text-left'
                    })
                    self.env.cr.execute(f"""select distinct bank_account_id from comission_profit where bank = {bank[0]} and concept_related = {concept[0]} {where_date} and status = 'approved' and reversed is null order by bank_account_id;""")
                    account_data = self.env.cr.fetchall()

                    for account in account_data:

                        total_account_1 = 0
                        total_account_2 = 0
                        total_account_3 = 0
                        total_account_4 = 0
                        total_account_5 = 0
                        total_account_6 = 0
                        total_account_7 = 0
                        total_account_8 = 0
                        total_account_9 = 0 
                        total_account_10 = 0
                        total_account_11 = 0
                        total_account_12 = 0
                        total_account_13 = 0

                        account_id = self.env['res.partner.bank'].browse(account[0])
                        lines.append({
                            'id': 'hierarchy_account_'+str(bank[0])+str(concept[0])+str(account[0]),
                            'name': '',
                            'columns': [{'name': ''},
                                        {'name': ''},
                                        {'name': account_id and account_id.account_siiof or '','class':'text-justify'},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        ],
                            'level': 3,
                            'unfoldable': True,
                            'unfolded': True,
                            'parent_id': 'hierarchy_bank_'+str(bank[0])+str(concept[0]),
                            'class':'text-left'
                        })

                        self.env.cr.execute(f"""select distinct comission_id from comission_profit where bank_account_id = {account[0]} and concept_related = {concept[0]} and bank = {bank[0]} and status = 'approved' {where_date} and reversed is null order by comission_id;""")
                        comission_data = self.env.cr.fetchall()

                        for comission in comission_data:
                            total_1 = 0
                            total_2 = 0
                            total_3 = 0
                            total_4 = 0
                            total_5 = 0
                            total_6 = 0
                            total_7 = 0
                            total_8 = 0
                            total_9 = 0
                            total_10 = 0
                            total_11 = 0
                            total_12 = 0
                            total_13 = 0
                            comission_id = self.env['comission.settings.finance'].browse(comission[0])
                            lines.append({
                                'id': 'hierarchy_comission_'+str(bank[0])+str(concept[0])+str(account[0])+str(comission[0]),
                                'name': '',
                                'columns': [{'name': ''},
                                            {'name': ''},
                                            {'name': ''},
                                            {'name': comission_id and comission_id.comission_description_mov or '','class':'text-justify'},
                                            {'name': ''},
                                            {'name': ''},
                                            {'name': ''},
                                            {'name': ''},
                                            {'name': ''},
                                            {'name': ''},
                                            {'name': ''},
                                            {'name': ''},
                                            {'name': ''},
                                            {'name': ''},
                                            {'name': ''},
                                            {'name': ''},
                                            {'name': ''},
                                            ],
                                'level': 4,
                                'unfoldable': True,
                                'unfolded': True,
                                'parent_id': 'hierarchy_account_'+str(bank[0])+str(concept[0])+str(account[0]),
                                'class':'text-left'
                            })

                            self.env.cr.execute(f"""
                                select max(com.id) as id, com.concept_related as concept, com.bank as bank, com.bank_account_id as bank_account, com.comission_id as comission,
                
                                COALESCE(sum(case when extract(month from movement_date) = 1 then (select COALESCE(sum(amount),0) as amount from comission_profit where id in(com.id)) else 0 end),0) january,
                                COALESCE(sum(case when extract(month from movement_date) = 2 then (select COALESCE(sum(amount),0) as amount from comission_profit where id in(com.id)) else 0 end),0) february,
                                COALESCE(sum(case when extract(month from movement_date) = 3 then (select COALESCE(sum(amount),0) as amount from comission_profit where id in(com.id)) else 0 end),0) march,
                                COALESCE(sum(case when extract(month from movement_date) = 4 then (select COALESCE(sum(amount),0) as amount from comission_profit where id in(com.id)) else 0 end),0) april,
                                COALESCE(sum(case when extract(month from movement_date) = 5 then (select COALESCE(sum(amount),0) as amount from comission_profit where id in(com.id)) else 0 end),0) may,
                                COALESCE(sum(case when extract(month from movement_date) = 6 then (select COALESCE(sum(amount),0) as amount from comission_profit where id in(com.id)) else 0 end),0) june,
                                COALESCE(sum(case when extract(month from movement_date) = 7 then (select COALESCE(sum(amount),0) as amount from comission_profit where id in(com.id)) else 0 end),0) july,
                                COALESCE(sum(case when extract(month from movement_date) = 8 then (select COALESCE(sum(amount),0) as amount from comission_profit where id in(com.id)) else 0 end),0) august,
                                COALESCE(sum(case when extract(month from movement_date) = 9 then (select COALESCE(sum(amount),0) as amount from comission_profit where id in(com.id)) else 0 end),0) september,
                                COALESCE(sum(case when extract(month from movement_date) = 10 then (select COALESCE(sum(amount),0) as amount from comission_profit where id in(com.id)) else 0 end),0) october,
                                COALESCE(sum(case when extract(month from movement_date) = 11 then (select COALESCE(sum(amount),0) as amount from comission_profit where id in(com.id)) else 0 end),0) november,
                                COALESCE(sum(case when extract(month from movement_date) = 12 then (select COALESCE(sum(amount),0) as amount from comission_profit where id in(com.id)) else 0 end),0) december,
                                COALESCE(sum((select COALESCE(sum(amount),0) as amount from comission_profit where id in(com.id))),0) as total
                                from comission_profit com
                                where com.status in ('approved') and com.bank_account_id = {account[0]} and com.concept_related = {concept[0]} and com.bank = {bank[0]} and com.comission_id = {comission[0]}
                                {where_date}
                                and reversed is null
                                group by concept, bank, bank_account, comission
                                order by concept, bank, bank_account, comission;
                                """)
                            comission_amount_data = self.env.cr.fetchall()

                            #logging.critical("Monto: "+str(comission_amount_data))

                            for amount in comission_amount_data:
                                lines.append({
                                    'id': 'hierarchy_comission_amount_'+str(bank[0])+str(concept[0])+str(account[0])+str(comission[0]),
                                    'name': '',
                                    'columns': [{'name': ''},
                                                {'name': ''},
                                                {'name': ''},
                                                {'name': 'Monto','class':'text-justify'},
                                                {'name': self._format({'name': amount[5]}, figure_type='float')['name'],'class':'text-right'},
                                                {'name': self._format({'name': amount[6]}, figure_type='float')['name'],'class':'text-right'},
                                                {'name': self._format({'name': amount[7]}, figure_type='float')['name'],'class':'text-right'},
                                                {'name': self._format({'name': amount[8]}, figure_type='float')['name'],'class':'text-right'},
                                                {'name': self._format({'name': amount[9]}, figure_type='float')['name'],'class':'text-right'},
                                                {'name': self._format({'name': amount[10]}, figure_type='float')['name'],'class':'text-right'},
                                                {'name': self._format({'name': amount[11]}, figure_type='float')['name'],'class':'text-right'},
                                                {'name': self._format({'name': amount[12]}, figure_type='float')['name'],'class':'text-right'},
                                                {'name': self._format({'name': amount[13]}, figure_type='float')['name'],'class':'text-right'},
                                                {'name': self._format({'name': amount[14]}, figure_type='float')['name'],'class':'text-right'},
                                                {'name': self._format({'name': amount[15]}, figure_type='float')['name'],'class':'text-right'},
                                                {'name': self._format({'name': amount[16]}, figure_type='float')['name'],'class':'text-right'},
                                                {'name': self._format({'name': amount[17]}, figure_type='float')['name'],'class':'text-right'},
                                                ],
                                    'level': 5,
                                    'unfoldable': False,
                                    'unfolded': True,
                                    'parent_id': 'hierarchy_comission_'+str(bank[0])+str(concept[0])+str(account[0])+str(comission[0]),
                                    'class':'text-left'
                                })

                            self.env.cr.execute(f"""
                                select max(com.id) as id, com.concept_related as concept, com.bank as bank, com.bank_account_id as bank_account, com.comission_id as comission,
                
                                COALESCE(sum(case when extract(month from movement_date) = 1 then (select COALESCE(sum(iva_amount),0) as iva_amount from comission_profit where id in(com.id)) else 0 end),0) january,
                                COALESCE(sum(case when extract(month from movement_date) = 2 then (select COALESCE(sum(iva_amount),0) as iva_amount from comission_profit where id in(com.id)) else 0 end),0) february,
                                COALESCE(sum(case when extract(month from movement_date) = 3 then (select COALESCE(sum(iva_amount),0) as iva_amount from comission_profit where id in(com.id)) else 0 end),0) march,
                                COALESCE(sum(case when extract(month from movement_date) = 4 then (select COALESCE(sum(iva_amount),0) as iva_amount from comission_profit where id in(com.id)) else 0 end),0) april,
                                COALESCE(sum(case when extract(month from movement_date) = 5 then (select COALESCE(sum(iva_amount),0) as iva_amount from comission_profit where id in(com.id)) else 0 end),0) may,
                                COALESCE(sum(case when extract(month from movement_date) = 6 then (select COALESCE(sum(iva_amount),0) as iva_amount from comission_profit where id in(com.id)) else 0 end),0) june,
                                COALESCE(sum(case when extract(month from movement_date) = 7 then (select COALESCE(sum(iva_amount),0) as iva_amount from comission_profit where id in(com.id)) else 0 end),0) july,
                                COALESCE(sum(case when extract(month from movement_date) = 8 then (select COALESCE(sum(iva_amount),0) as iva_amount from comission_profit where id in(com.id)) else 0 end),0) august,
                                COALESCE(sum(case when extract(month from movement_date) = 9 then (select COALESCE(sum(iva_amount),0) as iva_amount from comission_profit where id in(com.id)) else 0 end),0) september,
                                COALESCE(sum(case when extract(month from movement_date) = 10 then (select COALESCE(sum(iva_amount),0) as iva_amount from comission_profit where id in(com.id)) else 0 end),0) october,
                                COALESCE(sum(case when extract(month from movement_date) = 11 then (select COALESCE(sum(iva_amount),0) as iva_amount from comission_profit where id in(com.id)) else 0 end),0) november,
                                COALESCE(sum(case when extract(month from movement_date) = 12 then (select COALESCE(sum(iva_amount),0) as iva_amount from comission_profit where id in(com.id)) else 0 end),0) december,
                                COALESCE(sum((select COALESCE(sum(iva_amount),0) as iva_amount from comission_profit where id in(com.id))),0) as total
                                from comission_profit com
                                where com.status in ('approved') and com.bank_account_id = {account[0]} and com.concept_related = {concept[0]} and com.bank = {bank[0]} and com.comission_id = {comission[0]}
                                {where_date}
                                and reversed is null
                                group by concept, bank, bank_account, comission
                                order by concept, bank, bank_account, comission;
                                """)
                            comission_iva_data = self.env.cr.fetchall()

                            for iva in comission_iva_data:
                                lines.append({
                                    'id': 'hierarchy_comission_iva_'+str(bank[0])+str(concept[0])+str(account[0])+str(comission[0]),
                                    'name': '',
                                    'columns': [{'name': ''},
                                                {'name': ''},
                                                {'name': ''},
                                                {'name': 'IVA','class':'text-justify'},
                                                {'name': self._format({'name': iva[5]}, figure_type='float')['name'],'class':'text-right'},
                                                {'name': self._format({'name': iva[6]}, figure_type='float')['name'],'class':'text-right'},
                                                {'name': self._format({'name': iva[7]}, figure_type='float')['name'],'class':'text-right'},
                                                {'name': self._format({'name': iva[8]}, figure_type='float')['name'],'class':'text-right'},
                                                {'name': self._format({'name': iva[9]}, figure_type='float')['name'],'class':'text-right'},
                                                {'name': self._format({'name': iva[10]}, figure_type='float')['name'],'class':'text-right'},
                                                {'name': self._format({'name': iva[11]}, figure_type='float')['name'],'class':'text-right'},
                                                {'name': self._format({'name': iva[12]}, figure_type='float')['name'],'class':'text-right'},
                                                {'name': self._format({'name': iva[13]}, figure_type='float')['name'],'class':'text-right'},
                                                {'name': self._format({'name': iva[14]}, figure_type='float')['name'],'class':'text-right'},
                                                {'name': self._format({'name': iva[15]}, figure_type='float')['name'],'class':'text-right'},
                                                {'name': self._format({'name': iva[16]}, figure_type='float')['name'],'class':'text-right'},
                                                {'name': self._format({'name': iva[17]}, figure_type='float')['name'],'class':'text-right'},
                                                ],
                                    'level': 5,
                                    'unfoldable': False,
                                    'unfolded': True,
                                    'parent_id': 'hierarchy_comission_'+str(bank[0])+str(concept[0])+str(account[0])+str(comission[0]),
                                    'class':'text-left'
                                })

                            total_1 = amount[5]+iva[5]
                            total_2 = amount[6]+iva[6]
                            total_3 = amount[7]+iva[7]
                            total_4 = amount[8]+iva[8]
                            total_5 = amount[9]+iva[9]
                            total_6 = amount[10]+iva[10]
                            total_7 = amount[11]+iva[11]
                            total_8 = amount[12]+iva[12]
                            total_9 = amount[13]+iva[13]
                            total_10 = amount[14]+iva[14]
                            total_11 = amount[15]+iva[15]
                            total_12 = amount[16]+iva[16]
                            total_13 = amount[17]+iva[17]

                            lines.append({
                                'id': 'hierarchy_total1_'+str(bank[0])+str(concept[0])+str(account[0])+str(comission[0]),
                                'name': '',
                                'columns': [{'name': ''},
                                            {'name': ''},
                                            {'name': ''},
                                            {'name': 'Total','class':'text-justify'},
                                            {'name': ''},
                                            {'name': ''},
                                            {'name': ''},
                                            {'name': ''},
                                            {'name': ''},
                                            {'name': ''},
                                            {'name': ''},
                                            {'name': ''},
                                            {'name': ''},
                                            {'name': ''},
                                            {'name': ''},
                                            {'name': ''},
                                            {'name': ''},
                                            ],
                                'level': 2,
                                'unfoldable': False,
                                'unfolded': True,
                                'parent_id': 'hierarchy_comission_'+str(bank[0])+str(concept[0])+str(account[0])+str(comission[0]),
                                'class':'text-left'
                            })

                            lines.append({
                                'id': 'hierarchy_total_'+str(bank[0])+str(concept[0])+str(account[0])+str(comission[0]),
                                'name': '',
                                'columns': [{'name': ''},
                                            {'name': ''},
                                            {'name': ''},
                                            {'name': ''},
                                            {'name': self._format({'name': total_1}, figure_type='float')['name'],'class':'text-right'},
                                            {'name': self._format({'name': total_2}, figure_type='float')['name'],'class':'text-right'},
                                            {'name': self._format({'name': total_3}, figure_type='float')['name'],'class':'text-right'},
                                            {'name': self._format({'name': total_4}, figure_type='float')['name'],'class':'text-right'},
                                            {'name': self._format({'name': total_5}, figure_type='float')['name'],'class':'text-right'},
                                            {'name': self._format({'name': total_6}, figure_type='float')['name'],'class':'text-right'},
                                            {'name': self._format({'name': total_7}, figure_type='float')['name'],'class':'text-right'},
                                            {'name': self._format({'name': total_8}, figure_type='float')['name'],'class':'text-right'},
                                            {'name': self._format({'name': total_9}, figure_type='float')['name'],'class':'text-right'},
                                            {'name': self._format({'name': total_10}, figure_type='float')['name'],'class':'text-right'},
                                            {'name': self._format({'name': total_11}, figure_type='float')['name'],'class':'text-right'},
                                            {'name': self._format({'name': total_12}, figure_type='float')['name'],'class':'text-right'},
                                            {'name': self._format({'name': total_13}, figure_type='float')['name'],'class':'text-right'},
                                            ],
                                'level': 5,
                                'unfoldable': False,
                                'unfolded': True,
                                'parent_id': 'hierarchy_comission_'+str(bank[0])+str(concept[0])+str(account[0])+str(comission[0]),
                                'class':'font-weight-bold'
                            })

                            total_account_1 += total_1
                            total_account_2 += total_2
                            total_account_3 += total_3
                            total_account_4 += total_4
                            total_account_5 += total_5
                            total_account_6 += total_6
                            total_account_7 += total_7
                            total_account_8 += total_8
                            total_account_9 += total_9
                            total_account_10 += total_10
                            total_account_11 += total_11
                            total_account_12 += total_12
                            total_account_13 += total_13


                        lines.append({
                            'id': 'hierarchy_total1_account_'+str(bank[0])+str(concept[0])+str(account[0]),
                            'name': '',
                            'columns': [{'name': ''},
                                        {'name': ''},
                                        {'name': 'Total '+ str(account_id.account_siiof),'class':'text-justify'},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        ],
                            'level': 2,
                            'unfoldable': False,
                            'unfolded': True,
                            'parent_id': 'hierarchy_bank_'+str(bank[0])+str(concept[0]),
                            'class':'text-left'
                        })


                        lines.append({
                            'id': 'hierarchy_total_account_'+str(bank[0])+str(concept[0])+str(account[0]),
                            'name': '',
                            'columns': [{'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': self._format({'name': total_account_1}, figure_type='float')['name'],'class':'text-right'},
                                        {'name': self._format({'name': total_account_2}, figure_type='float')['name'],'class':'text-right'},
                                        {'name': self._format({'name': total_account_3}, figure_type='float')['name'],'class':'text-right'},
                                        {'name': self._format({'name': total_account_4}, figure_type='float')['name'],'class':'text-right'},
                                        {'name': self._format({'name': total_account_5}, figure_type='float')['name'],'class':'text-right'},
                                        {'name': self._format({'name': total_account_6}, figure_type='float')['name'],'class':'text-right'},
                                        {'name': self._format({'name': total_account_7}, figure_type='float')['name'],'class':'text-right'},
                                        {'name': self._format({'name': total_account_8}, figure_type='float')['name'],'class':'text-right'},
                                        {'name': self._format({'name': total_account_9}, figure_type='float')['name'],'class':'text-right'},
                                        {'name': self._format({'name': total_account_10}, figure_type='float')['name'],'class':'text-right'},
                                        {'name': self._format({'name': total_account_11}, figure_type='float')['name'],'class':'text-right'},
                                        {'name': self._format({'name': total_account_12}, figure_type='float')['name'],'class':'text-right'},
                                        {'name': self._format({'name': total_account_13}, figure_type='float')['name'],'class':'text-right'},
                                        ],
                            'level': 5,
                            'unfoldable': False,
                            'unfolded': True,
                            'parent_id': 'hierarchy_bank_'+str(bank[0])+str(concept[0]),
                            'class':'font-weight-bold'
                        })

                        total_bank_1 += total_account_1
                        total_bank_2 += total_account_2
                        total_bank_3 += total_account_3
                        total_bank_4 += total_account_4
                        total_bank_5 += total_account_5
                        total_bank_6 += total_account_6
                        total_bank_7 += total_account_7
                        total_bank_8 += total_account_8
                        total_bank_9 += total_account_9
                        total_bank_10 += total_account_10
                        total_bank_11 += total_account_11
                        total_bank_12 += total_account_12
                        total_bank_13 += total_account_13

                    lines.append({
                            'id': 'hierarchy_total1_bank_'+str(bank[0])+str(concept[0]),
                            'name': '',
                            'columns': [{'name': ''},
                                        {'name': 'TOTAL '+ str(bank_id.name),'class':'text-justify'},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        ],
                            'level': 1,
                            'unfoldable': False,
                            'unfolded': True,
                            'parent_id': 'hierarchy_bank_'+str(bank[0])+str(concept[0]),
                            'class':'text-left'
                        })


                    lines.append({
                        'id': 'hierarchy_total_bank_'+str(bank[0])+str(concept[0]),
                        'name': '',
                        'columns': [{'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': ''},
                                    {'name': self._format({'name': total_bank_1}, figure_type='float')['name'],'class':'text-right'},
                                    {'name': self._format({'name': total_bank_2}, figure_type='float')['name'],'class':'text-right'},
                                    {'name': self._format({'name': total_bank_3}, figure_type='float')['name'],'class':'text-right'},
                                    {'name': self._format({'name': total_bank_4}, figure_type='float')['name'],'class':'text-right'},
                                    {'name': self._format({'name': total_bank_5}, figure_type='float')['name'],'class':'text-right'},
                                    {'name': self._format({'name': total_bank_6}, figure_type='float')['name'],'class':'text-right'},
                                    {'name': self._format({'name': total_bank_7}, figure_type='float')['name'],'class':'text-right'},
                                    {'name': self._format({'name': total_bank_8}, figure_type='float')['name'],'class':'text-right'},
                                    {'name': self._format({'name': total_bank_9}, figure_type='float')['name'],'class':'text-right'},
                                    {'name': self._format({'name': total_bank_10}, figure_type='float')['name'],'class':'text-right'},
                                    {'name': self._format({'name': total_bank_11}, figure_type='float')['name'],'class':'text-right'},
                                    {'name': self._format({'name': total_bank_12}, figure_type='float')['name'],'class':'text-right'},
                                    {'name': self._format({'name': total_bank_13}, figure_type='float')['name'],'class':'text-right'},
                                    ],
                        'level': 5,
                        'unfoldable': False,
                        'unfolded': True,
                        'parent_id': 'hierarchy_bank_'+str(bank[0])+str(concept[0]),
                        'class':'font-weight-bold'
                    })

                    total_concept_1 += total_bank_1
                    total_concept_2 += total_bank_2
                    total_concept_3 += total_bank_3
                    total_concept_4 += total_bank_4
                    total_concept_5 += total_bank_5
                    total_concept_6 += total_bank_6
                    total_concept_7 += total_bank_7
                    total_concept_8 += total_bank_8
                    total_concept_9 += total_bank_9
                    total_concept_10 += total_bank_10
                    total_concept_11 += total_bank_11
                    total_concept_12 += total_bank_12
                    total_concept_13 += total_bank_13

                lines.append({
                            'id': 'hierarchy_total1_concept_'+str(concept[0]),
                            'name': '',
                            'columns': [{'name': 'TOTAL '+ str(concept[1]),'class':'text-justify'},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        ],
                            'level': 1,
                            'unfoldable': False,
                            'unfolded': True,
                            'parent_id': 'hierarchy_direction_'+str(direction[0]),
                            'class':'text-left'
                        })


                lines.append({
                    'id': 'hierarchy_total_concept_'+str(concept[0]),
                    'name': '',
                    'columns': [{'name': ''},
                                {'name': ''},
                                {'name': ''},
                                {'name': ''},
                                {'name': self._format({'name': total_concept_1}, figure_type='float')['name'],'class':'text-right'},
                                {'name': self._format({'name': total_concept_2}, figure_type='float')['name'],'class':'text-right'},
                                {'name': self._format({'name': total_concept_3}, figure_type='float')['name'],'class':'text-right'},
                                {'name': self._format({'name': total_concept_4}, figure_type='float')['name'],'class':'text-right'},
                                {'name': self._format({'name': total_concept_5}, figure_type='float')['name'],'class':'text-right'},
                                {'name': self._format({'name': total_concept_6}, figure_type='float')['name'],'class':'text-right'},
                                {'name': self._format({'name': total_concept_7}, figure_type='float')['name'],'class':'text-right'},
                                {'name': self._format({'name': total_concept_8}, figure_type='float')['name'],'class':'text-right'},
                                {'name': self._format({'name': total_concept_9}, figure_type='float')['name'],'class':'text-right'},
                                {'name': self._format({'name': total_concept_10}, figure_type='float')['name'],'class':'text-right'},
                                {'name': self._format({'name': total_concept_11}, figure_type='float')['name'],'class':'text-right'},
                                {'name': self._format({'name': total_concept_12}, figure_type='float')['name'],'class':'text-right'},
                                {'name': self._format({'name': total_concept_13}, figure_type='float')['name'],'class':'text-right'},
                                ],
                    'level': 5,
                    'unfoldable': False,
                    'unfolded': True,
                    'parent_id': 'hierarchy_direction_'+str(direction[0]),
                    'class':'font-weight-bold'
                })

                total_direction_1 += total_concept_1 
                total_direction_2 +=total_concept_2 
                total_direction_3 +=total_concept_3 
                total_direction_4 +=total_concept_4 
                total_direction_5 +=total_concept_5 
                total_direction_6 +=total_concept_6 
                total_direction_7 +=total_concept_7 
                total_direction_8 +=total_concept_8 
                total_direction_9 +=total_concept_9 
                total_direction_10 +=total_concept_10 
                total_direction_11 +=total_concept_11 
                total_direction_12 +=total_concept_12 
                total_direction_13 +=total_concept_13

            lines.append({
                            'id': 'hierarchy_total1_direction_'+str(direction[0]),
                            'name': 'TOTAL '+ str(new_direction),'class':'text-justify',
                            'columns': [{'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        {'name': ''},
                                        ],
                            'level': 1,
                            'unfoldable': False,
                            'unfolded': True,
                            'class':'text-left'
                        })


            lines.append({
                'id': 'hierarchy_total_direction_'+str(direction[0]),
                'name': '',
                'columns': [{'name': ''},
                            {'name': ''},
                            {'name': ''},
                            {'name': ''},
                            {'name': self._format({'name': total_direction_1}, figure_type='float')['name'],'class':'text-right'},
                            {'name': self._format({'name': total_direction_2}, figure_type='float')['name'],'class':'text-right'},
                            {'name': self._format({'name': total_direction_3}, figure_type='float')['name'],'class':'text-right'},
                            {'name': self._format({'name': total_direction_4}, figure_type='float')['name'],'class':'text-right'},
                            {'name': self._format({'name': total_direction_5}, figure_type='float')['name'],'class':'text-right'},
                            {'name': self._format({'name': total_direction_6}, figure_type='float')['name'],'class':'text-right'},
                            {'name': self._format({'name': total_direction_7}, figure_type='float')['name'],'class':'text-right'},
                            {'name': self._format({'name': total_direction_8}, figure_type='float')['name'],'class':'text-right'},
                            {'name': self._format({'name': total_direction_9}, figure_type='float')['name'],'class':'text-right'},
                            {'name': self._format({'name': total_direction_10}, figure_type='float')['name'],'class':'text-right'},
                            {'name': self._format({'name': total_direction_11}, figure_type='float')['name'],'class':'text-right'},
                            {'name': self._format({'name': total_direction_12}, figure_type='float')['name'],'class':'text-right'},
                            {'name': self._format({'name': total_direction_13}, figure_type='float')['name'],'class':'text-right'},
                            ],
                'level': 5,
                'unfoldable': False,
                'unfolded': True,
                'class':'font-weight-bold'
            })

            total_final_1 += total_direction_1 
            total_final_2 += total_direction_2 
            total_final_3 += total_direction_3 
            total_final_4 += total_direction_4 
            total_final_5 += total_direction_5 
            total_final_6 += total_direction_6 
            total_final_7 += total_direction_7 
            total_final_8 += total_direction_8 
            total_final_9 += total_direction_9 
            total_final_10 += total_direction_10 
            total_final_11 += total_direction_11 
            total_final_12 += total_direction_12 
            total_final_13 += total_direction_13

        lines.append({
                    'id': 'hierarchy_total1_final',
                    'name': 'TOTAL FINAL','class':'text-justify',
                    'columns': [{'name': ''},
                                {'name': ''},
                                {'name': ''},
                                {'name': ''},
                                {'name': ''},
                                {'name': ''},
                                {'name': ''},
                                {'name': ''},
                                {'name': ''},
                                {'name': ''},
                                {'name': ''},
                                {'name': ''},
                                {'name': ''},
                                {'name': ''},
                                {'name': ''},
                                {'name': ''},
                                {'name': ''},
                                ],
                    'level': 1,
                    'unfoldable': False,
                    'unfolded': True,
                    'class':'text-left'
                })


        lines.append({
            'id': 'hierarchy_total_final',
            'name': '',
            'columns': [{'name': ''},
                        {'name': ''},
                        {'name': ''},
                        {'name': ''},
                        {'name': self._format({'name': total_final_1}, figure_type='float')['name'],'class':'text-right'},
                        {'name': self._format({'name': total_final_2}, figure_type='float')['name'],'class':'text-right'},
                        {'name': self._format({'name': total_final_3}, figure_type='float')['name'],'class':'text-right'},
                        {'name': self._format({'name': total_final_4}, figure_type='float')['name'],'class':'text-right'},
                        {'name': self._format({'name': total_final_5}, figure_type='float')['name'],'class':'text-right'},
                        {'name': self._format({'name': total_final_6}, figure_type='float')['name'],'class':'text-right'},
                        {'name': self._format({'name': total_final_7}, figure_type='float')['name'],'class':'text-right'},
                        {'name': self._format({'name': total_final_8}, figure_type='float')['name'],'class':'text-right'},
                        {'name': self._format({'name': total_final_9}, figure_type='float')['name'],'class':'text-right'},
                        {'name': self._format({'name': total_final_10}, figure_type='float')['name'],'class':'text-right'},
                        {'name': self._format({'name': total_final_11}, figure_type='float')['name'],'class':'text-right'},
                        {'name': self._format({'name': total_final_12}, figure_type='float')['name'],'class':'text-right'},
                        {'name': self._format({'name': total_final_13}, figure_type='float')['name'],'class':'text-right'},
                        ],
            'level': 5,
            'unfoldable': False,
            'unfolded': True,
            'class':'font-weight-bold'
        })


                
        return lines

    def _get_report_name(self):
        return _("Comisiones bancarias institucionales")
    
    @api.model
    def _get_super_columns(self, options):
        date_cols = options.get('date') and [options['date']] or []
        date_cols += (options.get('comparison') or {}).get('periods', [])
        columns = reversed(date_cols)
        return {'columns': columns, 'x_offset': 1, 'merge': 5}
    
    def get_month_name(self,month):
        month_name = ''
        if month==1:
            month_name = 'Enero'
        elif month==2:
            month_name = 'Febrero'
        elif month==3:
            month_name = 'Marzo'
        elif month==4:
            month_name = 'Abril'
        elif month==5:
            month_name = 'Mayo'
        elif month==6:
            month_name = 'Junio'
        elif month==7:
            month_name = 'Julio'
        elif month==8:
            month_name = 'Agosto'
        elif month==9:
            month_name = 'Septiembre'
        elif month==10:
            month_name = 'Octubre'
        elif month==11:
            month_name = 'Noviembre'
        elif month==12:
            month_name = 'Diciembre'
            
        return month_name.lower()
    
    def get_header_year_list(self,options):
        start = datetime.strptime(
            str(options['date'].get('date_from')), '%Y-%m-%d').date()
        end = datetime.strptime(
            options['date'].get('date_to'), '%Y-%m-%d').date()
        
        str1 = ''
        if start.year == end.year:
            if start.month != end.month:
                str1 = self.get_month_name(start.month) + "-" +self.get_month_name(end.month) + " " + str(start.year)
            else:   
                str1 = self.get_month_name(start.month) + " " + str(start.year)
        else:
            str1 = self.get_month_name(start.month)+" "+ str(start.year) + "-" +self.get_month_name(end.month) + " " + str(end.year)
        return str1

    def is_float(self, num):
        try:
            float(num)
            return True
        except ValueError:
            return False

    def get_xlsx(self, options, response=None):
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        sheet = workbook.add_worksheet(self._get_report_name()[:31])
 
        date_default_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2, 'num_format': 'yyyy-mm-dd', 'text_wrap': True, 'valign': 'top'})
        date_default_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': 'yyyy-mm-dd', 'text_wrap': True, 'valign': 'top'})
        default_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2, 'text_wrap': True, 'valign': 'top'})
        default_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'text_wrap': True, 'valign': 'top'})
        title_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'bold': True, 'bottom': 2})
        super_col_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'bold': True, 'align': 'center', 'border': 2, 'valign': 'vcenter'})
        level_0_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'bottom': 6, 'font_color': '#666666', 'text_wrap': True, 'valign': 'top'})
        level_1_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'bottom': 1, 'font_color': '#666666', 'text_wrap': True, 'valign': 'top'})
        level_2_col1_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 1, 'text_wrap': True, 'valign': 'top'})
        level_2_col1_total_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'text_wrap': True, 'valign': 'top'})
        level_2_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'text_wrap': True, 'valign': 'top'})
        level_3_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2, 'text_wrap': True, 'valign': 'top'})
        level_3_col1_total_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 1, 'text_wrap': True, 'valign': 'top'})
        level_3_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'text_wrap': True, 'valign': 'top'})
        currect_date_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'align': 'right', 'text_wrap': True, 'valign': 'top'})
        currency_format = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 11, 'font_color': '#666666', 'num_format': '$#,##0.00'})

        sheet.set_column(0, 4,20)
        sheet.set_column(5, 17,16, currency_format)
        
        #Set the first column width to 50
#         sheet.set_column(0, 0,15)
#         sheet.set_column(0, 1,20)
#         sheet.set_column(0, 2,20)
#         sheet.set_column(0, 3,12)
#         sheet.set_column(0, 4,20)
        #sheet.set_row(0, 0,50)
        #sheet.col(0).width = 90 * 20
        super_columns = self._get_super_columns(options)
        #y_offset = bool(super_columns.get('columns')) and 1 or 0
        #sheet.write(y_offset, 0,'', title_style)
        y_offset = 0
        col = 0
        start = datetime.strptime(str(options['date'].get('date_from')), '%Y-%m-%d').date()
        end = datetime.strptime(options['date'].get('date_to'), '%Y-%m-%d').date()
        start_month_name = start.strftime("%B")
        end_month_name = end.strftime("%B")
        

        if self.env.user.lang == 'es_MX':
            start_month_name = self.get_month_name(start.month)
            end_month_name = self.get_month_name(end.month)

        header_date = str(start.day) + " de " + start_month_name+" de "+str(start.year)
        header_date += " al "+str(end.day) + " " + end_month_name +" de "+str(end.year)

        actual_date = datetime.today().strftime('%d/%m/%Y')
        
#         sheet.merge_range(y_offset, col, 6, col, '')
        if self.env.user and self.env.user.company_id and self.env.user.company_id.header_logo:
            filename = 'logo.png'
            image_data = io.BytesIO(base64.standard_b64decode(self.env.user.company_id.header_logo))
            sheet.insert_image(0,0, filename, {'image_data': image_data,'x_offset':8,'y_offset':3,'x_scale':0.63,'y_scale':0.63})

        header_title = '''DIRECCIÓN GENERAL DE FINANZAS\nCOMISIONES BANCARIAS INSTITUCIONALES\n%s\nFecha de consulta: %s'''%(header_date, actual_date)
        sheet.merge_range(y_offset, col, 5, col+17, header_title,super_col_style)
        y_offset += 6
#         col=1
#         currect_time_msg = "Fecha y hora de impresión: "
#         currect_time_msg += datetime.today().strftime('%d/%m/%Y %H:%M')
#         sheet.merge_range(y_offset, col, y_offset, col+17, currect_time_msg,currect_date_style)
#         y_offset += 1
        
        # Todo in master: Try to put this logic elsewhere
#         x = super_columns.get('x_offset', 0)
#         for super_col in super_columns.get('columns', []):
#             cell_content = super_col.get('string', '').replace('<br/>', ' ').replace('&nbsp;', ' ')
#             x_merge = super_columns.get('merge')
#             if x_merge and x_merge > 1:
#                 sheet.merge_range(0, x, 0, x + (x_merge - 1), cell_content, super_col_style)
#                 x += x_merge
#             else:
#                 sheet.write(0, x, cell_content, super_col_style)
#                 x += 1
        for row in self.get_header(options):
            x = 0
            for column in row:
                colspan = column.get('colspan', 1)
                header_label = column.get('name', '').replace('<br/>', ' ').replace('&nbsp;', ' ')
                if colspan == 1:
                    if x>4 and header_label!='Total':
                        sheet.write(y_offset, x, header_label[0:3], title_style)
                    else:
                        sheet.write(y_offset, x, header_label, title_style)
                else:
                    sheet.merge_range(y_offset, x, y_offset, x + colspan - 1, header_label, title_style)
                x += colspan
            y_offset += 1
        ctx = self._set_context(options)
        ctx.update({'no_format':True, 'print_mode':True, 'prefetch_fields': False})
        # deactivating the prefetching saves ~35% on get_lines running time
        lines = self.with_context(ctx)._get_lines(options)
 
        if options.get('hierarchy'):
            lines = self._create_hierarchy(lines, options)
        if options.get('selected_column'):
            lines = self._sort_lines(lines, options)
 
        #write all data rows
        for y in range(0, len(lines)):
            level = lines[y].get('level')
            if lines[y].get('caret_options'):
                style = level_3_style
                col1_style = level_3_col1_style
            elif level == 0:
                y_offset += 1
                style = level_0_style
                col1_style = style
            elif level == 1:
                style = level_1_style
                col1_style = style
            elif level == 2:
                style = level_2_style
                col1_style = 'total' in lines[y].get('class', '').split(' ') and level_2_col1_total_style or level_2_col1_style
            elif level == 3:
                style = level_3_style
                col1_style = 'total' in lines[y].get('class', '').split(' ') and level_3_col1_total_style or level_3_col1_style
            else:
                style = default_style
                col1_style = default_col1_style
 
            #write the first column, with a specific style to manage the indentation
            cell_type, cell_value = self._get_cell_type_value(lines[y])
            if cell_type == 'date':
                sheet.write_datetime(y + y_offset, 0, cell_value, date_default_col1_style)
            else:
                sheet.write(y + y_offset, 0, cell_value, col1_style)
 
            #write all the remaining cells
            for x in range(1, len(lines[y]['columns']) + 1):
                cell_type, cell_value = self._get_cell_type_value(lines[y]['columns'][x - 1])
                if cell_type == 'date':
                    sheet.write_datetime(y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, date_default_style)
                elif cell_type=='text' and self.is_float(cell_value) and x>4:
                    sheet.write(y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, currency_format)
                else:
                    sheet.write(y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, style)
 
        workbook.close()
        output.seek(0)
        generated_file = output.read()
        output.close()



        return generated_file    
        
    
    def get_pdf(self, options, minimal_layout=True):
        # As the assets are generated during the same transaction as the rendering of the
        # templates calling them, there is a scenario where the assets are unreachable: when
        # you make a request to read the assets while the transaction creating them is not done.
        # Indeed, when you make an asset request, the controller has to read the `ir.attachment`
        # table.
        # This scenario happens when you want to print a PDF report for the first time, as the
        # assets are not in cache and must be generated. To workaround this issue, we manually
        # commit the writes in the `ir.attachment` table. It is done thanks to a key in the context.
        minimal_layout = False
        start = datetime.strptime(str(options['date'].get('date_from')), '%Y-%m-%d').date()
        end = datetime.strptime(options['date'].get('date_to'), '%Y-%m-%d').date()
        start_month_name = start.strftime("%B")
        end_month_name = end.strftime("%B")

        if self.env.user.lang == 'es_MX':
            start_month_name = self.get_month_name(start.month)
            end_month_name = self.get_month_name(end.month)

        header_date = str(start.day) + " de " + start_month_name+" de "+str(start.year)
        header_date += " al "+str(end.day) + " " + end_month_name +" de "+str(end.year)

        actual_date = datetime.today().strftime('%d/%m/%Y')
        if not config['test_enable']:
            self = self.with_context(commit_assetsbundle=True)

        base_url = self.env['ir.config_parameter'].sudo().get_param('report.url') or self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        rcontext = {
            'mode': 'print',
            'base_url': base_url,
            'company': self.env.company,
        }

        body = self.env['ir.ui.view'].render_template(
            "account_reports.print_template",
            values=dict(rcontext),
        )
        body_html = self.with_context(print_mode=True).get_html(options)
        #logging.critical("BODY: "+str(body_html))
        body = body.replace(b'<body class="o_account_reports_body_print">', b'<body class="o_account_reports_body_print">' + body_html)
        
        if minimal_layout:
            header=''
            footer = self.env['ir.actions.report'].render_template("web.internal_layout", values=rcontext)
            spec_paperformat_args = {'data-report-margin-top': 10, 'data-report-header-spacing': 10}
            footer = self.env['ir.actions.report'].render_template("web.minimal_layout", values=dict(rcontext, subst=True, body=footer))
        else:
            rcontext.update({
                    'css': '',
                    'o': self.env.user,
                    'res_company': self.env.company,
                    'header_date': header_date,
                    'header_date_2': actual_date,
                })
#             header = self.env['ir.actions.report'].render_template("jt_income.pdf_external_layout_income_annual_report_new", values=rcontext)
#             
#             header = header.decode('utf-8') # Ensure that headers and footer are correctly encoded
#             current_filtered = self.get_header_year_list(options)
#             header.replace("REAL", "Test")
            image_data = io.BytesIO(base64.standard_b64decode(self.env.user.company_id.header_logo))
            '''header=<div class="header">
                        <div class="row">
                            <div class="col-12 text-center">
                                   <span style="font-size:16px;">DIRECCIÓN GENERAL DE FINANZAS</span><br/>
                                   <span style="font-size:14px;">COMISIONES BANCARIAS INSTITUCIONALES</span><br/>
                                   <span style="font-size:12px;">%s</span><br/>
                                   <span style="font-size:12px;">Fecha de consulta: %s</span><br/>
                            </div>
                        </div>
                    </div>
                %(header_date, actual_date)'''
            header = self.env['ir.actions.report'].render_template(
                "siif_finance.comission_header_id", values=rcontext)
            header = header.decode('utf-8')

            headers = header.encode()
            '''footer = <div class="col-12 text-center">
                            <div class="footer">
                                <small>
                                    <span>Hoja  </span><span class="page"/> de <span class="topage"/>
                                </small>
                            </div>
                        </div>'''
            # parse header as new header contains header, body and footer
            footer = self.env['ir.actions.report'].render_template(
                "siif_finance.comission_footer_id", values=rcontext)
            footer = footer.decode('utf-8')
            footers = footer.encode()
            try:
                root = lxml.html.fromstring(header)

                match_klass = "//div[contains(concat(' ', normalize-space(@class), ' '), ' {} ')]"

                for node in root.xpath(match_klass.format('header')):
                    headers = lxml.html.tostring(node)
                    headers = self.env['ir.actions.report'].render_template("web.minimal_layout", values=dict(rcontext, subst=True, body=headers))
                root = lxml.html.fromstring(footer)
                for node in root.xpath(match_klass.format('footer')):
                    footers = lxml.html.tostring(node)
                    footers = self.env['ir.actions.report'].render_template("web.minimal_layout", values=dict(rcontext, subst=True, body=footers))

            except lxml.etree.XMLSyntaxError:
                headers = header.encode()
                footers = footer.encode()
            header = headers
            footer = footers


        landscape = True
        if len(self.with_context(print_mode=True).get_header(options)[-1]) > 5:
            landscape = True

        reporte = self.env['ir.actions.report'].search([('model','=','comission.report')])

        return reporte._run_wkhtmltopdf(
            [body],
            header=header, footer=footer,
            landscape=landscape,
            specific_paperformat_args=None,
        )

    def get_html(self, options, line_id=None, additional_context=None):
        '''
        return the html value of report, or html value of unfolded line
        * if line_id is set, the template used will be the line_template
        otherwise it uses the main_template. Reason is for efficiency, when unfolding a line in the report
        we don't want to reload all lines, just get the one we unfolded.
        '''
        # Check the security before updating the context to make sure the options are safe.
        self._check_report_security(options)

        # Prevent inconsistency between options and context.
        self = self.with_context(self._set_context(options))

        templates = self._get_templates()
        report_manager = self._get_report_manager(options)
        report = {'name': self._get_report_name(),
                'summary': report_manager.summary,
                'company_name': self.env.company.name,}
        report = {}
        #options.get('date',{}).update({'string':''}) 
        lines = self._get_lines(options, line_id=line_id)

        if options.get('hierarchy'):
            lines = self._create_hierarchy(lines, options)
        if options.get('selected_column'):
            lines = self._sort_lines(lines, options)

        footnotes_to_render = []
        if self.env.context.get('print_mode', False):
            # we are in print mode, so compute footnote number and include them in lines values, otherwise, let the js compute the number correctly as
            # we don't know all the visible lines.
            footnotes = dict([(str(f.line), f) for f in report_manager.footnotes_ids])
            number = 0
            for line in lines:
                f = footnotes.get(str(line.get('id')))
                if f:
                    number += 1
                    line['footnote'] = str(number)
                    footnotes_to_render.append({'id': f.id, 'number': number, 'text': f.text})

        rcontext = {'report': report,
                    'lines': {'columns_header': self.get_header(options), 'lines': lines},
                    'options': {},
                    'context': self.env.context,
                    'model': self,
                }
        if additional_context and type(additional_context) == dict:
            rcontext.update(additional_context)
        if self.env.context.get('analytic_account_ids'):
            rcontext['options']['analytic_account_ids'] = [
                {'id': acc.id, 'name': acc.name} for acc in self.env.context['analytic_account_ids']
            ]

        render_template = templates.get('main_template', 'account_reports.main_template')
        if line_id is not None:
            render_template = templates.get('line_template', 'account_reports.line_template')
        html = self.env['ir.ui.view'].render_template(
            render_template,
            values=dict(rcontext),
        )
        if self.env.context.get('print_mode', False):
            for k,v in self._replace_class().items():
                html = html.replace(k, v)
            # append footnote as well
            html = html.replace(b'<div class="js_account_report_footnotes"></div>', self.get_html_footnotes(footnotes_to_render))
        return html