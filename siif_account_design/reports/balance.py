from odoo import models, api, _
from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.tools.misc import formatLang
from odoo.tools.misc import xlsxwriter
from odoo.exceptions import ValidationError, UserError
import io
import base64
from odoo.tools import config, date_utils, get_lang
import lxml.html
import pandas as pd
import logging
import json


class SiifBalanceReport(models.AbstractModel):
    _name = "siif.balance.report"
    _inherit = "account.coa.report"
    _description = "Siif Balance Report"

    filter_date = {'mode': 'range', 'filter': 'this_month'}
    filter_comparison = None
    filter_all_entries = None
    filter_journals = None
    filter_analytic = None
    filter_unfold_all = None
    filter_cash_basis = None
    filter_hierarchy = None
    filter_unposted_in_period = True
    filter_dependency = True
    filter_account_group = True
    filter_account_account = True
    MAX_LINES = None

    filter_levels = 0

    def siif_print_xlsx(self, options):
        return {
                'type': 'ir_actions_account_report_download',
                'data': {'model': self.env.context.get('model'),
                         'options': json.dumps(options),
                         'output_format': 'xlsx',
                         'financial_id': self.env.context.get('id'),
                         }
                }

    @api.model
    def _get_filter_dependency(self):
        return self.env['dependency'].search([])

    @api.model
    def _get_filter_account_group(self):
        return self.env['account.group'].search([])

    @api.model
    def _get_filter_account_account(self):
        return self.env['account.account'].search([])

    @api.model
    def _init_filter_dependency(self, options, previous_options=None):
        if self.filter_dependency is None:
            return
        if previous_options and previous_options.get('dependency'):
            journal_map = dict((opt['id'], opt['selected']) for opt in previous_options['dependency'] if opt['id'] != 'divider' and 'selected' in opt)
        else:
            journal_map = {}
        options['dependency'] = []

        default_group_ids = []

        for j in self._get_filter_dependency():
            options['dependency'].append({
                'id': j.id,
                'name': j.dependency,
                'selected': journal_map.get(j.id, j.id in default_group_ids),
            })

    @api.model
    def _init_filter_account_group(self, options, previous_options=None):
        if self.filter_account_group is None:
            return
        if previous_options and previous_options.get('account_group'):
            journal_map = dict((opt['id'], opt['selected']) for opt in previous_options['account_group'] if opt['id'] != 'divider' and 'selected' in opt)
        else:
            journal_map = {}
        options['account_group'] = []

        default_group_ids = []

        for j in self._get_filter_account_group():
            options['account_group'].append({
                'id': j.id,
                'name': j.code_prefix+' '+j.name,
                'parent_path': j.parent_path,
                'selected': journal_map.get(j.id, j.id in default_group_ids),
            })


    @api.model
    def _init_filter_account_account(self, options, previous_options=None):
        if self.filter_account_account is None:
            return
        if previous_options and previous_options.get('account_account'):
            journal_map = dict((opt['id'], opt['selected']) for opt in previous_options['account_account'] if opt['id'] != 'divider' and 'selected' in opt)
        else:
            journal_map = {}
        options['account_account'] = []

        default_group_ids = []

        for j in self._get_filter_account_account():
            options['account_account'].append({
                'id': j.id,
                'name': j.code,
                'selected': journal_map.get(j.id, j.id in default_group_ids),
            })


    def _get_reports_buttons(self):
        return [
            {'name': _('Export to PDF'), 'sequence': 1, 'action': 'print_pdf', 'file_export_type': _('PDF')},
            {'name': _('Export (XLSX)'), 'sequence': 2, 'action': 'siif_print_xlsx', 'file_export_type': _('XLSX')},
        ]

    def _get_templates(self):
        templates = super(
            SiifBalanceReport, self)._get_templates()
        templates[
            'main_table_header_template'] = 'account_reports.main_table_header'
        templates['main_template'] = 'account_reports.main_template'
        return templates

    def _get_columns_name(self, options):
        return [
            {'name': _('Cuenta contable')},
            {'name': _('Descripción cuenta')},
            {'name': _('Dependencia')},
            {'name': _('Descripción dependencia')},
            {'name': _('Subdependencia')},
            {'name': _('Descripción subdependencia')},
            {'name': _('Debe')},
            {'name': _('Haber')},
            {'name': _('Debe')},
            {'name': _('Haber')},
            {'name': _('Debe')},
            {'name': _('Haber')},
        ]

    def _format(self, value,figure_type):
        if self.env.context.get('no_format'):
            return value
        value['no_format_name'] = value['name']
        
        if figure_type == 'float':
            currency_id = self.env.company.currency_id
            if currency_id.is_zero(value['name']):
                # don't print -0.0 in reports
                value['name'] = abs(value['name'])
                value['class'] = 'number text-muted'
            value['name'] = formatLang(self.env, value['name'], currency_obj=currency_id)
            value['class'] = 'number'
            return value
        if figure_type == 'percents':
            value['name'] = str(round(value['name'] * 100, 1)) + '%'
            value['class'] = 'number'
            return value
        value['name'] = round(value['name'], 1)
        return value['name']


    def _get_lines(self, options, line_id=None):

        lines = []
        columns = []
        self.env['ir.rule'].clear_cache()
        start = datetime.strptime(
            str(options['date'].get('date_from')), '%Y-%m-%d').date()
        end = datetime.strptime(
            str(options['date'].get('date_to')), '%Y-%m-%d').date()

        dependency_list = []
        group_list = []
        account_list = []
        id_group_list = []
        level_list = []

        #### Se obtienen los ids de las dependencias para agregarlos al where de las consultas####
        if options.get('dependency')!=None:
            for select_dependency in options.get('dependency'):
                if select_dependency.get('selected',False)==True:
                    dependency_list.append(select_dependency.get('id',0))
        
        if len(dependency_list)>1:
            dep_where = "and am.dependency_id in "+str(dependency_list).replace('[','(').replace(']',')')
        elif len(dependency_list)==1:
            dep_where = "and am.dependency_id = "+str(dependency_list[0])
        else:
            dep_where = ""

        ###Dependencias del filtro de la balanza UNAM
        dependency_ids = options.get('domain_dep', [])
        if len(dependency_ids)>1:
            dep_balance_where = "and am.dependency_id in "+str(dependency_ids).replace('[','(').replace(']',')')
        elif len(dependency_ids)==1:
            dep_balance_where = "and am.dependency_id = "+str(dependency_ids[0])
        else:
            dep_balance_where = ""

        sub_dependency_ids = options.get('domain_sd', [])
        #logging.critical("Subdependencias: "+str(sub_dependency_ids))
        if len(sub_dependency_ids)>1:
            sub_dep_balance_where = "and am.sub_dependency_id in "+str(sub_dependency_ids).replace('[','(').replace(']',')')
        elif len(sub_dependency_ids)==1:
            sub_dep_balance_where = "and am.sub_dependency_id = "+str(sub_dependency_ids[0])
        else:
            sub_dep_balance_where = ""

        ###Cuentas del filtro de la balanza UNAM
        account_ids = options.get('account_ids', [])
        if len(account_ids)>1:
            account_balance_where = "and am.account_id in "+str(account_ids).replace('[','(').replace(']',')')
        elif len(account_ids)==1:
            account_balance_where = "and am.account_id = "+str(account_ids[0])
        else:
            account_balance_where = ""


        #### Se obtienen los ids de las cuentas contables para agregarlos al where de las consultas####
        if options.get('account_account')!=None:
            for select_account in options.get('account_account'):
                if select_account.get('selected',False)==True:
                    account_list.append(select_account.get('id',0))
        
        if len(account_list)>1:
            account_where = "and am.account_id in "+str(account_list).replace('[','(').replace(']',')')
        elif len(account_list)==1:
            account_where = "and am.account_id = "+str(account_list[0])
        else:
            account_where = ""


        ### Se obtienen los ids y los parent_path de los grupos de cuentas para gregarlos a los where de las consultas ###
        if options.get('account_group')!=None:
            for select_groups in options.get('account_group'):
                if select_groups.get('selected',False)==True:
                    group_list.append(select_groups.get('parent_path',0))
                    id_group_list.append(select_groups.get('id',0))
        
        if len(id_group_list)>1:
            id_group_where = "or acc.group_id in "+str(id_group_list).replace('[','(').replace(']',')')
        elif len(id_group_list)==1:
            id_group_where = "or acc.group_id = "+str(id_group_list[0])
        else:
            id_group_where = ""
        
        if len(group_list)>1:
            r=0
            group_where=""
            group_query_where="where "
            for group in group_list:
                if r==0:
                    group_where += "and (grp.parent_path like "+"'"+str(group)+"%'"
                    group_query_where += "grp.parent_path like "+"'"+str(group)+"%'"
                else:
                    group_where += " or grp.parent_path like "+"'"+str(group)+"%'"
                    group_query_where += " or grp.parent_path like "+"'"+str(group)+"%'"
                r=r+1
            group_where += ")"
        elif len(group_list)==1:
            group_where = "and grp.parent_path like "+"'"+str(group_list[0])+"%'" 
            group_query_where = "where grp.parent_path like "+"'"+str(group_list[0])+"%'" 
        else:
            group_where = ""
            group_query_where = ""

        #### Se obtienen los niveles####
        level = options.get('levels', 0)

        if level==0:
            ##### Universo de subdependencias ######

            query_1 = f"""select distinct
                            am.account_id,
                            acc.name, acc.code, acc.group_id,
                            grp.parent_path, grp.parent_id, grp.code_prefix,
                            d.dependency,
                            am.dependency_id,
                            sd.sub_dependency,
                            am.sub_dependency_id,
                            d.description,
                            sd.description as description_subdependency,
                            am.account_id || '-' || coalesce(am.dependency_id,0) || '-' || coalesce(sd.id,0) as ids

                            FROM account_move_line am
                            inner join account_account acc
                                            on acc.id = am.account_id
                            inner join account_group grp
                                            on grp.id = acc.group_id
                            LEFT JOIN dependency d
                                            on d.id = am.dependency_id
                            LEFT JOIN sub_dependency sd
                                            on sd.dependency_id = d.id and sd.id = am.sub_dependency_id
                            WHERE
                            --AND
                            (
                                            -- Saldos inciales
                                            (extract(YEAR FROM am.date) = {start.year-1} and am.journal_id = 430) OR
                                            -- Movimientos
                                            (am.date between '{start.year}-01-01' and '{end}')
                            )
                            AND am.parent_state = 'posted' {dep_balance_where} {account_balance_where} {sub_dep_balance_where} {dep_where} {group_where} {id_group_where} {account_where}
                            ORDER BY acc.code, d.dependency, sd.sub_dependency;""" 
            #logging.critical("Query 1: "+str(query_1))

            self.env.cr.execute(query_1)

            unMovCtasDS = self.env.cr.fetchall()
            
            unMovCtasDS = pd.DataFrame(unMovCtasDS, columns=['account_id', 'name', 'code', 'group_id', 'parent_path', 'parent_id', 'code_prefix', 'dependency', 'dependency_id', 'sub_dependency', 'sub_dependency_id', 'description', 'description_subdependency', 'ids'])

            ########## Saldos iniciales #########


            query_2 = f"""select distinct
                    am.account_id,
                    am.dependency_id,
                    am.sub_dependency_id,
                    am.account_id || '-' || coalesce(am.dependency_id,0) || '-' || coalesce(am.sub_dependency_id,0) as ids,
                    sum(am.debit) as debit_si,
                    sum(am.credit) as credit_si,
                    sum(am.balance) as balance_si
     
                    FROM account_move_line am
                    inner join account_account acc
                        on acc.id = am.account_id
                    inner join account_group grp
                        on grp.id = acc.group_id
     
                    WHERE
                    --AND
                    (
                                    -- Saldos inciales
                                    (extract(YEAR FROM am.date) = {start.year-1} and am.journal_id = 430) OR
                                    -- Movimientos
                                    (am.date between '{start.year}-01-01' and '{start-timedelta(days=1)}')
                    )
                    AND am.parent_state = 'posted' {dep_balance_where} {account_balance_where} {sub_dep_balance_where} {dep_where} {group_where} {id_group_where} {account_where}
                    group by am.account_id, am.dependency_id, am.sub_dependency_id, ids
                    ORDER BY am.account_id, am.dependency_id, am.sub_dependency_id;"""

            #logging.critical("Query 2: "+str(query_2))


            self.env.cr.execute(query_2)

            unSI = self.env.cr.fetchall()

            

            unSI = pd.DataFrame(unSI, columns=['account_id','dependency_id', 'sub_dependency_id', 'ids', 'debit_si', 'credit_si', 'balance_si'])

            ##### Movimientos #####

            query_3 = f"""select distinct
                    am.account_id,
                    am.dependency_id,
                    am.sub_dependency_id,
                    am.account_id || '-' || coalesce(am.dependency_id,0) || '-' || coalesce(am.sub_dependency_id,0) as ids,
                    sum(am.debit) as debit_movs,
                    sum(am.credit) as credit_movs,
                    sum(am.balance) as balance_movs
     
                    FROM account_move_line am
                    inner join account_account acc
                        on acc.id = am.account_id
                    inner join account_group grp
                        on grp.id = acc.group_id
     
                    WHERE
                    --AND
                    (
                                    -- Saldos inciales
                                    --(extract(YEAR FROM am.date) = '2021' and am.journal_id = 430) --OR
                                    -- Movimientos
                                    (am.date between '{start}' and '{end}')
                    )
                    AND am.parent_state = 'posted' {dep_balance_where} {account_balance_where} {sub_dep_balance_where} {dep_where} {group_where} {id_group_where} {account_where}
                    group by am.account_id, am.dependency_id, am.sub_dependency_id, ids
                    ORDER BY am.account_id, am.dependency_id, am.sub_dependency_id;"""

            #logging.critical("Query3: "+str(query_3))


            self.env.cr.execute(query_3)

            unMov = self.env.cr.fetchall()

            unMov = pd.DataFrame(unMov, columns=['account_id','dependency_id', 'sub_dependency_id', 'ids', 'debit_movs', 'credit_movs', 'balance_movs'])


            ##### Universo de cortes por dependencia ######

            query_6 = f"""select distinct
                    am.account_id,
                    acc.name, acc.code, acc.group_id,
                    grp.parent_path, grp.parent_id, grp.code_prefix,
                    d.dependency,
                    am.dependency_id,
                    0 as sub_dependency,
                    '' as sub_dependency_id,
                    d.description,
                    '' as description_subdependency,
                    am.account_id || '-' || coalesce(am.dependency_id,0) || '-0' as ids
                    FROM account_move_line am
                    inner join account_account acc
                                    on acc.id = am.account_id
                    inner join account_group grp
                                    on grp.id = acc.group_id
                    INNER JOIN dependency d
                                    on d.id = am.dependency_id
                    WHERE
                    --AND
                    (
                            -- Saldos inciales
                            (extract(YEAR FROM am.date) = {start.year-1} and am.journal_id = 430) OR
                            -- Movimientos
                            (am.date between '{start.year}-01-01' and '{end}')
                    )
                    AND am.parent_state = 'posted' {dep_balance_where} {account_balance_where} {sub_dep_balance_where} {dep_where} {group_where} {id_group_where} {account_where}
                    ORDER BY acc.code, d.dependency;"""

            #logging.critical("Query 6: "+str(query_6))

            self.env.cr.execute(query_6)

            cteDep = self.env.cr.fetchall()

            cteDep = pd.DataFrame(cteDep, columns=['account_id', 'name', 'code', 'group_id', 'parent_path', 'parent_id', 'code_prefix', 'dependency', 'dependency_id', 'sub_dependency', 'sub_dependency_id', 'description', 'description_subdependency', 'ids'])

            ##### Cortes de saldos iniciales por dependencia ####


            query_7 = f"""select distinct
                    am.account_id,
                    am.dependency_id,
                    '' as sub_dependency_id,
                    am.account_id || '-' || coalesce(am.dependency_id,0) || '-0' as ids,
                    sum(am.debit) as debit_si,
                    sum(am.credit) as credit_si,
                    sum(am.balance) as balance_si
                    FROM account_move_line am
                    inner join account_account acc
                        on acc.id = am.account_id
                    inner join account_group grp
                        on grp.id = acc.group_id
                    INNER JOIN dependency d
                    ON am.dependency_id = d.id
                    WHERE
                    (
                        -- Saldos inciales
                        (extract(YEAR FROM am.date) = {start.year-1} and am.journal_id = 430) OR
                        -- Movimientos
                        (am.date between '{start.year}-01-01' and '{start-timedelta(days=1)}')
                    ) 
                    AND am.parent_state = 'posted' {dep_balance_where} {account_balance_where} {sub_dep_balance_where} {dep_where} {group_where} {id_group_where} {account_where}
                    group by am.account_id, am.dependency_id, ids
                    ORDER BY am.account_id, am.dependency_id;"""

            #logging.critical("Query 7: "+str(query_7))


            self.env.cr.execute(query_7)

            cteSI = self.env.cr.fetchall()

            cteSI = pd.DataFrame(cteSI, columns=['account_id', 'dependency_id', 'sub_dependency_id', 'ids', 'debit_si','credit_si', 'balance_si'])


            #### Cortes de movimientos por dependencias ####

            query_8 = f"""select distinct
                    am.account_id,
                    am.dependency_id,
                    '' as sub_dependency_id,
                    am.account_id || '-' || coalesce(am.dependency_id,0) || '-0' as ids,
                    sum(am.debit) as debit_movs,
                    sum(am.credit) as credit_movs,
                    sum(am.balance) as balance_movs
                    FROM account_move_line am
                    inner join account_account acc
                        on acc.id = am.account_id
                    inner join account_group grp
                        on grp.id = acc.group_id
                    INNER JOIN dependency d
                    ON am.dependency_id = d.id     
                    WHERE
                    (
                            -- Saldos inciales
                            --(extract(YEAR FROM am.date) = '2021' and am.journal_id = 430) --OR
                            -- Movimientos
                            (am.date between '{start}' and '{end}')
                    )
                    AND am.parent_state = 'posted' {dep_balance_where} {account_balance_where} {sub_dep_balance_where} {dep_where} {group_where} {id_group_where} {account_where}
                    group by am.account_id, am.dependency_id, ids
                    ORDER BY am.account_id, am.dependency_id;"""

            #logging.critical("Query 8: "+str(query_8))

            self.env.cr.execute(query_8)

            cteMov = self.env.cr.fetchall()

            cteMov = pd.DataFrame(cteMov, columns=['account_id', 'dependency_id', 'sub_dependency_id', 'ids', 'debit_movs','credit_movs', 'balance_movs'])


            query_gpos = f"""select grp.name, grp.code_prefix, grp.parent_path, grp.parent_id, length(grp.code_prefix) as length
                            from account_group grp {group_query_where}
                            order by length, grp.code_prefix;"""

            #logging.critical("Query gpos: "+str(query_gpos))

            self.env.cr.execute(query_gpos)

            Gpos = self.env.cr.fetchall()

            Gpos = pd.DataFrame(Gpos, columns=['name', 'code_prefix', 'parent_path', 'parent_id', 'length'])

            ### Query_9 Cortes cta por dependencias

            query_9 = f"""select distinct
                            am.account_id,
                            acc.name, acc.code, acc.group_id,
                            grp.parent_path, grp.parent_id, grp.code_prefix,
                            0 dependency,
                            '' dependency_id,
                            0 as sub_dependency,
                            '' as sub_dependency_id,
                            '' as description,
                            '' as description_subdependency,
                            am.account_id || '-0-0' as ids
                        FROM account_move_line am
                        inner join account_account acc
                           on acc.id = am.account_id
                        inner join account_group grp
                           on grp.id = acc.group_id
                        INNER JOIN dependency d
                           on d.id = am.dependency_id
                        WHERE
                          (
                           -- Saldos inciales
                          (extract(YEAR FROM am.date) = {start.year-1}  and am.journal_id = 430) OR
                           -- Movimientos
                           (am.date between '{start.year}-01-01' and '{end}')
                            )
                          AND am.parent_state = 'posted' {dep_balance_where} {account_balance_where} {sub_dep_balance_where} {dep_where} {group_where} {id_group_where} {account_where}

                        ORDER BY acc.code;"""

            #logging.critical("Query 9: "+str(query_9))

            self.env.cr.execute(query_9)

            cteCtaD = self.env.cr.fetchall()

            cteCtaD = pd.DataFrame(cteCtaD, columns=['account_id', 'name', 'code', 'group_id', 'parent_path', 'parent_id', 'code_prefix', 'dependency', 'dependency_id', 'sub_dependency', 'sub_dependency_id', 'description', 'description_subdependency', 'ids'])

            query_10 = f"""select distinct
                            am.account_id,
                            '' dependency_id,
                                '' as sub_dependency_id,
                            am.account_id || '-0-0' as ids,
                            sum(am.debit) as debit_si,
                            sum(am.credit) as credit_si,
                            sum(am.balance) as balance_si
                        FROM account_move_line am
                        inner join account_account acc
                            on acc.id = am.account_id
                        inner join account_group grp
                            on grp.id = acc.group_id
                        INNER JOIN dependency d
                           ON am.dependency_id = d.id
                        WHERE
                          (
                            -- Saldos inciales
                            (extract(YEAR FROM am.date) = {start.year-1} and am.journal_id = 430) OR
                            -- Movimientos
                            (am.date between '{start.year}-01-01' and '{start-timedelta(days=1)}')
                          )
                        AND am.parent_state = 'posted' {dep_balance_where} {account_balance_where} {sub_dep_balance_where} {dep_where} {group_where} {id_group_where} {account_where}
                        group by am.account_id, ids
                        ORDER BY am.account_id;"""

            #logging.critical("Query 10: "+str(query_10))

            self.env.cr.execute(query_10)

            cteCtaSI = self.env.cr.fetchall()

            cteCtaSI = pd.DataFrame(cteCtaSI, columns=['account_id', 'dependency_id', 'sub_dependency_id', 'ids', 'debit_si','credit_si', 'balance_si'])

            query_11 = f"""select distinct
                            am.account_id,
                            '' as dependency_id,
                            '' as sub_dependency_id,
                            am.account_id || '-0-0' as ids,
                            sum(am.debit) as debit_movs,
                            sum(am.credit) as credit_movs,
                            sum(am.balance) as balance_movs
                        FROM account_move_line am
                        inner join account_account acc
                            on acc.id = am.account_id
                        inner join account_group grp
                            on grp.id = acc.group_id
                        INNER JOIN dependency d
                           ON am.dependency_id = d.id     
                        WHERE
                            (am.date between '{start}' and '{end}')
                        AND am.parent_state = 'posted' {dep_balance_where} {account_balance_where} {sub_dep_balance_where} {dep_where} {group_where} {id_group_where} {account_where}
                        group by am.account_id,  ids
                        ORDER BY am.account_id"""

            #logging.critical("Query 11: "+str(query_11))

            self.env.cr.execute(query_11)

            cteCtaMov = self.env.cr.fetchall()

            cteCtaMov = pd.DataFrame(cteCtaMov, columns=['account_id', 'dependency_id', 'sub_dependency_id', 'ids', 'debit_movs','credit_movs', 'balance_movs'])

            ######## SALDOS INICIALES ########

            # Quitar columnas
            unSI = unSI.drop(columns=['account_id', 'dependency_id', 'sub_dependency_id'])
            cteSI = cteSI.drop(columns=['account_id', 'dependency_id', 'sub_dependency_id'])
            cteCtaSI = cteCtaSI.drop(columns=['account_id', 'dependency_id', 'sub_dependency_id'])


            # Quitar columnas
            unMov = unMov.drop(columns=['account_id', 'dependency_id', 'sub_dependency_id'])
            cteMov = cteMov.drop(columns=['account_id', 'dependency_id', 'sub_dependency_id'])
            cteCtaMov = cteCtaMov.drop(columns=['account_id', 'dependency_id', 'sub_dependency_id'])


            ##################### UNIR LOS MOVIMIENTOS ############################
            # Unir las consultas
            unCtasMovSI = pd.merge(unMovCtasDS, unSI, on='ids', how='outer')
            movCtasSIMov = pd.merge(unCtasMovSI, unMov, on='ids', how='outer')

            # Sustituir valores NaN por 0
            movCtasSIMov[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']] = movCtasSIMov[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']].fillna(0)
            totalSIMov = movCtasSIMov[['debit_si','credit_si','balance_si','debit_movs','credit_movs','balance_movs']].sum()

            ##################### UNIR LOS CORTES ############################
            # Unir las consultas
            cortesDep = pd.merge(cteDep, cteSI, on='ids', how='outer')
            cortesMovSI = pd.merge(cortesDep, cteMov, on='ids', how='outer')

            # Sustituir valores NaN por 0
            cortesMovSI[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']] = cortesMovSI[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']].fillna(0)
            #cortesMovSI.to_csv('/home/bryan/Documentos/IINGEN-movCtesCtaSIMov.csv')

            ##################### UNIR LOS CORTES DE LAS CUENTAS CON DEPENDENCIA ############################
            # Unir las consultas
            uncorteCta = pd.merge(cteCtaD, cteCtaSI, on='ids', how='outer')
            ctesCtasMovSI = pd.merge(uncorteCta, cteCtaMov, on='ids', how='outer')

            # Sustituir valores NaN por 0
            ctesCtasMovSI[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']] = ctesCtasMovSI[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']].fillna(0)
            #ctesCtasMovSI.to_csv('/home/bryan/Documentos/IINGEN-movCtesSIMov.csv')


            # ORdenar por el largo del código y el código
            Gpos = Gpos.sort_values(['length','code_prefix'])

            #Recorrer los grupos ordenados 
            movCtasSIMovCtes = movCtasSIMov.copy()

            for i in range(len(Gpos)):
                movgpo1 = movCtasSIMov[movCtasSIMov.parent_path.str.startswith(Gpos.iloc[i]['parent_path'], na=False)]   
                balanceF = movgpo1[['debit_si','credit_si','balance_si','debit_movs','credit_movs','balance_movs']].sum()
                if (balanceF['debit_si'] > 0 or balanceF['credit_si'] > 0 or balanceF['debit_movs'] or balanceF['credit_movs']):
                    # Agregar el renglón del corte
                    encabezado = {"name": Gpos.iloc[i]['name'], "code": Gpos.iloc[i]['code_prefix'], "parent_path": Gpos.iloc[i]['parent_path'], 
                                 "parent_id": Gpos.iloc[i]['parent_id'], "code_prefix": Gpos.iloc[i]['code_prefix'],
                                 "debit_si": balanceF['debit_si'], "credit_si": balanceF['credit_si'], "balance_si": balanceF['balance_si'],
                                 "debit_movs": balanceF['debit_movs'], "credit_movs": balanceF['credit_movs'], "balance_movs": balanceF['balance_movs']}       
                    new_row = pd.DataFrame(encabezado,index=[0])
                    movCtasSIMovCtes = pd.concat([movCtasSIMovCtes, new_row])


            # Agregar los cortes de las dependencias
            movCtasSIMovCtes = pd.concat([movCtasSIMovCtes, cortesMovSI])
            # Agregar los cortes de las cuentas que tiene dependencias y subdependencias
            movCtasSIMovCtes = pd.concat([movCtasSIMovCtes, ctesCtasMovSI])


            # Ordenar la balanza por Code, dependencia y subdependencia.    
            movCtasSIMovCtes = movCtasSIMovCtes.drop(columns=['account_id', 'group_id', 'parent_path', 'parent_id', 'code_prefix', 'dependency_id','sub_dependency_id',  'ids', 'balance_movs'])
            movCtasSIMovCtes = movCtasSIMovCtes.sort_values(['code','dependency','sub_dependency'])

            movCtasSIMovCtes['dependency'] = movCtasSIMovCtes['dependency'].fillna(0)
            movCtasSIMovCtes['sub_dependency'] = movCtasSIMovCtes['sub_dependency'].fillna(0)

            movCtasSIMovCtes['description'] = movCtasSIMovCtes['description'].fillna('')
            movCtasSIMovCtes['description_subdependency'] = movCtasSIMovCtes['description_subdependency'].fillna('')

            movCtasSIMovCtes['dependency'] = movCtasSIMovCtes['dependency'].astype(int)
            movCtasSIMovCtes['sub_dependency'] = movCtasSIMovCtes['sub_dependency'].astype(int)

            #logging.critical(str(movCtasSIMovCtes.iloc[0]['name']))
        
        elif level==1:
            query_1 = f"""SELECT distinct ag.id, ag.name, ag.code_prefix, ag.id, ag.parent_path, ag.parent_id, ag.code_prefix, '0' as dependency, '0' as sub_dependency, '' as description, '' as description_subdependency, ag.id || '-0-0' as ids
                        FROM account_group ag
                        INNER JOIN account_account aa 
                        ON ag.code_prefix = substring(aa.code, 1, length(ag.code_prefix))
                        INNER JOIN account_move_line am
                        on aa.id = am.account_id
                        WHERE
                        length(ag.code_prefix)=3
                        AND
                        (
                                        -- Saldos inciales
                                        (extract(YEAR FROM am.date) = {start.year-1} and am.journal_id = 430) OR
                                        -- Movimientos
                                        (am.date between '{start.year}-01-01' and '{end}')
                        )
                        AND am.parent_state = 'posted'
                        ORDER BY ag.code_prefix;"""
            self.env.cr.execute(query_1)

            unMovCtasDS = self.env.cr.fetchall()
        
            unMovCtasDS = pd.DataFrame(unMovCtasDS, columns=['account_id', 'name', 'code', 'group_id', 'parent_path', 'parent_id', 'code_prefix', 'dependency', 'sub_dependency', 'description', 'description_subdependency', 'ids'])

            query_2 = f"""SELECT distinct ag.id, ag.id || '-0-0' as ids,
                        sum(am.debit) as debit_si,
                        sum(am.credit) as credit_si,
                        sum(am.balance) as balance_si
                        FROM account_group ag
                        INNER JOIN account_account aa 
                        ON ag.code_prefix = substring(aa.code, 1, length(ag.code_prefix))
                        INNER JOIN account_move_line am
                        on aa.id = am.account_id
                        WHERE
                        length(ag.code_prefix)=3
                        AND
                        (
                                        -- Saldos inciales
                                        (extract(YEAR FROM am.date) = {start.year-1} and am.journal_id = 430) OR
                                        -- Movimientos
                                        (am.date between '{start.year}-01-01' and '{start-timedelta(days=1)}')
                        )
                        AND am.parent_state = 'posted'
                        group by ag.id, ids
                        ORDER BY ag.id;"""

            self.env.cr.execute(query_2)

            unSI = self.env.cr.fetchall()

            unSI = pd.DataFrame(unSI, columns=['account_id', 'ids', 'debit_si', 'credit_si', 'balance_si'])

            query_3 = f"""SELECT distinct ag.id, ag.id || '-0-0' as ids,
                        sum(am.debit) as debit_movs,
                        sum(am.credit) as credit_movs,
                        sum(am.balance) as balance_movs
                        FROM account_group ag
                        INNER JOIN account_account aa 
                        ON ag.code_prefix = substring(aa.code, 1, length(ag.code_prefix))
                        INNER JOIN account_move_line am
                        on aa.id = am.account_id
                        WHERE
                        length(ag.code_prefix)=3
                        AND
                        (
                                        -- Movimientos
                                        (am.date between '{start}' and '{end}')
                        )
                        AND am.parent_state = 'posted'
                        group by ag.id, ids
                        ORDER BY ag.id;"""
            self.env.cr.execute(query_3)

            unMov = self.env.cr.fetchall()

            unMov = pd.DataFrame(unMov, columns=['account_id','ids', 'debit_movs', 'credit_movs', 'balance_movs'])

            ##################### UNIR LOS MOVIMIENTOS ############################
            # Unir las consultas
            unCtasMovSI = pd.merge(unMovCtasDS, unSI, on='ids', how='outer')
            movCtasSIMov = pd.merge(unCtasMovSI, unMov, on='ids', how='outer')
            movCtasSIMovCtes = pd.merge(unCtasMovSI, unMov, on='ids', how='outer')

            # Sustituir valores NaN por 0
            movCtasSIMov[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']] = movCtasSIMov[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']].fillna(0)
            movCtasSIMovCtes[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']] = movCtasSIMovCtes[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']].fillna(0)
            totalSIMov = movCtasSIMov[['debit_si','credit_si','balance_si','debit_movs','credit_movs','balance_movs']].sum()

        elif level==2:
            query_1 = f"""select distinct ag.id, ag.name, ag.code_prefix, ag.id, ag.parent_path, ag.parent_id, ag.code_prefix, '0' as dependency, '0' as sub_dependency, '' as description, '' as description_subdependency, ag.id || '-0-0' as ids
                            FROM account_group ag
                            INNER JOIN account_account aa 
                            ON ag.code_prefix = substring(aa.code, 1, length(ag.code_prefix))
                            INNER JOIN account_move_line am
                            on aa.id = am.account_id
                            WHERE
                            length(ag.code_prefix)-length(replace(ag.code_prefix,'.',''))=1
                            AND
                            (
                                            -- Saldos inciales
                                            (extract(YEAR FROM am.date) = {start.year-1} and am.journal_id = 430) OR
                                            -- Movimientos
                                            (am.date between '{start.year}-01-01' and '{end}')
                            )
                            AND am.parent_state = 'posted'
                            ORDER BY ag.code_prefix;"""
            self.env.cr.execute(query_1)

            unMovCtasDS = self.env.cr.fetchall()
        
            unMovCtasDS = pd.DataFrame(unMovCtasDS, columns=['account_id', 'name', 'code', 'group_id', 'parent_path', 'parent_id', 'code_prefix', 'dependency', 'sub_dependency', 'description', 'description_subdependency', 'ids'])

            query_2 = f"""SELECT distinct ag.id, ag.id || '-0-0' as ids,
                        sum(am.debit) as debit_si,
                        sum(am.credit) as credit_si,
                        sum(am.balance) as balance_si
                        FROM account_group ag
                        INNER JOIN account_account aa 
                        ON ag.code_prefix = substring(aa.code, 1, length(ag.code_prefix))
                        INNER JOIN account_move_line am
                        on aa.id = am.account_id
                        WHERE
                        length(ag.code_prefix)-length(replace(ag.code_prefix,'.',''))=1
                        AND
                        (
                                        -- Saldos inciales
                                        (extract(YEAR FROM am.date) = {start.year-1} and am.journal_id = 430) OR
                                        -- Movimientos
                                        (am.date between '{start.year}-01-01' and '{start-timedelta(days=1)}')
                        )
                        AND am.parent_state = 'posted'
                        group by ag.id, ids
                        ORDER BY ag.id;"""

            self.env.cr.execute(query_2)

            unSI = self.env.cr.fetchall()

            unSI = pd.DataFrame(unSI, columns=['account_id', 'ids', 'debit_si', 'credit_si', 'balance_si'])

            query_3 = f"""SELECT distinct ag.id, ag.id || '-0-0' as ids,
                        sum(am.debit) as debit_movs,
                        sum(am.credit) as credit_movs,
                        sum(am.balance) as balance_movs
                        FROM account_group ag
                        INNER JOIN account_account aa 
                        ON ag.code_prefix = substring(aa.code, 1, length(ag.code_prefix))
                        INNER JOIN account_move_line am
                        on aa.id = am.account_id
                        WHERE
                        length(ag.code_prefix)-length(replace(ag.code_prefix,'.',''))=1
                        AND
                        (
                                        -- Movimientos
                                        (am.date between '{start}' and '{end}')
                        )
                        AND am.parent_state = 'posted'
                        group by ag.id, ids
                        ORDER BY ag.id;"""
            self.env.cr.execute(query_3)

            unMov = self.env.cr.fetchall()

            unMov = pd.DataFrame(unMov, columns=['account_id','ids', 'debit_movs', 'credit_movs', 'balance_movs'])

            ##################### UNIR LOS MOVIMIENTOS ############################
            # Unir las consultas
            unCtasMovSI = pd.merge(unMovCtasDS, unSI, on='ids', how='outer')
            movCtasSIMov = pd.merge(unCtasMovSI, unMov, on='ids', how='outer')
            movCtasSIMovCtes = pd.merge(unCtasMovSI, unMov, on='ids', how='outer')

            # Sustituir valores NaN por 0
            movCtasSIMov[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']] = movCtasSIMov[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']].fillna(0)
            movCtasSIMovCtes[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']] = movCtasSIMovCtes[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']].fillna(0)
            totalSIMov = movCtasSIMov[['debit_si','credit_si','balance_si','debit_movs','credit_movs','balance_movs']].sum()

        elif level==3:
            query_1 = f"""select distinct ag.id, ag.name, ag.code_prefix, ag.id, ag.parent_path, ag.parent_id, ag.code_prefix, '0' as dependency, '0' as sub_dependency, '' as description, '' as description_subdependency, ag.id || '-0-0' as ids
                            FROM account_group ag
                            INNER JOIN account_account aa 
                            ON ag.code_prefix = substring(aa.code, 1, length(ag.code_prefix))
                            INNER JOIN account_move_line am
                            on aa.id = am.account_id
                            WHERE
                            length(ag.code_prefix)-length(replace(ag.code_prefix,'.',''))=2
                            AND
                            (
                                            -- Saldos inciales
                                            (extract(YEAR FROM am.date) = {start.year-1} and am.journal_id = 430) OR
                                            -- Movimientos
                                            (am.date between '{start.year}-01-01' and '{end}')
                            )
                            AND am.parent_state = 'posted'
                            ORDER BY ag.code_prefix;"""
            self.env.cr.execute(query_1)

            unMovCtasDS = self.env.cr.fetchall()
        
            unMovCtasDS = pd.DataFrame(unMovCtasDS, columns=['account_id', 'name', 'code', 'group_id', 'parent_path', 'parent_id', 'code_prefix', 'dependency', 'sub_dependency', 'description', 'description_subdependency', 'ids'])

            query_2 = f"""SELECT distinct ag.id, ag.id || '-0-0' as ids,
                        sum(am.debit) as debit_si,
                        sum(am.credit) as credit_si,
                        sum(am.balance) as balance_si
                        FROM account_group ag
                        INNER JOIN account_account aa 
                        ON ag.code_prefix = substring(aa.code, 1, length(ag.code_prefix))
                        INNER JOIN account_move_line am
                        on aa.id = am.account_id
                        WHERE
                        length(ag.code_prefix)-length(replace(ag.code_prefix,'.',''))=2
                        AND
                        (
                                        -- Saldos inciales
                                        (extract(YEAR FROM am.date) = {start.year-1} and am.journal_id = 430) OR
                                        -- Movimientos
                                        (am.date between '{start.year}-01-01' and '{start-timedelta(days=1)}')
                        )
                        AND am.parent_state = 'posted'
                        group by ag.id, ids
                        ORDER BY ag.id;"""

            self.env.cr.execute(query_2)

            unSI = self.env.cr.fetchall()

            unSI = pd.DataFrame(unSI, columns=['account_id', 'ids', 'debit_si', 'credit_si', 'balance_si'])

            query_3 = f"""SELECT distinct ag.id, ag.id || '-0-0' as ids,
                        sum(am.debit) as debit_movs,
                        sum(am.credit) as credit_movs,
                        sum(am.balance) as balance_movs
                        FROM account_group ag
                        INNER JOIN account_account aa 
                        ON ag.code_prefix = substring(aa.code, 1, length(ag.code_prefix))
                        INNER JOIN account_move_line am
                        on aa.id = am.account_id
                        WHERE
                        length(ag.code_prefix)-length(replace(ag.code_prefix,'.',''))=2
                        AND
                        (
                                        -- Movimientos
                                        (am.date between '{start}' and '{end}')
                        )
                        AND am.parent_state = 'posted'
                        group by ag.id, ids
                        ORDER BY ag.id;"""
            self.env.cr.execute(query_3)

            unMov = self.env.cr.fetchall()

            unMov = pd.DataFrame(unMov, columns=['account_id','ids', 'debit_movs', 'credit_movs', 'balance_movs'])

            ##################### UNIR LOS MOVIMIENTOS ############################
            # Unir las consultas
            unCtasMovSI = pd.merge(unMovCtasDS, unSI, on='ids', how='outer')
            movCtasSIMov = pd.merge(unCtasMovSI, unMov, on='ids', how='outer')
            movCtasSIMovCtes = pd.merge(unCtasMovSI, unMov, on='ids', how='outer')

            # Sustituir valores NaN por 0
            movCtasSIMov[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']] = movCtasSIMov[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']].fillna(0)
            movCtasSIMovCtes[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']] = movCtasSIMovCtes[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']].fillna(0)
            totalSIMov = movCtasSIMov[['debit_si','credit_si','balance_si','debit_movs','credit_movs','balance_movs']].sum()

        elif level==4:
            if len(dependency_ids)==0 and len(sub_dependency_ids)==0:
                query_1 = f"""select distinct
                                am.account_id,
                                acc.name, acc.code, acc.group_id,
                                grp.parent_path, grp.parent_id, grp.code_prefix,
                                d.dependency,
                                am.dependency_id,
                                sd.sub_dependency,
                                am.sub_dependency_id,
                                d.description,
                                sd.description as description_subdependency,
                                am.account_id || '-' || coalesce(am.dependency_id,0) || '-' || coalesce(sd.id,0) as ids
                                FROM account_move_line am
                                inner join account_account acc
                                                on acc.id = am.account_id
                                inner join account_group grp
                                                on acc.group_id=grp.id
                                left JOIN dependency d
                                                on d.id = am.dependency_id
                                left JOIN sub_dependency sd
                                                on sd.dependency_id = am.dependency_id and sd.id = am.sub_dependency_id
                                WHERE
                                (
                                                -- Saldos inciales
                                                (extract(YEAR FROM am.date) = {start.year-1} and am.journal_id = 430) OR
                                                -- Movimientos
                                                (am.date between '{start.year}-01-01' and '{end}')
                                )
                                AND am.parent_state = 'posted' {account_balance_where}
                                ORDER BY acc.code, d.dependency, sd.sub_dependency;"""
                #logging.critical("Query 1: "+str(query_1))
                self.env.cr.execute(query_1)

                unMovCtasDS = self.env.cr.fetchall()
            
                unMovCtasDS = pd.DataFrame(unMovCtasDS, columns=['account_id', 'name', 'code', 'group_id', 'parent_path', 'parent_id', 'code_prefix', 'dependency', 'dependency_id', 'sub_dependency', 'sub_dependency_id', 'description', 'description_subdependency', 'ids'])

                query_2 = f"""SELECT distinct am.account_id,
                                am.dependency_id,
                                am.sub_dependency_id,
                                am.account_id || '-' || coalesce(am.dependency_id,0) || '-' || coalesce(sd.id,0) as ids,
                                sum(am.debit) as debit_si,
                                sum(am.credit) as credit_si,
                                sum(am.balance) as balance_si
                                FROM account_move_line am
                                inner join account_account aa
                                                on aa.id = am.account_id
                                inner join account_group ag
                                                on aa.group_id=ag.id
                                LEFT JOIN dependency d
                                                on d.id = am.dependency_id
                                LEFT JOIN sub_dependency sd
                                                on sd.dependency_id = am.dependency_id and sd.id = am.sub_dependency_id
                                WHERE
                                (
                                                -- Saldos inciales
                                                (extract(YEAR FROM am.date) = {start.year-1} and am.journal_id = 430) OR
                                                -- Movimientos
                                                (am.date between '{start.year}-01-01' and '{start-timedelta(days=1)}')
                                )
                                AND am.parent_state = 'posted' {account_balance_where}
                                group by am.account_id, ids, am.dependency_id, am.sub_dependency_id
                                ORDER BY am.account_id;"""

                #logging.critical("Query 2: "+str(query_2))
                self.env.cr.execute(query_2)

                unSI = self.env.cr.fetchall()

                unSI = pd.DataFrame(unSI, columns=['account_id', 'dependency_id', 'sub_dependency_id', 'ids', 'debit_si', 'credit_si', 'balance_si'])

                query_3 = f"""SELECT distinct am.account_id,
                                am.dependency_id,
                                am.sub_dependency_id,
                                am.account_id || '-' || coalesce(am.dependency_id,0) || '-' || coalesce(sd.id,0) as ids,
                                sum(am.debit) as debit_movs,
                                sum(am.credit) as credit_movs,
                                sum(am.balance) as balance_movs
                                FROM account_move_line am
                                inner join account_account aa
                                                on aa.id = am.account_id
                                inner join account_group ag
                                                on aa.group_id=ag.id
                                LEFT JOIN dependency d
                                                on d.id = am.dependency_id
                                LEFT JOIN sub_dependency sd
                                                on sd.dependency_id = am.dependency_id and sd.id = am.sub_dependency_id
                                WHERE
                                (
                                                -- Movimientos
                                                (am.date between '{start}' and '{end}')
                                )
                                AND am.parent_state = 'posted' {account_balance_where}
                                group by am.account_id, ids, am.dependency_id, am.sub_dependency_id, d.dependency, sd.sub_dependency
                                ORDER BY am.account_id;"""
                
                #logging.critical("Query 3: "+str(query_3))
                self.env.cr.execute(query_3)

                unMov = self.env.cr.fetchall()

                unMov = pd.DataFrame(unMov, columns=['account_id', 'dependency_id', 'sub_dependency_id', 'ids', 'debit_movs', 'credit_movs', 'balance_movs'])

                ######### Cortes de cuentas #############
                ### Query_9 Cortes cta por dependencias

                query_9 = f"""select distinct
                            am.account_id,
                            acc.name, acc.code, acc.group_id,
                            grp.parent_path, grp.parent_id, grp.code_prefix,
                            0 dependency,
                            '' dependency_id,
                            0 as sub_dependency,
                            '' as sub_dependency_id,
                            '' as description,
                            '' as description_subdependency,
                            am.account_id || '-0-0' as ids
                        FROM account_move_line am
                        inner join account_account acc
                           on acc.id = am.account_id
                        inner join account_group grp
                           on grp.id = acc.group_id
                        INNER JOIN dependency d
                           on d.id = am.dependency_id
                        WHERE
                          (
                           -- Saldos inciales
                          (extract(YEAR FROM am.date) = {start.year-1}  and am.journal_id = 430) OR
                           -- Movimientos
                           (am.date between '{start.year}-01-01' and '{end}')
                            )
                          AND am.parent_state = 'posted' {account_balance_where}

                        ORDER BY acc.code;"""

                #logging.critical("Query 9: "+str(query_9))

                self.env.cr.execute(query_9)

                cteCtaD = self.env.cr.fetchall()

                cteCtaD = pd.DataFrame(cteCtaD, columns=['account_id', 'name', 'code', 'group_id', 'parent_path', 'parent_id', 'code_prefix', 'dependency', 'dependency_id', 'sub_dependency', 'sub_dependency_id', 'description', 'description_subdependency', 'ids'])

                query_10 = f"""select distinct
                                am.account_id,
                                '' dependency_id,
                                    '' as sub_dependency_id,
                                am.account_id || '-0-0' as ids,
                                sum(am.debit) as debit_si,
                                sum(am.credit) as credit_si,
                                sum(am.balance) as balance_si
                            FROM account_move_line am
                            inner join account_account acc
                                on acc.id = am.account_id
                            inner join account_group grp
                                on grp.id = acc.group_id
                            INNER JOIN dependency d
                               ON am.dependency_id = d.id
                            WHERE
                              (
                                -- Saldos inciales
                                (extract(YEAR FROM am.date) = {start.year-1} and am.journal_id = 430) OR
                                -- Movimientos
                                (am.date between '{start.year}-01-01' and '{start-timedelta(days=1)}')
                              )
                            AND am.parent_state = 'posted' {account_balance_where}
                            group by am.account_id, ids
                            ORDER BY am.account_id;"""

                #logging.critical("Query 10: "+str(query_10))

                self.env.cr.execute(query_10)

                cteCtaSI = self.env.cr.fetchall()

                cteCtaSI = pd.DataFrame(cteCtaSI, columns=['account_id', 'dependency_id', 'sub_dependency_id', 'ids', 'debit_si','credit_si', 'balance_si'])

                query_11 = f"""select distinct
                                am.account_id,
                                '' as dependency_id,
                                '' as sub_dependency_id,
                                am.account_id || '-0-0' as ids,
                                sum(am.debit) as debit_movs,
                                sum(am.credit) as credit_movs,
                                sum(am.balance) as balance_movs
                            FROM account_move_line am
                            inner join account_account acc
                                on acc.id = am.account_id
                            inner join account_group grp
                                on grp.id = acc.group_id
                            INNER JOIN dependency d
                               ON am.dependency_id = d.id     
                            WHERE
                                (am.date between '{start}' and '{end}')
                            AND am.parent_state = 'posted' {account_balance_where}
                            group by am.account_id,  ids
                            ORDER BY am.account_id"""

                #logging.critical("Query 11: "+str(query_11))

                self.env.cr.execute(query_11)

                cteCtaMov = self.env.cr.fetchall()

                cteCtaMov = pd.DataFrame(cteCtaMov, columns=['account_id', 'dependency_id', 'sub_dependency_id', 'ids', 'debit_movs','credit_movs', 'balance_movs'])

                # Unir las consultas
                uncorteCta = pd.merge(cteCtaD, cteCtaSI, on='ids', how='outer')
                ctesCtasMovSI = pd.merge(uncorteCta, cteCtaMov, on='ids', how='outer')

                # Sustituir valores NaN por 0
                ctesCtasMovSI[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']] = ctesCtasMovSI[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']].fillna(0)
                

                ##################### UNIR LOS MOVIMIENTOS ############################
                # Unir las consultas
                unCtasMovSI = pd.merge(unMovCtasDS, unSI, on='ids', how='outer')
                movCtasSIMov = pd.merge(unCtasMovSI, unMov, on='ids', how='outer')
                movCtasSIMovCtes = pd.merge(unCtasMovSI, unMov, on='ids', how='outer')

                # Agregar los cortes de las cuentas que tiene dependencias y subdependencias
                movCtasSIMovCtes = pd.concat([movCtasSIMovCtes, ctesCtasMovSI])


                # Ordenar la balanza por Code, dependencia y subdependencia.    
                movCtasSIMovCtes = movCtasSIMovCtes.drop(columns=['account_id', 'group_id', 'parent_path', 'parent_id', 'code_prefix', 'dependency_id','sub_dependency_id',  'ids'])
                movCtasSIMovCtes = movCtasSIMovCtes.sort_values(['code','dependency','sub_dependency'])

                # Sustituir valores NaN por 0
                movCtasSIMov[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']] = movCtasSIMov[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']].fillna(0)
                movCtasSIMovCtes[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']] = movCtasSIMovCtes[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']].fillna(0)
                totalSIMov = movCtasSIMov[['debit_si','credit_si','balance_si','debit_movs','credit_movs','balance_movs']].sum()

                movCtasSIMovCtes = movCtasSIMovCtes.sort_values(['code','dependency','sub_dependency'])

                movCtasSIMovCtes['dependency'] = movCtasSIMovCtes['dependency'].fillna(0)
                movCtasSIMovCtes['sub_dependency'] = movCtasSIMovCtes['sub_dependency'].fillna(0)

                movCtasSIMovCtes['description'] = movCtasSIMovCtes['description'].fillna('')
                movCtasSIMovCtes['description_subdependency'] = movCtasSIMovCtes['description_subdependency'].fillna('')

                movCtasSIMovCtes['dependency'] = movCtasSIMovCtes['dependency'].astype(int)
                movCtasSIMovCtes['sub_dependency'] = movCtasSIMovCtes['sub_dependency'].astype(int)

            elif len(dependency_ids)>0 or len(sub_dependency_ids)>0:
                query_1 = f"""select distinct
                                am.account_id,
                                acc.name, acc.code, acc.group_id,
                                grp.parent_path, grp.parent_id, grp.code_prefix,
                                d.dependency,
                                am.dependency_id,
                                sd.sub_dependency,
                                am.sub_dependency_id,
                                d.description,
                                sd.description as description_subdependency,
                                am.account_id || '-' || coalesce(am.dependency_id,0) || '-' || coalesce(sd.id,0) as ids
                                FROM account_move_line am
                                inner join account_account acc
                                                on acc.id = am.account_id
                                inner join account_group grp
                                                on acc.group_id=grp.id
                                INNER JOIN dependency d
                                                on d.id = am.dependency_id
                                INNER JOIN sub_dependency sd
                                                on sd.dependency_id = am.dependency_id and sd.id = am.sub_dependency_id
                                WHERE
                                (
                                                -- Saldos inciales
                                                (extract(YEAR FROM am.date) = {start.year-1} and am.journal_id = 430) OR
                                                -- Movimientos
                                                (am.date between '{start.year}-01-01' and '{end}')
                                )
                                AND am.parent_state = 'posted' {dep_balance_where} {sub_dep_balance_where} {account_balance_where}
                                ORDER BY acc.code, d.dependency, sd.sub_dependency;"""
                #logging.critical("Query 1: "+str(query_1))
                self.env.cr.execute(query_1)

                unMovCtasDS = self.env.cr.fetchall()
            
                unMovCtasDS = pd.DataFrame(unMovCtasDS, columns=['account_id', 'name', 'code', 'group_id', 'parent_path', 'parent_id', 'code_prefix', 'dependency', 'dependency_id', 'sub_dependency', 'sub_dependency_id', 'description', 'description_subdependency', 'ids'])

                query_2 = f"""SELECT distinct am.account_id,
                                am.dependency_id,
                                am.sub_dependency_id,
                                am.account_id || '-' || coalesce(am.dependency_id,0) || '-' || coalesce(sd.id,0) as ids,
                                sum(am.debit) as debit_si,
                                sum(am.credit) as credit_si,
                                sum(am.balance) as balance_si
                                FROM account_move_line am
                                inner join account_account aa
                                                on aa.id = am.account_id
                                inner join account_group ag
                                                on aa.group_id=ag.id
                                INNER JOIN dependency d
                                                on d.id = am.dependency_id
                                INNER JOIN sub_dependency sd
                                                on sd.dependency_id = am.dependency_id and sd.id = am.sub_dependency_id
                                WHERE
                                (
                                                -- Saldos inciales
                                                (extract(YEAR FROM am.date) = {start.year-1} and am.journal_id = 430) OR
                                                -- Movimientos
                                                (am.date between '{start.year}-01-01' and '{start-timedelta(days=1)}')
                                )
                                AND am.parent_state = 'posted' {dep_balance_where} {sub_dep_balance_where} {account_balance_where}
                                group by am.account_id, ids, am.dependency_id, am.sub_dependency_id
                                ORDER BY am.account_id;"""

                #logging.critical("Query 2: "+str(query_2))
                self.env.cr.execute(query_2)

                unSI = self.env.cr.fetchall()

                unSI = pd.DataFrame(unSI, columns=['account_id', 'dependency_id', 'sub_dependency_id', 'ids', 'debit_si', 'credit_si', 'balance_si'])

                query_3 = f"""SELECT distinct am.account_id,
                                am.dependency_id,
                                am.sub_dependency_id,
                                am.account_id || '-' || coalesce(am.dependency_id,0) || '-' || coalesce(sd.id,0) as ids,
                                sum(am.debit) as debit_movs,
                                sum(am.credit) as credit_movs,
                                sum(am.balance) as balance_movs
                                FROM account_move_line am
                                inner join account_account aa
                                                on aa.id = am.account_id
                                inner join account_group ag
                                                on aa.group_id=ag.id
                                INNER JOIN dependency d
                                                on d.id = am.dependency_id
                                INNER JOIN sub_dependency sd
                                                on sd.dependency_id = am.dependency_id and sd.id = am.sub_dependency_id
                                WHERE
                                (
                                                -- Movimientos
                                                (am.date between '{start}' and '{end}')
                                )
                                AND am.parent_state = 'posted' {dep_balance_where} {sub_dep_balance_where} {account_balance_where}
                                group by am.account_id, ids, am.dependency_id, am.sub_dependency_id, d.dependency, sd.sub_dependency
                                ORDER BY am.account_id;"""
                
                #logging.critical("Query 3: "+str(query_3))
                self.env.cr.execute(query_3)

                unMov = self.env.cr.fetchall()

                unMov = pd.DataFrame(unMov, columns=['account_id', 'dependency_id', 'sub_dependency_id', 'ids', 'debit_movs', 'credit_movs', 'balance_movs'])

                ##################### UNIR LOS MOVIMIENTOS ############################
                # Unir las consultas
                unCtasMovSI = pd.merge(unMovCtasDS, unSI, on='ids', how='outer')
                movCtasSIMov = pd.merge(unCtasMovSI, unMov, on='ids', how='outer')
                movCtasSIMovCtes = pd.merge(unCtasMovSI, unMov, on='ids', how='outer')

                # Sustituir valores NaN por 0
                movCtasSIMov[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']] = movCtasSIMov[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']].fillna(0)
                movCtasSIMovCtes[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']] = movCtasSIMovCtes[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']].fillna(0)
                totalSIMov = movCtasSIMov[['debit_si','credit_si','balance_si','debit_movs','credit_movs','balance_movs']].sum()

                movCtasSIMovCtes = movCtasSIMovCtes.sort_values(['code','dependency','sub_dependency'])

                movCtasSIMovCtes['dependency'] = movCtasSIMovCtes['dependency'].fillna(0)
                movCtasSIMovCtes['sub_dependency'] = movCtasSIMovCtes['sub_dependency'].fillna(0)

                movCtasSIMovCtes['description'] = movCtasSIMovCtes['description'].fillna('')
                movCtasSIMovCtes['description_subdependency'] = movCtasSIMovCtes['description_subdependency'].fillna('')

                movCtasSIMovCtes['dependency'] = movCtasSIMovCtes['dependency'].astype(int)
                movCtasSIMovCtes['sub_dependency'] = movCtasSIMovCtes['sub_dependency'].astype(int)

            #SOLO DEPENDENCIAS
            elif len(dependency_ids)>0:
                query_1 = f"""select distinct
                                am.account_id,
                                acc.name, acc.code, acc.group_id,
                                grp.parent_path, grp.parent_id, grp.code_prefix,
                                d.dependency,
                                '0' as sub_dependency,
                                am.dependency_id,
                                d.description,
                                '' as description_subdependency,
                                am.account_id || '-' || coalesce(am.dependency_id,0) || '-0' as ids
                                FROM account_move_line am
                                inner join account_account acc
                                                on acc.id = am.account_id
                                inner join account_group grp
                                                on acc.group_id=grp.id
                                INNER JOIN dependency d
                                                on d.id = am.dependency_id
                                WHERE
                                (
                                                -- Saldos inciales
                                                (extract(YEAR FROM am.date) = {start.year-1} and am.journal_id = 430) OR
                                                -- Movimientos
                                                (am.date between '{start.year}-01-01' and '{end}')
                                )
                                AND am.parent_state = 'posted' {dep_balance_where} {account_balance_where}
                                ORDER BY acc.code, d.dependency;"""
                #logging.critical("Query 1: "+str(query_1))
                self.env.cr.execute(query_1)

                unMovCtasDS = self.env.cr.fetchall()
            
                unMovCtasDS = pd.DataFrame(unMovCtasDS, columns=['account_id', 'name', 'code', 'group_id', 'parent_path', 'parent_id', 'code_prefix', 'dependency', 'sub_dependency', 'dependency_id', 'description', 'description_subdependency', 'ids'])

                query_2 = f"""SELECT distinct am.account_id,
                                am.dependency_id,
                                am.account_id || '-' || coalesce(am.dependency_id,0) || '-0' as ids,
                                sum(am.debit) as debit_si,
                                sum(am.credit) as credit_si,
                                sum(am.balance) as balance_si
                                FROM account_move_line am
                                inner join account_account aa
                                                on aa.id = am.account_id
                                inner join account_group ag
                                                on aa.group_id=ag.id
                                INNER JOIN dependency d
                                                on d.id = am.dependency_id
                                WHERE
                                (
                                                -- Saldos inciales
                                                (extract(YEAR FROM am.date) = {start.year-1} and am.journal_id = 430) OR
                                                -- Movimientos
                                                (am.date between '{start.year}-01-01' and '{start-timedelta(days=1)}')
                                )
                                AND am.parent_state = 'posted' {dep_balance_where} {account_balance_where}
                                group by am.account_id, ids, am.dependency_id
                                ORDER BY am.account_id;"""

                #logging.critical("Query 2: "+str(query_2))
                self.env.cr.execute(query_2)

                unSI = self.env.cr.fetchall()

                unSI = pd.DataFrame(unSI, columns=['account_id', 'dependency_id', 'ids', 'debit_si', 'credit_si', 'balance_si'])

                query_3 = f"""SELECT distinct am.account_id,
                                am.dependency_id,
                                am.account_id || '-' || coalesce(am.dependency_id,0) || '-0' as ids,
                                sum(am.debit) as debit_movs,
                                sum(am.credit) as credit_movs,
                                sum(am.balance) as balance_movs
                                FROM account_move_line am
                                inner join account_account aa
                                                on aa.id = am.account_id
                                inner join account_group ag
                                                on aa.group_id=ag.id
                                INNER JOIN dependency d
                                                on d.id = am.dependency_id
                                WHERE
                                (
                                                -- Movimientos
                                                (am.date between '{start}' and '{end}')
                                )
                                AND am.parent_state = 'posted' {dep_balance_where} {account_balance_where}
                                group by am.account_id, ids, am.dependency_id, d.dependency
                                ORDER BY am.account_id;"""
                
                #logging.critical("Query 3: "+str(query_3))
                self.env.cr.execute(query_3)

                unMov = self.env.cr.fetchall()

                unMov = pd.DataFrame(unMov, columns=['account_id', 'dependency_id', 'ids', 'debit_movs', 'credit_movs', 'balance_movs'])

                ##################### UNIR LOS MOVIMIENTOS ############################
                # Unir las consultas
                unCtasMovSI = pd.merge(unMovCtasDS, unSI, on='ids', how='outer')
                movCtasSIMov = pd.merge(unCtasMovSI, unMov, on='ids', how='outer')
                movCtasSIMovCtes = pd.merge(unCtasMovSI, unMov, on='ids', how='outer')

                # Sustituir valores NaN por 0
                movCtasSIMov[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']] = movCtasSIMov[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']].fillna(0)
                movCtasSIMovCtes[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']] = movCtasSIMovCtes[['debit_si', 'credit_si','balance_si', 'debit_movs', 'credit_movs', 'balance_movs']].fillna(0)
                totalSIMov = movCtasSIMov[['debit_si','credit_si','balance_si','debit_movs','credit_movs','balance_movs']].sum()

                movCtasSIMovCtes = movCtasSIMovCtes.sort_values(['code','dependency','sub_dependency'])

                movCtasSIMovCtes['dependency'] = movCtasSIMovCtes['dependency'].fillna(0)

                movCtasSIMovCtes['description'] = movCtasSIMovCtes['description'].fillna('')

                movCtasSIMovCtes['dependency'] = movCtasSIMovCtes['dependency'].astype(int)

            else:
                return lines
        else:
            return lines    

        total_credit = 0.0
        total_debit = 0.0
        final_total_debit = 0.0 
        final_total_credit = 0.0
        for index, data in movCtasSIMovCtes.iterrows():
            #logging.critical("Dependencia: "+str(data))
            if (str(int(data['dependency'])) == '0' and data['description']=='') or not data['dependency']:
                dependency = '';
            else:
                dependency = str(int(data['dependency']))

            if (str(int(data['sub_dependency'])) == '0' and data['description_subdependency']=='') or not data['sub_dependency']:
                sub_dependency = '';
            else:
                sub_dependency = str(int(data['sub_dependency']))


            cta = str(data['code'])
            description = str(data['name'])
            dep = dependency
            dep_des = data['description'] if data['description'] else ''
            subdep = sub_dependency
            subdep_des = data['description_subdependency'] if data['description_subdependency'] else ''
            izq = 0.0
            der = 0.0
            izq_si = 0.0
            der_si = 0.0

            if cta[0:1] != '6':
                if cta[0:1] == '1' or cta[0:1] == '5' or cta[0:2] == '82':
                    res_si = (data['debit_si']-data['credit_si'])
                    if res_si<0.0:
                        der_si=abs(res_si)
                        izq_si=0.0
                    else:
                        izq_si=res_si
                    

                elif cta[0:1] == '2' or cta[0:1] == '3' or cta[0:1] == '4' or cta[0:2] == '81':
                    res_si = (data['credit_si']-data['debit_si'])
                    if res_si<0.0:
                        izq_si=abs(res_si)
                        der_si=0.0
                    else:
                        der_si=res_si
                        izq_si=0.0

            else:
                izq_si=data['debit_si']
                der_si=data['credit_si']

            if level not in [0,4]:
                if data['balance_si']<0.0 and cta[0:1]!='6':
                    total_credit = total_credit+abs(data['balance_si'])
                elif data['balance_si']>0.0 and cta[0:1]!='6':
                    total_debit = total_debit+data['balance_si']
                elif cta[0:1]=='6':
                    total_debit = total_debit+data['debit_si']
                    total_credit = total_credit+abs(data['credit_si'])
            elif level == 4 and dep=='' and subdep == '':
                if data['balance_si']<0.0 and cta[0:1]!='6':
                    total_credit = total_credit+abs(data['balance_si'])
                elif data['balance_si']>0.0 and cta[0:1]!='6':
                    total_debit = total_debit+data['balance_si']
                elif cta[0:1]=='6':
                    total_debit = total_debit+data['debit_si']
                    total_credit = total_credit+abs(data['credit_si'])
            else:
                if cta=='1' or cta=='5':
                    if data['balance_si']<0.0:
                        total_credit = total_credit+abs(data['balance_si'])
                    else:
                        total_debit = total_debit+data['balance_si']
                elif cta=='2' or cta=='3' or cta=='4':
                    if data['balance_si']<0.0:
                        total_credit = total_credit+abs(data['balance_si'])
                    else:
                        total_debit = total_debit+data['balance_si']
                elif cta=='6':
                    total_debit = total_debit+data['debit_si']
                    total_credit = total_credit+abs(data['credit_si'])



            debit_si = self._format({'name': izq_si}, figure_type='float')
            credit_si = self._format({'name': der_si}, figure_type='float')
            debit_movs = self._format({'name': data['debit_movs']}, figure_type='float')
            credit_movs = self._format({'name': data['credit_movs']}, figure_type='float')

            if cta[0:1] == '1' or cta[0:1] == '5' or cta[0:2] == '82':
                izq = (izq_si+data['debit_movs'])-(der_si+data['credit_movs'])
                if izq<0.0:
                    der=abs(izq)
                    izq=0.0
                else:
                    izq=izq
                    der=0.0
                

            elif cta[0:1] == '2' or cta[0:1] == '3' or cta[0:1] == '4' or cta[0:2] == '81':
                der = (der_si+data['credit_movs'])-(izq_si+data['debit_movs'])
                if der<0.0:
                    izq=abs(der)
                    der=0.0
                else:
                    der=der
                    izq=0.0
                
            elif cta[0:1] == '6':
                der = (data['credit_si']+data['credit_movs'])
                izq = (data['debit_si']+data['debit_movs'])

            if level==0:
                if cta=='1' or cta=='5':
                    final_total_debit = final_total_debit+izq
                    final_total_credit = final_total_credit+der
                elif cta=='2' or cta=='3' or cta=='4':
                    final_total_credit = final_total_credit+der
                    final_total_debit = final_total_debit+izq
                elif cta=='6':
                    final_total_debit = final_total_debit+izq
                    final_total_credit = final_total_credit+der
            elif level == 4 and dep=='' and subdep == '':
                final_total_debit = final_total_debit+izq
                final_total_credit = final_total_credit+der

            elif level not in [0,4]:
                final_total_debit = final_total_debit+izq
                final_total_credit = final_total_credit+der


            izq_debit = self._format({'name': izq}, figure_type='float')
            der_credit = self._format({'name': der}, figure_type='float')



            lines.append({
                'id': 'hierarchy1_Bill',
                'name': cta,
                'columns': [{'name': description},
                            {'name': '00'+dep if len(dependency)==1 else dep},
                            {'name': dep_des},
                            {'name': '0'+subdep if len(sub_dependency)==1 else subdep},
                            {'name': subdep_des},
                            {'name': debit_si['name']},
                            {'name': credit_si['name']},
                            {'name': debit_movs['name']},
                            {'name': credit_movs['name']},
                            {'name': izq_debit['name']},
                            {'name': der_credit['name']},
                            ],
                'level': 1,
                'unfoldable': False,
                'unfolded': True,
            })
        lines.append({
                'id': 'hierarchy_Total',
                'name': '',
                'columns': [{'name': ''},
                            {'name': ''},
                            {'name': ''},
                            {'name': ''},
                            {'name': 'Totales'},
                            {'name': ''},
                            {'name': ''},
                            {'name': ''},
                            {'name': ''},
                            {'name': ''},
                            {'name': ''},
                            ],
                'level': 1,
                'unfoldable': False,
                'unfolded': True,
            })

        debit_si_total = self._format({'name': total_debit}, figure_type='float')
        total_debit_si = total_debit
        credit_si_total = self._format({'name': total_credit}, figure_type='float')
        total_credit_si = total_credit

        '''debit_si_total = self._format({'name': totalSIMov['debit_si']}, figure_type='float')
        total_debit_si = totalSIMov ['debit_si']
        credit_si_total = self._format({'name': totalSIMov['credit_si']}, figure_type='float')
        total_credit_si = totalSIMov ['credit_si']'''
        
        debit_movs_total = self._format({'name': totalSIMov ['debit_movs']}, figure_type='float')
        credit_movs_total = self._format({'name': totalSIMov ['credit_movs']}, figure_type='float')


        izq_2 = (total_debit_si+totalSIMov['debit_movs'])
        
        der_2 = (total_credit_si+totalSIMov['credit_movs'])


        izq_debit_total = self._format({'name': final_total_debit}, figure_type='float')
        der_credit_total = self._format({'name': final_total_credit}, figure_type='float')


        lines.append({
            'id': 'hierarchy_Totales',
            'name': '',
            'columns': [{'name': ''},
                        {'name': ''},
                        {'name': ''},
                        {'name': ''},
                        {'name': ''},
                        {'name': debit_si_total['name']},
                        {'name': credit_si_total['name']},
                        {'name': debit_movs_total['name']},
                        {'name': credit_movs_total['name']},
                        {'name': izq_debit_total['name']},
                        {'name': der_credit_total['name']},
                        ],
            'level': 1,
            'unfoldable': False,
            'unfolded': True,
        })


        return lines

    def _get_report_name(self):
        return _("Balanza de comprobación")
    
    @api.model
    def _get_super_columns(self, options):
        date_cols = options.get('date') and [options['date']] or []
        date_cols += (options.get('comparison') or {}).get('periods', [])
        columns = reversed(date_cols)
        return {'columns': columns, 'x_offset': 1, 'merge': 5}
    
    def get_month_name(self,month):
        month_name = ''
        if month==1:
            month_name = 'Enero'
        elif month==2:
            month_name = 'Febrero'
        elif month==3:
            month_name = 'Marzo'
        elif month==4:
            month_name = 'Abril'
        elif month==5:
            month_name = 'Mayo'
        elif month==6:
            month_name = 'Junio'
        elif month==7:
            month_name = 'Julio'
        elif month==8:
            month_name = 'Agosto'
        elif month==9:
            month_name = 'Septiembre'
        elif month==10:
            month_name = 'Octubre'
        elif month==11:
            month_name = 'Noviembre'
        elif month==12:
            month_name = 'Diciembre'
            
        return month_name.upper()
    
    def get_header_year_list(self,options):
        start = datetime.strptime(
            str(options['date'].get('date_from')), '%Y-%m-%d').date()
        end = datetime.strptime(
            options['date'].get('date_to'), '%Y-%m-%d').date()
        
        str1 = ''
        if start.year == end.year:
            if start.month != end.month:
                str1 = self.get_month_name(start.month) + "-" +self.get_month_name(end.month) + " " + str(start.year)
            else:   
                str1 = self.get_month_name(start.month) + " " + str(start.year)
        else:
            str1 = self.get_month_name(start.month)+" "+ str(start.year) + "-" +self.get_month_name(end.month) + " " + str(end.year)
        return str1

    def is_float(self, num):
        try:
            float(num)
            return True
        except ValueError:
            return False


    def get_xlsx(self, options, response=None):
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        sheet = workbook.add_worksheet(self._get_report_name()[:31])
 
        date_default_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2, 'num_format': 'yyyy-mm-dd'})
        date_default_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': 'yyyy-mm-dd'})
        default_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2})
        default_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'})
        title_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'bottom': 2})
        super_col_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'align': 'center'})
        level_0_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 13, 'bottom': 6, 'font_color': '#666666'})
        level_1_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 11, 'bottom': 1, 'font_color': '#666666'})
        level_2_col1_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 1})
        level_2_col1_total_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666'})
        level_2_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666'})
        level_3_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2})
        level_3_col1_total_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 1})
        level_3_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'})
        currect_date_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'align': 'right'})
        currency_format = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 11, 'bottom': 1, 'font_color': '#666666', 'num_format': '$#,##0.00'})
        
        #Set the first column width to 50
        sheet.set_column(0, 5,21)
        sheet.set_column(6, 11,21, currency_format)
#         sheet.set_column(0, 1,20)
#         sheet.set_column(0, 2,20)
#         sheet.set_column(0, 3,12)
#         sheet.set_column(0, 4,20)
        #sheet.set_row(0, 0,50)
        #sheet.col(0).width = 90 * 20
        super_columns = self._get_super_columns(options)
        #y_offset = bool(super_columns.get('columns')) and 1 or 0
        #sheet.write(y_offset, 0,'', title_style)
        y_offset = 0
        col = 0
        start = datetime.strptime(str(options['date'].get('date_from')), '%Y-%m-%d').date()
        end = datetime.strptime(options['date'].get('date_to'), '%Y-%m-%d').date()
        start_month_name = start.strftime("%B")
        end_month_name = end.strftime("%B")
        
        if self.env.user.lang == 'es_MX':
            start_month_name = self.get_month_name(start.month)
            end_month_name = self.get_month_name(end.month)

        header_date = str(start.day).zfill(2) + " " + start_month_name+" DE "+str(start.year)
        header_date += " AL "+str(end.day).zfill(2) + " " + end_month_name +" DE "+str(end.year)
        
#         sheet.merge_range(y_offset, col, 6, col, '')
        if self.env.user and self.env.user.company_id and self.env.user.company_id.header_logo:
            filename = 'logo.png'
            image_data = io.BytesIO(base64.standard_b64decode(self.env.user.company_id.header_logo))
            sheet.insert_image(0,0, filename, {'image_data': image_data,'x_offset':8,'y_offset':3,'x_scale':0.68,'y_scale':0.68})
        
        #col += 1
        #header_title = '''BALANZA DE COMPROBACIÓN\n%s'''%(self.get_header_year_list(options))
        header_title = '''UNIVERSIDAD NACIONAL AUTÓNOMA DE MÉXICO\nDIRECCIÓN GENERAL DE CONTROL PRESUPUESTAL-CONTADURÍA 
        GENERAL \nBALANZA DE COMPROBACIÓN DEL %s \nCifras en Pesos''' % (header_date)
        sheet.merge_range(y_offset, col, 5, col+11, header_title,super_col_style)
        y_offset += 6
        sheet.merge_range(y_offset, 6, y_offset, 7, "SALDO INICIAL",super_col_style)
        sheet.merge_range(y_offset, 8, y_offset, 9, "MOVIMIENTOS "+(self.get_header_year_list(options)),super_col_style)
        sheet.merge_range(y_offset, 10, y_offset, 11, "SALDO FINAL",super_col_style)
        y_offset += 1
#         col=1
#         currect_time_msg = "Fecha y hora de impresión: "
#         currect_time_msg += datetime.today().strftime('%d/%m/%Y %H:%M')
#         sheet.merge_range(y_offset, col, y_offset, col+17, currect_time_msg,currect_date_style)
#         y_offset += 1
        
        # Todo in master: Try to put this logic elsewhere
#         x = super_columns.get('x_offset', 0)
#         for super_col in super_columns.get('columns', []):
#             cell_content = super_col.get('string', '').replace('<br/>', ' ').replace('&nbsp;', ' ')
#             x_merge = super_columns.get('merge')
#             if x_merge and x_merge > 1:
#                 sheet.merge_range(0, x, 0, x + (x_merge - 1), cell_content, super_col_style)
#                 x += x_merge
#             else:
#                 sheet.write(0, x, cell_content, super_col_style)
#                 x += 1
        for row in self.get_header(options):
            x = 0
            for column in row:
                colspan = column.get('colspan', 1)
                header_label = column.get('name', '').replace('<br/>', ' ').replace('&nbsp;', ' ')
                if colspan == 1:
                    sheet.write(y_offset, x, header_label, title_style)
                else:
                    sheet.merge_range(y_offset, x, y_offset, x + colspan - 1, header_label, title_style)
                x += colspan
            y_offset += 1
        ctx = self._set_context(options)
        ctx.update({'no_format':True, 'print_mode':True, 'prefetch_fields': False})
        # deactivating the prefetching saves ~35% on get_lines running time
        lines = self.with_context(ctx)._get_lines(options)
 
        if options.get('hierarchy'):
            lines = self._create_hierarchy(lines, options)
        if options.get('selected_column'):
            lines = self._sort_lines(lines, options)
 
        #write all data rows
        for y in range(0, len(lines)):
            level = lines[y].get('level')
            if lines[y].get('caret_options'):
                style = level_3_style
                col1_style = level_3_col1_style
            elif level == 0:
                y_offset += 1
                style = level_0_style
                col1_style = style
            elif level == 1:
                style = level_1_style
                col1_style = style
            elif level == 2:
                style = level_2_style
                col1_style = 'total' in lines[y].get('class', '').split(' ') and level_2_col1_total_style or level_2_col1_style
            elif level == 3:
                style = level_3_style
                col1_style = 'total' in lines[y].get('class', '').split(' ') and level_3_col1_total_style or level_3_col1_style
            else:
                style = default_style
                col1_style = default_col1_style
 
            #write the first column, with a specific style to manage the indentation
            cell_type, cell_value = self._get_cell_type_value(lines[y])
            #logging.critical("Tipo de celda: "+str(style))
            if cell_type == 'date':
                sheet.write_datetime(y + y_offset, 0, cell_value, date_default_col1_style)
            else:
                sheet.write(y + y_offset, 0, cell_value, col1_style)
 
            #write all the remaining cells
            for x in range(1, len(lines[y]['columns']) + 1):
                cell_type, cell_value = self._get_cell_type_value(lines[y]['columns'][x - 1])
                if cell_type == 'date':
                    sheet.write_datetime(y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, date_default_style)
                elif cell_type=='text' and self.is_float(cell_value) and x>5:
                    sheet.write(y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, currency_format)
                else:
                    sheet.write(y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, style)
 
        workbook.close()
        output.seek(0)
        generated_file = output.read()
        output.close()
        return generated_file    
        
    
    def get_pdf(self, options, minimal_layout=True):
        # As the assets are generated during the same transaction as the rendering of the
        # templates calling them, there is a scenario where the assets are unreachable: when
        # you make a request to read the assets while the transaction creating them is not done.
        # Indeed, when you make an asset request, the controller has to read the `ir.attachment`
        # table.
        # This scenario happens when you want to print a PDF report for the first time, as the
        # assets are not in cache and must be generated. To workaround this issue, we manually
        # commit the writes in the `ir.attachment` table. It is done thanks to a key in the context.
        minimal_layout = False
        if not config['test_enable']:
            self = self.with_context(commit_assetsbundle=True)

        base_url = self.env['ir.config_parameter'].sudo().get_param('report.url') or self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        rcontext = {
            'mode': 'print',
            'base_url': base_url,
            'company': self.env.company,
        }

        body = self.env['ir.ui.view'].render_template(
            "account_reports.print_template",
            values=dict(rcontext),
        )
        body_html = self.with_context(print_mode=True).get_html(options)
        body = body.replace(b'<body class="o_account_reports_body_print">', b'<body class="o_account_reports_body_print">' + body_html)
        
        if minimal_layout:
            header = ''
            footer = self.env['ir.actions.report'].render_template("web.internal_layout", values=rcontext)
            spec_paperformat_args = {'data-report-margin-top': 10, 'data-report-header-spacing': 10}
            footer = self.env['ir.actions.report'].render_template("web.minimal_layout", values=dict(rcontext, subst=True, body=footer))
        else:
            rcontext.update({
                    'css': '',
                    'o': self.env.user,
                    'res_company': self.env.company,
                })
#             header = self.env['ir.actions.report'].render_template("jt_income.pdf_external_layout_income_annual_report_new", values=rcontext)
#             
#             header = header.decode('utf-8') # Ensure that headers and footer are correctly encoded
#             current_filtered = self.get_header_year_list(options)
#             header.replace("REAL", "Test")
            header='''<div class="header">
                        <div class="row">
                            <div class="col-12 text-center">
                                   <span style="font-size:16px;">DIRECCIÓN GENERAL DE FINANZAS</span><br/>
                                   <span style="font-size:14px;">DIRECCIÓN DE INGRESOS Y OPERACIÓN FINANCIERA</span><br/>
                                   <span style="font-size:12px;">DEPARTAMENTO DE INGRESOS</span><br/>
                                   <span style="font-size:12px;">INFORME ANUAL INGRESOS</span><br/>
                                   <span style="font-size:12px;">INGRESOS POR SERVICIOS DE EDUCACIÓN Y PATRIMONIALES</span><br/>
                                   <span style="font-size:12px;">%s</span><br/>
                                   <span style="font-size:12px;">REAL</span><br/>
                            </div>
                        </div>
                    </div>
                    <div class="article" data-oe-model="res.users" data-oe-id="2" data-oe-lang="en_US">
                      
                    </div>
                '''%(self.get_header_year_list(options))
            spec_paperformat_args = {}
            # Default header and footer in case the user customized web.external_layout and removed the header/footer
            headers = header.encode()
            footer = b''
            # parse header as new header contains header, body and footer
            try:
                root = lxml.html.fromstring(header)
                match_klass = "//div[contains(concat(' ', normalize-space(@class), ' '), ' {} ')]"

                for node in root.xpath(match_klass.format('header')):
                    headers = lxml.html.tostring(node)
                    headers = self.env['ir.actions.report'].render_template("web.minimal_layout", values=dict(rcontext, subst=True, body=headers))

                for node in root.xpath(match_klass.format('footer')):
                    footer = lxml.html.tostring(node)
                    footer = self.env['ir.actions.report'].render_template("web.minimal_layout", values=dict(rcontext, subst=True, body=footer))

            except lxml.etree.XMLSyntaxError:
                headers = header.encode()
                footer = b''
            header = headers

        landscape = False
        if len(self.with_context(print_mode=True).get_header(options)[-1]) > 5:
            landscape = True

        return self.env['ir.actions.report']._run_wkhtmltopdf(
            [body],
            header=header, footer=footer,
            landscape=landscape,
            specific_paperformat_args=spec_paperformat_args
        )

    def get_html(self, options, line_id=None, additional_context=None):
        '''
        return the html value of report, or html value of unfolded line
        * if line_id is set, the template used will be the line_template
        otherwise it uses the main_template. Reason is for efficiency, when unfolding a line in the report
        we don't want to reload all lines, just get the one we unfolded.
        '''
        # Check the security before updating the context to make sure the options are safe.
        self._check_report_security(options)

        # Prevent inconsistency between options and context.
        self = self.with_context(self._set_context(options))

        templates = self._get_templates()
        report_manager = self._get_report_manager(options)
        report = {'name': self._get_report_name(),
                'summary': report_manager.summary,
                'company_name': self.env.company.name,}
        report = {}
        #options.get('date',{}).update({'string':''}) 
        lines = self._get_lines(options, line_id=line_id)

        if options.get('hierarchy'):
            lines = self._create_hierarchy(lines, options)
        if options.get('selected_column'):
            lines = self._sort_lines(lines, options)

        footnotes_to_render = []
        if self.env.context.get('print_mode', False):
            # we are in print mode, so compute footnote number and include them in lines values, otherwise, let the js compute the number correctly as
            # we don't know all the visible lines.
            footnotes = dict([(str(f.line), f) for f in report_manager.footnotes_ids])
            number = 0
            for line in lines:
                f = footnotes.get(str(line.get('id')))
                if f:
                    number += 1
                    line['footnote'] = str(number)
                    footnotes_to_render.append({'id': f.id, 'number': number, 'text': f.text})

        rcontext = {'report': report,
                    'lines': {'columns_header': self.get_header(options), 'lines': lines},
                    'options': {},
                    'context': self.env.context,
                    'model': self,
                }
        if additional_context and type(additional_context) == dict:
            rcontext.update(additional_context)
        if self.env.context.get('analytic_account_ids'):
            rcontext['options']['analytic_account_ids'] = [
                {'id': acc.id, 'name': acc.name} for acc in self.env.context['analytic_account_ids']
            ]

        render_template = templates.get('main_template', 'account_reports.main_template')
        if line_id is not None:
            render_template = templates.get('line_template', 'account_reports.line_template')
        html = self.env['ir.ui.view'].render_template(
            render_template,
            values=dict(rcontext),
        )
        if self.env.context.get('print_mode', False):
            for k,v in self._replace_class().items():
                html = html.replace(k, v)
            # append footnote as well
            html = html.replace(b'<div class="js_account_report_footnotes"></div>', self.get_html_footnotes(footnotes_to_render))
        return html




