# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2023 UNAM.
#    Author: SIIF UNAM
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


import io
import base64
import json
from datetime import datetime
import pytz
from psycopg2 import sql
import logging

from odoo import models, api, _
from odoo.tools.misc import xlsxwriter

DATE_FORMAT = '%Y-%m-%d'
CURRENCY_FORMAT = '#,##0'
TEXT_HEADER_FORMAT_RIGHT = 'text-right text-dark'
TEXT_HEADER_FORMAT_LEFT = 'text-left text-dark'
class IncomeExpensesInvestmentStatementReportToExecute(models.AbstractModel):

    _name = "income.expenses.and.investment.statement.report.to.execute"
    _inherit = "account.report"
    _description = "Income, Expenses and Investment Statement Report to Execute"

    filter_date = {'mode': 'single', 'filter': 'this_year'}
    filter_all_entries = None
    filter_journals = None
    filter_unfold_all = None
    filter_cash_basis = None
    filter_hierarchy = None
    MAX_LINES = None
    filter_levels = None

    def _get_reports_buttons(self):
        return [
            {'name': _('Export (XLSX)'),
             'sequence': 1,
             'action': 'print_xlsx',
             'file_export_type': _('XLSX')
             },
        ]

    def print_xlsx(self, options, extra_data):
        options['is_xlsx_report'] = True
        return {
                'type': 'ir_actions_account_report_download',
                'data': {'model': self.env.context.get('model'),
                         'options': json.dumps(options),
                         'output_format': 'xlsx',
                         'financial_id': self.env.context.get('id'),
                         }
                }

    @api.model
    def _get_columns_name(self, options):
        columns = [
            {'name': _('ANNEXES'), 'class': TEXT_HEADER_FORMAT_LEFT},
            {'name': _('CONCEPT'), 'class': TEXT_HEADER_FORMAT_LEFT},
            {'name': _('AUTHORIZED'), 'class': TEXT_HEADER_FORMAT_RIGHT},
            {'name': _('TOTAL of ')+options['date'].get('date_to'), 'class': TEXT_HEADER_FORMAT_RIGHT},
            {'name': _('%'), 'class': TEXT_HEADER_FORMAT_RIGHT},
            {'name': _('TO EXECUTE'), 'class': TEXT_HEADER_FORMAT_RIGHT},    
        ]
        return columns

    @api.model
    def _get_lines(self, options, line_id=None):
        # Groups that have access to the report
        list_groups = [
            'jt_account_base.group_account_admin_user',
            'jt_account_base.group_account_super_admin_user',
            'jt_account_base.group_account_guest_user',
            'jt_account_base.group_account_approval_user']
        
        self.env['security.tools'].check_user_permissions(list_groups)         

        lines = []

        category_name = 'A'
        annexe_name = 'INGRESOS'
        level = 0
        first_column_name = category_name
        lines.append(self.get_category_dict_column(category_name, annexe_name, level, False, first_column_name))
        
        ## Subsidio Federal
        
        category_name = 'B'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_federal_subsidy')
        parent_category_name = 'A'
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))

        subsidy_total_amount_exercised, subsidy_total_amount_authorized  = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        subsidy_total_percent = self.get_percent_exercised(subsidy_total_amount_authorized, subsidy_total_amount_exercised)
        subsidy_to_execute = subsidy_total_amount_authorized - subsidy_total_amount_exercised
        
        ## Total 
        concept_name = 'SUBSIDIO'
        parent_category_name = 'A'
        level = 1
        second_column_name = concept_name
        text_align = 'text-right'
        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, subsidy_total_amount_authorized, subsidy_total_amount_exercised,  subsidy_total_percent, subsidy_to_execute, level, second_column_name, text_align))

        ## Productos Financieros 
        category_name = 'C'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_financial_products')
        parent_category_name = 'A'
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        financial_products_total_amount_exercised, financial_products_total_amount_authorized = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        financial_products_total_percent = self.get_percent_exercised(financial_products_total_amount_authorized, financial_products_total_amount_exercised)
        financial_products_to_execute = financial_products_total_amount_authorized - financial_products_total_amount_exercised

        ## Ingresos Extroardinarios  
        category_name = 'D'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_extraordinary_income')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        extraordinary_income_total_amount_exercised, extraordinary_income_total_amount_authorized = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        extraordinary_income_total_percent = self.get_percent_exercised(extraordinary_income_total_amount_authorized, extraordinary_income_total_amount_exercised)
        extraordinary_income_to_execute = extraordinary_income_total_amount_authorized - extraordinary_income_total_amount_exercised 

        ## Ingresos por servicios de educación  
        category_name = 'E'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_education_services')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        educational_income_total_amount_exercised, educational_income_total_amount_authorized  = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        educational_income_total_percent =  self.get_percent_exercised(educational_income_total_amount_authorized, educational_income_total_amount_exercised)
        educational_income_to_execute =  educational_income_total_amount_authorized -  educational_income_total_amount_exercised

        ## Otros ingresos
        category_name = 'F'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_other_income')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        other_income_total_amount_exercised, other_income_total_amount_authorized = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        other_income_total_percent = self.get_percent_exercised(other_income_total_amount_authorized, other_income_total_amount_exercised)
        other_income_to_execute = other_income_total_amount_authorized - other_income_total_amount_exercised

        ## Total INGRESOS PROPIOS 
        concept_name = 'INGRESOS PROPIOS'
        parent_category_name = 'A'
        own_income_total_amount_exercised =  financial_products_total_amount_exercised + extraordinary_income_total_amount_exercised + educational_income_total_amount_exercised + other_income_total_amount_exercised
        own_income_total_amount_authorized = financial_products_total_amount_authorized + extraordinary_income_total_amount_authorized + educational_income_total_amount_authorized + other_income_total_amount_authorized
    
        level = 1
        second_column_name = concept_name
        own_income_total_percent = self.get_percent_exercised(own_income_total_amount_authorized, own_income_total_amount_exercised)
        own_income_to_execute = own_income_total_amount_authorized - own_income_total_amount_exercised
        text_align = 'text-right'
        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, own_income_total_amount_authorized, own_income_total_amount_exercised,  own_income_total_percent, own_income_to_execute, level, second_column_name, text_align))
        
        ## TOTAL INGRESOS
        concept_name = 'TOTAL INGRESOS'
        income_total_amount_exercised = subsidy_total_amount_exercised + financial_products_total_amount_exercised + extraordinary_income_total_amount_exercised + educational_income_total_amount_exercised + other_income_total_amount_exercised
        income_total_amount_authorized = subsidy_total_amount_authorized + financial_products_total_amount_authorized + extraordinary_income_total_amount_authorized + educational_income_total_amount_authorized + other_income_total_amount_authorized
        income_total_percent = self.get_percent_exercised(income_total_amount_authorized, income_total_amount_exercised)
        income_to_execute = income_total_amount_authorized - income_total_amount_exercised
        level = 1
        second_column_name = concept_name
        text_align = 'text-right'
        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, income_total_amount_authorized, income_total_amount_exercised,  income_total_percent, income_to_execute, level, second_column_name, text_align))
        
        category_name = 'gastos_operacion'
        annexe_name = 'Gastos de operación'

        level = 0
        lines.append(self.get_category_dict_column(category_name, annexe_name, level))
        
        category_name = 'remuneraciones_personales'
        annexe_name = 'Remuneraciones Personales y Prestaciones'
        parent_category_name = 'gastos_operacion'
        level = 1
        lines.append(self.get_category_dict_column(category_name, annexe_name, level, parent_category_name))

        ## Remuneraciones al personal académico
        category_name = 'G'
        parent_category_name = 'remuneraciones_personales'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_remuneration_academic_personal')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        remuneration_academic_personal_total_amount_exercised, remuneration_academic_personal_total_amount_authorized = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        remuneration_academic_personal_total_percent = self.get_percent_exercised(remuneration_academic_personal_total_amount_authorized, remuneration_academic_personal_total_amount_exercised)
        remuneration_academic_personal_to_execute = remuneration_academic_personal_total_amount_authorized - remuneration_academic_personal_total_amount_exercised

        ## Remuneraciones al personal administrativo
        category_name = 'H'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_remuneration_academic_administrative')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        remuneration_academic_administrative_total_amount_exercised, remuneration_academic_administrative_total_amount_authorized = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        remuneration_academic_administrative_total_percent = self.get_percent_exercised(remuneration_academic_administrative_total_amount_authorized, remuneration_academic_administrative_total_amount_exercised)
        remuneration_academic_administrative_to_execute = remuneration_academic_administrative_total_amount_authorized - remuneration_academic_administrative_total_amount_exercised

        ## Aguinaldo y prima vacacional
        category_name = 'I'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_bonus_vacation')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        bonus_vacation_total_amount_exercised, bonus_vacation_total_amount_authorized = self._add_accounts_to_lines(lines, options, annexe_name, category_name) 
        bonus_vacation_total_percent = self.get_percent_exercised(bonus_vacation_total_amount_authorized, bonus_vacation_total_amount_exercised)
        bonus_vacation_to_execute = bonus_vacation_total_amount_authorized - bonus_vacation_total_amount_exercised

        ## Gratificaciones por separación y jubilación e indemnizaciones por defunción
        category_name = 'J'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_gratification_separation_retirement')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        gratification_separation_retirement_total_amount_exercised, gratification_separation_retirement_total_amount_authorized= self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        gratification_separation_retirement_total_percent = self.get_percent_exercised(gratification_separation_retirement_total_amount_authorized, gratification_separation_retirement_total_amount_exercised)
        gratification_separation_retirement_to_execute = gratification_separation_retirement_total_amount_authorized - gratification_separation_retirement_total_amount_exercised

        ## Cuotas de seguridad social
        category_name = 'K'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_social_security_fees')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        social_security_fees_total_amount_exercised, social_security_fees_total_amount_authorized = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        social_security_fees_total_percent = self.get_percent_exercised(social_security_fees_total_amount_authorized, social_security_fees_total_amount_exercised)
        social_security_fees_to_execute = social_security_fees_total_amount_authorized - social_security_fees_total_amount_exercised

        ## Otras prestaciones sociales
        category_name = 'L'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_other_social_benefits')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        other_social_benefits_total_amount_exercised, other_social_benefits_total_amount_authorized = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        other_social_benefits_total_percent = self.get_percent_exercised(other_social_benefits_total_amount_authorized, other_social_benefits_total_amount_exercised)
        other_social_benefits_to_execute = other_social_benefits_total_amount_authorized - other_social_benefits_total_amount_exercised


        ## TOTAL 
        concept_name = 'remunerations_total'
        remunerations_total_amount_exercised = remuneration_academic_personal_total_amount_exercised + remuneration_academic_administrative_total_amount_exercised + bonus_vacation_total_amount_exercised + gratification_separation_retirement_total_amount_exercised + social_security_fees_total_amount_exercised + other_social_benefits_total_amount_exercised
        remunerations_total_amount_authorized = remuneration_academic_personal_total_amount_authorized + remuneration_academic_administrative_total_amount_authorized + bonus_vacation_total_amount_authorized + gratification_separation_retirement_total_amount_authorized + social_security_fees_total_amount_authorized + other_social_benefits_total_amount_authorized
        level = 1
        remunerations_total_percent = self.get_percent_exercised(remunerations_total_amount_authorized, remunerations_total_amount_exercised)
        remunerations_to_execute = remunerations_total_amount_authorized - remunerations_total_amount_exercised
        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, remunerations_total_amount_authorized, remunerations_total_amount_exercised,  remunerations_total_percent, remunerations_to_execute, level))
        

        ## Becas
        category_name = 'M'
        parent_category_name = 'gastos_operacion'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_scolarship')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        scolarship_total_amount_exercised, scolarship_total_amount_authorized = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        scolarship_total_percent = self.get_percent_exercised(scolarship_total_amount_authorized, scolarship_total_amount_exercised)
        scolarship_to_execute = scolarship_total_amount_authorized - scolarship_total_amount_exercised

        ## Erogaciones para sedes en el extranjero
        category_name = 'N'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_foreign_expenses')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        foreign_expenses_total_amount_exercised, foreign_expenses_total_amount_authorized = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        foreign_expenses_total_percent = self.get_percent_exercised(foreign_expenses_total_amount_authorized, foreign_expenses_total_amount_exercised)
        foreign_expenses_to_execute = foreign_expenses_total_amount_authorized - foreign_expenses_total_amount_exercised

        category_name = 'services_articles_materials'
        annexe_name = 'Servicios, Artículos y Materiales de Consumo'
        level = 0
        lines.append(self.get_category_dict_column(category_name, annexe_name, level, parent_category_name))
        
        ## Materiales, refacciones, herramientas y accesorios didácticos y de investigación
        category_name = 'O'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_materials')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        materials_total_amount_exercised, materials_total_amount_authorized = self._add_accounts_to_lines(lines, options, annexe_name, category_name) 
        materials_total_percent = self.get_percent_exercised(materials_total_amount_authorized, materials_total_amount_exercised)
        materials_to_execute = materials_total_amount_authorized - materials_total_amount_exercised
        
        ## Servicios y materiales de mantenimiento para edificios, equipos e instalaciones
        category_name = 'P'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_maintenance')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        maintenance_total_amount_exercised, maintenance_total_amount_authorized  = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        maintenance_total_percent = self.get_percent_exercised(maintenance_total_amount_authorized, maintenance_total_amount_exercised)
        maintenance_to_execute = maintenance_total_amount_authorized - maintenance_total_amount_exercised

        ## Energía eléctrica y servicios de comunicación
        category_name = 'Q'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_electricity')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        electricity_total_amount_exercised, electricity_total_amount_authorized = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        electricity_total_percent = self.get_percent_exercised(electricity_total_amount_authorized, electricity_total_amount_exercised)
        electricity_to_execute = electricity_total_amount_authorized - electricity_total_amount_exercised

        ## Trabajos de campo, prácticas escolares, paisajes y viáticos
        category_name = 'R'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_field_work')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        field_work_total_amount_exercised, field_work_total_amount_authorized = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        field_work_total_percent = self.get_percent_exercised(field_work_total_amount_authorized, field_work_total_amount_exercised)
        field_work_to_execute = field_work_total_amount_authorized - field_work_total_amount_exercised

        ## Arrendamiento de muebles y equipo
        category_name = 'S'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_rent')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        services_articles_total_amount_exercised, services_articles_total_amount_authorized = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        services_articles_total_percent = self.get_percent_exercised(services_articles_total_amount_authorized, services_articles_total_amount_exercised)
        services_articles_to_execute = services_articles_total_amount_authorized - services_articles_total_amount_exercised

        ## Ediciones de libros y revistas
        category_name = 'T'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_editions')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        editions_total_amount_exercised, editions_total_amount_authorized = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        editions_total_percent = self.get_percent_exercised(editions_total_amount_authorized, editions_total_amount_exercised)
        editions_to_execute = editions_total_amount_authorized - editions_total_amount_exercised


        ## Cuotas de afiliación a sociedades e instituciones científicas, culturales y deportivas; simposios, congresos y seminarios
        category_name = 'U'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_membership_fees')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        membership_fees_total_amount_exercised, membership_fees_total_amount_authorized  = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        membership_fees_total_percent = self.get_percent_exercised(membership_fees_total_amount_authorized, membership_fees_total_amount_exercised)
        membership_fees_to_execute = membership_fees_total_amount_authorized - membership_fees_total_amount_exercised

        ## Gastos y derechos de importación, seguros y fianzas
        category_name = 'V'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_importation_expenses')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        importation_expenses_total_amount_exercised, importation_expenses_total_amount_authorized = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        importation_expenses_total_percent = self.get_percent_exercised(importation_expenses_total_amount_authorized, importation_expenses_total_amount_exercised)
        importation_expenses_to_execute = importation_expenses_total_amount_authorized - importation_expenses_total_amount_exercised

        ## Servicios por asesorías para operación de programas
        category_name = 'W'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_advisory_services')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        advisory_services_total_amount_exercised, advisory_services_total_amount_authorized = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        advisory_services_total_percent = self.get_percent_exercised(advisory_services_total_amount_authorized, advisory_services_total_amount_exercised)
        advisory_services_to_execute = advisory_services_total_amount_authorized - advisory_services_total_amount_exercised

        ## Apoyo a programas de extensión y colaboración académica y científica
        category_name = 'X'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_support_programs')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        support_programs_total_amount_exercised, support_programs_total_amount_authorized = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        support_programs_total_percent = self.get_percent_exercised(support_programs_total_amount_authorized, support_programs_total_amount_exercised)
        support_programs_to_execute = support_programs_total_amount_authorized - support_programs_total_amount_exercised

        ## TOTAL 
        concept_name = 'services_total'
        total_services_amount_exercised = materials_total_amount_exercised + maintenance_total_amount_exercised + electricity_total_amount_exercised + field_work_total_amount_exercised + services_articles_total_amount_exercised + editions_total_amount_exercised + membership_fees_total_amount_exercised + importation_expenses_total_amount_exercised + advisory_services_total_amount_exercised + support_programs_total_amount_exercised
        total_services_amount_authorized = materials_total_amount_authorized + maintenance_total_amount_authorized + electricity_total_amount_authorized + field_work_total_amount_authorized  + services_articles_total_amount_authorized + editions_total_amount_authorized + membership_fees_total_amount_authorized + importation_expenses_total_amount_authorized + advisory_services_total_amount_authorized + support_programs_total_amount_authorized
        total_services_total_percent = self.get_percent_exercised(total_services_amount_authorized, total_services_amount_exercised)
        total_services_to_execute = total_services_amount_authorized - total_services_amount_exercised

        level = 1
        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, total_services_amount_authorized, total_services_amount_exercised, total_services_total_percent, total_services_to_execute, level))
        
        ## TOTAL OPERACION
        concept_name = 'operating_costs'
        operating_costs_total_amount_exercised = remunerations_total_amount_exercised + scolarship_total_amount_exercised + foreign_expenses_total_amount_exercised + total_services_amount_exercised
        operating_costs_total_amount_authorized = remunerations_total_amount_authorized + scolarship_total_amount_authorized + foreign_expenses_total_amount_authorized + total_services_amount_authorized
        operating_costs_total_percent = self.get_percent_exercised(operating_costs_total_amount_authorized, operating_costs_total_amount_exercised)
        operating_costs_to_execute = operating_costs_total_amount_authorized - operating_costs_total_amount_exercised

        level = 1
        second_column_name = 'GASTOS DE OPERACIÓN'
        text_align = 'text-right'
        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, operating_costs_total_amount_authorized, operating_costs_total_amount_exercised, operating_costs_total_percent, operating_costs_to_execute, level, second_column_name, text_align))
        
        category_name = 'properties_equipment'
        annexe_name = 'Inversiones en Propiedades y Equipo'
        level = 0
        lines.append(self.get_category_dict_column(category_name, annexe_name, level))
        
        ## Equipo, mobiliario e instrumentos científicos y didácticos
        category_name = 'Y'
        parent_category_name = 'properties_equipment'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_equipment')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        equipment_total_amount_exercised, equipment_total_amount_authorized = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        equipment_total_percent = self.get_percent_exercised(equipment_total_amount_authorized, equipment_total_amount_exercised)
        equipment_to_execute = equipment_total_amount_authorized - equipment_total_amount_exercised

        # Construcciones, rehabilitaciones y remodelación de inmuebles
        category_name = 'Z'
        parent_category_name = 'properties_equipment'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_construction')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        construction_total_amount_exercised, construction_total_amount_authorized = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        construction_total_percent = self.get_percent_exercised(construction_total_amount_authorized, construction_total_amount_exercised)
        construction_to_execute = construction_total_amount_authorized - construction_total_amount_exercised

        ## Libros y revistas académicas y científicas
        category_name = 'AA'
        parent_category_name = 'properties_equipment'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_books')
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        books_total_amount_exercised, books_total_amount_authorized= self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        books_total_percent = self.get_percent_exercised(books_total_amount_authorized, books_total_amount_exercised)
        books_to_execute = books_total_amount_authorized - books_total_amount_exercised

        ## TOTAL INVERSIONES
        concept_name = 'investments_propertie_equipment'
        investments_propertie_equipment_total_amount_exercised = equipment_total_amount_exercised + construction_total_amount_exercised + books_total_amount_exercised
        investments_propertie_equipment_total_amount_authorized = equipment_total_amount_authorized + construction_total_amount_authorized + books_total_amount_authorized
        investments_propertie_equipment_total_percent = self.get_percent_exercised(investments_propertie_equipment_total_amount_authorized, investments_propertie_equipment_total_amount_exercised)
        investments_propertie_equipment_to_execute = investments_propertie_equipment_total_amount_authorized - investments_propertie_equipment_total_amount_exercised
        level = 1
        second_column_name = 'INVERSIONES'
        text_align = 'text-right'
        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, investments_propertie_equipment_total_amount_authorized, investments_propertie_equipment_total_amount_exercised, investments_propertie_equipment_total_percent, investments_propertie_equipment_to_execute, level, second_column_name, text_align))

        # EROGACIONES CON INGRESOS EXTRAORDINARIOS
        concept_name = 'income_expenses'
        date_to = options['date'].get('date_to')
        date_from = self.get_first_and_last_day_of_year(date_to)[0]
        income_expenses_total_amount_exercised = 0
        income_expenses_total_amount_authorized = self.get_income_expenses_investment_statement(date_from)
        income_expenses_total_percent = 0
        income_expenses_to_execute = income_expenses_total_amount_authorized
        level = 1
        second_column_name = 'Erogaciones con Ingresos Extraordinarios'
        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, income_expenses_total_amount_authorized, income_expenses_total_amount_exercised, income_expenses_total_percent, income_expenses_to_execute, level, second_column_name))
        
        # PRESUPUESTO MAS ASIGNADO PARTIDA 711
        concept_name = 'income_expenses'
        income_expenses_total_amount_exercised = operating_costs_total_amount_exercised + investments_propertie_equipment_total_amount_exercised + income_expenses_total_amount_exercised
        income_expenses_total_amount_authorized = operating_costs_total_amount_authorized + investments_propertie_equipment_total_amount_authorized +  income_expenses_total_amount_authorized
        income_expenses_total_percent = self.get_percent_exercised(income_expenses_total_amount_authorized, income_expenses_total_amount_exercised)
        income_expenses_to_execute = income_expenses_total_amount_authorized - income_expenses_total_amount_exercised
        level = 1
        second_column_name = ''
        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, income_expenses_total_amount_authorized, income_expenses_total_amount_exercised, income_expenses_total_percent, income_expenses_to_execute, level, second_column_name))
        
        
        ## TOTAL REMANENTE        
        concept_name = 'profit'
        profit_total_amount_exercised = income_total_amount_exercised - operating_costs_total_amount_exercised -investments_propertie_equipment_total_amount_exercised
        profit_total_amount_authorized = 0
        profit_total_percent = self.get_percent_exercised(profit_total_amount_authorized, profit_total_amount_exercised)
        profit_to_execute = profit_total_amount_authorized - profit_total_amount_exercised
        level = 1
        second_column_name = 'REMANENTE DEL EJERCICIO'
        text_align = 'text-right'
        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, profit_total_amount_authorized, profit_total_amount_exercised,  profit_total_percent, profit_to_execute, level, second_column_name, text_align))
        
        total_dict = {
            'B': [subsidy_total_amount_exercised, subsidy_total_amount_authorized, subsidy_total_percent, subsidy_to_execute],
            'C': [financial_products_total_amount_exercised, financial_products_total_amount_authorized, financial_products_total_percent, financial_products_to_execute],
            'D': [extraordinary_income_total_amount_exercised, extraordinary_income_total_amount_authorized, extraordinary_income_total_percent, extraordinary_income_to_execute],
            'E': [educational_income_total_amount_exercised, educational_income_total_amount_authorized, educational_income_total_percent, educational_income_to_execute],
            'F': [other_income_total_amount_exercised, other_income_total_amount_authorized, other_income_total_percent, other_income_to_execute],
            'G': [remuneration_academic_personal_total_amount_exercised, remuneration_academic_personal_total_amount_authorized, remuneration_academic_personal_total_percent, remuneration_academic_personal_to_execute],
            'H': [remuneration_academic_administrative_total_amount_exercised, remuneration_academic_administrative_total_amount_authorized, remuneration_academic_administrative_total_percent, remuneration_academic_administrative_to_execute],
            'I': [bonus_vacation_total_amount_exercised, bonus_vacation_total_amount_authorized, bonus_vacation_total_percent, bonus_vacation_to_execute],
            'J': [gratification_separation_retirement_total_amount_exercised, gratification_separation_retirement_total_amount_authorized, gratification_separation_retirement_total_percent, gratification_separation_retirement_to_execute],
            'K': [social_security_fees_total_amount_exercised, social_security_fees_total_amount_authorized, social_security_fees_total_percent, social_security_fees_to_execute],
            'L': [other_social_benefits_total_amount_exercised, other_social_benefits_total_amount_authorized, other_social_benefits_total_percent, other_social_benefits_to_execute],
            'M': [scolarship_total_amount_exercised, scolarship_total_amount_authorized, scolarship_total_percent, scolarship_to_execute],
            'N': [foreign_expenses_total_amount_exercised, foreign_expenses_total_amount_authorized, foreign_expenses_total_percent, foreign_expenses_to_execute],
            'O': [materials_total_amount_exercised, materials_total_amount_authorized, materials_total_percent, materials_to_execute],
            'P': [maintenance_total_amount_exercised, maintenance_total_amount_authorized, maintenance_total_percent, maintenance_to_execute],
            'Q': [electricity_total_amount_exercised, electricity_total_amount_authorized, electricity_total_percent, electricity_to_execute],
            'R': [field_work_total_amount_exercised, field_work_total_amount_authorized, field_work_total_percent, field_work_to_execute],
            'S': [services_articles_total_amount_exercised, services_articles_total_amount_authorized, services_articles_total_percent, services_articles_to_execute],
            'T': [editions_total_amount_exercised, editions_total_amount_authorized, editions_total_percent, editions_to_execute],
            'U': [membership_fees_total_amount_exercised, membership_fees_total_amount_authorized, membership_fees_total_percent, membership_fees_to_execute],
            'V': [importation_expenses_total_amount_exercised, importation_expenses_total_amount_authorized, importation_expenses_total_percent, importation_expenses_to_execute],
            'W': [advisory_services_total_amount_exercised, advisory_services_total_amount_authorized, advisory_services_total_percent, advisory_services_to_execute],
            'X': [support_programs_total_amount_exercised, support_programs_total_amount_authorized, support_programs_total_percent, support_programs_to_execute],
            'Y': [equipment_total_amount_exercised, equipment_total_amount_authorized, equipment_total_percent, equipment_to_execute],
            'Z': [construction_total_amount_exercised, construction_total_amount_authorized, construction_total_percent, construction_to_execute],
            'AA': [books_total_amount_exercised, books_total_amount_authorized, books_total_percent, books_to_execute],
        }

        self._update_total_for_category(lines, total_dict)         
   
        return lines
        
    @api.model
    def _get_report_name(self):
        return _("Income, Expenses and Investment Statement Report to Execute")

    def get_month_name(self, month):
        month_name = ''
        if month == 1:
            month_name = 'Enero'
        elif month == 2:
            month_name = 'Febrero'
        elif month == 3:
            month_name = 'Marzo'
        elif month == 4:
            month_name = 'Abril'
        elif month == 5:
            month_name = 'Mayo'
        elif month == 6:
            month_name = 'Junio'
        elif month == 7:
            month_name = 'Julio'
        elif month == 8:
            month_name = 'Agosto'
        elif month == 9:
            month_name = 'Septiembre'
        elif month == 10:
            month_name = 'Octubre'
        elif month == 11:
            month_name = 'Noviembre'
        elif month == 12:
            month_name = 'Diciembre'

        return month_name.upper()

    def get_xlsx(self, options, response=None):
        # Groups that have access to export report
        list_groups = [
            'jt_account_base.group_account_admin_user',
            'jt_account_base.group_account_super_admin_user',
            'jt_account_base.group_account_guest_user',
            'jt_account_base.group_account_approval_user']
        
        self.env['security.tools'].check_user_permissions(list_groups)  

        options['xlsx'] = True

        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        sheet = workbook.add_worksheet(self._get_report_name()[:31])
        date_default_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2, 'num_format': 'yyyy-mm-dd'})
        date_default_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': 'yyyy-mm-dd'})
        default_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2})
        default_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'})
        title_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'bottom': 2, 'align': 'center'})
        super_col_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'align': 'center'})
        level_0_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': False, 'font_size': 13, 'bottom': 6, 'font_color': '#666666', 'num_format': CURRENCY_FORMAT})
        level_1_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': False, 'font_size': 13, 'bottom': 1, 'font_color': '#666666', 'num_format': CURRENCY_FORMAT})
        level_2_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': False, 'font_size': 12, 'font_color': '#666666', 'indent': 0})
        level_2_col1_total_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': False, 'font_size': 12, 'font_color': '#666666'})
        level_2_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': False, 'font_size': 12, 'font_color': '#666666', 'num_format': CURRENCY_FORMAT})
        level_3_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 0})
        level_3_col1_total_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': False, 'font_size': 12, 'font_color': '#666666', 'indent': 0})
        level_3_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': CURRENCY_FORMAT})
        level_4_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 0})
        level_4_col1_total_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': False, 'font_size': 12, 'font_color': '#666666', 'indent': 0})
        level_4_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': CURRENCY_FORMAT})
        level_5_col1_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 0})
        level_5_col1_total_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': False, 'font_size': 12, 'font_color': '#666666', 'indent': 0})
        level_5_style = workbook.add_format(
            {'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': CURRENCY_FORMAT})
        currect_date_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': False, 'align': 'right'})
        currect_date_style.set_border(0)
        super_col_style.set_border(0)
        # Set columns width
        sheet.set_column(0, 0, 25)
        sheet.set_column(1, 1, 80)
        sheet.set_column(2, 2, 20)
        sheet.set_column(3, 3, 20)
        sheet.set_column(4, 4, 20)
        sheet.set_column(5, 5, 20)
        sheet.set_column(6, 6, 20)
        sheet.set_column(7, 7, 20)
        sheet.set_column(8, 8, 20)
        
        y_offset = 0
        col = 0
        
        sheet.merge_range(y_offset, col, 7, col, '', super_col_style)
        if self.env.user and self.env.user.company_id and self.env.user.company_id.header_logo:
            filename = 'logo.png'
            image_data = io.BytesIO(base64.standard_b64decode(self.env.user.company_id.header_logo))
            sheet.insert_image(0,0, filename, {'image_data': image_data,'x_offset':60,'y_offset':15,'x_scale':0.6,'y_scale':0.6})
        
        col += 1
        x_offset = 1

        tz = pytz.timezone('America/Mexico_City')
        actual_date = datetime.now(tz)
        currect_time_msg = "Fecha y hora de impresión: "
        month_name = self.get_month_name(actual_date.month)
        currect_time_msg += str(actual_date.day) + " de " + month_name + " del " + str(actual_date.year) + str(actual_date.strftime(" %I:%M:%S %p"))
        user_msg = " Reporte generado por: " + self.env.user.name
        sheet.merge_range(y_offset, x_offset, y_offset, x_offset+4, '', super_col_style)
        sheet.merge_range(y_offset+1, x_offset, y_offset+1, x_offset+4, 'UNIVERSIDAD NACIONAL AUTÓNOMA DE MÉXICO', super_col_style)
        sheet.merge_range(y_offset+2, x_offset, y_offset+2, x_offset+4, 'PATRONATO UNIVERSITARIO', super_col_style)
        sheet.merge_range(y_offset+3, x_offset, y_offset+3, x_offset+4, 'TESORERÍA', super_col_style)
        sheet.merge_range(y_offset+4, x_offset, y_offset+4, x_offset+4, self._get_report_name().upper(), super_col_style)
        sheet.merge_range(y_offset+5, x_offset, y_offset+5, x_offset+4, 'Cifras en Miles de Pesos', super_col_style)
        sheet.merge_range(y_offset+6, x_offset, y_offset+6, x_offset+4, currect_time_msg, super_col_style)
        sheet.merge_range(y_offset+7, x_offset, y_offset+7, x_offset+4, user_msg, super_col_style)
        y_offset += 10
  
        for row in self.get_header(options):
            x = 0
            for column in row:
                colspan = column.get('colspan', 1)
                header_label = column.get('name', '').replace('<br/>', ' ').replace('&nbsp;', ' ')

                if colspan == 1:
                    sheet.write(y_offset, x, header_label, title_style)
                x += colspan
            y_offset += 1
        ctx = self._set_context(options)
        ctx.update({'no_format':True, 'print_mode':True, 'prefetch_fields': False})

        # deactivating the prefetching saves ~35% on get_lines running time
        lines = self.with_context(ctx)._get_lines(options)
        if options.get('hierarchy'):
            lines = self._create_hierarchy(lines, options)
        if options.get('selected_column'):
            lines = self._sort_lines(lines, options)            
        
        # # write all data rows
        row_y = 0
        for y in range(0, len(lines)):
            level = lines[y].get('level')
            if lines[y].get('caret_options'):
                style = level_3_style
                col1_style = level_3_col1_style
            elif level == 0:
                style = level_0_style
                col1_style = style
            elif level == 1:
                style = level_1_style
                col1_style = style
            elif level == 2:
                style = level_2_style
                col1_style = 'total' in lines[y].get('class', '').split(
                    ' ') and level_2_col1_total_style or level_2_col1_style
            elif level == 3:
                style = level_3_style
                col1_style = 'total' in lines[y].get('class', '').split(
                    ' ') and level_3_col1_total_style or level_3_col1_style
            elif level == 4:
                style = level_4_style
                col1_style = 'total' in lines[y].get('class', '').split(
                    ' ') and level_4_col1_total_style or level_4_col1_style
            elif level == 5:
                style = level_5_style
                col1_style = 'total' in lines[y].get('class', '').split(
                    ' ') and level_5_col1_total_style or level_5_col1_style
            else:
                style = default_style
                col1_style = default_col1_style

            if lines[y]['level'] != 3:
                #write the first column, with a specific style to manage the indentation
                cell_type, cell_value = self._get_cell_type_value(lines[y])
                if cell_type == 'date':
                    sheet.write_datetime(row_y + y_offset, 0, cell_value, date_default_col1_style)
                else:
                    sheet.write(row_y + y_offset, 0, cell_value, col1_style)

                #write all the remaining cells
                for x in range(1, len(lines[y]['columns']) + 1):
                    cell_type, cell_value = self._get_cell_type_value(lines[y]['columns'][x - 1])
                    if cell_type == 'date':
                        sheet.write_datetime(row_y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, date_default_style)
                    else:
                        sheet.write(row_y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, style)
                row_y += 1

        workbook.close()
        output.seek(0)
        generated_file = output.read()
        output.close()
        return generated_file    

    def _get_accounts_from_detailed_report_configuration(self, name):
        
        query = """
            select
                account_account_id
            from
                account_account_detailed_report_configuration_rel
            where
                detailed_report_configuration_id = (
                select
                    id
                from
                    detailed_report_configuration
                where
                    name = %(name)s)
        
        """
        params = {'name' : name}


        self.env.cr.execute(query, params)
        result = self.env.cr.fetchall()

        result_list = []

        for rec in result :
            result_list.append(rec[0])

        return result_list

    def _add_accounts_to_lines (self, lines, options, category_name, parent_name):
        accounts_list  =  self.env['account.account'].browse(self._get_accounts_from_detailed_report_configuration(category_name))

        total_amount_exercised = 0

        total_authorized = 0
        list_cri = []

        for account in accounts_list:
            actual_date = options['date'].get('date_to')
            authorized_amount = 0
            amount_exercised, account_type = self._verify_account_type(account.code, self._get_account_total_amount(actual_date, account.id))
            
            if account_type == 'CRI':
                authorized_amount, cri_id = self._get_cri_total_amount(actual_date, account.id)
                result = self.verify_unique_element_in_list(list_cri, cri_id)
                if result:
                    total_authorized += authorized_amount
            elif account_type == 'COG':
                authorized_amount = self._get_cog_total_amount(actual_date, account.id)
                total_authorized += authorized_amount
            else:
                authorized_amount = 0

            total_amount_exercised += amount_exercised
            
            lines.append(
                {'id': 'column_' + str(account.id),
                'name': '',
                'level': 3,
                'parent_id': parent_name,
                'columns' : [
                    {
                        'id': 'account_' + str(account.id),
                        'name': account.code,
                        'level': 3,
                        'columns' : [],
                        'unfoldable': False,
                    },
                    {},
                    {
                        'id': 'account_total_' + str(account.id),
                        'name': self._get_rounded_formatted_value(amount_exercised), 'class': 'number', 'raw_value': self._get_rounded_formatted_value(amount_exercised),
                        'level': 3,
                        'columns' : [],
                        'unfoldable': False,
                    },
                    {},
                    {},

                ]})
            
        return total_amount_exercised , total_authorized
        
    def _get_dict_column(self, name, level):
        dict_column = {
            'id': 'category_' + name,
            'name': name,
            'level': level
        }
        return dict_column

    def _get_dict_number_format(self, amount, level):
        dict_column = {
            'name': amount, 
            'class': 'number',
            'level': level
        }
        return dict_column
    
    def _get_dict_number_rounded_thousand_format(self, amount, level):
        dict_column = {
            'name': self._get_rounded_formatted_value(amount), 
            'class': 'number',
            'level': level
        }
        return dict_column


    def _get_account_total_amount(self, date_to, account_id):
        
        date_from = self.get_first_and_last_day_of_year(date_to)[0]

        query = sql.SQL("""
                select
                    COALESCE(SUM(balance), 0) as sumatoria_apuntes
                from
                    account_move_line
                where
                    account_id = {}
                    and date >= {}
                    and date <= {}
                    and parent_state = 'posted';
            """).format(
                sql.Literal(account_id),
                sql.Literal(date_from),
                sql.Literal(date_to)
                )
            
        self.env.cr.execute(query)
        result = self.env.cr.fetchall()
        if result:
            return result[0][0]
        else:
            return 0.0
        
    def _get_cog_total_amount(self, date_to, account_id):
        date_from = self.get_first_and_last_day_of_year(date_to)[0]

        query = sql.SQL("""
                select
	                COALESCE(SUM(authorized), 0) as sumatoria
	                from
	                    expenditure_budget_line
	                where
	                    item = (select item from expenditure_item where unam_account_id = {} and active = true)
                        and budget_line_yearly_id is null
	                    and start_date = {}
	                    and state = 'success'
                """).format(
                sql.Literal(account_id),
                sql.Literal(date_from)
                )
            
        self.env.cr.execute(query)
        result = self.env.cr.fetchall()
        if result:
            return result[0][0]
        else:
            return 0.0
        
    def _get_cri_total_amount(self, date_to, account_id):
        date_from = self.get_first_and_last_day_of_year(date_to)[0]

        query = sql.SQL("""
                select
                    ibl.estimated_total, estimated_cri_id
                from
                    siif_budget_mgmt_income_budgets ib
                join siif_budget_mgmt_income_budgets_lines ibl on
                    ibl.my_id = ib.id
                where
                    ib.state = 'validated'
                    and ib.from_date = {}
                    and ibl.estimated_cri_id = (
                    select
                        cri
                    from
                        relation_account_cri rac
                    where
                        cuentas_id = {})
                """).format(
                sql.Literal(date_from),
                sql.Literal(account_id)
                )
            
        self.env.cr.execute(query)
        result = self.env.cr.fetchall()
        if result:
            total = result[0][0]
            cri_id = result[0][1]
        else:
            total = 0.0
            cri_id = 0
        return total, cri_id
             
    def get_first_and_last_day_of_year(self, date):
        date = datetime.strptime(date, DATE_FORMAT)
        first_day = datetime(date.year, 1, 1)
        last_day = datetime(date.year, 12, 31)
        return first_day, last_day
    
    def convert_negative_number_to_absolute_with_parentheses(self,number):
        if number < 0:
            return f"({abs(number)})"
        else:
            return str(number)
        

    def get_percent_variation(self, actual_amount, last_year_amount):
        if last_year_amount == 0:
            return 0
        else:
            percent = ((actual_amount - last_year_amount) / last_year_amount) * 100
            return round(percent)
        
    def _verify_account_type(self, account_code,amount):
        account_type =  ''
        if account_code.startswith('4'):
            amount = amount * -1
            account_type = 'CRI'
        elif account_code.startswith('5'):
            account_type = 'COG'
        return amount, account_type
    
    def _get_annexes_name(self, xml_id):
        annexe = self.env.ref(xml_id).name
        return annexe

    def _update_total_for_category(self, lines, dict_total):
        categorys = ['B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P','Q','R','S','T','U','V','W','X','Y','Z','AA']
        for line in lines:
            if line['id'] in categorys:
                line['columns'][1] = self._get_dict_number_rounded_thousand_format(dict_total[line['id']][1], 1)
                line['columns'][2] = self._get_dict_number_rounded_thousand_format(dict_total[line['id']][0], 1)
                line['columns'][3] = self._get_dict_number_format(dict_total[line['id']][2], 1)
                line['columns'][4] = self._get_dict_number_rounded_thousand_format(dict_total[line['id']][3], 1)
                
    def _get_rounded_formatted_value(self, value):
        round_value = round(value/1000)
        return round_value

    def get_annexe_dict_colum(self, category_name, parent_category_name, annexe_name, level, unfolded=False):
        dict_column = {
            'id': category_name,
            'name': category_name,
            'level': 1,
            'columns' : [
                self._get_dict_column(annexe_name, level),
                {},
                self._get_dict_number_format(0, level),
                self._get_dict_number_format(0, level),
                self._get_dict_number_format(0, level),
                 ],
            'unfoldable': True,
            'parent_id': parent_category_name,
            'unfolded': unfolded,
            }
        return dict_column
                
    def get_total_annexe_dict_column(self, concept_name, parent_category_name, total_amount_authorized, total_amount_exercised, total_percent, total_to_execute, level, second_column_name='', text_align='text-left'):
        dict_total_column = {
            'id': 'total_' + concept_name,
            'name': '',
            'level': level,
            
            'columns' : [
                {
                    'id': 'total_amount' + concept_name,
                    'class': text_align,
                    'name': second_column_name,
                    'level': level,
                    'columns' : [],
                    'unfoldable': False,
                },
                {
                    'id': 'total_amount_authorized' + concept_name,
                    'name': self._get_rounded_formatted_value(total_amount_authorized), 'class': 'number',
                    'level': level,
                    'columns' : [],
                    'unfoldable': False,
                },
                {
                    'id': 'total_amount_exercised' + concept_name,
                    'name': self._get_rounded_formatted_value(total_amount_exercised), 'class': 'number',
                    'level': level,
                    'columns' : [],
                    'unfoldable': False,
                },
                {
                    'id': 'total_percent' + concept_name,
                    'name': total_percent,
                    'level': level,
                    'columns' : [],
                    'unfoldable': False,
                },
                {
                    'id': 'total_to_execute' + concept_name,
                    'name': self._get_rounded_formatted_value(total_to_execute),
                    'level': level,
                    'columns' : [],
                    'unfoldable': False,
                }
                 ],
            'unfoldable': False,
            'parent_id': parent_category_name,
            }
        return dict_total_column
    
    def get_category_dict_column(self, category_name, annexe_name, level, parent_category_name=False, first_column_name=''):
        dict_column = {
            'id': category_name,
            'name':  first_column_name,
            'level': level,
            'columns' : [
                self._get_dict_column(annexe_name, level),
                {},
                {},
                {},
                {},
                 ],
            'unfoldable': True,
            'unfolded': True,
            'parent_id': parent_category_name
            }
        
        return dict_column
    
    def _get_cog_(self, account_id):
        query = sql.SQL("""
                select item from expenditure_item where unam_account_id = {}
                """).format(
                sql.Literal(account_id)
                )
        self.env.cr.execute(query)
        result = self.env.cr.fetchall()
        
        if result:
            return result[0][0]
    
    def verify_unique_element_in_list(self, list, element):
        if element not in list:
            list.append(element)
            return True
        else :
            return False

    def get_percent_exercised(self, authorized_amount, exercised_amount):
        if authorized_amount == 0:
            return 0
        else:
            percent = (exercised_amount * 100) / authorized_amount
            return round(percent)

    def get_income_expenses_investment_statement(self,date_from, par='711'):
        query = sql.SQL("""
                select
	                COALESCE(SUM(authorized), 0) as sumatoria
	                from
	                    expenditure_budget_line
	                where
	                    item = {}
	                    and start_date = {}
                        and budget_line_yearly_id is null 
	                    and state = 'success'
                """).format(
                sql.Literal(par),
                sql.Literal(date_from)
                )
            
        self.env.cr.execute(query)
        result = self.env.cr.fetchall()
        if result:
            return result[0][0]
        else:
            return 0.0