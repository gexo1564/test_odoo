# -*- coding: utf-8 -*-
import logging
from odoo import models, fields, _
from odoo.exceptions import UserError, AccessError, ValidationError
from odoo.tools import float_compare
from odoo.tools.misc import get_lang

from datetime import timedelta
import logging
_logger = logging.getLogger(__name__)

ACC_MOVE='account.move'
BASE_MODULE_SECURITY_MODEL = 'base.module.security'
ACCOUNT_MOVE_MANUAL_PERMISSION_MODEL = 'account.move.manual.permission'
# Constants for permissions
ACCOUNT_SUPER_ADMIN_USER_GROUP = 'jt_account_base.group_account_super_admin_user'
ACCOUNT_ADMIN_USER_GROUP = 'jt_account_base.group_account_admin_user'
ACCOUNT_REGISTER_USER_GROUP = 'jt_account_base.group_account_register_user'
ACCOUNT_APPROVAL_USER_GROUP = 'jt_account_base.group_account_approval_user'
ACCOUNT_TRAINING_USER_GROUP = 'jt_account_base.group_account_training_user'
ACCOUNT_APPROVAL_EXTERNAL_USER_GROUP = 'jt_account_base.group_account_external_approval_user'

class AccountMove(models.AbstractModel):
    _inherit = ACC_MOVE

    is_original_of_reverse = fields.Boolean(readonly=False)
    is_manual_accounting_record = fields.Boolean('Is a manual accounting record', readonly=True)     
    
    def post(self):
        user_permission = self.env.user.has_group(ACCOUNT_APPROVAL_USER_GROUP)\
            or self.env.user.has_group(ACCOUNT_SUPER_ADMIN_USER_GROUP)\
            or self.env.user.has_group(ACCOUNT_ADMIN_USER_GROUP)\
            or self.env.user.has_group(ACCOUNT_TRAINING_USER_GROUP)\
            or self.env.user.has_group(ACCOUNT_APPROVAL_EXTERNAL_USER_GROUP)\
            or self.env.user.has_group('sidia.Rol012')\
            or self.env.user.has_group('sidia.Rol013')\
            or self.env.user.has_group('sidia.Rol264')\
            or self.env.user.has_group('sidia.Rol319')\
            or self.env.user.has_group('jt_investment.group_investment_admin_user')\
            or self.env.user.has_group('jt_payroll_payment.group_on_finance_admin')\
            or self.env.user.has_group('jt_payroll_payment.group_admin_finance_user')\
            or self.env.user.has_group('jt_payroll_payment.group_approval_user')\
            or self.env.user.has_group('jt_payroll_payment.group_register_user')

        for move in self:
            if move.is_manual_accounting_record:
                move._verify_permissions_from_user(move.journal_id.id)

        if not self.env.su and not user_permission:
            raise AccessError(_("You don't have the access rights to post an invoice."))
        for move in self:
            if not move.line_ids.filtered(lambda line: not line.display_type):
                raise UserError(_('You need to add a line before posting.'))
            if move.auto_post and move.date > fields.Date.today():
                date_msg = move.date.strftime(get_lang(self.env).date_format)
                raise UserError(_("This move is configured to be auto-posted on %s" % date_msg))

            if not move.partner_id:
                if move.is_sale_document():
                    raise UserError(_("The field 'Customer' is required, please complete it to validate the Customer Invoice."))
                elif move.is_purchase_document():
                    raise UserError(_("The field 'Vendor' is required, please complete it to validate the Vendor Bill."))

            if move.is_invoice(include_receipts=True) and float_compare(move.amount_total, 0.0, precision_rounding=move.currency_id.rounding) < 0:
                raise UserError(_("You cannot validate an invoice with a negative total amount. You should create a credit note instead. Use the action menu to transform it into a credit note or refund."))

            # Handle case when the invoice_date is not set. In that case, the invoice_date is set at today and then,
            # lines are recomputed accordingly.
            # /!\ 'check_move_validity' must be there since the dynamic lines will be recomputed outside the 'onchange'
            # environment.
            if not move.invoice_date and move.is_invoice(include_receipts=True):
                move.invoice_date = fields.Date.context_today(self)
                move.with_context(check_move_validity=False)._onchange_invoice_date()

            # When the accounting date is prior to the tax lock date, move it automatically to the next available date.
            # /!\ 'check_move_validity' must be there since the dynamic lines will be recomputed outside the 'onchange'
            # environment.
            if (move.company_id.tax_lock_date and move.date <= move.company_id.tax_lock_date) and (move.line_ids.tax_ids or move.line_ids.tag_ids):
                move.date = move.company_id.tax_lock_date + timedelta(days=1)
                move.with_context(check_move_validity=False)._onchange_currency()

        # Create the analytic lines in batch is faster as it leads to less cache invalidation.
        self.mapped('line_ids').create_analytic_lines()
        for move in self:
            if move.auto_post and move.date > fields.Date.today():
                raise UserError(_("This move is configured to be auto-posted on {}".format(move.date.strftime(get_lang(self.env).date_format))))

            move.message_subscribe([p.id for p in [move.partner_id] if p not in move.sudo().message_partner_ids])

            to_write = {'state': 'posted'}

            if move.name == '/':
                # Get the journal's sequence.
                sequence = move._get_sequence()
                seq = move._get_sequence().prefix.split('/')

                if not sequence:
                    raise UserError(('Please define a sequence on your journal.'))
                #logging.critical(seq[0])

                #Case when sequence has a different code
                if(seq[0] != str(move.journal_id.code)):
                    aux = sequence.with_context(ir_sequence_date=move.date).next_by_id()
                    name_aux = aux.split('/')
                    to_write['name']=str(move.journal_id.code)
                    for i in range (1,len(name_aux)):
                        to_write['name']+="/"+name_aux[i]
                else:
                    # Consume a new number.
                    to_write['name'] = sequence.with_context(ir_sequence_date=move.date).next_by_id()     

            else:
                #Case Change the code of a Journey
                logging.critical(dir(move._get_sequence()))
                seq = move.name.split('/')
                
                if(str(move.journal_id.code) != seq[0]):
                    #logging.critical("Codigos diferentes")
                    to_write['name']=str(move.journal_id.code)
                    for i in range (1,len(seq)):
                        to_write['name']+="/"+seq[i]
                    #logging.critical(to_write['name'])

            move.write(to_write)

            # Compute 'ref' for 'out_invoice'.
            if move.type == 'out_invoice' and not move.invoice_payment_ref:
                to_write = {
                    'invoice_payment_ref': move._get_invoice_computed_reference(),
                    'line_ids': []
                }
               
                for line in move.line_ids.filtered(lambda line: line.account_id.user_type_id.type in ('receivable', 'payable')):
                    to_write['line_ids'].append((1, line.id, {'name': to_write['invoice_payment_ref']}))

                move.write(to_write)

                # If account 120.001.001 change description
                change_description = f"Cuenta por cobrar {move.partner_id.name} {move.payment_of_id.name} {move.payment_of_id.description} {move.invoice_date.strftime('%d/%m/%Y')}"     
                account_id = self.env['account.account'].search([('code', '=', '120.001.001')])
                new_name = self.env['account.move.line'].search([('move_id', '=', move.id),('account_id','=',account_id.id)])
                new_name.write({'name' : change_description})

            if move == move.company_id.account_opening_move_id and not move.company_id.account_bank_reconciliation_start:
                # For opening moves, we set the reconciliation date threshold
                # to the move's date if it wasn't already set (we don't want
                # to have to reconcile all the older payments -made before
                # installing Accounting- with bank statements)
                move.company_id.account_bank_reconciliation_start = move.date

        for move in self:
            if not move.partner_id: continue
            partners = (move.partner_id | move.partner_id.commercial_partner_id)
            if move.type.startswith('out_'):
                partners._increase_rank('customer_rank')
            elif move.type.startswith('in_'):
                partners._increase_rank('supplier_rank')
            else:
                continue

        # Trigger action for paid invoices in amount is zero
        self.filtered(
            lambda m: m.is_invoice(include_receipts=True) and m.currency_id.is_zero(m.amount_total)
        ).action_invoice_paid()

        # Force balance check since nothing prevents another module to create an incorrect entry.
        # This is performed at the very end to avoid flushing fields before the whole processing.
        self._check_balanced()
        return True
    
    def _reverse_moves(self, default_values_list=None, cancel=False):
        ''' Reverse a recordset of account.move.
        If cancel parameter is true, the reconcilable or liquidity lines
        of each original move will be reconciled with its reverse's.

        :param default_values_list: A list of default values to consider per move.
                                    ('type' & 'reversed_entry_id' are computed in the method).
        :return:                    An account.move recordset, reverse of the current self.
        '''
        # Groups that have access to the module
        list_groups = [
            ACCOUNT_SUPER_ADMIN_USER_GROUP,
            ACCOUNT_ADMIN_USER_GROUP,
            ACCOUNT_REGISTER_USER_GROUP,
            ACCOUNT_APPROVAL_USER_GROUP,
            ACCOUNT_TRAINING_USER_GROUP,
            ACCOUNT_APPROVAL_EXTERNAL_USER_GROUP
            ]
        
        self.env[BASE_MODULE_SECURITY_MODEL].check_user_permissions(list_groups)    

        for move in self:
            reversed_id = self.env[ACC_MOVE].search([('reversed_entry_id', '=', move.id), ('type', '=', 'out_refund')])
            logging.critical("ID: "+str(move.id))

            if reversed_id:
                if self.env.user.lang == 'es_MX':
                    raise ValidationError(_("Ya existe una nota de crédito asociada a esta factura: " + str(reversed_id.name)))
                else:
                    raise ValidationError(_("There is already a credit note associated with this invoice"))

        if not default_values_list:
            default_values_list = [{}] * len(self)

        if cancel:
            lines = self.mapped('line_ids')
            # Avoid maximum recursion depth.
            if lines:
                lines.remove_move_reconcile()

        reverse_type_map = {
            'entry': 'entry',
            'out_invoice': 'out_refund',
            'out_refund': 'entry',
            'in_invoice': 'in_refund',
            'in_refund': 'entry',
            'out_receipt': 'entry',
            'in_receipt': 'entry',
        }

        move_vals_list = []
        for move, default_values in zip(self, default_values_list):
            if self.is_manual_accounting_record:
                self._verify_permissions_from_user(default_values['journal_id'])
                
            default_values.update({
                'type': reverse_type_map[move.type],
                'reversed_entry_id': move.id,
                'income_bank_journal_id': move.income_bank_journal_id.id,
            })
            move_vals_list.append(move.with_context(move_reverse_cancel=cancel)._reverse_move_vals(default_values, cancel=cancel))

        reverse_moves = self.env[ACC_MOVE].sudo().create(move_vals_list)
        for move, reverse_move in zip(self, reverse_moves.with_context(check_move_validity=False)):
            # Update amount_currency if the date has changed.
            if move.date != reverse_move.date:
                for line in reverse_move.line_ids:
                    if line.currency_id:
                        line._onchange_currency()
            reverse_move._recompute_dynamic_lines(recompute_all_taxes=False)
        reverse_moves._check_balanced()

        # Reconcile moves together to cancel the previous one.
        if cancel:
            reverse_moves.with_context(move_reverse_cancel=cancel).post()
            for move, reverse_move in zip(self, reverse_moves):
                accounts = move.mapped('line_ids.account_id') \
                    .filtered(lambda account: account.reconcile or account.internal_type == 'liquidity')
                for account in accounts:
                    (move.line_ids + reverse_move.line_ids)\
                        .filtered(lambda line, account_id = account: line.account_id == account_id and line.balance)\
                        .reconcile()

        self.is_original_of_reverse = True
        return reverse_moves

    def button_cancel(self):
        if self.is_manual_accounting_record:
            self._verify_permissions_from_user(self.journal_id.id)
        # Groups that have access to the module
        list_groups = [
            ACCOUNT_SUPER_ADMIN_USER_GROUP,
            ACCOUNT_ADMIN_USER_GROUP,
            ACCOUNT_REGISTER_USER_GROUP,
            ACCOUNT_APPROVAL_USER_GROUP,
            ACCOUNT_TRAINING_USER_GROUP,
            ACCOUNT_APPROVAL_EXTERNAL_USER_GROUP
            ] 
        self.env[BASE_MODULE_SECURITY_MODEL].check_user_permissions(list_groups) 

        result = super(AccountMove,self).button_cancel()      
        return result

    def button_draft(self):
        if self.is_manual_accounting_record:
            self._verify_permissions_from_user(self.journal_id.id)
        # Groups that have access to the module
        list_groups = [
            ACCOUNT_SUPER_ADMIN_USER_GROUP,
            ACCOUNT_ADMIN_USER_GROUP,
            ACCOUNT_APPROVAL_USER_GROUP,
            ACCOUNT_TRAINING_USER_GROUP,
            ACCOUNT_APPROVAL_EXTERNAL_USER_GROUP
            ]
        self.env[BASE_MODULE_SECURITY_MODEL].check_user_permissions(list_groups) 

        result = super(AccountMove,self).button_draft()
        return result

    def action_invoice_sent(self):
        # Groups that have access to the module
        list_groups = [
            ACCOUNT_SUPER_ADMIN_USER_GROUP,
            ACCOUNT_ADMIN_USER_GROUP,
            ACCOUNT_REGISTER_USER_GROUP,
            ACCOUNT_APPROVAL_USER_GROUP,
            ACCOUNT_TRAINING_USER_GROUP,
            ACCOUNT_APPROVAL_EXTERNAL_USER_GROUP,
            'jt_income.group_income_admin_user'
            ]
        self.env[BASE_MODULE_SECURITY_MODEL].check_user_permissions(list_groups) 

        result = super(AccountMove, self).action_invoice_sent()
        return result 
    
    def _verify_permissions_from_user(self, journal_id):
        user = self.env.user
        user_permission = self.env[ACCOUNT_MOVE_MANUAL_PERMISSION_MODEL].search([('res_users_id','=',user.id)])
        if journal_id not in user_permission.allowed_journals.ids:
            raise AccessError(_("You don't have permission to use this journal."))

#    def write(self,values):

 #       for value in self.line_ids:
  #          if value.debit == 0 and value.credit == 0:
   #             if self.env.user.lang == 'es_MX':
    #                raise ValidationError('Al menos uno de los valores (debe/haber) debe ser mayor a 0')
     #           else:
      #              raise UserError('At least one of the values (debit/credit) must be greater than 0')
