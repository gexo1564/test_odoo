from odoo import api, fields, models

class DetailedReportConfiguration(models.Model):
    _name = 'detailed.report.configuration'
    _description = 'Groups of accounts used for the income, expenses and investments report'

    name = fields.Char(string='Name')
    account_group_ids = fields.Many2many(comodel_name='account.group', string='Account Groups')
    accounts_ids = fields.Many2many('account.account')
    conf_type = fields.Selection(string = 'Type',
                                 selection = [
                                     ('income', 'Income'),
                                     ('expenses', 'Expenses'),
                                     ('investments', 'Investments'),
                                     ('subsidy', 'Subsidy'),
                                 ],
                                 help = 'Used for grouping configuration groups in the reports')
    order = fields.Integer(string = 'Ordering')
    expenditure_item_ids = fields.Many2many('expenditure.item', string = 'Annex Items', help = 'Select the expenditure items that will become annexes')

    annex_account_group_ids = fields.One2many(
        'relation.annex.account.group', 'annex_account_group_id', string="PAR Annex Account Group")


    @api.onchange('account_group_ids')
    def _onchange_account_group_ids(self):
        for record in self:
            accounts = self.env['account.account'].search([('group_id', 'child_of', self.account_group_ids.ids)])
            record.accounts_ids = None
            record.accounts_ids = [(4, account.id, 0) for account in accounts]


class RelationAnnexAccountGroup(models.Model):

    _inherit =  'relation.annex.account.group'

    annex_account_group_id = fields.Many2one(
        'detailed.report.configuration', string="Relation Annex Account Group")