# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import ValidationError

class BaseSubProgram(models.Model):
    _name = 'base.sub.program'
    _rec_name = 'sub_program'

    sub_program = fields.Char('Sub Program', size=2)

    _sql_constraints = [
        ('sub_program_uk', 'unique(sub_program)', 'Sub Program exists!')
    ]

    def name_get(self):
        return [(rec.id, rec.sub_program) for rec in self]