# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

from odoo.addons.siif_security.tools.utils import check_authorization, check_only_allowed_fields
from datetime import datetime
class GeographicLocation(models.Model):

    _name = 'geographic.location'
    _description = 'Geographic Location'
    _rec_name = 'state_key'

    state_key = fields.Char(string='Geographic location', size=2)
    state_name = fields.Text(string='Name of Geographic Location')

    _sql_constraints = [('state_key', 'unique(state_key, start_date, end_date)',
                         'The state key must be unique.')]

    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')

    @api.constrains('start_date', 'end_date')
    def check_end_date(self):
        if not self.start_date:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("Configure la fecha inicial"))
            else:
                raise ValidationError(_("The start date of validity is required"))
        if self.end_date and self.end_date <= self.start_date:
            if self.env.user.lang == 'es_MX':
                raise ValidationError(_("La fecha de fin de vigencia no puede ser menor o igual a la fecha de inicio de vigencia"))
            else:
                raise ValidationError(_("The effective end date cannot be less than or equal to the effective date"))

    @api.constrains('state_key')
    def _check_state_key(self):
        if not str(self.state_key).isnumeric():
            raise ValidationError(_('The state key must be numeric value'))

    def fill_zero(self, code):
        return str(code).zfill(2)

    @api.model
    def create(self, vals):
        if vals.get('state_key') and len(vals.get('state_key')) != 2:
            vals['state_key'] = self.fill_zero(vals.get('state_key'))
        return super(GeographicLocation, self).create(vals)

    @check_authorization(
        'jt_budget_mgmt.group_budget_catalogs_admin_user',
        'jt_budget_mgmt.group_budget_admin_user'
    )
    @check_only_allowed_fields('state_name', 'start_date', 'end_date')
    def write(self, vals):
        return super(GeographicLocation, self).write(vals)

    @check_authorization(
        'jt_budget_mgmt.group_budget_catalogs_admin_user',
        'jt_budget_mgmt.group_budget_admin_user'
    )
    def unlink(self):
        for location in self:
            program_code = self.env['program.code'].search([('location_id', '=', location.id)], limit=1)
            if program_code:
                raise ValidationError(_('You can not delete geographic location item which are mapped with program code!'))
        return super(GeographicLocation, self).unlink()

    def validate_geo_location(self, location_string):
        if len(str(location_string)) > 1:
            location_str = str(location_string).zfill(2)
            if location_str.isnumeric():
                location = self.search(
                    [('state_key', '=', location_str)], limit=1)
                if location:
                    return location
        return False
    
    # if not return value is not valid #
    def get_validity_geographic_location(self, state_key, today = datetime.now()):
        query = """
                SELECT id
                FROM geographic_location
                WHERE 
                    (
                        (start_date IS NULL AND end_date IS NULL AND state_key = %(state_key)s) OR
                        (start_date IS NOT NULL AND end_date IS NULL AND state_key = %(state_key)s AND start_date <= %(today)s) OR
                        (start_date IS NOT NULL AND end_date IS NOT NULL AND state_key = %(state_key)s AND start_date <= %(today)s AND end_date >= %(today)s)
                    )
                """
        params = {'state_key' : str(state_key),
                  'today': today}
        self.env.cr.execute(query, params)
        geographic_location_validity = self.env.cr.fetchall()

        return geographic_location_validity
