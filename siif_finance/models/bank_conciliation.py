from odoo import models, fields, api,_
from datetime import datetime, timedelta
from odoo.exceptions import ValidationError
import math
import logging
from lxml import etree

class BankConciliation(models.Model):
    
    _name = 'bank.conciliation'
    _description = "Bank conciliation"
    _rec_name = 'identifier'

    journal_id = fields.Many2one('account.journal', "Journal")
    start_date = fields.Date("Start date")
    end_date = fields.Date("End date")
    bank_account = fields.Many2one(related="journal_id.bank_account_id", string="Bank Account", store=True)
    identifier = fields.Char("Identfier")
    bank_conciliation_line_ids = fields.One2many('bank.conciliation.line','bank_conciliation_id','Movements')

    def reconcile(self):
        for record in self.bank_conciliation_line_ids:
            if record.state == 'reconciled':
                continue
            # Si es un movimiento gasto (pagos)
            if record.charge_amount > 0:
                # Se busca si hay alguna solicitud que pago que haga match para validar
                # el pago
                ok = self.env['expense.recognition'].recognize_payment(record)
                # Si el pago fue validado se cambia el estado del movimiento bancario
                if ok:
                    record.state = 'reconciled'
                    continue
            invoice_list  = []
            ref_aux = ''

            if not record.reference_plugin_1 and not record.reference_plugin_2 and not record.reference_plugin_3 and not record.ref:
                if self.env.user.lang == 'es_MX':
                    raise ValidationError(_("El registro no tiene referencias para validar el movimiento"))
                else:
                    raise ValidationError(_("The registry has not references"))

            if record.reference_plugin_1:
                string_len = len(record.reference_plugin_1)

                if string_len>20:
                    if record.reference_plugin_1[0:2]=='CE' or record.reference_plugin_1[0:2]=='CC' or string_len == 28:
                        ref_aux = record.reference_plugin_1[8:string_len]
                    elif record.reference_plugin_1[0:2]=='CI':
                        ref_aux = record.reference_plugin_1[2:22]
                    else:
                        ref_aux = record.reference_plugin_1
                else:
                    ref_aux = record.reference_plugin_1
            else:
               ref_aux = record.reference_plugin_1 

            record.state = 'unreconciled'

            move_id = self.env['account.move'].search([('type_of_revenue_collection', '=', 'billing'),('ref', 'in', (ref_aux, record.ref, record.reference_plugin_2, record.reference_plugin_3)),('amount_total', '=', record.amount), ('ref', 'not in', ('', False)), ('state','!=', 'cancel') ])

            donations_id = self.env['sd_base.deposit_slip.line'].search([('reference', 'in', (ref_aux, record.ref, record.reference_plugin_2, record.reference_plugin_3)),('amout_p_token', '=', record.amount), ('reference', 'not in', ('', False))])
            payment_id = self.env['account.payment'].search([('bank_reference', 'in', (ref_aux, record.ref, record.reference_plugin_2, record.reference_plugin_3)), ('bank_reference', 'not in', ('', False)), ('amount', '=', record.amount)])
            bank_ref_id =  self.env['bank.reference'].search([('reference', 'in', (ref_aux, record.ref, record.reference_plugin_2, record.reference_plugin_3)), ('reference', 'not in', ('', False)), ('amount', '=', record.amount)])
            movement_id = record.search_movement()
            logging.critical(donations_id)
            logging.critical(payment_id)
            logging.critical(bank_ref_id)
            today = datetime.today().date()
            user = self.env.user
            partner_id = user.partner_id.id
            journal = self.journal_id
            if move_id and movement_id:
                if move_id.invoice_payment_state == 'not_paid':
                    if not move_id.income_bank_journal_id:
                        move_id.income_bank_journal_id = self.journal_id.id
                    if not move_id.invoice_date_conciliation:
                        move_id.invoice_date_conciliation=record.movement_date
                    if move_id.state == 'posted':
                        invoice_list.append((4,move_id.id))
                        payment_register_id = self.env['account.payment.register'].create({'payment_method_id':1,'journal_id':move_id.income_bank_journal_id.id,'invoice_ids':invoice_list})
                        id_payment = payment_register_id.create_payments()
                        record.state = 'reconciled'
                        record.invoice_id = move_id.id
                        record.payment_id = id_payment
                        record.movement_id = movement_id.id
                        movement_id.conciliation_state = 'reconciled'

                        id_reconcile = id_payment.move_line_ids.filtered(lambda x:x.full_reconcile_id != False).full_reconcile_id.id
                        for account_bank in id_payment.move_line_ids.filtered(lambda x:x.account_id.id == self.journal_id.default_debit_account_id.id):
                            account_bank.full_reconcile_id = id_reconcile

                    elif move_id.state == 'draft':
                        move_id.action_post()
                        payment_register_id = self.env['account.payment.register'].create({'payment_method_id':1,'journal_id':move_id.income_bank_journal_id.id,'invoice_ids':invoice_list})
                        id_payment = payment_register_id.create_payments()
                        record.state = 'reconciled'
                        record.invoice_id = move_id.id
                        record.payment_id = id_payment
                        record.movement_id = movement_id.id
                        movement_id.conciliation_state = 'reconciled'

                        id_reconcile = id_payment.move_line_ids.filtered(lambda x:x.full_reconcile_id != False).full_reconcile_id.id
                        for account_bank in id_payment.move_line_ids.filtered(lambda x:x.account_id.id == self.journal_id.default_debit_account_id.id):
                            account_bank.full_reconcile_id = id_reconcile
                else:
                    record.state = 'reconciled'
                    record.invoice_id = move_id.id
                    record.movement_id = movement_id.id
                    movement_id.conciliation_state = 'reconciled'
            if donations_id and movement_id:
                donations_id.recognized = True
                record.state = 'reconciled'
                record.donations_id = donations_id.donations_id.id
                record.movement_id = movement_id.id
                movement_id.conciliation_state = 'reconciled'
            if payment_id and movement_id:
                record.state = 'reconciled'
                record.payment_id = payment_id.id
                record.movement_id = movement_id.id
                movement_id.conciliation_state = 'reconciled'
                id_reconcile = payment_id.move_line_ids.filtered(lambda x:x.full_reconcile_id != False).full_reconcile_id.id
                for account_bank in payment_id.move_line_ids.filtered(lambda x:x.account_id.id == self.journal_id.default_debit_account_id.id):
                    account_bank.full_reconcile_id = id_reconcile
            elif bank_ref_id and movement_id:
                money_fund_req_id =  self.env['money.funds.request.returns'].search([('bank_ref', '=', bank_ref_id.id)])
                money_fund_cancel_id =  self.env['money.funds.cancel'].search([('bank_ref', '=', bank_ref_id.id)])
                account_return_id = self.env['account.returns'].search([('bank_ref', '=', bank_ref_id.id)])
                if money_fund_req_id:
                    
                    record.state = 'reconciled'
                    money_fund_req_id.state = 'recived_deposit'
                    record.movement_id = movement_id.id
                    movement_id.conciliation_state = 'reconciled'
                    self.env['money.funds.request.returns']._generate_returns_account_moves(money_fund_req_id)
                elif money_fund_cancel_id:
                    if record.income_bank_journal_id==self.env['bank.reference.conf'].get_param('journal'):
                        record.state = 'reconciled'
                        money_fund_cancel_id.state = 'recived_deposit'
                        money_fund_cancel_id.money_fund.state='cancelled'
                        record.movement_id = movement_id.id
                        movement_id.conciliation_state = 'reconciled'
                        self.env['money.funds.cancel']._gen_account_moves(money_fund_cancel_id)
                    else:
                        record.state = 'unreconciled'
                        movement_id.conciliation_state = 'unreconciled'
                elif account_return_id:
                    record.state = 'reconciled'
                    record.movement_id = movement_id.id
                    movement_id.conciliation_state = 'reconciled'
                    account_return_id.return_paid(record)
                else:
                    record.state = 'unreconciled'
                    movement_id.conciliation_state = 'unreconciled'

            else:
                record.state = 'unreconciled'
                movement_id.conciliation_state = 'unreconciled'

class BankConciliationLine(models.Model):

    _name = 'bank.conciliation.line'
    _desription = 'Bank conciliation line'

    movement_date = fields.Date('Movement date')
    # Referencia Bancaria
    ref = fields.Char("Bank Reference")
    # Referencia Cliente
    customer_ref = fields.Char("Customer Reference")
    # Referencias complementarias
    reference_plugin_1 = fields.Char('Reference Plugin 1')
    reference_plugin_2 = fields.Char("Reference plugin 2")
    reference_plugin_3 = fields.Char("Reference plugin 3")
    # Descripción del movimiento
    description = fields.Char('Description of movement')
    invoice_id = fields.Many2one('account.move', "Related invoice/payment")
    comission_id = fields.Many2one('comission.profit', "Related comision")
    #move_line_ids = fields.One2many('account.move.line', 'bank_movement_id', string="Accounting Notes")
    donations_id = fields.Many2one('donations', "Related donation")
    payment_id = fields.Many2one('account.payment', "Related payment")
    amount = fields.Float("Amount")
    charge_amount = fields.Float("Charge amount")
    bank_conciliation_id = fields.Many2one('bank.conciliation',string='Conciliation',ondelete='cascade')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('reconciled', 'Reconciled'),
        ('unreconciled', 'Unreconciled')
    ], "State", default="draft")
    movement_id = fields.Many2one('bank.movements', "Related movement")

    def search_movement(self):
        for record in self:
            if record.amount > 0.0:
                journal_id = record.bank_conciliation_id.journal_id.id
                if record.bank_conciliation_id.journal_id.bank_id.l10n_mx_edi_code in ('012', '014', '072', '044'):
                    movement_id = self.env['bank.movements'].search([('movement_date', '=', record.movement_date), ('amount_interest', '=', record.amount), 
                        ('income_bank_journal_id', '=', journal_id), ('ref', '=', record.ref), ('customer_ref', '=', record.customer_ref), ('reference_plugin_1', '=', record.reference_plugin_1),
                        ('reference_plugin_2', '=', record.reference_plugin_2), ('reference_plugin_3', '=', record.reference_plugin_3)])
                elif record.bank_conciliation_id.journal_id.bank_id.l10n_mx_edi_code == '002':
                    ref_com_1 = record.reference_plugin_1.split()
                    movement_id = self.env['bank.movements'].search([('movement_date', '=', record.movement_date), ('amount_interest', '=', record.amount), 
                        ('income_bank_journal_id', '=', journal_id), ('ref', 'in', record.ref_com_1[0])])
                elif record.bank_conciliation_id.journal_id.bank_id.l10n_mx_edi_code in ('036', '021'):
                    movement_id = self.env['bank.movements'].search([('movement_date', '=', record.movement_date), ('amount_interest', '=', record.amount), 
                        ('income_bank_journal_id', '=', journal_id), ('reference_plugin_1', '=', record.reference_plugin_1)])
                else:
                    movement_id = False
            elif record.charge_amount > 0.0:
                journal_id = record.bank_conciliation_id.journal_id.id
                if record.bank_conciliation_id.journal_id.bank_id.l10n_mx_edi_code in ('012', '014', '072', '044'):
                    movement_id = self.env['bank.movements'].search([('movement_date', '=', record.movement_date), ('charge_amount', '=', record.charge_amount), 
                        ('income_bank_journal_id', '=', journal_id), ('ref', '=', record.ref), ('customer_ref', '=', record.customer_ref), ('reference_plugin_1', '=', record.reference_plugin_1),
                        ('reference_plugin_2', '=', record.reference_plugin_2), ('reference_plugin_3', '=', record.reference_plugin_3)])
                elif record.bank_conciliation_id.journal_id.bank_id.l10n_mx_edi_code == '002':
                    ref_com_1 = record.reference_plugin_1.split()
                    movement_id = self.env['bank.movements'].search([('movement_date', '=', record.movement_date), ('charge_amount', '=', record.charge_amount), 
                        ('income_bank_journal_id', '=', journal_id), ('ref', 'in', record.ref_com_1[0])])
                elif record.bank_conciliation_id.journal_id.bank_id.l10n_mx_edi_code in ('036', '021'):
                    movement_id = self.env['bank.movements'].search([('movement_date', '=', record.movement_date), ('charge_amount', '=', record.charge_amount), 
                        ('income_bank_journal_id', '=', journal_id), ('reference_plugin_1', '=', record.reference_plugin_1)])
                else:
                    movement_id = False
        return movement_id