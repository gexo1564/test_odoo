from odoo import models, fields, api, _
from odoo.exceptions import RedirectWarning, UserError, ValidationError, AccessError
from datetime import datetime, timedelta, date
import time
import logging

class AccountTopayDocuments(models.TransientModel):
	_name = 'account.topay.approve'
	_description = 'Account Topay Approvation'

	#Función para aceptar el estado de la cuenta por pagar a aprobar
	def approve_account_topay(self):
		model = self._context.get('active_model')
		account_to_pay = self.env[model].browse(self._context.get('active_id'))
		account_to_pay.sudo().approve()
		self.email_approve_format(account_to_pay.secretary_admin,account_to_pay.secretary_admin_email,account_to_pay.name)

	def reject_account_topay(self):
		logging.critical("Reject info")
		logging.critical(self.env.context['current_account_topay_id'])
		state="rejected"
		return {
			'name': _('Motivos de Rechazo'),
			'type': 'ir.actions.act_window',
			'res_model': 'account.topay.rejection',
			'view_mode': 'form',
			'view_type': 'form',
			'views': [(False, 'form')],
			'context': {'account_topay_id': self.env.context['current_account_topay_id'],'next_state':state},
			'target': 'new',
		}

		#Enviar correo sección
	def email_approve_format(self,name,emails,id):
		
		subject = 'Aprobación de Cuenta por Pagar'
		body = "<p>La solicitud de creación de la cuenta por pagar fue revisada y aprobada para su uso.</p></br><p> Nombre asignado en el sistema: {info}</p>".format(info=id)

		self.send_approve_email(emails,name,subject,body)

		#Envio de correo con respecto al rechazo de cuenta por pagar
	def send_approve_email(self,email_delivery,name_delivery,subject,body):
		self.env.context = dict(self.env.context)
		self.env.context.update({
				'is_email' : True
			})

		base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
		template_obj = self.env['mail.template'].sudo().search([('name','=','mail_template_bac')], limit=1)
		aux_body = template_obj.body_html
		aux_body = aux_body.replace('--subject--',subject)
		aux_body = aux_body.replace('--summary--',body)
		aux_body = aux_body.replace('--base_url--',base_url)
		name_to = ""

		root_email = self.env['res.users'].search([('name','=','Admin correo sidia')])
		if not root_email:
			raise UserError('No se encuentra al usuario de correo admin')

		author_id = self.env['res.partner'].search([('id','=',root_email.partner_id.id)])
		logging.critical(author_id)

		mail_pool = self.env['mail.mail']
		values = {
						'subject' : subject,
						'email_to' : "",
						'email_from' : author_id.email,
						'author_id':author_id.id,
						'reply_to' : 'Favor de no responder este mensaje.',
						'body_html' : "",
						'body' : body,
			}
		
		if email_delivery:
			aux_mails=email_delivery.split(',')
			for mail in aux_mails:
				values['email_to'] = str(mail)
				if name_delivery:
					name_to = name_delivery

				aux_body = aux_body.replace('--name_to--',name_to)
				values['body_html'] = aux_body

				msg_id = mail_pool.create(values)

				if msg_id:
					mail_pool.sudo().send([msg_id])
					logging.critical("***** Correo electrónico enviado a " + values['email_to'])

				else:
					raise UserError('No se creó el mensaje')
		else:
			raise UserError('No se encontraron correos del secretari@')

	