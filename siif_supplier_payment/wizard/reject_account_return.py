from odoo import models, fields, api, _
import logging
logger = logging.getLogger(__name__)


class AccountReturnRejection(models.TransientModel):
    
    _name = 'account.return.rejection'
    _description = 'Account Return Rejection'
    reasons = fields.Text(string='Reasons')
    
    @api.model
    def create(self,vals):
        res = super(AccountReturnRejection, self).create(vals)
        return res

    #Función para asociar el motivo de rechazo y la descripción del rechazo de la solicitud de incremento y ministración
    def rejected(self):
        acc_return=self.env['account.returns']
        info = acc_return.search([('id','=',self.env.context['acc_return_info'])])
        if info:
            if self.env.context['type']=='rejection':
                info.write({'observations':self.reasons,'state':'rejected'})
                if acc_return.search_count([('account_move_id','=',info.account_move_id.id),('state','=','done')])==0:
                    info.account_move_id.write({'payment_state':'paid'})
                else:
                    info.account_move_id.write({'payment_state':'with_returns'})
                
                acc_return.notify_request(info,'reject')
            elif self.env.context['type']=='correction':
                info.write({'observations':self.reasons,'state':'reject_correction'})
                acc_return.notify_request(info,'doc')
                info.feu_signature_dep.unlink()