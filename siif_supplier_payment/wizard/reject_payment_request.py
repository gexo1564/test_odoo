from odoo import models, fields,_,api
from odoo.exceptions import UserError, ValidationError
from datetime import datetime, timedelta
import logging
logger = logging.getLogger(__name__)

class RejectPaymentRequest(models.TransientModel):

    _name = 'reject.payment.request'

    move_id = fields.Many2one('account.move', string="Payment Request")
    reason_for_rejection = fields.Text('Reason For Rejection')
    state = fields.Char('State Reject')

    def action_reject(self):
        self.move_id.reject_payment_request(self.reason_for_rejection, self.state)