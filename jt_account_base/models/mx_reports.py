from odoo import _, api, fields, models

GROUP_ACCOUNT_ADMIN = 'jt_account_base.group_account_admin_user'
GROUP_ACCOUNT_TRAINING = 'jt_account_base.group_account_training_user'
GROUP_ACCOUNT_REGISTER = 'jt_account_base.group_account_register_user'
EXPORT_SAT_XML = 'Exportar para SAT (XML)'

class MxReportPartnerLedger(models.AbstractModel):
    _inherit = "l10n_mx.account.diot"

    def _get_reports_buttons(self):
        buttons = super()._get_reports_buttons()
        is_admin_contabilidad = self.env.user.has_group(GROUP_ACCOUNT_ADMIN)
        is_training_contabilidad = self.env.user.has_group(GROUP_ACCOUNT_TRAINING)
        is_registro_contabilidad = self.env.user.has_group(GROUP_ACCOUNT_REGISTER)
        if not (is_admin_contabilidad or is_training_contabilidad or is_registro_contabilidad):
            buttons_to_hide = ["Guardar", "Imprimir DIOT (TXT)", "Print DPIVA (TXT)"]
            buttons = filter(
                lambda x: x['name'] not in buttons_to_hide, buttons
            )
        return buttons

class MxReportAccountTrial(models.AbstractModel):
    _inherit = "l10n_mx.trial.report"

    def _get_reports_buttons(self):
        buttons = super()._get_reports_buttons()
        is_admin_contabilidad = self.env.user.has_group(GROUP_ACCOUNT_ADMIN)
        is_training_contabilidad = self.env.user.has_group(GROUP_ACCOUNT_TRAINING)
        if not (is_admin_contabilidad or is_training_contabilidad):
            buttons_to_hide = ["Guardar", EXPORT_SAT_XML]
            buttons = filter(
                lambda x: x['name'] not in buttons_to_hide, buttons
            )
        return buttons

class MXReportAccountCoa(models.AbstractModel):
    _inherit = "l10n_mx.coa.report"

    def _get_reports_buttons(self):
        buttons = super()._get_reports_buttons()
        is_admin_contabilidad = self.env.user.has_group(GROUP_ACCOUNT_ADMIN)
        is_training_contabilidad = self.env.user.has_group(GROUP_ACCOUNT_TRAINING)
        if not (is_admin_contabilidad or is_training_contabilidad):
            buttons_to_hide = ["Guardar", EXPORT_SAT_XML]
            buttons = filter(
                lambda x: x['name'] not in buttons_to_hide, buttons
            )
        return buttons

class MxClosingReportAccountTrial(models.AbstractModel):
    _inherit = "l10n_mx.trial.closing.report"

    def _get_reports_buttons(self):
        buttons = super()._get_reports_buttons()
        is_admin_contabilidad = self.env.user.has_group(GROUP_ACCOUNT_ADMIN)
        is_training_contabilidad = self.env.user.has_group(GROUP_ACCOUNT_TRAINING)
        is_registro_contabilidad = self.env.user.has_group(GROUP_ACCOUNT_REGISTER)
        if not (is_admin_contabilidad or is_training_contabilidad):
            buttons = [b for b in buttons if b['name'] != 'Guardar']
        if not (is_admin_contabilidad or is_training_contabilidad or is_registro_contabilidad):
            buttons_to_hide = [EXPORT_SAT_XML]
            buttons = filter(
                lambda x: x['name'] not in buttons_to_hide, buttons
            )
        return buttons