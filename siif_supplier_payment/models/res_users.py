# -*- coding: utf-8 -*-
from odoo import models, fields, api

class ResUsers(models.Model):
    _inherit = 'res.users'

    # Permisos de un usuario una dependencia
    dependency_payment_ids = fields.Many2many(
        'dependency',
        'allowed_users_dep_payment_rel',
        string = "User Permissions for Dependencies"
    )
    all_dep_subdep_payment = fields.Boolean(string='All Dependencies selected')
    # Permisos de un usuario una subdependencia
    sub_dependency_payment_ids = fields.Many2many(
        'sub.dependency',
        'allowed_users_sd_payment_rel',
        string = "User Permissions for Sub Dependencies"
    )
    # Permisos de un usuario a una UPA
    permission_upa_payment = fields.Many2many('policy.keys','allowed_users_upa_payment_rel', string="UPA Keys")
    all_upa_sel_payment = fields.Boolean(string='All UPA selected')

    # Asignación de Todos las Dependencias
    @api.onchange('all_dep_subdep_payment')
    def _all_deps_selected_payment(self):
        if self.all_dep_subdep_payment:
            dependency_ids = self.env['dependency'].search([])
            self.dependency_payment_ids=[(6, 0, dependency_ids.ids)]
        else:
            self.dependency_payment_ids=[(6, 0, [])]

    # Asignación de todas las UPA
    @api.onchange('all_upa_sel_payment')
    def _all_upa_selected_payment(self):
        if self.all_upa_sel_payment:
            upa = self.env['policy.keys'].search([])
            self.permission_upa_payment = [(6, 0, upa.ids)]
        else:
            self.permission_upa_payment=[(6, 0, [])]