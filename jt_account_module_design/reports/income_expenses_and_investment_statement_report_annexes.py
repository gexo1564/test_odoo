# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2023 UNAM.
#    Author: SIIF UNAM
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


import io
import base64
import json
from datetime import datetime
import pytz
from psycopg2 import sql
import psycopg2

from odoo import models, api, _
from odoo.tools.misc import xlsxwriter
from odoo.exceptions import ValidationError

DATE_FORMAT = '%Y-%m-%d'
CURRENCY_FORMAT = '#,##0'
TEXT_HEADER_FORMAT_RIGHT = 'text-right text-dark'
TEXT_HEADER_FORMAT_LEFT = 'text-left text-dark'

# Annexes ID
SUBSIDY_FEDERAL_ANNEXE_ID = 'jt_account_module_design.income_expenses_investment_statement_federal_subsidy'
FINANCIAL_PRODUCTS_ANNEXE_ID = 'jt_account_module_design.income_expenses_investment_statement_financial_products'
EXTRAORDINARY_INCOME_ANNEXE_ID = 'jt_account_module_design.income_expenses_investment_statement_extraordinary_income'
EDUCATION_SERVICES_ANNEXE_ID = 'jt_account_module_design.income_expenses_investment_statement_education_services'
OTHER_INCOME_ANNEXE_ID = 'jt_account_module_design.income_expenses_investment_statement_other_income'
RETIREMENT_AND_SEPARATION_GRATIFICATION = 'jt_account_module_design.income_expenses_investment_statement_gratification_separation_retirement'
OTHER_SOCIAL_BENEFITS = 'jt_account_module_design.income_expenses_investment_statement_other_social_benefits'
REMUNERATION_ACADEMIC_PERSONAL_ANNEXE_ID = 'jt_account_module_design.income_expenses_investment_statement_remuneration_academic_personal'
REMUNERATION_ADMINISTRATIVE_PERSONAL_ANNEXE_ID = 'jt_account_module_design.income_expenses_investment_statement_remuneration_academic_administrative'
BONUS_VACATION_ANNEXE_ID = 'jt_account_module_design.income_expenses_investment_statement_bonus_vacation'
GRATIFICATION_SEPARATION_RETIREMENT_ANNEXE_ID = 'jt_account_module_design.income_expenses_investment_statement_gratification_separation_retirement'
SOCIAL_SECURITY_FEES_ANNEXE_ID = 'jt_account_module_design.income_expenses_investment_statement_social_security_fees'
OTHER_SOCIAL_BENEFITS_ANNEXE_ID = 'jt_account_module_design.income_expenses_investment_statement_other_social_benefits'
SCOLARSHIP_ANNEXE_ID = 'jt_account_module_design.income_expenses_investment_statement_scolarship'
MATERIALS_ANNEXE_ID = 'jt_account_module_design.income_expenses_investment_statement_materials'
MAINTENANCE_ANNEXE_ID = 'jt_account_module_design.income_expenses_investment_statement_maintenance'
ELECTRICITY_ANNEXE_ID = 'jt_account_module_design.income_expenses_investment_statement_electricity'
FIELD_WORK_ANNEXE_ID = 'jt_account_module_design.income_expenses_investment_statement_field_work'
RENT_ANNEXE_ID = 'jt_account_module_design.income_expenses_investment_statement_rent'
EDITIONS_ANNEXE_ID = 'jt_account_module_design.income_expenses_investment_statement_editions'
MEMBERSHIP_FEES_ANNEXE_ID = 'jt_account_module_design.income_expenses_investment_statement_membership_fees'
IMPORTATION_EXPENSES_ANNEXE_ID = 'jt_account_module_design.income_expenses_investment_statement_importation_expenses'
ADVISORY_SERVICES_ANNEXE_ID = 'jt_account_module_design.income_expenses_investment_statement_advisory_services'
SUPPORT_PROGRAMS_ANNEXE_ID = 'jt_account_module_design.income_expenses_investment_statement_support_programs'
EQUIPMENT_ANNEXE_ID = 'jt_account_module_design.income_expenses_investment_statement_equipment'
CONSTRUCTION_ANNEXE_ID = 'jt_account_module_design.income_expenses_investment_statement_construction'
BOOKS_ANNEXE_ID = 'jt_account_module_design.income_expenses_investment_statement_books'



class IncomeExpensesInvestmentStatementReportAnnexes(models.AbstractModel):

    _name = "income.expenses.and.investment.statement.report.annexes"
    _inherit = "account.report"
    _description = "Income, Expenses and Investment Statement Report Budget with Annexes"

    filter_date = {'mode': 'single', 'filter': 'this_year'}
    filter_all_entries = None
    filter_journals = None
    filter_unfold_all = None
    filter_cash_basis = None
    filter_hierarchy = None
    MAX_LINES = None
    filter_levels = None

    # Annexes total amounts
    totals_dict_federal_subsidy = {}
    totals_dict_financial_products = {}
    totals_dict_extraordinary_income = {}
    totals_dict_education_services = {}
    totals_dict_other_income = {}
    totals_dict_own_income = {}

    def _get_reports_buttons(self):
        return [
            {'name': _('Export (XLSX)'),
             'sequence': 1,
             'action': 'print_xlsx',
             'file_export_type': _('XLSX')
             },
        ]

    def print_xlsx(self, options, extra_data):
        options['is_xlsx_report'] = True
        return {
                'type': 'ir_actions_account_report_download',
                'data': {'model': self.env.context.get('model'),
                         'options': json.dumps(options),
                         'output_format': 'xlsx',
                         'financial_id': self.env.context.get('id'),
                         }
                }

    @api.model
    def _get_columns_name(self, options):
        columns = [
            {'name': _('ANNEXES'), 'class': TEXT_HEADER_FORMAT_LEFT},
            {'name': _('CONCEPT'), 'class': TEXT_HEADER_FORMAT_LEFT},
            {'name': _('ANNUAL ASSIGNED'), 'class': TEXT_HEADER_FORMAT_RIGHT},
            {'name': _('TRANSFERS'), 'class': TEXT_HEADER_FORMAT_RIGHT},
            {'name': _('ANNUAL AUTHORIZED ')+options['date'].get('date_to'), 'class': TEXT_HEADER_FORMAT_RIGHT},
            {'name': _('SHOP'), 'class': TEXT_HEADER_FORMAT_RIGHT},
            {'name': _('TOTAL EXERCISED ')+options['date'].get('date_to'), 'class': TEXT_HEADER_FORMAT_RIGHT},
            {'name': _('EXTRAORDINARY INCOME'), 'class': TEXT_HEADER_FORMAT_RIGHT},
            {'name': _('EXERCISED ')+options['date'].get('date_to'), 'class': TEXT_HEADER_FORMAT_RIGHT},
            {'name': _('%'), 'class': TEXT_HEADER_FORMAT_RIGHT},
            {'name': _('TO EXECUTE'), 'class': TEXT_HEADER_FORMAT_RIGHT},   
        ]
        return columns

    @api.model
    def _get_lines(self, options, line_id=None):
        # Groups that have access to the report
        list_groups = [
            'jt_account_base.group_account_admin_user',
            'jt_account_base.group_account_super_admin_user',
            'jt_account_base.group_account_guest_user',
            'jt_account_base.group_account_approval_user']
        
        self.env['security.tools'].check_user_permissions(list_groups)         

        lines = []

        category_name = 'A'
        annexe_name = 'INGRESOS'
        level = 0
        first_column_name = category_name
        lines.append(self.get_category_dict_column(category_name, annexe_name, level, False, first_column_name))
        
        ## Subsidio Federal
        
        category_name = 'B'
        annexe_name = self._get_annexes_name(SUBSIDY_FEDERAL_ANNEXE_ID)
        parent_category_name = 'A'
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))

        self.totals_dict_federal_subsidy.update(self._add_accounts_to_lines(lines, options, annexe_name, category_name))
        
        ## Total 
        concept_name = 'SUBSIDIO'
        parent_category_name = 'A'
        level = 0
        second_column_name = concept_name
        text_align = 'text-right'

        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, self.totals_dict_federal_subsidy, level, second_column_name, text_align))

        ## Productos Financieros 
        category_name = 'C'
        annexe_name = self._get_annexes_name(FINANCIAL_PRODUCTS_ANNEXE_ID)
        parent_category_name = 'A'
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        self.totals_dict_financial_products.update(self._add_accounts_to_lines(lines, options, annexe_name, category_name))
        
        ## Ingresos Extroardinarios  
        category_name = 'D'
        annexe_name = self._get_annexes_name(EXTRAORDINARY_INCOME_ANNEXE_ID)
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        self.totals_dict_extraordinary_income.update(self._add_accounts_to_lines(lines, options, annexe_name, category_name))
        
        ## Ingresos por servicios de educación  
        category_name = 'E'
        annexe_name = self._get_annexes_name(EDUCATION_SERVICES_ANNEXE_ID)
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        self.totals_dict_education_services.update(self._add_accounts_to_lines(lines, options, annexe_name, category_name))
        
        ## Otros ingresos
        category_name = 'F'
        annexe_name = self._get_annexes_name(OTHER_INCOME_ANNEXE_ID)
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        self.totals_dict_other_income.update(self._add_accounts_to_lines(lines, options, annexe_name, category_name))
        

        ## Total INGRESOS PROPIOS 
        concept_name = 'INGRESOS PROPIOS'
        parent_category_name = 'A'
 
        level = 0
        second_column_name = concept_name
       
        text_align = 'text-right'

        
        total_annual_assigned = self.totals_dict_financial_products['total_annual_assigned'] + self.totals_dict_extraordinary_income['total_annual_assigned'] + self.totals_dict_education_services['total_annual_assigned'] + self.totals_dict_other_income['total_annual_assigned']
        total_transfers = self.totals_dict_financial_products['total_transfers'] + self.totals_dict_extraordinary_income['total_transfers'] + self.totals_dict_education_services['total_transfers'] + self.totals_dict_other_income['total_transfers']
        total_annual_authorized = self.totals_dict_financial_products['total_annual_authorized'] + self.totals_dict_extraordinary_income['total_annual_authorized'] + self.totals_dict_education_services['total_annual_authorized'] + self.totals_dict_other_income['total_annual_authorized']
        total_shop = 0
        total_account_exercised = self.totals_dict_financial_products['total_account_exercised'] + self.totals_dict_extraordinary_income['total_account_exercised'] + self.totals_dict_education_services['total_account_exercised'] + self.totals_dict_other_income['total_account_exercised']
        total_extraordinary_income = self.totals_dict_financial_products['total_extraordinary_income'] + self.totals_dict_extraordinary_income['total_extraordinary_income'] + self.totals_dict_education_services['total_extraordinary_income'] + self.totals_dict_other_income['total_extraordinary_income']
        total_exercised = self.totals_dict_financial_products['total_exercised'] + self.totals_dict_extraordinary_income['total_exercised'] + self.totals_dict_education_services['total_exercised'] + self.totals_dict_other_income['total_exercised']
        total_percent = self.get_percent_exercised(total_account_exercised,total_annual_authorized)
        total_to_execute = self.totals_dict_financial_products['total_to_execute'] + self.totals_dict_extraordinary_income['total_to_execute'] + self.totals_dict_education_services['total_to_execute'] + self.totals_dict_other_income['total_to_execute']
        total_last_year_account_exercised = self.totals_dict_financial_products['last_year_total_account_exercised'] + self.totals_dict_extraordinary_income['last_year_total_account_exercised'] + self.totals_dict_education_services['last_year_total_account_exercised'] + self.totals_dict_other_income['last_year_total_account_exercised']

        self.totals_dict_own_income.update({
            'total_annual_assigned': total_annual_assigned,
            'total_transfers': total_transfers,
            'total_annual_authorized': total_annual_authorized,
            'total_shop': total_shop,
            'total_account_exercised': total_account_exercised,
            'total_extraordinary_income': total_extraordinary_income,
            'total_exercised': total_exercised,
            'total_percent': total_percent,
            'total_to_execute': total_to_execute,
            'last_year_total_account_exercised': total_last_year_account_exercised,
            'category_name': concept_name
        })

        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, self.totals_dict_own_income, level, second_column_name, text_align))
        
        ## TOTAL INGRESOS
        concept_name = 'TOTAL INGRESOS'
        level = 0
        second_column_name = concept_name
        text_align = 'text-right'

        
        total_annual_assigned = self.totals_dict_own_income['total_annual_assigned'] + self.totals_dict_federal_subsidy['total_annual_assigned']
        total_transfers = self.totals_dict_own_income['total_transfers'] + self.totals_dict_federal_subsidy['total_transfers']
        total_annual_authorized = self.totals_dict_own_income['total_annual_authorized'] + self.totals_dict_federal_subsidy['total_annual_authorized']
        total_shop = 0
        total_account_exercised =  self.totals_dict_own_income['total_account_exercised'] + self.totals_dict_federal_subsidy['total_account_exercised']
        total_extraordinary_income = self.totals_dict_own_income['total_extraordinary_income'] + self.totals_dict_federal_subsidy['total_extraordinary_income']
        total_exercised = self.totals_dict_own_income['total_exercised'] + self.totals_dict_federal_subsidy['total_exercised']
        total_percent = self.get_percent_exercised(total_account_exercised,total_annual_authorized)
        total_to_execute = self.totals_dict_own_income['total_to_execute'] + self.totals_dict_federal_subsidy['total_to_execute']
    
        totals_dict_income = {
            'total_annual_assigned': total_annual_assigned,
            'total_transfers': total_transfers,
            'total_annual_authorized': total_annual_authorized,
            'total_shop': total_shop,
            'total_account_exercised': total_account_exercised,
            'total_extraordinary_income': total_extraordinary_income,
            'total_exercised': total_exercised,
            'total_percent': total_percent,
            'total_to_execute': total_to_execute,
        }

        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, totals_dict_income, level, second_column_name, text_align))
        
        category_name = 'gastos_operacion'
        annexe_name = 'GASTOS DE OPERACIÓN'

        level = 0
        lines.append(self.get_category_dict_column(category_name, annexe_name, level))
        
        category_name = 'remuneraciones_personales'
        annexe_name = 'Remuneraciones Personales y Prestaciones'
        parent_category_name = 'gastos_operacion'
        level = 0
        lines.append(self.get_category_dict_column(category_name, annexe_name, level, parent_category_name))

        ## Remuneraciones al personal académico
        category_name = 'G'
        parent_category_name = 'remuneraciones_personales'
        annexe_name = self._get_annexes_name(REMUNERATION_ACADEMIC_PERSONAL_ANNEXE_ID)
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        totals_dict_remuneration_academic_personal = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
     
        ## Remuneraciones al personal administrativo
        category_name = 'H'
        annexe_name = self._get_annexes_name(REMUNERATION_ADMINISTRATIVE_PERSONAL_ANNEXE_ID)
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        totals_dict_remuneration_academic_administrative = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        
        ## Aguinaldo y prima vacacional
        category_name = 'I'
        annexe_name = self._get_annexes_name(BONUS_VACATION_ANNEXE_ID)
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        totals_dict_statement_bonus_vacation = self._add_accounts_to_lines(lines, options, annexe_name, category_name) 
        
        ## Gratificaciones por separación y jubilación e indemnizaciones por defunción
        category_name = 'J'
        annexe_name = self._get_annexes_name(GRATIFICATION_SEPARATION_RETIREMENT_ANNEXE_ID)
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        totals_dict_gratification_separation_retirement = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        
        ## Cuotas de seguridad social
        category_name = 'K'
        annexe_name = self._get_annexes_name(SOCIAL_SECURITY_FEES_ANNEXE_ID)
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        totals_dict_social_security_fees = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        

        ## Otras prestaciones sociales
        category_name = 'L'
        annexe_name = self._get_annexes_name(OTHER_SOCIAL_BENEFITS_ANNEXE_ID)
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        totals_dict_other_social_benefits = self._add_accounts_to_lines(lines, options, annexe_name, category_name)

        ## TOTAL 
        concept_name = 'remunerations_total'
        
        level = 1
        
        total_annual_assigned = totals_dict_remuneration_academic_personal['total_annual_assigned'] + totals_dict_remuneration_academic_administrative['total_annual_assigned'] + totals_dict_statement_bonus_vacation['total_annual_assigned'] + totals_dict_gratification_separation_retirement['total_annual_assigned'] + totals_dict_social_security_fees['total_annual_assigned'] + totals_dict_other_social_benefits['total_annual_assigned']
        total_transfers = totals_dict_remuneration_academic_personal['total_transfers'] + totals_dict_remuneration_academic_administrative['total_transfers'] + totals_dict_statement_bonus_vacation['total_transfers'] + totals_dict_gratification_separation_retirement['total_transfers'] + totals_dict_social_security_fees['total_transfers'] + totals_dict_other_social_benefits['total_transfers']
        total_annual_authorized = totals_dict_remuneration_academic_personal['total_annual_authorized'] + totals_dict_remuneration_academic_administrative['total_annual_authorized'] + totals_dict_statement_bonus_vacation['total_annual_authorized'] + totals_dict_gratification_separation_retirement['total_annual_authorized'] + totals_dict_social_security_fees['total_annual_authorized'] + totals_dict_other_social_benefits['total_annual_authorized']
        total_shop = 0
        total_account_exercised =  totals_dict_remuneration_academic_personal['total_account_exercised'] + totals_dict_remuneration_academic_administrative['total_account_exercised'] + totals_dict_statement_bonus_vacation['total_account_exercised'] + totals_dict_gratification_separation_retirement['total_account_exercised'] + totals_dict_social_security_fees['total_account_exercised'] + totals_dict_other_social_benefits['total_account_exercised']
        total_extraordinary_income = totals_dict_remuneration_academic_personal['total_extraordinary_income'] + totals_dict_remuneration_academic_administrative['total_extraordinary_income'] + totals_dict_statement_bonus_vacation['total_extraordinary_income'] + totals_dict_gratification_separation_retirement['total_extraordinary_income'] + totals_dict_social_security_fees['total_extraordinary_income'] + totals_dict_other_social_benefits['total_extraordinary_income']
        total_exercised = totals_dict_remuneration_academic_personal['total_exercised'] + totals_dict_remuneration_academic_administrative['total_exercised'] + totals_dict_statement_bonus_vacation['total_exercised'] + totals_dict_gratification_separation_retirement['total_exercised'] + totals_dict_social_security_fees['total_exercised'] + totals_dict_other_social_benefits['total_exercised']
        total_percent = self.get_percent_exercised(total_account_exercised,total_annual_authorized)
        total_to_execute = totals_dict_remuneration_academic_personal['total_to_execute'] + totals_dict_remuneration_academic_administrative['total_to_execute'] + totals_dict_statement_bonus_vacation['total_to_execute'] + totals_dict_gratification_separation_retirement['total_to_execute'] + totals_dict_social_security_fees['total_to_execute'] + totals_dict_other_social_benefits['total_to_execute']
    
        totals_dict_remunerations = {
            'total_annual_assigned': total_annual_assigned,
            'total_transfers': total_transfers,
            'total_annual_authorized': total_annual_authorized,
            'total_shop': total_shop,
            'total_account_exercised': total_account_exercised,
            'total_extraordinary_income': total_extraordinary_income,
            'total_exercised': total_exercised,
            'total_percent': total_percent,
            'total_to_execute': total_to_execute,
        }

        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, totals_dict_remunerations, level))

        ## Becas
        category_name = 'M'
        parent_category_name = 'gastos_operacion'
        annexe_name = self._get_annexes_name(SCOLARSHIP_ANNEXE_ID)
        level = 0
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        totals_dict_statement_scolarship = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        

        ## Erogaciones para sedes en el extranjero
        category_name = 'N'
        annexe_name = self._get_annexes_name('jt_account_module_design.income_expenses_investment_statement_foreign_expenses')
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        totals_dict_foreign_expenses = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        category_name = 'services_articles_materials'
        annexe_name = 'Servicios, Artículos y Materiales de Consumo'
        level = 0
        lines.append(self.get_category_dict_column(category_name, annexe_name, level, parent_category_name))
        
        ## Materiales, refacciones, herramientas y accesorios didácticos y de investigación
        category_name = 'O'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name(MATERIALS_ANNEXE_ID)
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        totals_dict_statement_materials = self._add_accounts_to_lines(lines, options, annexe_name, category_name) 
        
        ## Servicios y materiales de mantenimiento para edificios, equipos e instalaciones
        category_name = 'P'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name(MAINTENANCE_ANNEXE_ID)
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        totals_dict_maintenance = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        
        ## Energía eléctrica y servicios de comunicación
        category_name = 'Q'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name(ELECTRICITY_ANNEXE_ID)
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        totals_dict_electricity= self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        
        ## Trabajos de campo, prácticas escolares, paisajes y viáticos
        category_name = 'R'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name(FIELD_WORK_ANNEXE_ID)
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        totals_dict_field_work = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        
        ## Arrendamiento de muebles y equipo
        category_name = 'S'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name(RENT_ANNEXE_ID)
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        totals_dict_statement_rent = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        
        ## Ediciones de libros y revistas
        category_name = 'T'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name(EDITIONS_ANNEXE_ID)
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        totals_dict_statement_editions = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        

        ## Cuotas de afiliación a sociedades e instituciones científicas, culturales y deportivas; simposios, congresos y seminarios
        category_name = 'U'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name(MEMBERSHIP_FEES_ANNEXE_ID)
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        totals_dict_membership_fees  = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        

        ## Gastos y derechos de importación, seguros y fianzas
        category_name = 'V'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name(IMPORTATION_EXPENSES_ANNEXE_ID)
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        totals_dict_importation_expenses = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        
        ## Servicios por asesorías para operación de programas
        category_name = 'W'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name(ADVISORY_SERVICES_ANNEXE_ID)
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        totals_dict_advisory_services = self._add_accounts_to_lines(lines, options, annexe_name, category_name)

        ## Apoyo a programas de extensión y colaboración académica y científica
        category_name = 'X'
        parent_category_name = 'services_articles_materials'
        annexe_name = self._get_annexes_name(SUPPORT_PROGRAMS_ANNEXE_ID)
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        totals_dict_support_programs = self._add_accounts_to_lines(lines, options, annexe_name, category_name)

        ## TOTAL 
        concept_name = 'services_total'

        level = 1

        total_annual_assigned = totals_dict_statement_materials['total_annual_assigned'] + totals_dict_maintenance['total_annual_assigned'] + totals_dict_electricity['total_annual_assigned'] + totals_dict_field_work['total_annual_assigned'] + totals_dict_statement_rent['total_annual_assigned'] + totals_dict_statement_editions['total_annual_assigned'] + totals_dict_membership_fees['total_annual_assigned'] + totals_dict_importation_expenses['total_annual_assigned'] + totals_dict_advisory_services['total_annual_assigned'] +  totals_dict_support_programs['total_annual_assigned']
        total_transfers = totals_dict_statement_materials['total_transfers'] + totals_dict_maintenance['total_transfers'] + totals_dict_electricity['total_transfers'] + totals_dict_field_work['total_transfers'] + totals_dict_statement_rent['total_transfers'] + totals_dict_statement_editions['total_transfers'] + totals_dict_membership_fees['total_transfers'] + totals_dict_importation_expenses['total_transfers'] + totals_dict_advisory_services['total_transfers'] +  totals_dict_support_programs['total_transfers']
        total_annual_authorized = totals_dict_statement_materials['total_annual_authorized'] + totals_dict_maintenance['total_annual_authorized'] + totals_dict_electricity['total_annual_authorized'] + totals_dict_field_work['total_annual_authorized'] + totals_dict_statement_rent['total_annual_authorized'] + totals_dict_statement_editions['total_annual_authorized'] + totals_dict_membership_fees['total_annual_authorized'] + totals_dict_importation_expenses['total_annual_authorized'] + totals_dict_advisory_services['total_annual_authorized'] +  totals_dict_support_programs['total_annual_authorized']
        total_shop = 0
        total_account_exercised = totals_dict_statement_materials['total_account_exercised'] + totals_dict_maintenance['total_account_exercised'] + totals_dict_electricity['total_account_exercised'] + totals_dict_field_work['total_account_exercised'] + totals_dict_statement_rent['total_account_exercised'] + totals_dict_statement_editions['total_account_exercised'] + totals_dict_membership_fees['total_account_exercised'] + totals_dict_importation_expenses['total_account_exercised'] + totals_dict_advisory_services['total_account_exercised'] +  totals_dict_support_programs['total_account_exercised']
        total_extraordinary_income = totals_dict_statement_materials['total_extraordinary_income'] + totals_dict_maintenance['total_extraordinary_income'] + totals_dict_electricity['total_extraordinary_income'] + totals_dict_field_work['total_extraordinary_income'] + totals_dict_statement_rent['total_extraordinary_income'] + totals_dict_statement_editions['total_extraordinary_income'] + totals_dict_membership_fees['total_extraordinary_income'] + totals_dict_importation_expenses['total_extraordinary_income'] + totals_dict_advisory_services['total_extraordinary_income'] +  totals_dict_support_programs['total_extraordinary_income']
        total_exercised = totals_dict_statement_materials['total_exercised'] + totals_dict_maintenance['total_exercised'] + totals_dict_electricity['total_exercised'] + totals_dict_field_work['total_exercised'] + totals_dict_statement_rent['total_exercised'] + totals_dict_statement_editions['total_exercised'] + totals_dict_membership_fees['total_exercised'] + totals_dict_importation_expenses['total_exercised'] + totals_dict_advisory_services['total_exercised'] +  totals_dict_support_programs['total_exercised']
        total_percent = self.get_percent_exercised(total_account_exercised,total_annual_authorized)
        total_to_execute = totals_dict_statement_materials['total_to_execute'] + totals_dict_maintenance['total_to_execute'] + totals_dict_electricity['total_to_execute'] + totals_dict_field_work['total_to_execute'] + totals_dict_statement_rent['total_to_execute'] + totals_dict_statement_editions['total_to_execute'] + totals_dict_membership_fees['total_to_execute'] + totals_dict_importation_expenses['total_to_execute'] + totals_dict_advisory_services['total_to_execute'] +  totals_dict_support_programs['total_to_execute']
        
        totals_dict_services_total = {
            'total_annual_assigned': total_annual_assigned,
            'total_transfers': total_transfers,
            'total_annual_authorized': total_annual_authorized,
            'total_shop': total_shop,
            'total_account_exercised': total_account_exercised,
            'total_extraordinary_income': total_extraordinary_income,
            'total_exercised': total_exercised,
            'total_percent': total_percent,
            'total_to_execute': total_to_execute,
        }

        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, totals_dict_services_total, level))
        
        ## TOTAL OPERACION
        concept_name = 'operating_costs'

        level = 0
        second_column_name = 'GASTOS DE OPERACIÓN'
        text_align = 'text-right'

        total_annual_assigned = totals_dict_remunerations['total_annual_assigned'] + totals_dict_statement_scolarship['total_annual_assigned'] + totals_dict_foreign_expenses['total_annual_assigned'] + totals_dict_services_total['total_annual_assigned']
        total_transfers =  totals_dict_remunerations['total_transfers'] + totals_dict_statement_scolarship['total_transfers'] + totals_dict_foreign_expenses['total_transfers'] + totals_dict_services_total['total_transfers']
        total_annual_authorized = totals_dict_remunerations['total_annual_authorized'] + totals_dict_statement_scolarship['total_annual_authorized'] + totals_dict_foreign_expenses['total_annual_authorized'] + totals_dict_services_total['total_annual_authorized']
        total_shop = 0
        total_account_exercised = totals_dict_remunerations['total_account_exercised'] + totals_dict_statement_scolarship['total_account_exercised'] + totals_dict_foreign_expenses['total_account_exercised'] + totals_dict_services_total['total_account_exercised']
        total_extraordinary_income = totals_dict_remunerations['total_extraordinary_income'] + totals_dict_statement_scolarship['total_extraordinary_income'] + totals_dict_foreign_expenses['total_extraordinary_income'] + totals_dict_services_total['total_extraordinary_income']
        total_exercised = totals_dict_remunerations['total_exercised'] + totals_dict_statement_scolarship['total_exercised'] + totals_dict_foreign_expenses['total_exercised'] + totals_dict_services_total['total_exercised']
        total_percent = self.get_percent_exercised(total_account_exercised,total_annual_authorized)
        total_to_execute = totals_dict_remunerations['total_to_execute'] + totals_dict_statement_scolarship['total_to_execute'] + totals_dict_foreign_expenses['total_to_execute'] + totals_dict_services_total['total_to_execute']
        
        totals_dict_operating_costs = {
            'total_annual_assigned': total_annual_assigned,
            'total_transfers': total_transfers,
            'total_annual_authorized': total_annual_authorized,
            'total_shop': total_shop,
            'total_account_exercised': total_account_exercised,
            'total_extraordinary_income': total_extraordinary_income,
            'total_exercised': total_exercised,
            'total_percent': total_percent,
            'total_to_execute': total_to_execute,
        }

        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, totals_dict_operating_costs, level, second_column_name, text_align))
        
        ## TOTAL REMANENTE
        concept_name = 'remaining_investment'

        total_annual_assigned = totals_dict_income['total_annual_assigned'] - totals_dict_operating_costs['total_annual_assigned'] 
        total_transfers =  totals_dict_income['total_transfers'] - totals_dict_operating_costs['total_transfers'] 
        total_annual_authorized = totals_dict_income['total_annual_authorized'] - totals_dict_operating_costs['total_annual_authorized'] 
        total_shop = 0
        total_account_exercised = totals_dict_income['total_account_exercised'] - totals_dict_operating_costs['total_account_exercised'] 
        total_extraordinary_income = totals_dict_income['total_extraordinary_income'] - totals_dict_operating_costs['total_extraordinary_income'] 
        total_exercised = totals_dict_income['total_exercised'] - totals_dict_operating_costs['total_exercised']
        total_percent = self.get_percent_exercised(total_account_exercised,total_annual_authorized)
        total_to_execute = totals_dict_income['total_to_execute'] - totals_dict_operating_costs['total_to_execute'] 
        
        totals_dict_remaining_investment = {
            'total_annual_assigned': total_annual_assigned,
            'total_transfers': total_transfers,
            'total_annual_authorized': total_annual_authorized,
            'total_shop': total_shop,
            'total_account_exercised': total_account_exercised,
            'total_extraordinary_income': total_extraordinary_income,
            'total_exercised': total_exercised,
            'total_percent': total_percent,
            'total_to_execute': total_to_execute,
        }
        level = 0
        second_column_name = 'REMANENTE ANTES DE INVERSIONES'
        text_align = 'text-right'
        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name,totals_dict_remaining_investment, level, second_column_name, text_align))

        category_name = 'properties_equipment'
        annexe_name = 'Inversiones en Propiedades y Equipo'
        level = 0
        lines.append(self.get_category_dict_column(category_name, annexe_name, level))
        
        ## Equipo, mobiliario e instrumentos científicos y didácticos
        category_name = 'Y'
        parent_category_name = 'properties_equipment'
        annexe_name = self._get_annexes_name(EQUIPMENT_ANNEXE_ID)
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        totals_dict_statement_equipment = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        

        # Construcciones, rehabilitaciones y remodelación de inmuebles
        category_name = 'Z'
        parent_category_name = 'properties_equipment'
        annexe_name = self._get_annexes_name(CONSTRUCTION_ANNEXE_ID)
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        totals_dict_statement_construction = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        

        ## Libros y revistas académicas y científicas
        category_name = 'AA'
        parent_category_name = 'properties_equipment'
        annexe_name = self._get_annexes_name(BOOKS_ANNEXE_ID)
        level = 1
        lines.append(self.get_annexe_dict_colum(category_name, parent_category_name, annexe_name, level))
        totals_dict_statement_books = self._add_accounts_to_lines(lines, options, annexe_name, category_name)
        
        ## TOTAL INVERSIONES
        concept_name = 'investments_propertie_equipment'
        
        level = 0
        second_column_name = 'INVERSIONES'
        text_align = 'text-right'

        total_annual_assigned = totals_dict_statement_equipment['total_annual_assigned'] + totals_dict_statement_construction['total_annual_assigned'] + totals_dict_statement_books['total_annual_assigned']
        total_transfers =  totals_dict_statement_equipment['total_transfers'] + totals_dict_statement_construction['total_transfers'] + totals_dict_statement_books['total_transfers']
        total_annual_authorized = totals_dict_statement_equipment['total_annual_authorized'] + totals_dict_statement_construction['total_annual_authorized'] + totals_dict_statement_books['total_annual_authorized']
        total_shop = 0
        total_account_exercised = totals_dict_statement_equipment['total_account_exercised'] + totals_dict_statement_construction['total_account_exercised'] + totals_dict_statement_books['total_account_exercised']
        total_extraordinary_income = totals_dict_statement_equipment['total_extraordinary_income'] + totals_dict_statement_construction['total_extraordinary_income'] + totals_dict_statement_books['total_extraordinary_income']
        total_exercised = totals_dict_statement_equipment['total_exercised'] + totals_dict_statement_construction['total_exercised'] + totals_dict_statement_books['total_exercised']
        total_percent = self.get_percent_exercised(total_account_exercised,total_annual_authorized)
        total_to_execute = totals_dict_statement_equipment['total_to_execute'] + totals_dict_statement_construction['total_to_execute'] + totals_dict_statement_books['total_to_execute']
        
        totals_dict_investments_propertie_equipment = {
            'total_annual_assigned': total_annual_assigned,
            'total_transfers': total_transfers,
            'total_annual_authorized': total_annual_authorized,
            'total_shop': total_shop,
            'total_account_exercised': total_account_exercised,
            'total_extraordinary_income': total_extraordinary_income,
            'total_exercised': total_exercised,
            'total_percent': total_percent,
            'total_to_execute': total_to_execute,
        }

        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, totals_dict_investments_propertie_equipment, level, second_column_name, text_align))

        # EROGACIONES CON INGRESOS EXTRAORDINARIOS
        concept_name = 'income_expenses'
        date_to = options['date'].get('date_to')
        date_from = self.get_first_and_last_day_of_year(date_to)[0]
        
        level = 1
        second_column_name = 'Erogaciones con Ingresos Extraordinarios'
        
        date_transfer = self._get_date_formated(date_to)
        total_annual_assigned = self.get_income_expenses_investment_statement(date_from)
        total_transfers =  self.get_transfers_income_expenses_investment_statement(date_transfer)
        total_annual_authorized = total_annual_assigned + total_transfers
        total_shop = 0
        total_account_exercised = 0
        total_extraordinary_income = totals_dict_remaining_investment['total_extraordinary_income'] - totals_dict_investments_propertie_equipment['total_extraordinary_income']
        total_exercised = total_account_exercised - total_extraordinary_income
        total_percent = self.get_percent_exercised(total_annual_authorized,total_annual_assigned)
        total_to_execute = total_annual_assigned + total_transfers

        totals_dict_income_expenses = {
            'total_annual_assigned': total_annual_assigned,
            'total_transfers': total_transfers,
            'total_annual_authorized': total_annual_authorized,
            'total_shop': total_shop,
            'total_account_exercised': total_account_exercised,
            'total_extraordinary_income': total_extraordinary_income,
            'total_exercised': total_exercised,
            'total_percent': total_percent,
            'total_to_execute': total_to_execute,
        }

        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, totals_dict_income_expenses, level, second_column_name))
        
        concept_name = 'costs'
        level = 1

        total_annual_assigned = totals_dict_operating_costs['total_annual_assigned'] + totals_dict_investments_propertie_equipment['total_annual_assigned'] + totals_dict_income_expenses['total_annual_assigned']
        total_transfers =  totals_dict_operating_costs['total_transfers'] + totals_dict_investments_propertie_equipment['total_transfers'] + totals_dict_income_expenses['total_transfers']
        total_annual_authorized = total_annual_assigned + total_transfers
        total_shop = 0
        total_account_exercised = totals_dict_operating_costs['total_account_exercised'] + totals_dict_investments_propertie_equipment['total_account_exercised'] + totals_dict_income_expenses['total_account_exercised']
        total_extraordinary_income = 0
        total_exercised = totals_dict_operating_costs['total_exercised'] + totals_dict_investments_propertie_equipment['total_exercised'] + totals_dict_income_expenses['total_exercised']
        total_percent = self.get_percent_exercised(total_account_exercised,total_annual_authorized)
        total_to_execute = total_annual_authorized - total_exercised

        totals_dict_costs = {
            'total_annual_assigned': total_annual_assigned,
            'total_transfers': total_transfers,
            'total_annual_authorized': total_annual_authorized,
            'total_shop': total_shop,
            'total_account_exercised': total_account_exercised,
            'total_extraordinary_income': total_extraordinary_income,
            'total_exercised': total_exercised,
            'total_percent': total_percent,
            'total_to_execute': total_to_execute,
        }

        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, totals_dict_costs, level))

        
        
        ## TOTAL REMANENTE        
        concept_name = 'profit'
        
        level = 0
        second_column_name = 'REMANENTE DEL EJERCICIO'
        text_align = 'text-right'

        total_annual_assigned = totals_dict_income['total_annual_assigned'] - totals_dict_costs['total_annual_assigned']
        total_transfers =  totals_dict_income['total_transfers'] - totals_dict_costs['total_transfers']
        total_annual_authorized = totals_dict_income['total_annual_authorized'] - totals_dict_costs['total_annual_authorized']
        total_shop = 0
        total_account_exercised = totals_dict_income['total_account_exercised'] - totals_dict_costs['total_account_exercised']
        total_extraordinary_income = 0
        total_exercised = totals_dict_income['total_exercised'] - totals_dict_costs['total_exercised'] 
        total_percent = 0
        total_to_execute = total_annual_authorized - total_exercised

        totals_dict_profit = {
            'total_annual_assigned': total_annual_assigned,
            'total_transfers': total_transfers,
            'total_annual_authorized': total_annual_authorized,
            'total_shop': total_shop,
            'total_account_exercised': total_account_exercised,
            'total_extraordinary_income': total_extraordinary_income,
            'total_exercised': total_exercised,
            'total_percent': total_percent,
            'total_to_execute': total_to_execute,
        }

        lines.append(self.get_total_annexe_dict_column(concept_name, parent_category_name, totals_dict_profit, level, second_column_name, text_align))
        
        totals_dict_annexes_to_update = {
            'B': self.totals_dict_federal_subsidy,
            'C': self.totals_dict_financial_products,
            'D': self.totals_dict_extraordinary_income,
            'E': self.totals_dict_education_services,
            'F': self.totals_dict_other_income,
            'G': totals_dict_remuneration_academic_personal,
            'H': totals_dict_remuneration_academic_administrative,
            'I': totals_dict_statement_bonus_vacation,
            'J': totals_dict_gratification_separation_retirement,
            'K': totals_dict_social_security_fees,
            'L': totals_dict_other_social_benefits,
            'M': totals_dict_statement_scolarship,
            'N': totals_dict_foreign_expenses,
            'O': totals_dict_statement_materials,
            'P': totals_dict_maintenance,
            'Q': totals_dict_electricity,
            'R': totals_dict_field_work,
            'S': totals_dict_statement_rent,
            'T': totals_dict_statement_editions,
            'U': totals_dict_membership_fees,
            'V': totals_dict_importation_expenses,
            'W': totals_dict_advisory_services,
            'X': totals_dict_support_programs,
            'Y': totals_dict_statement_equipment,
            'Z': totals_dict_statement_construction,
            'AA': totals_dict_statement_books,
        }

        self._update_total_for_category(lines, totals_dict_annexes_to_update)     
   
        return lines


    @api.model
    def _get_report_name(self):
        return _("Income, Expenses and Investment Statement Report Budget with Annexes")

    def get_month_name(self, month):
        month_name = {
            1 : 'Enero',
            2 : 'Febrero',
            3 : 'Marzo',
            4 : 'Abril',
            5 : 'Mayo',
            6 : 'Junio',
            7 : 'Julio',
            8 : 'Agosto',
            9 : 'Septiembre',
            10 : 'Octubre',
            11 : 'Noviembre',
            12 : 'Diciembre'
        }
        return month_name.get(month)

    def get_xlsx(self, options, response=None): 
        # Groups that have access to export report
        list_groups = [
            'jt_account_base.group_account_admin_user',
            'jt_account_base.group_account_super_admin_user',
            'jt_account_base.group_account_guest_user',
            'jt_account_base.group_account_approval_user']
        
        self.env['security.tools'].check_user_permissions(list_groups)  

        options['xlsx'] = True

        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        sheet = workbook.add_worksheet(self._get_report_name()[:31])
        style_dict = self._get_xlsx_styles(workbook)
        
        annexes = (self.env.ref(SUBSIDY_FEDERAL_ANNEXE_ID), 
                self.env.ref(FINANCIAL_PRODUCTS_ANNEXE_ID),
                self.env.ref(EXTRAORDINARY_INCOME_ANNEXE_ID),
                self.env.ref(EDUCATION_SERVICES_ANNEXE_ID),
                self.env.ref(OTHER_INCOME_ANNEXE_ID),
                self.env.ref(RETIREMENT_AND_SEPARATION_GRATIFICATION),
                self.env.ref(OTHER_SOCIAL_BENEFITS),)

        """
        letter = 'J-'
        for conf in annexes[5:]:
            cont = 1
            for item in conf.accounts_ids:                
                self._generate_annex(workbook, letter + str(cont), 'GASTOS', item, options, style_dict, item.name, top = True)
                cont += 1
            letter = 'L-'
        """
        total_annexes_a = (self.totals_dict_federal_subsidy, 
                self.totals_dict_financial_products,
                self.totals_dict_extraordinary_income,
                self.totals_dict_education_services,
                self.totals_dict_other_income, )
        
        total_annexes_a1 = (self.totals_dict_federal_subsidy, 
                            self.totals_dict_own_income, )
        
        total_annexes_a1_1 = (self.totals_dict_financial_products,
                self.totals_dict_extraordinary_income,
                self.totals_dict_education_services,
                self.totals_dict_other_income, )
        
        total_annexes_b = (self.totals_dict_federal_subsidy, )

        category_name_income = 'INGRESOS'
        self._generate_annex(workbook, 'A', category_name_income, total_annexes_a, options, style_dict)
        self._generate_annex(workbook, 'A-1', category_name_income, total_annexes_a1, options, style_dict)
        self._generate_annex(workbook, 'A-1.1', category_name_income, total_annexes_a1_1, options, style_dict)
        self._generate_annex(workbook, 'B', category_name_income, total_annexes_b, options, style_dict)
        self._generate_annex(workbook, 'C', category_name_income, self.env.ref(FINANCIAL_PRODUCTS_ANNEXE_ID), options, style_dict, self.env.ref(FINANCIAL_PRODUCTS_ANNEXE_ID).name)
        self._generate_annex(workbook, 'D', category_name_income, self.env.ref(EXTRAORDINARY_INCOME_ANNEXE_ID), options, style_dict, self.env.ref(EXTRAORDINARY_INCOME_ANNEXE_ID).name)
        self._generate_annex(workbook, 'E', category_name_income, self.env.ref(EDUCATION_SERVICES_ANNEXE_ID), options, style_dict, self.env.ref(EDUCATION_SERVICES_ANNEXE_ID).name)
        self._generate_annex(workbook, 'F', category_name_income, self.env.ref(OTHER_INCOME_ANNEXE_ID), options, style_dict, self.env.ref(OTHER_INCOME_ANNEXE_ID).name)
        category_name_expenses = 'GASTOS'
        annex_subtitle = 'REMUNERACIONES PERSONALES Y PRESTACIONES'
        self._generate_annex(workbook, 'G', category_name_expenses, self.env.ref(REMUNERATION_ACADEMIC_PERSONAL_ANNEXE_ID), options, style_dict, annex_subtitle, self.env.ref(REMUNERATION_ACADEMIC_PERSONAL_ANNEXE_ID).name)
        self._generate_annex(workbook, 'G-1', category_name_expenses, self.env.ref(REMUNERATION_ACADEMIC_PERSONAL_ANNEXE_ID), options, style_dict, annex_subtitle, self.env.ref(REMUNERATION_ACADEMIC_PERSONAL_ANNEXE_ID).name)
        self._generate_annex(workbook, 'H', category_name_expenses, self.env.ref(REMUNERATION_ADMINISTRATIVE_PERSONAL_ANNEXE_ID), options, style_dict, annex_subtitle, self.env.ref(REMUNERATION_ADMINISTRATIVE_PERSONAL_ANNEXE_ID).name)
        self._generate_annex(workbook, 'H-1', category_name_expenses, self.env.ref(REMUNERATION_ADMINISTRATIVE_PERSONAL_ANNEXE_ID), options, style_dict, annex_subtitle, self.env.ref(REMUNERATION_ADMINISTRATIVE_PERSONAL_ANNEXE_ID).name)
        self._generate_annex(workbook, 'I', category_name_expenses, self.env.ref(BONUS_VACATION_ANNEXE_ID), options, style_dict, annex_subtitle, self.env.ref(BONUS_VACATION_ANNEXE_ID).name)
        self._generate_annex(workbook, 'J', category_name_expenses, self.env.ref(GRATIFICATION_SEPARATION_RETIREMENT_ANNEXE_ID), options, style_dict, annex_subtitle, self.env.ref(GRATIFICATION_SEPARATION_RETIREMENT_ANNEXE_ID).name)
        cont = 1
        for item in self.env.ref(GRATIFICATION_SEPARATION_RETIREMENT_ANNEXE_ID).expenditure_item_ids:
            self._generate_annex(workbook, 'J-' + str(cont), category_name_expenses, item.unam_account_id, options, style_dict, item.unam_account_id.name, top = True)
            cont += 1
            
        self._generate_annex(workbook, 'K', category_name_expenses, self.env.ref(SOCIAL_SECURITY_FEES_ANNEXE_ID), options, style_dict, annex_subtitle, self.env.ref(SOCIAL_SECURITY_FEES_ANNEXE_ID).name)
        self._generate_annex(workbook, 'L', category_name_expenses, self.env.ref(OTHER_SOCIAL_BENEFITS_ANNEXE_ID), options, style_dict, annex_subtitle, self.env.ref(OTHER_SOCIAL_BENEFITS_ANNEXE_ID).name)
        cont = 1
        for item in self.env.ref(OTHER_SOCIAL_BENEFITS_ANNEXE_ID).expenditure_item_ids:
            self._generate_annex(workbook, 'L-' + str(cont), category_name_expenses, item.unam_account_id, options, style_dict, item.unam_account_id.name, top = True)
            cont += 1
        
        annex_subtitle = ''
        self._generate_annex(workbook, 'M', category_name_expenses, self.env.ref(SCOLARSHIP_ANNEXE_ID), options, style_dict, annex_subtitle, self.env.ref(SCOLARSHIP_ANNEXE_ID).name)
        cont = 1
        for item in self.env.ref(SCOLARSHIP_ANNEXE_ID).expenditure_item_ids:
            self._generate_annex(workbook, 'M-' + str(cont), category_name_expenses, item.unam_account_id, options, style_dict, item.unam_account_id.name, top = True)
            cont += 1

        annex_subtitle = 'SERVICIOS, ARTÍCULOS Y MATERIALES DE CONSUMO'
        self._generate_annex(workbook, 'O', category_name_expenses, self.env.ref(MATERIALS_ANNEXE_ID), options, style_dict, annex_subtitle, self.env.ref(MATERIALS_ANNEXE_ID).name)
        self._generate_annex(workbook, 'P', category_name_expenses, self.env.ref(MAINTENANCE_ANNEXE_ID), options, style_dict, annex_subtitle, self.env.ref(MAINTENANCE_ANNEXE_ID).name)
        self._generate_annex(workbook, 'Q', category_name_expenses, self.env.ref(ELECTRICITY_ANNEXE_ID), options, style_dict, annex_subtitle, self.env.ref(ELECTRICITY_ANNEXE_ID).name)
        self._generate_annex(workbook, 'R', category_name_expenses, self.env.ref(FIELD_WORK_ANNEXE_ID), options, style_dict, annex_subtitle, self.env.ref(FIELD_WORK_ANNEXE_ID).name)
        self._generate_annex(workbook, 'S', category_name_expenses, self.env.ref(RENT_ANNEXE_ID), options, style_dict, annex_subtitle, self.env.ref(RENT_ANNEXE_ID).name)
        self._generate_annex(workbook, 'T', category_name_expenses, self.env.ref(EDITIONS_ANNEXE_ID), options, style_dict, annex_subtitle, self.env.ref(EDITIONS_ANNEXE_ID).name)
        self._generate_annex(workbook, 'U', category_name_expenses, self.env.ref(MEMBERSHIP_FEES_ANNEXE_ID), options, style_dict, annex_subtitle, self.env.ref(MEMBERSHIP_FEES_ANNEXE_ID).name)
        self._generate_annex(workbook, 'V', category_name_expenses, self.env.ref(IMPORTATION_EXPENSES_ANNEXE_ID), options, style_dict, annex_subtitle, self.env.ref(IMPORTATION_EXPENSES_ANNEXE_ID).name)
        self._generate_annex(workbook, 'W', category_name_expenses, self.env.ref(ADVISORY_SERVICES_ANNEXE_ID), options, style_dict, annex_subtitle, self.env.ref(ADVISORY_SERVICES_ANNEXE_ID).name)
        self._generate_annex(workbook, 'X', category_name_expenses, self.env.ref(SUPPORT_PROGRAMS_ANNEXE_ID), options, style_dict, annex_subtitle, self.env.ref(SUPPORT_PROGRAMS_ANNEXE_ID).name)

        annex_subtitle = 'INVERSIONES EN PROPIEDADES Y EQUIPO'
        self._generate_annex(workbook, 'Y', category_name_expenses, self.env.ref(EQUIPMENT_ANNEXE_ID), options, style_dict, annex_subtitle, self.env.ref(EQUIPMENT_ANNEXE_ID).name)
        self._generate_annex(workbook, 'Z', category_name_expenses, self.env.ref(CONSTRUCTION_ANNEXE_ID), options, style_dict, annex_subtitle, self.env.ref(CONSTRUCTION_ANNEXE_ID).name)
        self._generate_annex(workbook, 'AA', category_name_expenses, self.env.ref(BOOKS_ANNEXE_ID), options, style_dict, annex_subtitle, self.env.ref(BOOKS_ANNEXE_ID).name)

        # Get XLSX Styles
        super_col_style = style_dict['super_col_style']
        title_style = style_dict['title_style']
        date_default_col1_style = style_dict['date_default_col1_style']
        date_default_style = style_dict['date_default_style']
        
        # Set borders style
        style_dict['currect_date_style'].set_border(0)
        super_col_style.set_border(0)
        
        # Set columns width
        sheet.set_column(0, 0, 25)
        sheet.set_column(1, 1, 60)
        sheet.set_column(2, 2, 30)
        sheet.set_column(3, 3, 20)
        sheet.set_column(4, 4, 35)
        sheet.set_column(5, 5, 10)
        sheet.set_column(6, 6, 35)
        sheet.set_column(7, 7, 35)
        sheet.set_column(8, 8, 30)
        sheet.set_column(9, 9, 10)
        sheet.set_column(10, 10, 25)
        
        y_offset = 0
        col = 0
        
        sheet.merge_range(y_offset, col, 7, col, '', super_col_style)
        if self.env.user and self.env.user.company_id and self.env.user.company_id.header_logo:
            filename = 'logo.png'
            image_data = io.BytesIO(base64.standard_b64decode(self.env.user.company_id.header_logo))
            sheet.insert_image(0,0, filename, {'image_data': image_data,'x_offset':60,'y_offset':15,'x_scale':0.6,'y_scale':0.6})
        
        col += 1
        x_offset = 1

        tz = pytz.timezone('America/Mexico_City')
        actual_date = datetime.now(tz)
        currect_time_msg = "Fecha y hora de impresión: "
        month_name = self.get_month_name(actual_date.month).upper()
        currect_time_msg += str(actual_date.day) + " de " + str(month_name) + " del " + str(actual_date.year) + str(actual_date.strftime(" %I:%M:%S %p"))
        user_msg = " Reporte generado por: " + self.env.user.name
        sheet.merge_range(y_offset, x_offset, y_offset, x_offset+9, '', super_col_style)
        sheet.merge_range(y_offset+1, x_offset, y_offset+1, x_offset+9, 'UNIVERSIDAD NACIONAL AUTÓNOMA DE MÉXICO', super_col_style)
        sheet.merge_range(y_offset+2, x_offset, y_offset+2, x_offset+9, 'PATRONATO UNIVERSITARIO', super_col_style)
        sheet.merge_range(y_offset+3, x_offset, y_offset+3, x_offset+9, 'TESORERÍA', super_col_style)
        sheet.merge_range(y_offset+4, x_offset, y_offset+4, x_offset+9, self._get_report_name().upper(), super_col_style)
        sheet.merge_range(y_offset+5, x_offset, y_offset+5, x_offset+9, 'Cifras en Miles de Pesos', super_col_style)
        sheet.merge_range(y_offset+6, x_offset, y_offset+6, x_offset+9, currect_time_msg, super_col_style)
        sheet.merge_range(y_offset+7, x_offset, y_offset+7, x_offset+9, user_msg, super_col_style)
        y_offset += 10
  
        for row in self.get_header(options):
            x = 0
            for column in row:
                colspan = column.get('colspan', 1)
                header_label = column.get('name', '').replace('<br/>', ' ').replace('&nbsp;', ' ')

                if colspan == 1:
                    sheet.write(y_offset, x, header_label, title_style)
                x += colspan
            y_offset += 1
        ctx = self._set_context(options)
        ctx.update({'no_format':True, 'print_mode':True, 'prefetch_fields': False})

        # deactivating the prefetching saves ~35% on get_lines running time
        lines = self.with_context(ctx)._get_lines(options)
        if options.get('hierarchy'):
            lines = self._create_hierarchy(lines, options)
        if options.get('selected_column'):
            lines = self._sort_lines(lines, options)    

        self.write_all_data_rows_xlsx(lines, y_offset, style_dict, sheet, date_default_col1_style, date_default_style)        

        workbook.close()
        output.seek(0)
        generated_file = output.read()
        output.close()
        return generated_file    

    def _get_accounts_from_detailed_report_configuration(self, name):
        
        query = """
            select
                account_account_id
            from
                account_account_detailed_report_configuration_rel
            where
                detailed_report_configuration_id = (
                select
                    id
                from
                    detailed_report_configuration
                where
                    name = %(name)s)
        
        """
        params = {'name' : name}


        self.env.cr.execute(query, params)
        result = self.env.cr.fetchall()

        result_list = []

        for rec in result :
            result_list.append(rec[0])

        return result_list

    def _add_accounts_to_lines (self, lines, options, category_name, parent_name):
        accounts_list  =  self.env['account.account'].browse(self._get_accounts_from_detailed_report_configuration(category_name))

        total_annual_assigned = 0
        total_transfers = 0
        total_annual_authorized = 0
        total_account_exercised = 0
        total_extraordinary_income = 0
        total_exercised = 0
        last_year_total_account_exercised = 0

        list_cri = []

        for account in accounts_list:
            actual_date = options['date'].get('date_to')
            annual_assigned_amount = 0
            last_year_date = self.get_last_year_date(actual_date)
            
            account_exercised_amount, account_type = self._verify_account_type(account.code, self._get_account_total_amount(actual_date, account.id))
            total_account_exercised += account_exercised_amount

            last_year_account_exercised_amount = self._verify_account_type(account.code, self._get_account_total_amount(last_year_date, account.id))[0]
            last_year_total_account_exercised += last_year_account_exercised_amount

            transfers_amount = 0
            
            if account_type == 'CRI':
                annual_assigned_amount, cri_id, transfers_amount = self._get_cri_total_amount(actual_date, account.id)
                result = self.verify_unique_element_in_list(list_cri, cri_id)
                if result:
                    total_annual_assigned += annual_assigned_amount
                    annual_authorized_amount = annual_assigned_amount + transfers_amount
                    total_annual_authorized += annual_authorized_amount
                    total_transfers += transfers_amount
            elif account_type == 'COG':
                annual_assigned_amount = self._get_cog_total_amount(actual_date, account.id, account.code)
                total_annual_assigned += annual_assigned_amount
                transfers_amount = self._get_transfer_amount_by_account(account.id, actual_date)
                annual_authorized_amount = annual_assigned_amount + transfers_amount
                total_annual_authorized += annual_authorized_amount
                total_transfers += transfers_amount
            else:
                annual_assigned_amount = 0
             
            extraordinary_income_amount = self._get_ext_inc_by_account(account.id, actual_date)
            total_extraordinary_income += extraordinary_income_amount   

            exercised_amount =  account_exercised_amount - extraordinary_income_amount 
            total_exercised += exercised_amount
            
            to_execute_amount = annual_authorized_amount - account_exercised_amount
            

            percent =  self.get_percent_exercised(account_exercised_amount,annual_authorized_amount)

            lines.append(
                {'id': 'column_' + str(account.id),
                'name': '',
                'level': 3,
                'parent_id': parent_name,
                'columns' : [
                    {
                        'id': 'account_' + str(account.id),
                        'name': account.code,
                        'level': 3,
                        'columns' : [],
                        'unfoldable': False,
                    },
                    {
                        'id': 'annual_assigned_amount' + str(account.id),
                        'name': self._get_rounded_formatted_value(annual_assigned_amount), 'class': 'number', 'raw_value': self._get_rounded_formatted_value(annual_assigned_amount),
                        'level': 3,
                        'columns' : [],
                        'unfoldable': False,
                    },
                    {
                        'id': 'transfers_amount' + str(account.id),
                        'name': self._get_rounded_formatted_value(transfers_amount), 'class': 'number', 'raw_value': self._get_rounded_formatted_value(transfers_amount),
                        'level': 3,
                        'columns' : [],
                        'unfoldable': False,
                    },
                    {
                        'id': 'annual_authorized_amount' + str(account.id),
                        'name': self._get_rounded_formatted_value(annual_authorized_amount), 'class': 'number', 'raw_value': self._get_rounded_formatted_value(annual_authorized_amount),
                        'level': 3,
                        'columns' : [],
                        'unfoldable': False,
                    },
                    {}, # Shop
                    {
                        'id': 'account_exercised_amount_' + str(account.id),
                        'name': self._get_rounded_formatted_value(account_exercised_amount), 'class': 'number', 'raw_value': self._get_rounded_formatted_value(account_exercised_amount),
                        'level': 3,
                        'columns' : [],
                        'unfoldable': False,
                    },
                    {
                        'id': 'extraordinary_income_amount_' + str(account.id),
                        'name': self._get_rounded_formatted_value(extraordinary_income_amount), 'class': 'number', 'raw_value': self._get_rounded_formatted_value(extraordinary_income_amount),
                        'level': 3,
                        'columns' : [],
                        'unfoldable': False,
                    },
                    {
                        'id': 'exercised_amount' + str(account.id),
                        'name': self._get_rounded_formatted_value(exercised_amount), 'class': 'number', 'raw_value': self._get_rounded_formatted_value(exercised_amount),
                        'level': 3,
                        'columns' : [],
                        'unfoldable': False,
                    },
                    {
                        'id': 'percent' + str(account.id),
                        'name': percent, 'class': 'number', 'raw_value': percent,
                        'level': 3,
                        'columns' : [],
                        'unfoldable': False,
                    },
                    {
                        'id': 'to_execute_amount' + str(account.id),
                        'name': self._get_rounded_formatted_value(to_execute_amount), 'class': 'number', 'raw_value': self._get_rounded_formatted_value(to_execute_amount),
                        'level': 3,
                        'columns' : [],
                        'unfoldable': False,
                    },

                ]})
            
        totals_dict = {
            'total_annual_assigned': total_annual_assigned,
            'total_transfers': total_transfers,
            'total_annual_authorized': total_annual_authorized,
            'total_shop': 0,
            'total_account_exercised': total_account_exercised,
            'total_extraordinary_income': total_extraordinary_income,
            'total_exercised': total_exercised,
            'total_percent': self.get_percent_exercised(total_account_exercised,total_annual_authorized),
            'total_to_execute': total_annual_authorized-total_account_exercised,
            'last_year_total_account_exercised': last_year_total_account_exercised,
            'category_name': category_name
            }
            
        return totals_dict
        
    def _get_dict_column(self, name, level):
        dict_column = {
            'id': 'category_' + name,
            'name': name,
            'level': level
        }
        return dict_column

    def _get_dict_number_format(self, amount, level):
        dict_column = {
            'name': amount, 
            'class': 'number',
            'level': level
        }
        return dict_column
    
    def _get_dict_number_rounded_thousand_format(self, amount, level):
        dict_column = {
            'name': self._get_rounded_formatted_value(amount), 
            'class': 'number',
            'level': level
        }
        return dict_column


    def _get_account_total_amount(self, date_to, account_id):
        
        date_from = self.get_first_and_last_day_of_year(date_to)[0]

        query = sql.SQL("""
                select
                    COALESCE(SUM(balance), 0) as sumatoria_apuntes
                from
                    account_move_line
                where
                    account_id = {}
                    and date >= {}
                    and date <= {}
                    and parent_state = 'posted';
            """).format(
                sql.Literal(account_id),
                sql.Literal(date_from),
                sql.Literal(date_to)
                )
            
        self.env.cr.execute(query)
        result = self.env.cr.fetchall()
        if result:
            return result[0][0]
        else:
            return 0.0
        
    def _get_cog_total_amount(self, date_to, account_id, account_code):
        date_from = self.get_first_and_last_day_of_year(date_to)[0]

        try:        
            query = sql.SQL("""
                select
	                COALESCE(SUM(authorized), 0) as sumatoria
	                from
	                    expenditure_budget_line
	                where
	                    item = (select item from expenditure_item where unam_account_id = {} and active = true)
                        and budget_line_yearly_id is null
	                    and start_date = {}
	                    and state = 'success'
                """).format(
                sql.Literal(account_id),
                sql.Literal(date_from)
                )

            self.env.cr.execute(query)
            result = self.env.cr.fetchall()
            if result:
                return result[0][0]
            else:
                return 0.0
        except psycopg2.Error:
            raise ValidationError(_("Verify that only one active Expense Item exists for the account %s in the Expense Item catalog") % (account_code))

    def _get_cri_total_amount(self, date_to, account_id):
        date_from = self.get_first_and_last_day_of_year(date_to)[0]

        query = sql.SQL("""
                select
                    ibl.estimated_total, estimated_cri_id, increases_total, decreases_total
                from
                    siif_budget_mgmt_income_budgets ib
                join siif_budget_mgmt_income_budgets_lines ibl on
                    ibl.my_id = ib.id
                where
                    ib.state = 'validated'
                    and ib.from_date = {}
                    and ibl.estimated_cri_id = (
                    select
                        cri
                    from
                        relation_account_cri rac
                    where
                        cuentas_id = {})
                """).format(
                sql.Literal(date_from),
                sql.Literal(account_id)
                )
            
        self.env.cr.execute(query)
        result = self.env.cr.fetchall()
        if result:
            estimated_total = result[0][0]
            cri_id = result[0][1]
            increase_total = result[0][2]
            decrease_total = result[0][3]
            transfer_total = (increase_total - decrease_total)
        else:
            estimated_total = 0.0
            cri_id = 0
            transfer_total = 0.0
        
        return estimated_total, cri_id, transfer_total
             
    def get_first_and_last_day_of_year(self, date):
        date = datetime.strptime(date, DATE_FORMAT)
        first_day = datetime(date.year, 1, 1)
        last_day = datetime(date.year, 12, 31)
        return first_day, last_day
    
    def get_last_year_date(self, date):
        date = datetime.strptime(date, DATE_FORMAT)
        if  date.month == 2 and date.day == 29:
            date = date.replace(day=date.day - 1)
        last_year_date = date.replace(year=date.year - 1)
        last_year_date = last_year_date.strftime(DATE_FORMAT)

        return last_year_date
    
    def convert_negative_number_to_absolute_with_parentheses(self,number):
        if number < 0:
            return f"({abs(number)})"
        else:
            return str(number)
                
    def _verify_account_type(self, account_code,amount):
        account_type =  ''
        if account_code.startswith('4'):
            amount = amount * -1
            account_type = 'CRI'
        elif account_code.startswith('5'):
            account_type = 'COG'
        return amount, account_type
    
    def _get_annexes_name(self, xml_id):
        annexe = self.env.ref(xml_id).name
        return annexe

    def _update_total_for_category(self, lines, total_dict_to_update):
        categorys = ['B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'O', 'P','Q','R','S','T','U','V','W','X','Y','Z','AA']
        level = 1 
        for line in lines:
            if line['id'] in categorys:
                line['columns'][1] = self._get_dict_number_rounded_thousand_format(total_dict_to_update[line['id']]['total_annual_assigned'], level) 
                line['columns'][2] = self._get_dict_number_rounded_thousand_format(total_dict_to_update[line['id']]['total_transfers'], level)
                line['columns'][3] = self._get_dict_number_rounded_thousand_format(total_dict_to_update[line['id']]['total_annual_authorized'], level)
                line['columns'][4] = self._get_dict_number_rounded_thousand_format(total_dict_to_update[line['id']]['total_shop'], level)
                line['columns'][5] = self._get_dict_number_rounded_thousand_format(total_dict_to_update[line['id']]['total_account_exercised'], level)
                line['columns'][6] = self._get_dict_number_rounded_thousand_format(total_dict_to_update[line['id']]['total_extraordinary_income'], level)
                line['columns'][7] = self._get_dict_number_rounded_thousand_format(total_dict_to_update[line['id']]['total_exercised'], level)
                line['columns'][8] = self._get_dict_number_format(total_dict_to_update[line['id']]['total_percent'], level)
                line['columns'][9] = self._get_dict_number_rounded_thousand_format(total_dict_to_update[line['id']]['total_to_execute'], level)
                
    def _get_rounded_formatted_value(self, value):
        round_value = round(value/1000)
        return round_value

    def get_annexe_dict_colum(self, category_name, parent_category_name, annexe_name, level, unfolded=False):
        dict_column = {
            'id': category_name,
            'name': category_name,
            'level': level,
            'columns' : [
                self._get_dict_column(annexe_name, level),
                self._get_dict_number_format(0, level),
                self._get_dict_number_format(0, level),
                self._get_dict_number_format(0, level),
                self._get_dict_number_format(0, level),
                self._get_dict_number_format(0, level),
                self._get_dict_number_format(0, level),
                self._get_dict_number_format(0, level),
                self._get_dict_number_format(0, level),
                self._get_dict_number_format(0, level),
                 ],
            'unfoldable': True,
            'parent_id': parent_category_name,
            'unfolded': unfolded,
            }
        return dict_column
                
    def get_total_annexe_dict_column(self, concept_name, parent_category_name, totals_dict, level, second_column_name='', text_align='text-left'):
        dict_total_column = {
            'id': 'total_' + concept_name,
            'name': '',
            'level': level,
            
            'columns' : [
                {
                    'id': 'total_amount' + concept_name,
                    'class': text_align,
                    'name': second_column_name,
                    'level': level,
                    'columns' : [],
                    'unfoldable': False,
                },
                {
                    'id': 'total_annual_assigned' + concept_name,
                    'name': self._get_rounded_formatted_value(totals_dict.get('total_annual_assigned', 0)), 'class': 'number',
                    'level': level,
                    'columns' : [],
                    'unfoldable': False,
                },
                {
                    'id': 'total_transfers' + concept_name,
                    'name': self._get_rounded_formatted_value(totals_dict.get('total_transfers', 0)), 'class': 'number',
                    'level': level,
                    'columns' : [],
                    'unfoldable': False,
                },
                {
                    'id': 'total_annual_authorized' + concept_name,
                    'name': self._get_rounded_formatted_value(totals_dict.get('total_annual_authorized', 0)), 'class': 'number',
                    'level': level,
                    'columns' : [],
                    'unfoldable': False,
                },
                {
                    'id': 'total_shop' + concept_name,
                    'name': self._get_rounded_formatted_value(totals_dict.get('total_shop', 0)), 'class': 'number',
                    'level': level,
                    'columns' : [],
                    'unfoldable': False,
                },
                {
                    'id': 'total_account_exercised' + concept_name,
                    'name': self._get_rounded_formatted_value(totals_dict.get('total_account_exercised', 0)), 'class': 'number',
                    'level': level,
                    'columns' : [],
                    'unfoldable': False,
                },
                {
                    'id': 'total_extraordinary_income' + concept_name,
                    'name': self._get_rounded_formatted_value(totals_dict.get('total_extraordinary_income', 0)), 'class': 'number',
                    'level': level,
                    'columns' : [],
                    'unfoldable': False,
                },
                {
                    'id': 'total_exercised' + concept_name,
                    'name': self._get_rounded_formatted_value(totals_dict.get('total_exercised', 0)), 'class': 'number',
                    'level': level,
                    'columns' : [],
                    'unfoldable': False,
                },
                {
                    'id': 'total_percent' + concept_name,
                    'name': totals_dict.get('total_percent', 0), 'class': 'number',
                    'level': level,
                    'columns' : [],
                    'unfoldable': False,
                },
                {
                    'id': 'total_to_execute' + concept_name,
                    'name': self._get_rounded_formatted_value(totals_dict.get('total_to_execute', 0)), 'class': 'number',
                    'level': level,
                    'columns' : [],
                    'unfoldable': False,
                },
                 ],
            'unfoldable': False,
            'parent_id': parent_category_name,
            }
        return dict_total_column
    
    def get_category_dict_column(self, category_name, annexe_name, level, parent_category_name=False, first_column_name=''):
        dict_column = {
            'id': category_name,
            'name':  first_column_name,
            'level': level,
            'columns' : [
                self._get_dict_column(annexe_name, level),
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                 ],
            'unfoldable': True,
            'unfolded': True,
            'parent_id': parent_category_name
            }
        
        return dict_column
    
    def _get_cog_(self, account_id):
        query = sql.SQL("""
                select item from expenditure_item where unam_account_id = {}
                """).format(
                sql.Literal(account_id)
                )
        self.env.cr.execute(query)
        result = self.env.cr.fetchall()
        
        if result:
            return result[0][0]
    
    def verify_unique_element_in_list(self, list, element):
        if element not in list:
            list.append(element)
            return True
        else :
            return False

    def get_percent_exercised(self, amount_account_exercised, amount_annual_authorized):
        if amount_annual_authorized == 0 or amount_account_exercised == 0:
            return 'N/A'
        else:
            percent = (amount_account_exercised * 100) / amount_annual_authorized
            return round(percent)


    def get_income_expenses_investment_statement(self,date_from, par='711'):
        query = sql.SQL("""
                select
	                COALESCE(SUM(authorized), 0) as sumatoria
	                from
	                    expenditure_budget_line
	                where
	                    item = {}
	                    and start_date = {}
                        and budget_line_yearly_id is null 
	                    and state = 'success'
                """).format(
                sql.Literal(par),
                sql.Literal(date_from)
                )
            
        self.env.cr.execute(query)
        result = self.env.cr.fetchall()
        if result:
            return result[0][0]
        else:
            return 0.0

    def get_transfers_income_expenses_investment_statement(self,date_to, par='711'):
        first_day = self.get_first_and_last_day_of_year(date_to)[0]
        query = sql.SQL("""
                select coalesce(SUM(coalesce(al.increase_type,0) - coalesce(al.decrease_type,0)),0)
                    from adequacies_lines al 
                    join adequacies a on a.id = al.adequacies_id 
                    join program_code pc on pc.id = al.program
                    where a.state='accepted' and a.date_of_budget_affected between {} and {}
                    and pc.item_id = (select id from expenditure_item where item = {} and active = true);
                """).format(
                sql.Literal(first_day),
                sql.Literal(date_to),
                sql.Literal(par)
                )
            
        self.env.cr.execute(query)
        result = self.env.cr.fetchall()
        if result:
            return result[0][0]
        else:
            return 0.0

    def _get_transfer_amount_by_account(self, acc_id, date):
        first_day, last_day = self.get_first_and_last_day_of_year(date)      
        query = sql.SQL("""
            SELECT 
                COALESCE(SUM(CASE WHEN al.line_type = 'increase' THEN al.amount ELSE 0 END), 0) -
                COALESCE(SUM(CASE WHEN al.line_type = 'decrease' THEN al.amount ELSE 0 END), 0) AS amount
            FROM adequacies_lines al
            JOIN adequacies a ON a.id = al.adequacies_id
            JOIN program_code pc ON pc.id = al.program
            WHERE 
                pc.item_id = (select id from expenditure_item where unam_account_id = {} and active = true)
                AND a.state = 'accepted'
                AND a.date_of_budget_affected BETWEEN {} AND {}
        """).format(
            sql.Literal(acc_id),
            sql.Literal(first_day),
            sql.Literal(last_day)
        )

        self.env.cr.execute(query)
        result = self.env.cr.fetchall()
        if result:
            return result[0][0]
        else:
            return 0.0

    def _get_ext_inc_by_account(self, acc_id, date):
        first_day = self.get_first_and_last_day_of_year(date)[0]
        
        query = sql.SQL("""
            select
            	coalesce(SUM(aml.balance),0) as amount
            from
            	account_move_line aml
            join program_code pc on aml.program_code_id = pc.id
            join resource_origin ro on ro.id = pc.resource_origin_id 
            where
            	aml.account_id = {}
            	and aml.parent_state = 'posted'
            	and ro.key_origin = '01'
            	and aml.date between {} and {};
            """).format(
            sql.Literal(acc_id),
            sql.Literal(first_day),
            sql.Literal(date)
            )

        self.env.cr.execute(query)
        result = self.env.cr.fetchall()
        if result:
            return result[0][0]
        else:
            return 0.0

    def _get_date_formated(self, date):
        date = datetime.strptime(date, DATE_FORMAT)
        date = date.strftime(DATE_FORMAT)
        return date
    def _generate_annex(self,
                        workbook,
                        annex_letter,
                        annex_title,
                        configs,
                        options,
                        styles_dict,
                        annex_subtitle = '',
                        annex_subsubtitle = '',
                        acc_code = False,
                        authorized = False,
                        top = False):
        sheet = workbook.add_worksheet(_('Annex ') + annex_letter)

        actual_date_to = options['date'].get('date_to')
        actual_date_to_datetime = datetime.strptime(actual_date_to, DATE_FORMAT)
        actual_year = actual_date_to_datetime.year
        
        date_to_datetime = datetime.strptime(options['date'].get('date_to'), DATE_FORMAT)
        date_to_str = options['date'].get('date_to')
        date_to_msg = str(date_to_datetime.day) + ' DE ' + str(self.get_month_name(date_to_datetime.month).upper())
        
        # XLSX Styles
        super_col_style = styles_dict['super_col_style']
        default_style = styles_dict['default_style']
        title_style = styles_dict['title_style']
        level_0_style = styles_dict['level_0_style']
        
        # Headers
        column_number = 7
    
        y_offset = 10
        
        if top:
            sheet.merge_range(7, 0, 7, 7, '', super_col_style)
            sheet.merge_range(8, 0, 9, 0, 'DEP', title_style)
            sheet.merge_range(8, 1, 9, 4, configs.name, title_style) 
            sheet.merge_range(8, 5, 8, 7, 'RECURSOS EJERCIDOS', title_style) 
            sheet.write(9, 5, 'PRESUPUESTO', title_style) 
            sheet.write(9, 6, 'I. E.', title_style) 
            sheet.write(9, 7, 'TOTAL', title_style)

            sheet.set_column(1, 4, 13)
            sheet.set_column(5, 7, 14)

            domain = [
                ('parent_state', '=', 'posted'),
                ('account_id', '=', configs.id),
                ('date', '<=', actual_date_to_datetime),
                ('date', '>=', datetime(actual_date_to_datetime.year, 1, 1)),
                ]
            ln_obj = self.env['account.move.line']
            deps = ln_obj.search(domain).mapped('dependency_id')
            deps_totals = []
            domain.append(())
            domain.append(())
            for dep in deps:
                domain[4] = ('dependency_id', '=', dep.id)
                domain[5] = ('program_code_id.resource_origin_id.key_origin', '=', '00')
                budget = sum(ln_obj.search(domain).mapped('balance'))
                domain[5] = ('program_code_id.resource_origin_id.key_origin', '=', '01')
                ext_inc = sum(ln_obj.search(domain).mapped('balance'))
                total = sum(ln_obj.search(domain[:-1]).mapped('balance'))
                deps_totals.append([dep, budget, ext_inc, total])
            deps_totals = sorted(deps_totals, key = lambda x: x[3], reverse = True)

            for i in range(10 if len(deps) > 10 else len(deps)):
                sheet.write(y_offset, 0, deps_totals[i][0].dependency, default_style)
                sheet.merge_range(y_offset, 1, y_offset,  4, deps_totals[i][0].description, default_style)
                sheet.write(y_offset, 5, round(deps_totals[i][1] / 1000), default_style)
                sheet.write(y_offset, 6, round(deps_totals[i][2] / 1000), default_style)
                sheet.write(y_offset, 7, round(deps_totals[i][3] / 1000), default_style)
                y_offset += 1

            sheet.merge_range(y_offset, 1, y_offset,  4, 'SUBTOTAL', default_style)
            subtotal_budget = sum(elem[1] for elem in deps_totals[:10])
            subtotal_ext_inc = sum(elem[2] for elem in deps_totals[:10])
            subtotal_total = sum(elem[3] for elem in deps_totals[:10])
            sheet.write(y_offset, 5, round(subtotal_budget / 1000), default_style)
            sheet.write(y_offset, 6, round(subtotal_ext_inc / 1000), default_style)
            sheet.write(y_offset, 7, round(subtotal_total / 1000), default_style)
            y_offset += 1

            total_budget = subtotal_budget
            total_ext_inc = subtotal_ext_inc
            total_total = subtotal_total
            
            if len(deps) > 10:
                domain[4] = ('dependency_id', 'in', [dep[0].id for dep in deps_totals[10:]])
                others_ext_inc = sum(ln_obj.search(domain).mapped('balance'))
                domain[5] = ('program_code_id.resource_origin_id.key_origin', '=', '00')
                others_budget = sum(ln_obj.search(domain).mapped('balance'))
                others_total = sum(ln_obj.search(domain[:-1]).mapped('balance'))
                sheet.merge_range(y_offset, 1, y_offset,  4, 'DIVERSAS', default_style)
                sheet.write(y_offset, 5, round(others_budget / 1000), default_style)
                sheet.write(y_offset, 6, round(others_ext_inc / 1000), default_style)
                sheet.write(y_offset, 7, round(others_total / 1000), default_style)
                y_offset += 1

                total_budget += others_budget
                total_ext_inc += others_ext_inc
                total_total += others_total

            sheet.merge_range(y_offset, 1, y_offset,  4, 'TOTAL', default_style)
            sheet.write(y_offset, 5, round(total_budget / 1000), default_style)
            sheet.write(y_offset, 6, round(total_ext_inc / 1000), default_style)
            sheet.write(y_offset, 7, round(total_total / 1000), default_style)
            y_offset += 1
            column_number = 7
        
        income_annexes_letters_total = ['A', 'A-1', 'A-1.1', 'B']
        income_annexes_letters = ['C', 'D', 'E', 'F']
        expenses_annexes_letters = ['G', 'G-1','H', 'H-1', 'I', 'J', 'K', 'L', 'M', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA']
        if annex_letter in income_annexes_letters_total:
            # Column Headers
            sheet.merge_range(7, 0, 7, 7, '', super_col_style)
            sheet.merge_range(8, 0, 9, 0, 'Concepto', title_style) 
            sheet.merge_range(8, 1, 9, 1, 'Asignado', title_style) 
            sheet.merge_range(8, 2, 9, 2, 'Aumentos (Disminuciones)', title_style) 
            sheet.merge_range(8, 3, 9, 3, 'Asignado Modificado', title_style) 
            sheet.merge_range(8, 4, 8, 5, str(self.get_month_name(date_to_datetime.month)), title_style)
            sheet.write(9, 4, actual_year, title_style)
            sheet.write(9, 5, str(int(actual_year) - 1), title_style)
            sheet.merge_range(8, 6, 9, 6, 'Variación', title_style)
            sheet.merge_range(8, 7, 9, 7, '%', title_style)
            column_number = 7
            if annex_letter == 'A-1' or annex_letter == 'A-1.1':
                sheet.merge_range(8, 8, 9, 8, 'Integral %', title_style)
                column_number = 8

            # Set columns width
            sheet.set_column(0, 6, 27)
            sheet.set_column(7, 7, 15)

            # Write rows
            total_annual_assigned_amount = 0
            total_transfers_amount = 0
            total_annual_authorized = 0
            total_account_exercised_amount = 0
            total_account_exercised_amount_last_year = 0   
            for conf in configs:
                result = self._set_annexe_variation_type_totals(conf, sheet, y_offset, default_style, annex_letter)
                total_annual_assigned_amount += result[0]
                total_transfers_amount += result[1]
                total_annual_authorized += result[2]
                total_account_exercised_amount += result[3]
                total_account_exercised_amount_last_year += result[4]
                y_offset += 1

            # Write total rows
            variation = total_account_exercised_amount - total_account_exercised_amount_last_year
            percent = self.get_percent_variation(total_account_exercised_amount, total_account_exercised_amount_last_year)    
            sheet.write(y_offset, 0, 'Total', level_0_style)
            sheet.write(y_offset, 1, self._get_rounded_formatted_value(total_annual_assigned_amount), level_0_style)
            sheet.write(y_offset, 2, self._get_rounded_formatted_value(total_transfers_amount), level_0_style)
            sheet.write(y_offset, 3, self._get_rounded_formatted_value(total_annual_authorized), level_0_style)
            sheet.write(y_offset, 4, self._get_rounded_formatted_value(total_account_exercised_amount), level_0_style)
            sheet.write(y_offset, 5, self._get_rounded_formatted_value(total_account_exercised_amount_last_year), level_0_style)
            sheet.write(y_offset, 6, self._get_rounded_formatted_value(variation), level_0_style)
            sheet.write(y_offset, 7, percent, level_0_style)
            if annex_letter == 'A-1' or annex_letter == 'A-1.1':
                y_offset = 10
                total_percent = 0
                for conf in configs:
                    integral_percent = self._get_integral_percent(conf['total_account_exercised'], total_account_exercised_amount)
                    sheet.write(y_offset, 8, integral_percent, default_style)
                    y_offset += 1
                    total_percent += integral_percent if isinstance(integral_percent, int) else 0
                sheet.write(y_offset, 8, total_percent, level_0_style)  
                sheet.set_column(8, 8, 15)
            
        elif annex_letter in income_annexes_letters:
            # Column Headers
            sheet.merge_range(7, 0, 7, 7, '', super_col_style)
            sheet.merge_range(8, 0, 9, 0, 'Cuenta', title_style) 
            sheet.merge_range(8, 1, 9, 1, 'Concepto', title_style) 
            sheet.merge_range(8, 2, 8, 3, str(self.get_month_name(date_to_datetime.month)), title_style)
            sheet.write(9, 2, actual_year, title_style)
            sheet.write(9, 3, str(int(actual_year) - 1), title_style)
            sheet.merge_range(8, 4, 9, 4, 'Variación', title_style)
            sheet.merge_range(8, 5, 9, 5, '%', title_style)
            column_number = 5

            # Set columns width
            sheet.set_column(0, 5, 27)
            sheet.set_column(7, 7, 27)
            sheet.set_column(5, 5, 15)

            self._set_annexe_variation_type(configs, options, date_to_str, sheet, y_offset, level_0_style, default_style)
        elif annex_letter in expenses_annexes_letters:
            expenses_annexes_letters_unfolded = ['I', 'J', 'K', 'L', 'M', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA']
            # Column Headers
            sheet.merge_range(7, 0, 7, 7, '', super_col_style)
            sheet.merge_range(8, 0, 9, 0, 'Partida', title_style) 
            sheet.merge_range(8, 1, 9, 1, 'Autorizado Modificado', title_style) 
            sheet.merge_range(8, 2, 9, 2, 'Concepto', title_style)
            sheet.merge_range(8, 3, 8, 4, str(self.get_month_name(date_to_datetime.month)), title_style)
            sheet.write(9, 3, actual_year, title_style)
            sheet.write(9, 4, str(int(actual_year) - 1), title_style)
            sheet.merge_range(8, 5, 9, 5, 'Variación', title_style)
            sheet.merge_range(8, 6, 9, 6, '%', title_style)
            column_number = 6
            # Set columns width
            sheet.set_column(0, 5, 27)
            sheet.set_column(6, 6, 15)
            if annex_letter in expenses_annexes_letters_unfolded:
                self._write_annexe_variation_expenses(configs, options, date_to_str, sheet, y_offset, level_0_style, default_style)
            else:
                self._write_annexe_par_folded(configs, options, date_to_str, sheet, y_offset, level_0_style, default_style, annex_letter)     

        # Header
        self._write_annexe_header(sheet, super_col_style, annex_letter, date_to_msg, actual_year, column_number, annex_title, annex_subtitle, annex_subsubtitle)

    def _write_annexe_header(self, sheet, super_col_style, annex_letter, date_to_msg, actual_year, column_number, annex_title, annex_subtitle, annex_subsubtitle):
        # Headers
        sheet.merge_range(0, 0, 0, column_number, '', super_col_style)
        sheet.merge_range(1, 0, 1, column_number, self._get_report_name().upper(), super_col_style)
        sheet.merge_range(2, 0, 2, column_number-1, 'DEL 1° DE ENERO' + ' AL ' + date_to_msg + ' DE ' + str(actual_year) + ' Y ' + str(int(actual_year) - 1), super_col_style)
        sheet.write(2, column_number, 'ANEXO ' + annex_letter, super_col_style)
        sheet.merge_range(3, 0, 3, column_number, annex_title, super_col_style)
        sheet.merge_range(4, 0, 4, column_number, annex_subtitle.upper(), super_col_style)
        sheet.merge_range(5, 0, 5, column_number, annex_subsubtitle.upper(), super_col_style)
        sheet.merge_range(6, 0, 6, column_number, '(Cifras en Miles de Pesos)', super_col_style)

    def _set_annexe_variation_type_totals(self, configs, sheet, y_offset, default_style, annex_letter):
        total_annual_assigned_amount = configs['total_annual_assigned']
        total_transfers_amount = configs['total_transfers']
        total_account_exercised_amount = configs['total_account_exercised']
        total_account_exercised_amount_last_year = configs['last_year_total_account_exercised']
        total_annual_authorized = configs['total_annual_authorized']
        name = configs['category_name']

        # Write total rows
        variation = total_account_exercised_amount - total_account_exercised_amount_last_year
        percent = self.get_percent_variation(total_account_exercised_amount, total_account_exercised_amount_last_year)

        sheet.write(y_offset, 0, name, default_style)
        sheet.write(y_offset, 1, self._get_rounded_formatted_value(total_annual_assigned_amount), default_style)
        sheet.write(y_offset, 2, self._get_rounded_formatted_value(total_transfers_amount), default_style)
        sheet.write(y_offset, 3, self._get_rounded_formatted_value(total_annual_authorized), default_style)
        sheet.write(y_offset, 4, self._get_rounded_formatted_value(total_account_exercised_amount), default_style)
        sheet.write(y_offset, 5, self._get_rounded_formatted_value(total_account_exercised_amount_last_year), default_style)
        sheet.write(y_offset, 6, self._get_rounded_formatted_value(variation), default_style)
        sheet.write(y_offset, 7, percent, default_style)
        if annex_letter == 'A-1' or annex_letter == 'A-1.1':
                integral_percent = 0
                sheet.write(y_offset, 8, integral_percent, default_style)
        
        return total_annual_assigned_amount, total_transfers_amount, total_annual_authorized, total_account_exercised_amount, total_account_exercised_amount_last_year

    def _set_annexe_variation_type(self, configs, options, date_to_str, sheet, y_offset, level_0_style, default_style):

        total_account_exercised_amount = 0
        total_account_exercised_amount_last_year = 0
                    
        for account in configs.accounts_ids:
            last_year_date_to_str = self.get_last_year_date(options['date'].get('date_to'))
            # Calculate amounts
            account_exercised_amount = self._verify_account_type(account.code, self._get_account_total_amount(date_to_str, account.id))[0]
            account_exercised_amount_last_year = self._verify_account_type(account.code, self._get_account_total_amount(last_year_date_to_str, account.id))[0]
            total_account_exercised_amount += account_exercised_amount
            total_account_exercised_amount_last_year += account_exercised_amount_last_year   

            variation = account_exercised_amount - account_exercised_amount_last_year
            percent = self.get_percent_variation(account_exercised_amount, account_exercised_amount_last_year)

            # Write accounts rows
            sheet.write(y_offset, 0, account.code, default_style)
            sheet.write(y_offset, 1, account.name, default_style)
            sheet.write(y_offset, 2, self._get_rounded_formatted_value(account_exercised_amount), default_style)
            sheet.write(y_offset, 3, self._get_rounded_formatted_value(account_exercised_amount_last_year), default_style)
            sheet.write(y_offset, 4, self._get_rounded_formatted_value(variation), default_style)
            sheet.write(y_offset, 5, percent, default_style)
            y_offset += 1

        # Write total rows
        variation = total_account_exercised_amount - total_account_exercised_amount_last_year
        percent = self.get_percent_variation(total_account_exercised_amount, total_account_exercised_amount_last_year)

        sheet.write(y_offset, 0, '', level_0_style)
        sheet.write(y_offset, 1, 'Total', level_0_style)
        sheet.write(y_offset, 2, self._get_rounded_formatted_value(total_account_exercised_amount), level_0_style)
        sheet.write(y_offset, 3, self._get_rounded_formatted_value(total_account_exercised_amount_last_year), level_0_style)
        sheet.write(y_offset, 4, self._get_rounded_formatted_value(variation), level_0_style)
        sheet.write(y_offset, 5, percent, level_0_style)

    def _write_annexe_par_folded(self, configs, options, date_to_str, sheet, y_offset, level_0_style, default_style, annex_letter):
        last_year_date_to_str = self.get_last_year_date(options['date'].get('date_to'))
        grand_total_annual_authorized = 0
        grand_total_account_exercised_amount = 0
        grand_total_account_exercised_amount_last_year = 0
        for group in configs.annex_account_group_ids:
            total_annual_authorized = 0
            total_account_exercised_amount = 0
            total_account_exercised_amount_last_year = 0
            group_key = group.key
            group_name = group.group_name
            for account in group.accounts_ids:
                # Calculate amounts
                annual_authorized = self._get_cog_total_amount(date_to_str, account.id, account.code)
                total_annual_authorized += annual_authorized
                account_exercised_amount = self._verify_account_type(account.code, self._get_account_total_amount(date_to_str, account.id))[0]
                account_exercised_amount_last_year = self._verify_account_type(account.code, self._get_account_total_amount(last_year_date_to_str, account.id))[0]
                total_account_exercised_amount += account_exercised_amount
                total_account_exercised_amount_last_year += account_exercised_amount_last_year
                if annex_letter not in ['G', 'H']:
                    variation = account_exercised_amount - account_exercised_amount_last_year
                    percent = self.get_percent_variation(account_exercised_amount, account_exercised_amount_last_year)
                    sheet.write(y_offset, 0, account.code.split('.')[-1], default_style)
                    sheet.write(y_offset, 1, annual_authorized, default_style)
                    sheet.write(y_offset, 2, account.name, default_style)
                    sheet.write(y_offset, 3, self._get_rounded_formatted_value(account_exercised_amount), default_style)
                    sheet.write(y_offset, 4, self._get_rounded_formatted_value(account_exercised_amount_last_year), default_style)
                    sheet.write(y_offset, 5, self._get_rounded_formatted_value(variation), default_style)
                    sheet.write(y_offset, 6, percent, default_style)
                    group_key = ''
                    group_name = ''
                    y_offset += 1
            
            variation = total_account_exercised_amount - total_account_exercised_amount_last_year
            percent = self.get_percent_variation(total_account_exercised_amount, total_account_exercised_amount_last_year)
            sheet.write(y_offset, 0, group_key, default_style)
            sheet.write(y_offset, 1, self._get_rounded_formatted_value(total_annual_authorized), default_style)
            sheet.write(y_offset, 2, group_name, default_style)
            sheet.write(y_offset, 3, self._get_rounded_formatted_value(total_account_exercised_amount), default_style)
            sheet.write(y_offset, 4, self._get_rounded_formatted_value(total_account_exercised_amount_last_year), default_style)
            sheet.write(y_offset, 5, self._get_rounded_formatted_value(variation), default_style)
            sheet.write(y_offset, 6, percent, default_style)
            y_offset += 1
            grand_total_annual_authorized += total_annual_authorized  
            grand_total_account_exercised_amount += total_account_exercised_amount
            grand_total_account_exercised_amount_last_year += total_account_exercised_amount_last_year

        total_variation = grand_total_account_exercised_amount - grand_total_account_exercised_amount_last_year
        total_percent = self.get_percent_variation(grand_total_account_exercised_amount, grand_total_account_exercised_amount_last_year)
        sheet.write(y_offset, 0, '', level_0_style)
        sheet.write(y_offset, 1, self._get_rounded_formatted_value(grand_total_annual_authorized), level_0_style)
        sheet.write(y_offset, 2, '', level_0_style)
        sheet.write(y_offset, 3, self._get_rounded_formatted_value(grand_total_account_exercised_amount), level_0_style)
        sheet.write(y_offset, 4, self._get_rounded_formatted_value(grand_total_account_exercised_amount_last_year), level_0_style)
        sheet.write(y_offset, 5, self._get_rounded_formatted_value(total_variation), level_0_style)
        sheet.write(y_offset, 6, total_percent, level_0_style)
        y_offset += 1

    def _write_annexe_variation_expenses(self, configs, options, date_to_str, sheet, y_offset, level_0_style, default_style):
        total_annual_authorized = 0
        total_account_exercised_amount = 0
        total_account_exercised_amount_last_year = 0        
        for account in configs.accounts_ids:
            last_year_date_to_str = self.get_last_year_date(options['date'].get('date_to'))
            # Calculate amounts
            annual_authorized = self._get_cog_total_amount(date_to_str, account.id, account.code)
            total_annual_authorized += annual_authorized
            account_exercised_amount = self._verify_account_type(account.code, self._get_account_total_amount(date_to_str, account.id))[0]
            account_exercised_amount_last_year = self._verify_account_type(account.code, self._get_account_total_amount(last_year_date_to_str, account.id))[0]
            total_account_exercised_amount += account_exercised_amount
            total_account_exercised_amount_last_year += account_exercised_amount_last_year
            variation = account_exercised_amount - account_exercised_amount_last_year
            percent = self.get_percent_variation(account_exercised_amount, account_exercised_amount_last_year)
            sheet.write(y_offset, 0, account.code.split('.')[-1], default_style)
            sheet.write(y_offset, 1, self._get_rounded_formatted_value(annual_authorized), default_style)
            sheet.write(y_offset, 2, account.name, default_style)
            sheet.write(y_offset, 3, self._get_rounded_formatted_value(account_exercised_amount), default_style)
            sheet.write(y_offset, 4, self._get_rounded_formatted_value(account_exercised_amount_last_year), default_style)
            sheet.write(y_offset, 5, self._get_rounded_formatted_value(variation), default_style)
            sheet.write(y_offset, 6, percent, default_style)
            y_offset += 1 

        variation = total_account_exercised_amount - total_account_exercised_amount_last_year
        percent = self.get_percent_variation(total_account_exercised_amount, total_account_exercised_amount_last_year)
        sheet.write(y_offset, 0, '', level_0_style)
        sheet.write(y_offset, 1, self._get_rounded_formatted_value(total_annual_authorized), level_0_style)
        sheet.write(y_offset, 2, '', level_0_style)
        sheet.write(y_offset, 3, self._get_rounded_formatted_value(total_account_exercised_amount), level_0_style)
        sheet.write(y_offset, 4, self._get_rounded_formatted_value(total_account_exercised_amount_last_year), level_0_style)
        sheet.write(y_offset, 5, self._get_rounded_formatted_value(variation), level_0_style)
        sheet.write(y_offset, 6, percent, level_0_style)
        y_offset += 1

    def get_percent_variation(self, account_exercised_amount, account_exercised_amount_last_year):
        if account_exercised_amount_last_year == 0:
            return 'N/A'
        else:
            percent = (account_exercised_amount - account_exercised_amount_last_year  * 100) / account_exercised_amount_last_year
            return round(percent)
        
    def _get_xlsx_styles(self, workbook):
        format_dict = {
            'currect_date_style' : workbook.add_format({'font_name': 'Arial', 'bold': False, 'align': 'right'}),
            'default_style' : workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'}),
            'title_style' : workbook.add_format({'font_name': 'Arial', 'bold': True, 'bottom': 2, 'align': 'center'}),
            'super_col_style' : workbook.add_format({'font_name': 'Arial', 'bold': True, 'align': 'center'}),
            'date_default_col1_style': workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2, 'num_format': 'yyyy-mm-dd'}),
            'date_default_style': workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': 'yyyy-mm-dd'}),
            'default_col1_style': workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2}),
            'level_0_style': workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 13, 'bottom': 6, 'font_color': '#666666'}),
            'level_1_style': workbook.add_format({'font_name': 'Arial', 'bold': False, 'font_size': 13, 'bottom': 1, 'font_color': '#666666'}),
            'level_2_col1_style': workbook.add_format({'font_name': 'Arial', 'bold': False, 'font_size': 12, 'font_color': '#666666', 'indent': 1}),
            'level_2_col1_total_style': workbook.add_format({'font_name': 'Arial', 'bold': False, 'font_size': 12, 'font_color': '#666666'}),
            'level_2_style': workbook.add_format({'font_name': 'Arial', 'bold': False, 'font_size': 12, 'font_color': '#666666'}),
            'level_3_col1_style': workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 2}),
            'level_3_col1_total_style': workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 1}),
            'level_3_style': workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'}),
            'level_4_col1_style': workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 3}),
            'level_4_col1_total_style': workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 2}),
            'level_4_style': workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'}),
            'level_5_col1_style': workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 4}),
            'level_5_col1_total_style': workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 3}),
            'level_5_style': workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'}),
            'level_6_col1_style': workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 5}),
            'level_6_col1_total_style': workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 4}),
            'level_6_style': workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666'})
        }
        return format_dict

    def _get_style_xlsx_by_level(self, lines, y_offset, position, style_dict):
        
        default_col1_style = style_dict['default_col1_style']
        default_style = style_dict['default_style']
        level_0_style = style_dict['level_0_style']
        level_1_style = style_dict['level_1_style']
        level_2_col1_style = style_dict['level_2_col1_style']
        level_2_col1_total_style = style_dict['level_2_col1_total_style']
        level_2_style = style_dict['level_2_style']
        level_3_col1_style = style_dict['level_3_col1_style']
        level_3_col1_total_style = style_dict['level_3_col1_total_style']
        level_3_style = style_dict['level_3_style']
        level_4_col1_style = style_dict['level_4_col1_style']
        level_4_col1_total_style = style_dict['level_4_col1_total_style']
        level_4_style = style_dict['level_4_style']
        level_5_col1_style = style_dict['level_5_col1_style']
        level_5_col1_total_style = style_dict['level_5_col1_total_style']
        level_5_style = style_dict['level_5_style']
        level_6_col1_style = style_dict['level_6_col1_style']
        level_6_col1_total_style = style_dict['level_6_col1_total_style']
        level_6_style = style_dict['level_6_style']

        style_map = {
            0: (level_0_style, level_0_style, default_style),
            1: (level_1_style, level_1_style, default_style),
            2: (level_2_style, level_2_col1_style, level_2_col1_total_style),
            3: (level_3_style, level_3_col1_style, level_3_col1_total_style),
            4: (level_4_style, level_4_col1_style, level_4_col1_total_style),
            5: (level_5_style, level_5_col1_style, level_5_col1_total_style),
            6: (level_6_style, level_6_col1_style, level_6_col1_total_style),
        }

        level = lines[position].get('level')
        style, col1_style, col1_total_style = style_map.get(level, (default_style, default_col1_style, default_col1_style))

        if lines[position].get('caret_options'):
            style = level_3_style
            col1_style = level_3_col1_style
        elif 'total' in lines[position].get('class', '').split(' '):
            col1_style = col1_total_style

        return y_offset, style, col1_style
    
    def write_all_data_rows_xlsx(self, lines, y_offset, style_dict, sheet, date_default_col1_style, date_default_style):
        row_y = 0
        for y in range(0, len(lines)):
            y_offset, style, col1_style = self._get_style_xlsx_by_level(lines, y_offset, y, style_dict)

            if lines[y]['level'] < 3:
                #write the first column, with a specific style to manage the indentation
                cell_type, cell_value = self._get_cell_type_value(lines[y])
                if cell_type == 'date':
                    sheet.write_datetime(row_y + y_offset, 0, cell_value, date_default_col1_style)
                else:
                    sheet.write(row_y + y_offset, 0, cell_value, col1_style)

                #write all the remaining cells
                for x in range(1, len(lines[y]['columns']) + 1):
                    cell_type, cell_value = self._get_cell_type_value(lines[y]['columns'][x - 1])
                    if cell_type == 'date':
                        sheet.write_datetime(row_y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, date_default_style)
                    else:
                        sheet.write(row_y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, style)
                row_y += 1

    def _get_integral_percent(self, account_exercised_amount, total_account_exercised_amount):
        if account_exercised_amount == 0:    
            return 'N/A'
        else:
            percent = (account_exercised_amount * 100) / total_account_exercised_amount
            return round(percent)
    
