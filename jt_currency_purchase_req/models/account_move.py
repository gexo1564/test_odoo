# -*- coding: utf-8 -*-
import logging
import json
from lxml import etree
from odoo import models, fields, api, _


"""Modelos"""
APP_REQUEST='application.request'
class AccountMove(models.Model):
    _inherit = 'account.move'

    
    currency_funds_move_id = fields.Many2one('currency.funds')

class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    # Campo de referencia de los asientos contables con la cuenta por pagar
    app_request_transfer_id = fields.Many2one(APP_REQUEST)
    app_request_buy_id = fields.Many2one(APP_REQUEST)
    app_request_recived_id = fields.Many2one(APP_REQUEST)