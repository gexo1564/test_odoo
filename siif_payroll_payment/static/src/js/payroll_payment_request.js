odoo.define("siif_payroll_payment.ListController", function (require) {
  "use strict";

  let ListController = require("web.ListController")
  let rpc = require('web.rpc');
  let session = require('web.session')

  ListController.include({
    action_create_payroll_payment_request: function () {
      let self = this
      let user = session.uid;
      let res_ids = self.initialState.res_ids

      rpc.query({
        model: 'employee.payroll.file',
        method: 'create_payroll_payment_request_action',
        args: [[user], this.initialState.context.active_id, res_ids],
      }).then(function (e) {
        self.do_action(e);
      });
    },
    action_payroll_payment_processing_check: function () {
      let self = this
      let user = session.uid;
      rpc.query({
        model: 'account.move',
        method: 'payroll_payment_processing_action',
        args: [[user], self.getSelectedIds()],
      }).then(function (e) {
        self.do_action(e);
      });
    },
    action_payroll_payment_processing_transfer: function () {
      let self = this
      let user = session.uid;
      rpc.query({
        model: 'payroll.payment.batch.wizard',
        method: 'open_wizard',
        args: [[user], self.getSelectedIds()],
      }).then(function (e) {
        self.do_action(e);
      });
    },
    action_payroll_payment_processing_reissue: function () {
      let self = this
      let user = session.uid;
      rpc.query({
        model: 'account.move',
        method: 'payroll_payment_processing_reissue_action',
        args: [[user], self.getSelectedIds()],
      }).then(function (e) {
        self.do_action(e);
      });
    },
    renderButtons: function($node) {
      this._super(...arguments)
      if (this.$buttons) {
        // Botón para Generar Solicitud de Pago
        this.$buttons.find('.oe_action_create_payroll_payment_request')
        .click(this.proxy('action_create_payroll_payment_request'))
        // Botón para Generar lotes y pagos de cheques
        this.$buttons.find('.oe_action_payroll_payment_processing_check')
        .click(this.proxy('action_payroll_payment_processing_check'))
        // Botón para Generar lotes y pagos de transferencia
        this.$buttons.find('.oe_action_payroll_payment_processing_transfer')
        .click(this.proxy('action_payroll_payment_processing_transfer'))
        // Botón para Generar lotes y pagos
        this.$buttons.find('.oe_action_payroll_payment_processing_reissue')
        .click(this.proxy('action_payroll_payment_processing_reissue'))
      }
    },
  });
});