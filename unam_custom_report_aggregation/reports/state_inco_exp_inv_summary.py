# -*- coding: utf-8 -*-
from datetime import datetime
from odoo import api, fields, models, _


class IncomeExpensesandInvestmentSummary(models.AbstractModel):
    _inherit = 'jt_account_module_design.stat.inc.exp.inv.summary.report'

    filter_all_entries = False

    def _get_lines(self, options, line_id=None):
        posted_only = not options.get('all_entries')

        lines = []

        start = datetime.strptime(str(options['date'].get('date_from')), '%Y-%m-%d').date()
        end = datetime.strptime(options['date'].get('date_to'), '%Y-%m-%d').date()

        if options.get('all_entries') is False:
            move_state_domain = ('move_id.state', '=', 'posted')
        else:
            move_state_domain = ('move_id.state', '!=', 'cancel')

        domain = [('date', '<=', end),move_state_domain]
        
        concept_ids = self.env['detailed.statement.income'].search([('inc_exp_type','!=',False)])
        
        list_data = ['income','expenses','investments','other expenses']
        
        remant_assign = 0
        remant_exercised = 0
        remant_to_exercised = 0

        expenses_assign = 0
        expenses_exercised = 0
        expenses_to_exercised = 0

        year_assign = 0
        year_exercised = 0
        year_to_exercised = 0
        
        for type in list_data:
            type_concept_ids = concept_ids.filtered(lambda x:x.inc_exp_type == type)
            major_ids = type_concept_ids.mapped('major_id')
            if type_concept_ids:
                name = type.upper()
                if self.env.lang == 'es_MX' and type == 'income':
                    str1 = 'INGRESOS'
                    name = str1.upper()
                if self.env.lang == 'es_MX' and type == 'expenses':
                    str2 = 'GASTOS'
                    name = str2.upper()
                if self.env.lang == 'es_MX' and type == 'investments':
                    str3 = 'INVERSIONES'
                    name = str3.upper()
                if self.env.lang == 'es_MX' and type == 'other expenses':
                    str4 = 'OTROS GASTOS'
                    name = str4.upper()

                lines.append({
                    'id': type,
                    'name': name,
                    'columns': [
                                {'name': ''},
                                {'name': ''},
                                {'name': ''},
                                {'name': ''},
                                ],
    
                    'level': 1,
                    'unfoldable': False,
                    'unfolded': True,
                })
            for major in major_ids:

                cons = type_concept_ids.filtered(lambda x:x.major_id.id == major.id)

                lines.append(self.format_line(maj=major, children=cons,
                    cols=[
                        {'name': ''},
                        {'name': ''},
                        {'name': ''},
                        {'name': ''},
                    ],
                    extra_info={
                        'class':'text-left'
                    }
                ))

                for con in type_concept_ids.filtered(lambda x:x.major_id.id == major.id):
                    account_lines = []
                    total_assign = 0
                    total_exercised = 0
                    total_to_exercised = 0
                    total_per = 0
                    
                    for acc in con.account_ids:
                        assign = 0
                        authorized = 0
                        if con.item_ids:
                            acc_item_ids = con.item_ids.filtered(lambda x:x.unam_account_id.id==acc.id)
                            if acc_item_ids:
                                program_code_ids = self.env['program.code'].search([('item_id','in',acc_item_ids.ids)])
                                if program_code_ids:
                                    self.env.cr.execute("select coalesce(sum(ebl.authorized),0) from expenditure_budget_line ebl where ebl.program_code_id in %s and ebl.imported_sessional IS NULL and end_date <= %s", (tuple(program_code_ids.ids),end))
                                    my_datas = self.env.cr.fetchone()
                                    if my_datas:
                                        authorized = my_datas[0] / 1000
                                    
                                    self.env.cr.execute("select coalesce(SUM(CASE WHEN al.line_type = %s THEN al.amount ELSE -al.amount END),0) from adequacies_lines al,adequacies a where a.state=%s and al.program in %s and a.id=al.adequacies_id", ('increase','accepted',tuple(program_code_ids.ids)))
                                    my_datas = self.env.cr.fetchone()
                                    if my_datas:
                                        assign = my_datas[0] / 1000
                        assign += authorized
                        total_assign += assign
                        
                        values = self.env['account.move.line'].search(domain + [('account_id', 'in', acc.ids)])
                        exercised = sum(x.credit - x.debit for x in values) / 1000
                        total_exercised += exercised
                        to_exercised = assign - exercised
                        total_to_exercised += to_exercised
                            
                        per = 0.00
                        if assign:
                            per = 100.00
                        if assign != 0:
                            per = (exercised/assign)*100
                            per = round(per,2)
                        

                        dep_domain = [
                            ('date', '>=', start),
                            ('date', '<=', end),
                            ('parent_state', '=', 'posted') if posted_only else ('parent_state', '!=', 'cancel')
                        ]
                        deps = self.env['dependency'].get_dependencies_for_account(acc, dep_domain)

                        account_lines.append(self.format_line(maj=major, con=con, acc=acc, children=deps, cols=[
                            self._format({'name': assign},figure_type='float'),
                            self._format({'name': exercised},figure_type='float'),
                            { 'name':per,'class':'number' },
                            self._format({'name': to_exercised},figure_type='float'),
                        ]))


                        ################################
                        ####### DEP CALCULATIONS #######
                        ################################
                        for dep in deps:
                            dep_assign = 0
                            dep_authorized = 0
                            if con.item_ids:
                                acc_item_ids = con.item_ids.filtered(lambda x:x.unam_account_id.id==acc.id)
                                if acc_item_ids:
                                    program_code_ids = self.env['program.code'].search([
                                        ('item_id','in',acc_item_ids.ids),
                                        ('dependency_id', '=', dep.id)])
                                    if program_code_ids:
                                        self.env.cr.execute("select coalesce(sum(ebl.authorized),0) from expenditure_budget_line ebl where ebl.program_code_id in %s and ebl.imported_sessional IS NULL and end_date <= %s", (tuple(program_code_ids.ids),end))
                                        my_datas = self.env.cr.fetchone()
                                        if my_datas:
                                            dep_authorized = my_datas[0] / 1000
                                        
                                        self.env.cr.execute("select coalesce(SUM(CASE WHEN al.line_type = %s THEN al.amount ELSE -al.amount END),0) from adequacies_lines al,adequacies a where a.state=%s and al.program in %s and a.id=al.adequacies_id", ('increase','accepted',tuple(program_code_ids.ids)))
                                        my_datas = self.env.cr.fetchone()
                                        if my_datas:
                                            dep_assign = my_datas[0] / 1000
                            dep_assign += dep_authorized
                            dep_exercised = sum(x.credit - x.debit for x in self.env['account.move.line'].search(
                                domain + [
                                ('account_id', 'in', acc.ids),
                                ('dependency_id', '=', dep.id)])) / 1000

                            dep_to_exercised = dep_assign - dep_exercised
                                
                            dep_per = 0.00
                            if dep_assign:
                                dep_per = 100.00
                            if dep_assign != 0:
                                dep_per = round((dep_exercised/dep_assign)*100, 2)
                        ################################
                        ##### END DEP CALCULATIONS #####
                        ################################
                            subdep_domain = [
                                ('date', '>=', start),
                                ('date', '<=', end),
                                ('parent_state', '=', 'posted') if posted_only else ('parent_state', '!=', 'cancel')
                            ]
                            subdeps = self.env['sub.dependency'].get_subdependencies_for_account(acc, dep, subdep_domain)

                            account_lines.append(self.format_line(maj=major, con=con, acc=acc, dep=dep, children=subdeps, cols=[
                                self._format({'name': dep_assign},figure_type='float'),
                                self._format({'name': dep_exercised},figure_type='float'),
                                { 'name':dep_per,'class':'number' },
                                self._format({'name': dep_to_exercised},figure_type='float'),
                            ]))


                        ################################
                        ###### SUBDEP CALCULATIONS #####
                        ################################
                            for subdep in subdeps:
                                subdep_assign = 0
                                subdep_authorized = 0
                                if con.item_ids:
                                    acc_item_ids = con.item_ids.filtered(lambda x:x.unam_account_id.id==acc.id)
                                    if acc_item_ids:
                                        subdep_program_code_ids = self.env['program.code'].search([
                                            ('item_id','in',acc_item_ids.ids),
                                            ('dependency_id', '=', dep.id),
                                            ('sub_dependency_id', '=', subdep.id)])
                                        if subdep_program_code_ids:
                                            self.env.cr.execute("select coalesce(sum(ebl.authorized),0) from expenditure_budget_line ebl where ebl.program_code_id in %s and ebl.imported_sessional IS NULL and end_date <= %s", (tuple(subdep_program_code_ids.ids),end))
                                            my_datas = self.env.cr.fetchone()
                                            if my_datas:
                                                subdep_authorized = my_datas[0] / 1000
                                            
                                            self.env.cr.execute("select coalesce(SUM(CASE WHEN al.line_type = %s THEN al.amount ELSE -al.amount END),0) from adequacies_lines al,adequacies a where a.state=%s and al.program in %s and a.id=al.adequacies_id", ('increase','accepted',tuple(program_code_ids.ids)))
                                            my_datas = self.env.cr.fetchone()
                                            if my_datas:
                                                subdep_assign = my_datas[0] / 1000
                                subdep_assign += subdep_authorized
                                subdep_exercised = sum(x.credit - x.debit for x in self.env['account.move.line'].search(
                                    domain + [
                                    ('account_id', 'in', acc.ids),
                                    ('dependency_id', '=', dep.id),
                                    ('sub_dependency_id', '=', subdep.id)])) / 1000

                                subdep_to_exercised = subdep_assign - subdep_exercised
                                    
                                subdep_per = 0.00
                                if subdep_assign:
                                    subdep_per = 100.00
                                if subdep_assign != 0:
                                    subdep_per = round((dep_exercised/dep_assign)*100, 2)

                                account_lines.append(self.format_line(maj=major, con=con, acc=acc, dep=dep, subdep=subdep, cols=[
                                    self._format({'name': subdep_assign},figure_type='float'),
                                    self._format({'name': subdep_exercised},figure_type='float'),
                                    {'name':subdep_per,'class':'number'},
                                    self._format({'name': subdep_to_exercised},figure_type='float'),
                                ]))

                        ################################
                        #### END SUBDEP CALCULATIONS ###
                        ################################

                    if type == 'income':
                        remant_assign += total_assign
                        remant_exercised += total_exercised
                        remant_to_exercised += total_to_exercised

                        year_assign += total_assign
                        year_exercised += total_exercised
                        year_to_exercised += total_to_exercised
                        
                    elif type == 'expenses':
                        remant_assign -= total_assign
                        remant_exercised -= total_exercised
                        remant_to_exercised -= total_to_exercised
                    
                    if type != 'income':

                        expenses_assign += total_assign
                        expenses_exercised += total_exercised
                        expenses_to_exercised += total_to_exercised
                        
                        year_assign -= total_assign
                        year_exercised -= total_exercised
                        year_to_exercised -= total_to_exercised
                    total_per = 0.00
                    if total_exercised:
                        total_per = 100.00
                    if total_assign != 0:
                        total_per = (total_exercised/total_assign)*100
                        total_per = round(per,2)


                    lines.append(self.format_line(maj=major, con=con, children=con.account_ids,
                        cols=[
                            self._format({'name': total_assign},figure_type='float'),
                            self._format({'name': total_exercised},figure_type='float'),
                            {'name':total_per,'class':'number'},
                            self._format({'name': total_to_exercised},figure_type='float'),
                        ],
                        extra_info={
                            'class': 'text-left'
                        }))

                    lines += account_lines
                    lines.append({
                        'id': 'group_total',
                        'name': 'SUMA',
                        'columns': [
                                    self._format({'name': total_assign},figure_type='float'),
                                    self._format({'name': total_exercised},figure_type='float'),
                                    {'name':total_per,'class':'number'},
                                    self._format({'name': total_to_exercised},figure_type='float'),
                                    ],
                        
                        'level': 1,
                        'unfoldable': False,
                        'unfolded': True,
                        'class':'text-right',
                        'parent_id': 'con' + self.str_id(con) + self.str_id(major),
                    })

            if type=="expenses":
                remant_per = 0.00
                if remant_exercised:
                    remant_per = 100.00
                if remant_assign != 0:
                    remant_per = (remant_exercised/remant_assign)*100
                    remant_per = round(remant_per,2)
                
                lines.append({
                    'id': 'REMNANT',
                    'name': _('REMAINING BEFORE INVESTMENTS'),
                    'columns': [
                                self._format({'name': remant_assign},figure_type='float'),
                                self._format({'name': remant_exercised},figure_type='float'),
                                {'name':remant_per,'class':'number'},
                                self._format({'name': remant_to_exercised},figure_type='float'),
                                ],
                    
                    'level': 1,
                    'unfoldable': False,
                    'unfolded': True,
                    'class':'text-right'
                })
        expenses_per = 0.00
        if expenses_exercised:
            expenses_per = 100.00
        if expenses_assign != 0:
            expenses_per = (expenses_exercised/expenses_assign)*100
            expenses_per = round(expenses_per,2)

        lines.append({
            'id': 'Total EXPENSES',
            'name': _('TOTAL EXPENSES, INVESTMENTS AND OTHER EXPENSES'),
            'columns': [
                        self._format({'name': expenses_assign},figure_type='float'),
                        self._format({'name': expenses_exercised},figure_type='float'),
                        {'name':expenses_per,'class':'number'},
                        self._format({'name': expenses_to_exercised},figure_type='float'),
                        ],
            
            'level': 1,
            'unfoldable': False,
            'unfolded': True,
            'class':'text-right'
        })
        year_per = 0.00
        if year_exercised:
            year_per = 100.00
        if year_assign != 0:
            year_per = (year_exercised/year_assign)*100
            year_per = round(year_per,2)

        lines.append({
            'id': 'Total Year',
            'name': _('REMAINING OF THE YEAR'),
            'columns': [
                        self._format({'name': year_assign},figure_type='float'),
                        self._format({'name': year_exercised},figure_type='float'),
                        {'name':year_per,'class':'number'},
                        self._format({'name': year_to_exercised},figure_type='float'),
                        ],
            
            'level': 1,
            'unfoldable': False,
            'unfolded': True,
            'class':'text-right'
        })
        
        return lines