#  -*- coding: utf-8 -*-

from odoo import api, fields, models

class PayrollSalaryAdjustmentXLSX(models.AbstractModel):
	_name = 'report.jt_check_controls.payroll_salary_adjustment_xlsx'
	_inherit = 'report.report_xlsx.abstract'

	def generate_xlsx_report(self, workbook, data, payroll_salary_adjustment):
		header = ['RFC', 'Nombre', 'Quincena nueva', 'Banco', 'Cheque nuevo', 'Importe', 'Firma']
		element_sum = 0

		sheet = workbook.add_worksheet('payroll_salary_adjustment')
		cell_format = workbook.add_format({'align': 'right'})
		row = 0
		col = 0
		for h in header:
			sheet.write(row, col, h)
			sheet.set_column(row, col, 35)
			col = col + 1

		for element in payroll_salary_adjustment:
			row = row + 1
			sheet.write(row, 0, element.employee_id.rfc)
			sheet.write(row, 1, element.employee_id.name)
			sheet.write(row, 2, element.new_fortnight)
			sheet.write(row, 3, element.new_bank_id.name)
			sheet.write(row, 4, element.new_check_id.folio_ch)
			sheet.write(row, 5, '{:.2f}'.format(element.new_amount), cell_format)
			sheet.write(row, 6, '__________________')
			element_sum = element_sum + element.new_amount

		row = row + 2
		sheet.write(row, 0, f'Folios impresos: {len(payroll_salary_adjustment)}')
		sheet.write(row, 5, 'Total: {:.2f}'.format(element_sum), cell_format)



