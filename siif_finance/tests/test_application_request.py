import base64
import logging
from odoo import models, fields, api,_
from odoo.tests import common
from odoo.exceptions import UserError, ValidationError
from datetime import date
APP_REQUEST='application.request'
DENOMINATION='total.denominations'
QUOTE='total.denominations.quote'
FILE='jt_currency_purchase_req/static/tests/test.txt'
PDF='file_pdf.pdf'
XML='file_xml.xml'
class TestApplicationRequest(common.TransactionCase):

	def setUp(self):
		super().setUp()
	
		self.account_account = self.env['account.account'].search([('name','=','FF EFECTIVO EN DÓLARES')])
		self.journal = self.env['account.journal'].search([('name','=','BBVA BANCOMER-8141 (BBV01)')])
		self.journal2 = self.env['account.journal'].search([('name','=','AFIRME-8266 (BAF04)')])
		self.currency_prueba_01 = self.env['res.currency'].search([('name','=','AED')])
		self.bank = self.env['res.bank'].search([('name','=','REFORMA')])
		self.currency_fund = self.env['currency.funds'].create({
			'name':'Fondo Fijo',
			'associated_account':self.account_account.id,
			'journal_id':self.journal.id,
			'currency_id':self.currency_prueba_01.id,
			'fund_limit':10000.00,
			'currency_reorder_point':1000.00})

		

	#Prueba de la creación de la solicitud de compra
	def test_create(self):
		app_request=self.env[APP_REQUEST]
		denominations={
			'count':20,
			'denominations':'den_500'
		}
		den_test = self.env[DENOMINATION].create(denominations)

		"""
			Escenario 01: Monto de divisa igual a 0
		"""
		vals = {
			'status':'draft',
			'currency_fund_id':self.currency_fund.id,
			'required_date':date.today(),
			'amount_funds':0,
			'account_journal':self.journal.id,
			'total_denomination':den_test,
		}
		with self.assertRaises(ValidationError):
			app_request.create(vals)

		"""
			Escenario 02: Monto de divisa menor a 0
		"""
		vals = {
			'status':'draft',
			'currency_fund_id':self.currency_fund.id,
			'required_date':date.today(),
			'amount_funds': -10000,
			'account_journal':self.journal.id,
			'total_denomination':den_test,
		}
		with self.assertRaises(ValidationError):
			app_request.create(vals)


		"""
		Escenario 0: Creación de la solicitud de compra
		"""
		vals = {
			'status':'draft',
			'currency_fund_id':self.currency_fund.id,
			'required_date':date.today(),
			'amount_funds':10000,
			'account_journal':self.journal.id,
			'total_denomination':den_test,
		}
		
		test01 = app_request.create(vals)

		#Se revisa si se asigna el nombre con la moneda asignada
			#Secuencia creada
		self.assertEqual(test01.name,'Solicitud (AED) No. 000003')



	#Prueba para publicar una solicitud de compra
	def test_action_approve_published(self):
		#Suma denominacion 3000
		den_01={
			'count':6,
			'denominations':'den_500'
		}
		den_test_01 = self.env[DENOMINATION].create(den_01)


		vals = {
			'status':'draft',
			'currency_fund_id':self.currency_fund.id,
			'required_date':date.today(),
			'amount_funds':10000,
			'account_journal':self.journal.id,
		}
		test01 = self.env[APP_REQUEST].create(vals)

		"""
			Caso 01: Publicar solicitud sin denominaciones asociadas
		"""

		with self.assertRaises(ValidationError):
			test01.action_approve_published()

		test01.update({'total_denomination':den_test_01})
		
		"""
			Caso 02: Publicar solicitud con suma de denominaciones diferentes al monto
		"""
		with self.assertRaises(ValidationError):
			test01.action_approve_published()


		"""
			Caso 03: Publicar solicitud con denominaciones iguales al monto
		"""
		den_02={
			'count':35,
			'denominations':'den_200',
			'application_request':test01.id,
		}
		#Suma total de denominaciones 10000
		den_test_02 = self.env[DENOMINATION].create(den_02)
		

		test01.action_approve_published()
		self.assertEqual(test01.status,'published')
		self.assertEqual(test01.amount_denomination,10000)

	
	#Revisar si se tienen cotizaciones una solicitud 
	def test_check_quotes(self):
		#Suma denominacion 3000
		den_01={
			'count':6,
			'denominations':'den_500'
		}
		den_test_01 = self.env[DENOMINATION].create(den_01)
		den_quote_01 = self.env[QUOTE].create(den_01)

		vals = {
			'status':'draft',
			'currency_fund_id':self.currency_fund.id,
			'required_date':date.today(),
			'amount_funds':3000,
			'account_journal':self.journal.id,
			'total_denomination':den_test_01,
		}
		test01 = self.env[APP_REQUEST].create(vals)
		test01.action_approve_published()

		self.assertEqual(test01.has_quotes,False)
		with self.assertRaises(ValidationError):
			test01.action_quotes()

		#Abrir archivo para asignarle información

		file = open(FILE,"rb")
		out = file.read()
		file.close()
		files = base64.b64encode(out)

		#Creación de cotizaciones
		quote = self.env['quote'].create({
			'status':'draft',
			'application_request':test01.id,
			'amount_app_request':3000,
			'currency_fund_id':self.currency_fund.id,
			'denominations_quote_ids':den_quote_01,
			'bank_id':self.bank.id,
			'exchange_rate_number':19.80,
			'equal_denominations':True,
			'doc_quote':files,
		})
		quote.action_published()

		#Actualizar al estado cotizada
		self.assertEqual(test01.has_quotes,True)
		test01.action_quotes()
		self.assertEqual(test01.status,'quoted')

	
	#Revisar el proceso general de la solicitud de compra 
	def test_PROCESS(self):
		den_01={
			'count':6,
			'denominations':'den_500'
		}
		den_test_01 = self.env[DENOMINATION].create(den_01)
		den_quote_01 = self.env[QUOTE].create(den_01)
		den_quote_02 = self.env[QUOTE].create(den_01)
		vals = {
			'status':'draft',
			'currency_fund_id':self.currency_fund.id,
			'required_date':date.today(),
			'amount_funds':3000,
			'account_journal':self.journal.id,
			'total_denomination':den_test_01,
		}
		test01 = self.env[APP_REQUEST].create(vals)
		test01.action_approve_published()
		#Abrir archivo para asignarle información
		file = open(FILE,"rb")
		out = file.read()
		file.close()
		files = base64.b64encode(out)
		#Creación de cotizaciones
		quote_01 = self.env['quote'].create({
			'status':'draft',
			'application_request':test01.id,
			'amount_app_request':3000,
			'currency_fund_id':self.currency_fund.id,
			'denominations_quote_ids':den_quote_01,
			'bank_id':self.bank.id,
			'exchange_rate_number':19.80,
			'equal_denominations':True,
			'doc_quote':files,
		})
		quote_01.action_published()
		quote_02 = self.env['quote'].create({
			'status':'draft',
			'application_request':test01.id,
			'amount_app_request':3000,
			'currency_fund_id':self.currency_fund.id,
			'denominations_quote_ids':den_quote_02,
			'bank_id':self.bank.id,
			'exchange_rate_number':18.00,
			'equal_denominations':True,
			'doc_quote':files,
		})
		quote_02.action_published()
		test01.action_quotes()


		self.assertEqual(quote_01.selected_quote,False)
		self.assertEqual(quote_02.selected_quote,True)
		#Creación de Solicitud de transferencia
		request={
			'state':'draft',
			'bank_account_id':self.journal2.id,
			'origin_account':self.journal2.bank_account_id.id,
			'desti_bank_account_id':test01.account_journal.id,
			'desti_account':test01.account_journal.bank_account_id.id,
			'currency_id':quote_02.currency_id.id,
			'amount':quote_02.withdrawal_amount,
			'prepared_by_user_id':self.env.user.id,
			'user_id':self.env.user.id,
			'trasnfer_request':'finances',
			'date_required':date.today(),
		}
		transfer=self.env['request.open.balance.finance'].create(request)
		test01.update({'transfer_request':transfer})

		test01.transfer_request.request_finance()
		self.assertEqual(test01.transfer_request.state,'requested')
		test01.transfer_request.approve_finance()
		self.assertEqual(test01.transfer_request.state,'approved')
		test01.transfer_request.action_schedule_transfers()
		self.assertEqual(test01.transfer_request.state,'sent')

		test01.transfer_request.payment_ids.post()

		self.assertEqual(test01.transfer_request.payment_ids.state,'posted')
		self.assertNotEqual(len(test01.transfer_request.payment_ids.move_line_ids),0)

		test01.action_resources()
		self.assertEqual(test01.status,'with_resources')

		#Revisar si la solicitud está 
		with self.assertRaises(UserError):
			test01.action_accept_adquired()
		
		#Asignar archivo
		file = open(FILE,"rb")
		out = file.read()
		file.close()
		file_app = base64.b64encode(out)

		test01.update({'doc_transfer':file_app})
		test01.action_accept_adquired()

		"""
			Caso 00: Sin archivos
		"""
		#Revisar si no se tienen los documentos para recibir la solicitud de compra
		with self.assertRaises(UserError):
			test01.action_recived()

		file_pdf = open('jt_currency_purchase_req/static/tests/file_pdf.pdf',"rb")
		out_pdf = file_pdf.read()
		file_pdf.close()
		file_pdf_app = base64.b64encode(out_pdf)
		
		file_xml = open('jt_currency_purchase_req/static/tests/file_xml.xml',"rb")
		out_xml = file_xml.read()
		file_xml.close()
		file_xml_app = base64.b64encode(out_xml)

		
		"""
			Caso 01: Solo PDF
		"""
		test01.update({
			'doc_CFDI_pdf':file_pdf_app,
			'doc_CFDI_pdf_name':PDF})
		with self.assertRaises(UserError):
			test01.action_recived()

		"""
			Caso 02: Solo XML
		"""
		test01.update({
			'doc_CFDI_xml':file_xml_app,
			'doc_CFDI_xml_name':XML,
			'doc_CFDI_pdf':False,
			'doc_CFDI_pdf_name':'',
		})
		with self.assertRaises(UserError):
			test01.action_recived()
		
		"""
			Caso 03: Invertidos
		"""
		test01.update({
			'doc_CFDI_xml':file_pdf_app,
			'doc_CFDI_xml_name':PDF,
			'doc_CFDI_pdf':file_xml_app,
			'doc_CFDI_pdf_name':XML,
		})
		
		with self.assertRaises(UserError):
			test01.action_recived()

		"""
			Caso 03: Archivos correctos
		"""
		test01.update({
			'proof_payment_doc':file_pdf_app,
			'proof_payment_doc_name':PDF,
			'doc_CFDI_xml':file_xml_app,
			'doc_CFDI_xml_name':XML,
			'doc_CFDI_pdf':file_pdf_app,
			'doc_CFDI_pdf_name':PDF,
		})
		test01.action_recived()

		#Revisar si aumentó el monto actual del fondo de divisa
		self.currency_fund
		self.assertEqual(self.currency_fund.actual_fund,3000)
