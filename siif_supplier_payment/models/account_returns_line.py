import logging
from odoo import models, fields, api, _
from odoo.exceptions import UserError
class AccountReturnsLines(models.Model):
    _name = 'account.returns.line'
    _description = 'Account Returns Lines'

    account_returns_line_ids = fields.Many2one('account.returns')
    currency_id = fields.Many2one('res.currency',string='Currency',required=True,related='account_returns_line_ids.currency_id')
    selected_line = fields.Boolean(string="Selected Line", default=False)
    egress_key_id = fields.Many2one('egress.keys', string="Egress Key")
    program_code_id = fields.Many2one('program.code', string="Program Code")
    account_id = fields.Many2one('account.account', string="Account ID")
    account_ie = fields.Many2one('association.distribution.ie.accounts', string="Account IE")
    dependency_id = fields.Many2one('dependency',string="Dependency")
    sub_dependency_id = fields.Many2one('sub.dependency', string="Subdependency")
    quantity = fields.Float(string="Quantity")
    price_unit = fields.Float(string="Price Unit")
    price_subtotal = fields.Monetary(string="Price Subtotal",currency_field='currency_id')
    returned_money = fields.Monetary(string="Returned Money",currency_field='currency_id')
    price_return = fields.Monetary(string="Price Return",currency_field='currency_id', default=0)

    acc_move_line_id = fields.Many2one('account.move.line')

    @api.constrains('price_return')
    def _check_price_return(self):
        if self.selected_line:
            if self.price_return>0:
                if self.price_return>(self.price_subtotal-self.returned_money):
                    raise UserError('El monto con valor de $ %s no está correcto, supera el monto no devuelto de la linea correspondiente ($ %s). Revisar lineas.' % (str(self.price_return),str(self.price_subtotal-self.returned_money)))
            else:
                raise UserError('Linea con monto de $ %s está incorrecto, no puede ser menor a 0. Corregir linea.' % (str(self.price_return)))

    @api.onchange('selected_line')
    def _reset_price(self):
        if not self.selected_line:
            self.price_return=0
    #Función de creación
    @api.model
    def create(self, vals):
        res = super(AccountReturnsLines, self).create(vals)
        return res
    