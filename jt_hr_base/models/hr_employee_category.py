# -*- coding: utf-8 -*-
from odoo import models, fields

class HrEmployeeCategory(models.Model):
    _inherit='hr.employee.category'
    active = fields.Boolean(string='Active',default=True)