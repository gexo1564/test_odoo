import json
import logging
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
import datetime
import requests
from dateutil.relativedelta import relativedelta


# CONSTANTS FOR MESSAGES
NEGATIVE_OR_ZERO_VALUES_NOT_ALLOWED_MESSAGE = 'Negative values or values equal to zero are not allowed'


class UDILiquidationWizard(models.TransientModel):
    _name = 'udi.liquidate.wizard'
    _description = 'Wizard para liquidacion'

    id_udibono = fields.Integer()

    date_consult = fields.Date(default=datetime.date.today(), string="Fecha de Consulta")

    udi_value = fields.Float("Valor de la UDI",digits=(4, 9))


    @api.constrains('previous_month_titles_value')
    def check_previous_month_titles_value(self):
        if self.previous_month_titles_value <= 0:
            raise UserError(_(NEGATIVE_OR_ZERO_VALUES_NOT_ALLOWED_MESSAGE))
        
    @api.constrains('current_month_titles_value')
    def check_current_month_titles_value(self):
        if self.current_month_titles_value <= 0:
            raise UserError(_(NEGATIVE_OR_ZERO_VALUES_NOT_ALLOWED_MESSAGE))
        
    @api.constrains('udi_value')
    def check_udi_value(self):
        if self.udi_value <= 0:
            raise UserError(_(NEGATIVE_OR_ZERO_VALUES_NOT_ALLOWED_MESSAGE))


    @api.onchange('date_consult')
    def get_udi_value_on_date(self):
        today = self.date_consult
        token = self.env['ir.config_parameter'].get_param('banxico_token')
        url_banxico = self.env['ir.config_parameter'].get_param('banxico_api_url')
        url = f"{url_banxico}SP68257/datos/{today}/{today}?token={token}"
        response = requests.get(url,timeout=10)

        if response.status_code == 200:
            data = json.loads(response.content)
            if "datos" not in data["bmx"]["series"][0]:
                self.udi_value = 0
            else:
                udibono = data["bmx"]["series"][0]["datos"][0]["dato"]
                if udibono is not None:
                    self.udi_value = udibono
                else:
                    self.udi_value = 0
        else:
            self.udi_value = 0
          
    def liquidate(self):

        self.id_udibono = self.env.context.get('active_id')

        # objeto de tipo udibono para consultar el id del udibono actual   
        udibono = self.env['investment.udibonos'].search([('id', '=', self.id_udibono)])

        udibono.movement_date = self.date_consult
        udibono.udi_value_end_month = self.udi_value

        #Llama a la funcion de liquidación 
        udibono.liquidate()

        return self.id_udibono

