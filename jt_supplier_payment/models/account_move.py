from odoo import models, fields, api
import logging
class AccountMove(models.Model):

    _inherit = 'account.move'

    date_for_payment_procedure = fields.Date("Date for Payment Procedure")

    #Función para el cambio de la información por el procesamiento de nómina
    @api.onchange('payroll_processing_id')
    def change_info_payroll_processing(self):
        if self.payroll_processing_id:
            nomina=self.payroll_processing_id
            self.period_start=nomina.period_start
            self.period_end=nomina.period_end
            self.fornight=nomina.fornight
            self.payroll_request_type=nomina.payroll_type
        else:
            self.period_start=""
            self.period_end=""
            self.fornight=""
            self.payroll_request_type=""
    
    #Función para asignar el diario correspondiente a las solicitudes de pago
    @api.onchange('currency_id')
    def currency_journal_change(self):
        """
            Query para obtener los datos siguientes
                ID de Diario de Solicitud de pago MXN (SPA04) Original
                ID de Diario de Solicitud de pago ME (SPA03) Original
            NOTA: Debido a que hay una traducción en los datos se revisa los códigos cortos 
            y se obtiene el ID del diario MXN primero y el del ME el segundo en la consulta
        """
        if self.currency_id.name == "MXN":
            self.journal_id=self.env['payment.parameters'].get_param('national_journal')
        else:
            self.journal_id=self.env['payment.parameters'].get_param('foreign_journal')
