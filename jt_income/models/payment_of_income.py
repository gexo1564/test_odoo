from odoo import models, fields,api,_
from odoo.exceptions import ValidationError
from lxml import etree

class PaymentOfIncome(models.Model):

    _name = 'payment.of.income'
    _description = "Payment Of Income"
    
    name = fields.Char("Payment of")
    description = fields.Text("Description")
    

    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        res = super().fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])

        is_group_income_fac_administrator_user = self.env.user.has_group('jt_income.group_income_fac_administrator_user')
     
        if not is_group_income_fac_administrator_user:
            for node in doc.xpath("//" + view_type):
                node.set('export', '0')
                node.set('delete', '0')
                
        res['arch'] = etree.tostring(doc)

        return res
        
    def name_get(self):
        result = []
        for rec in self:
            name = ''
            if rec.name:
                name = rec.name
            if rec.description: 
                name += ' ' + rec.description
                
            result.append((rec.id, name))
        return result

    def unlink(self):
        for this_pay_inc in self:
            is_exist_asign_pay_inc = self.env['account.move'].search([('payment_of_id','=',this_pay_inc.id)],limit=1)
            if is_exist_asign_pay_inc:
                raise ValidationError(_('You cannot delete a Service Key when it is already being used in the Billing records'))
        return super(PaymentOfIncome,self).unlink()

    @api.constrains('name')
    def _check_code(self):
        for rec in self:
            if rec.name:
                deposit_id = self.env['payment.of.income'].search([('name','=',rec.name),('id','!=',rec.id)],limit=1)
                if deposit_id:
                    raise ValidationError(_('There is already a record with the same Payment of'))        
