# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields, api, _
import base64
import openpyxl
from datetime import datetime
from odoo.modules.module import get_resource_path
import io
import sqlite3
import tempfile
from odoo.exceptions import UserError, ValidationError
from odoo.tools.misc import ustr

class LowsAndCancellationWizard(models.TransientModel):
    _name = 'lows.cancellation.wizard'
    _description = "Lows And Cancellation Wizard"

    type_of_movement = fields.Selection([('casualties_cancellations','Casualties and cancellations'),
                                         ],string='Casualties and cancellations')
    file = fields.Binary('File to import')
    filename = fields.Char('FileName')
    payroll_process_id = fields.Many2one('custom.payroll.processing','Payroll Process')
    
    
    def generate(self):
        if self.file:
            postgres_employee_payroll_file_obj_list = self.env['employee.payroll.file'].search([('payroll_processing_id', '=', self.payroll_process_id.id)])
            postgres_selection_field_redundancy_and_cancellation_dict = {key : value for key, value in self.env['employee.payroll.file'].fields_get([], {})['casualties_and_cancellations']['selection']}
            if len(postgres_employee_payroll_file_obj_list) != 0:
                redundancy_and_cancellation_header = ['rfc', 'type']
                file_path_db = tempfile.mkdtemp()
                fortnight = self.payroll_process_id.fornight
                year = self.payroll_process_id.fornight
                redundancy_cancellation_table_name = f'redundancy_cancellation_{fortnight}{year}'

                db_payroll_path = f"{file_path_db}/payroll_{fortnight}{year}.db"
                con = sqlite3.connect(db_payroll_path)
                cur = con.cursor()

                current_query = f"DROP TABLE IF EXISTS {redundancy_cancellation_table_name};"
                cur.execute(current_query)
                redundancy_cancellation_header_table_creation = [element + ' TEXT' for element in redundancy_and_cancellation_header]
                redundancy_cancellation_header_table_creation = ','.join(redundancy_cancellation_header_table_creation)
                current_query = f'CREATE TABLE {redundancy_cancellation_table_name}(id INTEGER PRIMARY KEY,{redundancy_cancellation_header_table_creation});'
                cur.execute(current_query)

                # To open the workbook
                # Workbook object is created
                wb_obj = openpyxl.load_workbook(filename=io.BytesIO(base64.b64decode(self.file)), read_only=True, data_only=True, keep_links=False)

                # Get workbook active sheet object
                # from the active attribute
                sheet_obj = wb_obj.active

                max_row = sheet_obj.max_row
                max_column = sheet_obj.max_column
                
                if max_column == len(redundancy_and_cancellation_header):
                    lite_rfc_dict_tmp = {}
                    postgres_employee_payroll_file_obj_dict = {}
                    for postgres_employee_payroll_file_obj_list_element in postgres_employee_payroll_file_obj_list:
                        postgres_employee_payroll_file_obj_dict[postgres_employee_payroll_file_obj_list_element.employee_id.rfc] = postgres_employee_payroll_file_obj_list_element

                    data_not_found_dict = {
                        'employee_rfc' : [],
                        'duplicated_rfc' : [],
                        'type' : []
                    }

                    counter = 1
                    for i in sheet_obj.iter_rows(values_only=True, min_row=2):
                        rfc = str(i[0]).strip() if i[0] is not None else ''
                        type_rc = str(i[1]).strip() if i[1] is not None else ''
                        
                        if i[0] not in lite_rfc_dict_tmp:
                            lite_rfc_dict_tmp.update({i[0] : ''})
                            if i[0] not in postgres_employee_payroll_file_obj_dict:
                                data_not_found_dict['employee_rfc'].append(i[0])
                        else:
                            data_not_found_dict['duplicated_rfc'].append(i[0])

                        if i[1] not in postgres_selection_field_redundancy_and_cancellation_dict and i[1] not in data_not_found_dict['type']:
                            data_not_found_dict['type'].append(i[1])

                        current_query = f"INSERT INTO {redundancy_cancellation_table_name} VALUES({counter}, \'{rfc}\', \'{type_rc}\');"
                        counter = counter + 1
                        cur.execute(current_query)

                    con.commit()

                    there_is_error = False
                    content = f'###################################################\nDATOS ERRÓNEOS BAJAS Y CANCELACIONES : {datetime.now()}\n##################################################\n\n\n'
                    if len(data_not_found_dict['employee_rfc']) > 0:
                        content = content  + f"\n################################################################\nEmpleados no encontrados(RFC) en la nómina actual para bajas y cancelaciones\n################################################################\n{data_not_found_dict['employee_rfc']}" + '\n\n\n'
                        there_is_error = True
                    if len(data_not_found_dict['duplicated_rfc']) > 0:
                        content = content  + f"\n#######################################################\nEmpleados(RFC) duplicados en archivo de bajas y cancelaciones\n#######################################################\n{data_not_found_dict['duplicated_rfc']}" + '\n\n\n'
                        there_is_error = True
                    if len(data_not_found_dict['type']) > 0:
                        content = content  + f"\n########################################################\nTipos de bajas o cancelaciones no encontradas en sistema\n########################################################\n{data_not_found_dict['type']}" + '\n\n\n'
                        there_is_error = True

                    if not there_is_error:
                        for i in sheet_obj.iter_rows(values_only=True, min_row=2):
                            postgres_employee_payroll_file_obj_dict[i[0]].write({'casualties_and_cancellations' : postgres_selection_field_redundancy_and_cancellation_dict[i[1]]})

                        return {
                            'name': 'Importación exitosa',
                            'type': 'ir.actions.act_window',
                            'view_mode': 'form',
                            'res_model': 'payroll.payment.message.wizard',
                            'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Importación exitosa de bajas y cancelaciones.'}).id,
                            'target': 'new'
                        }
                    else:
                        self.payroll_process_id.update_failed_file(content, 'payroll_redundancy_cancellation', 'completed', self.env.user.id, self.filename)
                        return {
                            'name': 'Error en validación',
                            'type': 'ir.actions.act_window',
                            'view_mode': 'form',
                            'res_model': 'payroll.payment.message.wizard',
                            'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Se encontraron errores durante la validación del archivo de bajas y cancelaciones, corriga los errores e intente nuevamente.'}).id,
                            'target': 'new'
                        }
                else:
                    return {
                        'name': 'Importación',
                        'type': 'ir.actions.act_window',
                        'view_mode': 'form',
                        'res_model': 'payroll.payment.message.wizard',
                        'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Error en importación de bajas y cancelaciones de nómina, número de columnas en archivo no coincide con el número de columnas esperadas.'}).id,
                        'target': 'new'
                    }
            else:
                return {
                    'name': 'Importación',
                    'type': 'ir.actions.act_window',
                    'view_mode': 'form',
                    'res_model': 'payroll.payment.message.wizard',
                    'res_id': self.env['payroll.payment.message.wizard'].create({'message': 'Error en importación de bajas y cancelaciones de nómina, debe importar primero percepciones.'}).id,
                    'target': 'new'
                }
