# -*- coding: utf-8 -*-
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo import api, fields, models, _


class StateIncomeExpensesInvestment(models.AbstractModel):
    _inherit = 'jt_account_design.state.income.expenses'

    filter_all_entries = False


    def _get_lines(self, options, line_id=None):
        lines = []

        start = datetime.strptime(str(options['date'].get('date_from')), '%Y-%m-%d').date()
        end = datetime.strptime(options['date'].get('date_to'), '%Y-%m-%d').date()
        posted_only = not options.get('all_entries')

        if options.get('all_entries') is False:
            move_state_domain = ('move_id.state', '=', 'posted')
        else:
            move_state_domain = ('move_id.state', '!=', 'cancel')
        
        pre_year_date = start - relativedelta(years=1)
        pre_start_date = pre_year_date.replace(month=1, day=1)
        pre_end_date = pre_year_date.replace(month=12, day=31)
        
        
        domain = [('date', '<=', end),move_state_domain]
        pre_domain = [('date', '>=', pre_start_date),('date', '<=', pre_end_date), move_state_domain]
        
        concept_ids = self.env['detailed.statement.income'].search([('inc_exp_type','!=',False)])
        
        list_data = ['income','expenses','investments','other expenses']
        
        remant_exercised = 0
        remant_exercised_pre = 0
        remant_variation = 0

        expenses_exercised = 0
        expenses_exercised_pre = 0
        expenses_variation = 0

        year_exercised = 0
        year_exercised_pre = 0
        year_variation = 0
        
        count = 1
        for type in list_data:
            type_concept_ids = concept_ids.filtered(lambda x:x.inc_exp_type == type)
            major_ids = type_concept_ids.mapped('major_id')
            if type_concept_ids:
                name = type.upper()
                if self.env.lang == 'es_MX' and type == 'income':
                    str1 = 'INGRESOS'
                    name = str1.upper()
                if self.env.lang == 'es_MX' and type == 'expenses':
                    str2 = 'GASTOS'
                    name = str2.upper()
                if self.env.lang == 'es_MX' and type == 'investments':
                    str3 = 'INVERSIONES'
                    name = str3.upper()
                if self.env.lang == 'es_MX' and type == 'other expenses':
                    str4 = 'OTROS GASTOS'
                    name = str4.upper()

                lines.append({
                    'id': type,
                    'name': name,
                    'columns': [
                        {'name': ''},
                        {'name': ''},
                        {'name': ''},
                        {'name': ''},
                    ],
                    'level': 1,
                    'unfoldable': False,
                    'unfolded': True,
                })
            for major in major_ids:
                count += 1
                cons = type_concept_ids.filtered(lambda x:x.major_id.id == major.id)
                lines.append(self.format_line(maj=major, children=cons,
                    cols=[
                        {'name': ''},
                        {'name': ''},
                        {'name': ''},
                        {'name': ''},
                    ],
                    extra_info={
                        'class':'text-left'
                    }
                ))

                for con in cons:
                    account_lines = []
                    total_exercised = 0
                    total_exercised_pre = 0
                    total_variation = 0

                    for acc in con.account_ids:
                        values= self.env['account.move.line'].search(domain + [('account_id', 'in', acc.ids)])
                        exercised = sum(x.credit - x.debit for x in values) / 1000
                        total_exercised += exercised

                        values = self.env['account.move.line'].search(pre_domain + [('account_id', 'in', acc.ids)])
                        exercised_pre = sum(x.credit - x.debit for x in values) / 1000
                        
                        variation = exercised - exercised_pre
                        total_variation += variation
                        total_exercised_pre += exercised_pre

                        per = 0.00
                        if exercised:
                            per = 100.00
                        if exercised_pre != 0:
                            per = round((exercised/exercised_pre)*100, 2)
                        
                        dep_domain = [
                            ('date', '>=', start),
                            ('date', '<=', end),
                            ('parent_state', '=', 'posted') if posted_only else ('parent_state', '!=', 'cancel')
                        ]
                        deps = self.env['dependency'].get_dependencies_for_account(acc,dep_domain)

                        account_lines.append(self.format_line(maj=major, con=con,acc=acc, children=deps, cols=[
                            self._format({'name': exercised},figure_type='float'),
                            self._format({'name': exercised_pre},figure_type='float'),
                            self._format({'name': variation},figure_type='float'),
                            {'name':per,'class':'number'},
                        ]))
                        ################################
                        ####### DEP CALCULATIONS #######
                        ################################
                        for dep in deps:
                            dep_exercised = sum(l.credit - l.debit for l in self.env['account.move.line'].search(
                                domain + [
                                    ('account_id', '=', acc.id),
                                    ('dependency_id', '=', dep.id)
                                ]
                            )) / 1000
                            dep_exercised_pre = sum(l.credit - l.debit for l in self.env['account.move.line'].search(
                                pre_domain + [
                                    ('account_id', '=', acc.id),
                                    ('dependency_id', '=', dep.id)
                                ]
                            )) / 1000
                            dep_variation = dep_exercised - dep_exercised_pre
                            dep_per = 0.00
                            if dep_exercised:
                                dep_per = 100.00
                            if dep_exercised_pre != 0:
                                dep_per = round((dep_exercised/dep_exercised_pre) * 100, 2)
                        ################################
                        ##### END DEP CALCULATIONS #####
                        ################################
                            subdep_domain = [
                                ('date', '>=', start),
                                ('date', '<=', end),
                                ('parent_state', '=', 'posted') if posted_only else ('parent_state', '!=', 'cancel')
                            ]
                            subdeps = self.env['sub.dependency'].get_subdependencies_for_account(acc, dep, subdep_domain)
                        
                            account_lines.append(self.format_line(maj=major, con=con, acc=acc, dep=dep, children=subdeps, cols=[
                                self._format({'name': dep_exercised},figure_type='float'),
                                self._format({'name': dep_exercised_pre},figure_type='float'),
                                self._format({'name': dep_variation},figure_type='float'),
                                {'name': dep_per,'class':'number'}
                            ]))

                            ################################
                            ###### SUBDEP CALCULATIONS #####
                            ################################
                            for subdep in subdeps:
                                subdep_exercised = sum(l.credit - l.debit for l in self.env['account.move.line'].search(
                                    domain + [
                                        ('account_id', '=', acc.id),
                                        ('dependency_id', '=', dep.id),
                                        ('sub_dependency_id', '=', subdep.id)
                                    ]
                                )) / 1000
                                subdep_exercised_pre = sum(l.credit - l.debit for l in self.env['account.move.line'].search(
                                    pre_domain + [
                                        ('account_id', '=', acc.id),
                                        ('dependency_id', '=', dep.id),
                                        ('sub_dependency_id', '=', subdep.id)
                                    ]
                                )) / 1000
                                subdep_variation = subdep_exercised - subdep_exercised_pre
                                subdep_per = 0.00
                                if subdep_exercised:
                                    subdep_per = 100.00
                                if subdep_exercised_pre != 0:
                                    subdep_per = round((subdep_exercised/subdep_exercised_pre) * 100, 2)


                                account_lines.append(self.format_line(maj=major, con=con, acc=acc, dep=dep, subdep=subdep, cols=[
                                    self._format({'name': subdep_exercised},figure_type='float'),
                                    self._format({'name': subdep_exercised_pre},figure_type='float'),
                                    self._format({'name': subdep_variation},figure_type='float'),
                                    { 'name': subdep_per,'class':'number' },
                                ]))

                            ################################
                            #### END SUBDEP CALCULATIONS ###
                            ################################

                    if type == 'income':
                        remant_exercised += total_exercised
                        remant_exercised_pre += total_exercised_pre
                        remant_variation += total_variation
                        
                        year_exercised += total_exercised
                        year_exercised_pre += total_exercised_pre
                        year_variation += total_variation
                        
                    elif type == 'expenses':
                        remant_exercised -= total_exercised
                        remant_exercised_pre -= total_exercised_pre
                        remant_variation -= total_variation
                    
                    if type != 'income':

                        expenses_exercised += total_exercised
                        expenses_exercised_pre += total_exercised_pre
                        expenses_variation += total_variation
                        
                        year_exercised -= total_exercised
                        year_exercised_pre -= total_exercised_pre
                        year_variation -= total_variation

                    total_per = 0.00
                    if total_exercised:
                        total_per = 100.00
                    if total_exercised_pre !=0:
                        total_per = (total_exercised/total_exercised_pre)*100
                        total_per = round(total_per,2)
                    
                    lines.append(self.format_line(maj=major, con=con, children=account_lines, 
                        cols=[
                            self._format({'name': total_exercised},figure_type='float'),
                            self._format({'name': total_exercised_pre},figure_type='float'),
                            self._format({'name': total_variation},figure_type='float'),
                            { 'name':total_per,'class':'number' },
                        ],
                        extra_info={
                            'class': 'text-left'
                        }
                    ))

                    lines += account_lines    
                    lines.append({
                        'id': 'group_total',
                        'name': 'SUMA',
                        'columns': [
                                    self._format({'name': total_exercised},figure_type='float'),
                                    self._format({'name': total_exercised_pre},figure_type='float'),
                                    self._format({'name': total_variation},figure_type='float'),
                                    {'name':total_per,'class':'number'},
                                    ],
                        
                        'level': 1,
                        'unfoldable': False,
                        'unfolded': True,
                        'class':'text-right',
                        'parent_id': 'con' + self.str_id(con) + self.str_id(major),
                    })
            if type=="expenses":
                remant_per = 0.00
                if remant_exercised:
                    remant_per = 100.00
                if remant_exercised_pre != 0:
                    remant_per = (remant_exercised/remant_exercised_pre)*100
                    remant_per = round(remant_per,2)
                    
                lines.append({
                    'id': 'REMNANT',
                    'name': _('REMAINING BEFORE INVESTMENTS'),
                    'columns': [
                                self._format({'name': remant_exercised},figure_type='float'),
                                self._format({'name': remant_exercised_pre},figure_type='float'),
                                self._format({'name': remant_variation},figure_type='float'),
                                {'name':remant_per,'class':'number'},
                                ],
                    
                    'level': 1,
                    'unfoldable': False,
                    'unfolded': True,
                    'class':'text-right'
                })
        expenses_per = 0.00
        if expenses_exercised:
            expenses_per = 100.00
        if expenses_exercised_pre != 0:
            expenses_per = (expenses_exercised/expenses_exercised_pre)*100
            expenses_per = round(expenses_per,2)
            
        lines.append({
            'id': 'Total EXPENSES',
            'name': _('TOTAL EXPENSES, INVESTMENTS AND OTHER EXPENSES'),
            'columns': [
                    self._format({'name': expenses_exercised},figure_type='float'),
                    self._format({'name': expenses_exercised_pre},figure_type='float'),
                    self._format({'name': expenses_variation},figure_type='float'),
                    {'name':expenses_per,'class':'number'},
                    ],
        
            'level': 1,
            'unfoldable': False,
            'unfolded': True,
            'class':'text-right'
        })
        year_per = 0.00
        if year_exercised:
            year_per = 100.00
        if year_exercised_pre != 0:
            year_per = float((year_exercised/year_exercised_pre)*100)
            year_per = round(year_per,2)

        lines.append({
            'id': 'Total Year',
            'name': _('REMAINING OF THE YEAR'),
            'columns': [
                    self._format({'name': year_exercised},figure_type='float'),
                    self._format({'name': year_exercised_pre},figure_type='float'),
                    self._format({'name': year_variation},figure_type='float'),
                    {'name':year_per,'class':'number'},
                    ],
        
            'level': 1,
            'unfoldable': False,
            'unfolded': True,
            'class':'text-right'
        })
        
        return lines