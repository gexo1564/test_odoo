odoo.define('jt_account_module_design.master_report', function(require) {
    'use strict';
    
    var account_reports = require('account_reports.account_report');
    var field_utils = require('web.field_utils');
    var WarningDialog = require('web.CrashManager').WarningDialog;
    var core = require('web.core');

    var _t = core._t;

    account_reports.include({
        render_searchview_buttons: function() {
            this._super.apply(this, arguments); // Call original method
            var self = this;
            
            this.$searchview_buttons.find('#generate_master_report').click(function (event) {
                var error = false;
                if ($(this).data('filter') === 'custom') {

                    var mr_filter_1 = self.$searchview_buttons.find('.o_datepicker_input[name="master_report_filter_1"]');
                    var mr_filter_2 = self.$searchview_buttons.find('.o_datepicker_input[name="master_report_filter_2"]');
                    var mr_filter_3 = self.$searchview_buttons.find('.o_datepicker_input[name="master_report_filter_3"]');
                    var mr_filter_4 = self.$searchview_buttons.find('.o_datepicker_input[name="master_report_filter_4"]');
                    var mr_filter_5 = self.$searchview_buttons.find('.o_datepicker_input[name="master_report_filter_5"]');

                    error = mr_filter_1.val() === "" || mr_filter_2.val() === "" || mr_filter_3.val() === "" || mr_filter_4.val() === "" || mr_filter_5.val() === "";

                    self.report_options.first_date = field_utils.parse.date(mr_filter_1.val())
                    self.report_options.second_date = field_utils.parse.date(mr_filter_2.val())
                    self.report_options.third_date = field_utils.parse.date(mr_filter_3.val())
                    self.report_options.fourth_date = field_utils.parse.date(mr_filter_4.val())
                    self.report_options.fifth_date = field_utils.parse.date(mr_filter_5.val())


                }
                if (error) {
                    new WarningDialog(self, {
                        title: _t("Odoo Warning"),
                    }, {
                        message: _t("Date cannot be empty")
                    }).open();
                } else {
                    self.reload();
                }
            });
        },
    });

});