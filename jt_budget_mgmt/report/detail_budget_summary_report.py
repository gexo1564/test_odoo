# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import datetime
import io
import logging
from odoo import models, fields, api, _
from odoo.tools.misc import xlsxwriter
from odoo.tools.misc import formatLang
from odoo.exceptions import UserError
import json
import math

ACCOUNT_ACCOUNT = 'account.account'
MONETARY_FORMAT = '$#,##0.00'
class DetailsBudgetSummaryReport(models.AbstractModel):
    _name = "details.budget.summary.report.onscreen"
    _inherit = "account.report"
    _description = "Details Budget Summary"

    filter_journals = None
    filter_multi_company = None
    filter_date = {'mode': 'range', 'filter': 'this_month'}
    filter_all_entries = None
    filter_comparison = None
    filter_journals = None
    filter_analytic = None
    filter_unfold_all = None
    filter_hierarchy = None
    filter_partner = None

    # Custom filters
    filter_line_pages = None
#     filter_budget_control = None
#     filter_program_code_section = None

    def _get_reports_buttons(self):
        user = self.env.user
        hide_export_button = [
            'jt_budget_mgmt.group_dependence_user',
        ]
        if any(user.has_group(group) for group in hide_export_button):
            # El usuario tiene el perfil, por lo que el botón XLSX se ocultara
            return []
        else:
            # El usuario no tiene el perfil, por lo que el botón XLSX se mostrará
            return [
                #{'name': _(''), 'sequence': 2, 'action': 'print_pdf', 'file_export_type': _('PDF')},
                {'name': _('Export (XLSX)'), 'sequence': 2, 'action': 'print_xlsx', 'file_export_type': _('XLSX')},
            ]

    def get_xlsx(self, options, response=None):
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        sheet = workbook.add_worksheet(self._get_report_name()[:31])

        date_default_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 0, 'num_format': 'yyyy-mm-dd'})
        date_default_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': 'yyyy-mm-dd'})
        default_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 0, 'num_format': MONETARY_FORMAT})
        default_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': MONETARY_FORMAT})
        title_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'bottom': 2, 'num_format': MONETARY_FORMAT})
        super_col_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'align': 'center'})
        level_0_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 13, 'bottom': 6, 'font_color': '#666666', 'num_format': MONETARY_FORMAT})
        level_1_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 13, 'bottom': 1, 'font_color': '#666666', 'num_format': MONETARY_FORMAT})
        level_2_col1_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 0, 'num_format': MONETARY_FORMAT})
        level_2_col1_total_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'num_format': MONETARY_FORMAT})
        level_2_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'num_format': MONETARY_FORMAT})
        level_3_col1_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'indent': 0, 'num_format': MONETARY_FORMAT})
        level_3_col1_total_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'font_size': 12, 'font_color': '#666666', 'indent': 0, 'num_format': MONETARY_FORMAT})
        level_3_style = workbook.add_format({'font_name': 'Arial', 'font_size': 12, 'font_color': '#666666', 'num_format': MONETARY_FORMAT})

        #Set the first column width to 50
        #sheet.set_column(0, 0, 50)
        super_columns = self._get_super_columns(options)
        sheet.write(0, 0,self._get_report_name(), title_style)
        y_offset = 1
        # Todo in master: Try to put this logic elsewhere
        x = super_columns.get('x_offset', 0)
        for super_col in super_columns.get('columns', []):
            cell_content = super_col.get('string', '').replace('<br/>', ' ').replace('&nbsp;', ' ')
            x_merge = super_columns.get('merge')
            if x_merge and x_merge > 1:
                sheet.merge_range(0, x, 0, x + (x_merge - 1), cell_content, super_col_style)
                x += x_merge
            else:
                sheet.write(0, x, cell_content, super_col_style)
                x += 1
        column_sizes = 0
        for row in self.get_header(options):
            x = 0
            for column in row:
                colspan = column.get('colspan', 1)
                header_label = column.get('name', '').replace('<br/>', ' ').replace('&nbsp;', ' ')
                if colspan == 1:
                    sheet.write(y_offset, x, header_label, title_style)
                else:
                    sheet.merge_range(y_offset, x, y_offset, x + colspan - 1, header_label, title_style)
                x += colspan
                column_sizes += 1
            y_offset += 1
        ctx = self._set_context(options)
        ctx.update({'no_format':True, 'print_mode':True, 'prefetch_fields': False})
        # deactivating the prefetching saves ~35% on get_lines running time
        lines = self.with_context(ctx)._get_lines(options)

        if options.get('hierarchy'):
            lines = self._create_hierarchy(lines, options)
        if options.get('selected_column'):
            lines = self._sort_lines(lines, options)

        #write all data rows
        for y in range(0, len(lines)):
            level = lines[y].get('level')
            if lines[y].get('caret_options'):
                style = level_3_style
                col1_style = level_3_col1_style
            elif level == 0:
                #This line was commented because of Proforma Budget Summary Report(XLSX) adds one empy row per each line with data
                #y_offset += 1
                style = level_0_style
                col1_style = style
            elif level == 1:
                style = level_1_style
                col1_style = style
            elif level == 2:
                style = level_2_style
                col1_style = 'total' in lines[y].get('class', '').split(' ') and level_2_col1_total_style or level_2_col1_style
            elif level == 3:
                style = level_3_style
                col1_style = 'total' in lines[y].get('class', '').split(' ') and level_3_col1_total_style or level_3_col1_style
            else:
                style = default_style
                col1_style = default_col1_style

            #write the first column, with a specific style to manage the indentation
            cell_type, cell_value = self._get_cell_type_value(lines[y])
            if cell_type == 'date':
                sheet.write_datetime(y + y_offset, 0, cell_value, date_default_col1_style)
            else:
                sheet.write(y + y_offset, 0, cell_value, col1_style)

            #write all the remaining cells
            for x in range(1, len(lines[y]['columns']) + 1):
                cell_type, cell_value = self._get_cell_type_value(lines[y]['columns'][x - 1])
                if cell_type == 'date':
                    sheet.write_datetime(y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, date_default_style)
                else:
                    sheet.write(y + y_offset, x + lines[y].get('colspan', 1) - 1, cell_value, style)

        #for i in range(1, column_sizes):
        sheet.set_column(0, 18, 8)
        sheet.set_column(19, column_sizes, 19)

        workbook.close()
        output.seek(0)
        generated_file = output.read()
        output.close()
        return generated_file

    # Set columns based on dynamic options
    def print_xlsx(self, options,test):
        options['is_xlsx_report'] = True
        return {
                'type': 'ir_actions_account_report_download',
                'data': {'model': self.env.context.get('model'),
                         'options': json.dumps(options),
                         'output_format': 'xlsx',
                         'financial_id': self.env.context.get('id'),
                         }
                }
    
#     def _get_templates(self):
#         templates = super(
#             DetailsBudgetSummaryReport, self)._get_templates()
#         templates[
#             'main_table_header_template'] = 'account_reports.main_table_header'
#         templates['main_template'] = 'account_reports.main_template'
#         return templates

    def _get_columns_name(self, options):
        column_list = [
                        {'name': _("Year")},
                        {'name': _("Program")},
                        {'name': _("Sub Program")},
                        {'name': _("Dependency")},
                        {'name': _("Sub Dependency")},
                        {'name': _("Expenditure Item")},
                        {'name': _("Check Digit")},
                        {'name': _("Source of Resource")},
                        {'name': _("Institutional Activity")},
                        {'name': _("Conversion of Budgetary Program")},
                        {'name': _("SHCP items")},
                        {'name': _("Type of Expenditure")},
                        {'name': _("Geographic Location")},
                        {'name': _("Wallet Key")},
                        {'name': _("Type of Project")},
                        {'name': _("Project Number")},
                        {'name': _("Stage")},
                        {'name': _("Type of Agreement")},
                        {'name': _("Agreement Number")},
                        
                        {'name': _("Annual authorized")},
                        {'name': _("Authorized 1st quarter")},
                        {'name': _("Authorized 2nd quarter")},
                        {'name': _("Authorized 3rd quarter")},
                        {'name': _("Authorized 4th quarter")},

                        {'name': _("Annual expansion")},
                        {'name': _("1st quarter expansion")},
                        {'name': _("2nd quarter expansion")},
                        {'name': _("3rd quarter expansion")},
                        {'name': _("4th quarter expansion")},
                        
                        {'name': _("Annual reduction")},
                        {'name': _("Reduction 1st quarter")},
                        {'name': _("Reduction 2nd quarter")},
                        {'name': _("Reduction 3rd quarter")},
                        {'name': _("Reduction 4th quarter")},
                        
                        {'name': _("Annual modified")},
                        {'name': _("Modified 1st quarter")},
                        {'name': _("Modified 2nd quarter")},
                        {'name': _("Modified 3rd quarter")},
                        {'name': _("Modified 4th quarter")},
                        
                        {'name': _("Exercised annually")},
                        {'name': _("Exercised January")},
                        {'name': _("Exercised February")},
                        {'name': _("Exercised March")},
                        {'name': _("Exercised April")},
                        {'name': _("Exercised May")},
                        {'name': _("Exercised June")},
                        {'name': _("Exercised July")},
                        {'name': _("Exercised August")},
                        {'name': _("Exercised September")},
                        {'name': _("Exercised October")},
                        {'name': _("Exercised November")},
                        {'name': _("Exercised December")},

                       ]
        return column_list
    
    @api.model
    def _init_filter_line_pages(self, options, previous_options=None):
        options['is_xlsx_report'] = False
        options['line_pages'] = []
        start = datetime.strptime(
            str(options['date'].get('date_from')), '%Y-%m-%d').date()
        end = datetime.strptime(
            options['date'].get('date_to'), '%Y-%m-%d').date()
                    
        budget_lines = self.env['expenditure.budget.line'].search(
            [('expenditure_budget_id.state', '=', 'validate'),('start_date', '>=', start), ('end_date', '<=', end),('program_code_id','!=',False)])
        program_codes = budget_lines.mapped('program_code_id')
        pages = math.ceil(len(program_codes) / 500)
        line_list = []
        for page in range(1, pages + 1):
            line_list.append(page)

        list_labels = self._context.get('lines_data', line_list)
        counter = 1

        if previous_options and previous_options.get('line_pages'):
            line_pages_map = dict((opt['id'], opt['selected'])
                                  for opt in previous_options['line_pages'] if
                                  opt['id'] != 'divider' and 'selected' in opt and opt['id'] not in previous_options['selected_line_pages'])
        else:
            line_pages_map = {}

        options['selected_line_pages'] = []
        for label in list_labels:
            options['line_pages'].append({
                'id': str(counter),
                'name': str(label),
                'code': str(label),
                'selected': line_pages_map.get(str(counter)),
            })
            if line_pages_map.get(str(counter)):
                options['selected_line_pages'].append(str(label))
            counter += 1

    def _format(self, value,figure_type):
        if self.env.context.get('no_format'):
            return value
        value['no_format_name'] = value['name']
        
        if figure_type == 'float':
            currency_id = self.env.company.currency_id
            if currency_id.is_zero(value['name']):
                # don't print -0.0 in reports
                value['name'] = abs(value['name'])
                value['class'] = 'number text-muted'
            value['name'] = formatLang(self.env, value['name'], currency_obj=currency_id)
            value['class'] = 'number'
            return value
        if figure_type == 'percents':
            value['name'] = str(round(value['name'] * 100, 1)) + '%'
            value['class'] = 'number'
            return value
        value['name'] = round(value['name'], 1)
        return value

    @api.model
    def _get_lines(self, options, line_id=None):
        lines = []
        end = datetime.strptime(
            options['date'].get('date_to'), '%Y-%m-%d').date()

        start = datetime.strptime(
            str("%s/01/01" % (str(end.year))), '%Y/%m/%d').date()

        start_year = start.year
        end_year = end.year

        account_comprometido_id = self.env[ACCOUNT_ACCOUNT].sudo().search(
            [('code', '=', '823.001.001.001')],
            limit=1
        ).id
        account_devengado_id = self.env[ACCOUNT_ACCOUNT].sudo().search(
            [('code', '=', '824.001.001.001')],
            limit=1
        ).id
        account_ejercido_id = self.env[ACCOUNT_ACCOUNT].sudo().search(
            [('code', '=', '825.001.001.001')],
            limit=1
        ).id
        account_pagado_id = self.env[ACCOUNT_ACCOUNT].sudo().search(
            [('code', '=', '826.001.001.001')],
            limit=1
        ).id

        query = f"""
        select
            program_code.year as year,
            program_code.pr as program,
            program_code.sp sub_program,
            program_code.dep dependency,
            program_code.sd sub_dependency,
            program_code.par exp_name,
            program_code.dv check_digit,
            program_code.or resource_origin_id,
            program_code.ai institutional_activity_id,
            program_code.conpp conversion_program,
            program_code.conpa shcp_item,
            program_code.tg type_of_expenditure,
            program_code.ug geographic_location,
            program_code.cc wallet_key,
            program_code.tp type_of_project,
            program_code.np project_number,
            program_code.e stage_identofier,
            program_code.tc type_of_agreement,
            program_code.nc number_of_agreement,
            coalesce(total_authorized,0) authorized,
            coalesce(first_authorized,0) authorized_q1,
            coalesce(second_authorized,0) authorized_q2,
            coalesce(third_authorized,0) authorized_q3,
            coalesce(fourth_authorized,0) authorized_q4,
            coalesce(total_expansion,0) annual_expansion,
            coalesce(first_expansion,0) annual_expansion_q1,
            coalesce(second_expansion,0) annual_expansion_q2,
            coalesce(third_expansion,0) annual_expansion_q3,
            coalesce(fourth_expansion,0) annual_expansion_q4,
            coalesce(total_reduction,0) annual_reduction,
            coalesce(first_reduction,0) annual_reduction_q1,
            coalesce(second_reduction,0) annual_reduction_q2,
            coalesce(third_reduction,0) annual_reduction_q3,
            coalesce(fourth_reduction,0) annual_reduction_q4,
            coalesce(total_authorized,0) + coalesce(total_expansion,0) - coalesce(total_reduction,0) modified,
            coalesce(first_authorized,0) + coalesce(first_expansion,0) - coalesce(first_reduction,0) modified_q1,
            coalesce(second_authorized,0) + coalesce(second_expansion,0) - coalesce(second_reduction,0) modified_q2,
            coalesce(third_authorized,0) + coalesce(third_expansion,0) - coalesce(third_reduction,0) modified_q3,
            coalesce(fourth_authorized,0) + coalesce(fourth_expansion,0) - coalesce(fourth_reduction,0) modified_q4,
            coalesce(anual, 0) total_gasto,
            coalesce(enero, 0) enero,
            coalesce(febrero, 0) febrero,
            coalesce(marzo, 0) marzo,
            coalesce(abril, 0) abril,
            coalesce(mayo, 0) mayo,
            coalesce(junio, 0) junio,
            coalesce(julio, 0) julio,
            coalesce(agosto, 0) agosto,
            coalesce(septiembre, 0) septiembre,
            coalesce(octubre, 0) octubre,
            coalesce(noviembre, 0) noviembre,
            coalesce(diciembre, 0) diciembre
        from (
            select
            pc.id program_code_id,
            yc.name as year,
            pp.key_unam as pr,
            sp.sub_program as sp,
            dp.dependency as dep,
            sdp.sub_dependency as sd,
            expi.item as par,
            pc.check_digit as dv,
            ro.key_origin as or,
            inac.number as ai,
            shcp.name as conpp,
            dc.federal_part as conpa,
            et.key_expenditure_type as tg,
            gl.state_key as ug,
            kw.wallet_password as cc,
            ptype.project_type_identifier as tp,
            projectp.number as np,
            si.stage_identifier as e,
            atype.agreement_type as tc,
            atypen.number_agreement as nc
        from program_code pc, expenditure_item exioder, year_configuration yc,program pp,
            sub_program sp,dependency dp,sub_dependency sdp,expenditure_item expi,
            resource_origin ro,institutional_activity inac,shcp_code shcp,budget_program_conversion bpc,
            departure_conversion as dc,expense_type as et,geographic_location as gl,key_wallet as kw,project_type as ptype,
            project_type as ptypen,project_project projectp,stage as si,agreement_type as atype,
            agreement_type as atypen
        where exioder.id=pc.item_id and pc.year=yc.id and pc.program_id=pp.id and pc.sub_program_id=sp.id and pc.dependency_id=dp.id
            and pc.sub_dependency_id=sdp.id and pc.item_id=expi.id  and pc.resource_origin_id=ro.id
            and pc.institutional_activity_id=inac.id and pc.budget_program_conversion_id=bpc.id and shcp.id=bpc.shcp
            and pc.conversion_item_id=dc.id and pc.expense_type_id=et.id and pc.location_id=gl.id and pc.portfolio_id=kw.id
            and pc.project_type_id=ptype.id and pc.project_type_id=ptypen.id and projectp.id=ptypen.project_id
            and pc.stage_id=si.id and pc.agreement_type_id=atype.id and pc.agreement_type_id=atypen.id
            and to_date(yc.name, 'yyyy') between to_date('{start_year}', 'yyyy') and to_date('{end_year}', 'yyyy')
            and pc.state = 'validated'
        """
        # where conditional based in budget dependence rol #   
        budget_dependence_user = self.env.user.has_group('jt_budget_mgmt.group_dependence_user')
        if budget_dependence_user:
            conditional_based_user = self.get_conditional_based_user_query()
            query += conditional_based_user

        query += f"""    
        ) program_code
        join (
            select *,
                coalesce(first_authorized,0) + coalesce(second_authorized,0) + coalesce(third_authorized, 0) + coalesce(fourth_authorized,0) total_authorized
            from crosstab('
                select program_code_id, extract(quarter from start_date) quarter, coalesce(authorized,0)
                from expenditure_budget_line budget, program_code, year_configuration year
                where budget.program_code_id = program_code.id
                    and program_code.year = year.id
                    and to_date(name, ''yyyy'') between to_date(''{start_year}'', ''yyyy'') and to_date(''{end_year}'', ''yyyy'')
                    and budget_line_yearly_id is not null
                order by program_code_id, quarter
            ', $$VALUES (1), (2), (3), (4)$$)
            as adequacies_lines_report(
                program_code_id integer,
                first_authorized numeric,
                second_authorized numeric,
                third_authorized numeric,
                fourth_authorized numeric
            )
        ) budget
        on program_code.program_code_id = budget.program_code_id
        left join (
            select
                coalesce(adecuaciones.program_code_id, recalendarizaciones.program_code_id) program_code_id,
                coalesce(adecuaciones.first_expansion,0) + coalesce(recalendarizaciones.first_expansion,0) first_expansion,
                coalesce(adecuaciones.second_expansion,0) + coalesce(recalendarizaciones.second_expansion,0) second_expansion,
                coalesce(adecuaciones.third_expansion,0) + coalesce(recalendarizaciones.third_expansion,0) third_expansion,
                coalesce(adecuaciones.fourth_expansion,0) + coalesce(recalendarizaciones.fourth_expansion,0) fourth_expansion,
                coalesce(adecuaciones.first_expansion,0) + coalesce(recalendarizaciones.first_expansion,0)+
                coalesce(adecuaciones.second_expansion,0) + coalesce(recalendarizaciones.second_expansion,0) +
                coalesce(adecuaciones.third_expansion,0) + coalesce(recalendarizaciones.third_expansion,0) +
                coalesce(adecuaciones.fourth_expansion,0) + coalesce(recalendarizaciones.fourth_expansion,0) total_expansion
            from (
                select *
                from crosstab('
                    select program_code_id, quarter, sum(expansion_amount)
                    from adequacies_lines_report where write_date between ''{start}'' and ''{end}''
                    group by program_code_id, quarter order by program_code_id, quarter
                ', $$VALUES (1), (2), (3), (4)$$)
                as adequacies_lines_report(
                    program_code_id integer,
                    first_expansion numeric,
                    second_expansion numeric,
                    third_expansion numeric,
                    fourth_expansion numeric
                )
            ) adecuaciones
            full outer join (
                select *
                from crosstab('
                    select sl.code_id, sl.quarter , sum(sl.amount) from standardization_line sl
                    JOIN standardization s on sl.standardization_id = s.id
                    JOIN account_fiscal_year afy on s.fiscal_year = afy.id
                    where EXTRACT(YEAR FROM afy.date_from) = ''{start_year}''
                    and sl.state = ''authorized''
                    group by sl.code_id, sl.quarter order by sl.code_id, sl.quarter
                ', $$VALUES (1), (2), (3), (4)$$)
                as recalendarizaciones_report(
                    program_code_id integer,
                    first_expansion numeric,
                    second_expansion numeric,
                    third_expansion numeric,
                    fourth_expansion numeric
                )
            ) recalendarizaciones
            on adecuaciones.program_code_id = recalendarizaciones.program_code_id
        ) expansiones
        on program_code.program_code_id = expansiones.program_code_id
        left join (
            select
                coalesce(adecuaciones.program_code_id, recalendarizaciones.program_code_id) program_code_id,
                coalesce(adecuaciones.first_reduction,0) + coalesce(recalendarizaciones.first_reduction,0) first_reduction,
                coalesce(adecuaciones.second_reduction,0) + coalesce(recalendarizaciones.second_reduction,0) second_reduction,
                coalesce(adecuaciones.third_reduction,0) + coalesce(recalendarizaciones.third_reduction,0) third_reduction,
                coalesce(adecuaciones.fourth_reduction,0) + coalesce(recalendarizaciones.fourth_reduction,0) fourth_reduction,
                coalesce(adecuaciones.first_reduction,0) + coalesce(recalendarizaciones.first_reduction,0)+
                coalesce(adecuaciones.second_reduction,0) + coalesce(recalendarizaciones.second_reduction,0) +
                coalesce(adecuaciones.third_reduction,0) + coalesce(recalendarizaciones.third_reduction,0) +
                coalesce(adecuaciones.fourth_reduction,0) + coalesce(recalendarizaciones.fourth_reduction,0) total_reduction
            from (
                select *
                from crosstab('
                    select program_code_id, quarter, sum(reduction_amount)
                    from adequacies_lines_report where write_date between ''{start}'' and ''{end}''
                    group by program_code_id, quarter order by program_code_id, quarter
                ', $$VALUES (1), (2), (3), (4)$$)
                as adequacies_lines_report(
                    program_code_id integer,
                    first_reduction numeric,
                    second_reduction numeric,
                    third_reduction numeric,
                    fourth_reduction numeric
                )
            ) adecuaciones
            full outer join (
                select *
                from crosstab('
                    select sl.code_id, sl.origin_id , sum(sl.amount) from standardization_line sl
                    JOIN standardization s on sl.standardization_id = s.id
                    JOIN account_fiscal_year afy on s.fiscal_year = afy.id
                    where EXTRACT(YEAR FROM afy.date_from) = ''{start_year}''
                    and sl.state = ''authorized''
                    group by sl.code_id, sl.origin_id order by sl.code_id, sl.origin_id
                ', $$VALUES (1), (2), (3), (4)$$)
                as recalendarizaciones_report(
                    program_code_id integer,
                    first_reduction numeric,
                    second_reduction numeric,
                    third_reduction numeric,
                    fourth_reduction numeric
                )
            ) recalendarizaciones
            on adecuaciones.program_code_id = recalendarizaciones.program_code_id
        ) reducciones
        on program_code.program_code_id = reducciones.program_code_id
        left join (
            select *,
                coalesce(enero,0) + coalesce(febrero,0) + coalesce(marzo,0) +
                coalesce(abril,0) + coalesce(mayo,0) + coalesce(junio,0) +
                coalesce(julio,0) + coalesce(agosto,0) + coalesce(septiembre,0) +
                coalesce(octubre,0) + coalesce(noviembre,0) + coalesce(diciembre,0) anual
            from crosstab('
                select l.program_code_id, extract(month from l.date) quarter, sum(debit-credit)
                from account_move m, account_move_line l
                where m.id = l.move_id
                    and l.account_id in ({account_comprometido_id}, {account_devengado_id}, {account_ejercido_id}, {account_pagado_id})
                    and l.date between ''{start}'' and ''{end}''
                    and m.state = ''posted''
                group by l.program_code_id, quarter order by l.program_code_id, quarter
            ', $$VALUES (1), (2), (3), (4), (5), (6), (7), (8), (9), (10), (11), (12)$$)
            as adequacies_lines_report(
                program_code_id integer,
                enero       numeric,
                febrero     numeric,
                marzo       numeric,
                abril       numeric,
                mayo        numeric,
                junio       numeric,
                julio       numeric,
                agosto      numeric,
                septiembre  numeric,
                octubre     numeric,
                noviembre   numeric,
                diciembre   numeric
            )
        ) gastos
        on program_code.program_code_id = gastos.program_code_id
        """

        self.env.cr.execute(query,())
        my_datas = self.env.cr.dictfetchall()
#        return lines

        selected_line_pages = options['selected_line_pages']
        selected_my_data = []

        for s in selected_line_pages:
            s = int(s)
            start = (s - 1) * 500
            end = s * 500
            my_datas
            selected_my_data = selected_my_data + my_datas[start:end]
        #===== 2nd =======
        total_authorized = 0
        total_authorized_q1 = 0
        total_authorized_q2 = 0
        total_authorized_q3 = 0
        total_authorized_q4 = 0
        #===== 3nd =======
        total_annual_expansion = 0
        total_annual_expansion_q1 = 0
        total_annual_expansion_q2 = 0
        total_annual_expansion_q3 = 0
        total_annual_expansion_q4 = 0
        #===== 4th =======
        total_annual_reduction = 0
        total_annual_reduction_q1 = 0
        total_annual_reduction_q2 = 0
        total_annual_reduction_q3 = 0
        total_annual_reduction_q4 = 0
        #===== 5th =======
        total_modified = 0
        total_modified_q1 = 0
        total_modified_q2 = 0
        total_modified_q3 = 0
        total_modified_q4 = 0

        total_gasto = 0
        total_enero = 0
        total_febrero = 0
        total_marzo = 0
        total_abril = 0
        total_mayo = 0
        total_junio = 0
        total_julio = 0
        total_agosto = 0
        total_septiembre = 0
        total_octubre = 0
        total_noviembre = 0
        total_diciembre = 0

        if not selected_line_pages:
            selected_my_data = my_datas[:500]
        if 'is_xlsx_report' in options and options['is_xlsx_report'] == True:
            selected_my_data = my_datas

        for data in selected_my_data:
            # Calculo de las sumas del footer
            # Autorizados
            total_authorized += data.get('authorized')
            total_authorized_q1 += data.get('authorized_q1')
            total_authorized_q2 += data.get('authorized_q2')
            total_authorized_q3 += data.get('authorized_q3')
            total_authorized_q4 += data.get('authorized_q4')

            # Expansiones
            total_annual_expansion += data.get('annual_expansion')
            total_annual_expansion_q1 += data.get('annual_expansion_q1')
            total_annual_expansion_q2 += data.get('annual_expansion_q2')
            total_annual_expansion_q3 += data.get('annual_expansion_q3')
            total_annual_expansion_q4 += data.get('annual_expansion_q4')

            # Reducciones
            total_annual_reduction += data.get('annual_reduction')
            total_annual_reduction_q1 += data.get('annual_reduction_q1')
            total_annual_reduction_q2 += data.get('annual_reduction_q2')
            total_annual_reduction_q3 += data.get('annual_reduction_q3')
            total_annual_reduction_q4 += data.get('annual_reduction_q4')

            # Modificado
            total_modified += data.get('modified')
            total_modified_q1 += data.get('modified_q1')
            total_modified_q2 += data.get('modified_q2')
            total_modified_q3 += data.get('modified_q3')
            total_modified_q4 += data.get('modified_q4')

            # Gastos
            total_gasto += data.get('total_gasto')
            total_enero += data.get('enero')
            total_febrero += data.get('febrero')
            total_marzo += data.get('marzo')
            total_abril += data.get('abril')
            total_mayo += data.get('mayo')
            total_junio += data.get('junio')
            total_julio += data.get('julio')
            total_agosto += data.get('agosto')
            total_septiembre += data.get('septiembre')
            total_octubre += data.get('octubre')
            total_noviembre += data.get('noviembre')
            total_diciembre += data.get('diciembre')

            lines.append({
                'id': 'hierarchy1',
                'name': data.get('year'),
                'columns': [
                            {'name':data.get('program')},
                            {'name':data.get('sub_program')},
                            {'name':data.get('dependency')},
                            {'name':data.get('sub_dependency')},
                            {'name':data.get('exp_name')},
                            {'name':data.get('check_digit')},
                            {'name':data.get('resource_origin_id')},
                            {'name':data.get('institutional_activity_id')},
                            {'name':data.get('conversion_program')},
                            {'name':data.get('shcp_item')},
                            {'name':data.get('type_of_expenditure')},
                            {'name':data.get('geographic_location')},
                            {'name':data.get('wallet_key')},
                            {'name':data.get('type_of_project')},
                            {'name':data.get('project_number')},
                            {'name':data.get('stage_identofier')},
                            {'name':data.get('type_of_agreement')},
                            {'name':data.get('number_of_agreement')},

                            self._format({'name': data.get('authorized')},figure_type='float'),
                            self._format({'name': data.get('authorized_q1')},figure_type='float'),
                            self._format({'name': data.get('authorized_q2')},figure_type='float'),
                            self._format({'name': data.get('authorized_q3')},figure_type='float'),
                            self._format({'name': data.get('authorized_q4')},figure_type='float'),

                            self._format({'name': data.get('annual_expansion')},figure_type='float'),
                            self._format({'name': data.get('annual_expansion_q1')},figure_type='float'),
                            self._format({'name': data.get('annual_expansion_q2')},figure_type='float'),
                            self._format({'name': data.get('annual_expansion_q3')},figure_type='float'),
                            self._format({'name': data.get('annual_expansion_q4')},figure_type='float'),

                            self._format({'name': data.get('annual_reduction')},figure_type='float'),
                            self._format({'name': data.get('annual_reduction_q1')},figure_type='float'),
                            self._format({'name': data.get('annual_reduction_q2')},figure_type='float'),
                            self._format({'name': data.get('annual_reduction_q3')},figure_type='float'),
                            self._format({'name': data.get('annual_reduction_q4')},figure_type='float'),

                            self._format({'name': data.get('modified')},figure_type='float'),
                            self._format({'name': data.get('modified_q1')},figure_type='float'),
                            self._format({'name': data.get('modified_q2')},figure_type='float'),
                            self._format({'name': data.get('modified_q3')},figure_type='float'),
                            self._format({'name': data.get('modified_q4')},figure_type='float'),

                            self._format({'name': data.get('total_gasto')},figure_type='float'),
                            self._format({'name': data.get('enero')},figure_type='float'),
                            self._format({'name': data.get('febrero')},figure_type='float'),
                            self._format({'name': data.get('marzo')},figure_type='float'),
                            self._format({'name': data.get('abril')},figure_type='float'),
                            self._format({'name': data.get('mayo')},figure_type='float'),
                            self._format({'name': data.get('junio')},figure_type='float'),
                            self._format({'name': data.get('julio')},figure_type='float'),
                            self._format({'name': data.get('agosto')},figure_type='float'),
                            self._format({'name': data.get('septiembre')},figure_type='float'),
                            self._format({'name': data.get('octubre')},figure_type='float'),
                            self._format({'name': data.get('noviembre')},figure_type='float'),
                            self._format({'name': data.get('diciembre')},figure_type='float'),
                            ],
                'level': 3,
                'unfoldable': False,
                'unfolded': True,
            })
        lines.append({
            'id': 'hierarchy1',
            'name': '',
            'columns': [
                        {'name':''},
                        {'name':''},
                        {'name':''},
                        {'name':''},
                        {'name':''},
                        {'name':''},
                        {'name':''},
                        {'name':''},
                        {'name':''},
                        {'name':''},
                        {'name':''},
                        {'name':''},
                        {'name':''},
                        {'name':''},
                        {'name':''},
                        {'name':''},
                        {'name':''},
                        {'name':''},

                        self._format({'name': total_authorized},figure_type='float'),
                        self._format({'name': total_authorized_q1},figure_type='float'),
                        self._format({'name': total_authorized_q2},figure_type='float'),
                        self._format({'name': total_authorized_q3},figure_type='float'),
                        self._format({'name': total_authorized_q4},figure_type='float'),

                        self._format({'name': total_annual_expansion},figure_type='float'),
                        self._format({'name': total_annual_expansion_q1},figure_type='float'),
                        self._format({'name': total_annual_expansion_q2},figure_type='float'),
                        self._format({'name': total_annual_expansion_q3},figure_type='float'),
                        self._format({'name': total_annual_expansion_q4},figure_type='float'),

                        self._format({'name': total_annual_reduction},figure_type='float'),
                        self._format({'name': total_annual_reduction_q1},figure_type='float'),
                        self._format({'name': total_annual_reduction_q2},figure_type='float'),
                        self._format({'name': total_annual_reduction_q3},figure_type='float'),
                        self._format({'name': total_annual_reduction_q4},figure_type='float'),

                        self._format({'name': total_modified},figure_type='float'),
                        self._format({'name': total_modified_q1},figure_type='float'),
                        self._format({'name': total_modified_q2},figure_type='float'),
                        self._format({'name': total_modified_q3},figure_type='float'),
                        self._format({'name': total_modified_q4},figure_type='float'),

                        self._format({'name': total_gasto},figure_type='float'),
                        self._format({'name': total_enero},figure_type='float'),
                        self._format({'name': total_febrero},figure_type='float'),
                        self._format({'name': total_marzo},figure_type='float'),
                        self._format({'name': total_mayo},figure_type='float'),
                        self._format({'name': total_abril},figure_type='float'),
                        self._format({'name': total_junio},figure_type='float'),
                        self._format({'name': total_julio},figure_type='float'),
                        self._format({'name': total_agosto},figure_type='float'),
                        self._format({'name': total_septiembre},figure_type='float'),
                        self._format({'name': total_octubre},figure_type='float'),
                        self._format({'name': total_noviembre},figure_type='float'),
                        self._format({'name': total_diciembre},figure_type='float'),
                    ],
            'level': 1,
            'unfoldable': False,
            'unfolded': True,
        })
        return lines

    def _get_report_name(self):
        context = self.env.context
        date_report = fields.Date.from_string(context['date_from']) if context.get(
            'date_from') else fields.date.today()
        name = ''
        if self.env.user.lang == 'es_MX':
            name = 'Reporte Detallado de Presupuesto'
        else:
            name = 'Details Budget Report'
        return '%s_%s_%s' % (
            name,
            date_report.year,
            str(date_report.month).zfill(2))
    

    def get_conditional_based_user_query(self):
        user_dependencies = self.env.user.dependency_budget_ids.ids
        user_sub_dependencies = self.env.user.sub_dependency_budget_ids.ids
        user_programas = self.env.user.program_budget_ids.ids
        user_subprograms = self.env.user.subprogram_budget_ids.ids
        user_departure_groups = self.env.user.departure_group_budget_ids.ids
        # Verificar si se tienen dependencias, subdependencias, programa o subprograma para generar la consulta 
        if not (user_dependencies or user_sub_dependencies or user_programas or user_subprograms or user_departure_groups):
            raise UserError(_('The user has no dependencies, subdependencies, program, subprogram or departure group assigned to query.'))

        conditional_query = ""

        # Agregar condiciones si están presentes
        if user_dependencies:
            conditional_query += f""" AND pc.dependency_id IN {tuple(user_dependencies + [0])}"""
        
        if user_sub_dependencies:
            conditional_query += f""" AND pc.sub_dependency_id IN {tuple(user_sub_dependencies + [0])}"""

        if user_programas:
            conditional_query += f""" AND pc.program_id IN {tuple(user_programas + [0])}"""
        
        if user_subprograms:
            conditional_query += f""" AND pc.sub_program_id IN {tuple(user_subprograms + [0])}"""

        if user_departure_groups:
            conditional_query += f""" AND exioder.departure_group_id IN {tuple(user_departure_groups + [0])}"""    

        return conditional_query