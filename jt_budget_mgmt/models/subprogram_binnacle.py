from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from datetime import datetime


class SubprogramBinnacle(models.Model):

	_name = 'subprogram.binnacle'
	_description = 'Subprogram Binnacle'

	subprogram_id = fields.Many2one('sub.program',string="SP")
	dependency_str = fields.Char("Dependency")
	sub_dependency_str = fields.Char("Subdependency")
	program_str = fields.Char("Program")
	subprogram_str = fields.Char("Subprogram")
	ug_str = fields.Char("UG")
	updated = fields.Char("Updated")
