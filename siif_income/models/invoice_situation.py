from odoo import models, fields,api,_
from odoo.exceptions import ValidationError

class InvoiceSituation(models.Model):

    _name = 'invoice.situation'
    _description = "Invoice situation"
    _rec_name = 'description'

    description = fields.Char("Description")
    key_situation = fields.Char("Key situation")