# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError

class ResPartnerBank(models.Model):

    _inherit = 'res.partner.bank'

    current_balance = fields.Monetary('Current Balance')
    committed_balance = fields.Monetary('Committed Balance')
    available_balance = fields.Monetary('Available Balance')

    def set_current_balance(self, current_balance):
        self.current_balance = current_balance
        self.available_balance = round(self.current_balance - self.committed_balance, 2)

    def add_expense(self, expense):
        self.committed_balance = round(self.committed_balance + expense, 2)
        self.available_balance = round(self.current_balance - self.committed_balance, 2)

    def confirm_expense(self, expense):
        self.committed_balance = round(self.committed_balance - expense, 2)

    def cancel_expense(self, expense):
        self.committed_balance = round(self.committed_balance - expense, 2)
        self.available_balance = round(self.available_balance + expense, 2)