from odoo import fields, models


class ExcelDetailedInformation(models.AbstractModel):
    _name = 'excel.detailed.information'
    _description = 'Common behaviour for records which will be printed in a excel file'

    excel_display_name = fields.Text(string='Excel Display Name', compute='_compute_excel_display_name')
    
    def _compute_excel_display_name(self):
        return NotImplemented
