# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields, api, _
import base64
import io
from datetime import datetime, timedelta

class CustomPayrollProcessing(models.Model):

    _name = 'custom.payroll.processing'
    _description = "Custom Payroll Processing"
    

    name = fields.Char("Name")

    period_start = fields.Date("Period Start")
    period_end = fields.Date("Period End")
    fornight = fields.Selection([('01', '01'), ('02', '02'), ('03', '03'), ('04', '04'), ('05', '05'),
                                      ('06', '06'), ('07', '07'), ('08', '08'), ('09', '09'), ('10', '10'),
                                      ('11', '11'), ('12', '12'), ('13', '13'), ('14', '14'), ('15', '15'),
                                      ('16', '16'), ('17', '17'), ('18', '18'), ('19', '19'), ('20', '20'),
                                      ('21', '21'), ('22', '22'), ('23', '23'), ('24', '24')],
                                string="Fornight")

    payroll_type = fields.Selection(selection=[('payroll_benefit_payment', 'Payroll and Benefit Payment'),
                                               ('special_payroll_payment', 'Special Payroll Payment')], string='Payroll Type')
    
    payroll_ids = fields.One2many('employee.payroll.file','payroll_processing_id')
    total_record = fields.Integer(compute='get_total_record',string='Payment Receipt')

    failed_row_file = fields.Binary(string='Failed Rows File')
    fialed_row_filename = fields.Char(string='File name', default=lambda self: _("Failed_Rows.txt"))
    

    perception_file = fields.Binary('File to import')
    perception_filename = fields.Char('FileName')
    perception_file_index = fields.Integer(default=0)
    perception_file_load = fields.Boolean(default=False)
    perception_hide = fields.Boolean(default=True)
    perception_user = fields.Many2one('res.users')
    
    deductions_file = fields.Binary('File to import')
    deductions_filename = fields.Char('FileName')
    deductions_file_index = fields.Integer(default=0)
    deductions_file_load = fields.Boolean(default=False)
    deductions_user = fields.Many2one('res.users')
    deductions_hide = fields.Boolean(default=True)
    
    failed_log_ids = fields.One2many("payroll.processing.failed.log",'payroll_processing_id','Failed Logs')
    
    def get_total_record(self):
        for rec in self:
            rec.total_record = len(rec.payroll_ids)
            
    def generate_payroll(self):
        return {
                'name': _('Generate Payroll'),
                'res_model':'generate.payroll.wizard',
                'view_mode': 'form',
                'view_id': self.env.ref('jt_payroll_payment.generate_payroll_wizard_view1').id,
                'context': {'default_payroll_process_id': self.id},
                'target': 'new',
                'type': 'ir.actions.act_window',
            }

    def generate_adjustment(self):
        return {
                'name': _('Generate Adjustment'),
                'res_model':'adjusted.payroll.wizard',
                'view_mode': 'form',
                'view_id': self.env.ref('jt_payroll_payment.adjusted_payroll_wizard_view1').id,
                'context': {'default_payroll_process_id': self.id},
                'target': 'new',
                'type': 'ir.actions.act_window',
            }

    def lows_and_cancellations(self):
        return {
                'name': _('Lows And Cancellations'),
                'res_model':'lows.cancellation.wizard',
                'view_mode': 'form',
                'view_id': self.env.ref('jt_payroll_payment.lows_cancellation_wizard_view1').id,
                'context': {'default_payroll_process_id': self.id},
                'target': 'new',
                'type': 'ir.actions.act_window',
            }

        
    def view_payment_receipt(self):
        return {
                'name': _('Payroll'),
                'res_model':'employee.payroll.file',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'views': [(self.env.ref('jt_payroll_payment.employee_payroll_file_tree_process').id, 'tree'), (self.env.ref("jt_payroll_payment.employee_payroll_file_form_processing").id, 'form')],
                'context': {
                    'show_category_name': True,
                    # 'payroll_payment_request': True,
                },
                'target': 'current',
                'type': 'ir.actions.act_window',
                'domain':[('id','in',self.payroll_ids.ids)]
            }

    def update_failed_file(self, failed_row, type_of_movement, state, user_id, import_file_name):
        if len(failed_row) > 0:
            vals = {
                    'payroll_processing_id': self.id,
                    'import_file_name': import_file_name,
                    'user_id': self.env.user.id,
                    'type_of_movement': type_of_movement,
                    'state': state,
            }

            failed_line_id = self.env['payroll.processing.failed.log'].create(vals)
            failed_data = base64.b64encode(failed_row.encode('utf-8'))
            failed_line_id.failed_row_file = failed_data
            failed_line_id.state = 'completed'



class PayrollProcessingFailedLog(models.Model):
    
    _name = 'payroll.processing.failed.log'
    _order = 'id desc'
    
    payroll_processing_id = fields.Many2one('custom.payroll.processing','Payroll processing')
    import_file_name = fields.Char("Import File Name")
    user_id = fields.Many2one('res.users','Imported by')
    failed_row_file = fields.Binary(string='Failed Rows File')
    fialed_row_filename = fields.Char(string='Log File', default=lambda self: _("Failed_Rows.txt"))
        
    type_of_movement = fields.Selection([('payroll_perceptions','Payroll Perceptions'),
                                         ('payroll_deductions','Payroll Deductions'),
                                         ('alimony_payments','Alimony Payments'),
                                         ('additional_payments','Additional Payments'),
                                         ('additional_pension_payments','Additional Pension Payments'),
                                         ('payroll_perception_adjustment', 'Payroll Perception Adjustment'),
                                         ('payroll_deduction_adjustment', 'Payroll Deduction Adjustment'),
                                         ('payroll_adjustment', 'Payroll Adjustment'),
                                         ('payroll_redundancy_cancellation', 'Redundancy and Cancellation'),
                                         ],string='Type of Movement')
    
    state = fields.Selection([('in_progress','In Progress'),
                                         ('completed','Completed'),
                                         ],string='Status')
    
    process_date_time = fields.Datetime('Process Date',default=lambda self: fields.Datetime.now())
    

    def download_file(self):
        self.ensure_one()
        return {
                'type': 'ir.actions.act_url',
                'target':'download',
                 'url': "web/content/?model=payroll.processing.failed.log&id=" + str(self.id) + "&filename_field=fialed_row_filename&field=failed_row_file&download=true&filename=" + self.fialed_row_filename,
            }