# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields, api, _
from datetime import datetime, timedelta
import calendar
from xlrd import *
import xlwt
import base64
from io import BytesIO
import math
from odoo.exceptions import ValidationError, UserError

ACCOUNT_ACCOUNT = 'account.account'

class DetailsBudgetSummaryReport(models.TransientModel):

    _name = 'details.budget.summary.report'
    _description = 'Details Budget Summary Report'

    state = fields.Selection([('draft', 'Draft'), ('request', 'Request'),
                              ('download', 'Download')], default='draft')

    # Date period related fields
    filter_date = fields.Selection([
        ('this_month', 'This Month'),
        ('this_quarter', 'This Quarter'),
        ('this_year', 'This Financial Year'),
        ('last_month', 'Last Month'),
        ('last_quarter', 'Last Quarter'),
        ('last_year', 'Last Financial Year'),
        ('custom', 'Custom'),
    ])

    start_date = fields.Date(string='Start Date')
    end_date = fields.Date(string='End Date')

    name = fields.Char("Name")
    report_file = fields.Binary("Download Report", filters='.xls')

               
    @api.onchange('filter_date')
    def onchange_filter(self):
        if self.filter_date:
            date = datetime.now()
            filter_date = self.filter_date
            if filter_date == 'this_month':
                self.start_date = date.replace(day=1)
                self.end_date = date.replace(
                    day=calendar.monthrange(date.year, date.month)[1])
            elif filter_date == 'this_quarter':
                month = date.month
                if month >= 1 and month <= 3:
                    self.start_date = date.replace(month=1, day=1)
                    self.end_date = date.replace(month=3, day=31)
                elif month >= 4 and month <= 6:
                    self.start_date = date.replace(month=4, day=1)
                    self.end_date = date.replace(month=6, day=30)
                elif month >= 7 and month <= 9:
                    self.start_date = date.replace(month=7, day=1)
                    self.end_date = date.replace(month=9, day=30)
                else:
                    self.start_date = date.replace(month=10, day=1)
                    self.end_date = date.replace(month=12, day=31)
            elif filter_date == 'this_year':
                self.start_date = date.replace(month=1, day=1)
                self.end_date = date.replace(month=12, day=31)
            elif filter_date == 'last_month':
                today = datetime.today()
                first = today.replace(day=1)
                last_month_date = first - timedelta(days=1)
                last_month = last_month_date.month
                if last_month == 12:
                    year = date.year - 1
                    self.start_date = date.replace(
                        day=1, month=last_month, year=year)
                    self.end_date = date.replace(day=calendar.monthrange(date.year, last_month)[1], month=last_month,
                                                 year=year)
                else:
                    self.start_date = date.replace(day=1, month=last_month)
                    self.end_date = date.replace(day=calendar.monthrange(
                        date.year, last_month)[1], month=last_month)
            elif filter_date == 'last_quarter':
                month = date.month
                year = date.year
                if month >= 1 and month <= 3:
                    self.start_date = date.replace(month=1, day=1, year=year)
                    self.end_date = date.replace(month=12, day=31, year=year)
                elif month >= 4 and month <= 6:
                    self.start_date = date.replace(month=1, day=1)
                    self.end_date = date.replace(month=3, day=31)
                elif month >= 7 and month <= 9:
                    self.start_date = date.replace(month=4, day=1)
                    self.end_date = date.replace(month=6, day=30)
                else:
                    self.start_date = date.replace(month=7, day=1)
                    self.end_date = date.replace(month=9, day=30)
            elif filter_date == 'last_year':
                year = date.year - 1
                self.start_date = date.replace(month=1, day=1, year=year)
                self.end_date = date.replace(month=12, day=31, year=year)

    def add_report_header(self,row,col,header_style,ws1):
        columnwidth = {}
        def write(row, col, data, style):
            if col in columnwidth:
                if len(data) > columnwidth[col]:
                    columnwidth[col] = len(data)
            else:
                columnwidth[col] = len(data)
            ws1.write(row, col, data, style)
            return col + 1
        if self.env.user.lang == 'es_MX':
            col = write(row, col, 'AÑO', header_style)
            col = write(row, col, 'PR', header_style)
            col = write(row, col, 'SP', header_style)
            col = write(row, col, 'DEP', header_style)
            col = write(row, col, 'SD', header_style)
            col = write(row, col, 'PAR', header_style)
            col = write(row, col, 'DV', header_style)
            col = write(row, col, 'OR', header_style)
            col = write(row, col, 'AI', header_style)
            col = write(row, col, 'CONPP', header_style)
            col = write(row, col, 'CONPA', header_style)
            col = write(row, col, 'TG', header_style)
            col = write(row, col, 'UG', header_style)
            col = write(row, col, 'CC', header_style)
            col = write(row, col, 'TP', header_style)
            col = write(row, col, 'NP', header_style)
            col = write(row, col, 'E', header_style)
            col = write(row, col, 'TC', header_style)
            col = write(row, col, 'NC', header_style)
            #==== 2nd section ===#
            col = write(row, col, 'Autorizado Anual', header_style)
            col = write(row, col, 'Aut_1_Trim', header_style)
            col = write(row, col, 'Aut_2_Trim', header_style)
            col = write(row, col, 'Aut_3_Trim', header_style)
            col = write(row, col, 'Aut_4_Trim', header_style)
            #=====3rd section====#
            col = write(row, col, 'Ampliación Anual', header_style)
            col = write(row, col, 'Amp_1_Trim', header_style)
            col = write(row, col, 'Amp_2_Trim', header_style)
            col = write(row, col, 'Amp_3_Trim', header_style)
            col = write(row, col, 'Amp_4_Trim', header_style)
            #=======4th section======#
            col = write(row, col, 'Reducción Anual', header_style)
            col = write(row, col, 'Red_1_Trim', header_style)
            col = write(row, col, 'Red_2_Trim', header_style)
            col = write(row, col, 'Red_3_Trim', header_style)
            col = write(row, col, 'Red_4_Trim', header_style)
            #=== 5th Section ====#
            col = write(row, col, 'Modificado Anual', header_style)
            col = write(row, col, 'Mod_1_Trim', header_style)
            col = write(row, col, 'Mod_2_Trim', header_style)
            col = write(row, col, 'Mod_3_Trim', header_style)
            col = write(row, col, 'Mod_4_Trim', header_style)
            #=== 6th section ====#
            col = write(row, col, 'Ejercido Anual', header_style)
            col = write(row, col, 'Ejer_ene', header_style)
            col = write(row, col, 'Ejer_feb', header_style)
            col = write(row, col, 'Ejer_mar', header_style)
            col = write(row, col, 'Ejer_abr', header_style)
            col = write(row, col, 'Ejer_may', header_style)
            col = write(row, col, 'Ejer_jun', header_style)
            col = write(row, col, 'Ejer_jul', header_style)
            col = write(row, col, 'Ejer_ago', header_style)
            col = write(row, col, 'Ejer_sep', header_style)
            col = write(row, col, 'Ejer_oct', header_style)
            col = write(row, col, 'Ejer_nov', header_style)
            col = write(row, col, 'Ejer_dic', header_style)
            write(row, col, 'Disponible_Anual', header_style)
            col += 1
            row+=1
        else:
            #=== 1st section ====#
            col = write(row, col, 'Year', header_style)
            col = write(row, col, 'Program', header_style)
            col = write(row, col, 'Sub Program', header_style)
            col = write(row, col, 'Dependency', header_style)
            col = write(row, col, 'Sub Dependency', header_style)
            col = write(row, col, 'Expenditure Item', header_style)
            col = write(row, col, 'Check Digit', header_style)
            col = write(row, col, 'Source of Resource', header_style)
            col = write(row, col, 'Institutional Activity', header_style)
            col = write(row, col, 'Conversion of Budgetary Program', header_style)
            col = write(row, col, 'SHCP items', header_style)
            col = write(row, col, 'Type of Expenditure', header_style)
            col = write(row, col, 'Geographic Location', header_style)
            col = write(row, col, 'Wallet Key', header_style)
            col = write(row, col, 'Type of Project', header_style)
            col = write(row, col, 'Project Number', header_style)
            col = write(row, col, 'Stage', header_style)
            col = write(row, col, 'Type of Agreement', header_style)
            col = write(row, col, 'Agreement Number', header_style)
            #==== 2nd section ===#
            col = write(row, col, 'Annual authorized', header_style)
            col = write(row, col, 'Authorized 1st quarter', header_style)
            col = write(row, col, 'Authorized 2nd quarter', header_style)
            col = write(row, col, 'Authorized 3rd quarter', header_style)
            col = write(row, col, 'Authorized 4th quarter', header_style)
            #=====3rd section====#
            col = write(row, col, 'Annual expansion', header_style)
            col = write(row, col, '1st quarter expansion', header_style)
            col = write(row, col, '2nd quarter expansion', header_style)
            col = write(row, col, '3rd quarter expansion', header_style)
            col = write(row, col, '4th quarter expansion', header_style)
            #=======4th section======#
            col = write(row, col, 'Annual reduction', header_style)
            col = write(row, col, 'Reduction 1st quarter', header_style)
            col = write(row, col, 'Reduction 2nd quarter', header_style)
            col = write(row, col, 'Reduction 3rd quarter', header_style)
            col = write(row, col, 'Reduction 4th quarter', header_style)
            #=== 5th Section ====#
            col = write(row, col, 'Annual modified', header_style)
            col = write(row, col, 'Modified 1st quarter', header_style)
            col = write(row, col, 'Modified 2nd quarter', header_style)
            col = write(row, col, 'Modified 3rd quarter', header_style)
            col = write(row, col, 'Modified 4th quarter', header_style)
            #=== 6th section ====#
            col = write(row, col, 'Exercised annually', header_style)
            col = write(row, col, 'Exercised January', header_style)
            col = write(row, col, 'Exercised February', header_style)
            col = write(row, col, 'Exercised March', header_style)
            col = write(row, col, 'Exercised April', header_style)
            col = write(row, col, 'Exercised May', header_style)
            col = write(row, col, 'Exercised June', header_style)
            col = write(row, col, 'Exercised July', header_style)
            col = write(row, col, 'Exercised August', header_style)
            col = write(row, col, 'Exercised September', header_style)
            col = write(row, col, 'Exercised October', header_style)
            col = write(row, col, 'Exercised November', header_style)
            col = write(row, col, 'Exercised December', header_style)

            row+=1
        return row, col, columnwidth

    def request_data(self):
        # Search Programs
        end = self.end_date
        start = datetime.strptime(
            str("%s/01/01" % (str(end.year))), '%Y/%m/%d').date()

        start_year = start.year
        end_year = end.year

        # Se obtiene el id de la cuenta contable correspondiente al ejercido
        ejercido = self.env[ACCOUNT_ACCOUNT].sudo().search(
            [('code', '=', '821.001.001.001')],
            limit=1
        )

        account_comprometido_id = self.env[ACCOUNT_ACCOUNT].sudo().search(
            [('code', '=', '823.001.001.001')],
            limit=1
        ).id
        account_devengado_id = self.env[ACCOUNT_ACCOUNT].sudo().search(
            [('code', '=', '824.001.001.001')],
            limit=1
        ).id
        account_ejercido_id = self.env[ACCOUNT_ACCOUNT].sudo().search(
            [('code', '=', '825.001.001.001')],
            limit=1
        ).id
        account_pagado_id = self.env[ACCOUNT_ACCOUNT].sudo().search(
            [('code', '=', '826.001.001.001')],
            limit=1
        ).id

        query = f"""
        select
            program_code.year as year,
            program_code.pr as program,
            program_code.sp sub_program,
            program_code.dep dependency,
            program_code.sd sub_dependency,
            program_code.par exp_name,
            program_code.dv check_digit,
            program_code.or resource_origin_id,
            program_code.ai institutional_activity_id,
            program_code.conpp conversion_program,
            program_code.conpa shcp_item,
            program_code.tg type_of_expenditure,
            program_code.ug geographic_location,
            program_code.cc wallet_key,
            program_code.tp type_of_project,
            program_code.np project_number,
            program_code.e stage_identofier,
            program_code.tc type_of_agreement,
            program_code.nc number_of_agreement,
            coalesce(total_authorized,0) authorized,
            coalesce(first_authorized,0) authorized_q1,
            coalesce(second_authorized,0) authorized_q2,
            coalesce(third_authorized,0) authorized_q3,
            coalesce(fourth_authorized,0) authorized_q4,
            coalesce(total_expansion,0) annual_expansion,
            coalesce(first_expansion,0) annual_expansion_q1,
            coalesce(second_expansion,0) annual_expansion_q2,
            coalesce(third_expansion,0) annual_expansion_q3,
            coalesce(fourth_expansion,0) annual_expansion_q4,
            coalesce(total_reduction,0) annual_reduction,
            coalesce(first_reduction,0) annual_reduction_q1,
            coalesce(second_reduction,0) annual_reduction_q2,
            coalesce(third_reduction,0) annual_reduction_q3,
            coalesce(fourth_reduction,0) annual_reduction_q4,
            coalesce(total_authorized,0) + coalesce(total_expansion,0) - coalesce(total_reduction,0) modified,
            coalesce(first_authorized,0) + coalesce(first_expansion,0) - coalesce(first_reduction,0) modified_q1,
            coalesce(second_authorized,0) + coalesce(second_expansion,0) - coalesce(second_reduction,0) modified_q2,
            coalesce(third_authorized,0) + coalesce(third_expansion,0) - coalesce(third_reduction,0) modified_q3,
            coalesce(fourth_authorized,0) + coalesce(fourth_expansion,0) - coalesce(fourth_reduction,0) modified_q4,
            coalesce(anual, 0) total_gasto,
            coalesce(enero, 0) enero,
            coalesce(febrero, 0) febrero,
            coalesce(marzo, 0) marzo,
            coalesce(abril, 0) abril,
            coalesce(mayo, 0) mayo,
            coalesce(junio, 0) junio,
            coalesce(julio, 0) julio,
            coalesce(agosto, 0) agosto,
            coalesce(septiembre, 0) septiembre,
            coalesce(octubre, 0) octubre,
            coalesce(noviembre, 0) noviembre,
            coalesce(diciembre, 0) diciembre,
            coalesce(total_authorized,0) + coalesce(total_expansion,0) - coalesce(total_reduction,0) - coalesce(anual, 0) available
        from (
            select
            pc.id program_code_id,
            yc.name as year,
            pp.key_unam as pr,
            sp.sub_program as sp,
            dp.dependency as dep,
            sdp.sub_dependency as sd,
            expi.item as par,
            pc.check_digit as dv,
            ro.key_origin as or,
            inac.number as ai,
            shcp.name as conpp,
            dc.federal_part as conpa,
            et.key_expenditure_type as tg,
            gl.state_key as ug,
            kw.wallet_password as cc,
            ptype.project_type_identifier as tp,
            projectp.number as np,
            si.stage_identifier as e,
            atype.agreement_type as tc,
            atypen.number_agreement as nc
        from program_code pc, expenditure_item exioder, year_configuration yc,program pp,
            sub_program sp,dependency dp,sub_dependency sdp,expenditure_item expi,
            resource_origin ro,institutional_activity inac,shcp_code shcp,budget_program_conversion bpc,
            departure_conversion as dc,expense_type as et,geographic_location as gl,key_wallet as kw,project_type as ptype,
            project_type as ptypen,project_project projectp,stage as si,agreement_type as atype,
            agreement_type as atypen
        where exioder.id=pc.item_id and pc.year=yc.id and pc.program_id=pp.id and pc.sub_program_id=sp.id and pc.dependency_id=dp.id
            and pc.sub_dependency_id=sdp.id and pc.item_id=expi.id  and pc.resource_origin_id=ro.id
            and pc.institutional_activity_id=inac.id and pc.budget_program_conversion_id=bpc.id and shcp.id=bpc.shcp
            and pc.conversion_item_id=dc.id and pc.expense_type_id=et.id and pc.location_id=gl.id and pc.portfolio_id=kw.id
            and pc.project_type_id=ptype.id and pc.project_type_id=ptypen.id and projectp.id=ptypen.project_id
            and pc.stage_id=si.id and pc.agreement_type_id=atype.id and pc.agreement_type_id=atypen.id
            and to_date(yc.name, 'yyyy') between to_date('{start_year}', 'yyyy') and to_date('{end_year}', 'yyyy')
            and pc.state = 'validated'
        """
        # where conditional based in budget dependence rol #   
        budget_dependence_user = self.env.user.has_group('jt_budget_mgmt.group_dependence_user')
        if budget_dependence_user:
            conditional_based_user = self.env['details.budget.summary.report.onscreen'].get_conditional_based_user_query()
            query += conditional_based_user

        query += f"""
        ) program_code
        join (
            select *,
                coalesce(first_authorized,0) + coalesce(second_authorized,0) + coalesce(third_authorized, 0) + coalesce(fourth_authorized,0) total_authorized
            from crosstab('
                select program_code_id, extract(quarter from start_date) quarter, coalesce(authorized,0)
                from expenditure_budget_line budget, program_code, year_configuration year
                where budget.program_code_id = program_code.id
                    and program_code.year = year.id
                    and to_date(name, ''yyyy'') between to_date(''{start_year}'', ''yyyy'') and to_date(''{end_year}'', ''yyyy'')
                    and budget_line_yearly_id is not null
                order by program_code_id, quarter
            ', $$VALUES (1), (2), (3), (4)$$)
            as adequacies_lines_report(
                program_code_id integer,
                first_authorized numeric,
                second_authorized numeric,
                third_authorized numeric,
                fourth_authorized numeric
            )
        ) budget
        on program_code.program_code_id = budget.program_code_id
        left join (
            select
                coalesce(adecuaciones.program_code_id, recalendarizaciones.program_code_id) program_code_id,
                coalesce(adecuaciones.first_expansion,0) + coalesce(recalendarizaciones.first_expansion,0) first_expansion,
                coalesce(adecuaciones.second_expansion,0) + coalesce(recalendarizaciones.second_expansion,0) second_expansion,
                coalesce(adecuaciones.third_expansion,0) + coalesce(recalendarizaciones.third_expansion,0) third_expansion,
                coalesce(adecuaciones.fourth_expansion,0) + coalesce(recalendarizaciones.fourth_expansion,0) fourth_expansion,
                coalesce(adecuaciones.first_expansion,0) + coalesce(recalendarizaciones.first_expansion,0)+
                coalesce(adecuaciones.second_expansion,0) + coalesce(recalendarizaciones.second_expansion,0) +
                coalesce(adecuaciones.third_expansion,0) + coalesce(recalendarizaciones.third_expansion,0) +
                coalesce(adecuaciones.fourth_expansion,0) + coalesce(recalendarizaciones.fourth_expansion,0) total_expansion
            from (
                select *
                from crosstab('
                    select program_code_id, quarter, sum(expansion_amount)
                    from adequacies_lines_report where write_date between ''{start}'' and ''{end}''
                    group by program_code_id, quarter order by program_code_id, quarter
                ', $$VALUES (1), (2), (3), (4)$$)
                as adequacies_lines_report(
                    program_code_id integer,
                    first_expansion numeric,
                    second_expansion numeric,
                    third_expansion numeric,
                    fourth_expansion numeric
                )
            ) adecuaciones
            full outer join (
                select *
                from crosstab('
                    select sl.code_id, sl.quarter , sum(sl.amount) from standardization_line sl
                    JOIN standardization s on sl.standardization_id = s.id
                    JOIN account_fiscal_year afy on s.fiscal_year = afy.id
                    where EXTRACT(YEAR FROM afy.date_from) = ''{start_year}''
                    and sl.state = ''authorized''
                    group by sl.code_id, sl.quarter order by sl.code_id, sl.quarter
                ', $$VALUES (1), (2), (3), (4)$$)
                as recalendarizaciones_report(
                    program_code_id integer,
                    first_expansion numeric,
                    second_expansion numeric,
                    third_expansion numeric,
                    fourth_expansion numeric
                )
            ) recalendarizaciones
            on adecuaciones.program_code_id = recalendarizaciones.program_code_id
        ) expansiones
        on program_code.program_code_id = expansiones.program_code_id
        left join (
            select
                coalesce(adecuaciones.program_code_id, recalendarizaciones.program_code_id) program_code_id,
                coalesce(adecuaciones.first_reduction,0) + coalesce(recalendarizaciones.first_reduction,0) first_reduction,
                coalesce(adecuaciones.second_reduction,0) + coalesce(recalendarizaciones.second_reduction,0) second_reduction,
                coalesce(adecuaciones.third_reduction,0) + coalesce(recalendarizaciones.third_reduction,0) third_reduction,
                coalesce(adecuaciones.fourth_reduction,0) + coalesce(recalendarizaciones.fourth_reduction,0) fourth_reduction,
                coalesce(adecuaciones.first_reduction,0) + coalesce(recalendarizaciones.first_reduction,0)+
                coalesce(adecuaciones.second_reduction,0) + coalesce(recalendarizaciones.second_reduction,0) +
                coalesce(adecuaciones.third_reduction,0) + coalesce(recalendarizaciones.third_reduction,0) +
                coalesce(adecuaciones.fourth_reduction,0) + coalesce(recalendarizaciones.fourth_reduction,0) total_reduction
            from (
                select *
                from crosstab('
                    select program_code_id, quarter, sum(reduction_amount)
                    from adequacies_lines_report where write_date between ''{start}'' and ''{end}''
                    group by program_code_id, quarter order by program_code_id, quarter
                ', $$VALUES (1), (2), (3), (4)$$)
                as adequacies_lines_report(
                    program_code_id integer,
                    first_reduction numeric,
                    second_reduction numeric,
                    third_reduction numeric,
                    fourth_reduction numeric
                )
            ) adecuaciones
            full outer join (
                select *
                from crosstab('
                    select sl.code_id, sl.origin_id , sum(sl.amount) from standardization_line sl
                    JOIN standardization s on sl.standardization_id = s.id
                    JOIN account_fiscal_year afy on s.fiscal_year = afy.id
                    where EXTRACT(YEAR FROM afy.date_from) = ''{start_year}''
                    and sl.state = ''authorized''
                    group by sl.code_id, sl.origin_id order by sl.code_id, sl.origin_id
                ', $$VALUES (1), (2), (3), (4)$$)
                as recalendarizaciones_report(
                    program_code_id integer,
                    first_reduction numeric,
                    second_reduction numeric,
                    third_reduction numeric,
                    fourth_reduction numeric
                )
            ) recalendarizaciones
            on adecuaciones.program_code_id = recalendarizaciones.program_code_id
        ) reducciones
        on program_code.program_code_id = reducciones.program_code_id
        left join (
            select *,
                coalesce(enero,0) + coalesce(febrero,0) + coalesce(marzo,0) +
                coalesce(abril,0) + coalesce(mayo,0) + coalesce(junio,0) +
                coalesce(julio,0) + coalesce(agosto,0) + coalesce(septiembre,0) +
                coalesce(octubre,0) + coalesce(noviembre,0) + coalesce(diciembre,0) anual
            from crosstab('
                select l.program_code_id, extract(month from l.date) quarter, sum(debit-credit)
                from account_move m, account_move_line l
                where m.id = l.move_id
                    and l.account_id in ({account_comprometido_id}, {account_devengado_id}, {account_ejercido_id}, {account_pagado_id})
                    and l.date between ''{start}'' and ''{end}''
                    and m.state = ''posted''
                group by l.program_code_id, quarter order by l.program_code_id, quarter
            ', $$VALUES (1), (2), (3), (4), (5), (6), (7), (8), (9), (10), (11), (12)$$)
            as adequacies_lines_report(
                program_code_id integer,
                enero       numeric,
                febrero     numeric,
                marzo       numeric,
                abril       numeric,
                mayo        numeric,
                junio       numeric,
                julio       numeric,
                agosto      numeric,
                septiembre  numeric,
                octubre     numeric,
                noviembre   numeric,
                diciembre   numeric
            )
        ) gastos
        on program_code.program_code_id = gastos.program_code_id
        """

        self.env.cr.execute(query,())

        my_datas = self.env.cr.dictfetchall()

        wb1 = xlwt.Workbook(encoding='utf-8')
        if self.env.user.lang == 'es_MX':
            ws1 = wb1.add_sheet('Reporte Detallado de Presupuesto')
        else:
            ws1 = wb1.add_sheet('Details Budget Report')
        fp = BytesIO()

        #if my_datas:
        header_style = xlwt.easyxf('font: bold 1')
        float_sytle = xlwt.easyxf(num_format_str = '$#,##0.00')
        #total_style = xlwt.easyxf('num_format_str :0.00;' 'font: bold 1;' )

        ezxf = xlwt.easyxf
        total_style = ezxf(
            'font: italic true; pattern: pattern solid, fore_colour grey25',num_format_str='$#,##0.00')
#             ws1.set_panes_frozen(True)
#             ws1.set_horz_split_pos(1)
#             ws1.set_vert_split_pos(18)
        row = 0
        col = 0
        if self.env.user.lang == 'es_MX':
            ws1.write_merge(row, row,col,col+19, 'Reporte Detallado de Presupuesto',header_style)
        else:
            ws1.write_merge(row, row,col,col+19, 'Details Budget Report',header_style)

        row+=1
        col = 0
        row, col, columnwidth = self.add_report_header(row,col,header_style,ws1)

        total_authorized = 0
        total_authorized_q1 = 0
        total_authorized_q2 = 0
        total_authorized_q3 = 0
        total_authorized_q4 = 0

        total_annual_expansion = 0
        total_annual_expansion_q1 = 0
        total_annual_expansion_q2 = 0
        total_annual_expansion_q3 = 0
        total_annual_expansion_q4 = 0

        total_annual_reduction = 0
        total_annual_reduction_q1 = 0
        total_annual_reduction_q2 = 0
        total_annual_reduction_q3 = 0
        total_annual_reduction_q4 = 0

        total_modified = 0
        total_modified_q1 = 0
        total_modified_q2 = 0
        total_modified_q3 = 0
        total_modified_q4 = 0

        total_gasto = 0
        total_enero = 0
        total_febrero = 0
        total_marzo = 0
        total_abril = 0
        total_mayo = 0
        total_junio = 0
        total_julio = 0
        total_agosto = 0
        total_septiembre = 0
        total_octubre = 0
        total_noviembre = 0
        total_diciembre = 0

        available = 0
        for data in my_datas:
            
            #=== 1st section ====#
            col = 0
            ws1.write(row, col, data.get('year'))
            col+=1
            ws1.write(row, col, data.get('program'))
            col+=1
            ws1.write(row, col, data.get('sub_program'))
            col+=1
            ws1.write(row, col, data.get('dependency'))
            col+=1
            ws1.write(row, col, data.get('sub_dependency'))
            col+=1
            ws1.write(row, col, data.get('exp_name'))
            col+=1
            ws1.write(row, col, data.get('check_digit'))
            col+=1
            ws1.write(row, col, data.get('resource_origin_id'))
            col+=1
            ws1.write(row, col, data.get('institutional_activity_id'))
            col+=1
            ws1.write(row, col, data.get('conversion_program'))
            col+=1
            ws1.write(row, col, data.get('shcp_item'))
            col+=1
            ws1.write(row, col, data.get('type_of_expenditure'))
            col+=1
            ws1.write(row, col, data.get('geographic_location'))
            col+=1
            ws1.write(row, col, data.get('wallet_key'))
            col+=1
            ws1.write(row, col, data.get('type_of_project'))
            col+=1
            ws1.write(row, col, data.get('project_number'))
            col+=1
            ws1.write(row, col, data.get('stage_identofier'))
            col+=1
            ws1.write(row, col, data.get('type_of_agreement'))
            col+=1
            ws1.write(row, col, data.get('number_of_agreement'))
            col+=1
            proramti = str(data.get('year')) + str(data.get('program')) + str(data.get('sub_program')) + str(data.get('dependency')) + str(data.get('sub_dependency')) + str(data.get('exp_name')) + str(data.get('check_digit')) + str(data.get('resource_origin_id')) + str(data.get('institutional_activity_id')) + str(data.get('conversion_program')) + str(data.get('shcp_item')) + str(data.get('type_of_expenditure')) + str(data.get('geographic_location')) + str(data.get('wallet_key')) + str(data.get('type_of_project')) +  str(data.get('project_number')) + str(data.get('stage_identofier')) + str(data.get('type_of_agreement')) + str(data.get('number_of_agreement'))
            #==== 2nd section ====#
            total_authorized += data.get('authorized')
            ws1.write(row, col, data.get('authorized'),float_sytle)
            col+=1
            total_authorized_q1 += data.get('authorized_q1')
            ws1.write(row, col, data.get('authorized_q1'),float_sytle)
            col+=1
            total_authorized_q2 += data.get('authorized_q2')
            ws1.write(row, col, data.get('authorized_q2'),float_sytle)
            col+=1
            total_authorized_q3 += data.get('authorized_q3')
            ws1.write(row, col, data.get('authorized_q3'),float_sytle)
            col+=1
            total_authorized_q4 += data.get('authorized_q4')
            ws1.write(row, col, data.get('authorized_q4'),float_sytle)
            col+=1
            #==== 3rd section ====#
            total_annual_expansion += data.get('annual_expansion')
            ws1.write(row, col, data.get('annual_expansion'),float_sytle)
            col+=1
            total_annual_expansion_q1 += data.get('annual_expansion_q1')
            ws1.write(row, col, data.get('annual_expansion_q1'),float_sytle)
            col+=1
            total_annual_expansion_q2 += data.get('annual_expansion_q2')
            ws1.write(row, col, data.get('annual_expansion_q2'),float_sytle)
            col+=1
            total_annual_expansion_q3 += data.get('annual_expansion_q3')
            ws1.write(row, col, data.get('annual_expansion_q3'),float_sytle)
            col+=1
            total_annual_expansion_q4 += data.get('annual_expansion_q4')
            ws1.write(row, col, data.get('annual_expansion_q4'),float_sytle)
            col+=1

            #==== 4th section ====#
            total_annual_reduction += data.get('annual_reduction')
            ws1.write(row, col, data.get('annual_reduction'),float_sytle)
            col+=1
            total_annual_reduction_q1 += data.get('annual_reduction_q1')
            ws1.write(row, col, data.get('annual_reduction_q1'),float_sytle)
            col+=1
            total_annual_reduction_q2 += data.get('annual_reduction_q2')
            ws1.write(row, col, data.get('annual_reduction_q2'),float_sytle)
            col+=1
            total_annual_reduction_q3 += data.get('annual_reduction_q3')
            ws1.write(row, col, data.get('annual_reduction_q3'),float_sytle)
            col+=1
            total_annual_reduction_q4 += data.get('annual_reduction_q4')
            ws1.write(row, col, data.get('annual_reduction_q4'),float_sytle)
            col+=1

            #==== 5th section ====#
            total_modified += data.get('modified')
            total_modified_q1 += data.get('modified_q1')
            total_modified_q2 += data.get('modified_q2')
            total_modified_q3 += data.get('modified_q3')
            total_modified_q4 += data.get('modified_q4')


            ws1.write(row, col, data.get('modified'),float_sytle)
            col+=1
            ws1.write(row, col, data.get('modified_q1'),float_sytle)
            col+=1
            ws1.write(row, col, data.get('modified_q2'),float_sytle)
            col+=1
            ws1.write(row, col, data.get('modified_q3'),float_sytle)
            col+=1
            ws1.write(row, col, data.get('modified_q4'),float_sytle)
            col+=1

            total_gasto += data.get('total_gasto')
            total_enero += data.get('enero')
            total_febrero += data.get('febrero')
            total_marzo += data.get('marzo')
            total_abril += data.get('abril')
            total_mayo += data.get('mayo')
            total_junio += data.get('junio')
            total_julio += data.get('julio')
            total_agosto += data.get('agosto')
            total_septiembre += data.get('septiembre')
            total_octubre += data.get('octubre')
            total_noviembre += data.get('noviembre')
            total_diciembre += data.get('diciembre')

            ws1.write(row, col, data.get('total_gasto'),float_sytle)
            col+=1
            ws1.write(row, col, data.get('enero'),float_sytle)
            col+=1
            ws1.write(row, col, data.get('febrero'),float_sytle)
            col+=1
            ws1.write(row, col, data.get('marzo'),float_sytle)
            col+=1
            ws1.write(row, col, data.get('abril'),float_sytle)
            col+=1
            ws1.write(row, col, data.get('mayo'),float_sytle)
            col+=1
            ws1.write(row, col, data.get('junio'),float_sytle)
            col+=1
            ws1.write(row, col, data.get('julio'),float_sytle)
            col+=1
            ws1.write(row, col, data.get('agosto'),float_sytle)
            col+=1
            ws1.write(row, col, data.get('septiembre'),float_sytle)
            col+=1
            ws1.write(row, col, data.get('octubre'),float_sytle)
            col+=1
            ws1.write(row, col, data.get('noviembre'),float_sytle)
            col+=1
            ws1.write(row, col, data.get('diciembre'),float_sytle)
            col+=1
            # Get new available annual
            available += data.get('available')
            ws1.write(row, col, data.get('available'), float_sytle)
            col += 1

            row+=1

        # Función auxiliar para obtener el tamaño máxico de las columnas
        def write(row, col, data, style):
            if col in columnwidth:
                if len(str(data)) > columnwidth[col]:
                    columnwidth[col] = len(str(data))
            else:
                columnwidth[col] = len(str(data))
            ws1.write(row, col, round(data, 2), style)
            return col + 1
        #===== Total=======#
        row+=1
        col=19
        #==== 2nd section ====#
        col = write(row, col, total_authorized,total_style)
        col = write(row, col, total_authorized_q1,total_style)
        col = write(row, col, total_authorized_q2,total_style)
        col = write(row, col, total_authorized_q3,total_style)
        col = write(row, col, total_authorized_q4,total_style)
        #==== 3rd section ====#
        col = write(row, col, total_annual_expansion,total_style)
        col = write(row, col, total_annual_expansion_q1,total_style)
        col = write(row, col, total_annual_expansion_q2,total_style)
        col = write(row, col, total_annual_expansion_q3,total_style)
        col = write(row, col, total_annual_expansion_q4,total_style)
        #==== 4th section ====#
        col = write(row, col, total_annual_reduction,total_style)
        col = write(row, col, total_annual_reduction_q1,total_style)
        col = write(row, col, total_annual_reduction_q2,total_style)
        col = write(row, col, total_annual_reduction_q3,total_style)
        col = write(row, col, total_annual_reduction_q4,total_style)
        #==== 5th section ====#
        col = write(row, col, total_modified,total_style)
        col = write(row, col, total_modified_q1,total_style)
        col = write(row, col, total_modified_q2,total_style)
        col = write(row, col, total_modified_q3,total_style)
        col = write(row, col, total_modified_q4,total_style)
        #==== 6th Section =====#
        col = write(row, col, total_gasto, total_style)
        col = write(row, col, total_enero,total_style)
        col = write(row, col, total_febrero,total_style)
        col = write(row, col, total_marzo,total_style)
        col = write(row, col, total_mayo,total_style)
        col = write(row, col, total_abril,total_style)
        col = write(row, col, total_junio,total_style)
        col = write(row, col, total_julio,total_style)
        col = write(row, col, total_agosto,total_style)
        col = write(row, col, total_septiembre,total_style)
        col = write(row, col, total_octubre,total_style)
        col = write(row, col, total_noviembre,total_style)
        col = write(row, col, total_diciembre,total_style)
        col = write(row, col, available,total_style)

        # Se establece el tamaño máximo de las columnas según la longitud máxima de los datos
        for column, widthvalue in columnwidth.items():
            ws1.col(column).width = (widthvalue + 4) * 367

        wb1.save(fp)
        out = base64.encodestring(fp.getvalue())
        self.report_file = out
        if self.env.user.lang == 'es_MX':
            self.name = 'reporte_detallado_de_presupuesto.xls'
        else:
            self.name = 'details_budget_report.xls'
        #self.state = 'download'

        #=======================#
        attch_obj = self.env['ir.attachment']
        vals = {'name': self.name,
                'datas': out,
                'res_model': 'jt_budget_mgmt.details.budget.summary.report',
                }
        attach_ids = attch_obj.search([
            ('res_model', '=', 'jt_budget_mgmt.details.budget.summary.report')])
        if attach_ids:
            try:
                attach_ids.sudo().unlink()
            except Exception as e:
                raise ValidationError(e)
        doc_id = attch_obj.sudo().create(vals)
        
        return {
            'type': 'ir.actions.act_url',
            'url': 'web/content/%s?download=true' % (doc_id.id),
            'target': 'current',
            'tag': 'close',
        }
        #========================================#
        return {
            'name': _('Report'),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': False,
            'res_model': 'details.budget.summary.report',
            'domain': [],
            'type': 'ir.actions.act_window',
            'target': 'new',
            'res_id': self.id,
        }
