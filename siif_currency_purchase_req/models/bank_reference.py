from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from datetime import date,datetime
import logging
import qrcode
from io import BytesIO
import base64


from barcode import Code39
import xml.etree.ElementTree as ET

BANK_REF_CONF='bank.reference.conf'
class BankReferenceConfiguration(models.Model):
            
    _name = 'bank.reference.conf'

    name = fields.Char(default='Parámetros', store=False)
    min_char = fields.Integer(default=0, string="Minimum reference character",required=True)
    max_char = fields.Integer(default=1, string="Maximum reference character",required=True)
    divisor = fields.Integer(default=1, string="Divisor",required=True)
    factor_fijo = fields.Integer(default=1,required=True)
    constant = fields.Selection(selection=[
            ('a', 'A'),
            ('2', '2'),
        ], string='State', default='a',required=True)
    year = fields.Selection(
        selection = 'year_selection',
        string="Year",
        default="2013", 
    )
    journal = fields.Many2one('account.journal',string="Cuenta Bancaria DGF",required=True)
    account = fields.Many2one('account.account',string="Associated Account",required=True)
    #==============================================================
	#-----------------------Revisar configuraciones
	#==============================================================
    @api.model
    def year_selection(self):
        year = 2013 # replace 2000 with your a start year
        year_list = []
        while year != (datetime.now().year): # replace 2030 with your end year
            year_list.append((str(year), str(year)))
            year += 1
        return year_list
    
    @api.constrains('min_char','max_char')
    def _constrain_char_limit(self):
        if self.min_char>=self.max_char:
            raise ValidationError("La longitud minima debe ser menor a la longitud máxima. Revisar.")

    @api.constrains('min_char')
    def _constrain_min_char(self):     
        if self.min_char<0:
            raise ValidationError("La longitud mínima debe ser mayor o igual a 0.")

    @api.constrains('max_char')
    def _constrain_max_char(self):     
        if self.max_char<=0:
            raise ValidationError("La longitud mínima debe ser mayor a 0.")
        
    @api.constrains('divisor')
    def _constrain_divisor(self):     
        if self.divisor<=0:
            raise ValidationError("El divisor debe ser mayor a 0.")

    @api.constrains('factor_fijo')
    def _constrain_divisor(self):     
        if self.divisor<0:
            raise ValidationError("El factor fijo debe ser mayor a 0.")
        
    @api.onchange('journal')
    def _assign_account(self):
        for rec in self:
            rec.account=rec.journal.default_credit_account_id


    #==============================================================
	#-----------------------Funciones
	#==============================================================


    @api.model
    def create(self, vals):
        count = self.env[BANK_REF_CONF].search_count([])
        if count >= 1:
            raise ValidationError("Solo se puede configurar un registro para la configuración de referencias bancarias")
        return super().create(vals)

    def name_get(self):
        return [(record.id, 'Configuración de Referencias Bancarias') for record in self]

    #==============================================================
	#-----------------------Revisar parametros para utilizarlos
	#==============================================================

    def get_param(self, param):
        params = self.search([], limit=1)
        if not params:
            raise ValidationError("Configure los parámetros de la Configuración de Referencias Bancarias")
        """
            Param_validations:
                Diccionario con la información
                    (nombre de la variable, mensaje de error si no se encuentra el valor)
        """
        param_validations = {
            'min_char': ('min_char',"la cantidad minima de carácteres"),
            'max_char': ('max_char',"la cantidad máxima de carácteres"),
            'div': ('divisor',"el valor del divisor"),
            'fijo': ('factor_fijo',"el factor fijo"),
            'const': ('constant',"la constante de cálculo"),
            'year': ('year',"el año base"),
            'journal': ('journal',"la Cuenta bancaria"),
            'account': ('account',"la Cuenta asignada por la DGF"),

        }
        
        #Se revisa si el parametro se encuentra en el diccionario
        if param in param_validations:
            #Obtiene el nombre de la variable y el mensaje de error
            attr_name, error_msg = param_validations[param]
            #obtiene el valor del parametro
            value = getattr(params, attr_name)
            #Si no encuentra el valor del parametro, mandará el mensaje de error
            if error_msg and not value:
                raise ValidationError(f"Configure '{error_msg}' en el menú 'Configuración de Referencias Bancarias'.")
            return value
        #Si el parametro no existe
        else:
            raise ValueError("Parámetro no encontrado")
        
        
        
UNIDADES = (
    'CERO',
    'UNO',
    'DOS',
    'TRES',
    'CUATRO',
    'CINCO',
    'SEIS',
    'SIETE',
    'OCHO',
    'NUEVE'
)

DECENAS = (
    'DIEZ',
    'ONCE',
    'DOCE',
    'TRECE',
    'CATORCE',
    'QUINCE',
    'DIECISEIS',
    'DIECISIETE',
    'DIECIOCHO',
    'DIECINUEVE'
)

DIEZ_DIEZ = (
    'CERO',
    'DIEZ',
    'VEINTE',
    'TREINTA',
    'CUARENTA',
    'CINCUENTA',
    'SESENTA',
    'SETENTA',
    'OCHENTA',
    'NOVENTA'
)

CIENTOS = (
    '_',
    'CIENTO',
    'DOSCIENTOS',
    'TRESCIENTOS',
    'CUATROSCIENTOS',
    'QUINIENTOS',
    'SEISCIENTOS',
    'SETECIENTOS',
    'OCHOCIENTOS',
    'NOVECIENTOS'
)
MAX_NUMERO = 999999999999

CENTIMOS_SINGULAR = 'CENTIMO'
CENTIMOS_PLURAL = 'CENTIMOS'
class BankReference(models.Model):
    _name = 'bank.reference'
    _description = 'Creator of Bank Reference'

    reference = fields.Char(string='Reference')
    short_ref = fields.Char(string='Short Reference')
    date = fields.Date(string="Date")
    currency_id = fields.Many2one('res.currency',string='Currency',required=True,default=lambda self: self.env.company.currency_id)

    amount = fields.Monetary(string="Monetary Reference",currency_field='currency_id',default=0)
    amount_char = fields.Char(string="Monetary Ref Char", compute='_get_amount_char')
    barcode = fields.Html(string="Código de barras", sanitize=False)
    qr_code = fields.Binary(string="QR Code", readonly=True)
    
    @api.model
    def create(self,vals):
        res = super(BankReference, self).create(vals)
        res._generate_img_codes()
        return res
    
    def name_get(self):
        return [(record.id,record.reference) for record in self]

    def _gen_short_ref(self,dep,sd,folio,type_info):
        start=''
        text_dep=dep.dependency
        text_sd=self._get_sd_text(sd.sub_dependency)
        id_folio= folio.split('/')[0]
        if type_info=='money_return':
            start='A'
        elif type_info=='account_return':
            start='B'    
        return start+text_dep+text_sd+'A'+id_folio
        
    def _generate_img_codes(self):
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=10,
            border=4,
        )
        qr.add_data(self.reference)
        qr.make(fit=True)
        img = qr.make_image()
        temp= BytesIO()
        img.save(temp,format="PNG")
        qr_image_s = str(base64.b64encode(temp.getvalue())).replace("\'","")
        qr_image = qr_image_s[1:]


        barcode = Code39(self.reference, add_checksum=False)
        barcode_object = barcode.render()
        xml_object = ET.fromstring(barcode_object)
        generated_barcode = ET.tostring(xml_object).decode('UTF-8')

        self.write({
            'barcode':generated_barcode,
            'qr_code':qr_image})


    def _get_sd_text(self,sd):
        text_sd=""
        if len(sd)==2:
            text_sd='A'+chr(int(sd)+65)
        return text_sd
    #Función base para obtener la referencia bancaria
    def calculate_bank_ref(self,ref,importe,fecha):
        bank_conf=self.env[BANK_REF_CONF]
        min_c = bank_conf.get_param('min_char')
        max_c = bank_conf.get_param('max_char')
        divisor = bank_conf.get_param('div')
        constant = bank_conf.get_param('const')
        v_digit=bank_conf.get_param('fijo')
        year_base = bank_conf.get_param('year')
        if (min_c<=len(ref) and len(ref)<=max_c):
            fecha_condensada=self._suma_conversion(year_base,fecha)
            importe_condensado=self._multiply_import(importe)
            referencia=self._conv_numero_referencia(ref,fecha_condensada,importe_condensado,constant,divisor,v_digit)
            return referencia
        else:
            raise ValidationError('La longitud de la referencia no está en el rango.')

    #===============================================================
    #--------------Calculo fecha condensada
    #===============================================================
    #Funcion para obtener la convesión de la fecha
    #   Regla: Resta de la fecha de vencimiento menos una fecha base 
    #   y multiplicarlo por 372

    def _year_conversion(self,year_base,year):
        aux=int(year)-int(year_base)
        return aux*372
    
    #Función para obtener la conversión del mes
    #   Regla: Número del mes menos la unidad y multiplicar por 31
    def _month_convesion(self,month):
        aux=int(month)-1
        return aux*31
    
    #Función para obtener la conversión del dia
    #   Regla: El dia restar la unidad
    def _day_conversion(self,day):
        return int(day)-1
    
    #   Suma de los valores obtenidos de las conversiones
    #   Dato de 4 digitos, rellena con 0 si no es del mismo tamaño
    def _suma_conversion(self,year_base,fecha):

        year_num=self._year_conversion(year_base,fecha.year)
        mes_num =self._month_convesion(fecha.month)
        day_num =self._day_conversion(fecha.day)
        conversion=""
        suma = year_num+mes_num+day_num

        if suma<=1000:
            conversion=str(suma)
        else:
            size=len(str(suma))
            i=4-size
            conversion='0'*i
            conversion=conversion+str(suma)
        return conversion

    #==============================================================
    #--------------Calculo importe condensado
    #==============================================================
    #Función para tomar el importe y tomarlo para valor en la referencia
    #Multiplicar por 7,3,1 consecutivamente cada digito
    def _multiply_import(self,importe):
        multiplicadores=[7,3,1]
        text_import=str(round(importe,2))
        text_import=text_import.replace('.','')
        i=len(text_import)
        add_imports=0
        aux=0
        #Multiplicación y suma de los digitos
        for a in range(i-1,-1,-1):
            add_imports+=(int(text_import[a])*multiplicadores[aux])
            if aux!=(len(multiplicadores)-1):
                aux+=1
            else:
                aux=0
        
        #Residio
        return str(add_imports%10)
    
    #==============================================================
    #--------------Calculo digitos verificados
    #==============================================================
    def _conv_numero_referencia(self,referencia,date_conv,import_conv,constant,divisor,v_digit):
        valores={
            "A":10,"B":11,"C":12,"D":13,"E":14,"F":15,"G":16,
            "H":17,"I":18,"J":19,"K":20,"L":21,"M":22,"N":23,
            "O":24,"P":25,"Q":26,"R":27,"S":28,"T":29,"U":30,
            "V":31,"W":32,"X":33,"Y":34,"Z":35     
        }
        multiplicadores=[11,13,17,19,23]
        converted_ref=referencia+date_conv+import_conv+constant
        aux=0
        suma=0
        #Obtener producto de cada digito de la referencia convertida
        for i in range(len(converted_ref)-1,-1,-1):
            if converted_ref[i].isdigit():
                suma+=(int(converted_ref[i])*multiplicadores[aux])
            else:
                suma+=(valores[converted_ref[i]]*multiplicadores[aux])

            if aux!=(len(multiplicadores)-1):
                aux+=1
            else:
                aux=0

        aux_ver = suma%divisor
        aux_ver += v_digit

        converted_ref = converted_ref+str(aux_ver)
        return converted_ref
    
    
    #==============================================================
    #--------------Cantidad en letra
    #==============================================================

    
    def number_letter(self,numero):
        numero_entero = int(numero)
        if numero_entero > MAX_NUMERO:
            raise OverflowError('Número demasiado alto')

        if numero_entero <= 99:
            resultado = self._get_tens(numero_entero)
        elif numero_entero <= 999:
            resultado = self._get_hundreds(numero_entero)
        elif numero_entero <= 999999:
            resultado = self._get_thousands(numero_entero)
        elif numero_entero <= 999999999:
            resultado = self._get_millions(numero_entero)
        else:
            resultado = self._get_billions(numero_entero)
        resultado = resultado.replace('UNO MIL', 'UN MIL')
        resultado = resultado.strip()
        resultado = resultado.replace(' _ ', ' ')
        resultado = resultado.replace('  ', ' ')

        return resultado


    def _get_tens(self,numero):
        if numero < 10:
            return UNIDADES[numero]
        decena, unidad = divmod(numero, 10)
        if numero <= 19:
            resultado = DECENAS[unidad]
        elif 21 <= numero <= 29:
            resultado = f"VEINTI{UNIDADES[unidad]}"
        else:
            resultado = DIEZ_DIEZ[decena]
            if unidad > 0:
                resultado = f"{resultado} y {UNIDADES[unidad]}"
        return resultado


    def _get_hundreds(self,numero):
        centena, decena = divmod(numero, 100)
        if numero == 0:
            resultado = 'CIEN'
        else:
            resultado = CIENTOS[centena]
            if decena > 0:
                decena_letras = self._get_tens(decena)
                resultado = f"{resultado} {decena_letras}"
        return resultado


    def _get_thousands(self,numero):
        millar, centena = divmod(numero, 1000)
        resultado = ''
        if millar == 1:
            resultado = ''
        if (millar >= 2) and (millar <= 9):
            resultado = UNIDADES[millar]
        elif (millar >= 10) and (millar <= 99):
            resultado = self._get_tens(millar)
        elif (millar >= 100) and (millar <= 999):
            resultado = self._get_hundreds(millar)
        resultado = f"{resultado} MIL"
        if centena > 0:
            centena_letras = self._get_hundreds(centena)
            resultado = f"{resultado} {centena_letras}"
        return resultado.strip()


    def _get_millions(self,numero):
        millon, millar = divmod(numero, 1000000)
        resultado = ''
        if millon == 1:
            resultado = ' UN MILLON '
        if (millon >= 2) and (millon <= 9):
            resultado = UNIDADES[millon]
        elif (millon >= 10) and (millon <= 99):
            resultado = self._get_tens(millon)
        elif (millon >= 100) and (millon <= 999):
            resultado = self._get_hundreds(millon)
        if millon > 1:
            resultado = f"{resultado} MILLONES"
        if (millar > 0) and (millar <= 999):
            centena_letras = self._get_hundreds(millar)
            resultado = f"{resultado} {centena_letras}"
        elif (millar >= 1000) and (millar <= 999999):
            miles_letras = self._get_thousands(millar)
            resultado = f"{resultado} {miles_letras}"
        return resultado


    def _get_billions(self,numero):
        millardo, millon = divmod(numero, 1000000)
        miles_letras = self._get_thousands(millardo)
        millones_letras = self._get_millions(millon)
        return f"{miles_letras} MILLONES {millones_letras}"


    def _get_amount_char(self):
        numero_entero = int(self.amount)
        parte_decimal = int(round((abs(self.amount) - abs(numero_entero)) * 100))
        if parte_decimal<=9:
            decimal="0"+str(parte_decimal)
        else:
            decimal=str(parte_decimal)
        moneda = "PESOS"

        letras = self.number_letter(numero_entero)
        letras = letras.replace('UNO', 'UN')
        letras = f"{letras} {moneda} {decimal}/100 M.N."
        
        self.amount_char=letras
