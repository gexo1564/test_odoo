# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
from datetime import datetime
import time
import json
import logging

# Define Constant
NAME_MODULE = 'account.move'
NAME_LINES = 'account.move.line'

class AccountMove(models.Model):
    _inherit = NAME_MODULE
    _order = 'create_date desc'

    income_butgets_id = fields.Many2one('siif.budget.mgmt.income.budgets', 'Account Budgets', ondelete="cascade")
    income_adequacies_id = fields.Many2one('income.adequacies', 'Account Income Adequacies', ondelete="cascade")

    ##################################################
    # Función para agregar CRI a cuentas de ingresos #
    ##################################################
    def add_cri_accounts(self):
        count = 0
        count_note = 0
        count_four = 0
        
        for record in self:
            account_move = record.id
            

            obj_account_move = self.env[NAME_MODULE]
            obj_move_id = self.env[NAME_LINES]
            obj_rel_account_cri = self.env['relation.account.cri']
            move_id = obj_account_move.search([('id', '=', account_move)])
            cri_name = ""
            
            for rec in move_id:
                id_account = obj_move_id.search([('move_id', '=', rec.id)])

                for data in id_account:

                    if data.account_id.code[0]=="4":

                        id_cri = obj_rel_account_cri.search([('cuentas_id', '=', data.account_id.id)])
                        cri_name = id_cri.cri.name

                 
                for data_id in id_account:
                    ##########################################################################################
                    # Se le pone su CRI a las cuentas 410, 411, 810, 811, 812, 813 y 814 que son de ingresos #
                    ##########################################################################################
                    if (data_id.account_id.code.startswith("410") or data_id.account_id.code.startswith("411")) or data_id.account_id.code.startswith("810") or data_id.account_id.code.startswith("811") or data_id.account_id.code.startswith("812") or data_id.account_id.code.startswith("813") or data_id.account_id.code.startswith("814"):          
                        if not data_id.cri:
                            data_id.write({
                                'cri': cri_name,
                            })
                            count_note += 1
                            if (data_id.account_id.code.startswith("410") or data_id.account_id.code.startswith("411")):
                                count_four += 1
                    #######################################################################################
                    # Se le pone NULL a las cuentas 820, 8821, 822, 823, 824, 825 y 826 que son de egresos #
                    #######################################################################################
                    codes_to_check = ["820", "821", "822", "823", "824", "825", "826"]
                    if data_id.account_id.code.startswith(tuple(codes_to_check)):
                        data_id.write({
                            'cri': None
                        })
                    
            count += 1
        if count_four <= 1:
            message = "Se agregó el CRI a %s apunte(s) contable(s) de %s asiento(s) contable(s), de los cuales %s es el que afecta al presupuesto de ingresos." % (count_note,count,count_four)
        if count_four >= 2:
            message = "Se agregó el CRI a %s apunte(s) contable(s) de %s asiento(s) contable(s), de los cuales %s son los que afectan al presupuesto de ingresos." % (count_note,count,count_four)
        if count_note == 0 and data_id.cri != None:
            message = "Se agregó el CRI a %s apunte(s) contable(s), porque ya tienen su CRI asignado." % (count_note)
        if count_note == 0 and data_id.cri == None:
            message = "Se agregó el CRI a %s apunte(s) contable(s), porque no afectan el presupuesto de ingresos." % (count_note)

        return {
            'type': 'ir.actions.act_window',
            'name': _('Notificación'),
            'res_model': 'notification.wizard',
            'view_mode': 'form',
            'view_type': 'form',
            'view_id': 'siif_budget_mgmt.view_notification_wizard_form',
            'views': [(False, 'form')],
            'target': 'new',
            'context': {
                'default_notification_message': message,
            },
        }
    
    ########################################################################################
    # Función para agregar CRI a cuentas de ingresos e impactar al presupuesto de ingresos #
    ########################################################################################
    def validated_accounts_four_hundred(self):
        entries = 0
        notes = 0
        accounts_four = 0
        account_four_cri = 0
        amount_four = 0
        amount_four_cri = 0
        notes_cri = 0
        date_accrued = False
        for record in self:
            account_move = record.id
            id_loogbook = self.env['siif.budget.mgmt.accounting.movements.logbook'].search([('cve', '=', record.name), ('journal_applied', '=', record.journal_id.id)])
            obj_account_move = self.env[NAME_MODULE]
            obj_move_id = self.env[NAME_LINES]
            obj_rel_account_cri = self.env['relation.account.cri']
            move_id = obj_account_move.search([('id', '=', account_move)])
            cri_name = ""
            flag_income = False
            flag_dev = False
            flag_rec = False
            
            
            eight_amount = " "
            affectation = ""
            type_affectation = " "
            
            
            date = datetime.today()
            for rec in move_id:
                id_account = obj_move_id.search([('move_id', '=', rec.id)])
                                
                for data_id in id_account:
                    if (data_id.account_id.code.startswith("410") or data_id.account_id.code.startswith("411")):

                        id_cri = obj_rel_account_cri.search([('cuentas_id', '=', data_id.account_id.id)])
                        cri_name = id_cri.cri.name
                    
                    ##########################################################################################
                    # Se le pone su CRI a las cuentas 410, 411, 810, 811, 812, 813 y 814 que son de ingresos #
                    ##########################################################################################
                    if (data_id.account_id.code.startswith("410") or data_id.account_id.code.startswith("411")) or data_id.account_id.code.startswith("810") or data_id.account_id.code.startswith("811") or data_id.account_id.code.startswith("812") or data_id.account_id.code.startswith("813") or data_id.account_id.code.startswith("814"):
                        if not data_id.cri:
                            data_id.write({
                                'cri': cri_name,
                            })
                            notes += 1
                            if (data_id.account_id.code.startswith("410") or data_id.account_id.code.startswith("411")):
                                accounts_four += 1
                                if data_id.debit > 0.0:
                                    amount_four += data_id.debit
                                if data_id.credit > 0.0:
                                    amount_four += data_id.credit
                        else:
                            notes_cri += 1
                            if (data_id.account_id.code.startswith("410") or data_id.account_id.code.startswith("411")):
                                account_four_cri += 1
                                if data_id.debit > 0.0:
                                    amount_four_cri += data_id.debit
                                if data_id.credit > 0.0:
                                    amount_four_cri += data_id.credit
                    #######################################################################################
                    # Se le pone NULL a las cuentas 820, 821, 822, 823, 824, 825 y 826 que son de egresos #
                    #######################################################################################
                    codes_to_check = ["820", "821", "822", "823", "824", "825", "826"]
                    if data_id.account_id.code.startswith(tuple(codes_to_check)):
                        data_id.write({
                            'cri': None
                        })
            #####################################
            # Pregunta si existe en la bitacora #
            ##################################### 
            if not id_loogbook.ids:
                for data in id_account:

                    if (data.account_id.code.startswith("410") or data.account_id.code.startswith("411")):
                        date_affectation = data.date
                        fiscal_year = date_affectation.year
                        fiscal_year_id =self.env['account.fiscal.year'].search([('name', '=', fiscal_year)])
                        flag_income = True
                        type_movement = "income"
                        type_origin = "accounting_seat"
                        if data.credit > 0.0:
                            amount_income = float(data.credit)
                        else:
                            eight_amount = data.debit
                            amount_income = float(eight_amount * -1)

                        for eight_account in id_account:
                            if eight_account.account_id.code.startswith("813") and (eight_account.credit == amount_income or eight_account.debit == eight_amount):
                                flag_dev = True
                            elif eight_account.account_id.code.startswith("814") and (eight_account.credit == amount_income or eight_account.debit == eight_amount):
                                flag_rec = True

                        if flag_income == True and flag_dev == True and flag_rec == True:
                            affectation = "for_exercising_collected"
                            type_affectation = "eje-rec"
                        elif flag_income == True and flag_dev == True and flag_rec == False:
                            affectation = "for_exercised_accrued"
                            type_affectation = "eje-dev"
                        elif flag_income == False and flag_dev == True and flag_rec == True:
                            affectation = "accrued_collected"
                            type_affectation = "dev-rec"
                        self.env['income.adequacies.function'].search([]).logbook_income_budget(fiscal_year_id.id,id_cri.cri.id,date_affectation,type_movement,affectation,type_origin,rec.name,rec.journal_id.id,date,amount_income,date)
                        self.env['affectation.income.budget'].search([]).affectation_income_budget(type_affectation,id_cri.cri.id,date_accrued,date_affectation,amount_income)
            entries += 1
        if accounts_four >= 1 or id_loogbook.ids == None:
            message = "Se agregó el CRI a %s apunte(s) contable(s) de %s asiento(s) contable(s), de los cuales %s pueden afectar al presupuesto de ingresos.\n• El monto que suma al presupuesto recaudado es de $%s" % (notes,entries,accounts_four,amount_four)
        if id_loogbook.id == True:
            message = "No se agregar CRI a %s apunte(s) contable(s) de %s asiento(s) contable(s), CRI ya se encuentra asignado. Y el monto de la suma al presupuesto recaudado es de $0. Porque ya fue sumado anteriormente." % (notes_cri,entries)
        if account_four_cri >= 1 or id_loogbook.ids == None:
            message = "No se agregar CRI a %s apunte(s) contable(s) de %s asiento(s) contable(s), CRI ya se encuentra asignado. Pero aún no se ha afectado el presupuesto recaudado.\n• El monto que suma al presupuesto recaudado es de $%s" % (notes_cri,entries,amount_four_cri)
        
        return {
            'type': 'ir.actions.act_window',
            'name': _('Notificación'),
            'res_model': 'notification.wizard',
            'view_mode': 'form',
            'view_type': 'form',
            'view_id': 'siif_budget_mgmt.view_notification_wizard_form',
            'views': [(False, 'form')],
            'target': 'new',
            'context': {
                'default_notification_message': message,
            },
        }

class AccountMoveLine(models.Model):
    _inherit = NAME_LINES

    income_butgets_id = fields.Many2one('siif.budget.mgmt.income.budgets', 'Account Budgets', ondelete="cascade")
    income_adequacies_id = fields.Many2one('income.adequacies', 'Account Income Adequacies', ondelete="cascade")
    # Adicionamos campo CRI a la tabla account_move_line
    cri = fields.Char(string = 'CRI')
