# -*- coding: utf-8 -*-
from datetime import datetime
import json
import logging
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
from lxml import etree

REQUEST_INCOME_RECOGNITION_MODEL = 'request.income.recognition'
SUB_DEPENDENCY_MODEL = 'sub.dependency'
ACCOUNT_MOVE_MODEL = 'account.move'
REQUEST_INCOME_RECOGNITION_WIZARD = 'request.income.recognition.wizard'
INCOME_ADMIN_USER = 'jt_income.group_income_admin_user'
INCOME_FINANCE_USER = 'jt_income.group_income_finance_user'
INCOME_COORDINATOR_USER = 'jt_income.group_income_project_coordinator_user'
INCOME_DEPENDENCE_USER = 'jt_income.group_income_dependence_user'
ACTIVITY_SCHEDULE_ID = 'siif_income.400'
ACTION_WINDOW = 'ir.actions.act_window'
class RequestIncomeRecognition(models.Model):
    _name = REQUEST_INCOME_RECOGNITION_MODEL
    _description = 'Solicitud de reconocimiento de ingresos'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char("Name", required=True, default="")
    currency_id = fields.Many2one('res.currency', default=lambda self: self.env.company.currency_id)
    state = fields.Selection(
        [
            ('draft', 'Draft'),
            ('registered', 'Registered'),
            ('accepted', 'Accepted'),
            ('rejected', 'Rejected'),
        ], default='draft', track_visibility = "always",
    )
    request_date = fields.Date(default=lambda self: fields.datetime.now())
    dependency_id = fields.Many2one('dependency', string='Dependence', domain=lambda self: self.get_domain_dependency())
    sub_dependency_id = fields.Many2one(SUB_DEPENDENCY_MODEL, "Subdependency")
    domain_sub_dependency = fields.Char(compute="_compute_domain_sub_dependency", store=False)
    observations = fields.Text('Observations')
    bank_movements_ids = fields.Many2many(
        'bank.movements',
        'request_income_recognition_bank_movements_rel',
        'request_income_recognition_id',
        'bank_movements_id', 
    )
    total_bank_movements = fields.Monetary(string='Total Deposits',compute='_compute_total_bank_movements',readonly=True)
    invoice_ids = fields.Many2many(
        ACCOUNT_MOVE_MODEL,
        'request_income_recognition_account_move_rel',
        'request_income_recognition_id',
        'account_move_id',
        string="Invoices",
    )
    total_invoices = fields.Monetary(string='Total Invoices',compute='_compute_total_invoices',readonly=True)
    total = fields.Monetary(string='Total',compute='_compute_total',readonly=True)
    documentary_support = fields.Many2many("ir.attachment", string="Add documentary support")
    reject_reason = fields.Text('Reason for rejection')
    type = fields.Selection([
        ('recognized', 'Recognized'),
        ('partial_recognized', 'Partial recognized')
    ], "Type", default="")
    action_ids = fields.Many2many(
        'income.recognition.actions',
        'request_income_recognition_action_rel',
        'request_income_recognition_id',
        'recognition_action_id',
        string="Actions",
    )
    domain_bank_movements = fields.Char(compute="_compute_domain_bank_movements", store=False)
    is_group_income_project_coordinator_user = fields.Boolean(compute="_compute_is_group_income_project_coordinator_user")
    is_group_income_dependence_user = fields.Boolean(compute="_compute_is_group_income_dependence_user")
    line_ids = fields.One2many(
        'account.move.line', 'request_income_recognition_id', string="Accounting Notes")
    group = fields.Selection(
        [
            ('income_admin', 'Income Administrator'),
            ('income_project_coordinator', 'Project Coordinator'),
        ],  "Group"
    )
    credit_note_ids = fields.Many2many(
        ACCOUNT_MOVE_MODEL,
        'account_move_credit_note_rel',
        'request_income_recognition_id',
        'credit_note_id',
        string="Credit Notes",
        domain=[('type', '=', 'entry')]
    )
    total_credit_notes = fields.Monetary(string='Total Credit Notes',compute='_compute_total_credit_notes',readonly=True)
    type_request = fields.Selection([
        ('new', 'New'),
        ('update', 'Update'),
    ], "Type Request", default="")

    tickets_ids = fields.Many2many(
        'tickets.store',
        'tickets_store_tickets_ids_rel',
        'request_income_recognition_id',
        'tickets_id',
        string="Tickets",
    )
    total_tickets = fields.Monetary(string='Total Tickets',compute='_compute_total_tickets',readonly=True)
    domain_invoices = fields.Char(compute="_compute_domain_invoices", store=False)


    @api.depends('invoice_ids')
    def _compute_domain_invoices(self):
        is_group_income_admin_user = self.env.user.has_group(INCOME_ADMIN_USER)
        is_group_income_finance_user = self.env.user.has_group(INCOME_FINANCE_USER)
        is_group_income_project_coordinator_user = self.env.user.has_group(INCOME_COORDINATOR_USER)
        is_group_income_dependence_user = self.env.user.has_group(INCOME_DEPENDENCE_USER)
        
        default_domain = self._get_default_domain_invoices()
        if is_group_income_admin_user or is_group_income_finance_user or is_group_income_dependence_user:
            domain = default_domain

        elif is_group_income_project_coordinator_user:
            poi_conacyt_domain = self._get_poi_conacyt_domain()
            poi_conacyt_moves = self.env[ACCOUNT_MOVE_MODEL].search(default_domain + poi_conacyt_domain)
            poi_conacyt_list = poi_conacyt_moves.mapped('payment_of_id.id')
            domain = default_domain + [('payment_of_id.id', 'in', poi_conacyt_list)]
        else:
            domain = []    
        
        self.domain_invoices = json.dumps(domain)


    def _get_default_domain_invoices(self):
        return [
            ('dependancy_id', 'in', self._get_user_dependency_ids()),
            ('sub_dependancy_id', 'in', self.env.user.sub_dependency_income_ids.ids),
            ('type_of_revenue_collection', '=', 'billing'),
            ('invoice_payment_state', '=', 'not_paid'),
            ('is_recognized', '=', False),
            ('currency_id', '=', self.currency_id.id),
            ('state', '=', 'posted'),
            ('type', '=', 'out_invoice'),
        ]


    def _get_user_dependency_ids(self):
        dep_ids = self.env.user.dependency_income_ids.ids
        sub_dependency = self.env[SUB_DEPENDENCY_MODEL].search([('dependency_id', 'in', dep_ids)])
        dep_ids += [d.id for d in self.env.user.sub_dependency_income_ids.dependency_id]
        return dep_ids + sub_dependency.ids


    def _get_poi_conacyt_domain(self):
        return [('payment_of_id.description', 'ilike', 'CONACYT%')]


    def get_domain_dependency(self):
        dep = self.env.user.dependency_income_ids.ids
        dep += [d.id for d in self.env.user.sub_dependency_income_ids.dependency_id]
        
        return [('id', 'in', dep)]

    @api.depends('invoice_ids')
    def _compute_domain_credit_notes(self):
        domain = [
            ('cfdi_serie', 'in', self.invoice_ids.mapped('cfdi_serie')),
            ('folio_cfdi', 'in', self.invoice_ids.mapped('folio_cfdi')),
            ('state', '=', 'draft'),
            ('credit_note_payment_state', '=', False),
            ('type', '=', 'entry'),
            ('type_of_registry_payment', '=', 'credit_note')
        ]

        self.domain_credit_notes = json.dumps(domain)

    domain_credit_notes = fields.Char(compute="_compute_domain_credit_notes", store=False)


    @api.depends('invoice_ids')
    def _compute_domain_tickets(self):
        domain = [
            ('serie', 'in', self.invoice_ids.mapped('cfdi_serie')),
            ('folio_cfd', 'in', self.invoice_ids.mapped('folio_cfdi')),
            ('ticket_store_state', '=', False)
        ]

        self.domain_tickets = json.dumps(domain)

    domain_tickets = fields.Char(compute="_compute_domain_tickets", store=False)


    @api.depends('bank_movements_ids')
    def _compute_domain_bank_movements(self):
        is_group_income_admin_user = self.env.user.has_group(INCOME_ADMIN_USER)
        is_group_income_finance_user = self.env.user.has_group(INCOME_FINANCE_USER)
        is_group_income_project_coordinator_user = self.env.user.has_group(INCOME_COORDINATOR_USER)
        is_group_income_dependence_user = self.env.user.has_group(INCOME_DEPENDENCE_USER)
        default_domain = [('amount_interest', '!=', 0), ('type_movement', '=','unrecognized')]

        if is_group_income_finance_user or is_group_income_admin_user:  
            domain = default_domain

        elif is_group_income_project_coordinator_user:
            domain = default_domain
            account_journal_configuration = self.env["configuration.journals"].search([("is_active", "=", True),("coordinator_user", "=", True)])
            income_bank_journal_ids = account_journal_configuration.journal_id.ids
            domain.append(('income_bank_journal_id.id', 'in', income_bank_journal_ids))
      
        elif  is_group_income_dependence_user:
            domain = default_domain

        self.domain_bank_movements = json.dumps(domain)
    

    @api.depends('is_group_income_project_coordinator_user')
    def _compute_is_group_income_project_coordinator_user(self):
        self.is_group_income_project_coordinator_user = self.env.user.has_group(INCOME_COORDINATOR_USER)


    @api.depends('is_group_income_dependence_user')
    def _compute_is_group_income_dependence_user(self):
        self.is_group_income_dependence_user = self.env.user.has_group(INCOME_DEPENDENCE_USER)    
        

    @api.depends('dependency_id')
    def _compute_domain_sub_dependency(self):
        for record in self:
            domain = [('dependency_id', '=', self.dependency_id.id)]
            if self.dependency_id.id not in self.env.user.dependency_income_ids.ids:
                domain.append(('id', 'in', self.env.user.sub_dependency_income_ids.ids))
            record.domain_sub_dependency = json.dumps(domain)
    

    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        res = super(RequestIncomeRecognition, self).fields_view_get(
            view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])

        is_group_income_admin_user = self.env.user.has_group(INCOME_ADMIN_USER)
        if not is_group_income_admin_user:
            for node in doc.xpath("//" + view_type):
                node.set('export', '0')

        res['arch'] = etree.tostring(doc)
        return res
           
    
    def get_dependancy_subdependancy(self):
        if self.invoice_ids.dependancy_id.ids[-1]:
            dependency_id = self.invoice_ids.dependancy_id.ids[-1]
        if self.invoice_ids.sub_dependancy_id.ids[-1]:    
            sub_dependency_id = self.invoice_ids.sub_dependancy_id.ids[-1]

        return dependency_id, sub_dependency_id
    

    def write(self,vals):
        if 'invoice_ids' in vals:
            self.update_bank_to_invoice()
   
        if self.has_invoice():
            dependency_id, sub_dependency_id = self.get_dependancy_subdependancy()
            vals['dependency_id'] = dependency_id
            vals['sub_dependency_id'] = sub_dependency_id

        if 'bank_movements_ids' in vals:
            bm_obj = self.env['bank.movements']
            original_movements = self._origin.bank_movements_ids.ids
            # Quitar movimento
            if original_movements not in vals['bank_movements_ids']:
                bank_movement = bm_obj.search([('id', 'in', original_movements)])
                for movement in bank_movement:
                    if movement.amount_unrecognized != 0:    
                        movement.type_movement = 'partial_recognized'
                    else:
                        movement.type_movement = 'unrecognized'

            # Agregar movimiento manual
            if len(vals['bank_movements_ids'][0]) >= 3:
                movements = vals['bank_movements_ids'][0][2]
                bank_movement = bm_obj.search([('id', 'in', movements)])
                for movement in bank_movement:
                    if movement.type_movement == 'unrecognized':
                            movement.type_movement = 'inprocess' 


        if 'credit_note_ids' in vals:
            am_obj = self.env[ACCOUNT_MOVE_MODEL]
            original_credit_notes = self._origin.credit_note_ids.ids
            # Quitar nota de credito
            if original_credit_notes not in vals['credit_note_ids']:
                credit_note = am_obj.search([('id', 'in', original_credit_notes)])
                for note in credit_note:
                    note.credit_note_payment_state = None

            # Agregar nota de credito
            movements = vals['credit_note_ids'][0][2]
            credit_note = am_obj.search([('id', 'in', movements)])
            for note in credit_note:
                note.credit_note_payment_state = 'in_request'      
    
        res = super(RequestIncomeRecognition,self).write(vals)
        return res


    def notify_users(self):
        admin_users = self.env.ref(INCOME_ADMIN_USER).users 
        finance_users = self.env.ref(INCOME_FINANCE_USER).users
        users = (admin_users + finance_users).filtered(lambda user: user not in self.activity_ids.filtered(lambda a: a.activity_type_id.name == 'Solicitud de reconocimiento de ingresos').mapped('user_id'))
        
        if users:
            note_msg = _("Request pending verification.")
            for user in users:
                self.activity_schedule(ACTIVITY_SCHEDULE_ID, user_id=user.id, note=note_msg)
                    
                
    def update_state(self, new_state):
        for rec in self:
            rec.state = new_state


    @api.depends('bank_movements_ids', 'bank_movements_ids.amount_interest')
    def _compute_total_bank_movements(self):
        for record in self:
            record.total_bank_movements = sum(x.amount_interest for x in record.bank_movements_ids)


    @api.depends('invoice_ids', 'invoice_ids.amount_interest')
    def _compute_total_invoices(self):
        for record in self:
            record.total_invoices = sum(x.amount_interest for x in record.invoice_ids)


    @api.depends('credit_note_ids', 'credit_note_ids.amount_note')
    def _compute_total_credit_notes(self):
        for record in self:
            record.total_credit_notes = sum(x.amount_note for x in record.credit_note_ids)


    @api.depends('tickets_ids', 'tickets_ids.importe')
    def _compute_total_tickets(self):
        for record in self:
            record.total_tickets = sum(x.importe for x in record.tickets_ids)                    


    @api.depends('total_invoices')
    def _compute_total(self):
        total_invoices = self.total_invoices
        total_credit_notes = self.total_credit_notes

        self.total = total_invoices - total_credit_notes


    def allow_account_configuration(self):
        bank_movement_journals = self.bank_movements_ids.mapped('income_bank_journal_id')
        configuration_journals = self.env["configuration.journals"].search([
            ("is_active", "=", True),
            ("journal_id", "in", bank_movement_journals.ids),
        ])

        return bool(configuration_journals)         


    def validations_register(self):
        if not self.has_bank_movement():
            raise ValidationError(_("There aren´t bank movements added."))

        if not self.has_action():
            total = round(self.total, 2)  
            total_movements = round(self.total_bank_movements, 2)

            if not self.has_invoice():
                raise ValidationError(_("No invoices added."))

            if not self.has_ticket() and total != total_movements :
                if not self.allow_account_configuration():
                    raise ValidationError(_("The amount of the deposits is not equal to the invoice."))


    def has_invoice(self):
        if self.invoice_ids:
            return True


    def has_credit_note(self):
        if self.credit_note_ids:
            return True


    def has_ticket(self):
        if self.tickets_ids:
            return True    
        

    def has_bank_movement(self):
        if self.bank_movements_ids:
            return True


    def has_action(self):
        if self.action_ids:
            return True    
        

    def register(self):
        # Si pasa todas las validaciones se registra
        self.validations_register()
        #self.notify_users()
        self.update_state('registered')  


    def was_reclassified_message(self):   
   
        return {
                'name': _('Done'),
                'type': ACTION_WINDOW,
                'view_mode': 'form',
                'res_model': REQUEST_INCOME_RECOGNITION_WIZARD,
                'view_type': 'form',
                'views': [(self.env.ref("siif_income.was_reclassified_wizard_view_form").id, 'form')],
                'target': 'new',
                }


    def make_actions(self):
        type_of_revenue_collection, accounting_account, dependency_id, sub_dependency_id = '','','',''

        for action in self.action_ids:
            if action.type_of_revenue_collection:
                type_of_revenue_collection = action.type_of_revenue_collection
                break
            elif action.accounting_account:
                accounting_account = action.accounting_account
                dependency_id = action.dependency_id
                sub_dependency_id = action.sub_dependency_id
                break
        
        if type_of_revenue_collection:
            was_reclassified = self.actions_reclassify(type_of_revenue_collection)
            if was_reclassified:
                self.update_state('accepted')
                return self.was_reclassified_message()    
            
        elif accounting_account:
            self.actions_accounting_account(accounting_account, dependency_id, sub_dependency_id)               


    def accept(self):
        total_movements = self.total_bank_movements
        gran_total = self.total
        RECOGNIZED = 'recognized'
        self.update_bank_movement_state_request(RECOGNIZED)

        if self.allow_account_configuration() and (total_movements != gran_total) and not self.has_action():
            self.update_bank_movement_state_amounts()
      
        if self.has_invoice():
            self.update_bank_reference()
            self.recognize_invoices()

        if self.has_action():
            self.make_actions()
            
        if self.has_ticket():
            self.update_ticket_state(RECOGNIZED)

        self.update_state('accepted')


    def update_bank_movement_state_request(self, new_state):
        for record in self.bank_movements_ids:
            record.type_movement = new_state
            record.recognition_request_id = self.id


    def update_bank_movement_state_amounts(self):
        total_invoices = self.total_invoices
        total_movements = self.total_bank_movements
        total_unrecognized =  sum(x.amount_unrecognized  for x in self.bank_movements_ids)
        amount_unrecognized =  total_movements - total_invoices
        
        for record in self.bank_movements_ids:
            # En el primer flujo se cubre
            if total_movements == total_invoices:
                record.type_movement = 'recognized'
                record.recognition_request_id = self.id
                record.amount_recognized += total_invoices
                record.amount_unrecognized -= total_invoices
        
            elif record.amount_unrecognized != 0:
                if total_unrecognized < total_invoices:
                    raise ValidationError(_("Available amount is less than invoiced."))

                if record.amount_unrecognized == total_invoices:
                    record.type_movement = 'recognized'
                    record.recognition_request_id = self.id
                    record.amount_recognized += total_invoices
                    record.amount_unrecognized -= total_invoices
                else:
                    record.amount_recognized += total_invoices
                    record.amount_unrecognized -= total_invoices

            else: 
                record.type_movement = 'partial_recognized'
                record.recognition_request_id = self.id
                record.amount_recognized = total_invoices
                record.amount_unrecognized = amount_unrecognized                                        
                

    def reject_wizard(self):
        movements_ids = self.env[REQUEST_INCOME_RECOGNITION_MODEL].browse(self._context.get('active_ids'))
        # Se actualizan los status
        return {
            'name': _('Reason for rejection'),
            'type': ACTION_WINDOW,
            'res_model': REQUEST_INCOME_RECOGNITION_WIZARD,
            'view_type': 'form',
            'view_mode': 'form',
            'views': [(self.env.ref("siif_income.reject_wizard_view_form").id, 'form')],
            'context': {
                'default_id': movements_ids,
            },
            'target': 'new'}
    

    def update_bank_to_invoice(self):
        income_bank_journal_id = self.bank_movements_ids[-1].income_bank_journal_id.id
        for record in self.invoice_ids:    
            record.income_bank_journal_id = income_bank_journal_id

    def update_bank_reference(self):
        bank_ref = ''
        for movement in self.bank_movements_ids:

            ref = movement.ref if movement.ref != None and movement.ref != False else ''
            ref_com_1 = movement.ref if movement.reference_plugin_1 != None and movement.reference_plugin_1 != False else ''
            ref_com_2 = movement.ref if movement.reference_plugin_2 != None and movement.reference_plugin_2 != False else ''
            ref_com_3 = movement.ref if movement.reference_plugin_3 != None and movement.reference_plugin_3 != False else ''
            customer_ref = movement.customer_ref if movement.customer_ref != None and movement.customer_ref != False else ''

            bank_ref = f"{bank_ref}REF: {ref}REF COM1: {ref_com_1}REF COM2: {ref_com_2}REF COM3: {ref_com_3}REF CLI: {customer_ref} " 

        for invoice in self.invoice_ids:
            invoice_ref = invoice.ref if invoice.ref != None and invoice.ref != False else ''
            invoice.ref = invoice_ref+bank_ref



    def recognize_invoices(self):
        payment_date = datetime.today().date()
        payment_list = []

        if self.has_credit_note():
            # Publicar notas
            self.env[ACCOUNT_MOVE_MODEL].post_credit_note(self.credit_note_ids)

        for record in self.invoice_ids:
            record.invoice_date_conciliation = payment_date
            record.is_recognized = True
            # Asociar donativo si la referencia es igual
            self.associate_donation(record)
            
            credit_note = next((cn for cn in self.credit_note_ids if cn.folio_uuid_related == record.invoice_uuid), None)
            if credit_note:
                amount = self._process_credit_note(record, credit_note)
            else:
                amount = record.amount_interest

            if record.type_of_invoice == 'cash_closing':
                for tickets_ids in self.tickets_ids:
                    for ticket in record.income_invoice_line_ids:
                        if tickets_ids.folio_cfs in ticket.name:
                            ticket_name = ticket.name
                            amount = ticket.price_subtotal
                            payment_list.append(self._prepare_payment_values(record, amount, payment_date, ticket_name))

            else:
                payment_list.append(self._prepare_payment_values(record, amount, payment_date))

        if payment_list:
            payment_register_id = self.env['account.payment'].create(payment_list)
            payment_register_id.post()
        
        if self.has_credit_note():
            # Despues del pago se actualiza sino por default no se hace
            for invoice in self.invoice_ids:
                invoice.invoice_payment_state = 'paid'
                invoice.amount_residual = 0

    
    def _process_credit_note(self, record, credit_note):
        # Reconoce la nota de crédito para no volver a usarla
        credit_note.credit_note_payment_state = 'recognized'
        # Resta el monto de la nota de crédito del monto de la factura
        amount = record.amount_interest - credit_note.amount_note
        # Asigna a factura el importe de la nota relacionada
        record.related_note_amount = credit_note.amount_note    
        return amount
    

    def _prepare_payment_values(self, record, amount, payment_date, ticket_name=None):
        return {
            'state': 'draft',
            'amount': amount,
            'payment_type': 'inbound',
            'currency_id': record.currency_id.id,
            'payment_date': payment_date,
            'dependancy_id': record.dependancy_id.id,
            'sub_dependancy_id': record.sub_dependancy_id.id,
            'communication': record.name,
            'partner_id': record.partner_id.id,
            'partner_type': 'customer',
            'type_of_revenue_collection': 'billing',
            'payment_method_id': 1,
            'journal_id': record.income_bank_journal_id.id,
            'invoice_ids': record.ids,
            'ticket_description': ticket_name if record.type_of_invoice == 'cash_closing' else False,
            'l10n_mx_edi_payment_method_id': record.l10n_mx_edi_payment_method_id.id,
            'cfdi_serie': record.cfdi_serie,
            'cfdi_folio': record.folio_cfdi,
        }
    '''def update_iva_payment(self):

        for record in self.credit_note_ids:
            if record.amount_tax_signed not in (0.0, False):
                tax_amount = abs(record.amount_tax_signed)
                payment_id = self.env['account.payment'].search([('cfdi_folio', '=', record.folio_cfdi), ('cfdi_serie','=', record.cfdi_serie)])
                tax_id = self.env['account.tax'].search([('description', '=', record.iva), ('price_include', '=', False)])
                if payment_id and tax_id:
                    for line in payment_id.move_line_ids.filtered(lambda x:x.account_id.id in (tax_id.iva_to_move_account_id.id, tax_id.iva_transferred_account_id.id)):
                        if line.debit>0.0:
                            line.debit = line.debit-tax_amount
                            line.balance = line.balancé-tax_amount
                        elif line.credit>0.0:
                            line.credit = line.credit-tax_amount
                            line.balance = line.balancé+tax_amount'''


    def associate_donation(self, record_invoice):
        donation = self.env['donations'].search([('deposit_token.payment_reference', '=', record_invoice.ref), ('donation_status', '=', 'with_reference')])

        if donation:
            invoice_id = record_invoice.id
            donation.write({'invoice_id': invoice_id, 'donation_status': 'charged'})
            self.bank_movements_ids.write({'donations_id': donation.id, 'invoice_id': invoice_id})

  
    def notify_user_reject(self):
        if self.name:
            create_uid =  self.create_uid.id
            note_msg = _("Rejected request.")                                                                                                                                        
            self.activity_schedule(ACTIVITY_SCHEDULE_ID, user_id=create_uid, note=note_msg)    
       
    
    def notify_user_approbe(self):
        if self.name:
            create_uid =  self.create_uid.id
            note_msg = _("Approved request.")                                                                                                                                            
            self.activity_schedule(ACTIVITY_SCHEDULE_ID, user_id=create_uid, note=note_msg)                


    def reject(self, reject_reason):
        # Actualización del status de los depositos a en proceso de reconocer
        for record in self.bank_movements_ids:
            record.type_movement = 'unrecognized'

        self.reject_reason = reject_reason
        #self.notify_user_reject()
        self.update_state('rejected')


    def back_draft(self):
        # Regresar a borrador si se rechaza
        self.update_state('draft')
                

    def succesful_request_popup(self):   
        message_id = self.env[REQUEST_INCOME_RECOGNITION_WIZARD].create({'message': _('Request created successfully')})

        return {
            'name': _('Done'),
            'type': ACTION_WINDOW,
            'view_mode': 'form',
            'res_model': REQUEST_INCOME_RECOGNITION_WIZARD,
            'views': [(self.env.ref("siif_income.successful_request_wizard_view_form").id, 'form')],
            'res_id': message_id.id,
            'target': 'new'
        }
        

    def get_domain_dep_subdep_list(self):
        dep = self.env.user.dependency_income_ids.ids
        sub_dependency = self.env[SUB_DEPENDENCY_MODEL].search([('dependency_id', 'in', dep)])
        dep += [d.id for d in self.env.user.sub_dependency_income_ids.dependency_id]
        subdep = self.env.user.sub_dependency_income_ids.ids
        subdep += sub_dependency.ids

        domain = [('dependency_id', 'in', dep), ('sub_dependency_id', 'in', subdep)]

        return domain               
    

    def logged_user(self):
        is_group_income_project_coordinator_user = self.env.user.has_group(INCOME_COORDINATOR_USER)
        views = [(self.env.ref('siif_income.request_income_recognition_tree').id, 'tree'), (self.env.ref('siif_income.request_income_recognition_form').id, 'form')]
        domain = self.get_domain_dep_subdep_list() 
  
        if is_group_income_project_coordinator_user:
            domain.append(('group', '=', 'income_project_coordinator'))
 
        return {
            'name': _('Request Income Recognition'),#'Solicitudes de reconocimiento de ingresos',
            "res_model": REQUEST_INCOME_RECOGNITION_MODEL,
            "domain": domain,
            "type": "ir.actions.act_window",
            "view_mode": "tree,form",
            "views": views,
        }


    def actions_reclassify(self, type_of_revenue_collection):
        # Solo usuario finanzas y admin ingresos se notifica cuando es acción
        account_move_obj = self.env[ACCOUNT_MOVE_MODEL]
        adia_obj = self.env['association.distribution.ie.accounts']
        aj_obj = self.env['account.journal']
        sor_obj = self.env['sub.origin.resource']
        journal_id = ''
        collection_type = ''
        affectation_account = ''
        sub_origin_resource = ''
        commission_description = ''
        bank = ''
        register_id = ''

        # Es reclasificación
        if type_of_revenue_collection:
            if type_of_revenue_collection == 'education_servs':
                collection_type = type_of_revenue_collection
                affectation_account = adia_obj.search([('ie_key','=','ServiciosEd')]).id
                sub_origin_resource = sor_obj.search([('name', '=', 'Servicios de Educación')]).id
                journal_id = aj_obj.search([('code', '=', 'FDA01')]).id
            elif type_of_revenue_collection == 'aspirants':
                collection_type = type_of_revenue_collection
                affectation_account = adia_obj.search([('ie_key','=','Aspirantes')]).id
                sub_origin_resource = sor_obj.search([('name', '=', 'Aspirantes')]).id
                journal_id = aj_obj.search([('code', '=', 'FDA01')]).id
            elif type_of_revenue_collection == 'exam_prep':
                collection_type = type_of_revenue_collection
                affectation_account = adia_obj.search([('ie_key','=','ExAdmision')]).id
                sub_origin_resource = sor_obj.search([('name', '=', 'Preparación de Examen')]).id
                journal_id = aj_obj.search([('code', '=', 'FDA01')]).id


            if self.bank_movements_ids:
                for record in self.bank_movements_ids:
                    if record.description_of_the_movement != '':
                        commission_description = record.description_of_the_movement
                    else:
                        commission_description = record.reference_plugin_1

                    if record.income_bank_journal_id:
                        bank = record.income_bank_journal_id

                    comission = self.env['comission.settings'].search([('type_of_revenue_collection','=',type_of_revenue_collection), ('income_bank_journal_id','=', bank.id),
                        ('comission_description','=', commission_description), ('is_retention','=', True)])   
                    payment_method = self.env['l10n_mx_edi.payment.method'].search([('code','=',comission.payment_method_code)])
                    
    
                    if type_of_revenue_collection == 'bank_interest':
                        journal_id = record.income_bank_journal_id.id

                    vals = {'type_of_revenue_collection': type_of_revenue_collection,
                            'collection_type': collection_type,
                            'income_type': 'extra',
                            'type': 'entry',
                            'income_bank_journal_id': record.income_bank_journal_id.id,
                            'movement_date': record.movement_date or False,
                            'settlement_date': record.settlement_date or False,
                            'income_id': record.id_movto_bancario,
                            'income_branch': record.bank_branch,
                            'ref': record.ref,
                            'customer_ref': record.customer_ref,
                            'reference_plugin': record.reference_plugin_1,
                            'reference_plugin_2': record.reference_plugin_2,
                            'reference_plugin_3': record.reference_plugin_3,
                            'id_movto_ban': record.id_movto_ban,
                            'description_of_the_movement': record.description_of_the_movement,
                            'income_status': 'approved',
                            'sub_origin_resource_id': sub_origin_resource,
                            'l10n_mx_edi_payment_method_id': payment_method.id,
                            'journal_id': journal_id,
                            'comission_id': record.comission_id or False,
                            'invoice_date': record.settlement_date or False,
                            'afectation_account_id': affectation_account,
                            'amount_interest': record.amount_interest or record.charge_amount,
                            'ie_account': True,
                        }

                    record.type_movement = 'reclassified'
                    record.recognition_request_id = self.id    

                    register_id = account_move_obj.create(vals)
                    record.reclassified_to = register_id.id

        return register_id


    def actions_accounting_account(self, accounting_account, dependency_id, sub_dependency_id):
        move_id = ''
        today = datetime.today().date()
        partner_id = self.env.user.partner_id.id

        # Es cuenta contable
        if accounting_account:
            if self.bank_movements_ids:
                    for record in self.bank_movements_ids:
                        
                        move_vals = {'ref': record.ref, 'conac_move': False,
                                    'date': today, 'journal_id': record.income_bank_journal_id.id, 'company_id': self.env.user.company_id.id,
                                    'line_ids': [(0, 0, {
                                        'name':	accounting_account.name,
                                        'dependency_id': dependency_id.id,
                                        'sub_dependency_id': sub_dependency_id.id,
                                        'account_id': accounting_account.id,
                                        # 'coa_conac_id': False,
                                        'credit': record.amount_interest,
                                        'partner_id': partner_id,
                                        'request_income_recognition_id': self.id,
                                        }),
                                        (0, 0, {
                                        'account_id': record.income_bank_journal_id.default_debit_account_id.id,
                                        # 'coa_conac_id': False,
                                        'debit': record.amount_interest,
                                        'partner_id': partner_id,
                                        'request_income_recognition_id': self.id,
                                        }),
                                    ]}

                        move_obj = self.env[ACCOUNT_MOVE_MODEL]
                        move_id = move_obj.create(move_vals)
                        move_id.action_post()

        return move_id


    def update_bank_movement_state(self, new_state):
        for record in self.bank_movements_ids:
            record.type_movement = new_state


    def update_invoice_state(self, new_state, is_recognized):
        for record in self.invoice_ids:
            record.invoice_payment_state = new_state
            record.is_recognized = is_recognized


    def update_credit_note_state(self, new_state_note):
        for record in self.credit_note_ids:
            record.credit_note_payment_state = new_state_note


    def update_ticket_state(self, new_state):
        for record in self.tickets_ids:
            record.ticket_store_state = new_state                         


    def unlink(self):
        allowed_groups = [INCOME_ADMIN_USER, 
                          'jt_income.group_income_admin_finance_user',    
                        ]
        
        if not any(self.env.user.has_group(group) for group in allowed_groups):
            raise UserError(_("You aren't allowed to perform this action."))
        
        if self.state == 'accepted':
            raise ValidationError(_("Can't delete approved requests."))
        # Regresar a su estado original
        self.update_bank_movement_state('unrecognized')
        self.update_invoice_state('not_paid', None)
        self.update_credit_note_state(None)
        self.update_ticket_state(None)

        res = super(RequestIncomeRecognition, self).unlink()
        return res


class AccountMoveLine(models.Model):

    _inherit = 'account.move.line'

    request_income_recognition_id = fields.Many2one(REQUEST_INCOME_RECOGNITION_MODEL,'Request Income Recognition')        

                   