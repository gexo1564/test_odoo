from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
import logging
class TotalDenominations(models.Model):

    _name = 'total.denominations'
    _description = 'Denominations for quotes'

    count = fields.Integer(string='Bills Number',required=True)
    denominations = fields.Selection(
        [
            ('den_500', '$500'),
            ('den_200', '$200'),
            ('den_100', '$100'),
            ('den_50', '$50'),
            ('den_20', '$20'),
            ('den_10', '$10'),
            ('den_5', '$5'),
            ('den_2', '$2'),
            ('den_1', '$1'),
        ], string="Denomination",required=True
    )
    application_request = fields.Many2one('application.request')

    @api.model
    def create(self, vals):
        res = super(TotalDenominations, self).create(vals)
        return res
    
    @api.constrains('count')
    def _check_count(self):  
        for record in self:
            if record.count<=0:
                raise ValidationError(_("Number bills cannot be 0 or a negative number."))

class TotalDenominationsQuote(models.Model):

    _name = 'total.denominations.quote'
    _description = 'Quote Denomination'

    count = fields.Integer(string='Bills Number',required=True)
    denominations = fields.Selection(
        [
            ('den_500', '$500'),
            ('den_200', '$200'),
            ('den_100', '$100'),
            ('den_50', '$50'),
            ('den_20', '$20'),
            ('den_10', '$10'),
            ('den_5', '$5'),
            ('den_2', '$2'),
            ('den_1', '$1'),
        ], string="Denomination",required=True
    )
    quote_id = fields.Many2one('quote')

    @api.model
    def create(self, vals):
        res = super(TotalDenominationsQuote, self).create(vals)
        return res
    
    @api.constrains('count')
    def _check_count(self):  
        for record in self:
            if record.count<=0:
                raise ValidationError(_("Number bills cannot be 0 or a negative number."))
