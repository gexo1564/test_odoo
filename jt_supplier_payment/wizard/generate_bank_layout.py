# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields,api,_
from odoo.exceptions import UserError, ValidationError
import base64
from datetime import datetime, timedelta
from pytz import timezone
from odoo.tools.misc import formatLang, format_date, get_lang
from babel.dates import format_datetime, format_date
import re as regex_str
import logging
_logger = logging.getLogger(__name__)

TXT_FILE_FORMAT = "%s.txt"
TXT_GEN_BANK_LAYOUT = 'Generate Bank Layout'

ACCOUNT_PAYMENT='account.payment'
GEN_BANK_LAYOUT='generate.bank.layout'
IR_ACT_WINDOW='ir.actions.act_window'

class GenerateBankLayout(models.TransientModel):

    _name = 'generate.bank.layout'
    _description = 'Generate Bank Layout'
    
    journal_id = fields.Many2one('account.journal','Select the file to generate')
    payment_ids = fields.Many2many(ACCOUNT_PAYMENT,'account_payment_bank_layout_rel','bank_layout_id','payment_id','Payments')
    an_inter_bank_partic = fields.Boolean('Does an intermediary bank participate?')
    inter_bank_id = fields.Many2one('res.bank','Intermediary bank')
    file_name = fields.Char('Filename')
    file_data = fields.Binary('Download')
    file_name_error = fields.Char(string='Filename Error')
    file_data_error = fields.Binary(string='Error File Download')
    sit_file_key = fields.Char('File Key',size=30)
    bank_format = fields.Selection(related='journal_id.bank_format')

    is_from_citibank = fields.Boolean('Is from citibank?', default=False)
    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')

    @api.constrains('start_date','end_date')
    def _check_start_end_date(self):

        if self.start_date>=self.end_date and self.is_from_citibank:
            raise UserError('El rango de la fecha está incorrecto, asigne las fechas en orden.')
    
    @api.onchange('journal_id')
    def onchange_journal_id(self):
        self.sit_file_key = False
        if self.bank_format=='bbva_sit':
            sit_file_key = 'TRANSFERUNAM'
            currect_time = datetime.today()
            
            sit_file_key +=str(currect_time.year)
            sit_file_key +=str(currect_time.month).zfill(2)
            sit_file_key +=str(currect_time.day).zfill(2)

            if self.payment_ids:
                if self.payment_ids[0].currency_id.name=='MXN':
                    sit_file_key += 'MN'
                else:
                    sit_file_key += "ME"
                if self.payment_ids[0].sit_operation_code=='payment_on_account_bancomer':
                    sit_file_key += 'BBVA'
                elif self.payment_ids[0].sit_operation_code=='payment_interbank':
                    sit_file_key += "OTROS"
            
            self.sit_file_key = sit_file_key
            self.is_from_citibank=False
        elif self.bank_format == 'citibank':
            self.is_from_citibank=True
        else:
            self.is_from_citibank=False
            
        
    def action_generate_bank_layout(self):
        
        active_ids = self.env.context.get('active_ids')
        for payment in self.env[ACCOUNT_PAYMENT].browse(active_ids):
            if payment.payment_state != 'for_payment_procedure':
                raise UserError(_("You can generate Bank Layout only for those payments which are in "
                "'For Payment Procedure'!"))
        if not active_ids:
            return ''
        active_rec = self.env[ACCOUNT_PAYMENT].browse(active_ids)
        if any(active_rec.filtered(lambda x:x.payment_request_type in ('project_payment','supplier_payment','different_to_payroll'))):
            return {
                'name': _(TXT_GEN_BANK_LAYOUT),
                'res_model': GEN_BANK_LAYOUT,
                'view_mode': 'form',
                'view_id': self.env.ref('jt_supplier_payment.view_generate_bank_layout').id,
                'context': {'default_payment_ids':[(6,0,active_ids)]},
                'target': 'new',
                'type': IR_ACT_WINDOW,
            }
        else:
            return {
                'name': _(TXT_GEN_BANK_LAYOUT),
                'res_model': GEN_BANK_LAYOUT,
                'view_mode': 'form',
                'view_id': self.env.ref('jt_supplier_payment.view_generate_payroll_payment_bank_layout').id,
                'context': {'default_payment_ids':[(6,0,active_ids)]},
                'target': 'new',
                'type': IR_ACT_WINDOW,
            }
            
    def banamex_file_format(self):
        file_data = ''
        for payment in self.payment_ids:
            file_data += '03'
            if payment.journal_id.account_type:
                if payment.journal_id.account_type=='check':
                    file_data += '01'
                    if payment.journal_id.branch_office:
                        file_data += payment.journal_id.branch_office.zfill(4)
                    else: 
                        file_data += '0000'                        
                elif payment.journal_id.account_type=='card':
                    file_data += '03'
                    file_data += '0000'
                elif payment.journal_id.account_type=='master':
                    file_data += '06'
                    file_data += '0000'
                else:
                    file_data += '000000'
            else:
                file_data += '000000'
            if payment.journal_id.bank_account_id:
                file_data += payment.journal_id.bank_account_id.acc_number.zfill(20)
            else:
                temp =''
                file_data +=temp.zfill(20)
           
            #==== Receipt Bank DATA ===========
            if payment.payment_bank_account_id:
                if payment.payment_bank_account_id.account_type=='checks':
                    file_data += '01'
                    if payment.payment_bank_account_id.branch_number:
                        file_data += payment.payment_bank_account_id.branch_number.zfill(4)
                    else: 
                        file_data += '0000'                        
                elif payment.payment_bank_account_id.account_type=='card':
                    file_data += '03'
                    file_data += '0000'
                elif payment.payment_bank_account_id.account_type=='master_acc':
                    file_data += '06'
                    file_data += '0000'
                else:
                    file_data += '00'
                    file_data += '0000'
            else:
                file_data += '000000'
            if payment.payment_bank_account_id:
                file_data += payment.payment_bank_account_id.acc_number.zfill(20)
            else:
                temp =''
                file_data +=temp.zfill(20)
            
            #====== Amount Data =========
            amount = round(payment.amount, 2)         
            amount = str(amount).split('.')
            file_data +=str(amount[0]).zfill(12)
            file_data +=str(amount[1])
            #====== Currency Data =========
            if payment.currency_id:
                if payment.currency_id.name=='USD':
                    file_data +='005'
                elif payment.currency_id.name=='MXN':
                    file_data +='001'
            
            #====== Description =========
            temp_desc = ''
#             if payment.banamex_description:
#                 temp_desc = payment.banamex_description
            if payment.dependancy_id and payment.dependancy_id.sort_description:
                temp_desc += payment.dependancy_id.sort_description
            if payment.folio:
                if temp_desc:
                    temp_desc += " "+payment.folio
                else:
                    temp_desc += payment.folio
                    
            file_data += temp_desc.ljust(24, " ")
            #====== banamex_concept =========
            temp_desc = ''
#             if payment.banamex_concept:
#                 temp_desc = payment.banamex_concept
            if payment.dependancy_id and payment.dependancy_id.sort_description:
                temp_desc += payment.dependancy_id.sort_description
            if payment.name:
                if temp_desc:
                    temp_desc += " "+payment.name
                else:
                    temp_desc += payment.name

            file_data += temp_desc.ljust(34, " ")
    
            #====== banamex_reference =========
            temp_desc = ''
            if payment.banamex_concept:
                temp_desc = payment.banamex_concept.split('/',2)
                file_data += temp_desc[2].zfill(10)
                
            #====== Currency constant =========
            file_data += "000"
            #====== Current Dat time=========
            file_data += ''.zfill(10)
            file_data +="\r\n"
        gentextfile = base64.b64encode(bytes(file_data,'utf-8'))
        #Archivo y nombre del Layout generado
        self.file_data = gentextfile
        self.file_name = TXT_FILE_FORMAT % self.sit_file_key
        
    def bbva_tnn_ptc_file_format(self):
        file_data = ''
        file_name = 'BBVA BANCOMER NET CASH​ (TNN or PTC).txt'
        
        for payment in self.payment_ids:
            #==== Receipt Bank DATA ===========#            
            if payment.payment_bank_account_id:
                file_data += payment.payment_bank_account_id.acc_number.zfill(18)
            else:
                temp =''
                file_data +=temp.zfill(18)
            #======= Bank Issuance of payment ======#    
            if payment.journal_id.bank_account_id:
                file_data += payment.journal_id.bank_account_id.acc_number.zfill(18)
            else:
                temp =''
                file_data +=temp.zfill(18)
            #====== Currency Data =========#
            if payment.currency_id:
                if payment.currency_id.name=='USD':
                    file_data +='USD'
                elif payment.currency_id.name=='MXN':
                    file_data += 'MXP'
                else:
                    file_data += payment.currency_id.name
            #====== Amount Data =========#
            amount = round(payment.amount, 2)
            amount = "%.2f" % payment.amount            
            amount = str(amount).split('.')
            file_data +=str(amount[0]).zfill(13)
            file_data +='.'
            file_data +=str(amount[1])
            
            #=====reason for payment======#
            reason_payment = 'PAGO'
            if payment.payment_date:
                payment_year = str(payment.payment_date.year)[2:]
                month_name = format_datetime(payment.payment_date, 'MMMM', locale=get_lang(self.env).code)
                if payment.payment_date.month==1:
                    month_name = 'ENERO'
                elif payment.payment_date.month==2:
                    month_name = 'FEBRERO'
                elif payment.payment_date.month==3:
                    month_name = 'MARZO'
                elif payment.payment_date.month==4:
                    month_name = 'ABRIL'
                elif payment.payment_date.month==5:
                    month_name = 'MAYO'
                elif payment.payment_date.month==6:
                    month_name = 'JUNIO'
                elif payment.payment_date.month==7:
                    month_name = 'JULIO'
                elif payment.payment_date.month==8:
                    month_name = 'AGOSTO'
                elif payment.payment_date.month==9:
                    month_name = 'SEPTIEMBRE'
                elif payment.payment_date.month==10:
                    month_name = 'OCTUBRE'
                elif payment.payment_date.month==11:
                    month_name = 'NOVIEMBRE'
                elif payment.payment_date.month==12:
                    month_name = 'DICIEMBRE'
                
                reason_payment += " "+month_name+" "+payment_year
            file_data += reason_payment.ljust(30, " ")     
            file_data +="\r\n"      
        gentextfile = base64.b64encode(bytes(file_data,'utf-8'))
        self.file_data = gentextfile
        self.file_name = TXT_FILE_FORMAT % self.sit_file_key

    def bbva_tsc_pcs_file_format(self):
        file_data = ''
        file_data_error = ''
        file_data_tmp = ''
        file_name = 'BBVA BANCOMER NET CASH​ ​ (TSC or PSC).txt'
        ascii_non_accepted = '!@#$%&/()*+;:[]=^`{|}~,.?^<\'".-'
        
        for payment in self.payment_ids:
            is_correct_data = True
            #==== Receipt Bank DATA ===========#            
            if payment.payment_bank_account_id:
                file_data_tmp += payment.payment_bank_account_id.acc_number.zfill(18)
            else:
                temp =''
                file_data_tmp +=temp.zfill(18)
            if payment.journal_id.bank_account_id:
                file_data_tmp += payment.journal_id.bank_account_id.acc_number.zfill(18)
            else:
                temp =''
                file_data_tmp +=temp.zfill(18)
            #====== Currency Data =========#
            file_data_tmp += 'MXP'
#             if payment.currency_id:
#                 file_data += payment.currency_id.name
            #====== Amount Data =========#
            amount = round(payment.amount, 2)
            amount = "%.2f" % payment.amount            
            amount = str(amount).split('.')
            file_data_tmp +=str(amount[0]).zfill(13)
            file_data_tmp +='.'
            file_data_tmp +=str(amount[1])

            #=====“Beneficiary”======#
            beneficiary_name=''
            if payment.partner_id:
                beneficiary_name = regex_str.sub(r'[^\x00-\x7F]+', '', payment.partner_id.name.translate(str.maketrans('áéíóúüñÁÉÍÓÚÜÑ', 'aeiouunAEIOUUN')))
                for special_character in ascii_non_accepted:
                    beneficiary_name = beneficiary_name.replace(special_character, '')
            file_data_tmp += (' '.join(beneficiary_name.split()))[:30].ljust(30)
            # ======= Type of Account ========#
            bank_account_code = '00'
            if payment.partner_id and payment.partner_id.bank_ids:
                #if payment.partner_id.bank_ids[0].account_type == 'card':
                #    bank_account_code = '03'
                #elif payment.partner_id.bank_ids[0].account_type == 'clabe_acc':
                #    bank_account_code = '40'
                if (payment.payment_bank_account_id and payment.payment_bank_account_id.account_type == 'clabe_acc' and payment.payment_bank_account_id.l10n_mx_edi_clabe) or (payment.payment_bank_account_id and payment.payment_bank_account_id.account_type == False and payment.payment_bank_account_id.l10n_mx_edi_clabe):
                    bank_account_code = '40'
                elif payment.payment_bank_account_id and payment.payment_bank_account_id.account_type == 'card' and payment.payment_bank_account_id.acc_number:
                    bank_account_code = '03'
                else:
                    is_correct_data = False

            file_data_tmp += bank_account_code

            #             if payment.payment_bank_account_id:
            #                 if payment.payment_bank_account_id.account_type=='card':
            #                     file_data += '03'
            #                 elif payment.payment_bank_account_id.account_type=='clabe_acc':
            #                     file_data += '40'
            #                 else:
            #                     file_data += '00'
            # ======= Bank Code / Key bank ========#
            bank_code = ''
            if payment.payment_bank_id:
                if payment.payment_bank_id.l10n_mx_edi_code:
                    bank_code = payment.payment_bank_id.l10n_mx_edi_code
            file_data_tmp += bank_code.zfill(3)
            #=====reason for payment======#
            reason_payment = 'PAGO'
            if payment.payment_date:
                payment_year = str(payment.payment_date.year)[2:]
                month_name = format_datetime(payment.payment_date, 'MMMM', locale=get_lang(self.env).code)
                if payment.payment_date.month==1:
                    month_name = 'ENERO'
                elif payment.payment_date.month==2:
                    month_name = 'FEBRERO'
                elif payment.payment_date.month==3:
                    month_name = 'MARZO'
                elif payment.payment_date.month==4:
                    month_name = 'ABRIL'
                elif payment.payment_date.month==5:
                    month_name = 'MAYO'
                elif payment.payment_date.month==6:
                    month_name = 'JUNIO'
                elif payment.payment_date.month==7:
                    month_name = 'JULIO'
                elif payment.payment_date.month==8:
                    month_name = 'AGOSTO'
                elif payment.payment_date.month==9:
                    month_name = 'SEPTIEMBRE'
                elif payment.payment_date.month==10:
                    month_name = 'OCTUBRE'
                elif payment.payment_date.month==11:
                    month_name = 'NOVIEMBRE'
                elif payment.payment_date.month==12:
                    month_name = 'DICIEMBRE'
                                
                reason_payment += " "+month_name+" "+payment_year
            file_data_tmp += reason_payment.ljust(30, " ")
            #====== net_cash_reference =======#
            net_cash_reference = ''
#             if payment.net_cash_reference:
#                 net_cash_reference = payment.net_cash_reference
            if payment.name:
                payment_name_arr = payment.name.split('/')
                net_cash_reference = payment_name_arr[len(payment_name_arr)-1].strip()
            file_data_tmp += net_cash_reference.zfill(7)
            #====== net_cash_availability =======#   
            #file_data += "SPEI"
            availability = 'H'
#             if payment.net_cash_availability:
#                 if payment.net_cash_availability=='SPEI':
#                     availability = 'H'
#                 elif payment.net_cash_availability=='CECOBAN':
#                     file_data += 'M'
            file_data_tmp += availability
            file_data_tmp +="\r\n"

            if is_correct_data:
                file_data += file_data_tmp
                file_data_tmp = ''
            else:
                file_data_error += 'Beneficiary\'s Bank details are not correct; VAT: {}, Name: {}\n'.format(payment.partner_id.vat, payment.partner_id.name)
                file_data_tmp = ''

        gentextfile = base64.b64encode(bytes(file_data,'utf-8'))
        gentextfile_error = base64.b64encode(bytes(file_data_error,'utf-8'))
        self.file_data = gentextfile
        self.file_data_error = gentextfile_error
        self.file_name = TXT_FILE_FORMAT % self.sit_file_key
        self.file_name_error = 'BBVA BANCOMER NET CASH​ ​ (TSC or PSC)_ERROR.txt'

    def bbva_sit_file_format(self):
        file_data = ''
        file_name = 'BBVA BANCOMER SIT.txt'
        
        #============= Header Data =========#
        file_data += 'H'
        file_data += '000747289'
        
        #========= Current Date =========
        currect_time = datetime.today()
        file_data +=str(currect_time.year)
        file_data +="-"+str(currect_time.month).zfill(2)
        file_data +="-"+str(currect_time.day).zfill(2)
        
        file_data += '01'
        
        #===== sit_file_key ====
#         sit_file_key = ''
#         if self.payment_ids and self.payment_ids[0].sit_file_key:
#             sit_file_key = self.payment_ids[0].sit_file_key
        sit_file_key = 'TRANSFERUNAM'
        sit_file_key +=str(currect_time.year)
        sit_file_key +=str(currect_time.month).zfill(2)
        sit_file_key +=str(currect_time.day).zfill(2)
        if self.payment_ids:
            if self.payment_ids[0].currency_id.name=='MXN':
                sit_file_key += 'MN'
            else:
                sit_file_key += "ME"
            if self.payment_ids[0].sit_operation_code=='payment_on_account_bancomer':
                sit_file_key += 'BBVA'
            elif self.payment_ids[0].sit_operation_code=='payment_interbank':
                sit_file_key += "OTROS"

        file_data +=sit_file_key.ljust(30)
        #======== Response Code =======#
        file_data += '00'
        #======== Response code description =======#
        file_data += ''.ljust(20)
        #======= Channel =======#
        file_data += ''.ljust(3)
        #========= Charge account ==========#
        if self.journal_id.bank_account_id:
            file_data += self.journal_id.bank_account_id.acc_number.zfill(35)
        else:
            temp =''
            file_data +=temp.zfill(35)
        #====== Currency of the agreement =======#
        file_data += ''.ljust(3)
        #======== FILLER =========
        file_data += ''.ljust(1251)
        file_data +="\r\n"
        total_amount = 0
        #============= Payment  Details =========#              
        for payment in self.payment_ids:

            #=======Registration indicato ========            
            file_data +="D"
            #=======Instruction ========            
            file_data +="A"
            #=======Document type ========            
            file_data +="P"
            #=== Reference  =====
            folio = ''
            if payment.folio:
                folio = payment.folio
            folio_splits = folio.split('/')
            rec_data = ''
            for sp_data in folio_splits:
                rec_data+=sp_data
            file_data += rec_data.ljust(20)
            #===== Provider Password / Beneficiary Password =======#
            pass_beneficiary = ''
            if payment.partner_id and payment.partner_id.password_beneficiary:
                pass_beneficiary = payment.partner_id.password_beneficiary
            file_data += pass_beneficiary.ljust(30)
            #======= Type of Service  =======
            file_data +="PDA"
            
            #======= Operation code  ========#
            sit_operation_code = '0'
#             if payment.sit_operation_code:
#                 if payment.sit_operation_code=='payment_on_account_bancomer':
#                     sit_operation_code = '2'
#                 elif payment.sit_operation_code=='payment_interbank':
#                     sit_operation_code = '5'
            
            if payment.partner_id and payment.partner_id.bank_ids and payment.partner_id.bank_ids[0].l10n_mx_edi_clabe:
                if payment.partner_id.bank_ids[0].l10n_mx_edi_clabe.startswith('012'):
                    sit_operation_code = '2'
                else:
                    sit_operation_code = '5'
            file_data +=sit_operation_code
            #======== Charge account  ==========#
            file_data += ''.zfill(20)
            #======== FILLER ========
            file_data += ''.ljust(15)

            #=======Personalized legend ========#
            sit_reference = ''
            if payment.sit_reference:
                sit_reference = payment.sit_reference
            file_data += sit_reference[:25].ljust(25)

            #=======Reason For Payment ========#
            sit_reason_for_payment = payment.sit_reason_for_payment.translate(str.maketrans('áéíóúüñÁÉÍÓÚÜÑ', 'aeiouunAEIOUUN')) if payment.sit_reason_for_payment else ''
#             if payment.sit_operation_code and payment.sit_operation_code=='payment_interbank' and payment.sit_reason_for_payment:
#                 sit_reason_for_payment = payment.sit_reason_for_payment
            sit_reason_for_payment = ''.join(correct_data for correct_data in sit_reason_for_payment if correct_data.isalnum() or correct_data.isspace())
            if self.payment_ids:
                if self.payment_ids[0].sit_operation_code=='payment_on_account_bancomer':
                    file_data += ' '[:37].ljust(37)
                elif self.payment_ids[0].sit_operation_code=='payment_interbank':
                    file_data += sit_reason_for_payment[:37].ljust(37)
            #======== FILLER ======== 
            file_data += ''.ljust(15)

            #======== FILLER ======== 
            file_data += ''.ljust(25)

            #======== CIE Concept ========# 
            file_data += ''.ljust(37)
            #======Tax receipt =====#
            file_data += "N"
            #======== FILLER ========#
            file_data += ''.ljust(8)
            #======= Account type ======#
            if payment.payment_bank_id.name.upper() == 'BBVA BANCOMER':
                file_data += '99'.ljust(2)   
            else:
                file_data += '40'.ljust(2)
            #======= Subscription account ======#
            file_data += ''.ljust(35)
            #======= Name of 1st beneficiary ======#
            file_data += ''.ljust(40)
            #======= Identification code of the 1st beneficiary======#
            file_data += ''.ljust(1)
            #======= Identification number 1st beneficiary======#
            file_data += ''.ljust(30)
            #======= Name of 2nd beneficiary======#
            file_data += ''.ljust(40)
            #======= Identification code of the 2nd beneficiary======#
            file_data += ''.ljust(1)
            #======= Identification number 2nd beneficiary======#
            file_data += ''.ljust(30)
            #====== Currency Data =========#
            if payment.currency_id:
                if payment.currency_id.name=='MXN':
                    file_data += 'MXP'
                else:
                    file_data += payment.currency_id.name
            #====== BIC / SWIFT =========#
            if payment.currency_id:
                if payment.currency_id.name=='MXN':
                    file_data += ''.ljust(11)
                else:
                    if payment.payment_bank_account_id and payment.payment_bank_account_id.bic_swift:
                        file_data += payment.payment_bank_account_id.bic_swift.ljust(11)
                    else:
                        file_data += ''.ljust(11)                         

            #====== ABA =========#
            if payment.currency_id:
                if payment.currency_id.name=='MXN':
                    file_data += ''.ljust(9)
                else:
                    if payment.payment_bank_account_id and payment.payment_bank_account_id.aba:
                        file_data += payment.payment_bank_account_id.aba.zfill(9)
                    else:
                        file_data += ''.zfill(9)                         
        
            #======= Document key ========#
            file_data += 'FA'
            #====== Amount Data =========
            total_amount += payment.amount
            amount = round(payment.amount, 2)
            amount = "%.2f" % payment.amount
            amount = str(amount).split('.')
            file_data +=str(amount[0]).zfill(13)
            file_data +=str(amount[1])
            #========FILLER==========
            file_data +=''.zfill(15)
            #======= Confirmation Type ========#
            file_data += '01'
            #========== Email ==========#
            email = ''
            if payment.partner_id and payment.partner_id.email:
                email =  payment.partner_id.email

            if len(email) > 50:
                file_data += email[:50]
            else:
                file_data += email.ljust(50)
            #===== Periodicity interest of ======#
            file_data += 'N'
            #======= FILLER =======#
            file_data +=''.zfill(8)
            #======= FILLER =======#
            file_data +=''.zfill(4)

            #========= Payment Date =========
            if payment.payment_date:
                file_data +=str(payment.payment_date.year)
                file_data +="-"+str(payment.payment_date.month).zfill(2)
                file_data +="-"+str(payment.payment_date.day).zfill(2)
            #====== Effective date =======#
            file_data += '0001-01-01'
            #====== Grace Date =======#
            file_data += '0001-01-01'
            #====== Expiration date=======#
            file_data += '0001-01-01'
            #=========Document Date =========
            if payment.payment_date:
                file_data +=str(payment.payment_date.year)
                file_data +="-"+str(payment.payment_date.month).zfill(2)
                file_data +="-"+str(payment.payment_date.day).zfill(2)
            #====== Recurring payment indicator =========#
            file_data += 'N'
            #======= FILLER =======#
            file_data += ''.ljust(1) 
            #====== Recurring payment end date=======#
            file_data += '0001-01-01'
            #==== Additional data length or Payment detail =====#
            file_data += '700'
            #======== Additional =======#
#             additional = 'PAGO UNAM'
#             if payment.folio:
#                 split_data = payment.folio.split('/')
#                 if split_data:
#                     additional +=" - "+split_data[0]
#             if payment.sit_additional_data:
#                 additional += payment.sit_additional_data
            additional = payment.sit_additional_data.translate(str.maketrans('áéíóúüñÁÉÍÓÚÜÑ', 'aeiouunAEIOUUN')) if payment.sit_additional_data else ''
            additional = ''.join(correct_data for correct_data in additional if correct_data.isalnum() or correct_data.isspace())
            file_data += additional[:700].ljust(700)
#             if payment.dependancy_id:
#                 if payment.dependancy_id.description:
#                     additional += payment.dependancy_id.description +" "
#                 if payment.dependancy_id.dependency:
#                     additional += payment.dependancy_id.dependency + "-"
#             if payment.sub_dependancy_id and payment.sub_dependancy_id.sub_dependency:
#                 additional += payment.sub_dependancy_id.sub_dependency + " "
#             if self.env.user.lang == 'es_MX':
#                 additional += "FAVOR DE MANTENER ACTUALIZADA SU INFORMACIÓN BANCARIA"
#             else: 
#                 additional += "PLEASE KEEP YOUR BANKING INFORMATION UPDATED"
            #======= FILLER =======#
            file_data += ''.ljust(10) 
            #======= FILLER =======#
            file_data += ''.ljust(10)
            #======= Code status of the document =======# 
            file_data += ''.ljust(2)
            #======= Document status code description =======# 
            file_data += ''.ljust(30)
            #====== Date of last document event=======#
            file_data += '0001-01-01'
            file_data +="\r\n"
        
        #============== Trailer or Summary =============#
        if self.payment_ids and self.payment_ids[0].currency_id and self.payment_ids[0].currency_id.name=='MXN':  
            file_data += "T"
            caracts = 1
            total_rec = len(self.payment_ids)
            #==== Number of records high======#
            file_data += str(total_rec).zfill(10)
            caracts += 10
            #==== Import Total MXP Highs======#
            amount = "%.2f" % total_amount
            amount = str(amount).split('.')
            file_data +=str(amount[0]).zfill(13)
            caracts += 13
            file_data +=str(amount[1])
            caracts += len(amount[1])
            #==== Number of records MXP divestitures======#
            #file_data += str(total_rec).zfill(10)
            #==== Total amount Retirements MXP======#
            #file_data +=str(amount[0]).zfill(13)
            #file_data +=str(amount[1])
            #===== Filler ===#
            falt = 251 - caracts
            file_data += ''.zfill(falt)
            # 1115 characters left, 1115 - 65 = 1050
            file_data += ''.zfill(1050)
            file_data += ''.ljust(65)
            file_data +="\r\n"
        else:
            file_data += "T"

            total_rec = len(self.payment_ids)
            #=====FILLER=====#
            file_data += ''.zfill(250)
            #============Number of records==============#
            file_data += str(total_rec).zfill(10)
            #============== Total Amount ============#
            amount = "%.2f" % total_amount
            amount = str(amount).split('.')
            file_data +=str(amount[0]).zfill(13)
            file_data +=str(amount[1])
            #============Number of records==============#
            file_data += str(total_rec).zfill(10)
            #============== Total Amount ============#
            amount = "%.2f" % total_amount
            amount = str(amount).split('.')
            file_data +=str(amount[0]).zfill(13)
            file_data +=str(amount[1])
            #=====FILLER=====#
            file_data += ''.zfill(100)
            
            file_data +="\r\n"
        gentextfile = base64.b64encode(bytes(file_data,'utf-8'))
        self.file_data = gentextfile
        self.file_name = TXT_FILE_FORMAT % self.sit_file_key
# ------------------      
    def santander_file_format(self):
        file_name = 'SANTANDER.txt'
        file_data = ''
        for payment in self.payment_ids:
            #========= Charge account ==========#
            if self.journal_id.bank_account_id:
                if len(self.journal_id.bank_account_id.acc_number) > 11:
                    file_data += self.journal_id.bank_account_id.acc_number[:11]
                else:
                    file_data += self.journal_id.bank_account_id.acc_number.zfill(11)
            else:
                temp =''
                file_data +=temp.zfill(11)
            #====== Spaces ====#
            file_data += ''.ljust(5)
            #===== Bank Account Payment Receiving ========#
            if payment.payment_bank_account_id:
                if len(payment.payment_bank_account_id.acc_number) > 11:
                    file_data += payment.payment_bank_account_id.acc_number[:11]
                else:
                    file_data += payment.payment_bank_account_id.acc_number.zfill(11)
            else:
                temp =''
                file_data +=temp.zfill(11)
            #====== Spaces ====#
            file_data += ''.ljust(5)
            #====== Amount Data =========#
            amount = round(payment.amount, 2)
            amount = "%.2f" % payment.amount
            amount = str(amount).split('.')
            file_data +=str(amount[0]).zfill(10)
            file_data +='.'
            file_data +=str(amount[1])
            #======= Payment concept ==========#
            santander_payment_concept = ''
#             if payment.santander_payment_concept:
#                 if len(payment.santander_payment_concept) > 40:
#                     santander_payment_concept = payment.santander_payment_concept[:40]
#                 else:
#                     santander_payment_concept = payment.santander_payment_concept
            if payment.folio:
                santander_payment_concept += payment.folio
            if payment.name:
                santander_payment_concept += " "+payment.name
                
            file_data += santander_payment_concept.ljust(40)
            
            #==== Payment Date ======
            if payment.payment_date:
                file_data +=str(payment.payment_date.day).zfill(2)
                file_data +=str(payment.payment_date.month).zfill(2)
                file_data +=str(payment.payment_date.year)
                file_data += '     '
            else:
                file_data += '           '
            file_data +="\r\n"
        gentextfile = base64.b64encode(bytes(file_data,'utf-8'))
        self.file_data = gentextfile
        self.file_name = TXT_FILE_FORMAT % self.sit_file_key
            
    def hsbc_file_format(self):
        ascii_non_accepted = '!@#$%&/()\'*+;:[]=^`{|}~,".?^<\'".-'
        file_name = 'HSBC.csv'
        record_count = 0
        file_data = ''
        for payment in self.payment_ids:
            #==========Sequence ========#
            record_count+=1
            file_data+=str(record_count)

            #==========Bank Account ========#
            if self.journal_id.bank_account_id:
                file_data +=","+self.journal_id.bank_account_id.acc_number[-10:].zfill(10)
            else:
                temp =''
                file_data +=","+temp.zfill(10)
            #===== Bank Account Payment Receiving ========#
            if payment.payment_bank_account_id:
                file_data += ","+payment.payment_bank_account_id.acc_number[-10:].zfill(10)
            else:
                temp =''
                file_data +=","+temp.zfill(10)            
            #====== Amount Data =========#
            amount = round(payment.amount, 2)
            amount = "%.2f" % payment.amount
            amount = str(amount).split('.')
            #print ('Amount=====',x)
            file_data +=","+str(amount[0])
            file_data +='.'
            file_data +=str(amount[1])
            
            #====== Currency Data =========#
            if payment.currency_id:
                if payment.currency_id.name=='MXN':
                    file_data += ','+'1'
                elif payment.currency_id.name=='USD':
                    file_data += ','+'2'
                else:
                    file_data += ','+''
            #===== hsbc_reference ====#
            hsbc_reference = ''
#             if payment.hsbc_reference:
#                 hsbc_reference = payment.hsbc_reference
            if payment.name:
                hsbc_reference = payment.name.replace('.', '').replace('/', '')
            file_data +="," + hsbc_reference
            #======= Partner Name ========#
            ascii_partner_name = ''
            if payment.partner_id:
                ascii_partner_name = regex_str.sub(r'[^\x00-\x7F]+', '', payment.partner_id.name.translate(str.maketrans('áéíóúüñÁÉÍÓÚÜÑ', 'aeiouunAEIOUUN')))
                for special_character in ascii_non_accepted:
                    ascii_partner_name = ascii_partner_name.replace(special_character, '')
            file_data +="," + (' '.join(ascii_partner_name.split()))[:40]
            #==== Fiscal Proof  ========#
            file_data +="," + '0'
            #==== RFC Of the beneficiary  ========#
            file_data +=",                  " # 18 Space
            #====VAT  ========#
            file_data +=",             "    # 13 Space 
            
            file_data +="\r\n"
        
        file_data += "1,"+ str(record_count) + ","+'Transfer terceros'
        file_data +="\r\n"
        gentextfile = base64.b64encode(bytes(file_data,'utf-8'))
        self.file_data = gentextfile
        self.file_name = TXT_FILE_FORMAT % self.sit_file_key

    def jp_morgan_wires_file_format(self):   

        file_name = 'jp_morgan_wires.csv'
        record_count = 0
        total_amount = 0
        file_data = 'HEADER'+","
        #====== Current Dat time=========
        currect_time = datetime.today()
        file_data +=str(currect_time.year)
        file_data +=str(currect_time.month).zfill(2)
        file_data +=str(currect_time.day).zfill(2)
        file_data +=str(currect_time.hour).zfill(2)
        file_data +=str(currect_time.minute).zfill(2)
        file_data +=str(currect_time.second).zfill(2)
        
        file_data += ","+"1"
        file_data +="\r\n"                
        for payment in self.payment_ids:
            record_count += 1
            file_data += "P"
            file_data += ','
            #======== Method ========#
            
#             if payment.jp_method:
#                 if payment.jp_method == 'WIRES':
#                     file_data += 'WIRES'
#                     file_data += ','
#                 if payment.jp_method == 'BOOKTX':
#                     file_data += 'BOOKTX'
#                     file_data += ','
#             else:
#                 file_data += ','
            
            file_data += 'WIRES'
            #Obtener BIC del Banco JP Morgan
            jp_morgan_bic = self.env['res.bank'].search([('name', '=', 'JP MORGAN')])
            file_data += ','+jp_morgan_bic.bic
            # ======== BIC /SWIFT ======
            #if payment.journal_id and payment.journal_id.bank_id and payment.journal_id.bank_id.country:
            #    if payment.journal_id.bank_id.country.code and payment.journal_id.bank_id.country.code=='MX':
            #        file_data += payment.journal_id.bank_id.bic or ''
            #    else:
            #        file_data += payment.journal_id.bank_id.bic or ''
            file_data += ','

            #==========Bank Account ========#
            if payment.journal_id.bank_account_id and payment.journal_id.bank_account_id.acc_number:
                file_data +=payment.journal_id.bank_account_id.acc_number
            file_data += ','
            # ===== Bank to bank transfer ========#
#             if payment.jp_bank_transfer:
#                 if payment.jp_bank_transfer == 'non_financial_institution':
#                     file_data += 'N'
#                 elif payment.jp_bank_transfer == 'financial_institution':
#                     file_data += 'Y'
            file_data += "N"
            file_data += ','

            #====== Currency Data =========#
            if payment.currency_id:
                file_data += payment.currency_id.name
            file_data += ','

            #====== Amount Data =========#
            total_amount += payment.amount
            amount = round(payment.amount, 2)
            amount = "%.2f" % payment.amount
            amount = str(amount).split('.')
            #print ('Amount=====',x)
            file_data +=str(amount[0])
            file_data +='.'
            file_data +=str(amount[1])
            file_data += ','
            #===== Equivalent Amount======#
            file_data += ' ,'
                   
            #===== N / A======#
            file_data += ' , , , ,'
            #======== Payment Date =========
            if payment.payment_date:
                file_data +=str(payment.payment_date.year)
                file_data +=str(payment.payment_date.month).zfill(2)
                file_data +=str(payment.payment_date.day).zfill(2)
            file_data += ','
            
            # ====== ID Type ======#
            #if payment.jp_id_type:
            #    if payment.jp_id_type == 'clabe':
            #        file_data += 'CLBE_CODE'
            #    elif payment.jp_id_type == 'debit_card':
            #        file_data += 'DR_CARD'
            #    elif payment.jp_id_type == 'vostro':
            #        file_data += 'VOSTRO_CD'
            if payment.payment_bank_account_id.acc_number:
                file_data += 'ACCT,'
            else :
                file_data += 'IBAN,'

            # ======== CLABE ======
            #if payment.partner_id and payment.partner_id.bank_ids:
            #    bank_clabe = ''
            #    if payment.partner_id.bank_ids[0].l10n_mx_edi_clabe:
            #        bank_clabe = payment.partner_id.bank_ids[0].l10n_mx_edi_clabe
            #    file_data += bank_clabe
            if payment.payment_bank_account_id.acc_number:
                file_data += payment.payment_bank_account_id.acc_number.zfill(18)
                file_data += ','
            else : 
                file_data += ','              
            
            #======= Partner Name ========#
            partner_name = ''
            if payment.partner_id:
                partner_name = payment.partner_id.name
            file_data +=partner_name   
            file_data += ','            

            #======= Partner Address1 ========#
            address_1 = ''
            if payment.partner_id and payment.partner_id.street:
                address_1 = payment.partner_id.street
            file_data +=address_1   
            file_data += ','            
            

            #======= Partner Address2 ========#
            address_2 = ''
            if payment.partner_id and payment.partner_id.street2:
                address_2 = payment.partner_id.street2
            file_data +=address_2   
            file_data += ','            

            #======= Partner Address3====#
            address_3 = ''
            if payment.partner_id:
                if payment.partner_id.city:
                    address_3 = payment.partner_id.city
                if payment.partner_id.zip:
                    address_3 += " "+payment.partner_id.zip
            file_data +=address_3   
            file_data += ','            
            # ==== Blank =======
            file_data += ' ,'

            # ==== Country Name =======
            country_code = ''
            if payment.partner_id and payment.partner_id.country_id:
                country_code = payment.partner_id.country_id.code or ''
            file_data +=country_code   
            
            file_data += ','
            #====== Blank Value ======
            file_data += ' , ,'
            #======== ID Type Beneficiary Bank ========#
            file_data += "USABA," 
            #===== ID Value 25 no =======
            #if payment.payment_bank_id:
            if payment.payment_bank_account_id:
                file_data += payment.payment_bank_account_id.aba if payment.payment_bank_account_id.aba else ''
            file_data += ','
            #==== Bank Name=========#
            if payment.payment_bank_id:
                bank_name = ''
                if payment.payment_bank_id.name:
                    bank_name = payment.payment_bank_id.name
                file_data += bank_name            
            file_data += ','
            
            #======= Bank Address1 ========#
            address_1 = ''
            if payment.payment_bank_id:
                address_1 = payment.payment_bank_id.street or ''
            file_data +=address_1   
            file_data += ','            
            

            #======= Bank Address2 ========#
            address_2 = ''
            if payment.payment_bank_id:
                address_2 = payment.payment_bank_id.street2 or ''
            file_data +=address_2   
            file_data += ','            

            #======= Bank Address3====#
            address_3 = ''
            if payment.payment_bank_id and payment.payment_bank_id.state:
                address_3 = payment.payment_bank_id.state.name
                address_3 += " "+payment.payment_bank_id.zip or ''
            file_data +=address_3   
            file_data += ','            

            # ==== Country Name =======
            country_code = ''
            if payment.payment_bank_account_id:
                country_code = payment.payment_bank_account_id.bic_swift[4:6] if payment.payment_bank_account_id.bic_swift else ''
            file_data +=country_code   
            file_data += ','
            #=====Supplementary ID======#
            file_data += ' ,'
            #=====Supplementary ID Value======#
            file_data += ' ,'            
            #=====N\A======#
            file_data += ' , , , , , , ,'            
            #=====ID Type======#
            file_data += ' ,'            
            #=====ID Voucher======#
            file_data += ' ,'            
            #=====Bank Name======#
            file_data += ' ,'            
            #=====Address 1======#
            file_data += ' ,'            
            #=====Address 2======#
            file_data += ' ,'
            #=====Address 3======#
            file_data += ' ,'            
            #=====Country======#
            file_data += ' ,'            
            #=====N/A======#
            file_data += ' , ,'            
            #=====ID Type======#
            file_data += ' , ,'            
            #=====By Order Name======#
            file_data += ' ,'            
            #=====Address 1======#
            file_data += ' ,'            
            #=====Address 2======#
            file_data += ' ,'
            #=====Address 3======#
            file_data += ' ,'            
            #=====Country======#
            file_data += ' ,'            
            #=====N/A======#
            file_data += ' , , , , , , , , , , , , , , , , , ,'
            #======== Reference Sent with Payment ======#
            file_data += ' ,'
            #======== Interanal Reference  ======#
            file_data += ' ,'
            #======== N/A  ======#
            file_data += ' ,'
            #======== Detail 1  ======#
#             if payment.jp_payment_concept:
#                 file_data += payment.jp_payment_concept 
            if payment.folio:
                file_data += payment.folio 
            file_data += ','
            #======== Detail 2  ======#
            file_data += ' ,'
            #======== Detail 3  ======#
            file_data += ' ,'
            #======== Detail 4  ======#
            file_data += ' ,'
            #======== N/A  ======#
            file_data += ' , , , , , , , ,'
            #======== Code  ======#
            file_data += ' ,'
            #======== Country  ======#
            file_data += ' ,'
            #======== Instruction 1  ======#
            file_data += ' ,'
            #======== Instruction 2  ======#
            file_data += ' ,'
            #======== Instruction 3  ======#
            file_data += ' ,'
            #======== Instruction Code1  ======#
            file_data += ' ,'
            #======== Instruction  Text 1 ======#
            file_data += ' ,'
            #======== Instruction Code 2 ======#
            file_data += ' ,'
            #======== Instruction  Text 2 ======#
            file_data += ' ,'
            #======== Instruction Code 3  ======#
            file_data += ' ,'
            #======== Instruction  Text 3 ======#
            file_data += ' ,'

            #======== Sender to Receiver Code 1  ======#
            file_data += ' ,'
            #======== Sender to Receiver Line 1======#
            file_data += ' ,'
            #======== Sender to Receiver Code 2  ======#
            file_data += ' ,'
            #======== Sender to Receiver Line 2 ======#
            file_data += ' ,'
            #======== Sender to Receiver Code 3  ======#
            file_data += ' ,'
            #======== Sender to Receiver Line 3 ======#
            file_data += ' ,'
            #======== Sender to Receiver Code 4  ======#
            file_data += ' ,'
            #======== Sender to Receiver Line 4 ======#
            file_data += ' ,'
            #======== Sender to Receiver Code 5  ======#
            file_data += ' ,'
            #======== Sender to Receiver Line 5 ======#
            file_data += ' ,'
            #======== Sender to Receiver Code 6  ======#
            file_data += ' ,'
            #======== Sender to Receiver Line 6 ======#
            file_data += ' ,'
            #======== Priority ======#
            file_data += ' ,'
            #======== N/A ======#
            file_data += ' ,'
            #======== Charges ======#
            if payment.jp_charges:
                if payment.jp_charges == 'shared':
                    file_data += 'SHA'
                elif payment.jp_charges == 'beneficiary':
                    file_data += 'BEN'
                elif payment.jp_charges == 'remitter':
                    file_data += 'OUR'
            file_data += ','
            #======== N/A ======#
            file_data += ' ,'

            #======== Aditional Details ======#
            file_data += ' ,'
            #======== Note ======#
            file_data += ' ,'
            #======== Beneficiary Email ======#
            file_data += ' ,'
            #======== Payroll Indicatorl ======#
            file_data += ' ,'
            #======== Confidential Indicator ======#
            file_data += ' ,'
            #====== Group Name==========#
            file_data += ' ,'
            
            file_data +="\r\n"    
        file_data += "TRAILER" +","+str(record_count)+","
        amount = "%.2f" % total_amount
        amount = str(amount).split('.')
        file_data +=str(amount[0])
        file_data +='.'
        file_data +=str(amount[1])
        file_data +="\r\n"                               
        gentextfile = base64.b64encode(bytes(file_data,'utf-8'))
        self.file_data = gentextfile
        self.file_name = TXT_FILE_FORMAT % self.sit_file_key
        
    def jp_morgan_us_drawdowns_file_format(self):
        file_name = 'jp_morgan_us_drawdowns.csv'
        record_count = 0
        total_amount = 0
        file_data = ''

        file_data = 'HEADER'+","
        #====== Current Dat time=========
        currect_time = datetime.today()
        file_data +=str(currect_time.year)
        file_data +=str(currect_time.month).zfill(2)
        file_data +=str(currect_time.day).zfill(2)
        file_data +=str(currect_time.hour).zfill(2)
        file_data +=str(currect_time.minute).zfill(2)
        file_data +=str(currect_time.second).zfill(2)
        
        file_data += ","+"1"
        file_data +="\r\n"                
        
        for payment in self.payment_ids:
            record_count += 1
            #======= Input Type =======
            file_data += "P"
            file_data += ','
            #========= Receipt Method ======
            file_data += "DRWDWN"
            file_data += ','
            # ======== BIC /SWIFT ======
            if payment.journal_id and payment.journal_id.bank_id:
                bank_code = ''
                if payment.journal_id.bank_id.bic:
                    bank_code = payment.journal_id.bank_id.bic
                file_data += bank_code            
            file_data += ','
            #==========Bank Account ========#
            if payment.journal_id.bank_account_id and payment.journal_id.bank_account_id.acc_number:
                file_data +=payment.journal_id.bank_account_id.acc_number
            file_data += ','
            #======= N/A ==========#
            file_data += ' ,'
            #======== Currency =========#
            file_data += "USD"
            file_data += ','
            #====== Amount Data =========#
            total_amount += payment.amount
            amount = round(payment.amount, 2)
            amount = "%.2f" % payment.amount
            amount = str(amount).split('.')
            file_data +=str(amount[0])
            file_data +='.'
            file_data +=str(amount[1])
            file_data += ','
            #======= N/A ==========#
            file_data += ' ,'

            # ===== Drawdown Type ========#
            if payment.jp_drawdown_type:
                if payment.jp_drawdown_type == 'WIRE':
                    file_data += 'WIRE'
                elif payment.jp_drawdown_type == 'BOOK':
                    file_data += 'BOOK'
            file_data += ','
            #======= N/A ==========#
            file_data += ' , , ,'
            #======== Payment Date =========
            if payment.payment_date:
                file_data +=str(payment.payment_date.year)
                file_data +=str(payment.payment_date.month).zfill(2)
                file_data +=str(payment.payment_date.day).zfill(2)
            file_data += ','
            #======= N/A ========== 14 to 60#
            file_data += ' , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , ,'
            #==========Bank Account ========#
            if payment.payment_bank_account_id:
                file_data +=payment.payment_bank_account_id.acc_number
            file_data += ','
            #======= Partner Name ========#
            partner_name = ''
            if payment.partner_id:
                partner_name = payment.partner_id.name
            file_data +=partner_name
            file_data += ','  
            #====== Address 1 ======# 
            file_data += ' ,'
            #======== N/A =======#
            file_data += ' ,'
            #====== City ======#
            file_data += ' ,'
            #===== Country ======#
            file_data += 'MX'
            file_data += ','
            #===== ID Type =======#
            #if payment.jp_drawdown_type and payment.jp_drawdown_type=='Drawdown':
            if payment.payment_bank_id:
                bank_code = ''
                if payment.payment_bank_id.bic:
                    bank_code = payment.payment_bank_id.bic
                file_data += bank_code            
                file_data += ','
            else:
                file_data += ' ,'
#             else:
#                 file_data += ' ,'
            #======== Journal Account =======#
            if payment.jp_drawdown_type and payment.jp_drawdown_type=='Drawdown':
                if payment.journal_id.bank_account_id:
                    file_data +=payment.journal_id.bank_account_id.acc_number
                file_data += ','
            else:
                file_data += ' ,'

            #======== Bank of receipt of payment =======#
            #if payment.jp_drawdown_type and payment.jp_drawdown_type=='Drawdown':
            if payment.payment_bank_id:
                file_data += payment.payment_bank_id.name
            file_data += ','
#             else:
#                 file_data += ' ,'

            #======== Bank Address 1 =======#
            if payment.jp_drawdown_type and payment.jp_drawdown_type=='Drawdown':
                if payment.payment_bank_id and payment.payment_bank_id.street:
                    file_data += payment.payment_bank_id.street
                file_data += ','
            else:
                file_data += ' ,'
            #===== N/A=======#
            file_data += ' ,'
            #===== City=======#
            file_data += ' ,'
            #=====Country=======#
            file_data += ' ,'
            #===== N/A=======#
            file_data += ' ,'
            #===== Internal Reference=======#
            file_data += ' ,'
            #===== N/A=======#
            file_data += ' ,'
            #===== Details1=======#
            file_data += ' ,'
            #===== Details2=======#
            file_data += ' ,'
            #===== Details3=======#
            file_data += ' ,'
            #===== Details4=======#
            file_data += ' ,'
            #===== N/A======= 81 to 116#
            file_data += ' , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , ,'
            #===== Note=======#
            file_data += ' ,'
            #===== N/A=======#
            file_data += ' ,'
            file_data += '\r\n'
        
        #===============TRAILER=================#
        file_data += 'TRAILER'
        file_data += ' ,'
        #------Total Number of Records --------#
        total_rec = len(self.payment_ids)
        file_data +=str(total_rec)
        file_data += ' ,'
        #------Total amount --------#
        amount = "%.2f" % total_amount
        amount = str(amount).split('.')
        file_data +=str(amount[0])
        file_data +='.'
        file_data +=str(amount[1])
        file_data += ','
        file_data += '\r\n'
                 
        gentextfile = base64.b64encode(bytes(file_data,'utf-8'))
        self.file_data = gentextfile
        self.file_name = TXT_FILE_FORMAT % self.sit_file_key
        
    def jp_morgan_advice_to_receive_file_format(self):
        file_name = 'jp_morgan_advice_to_receive.csv'
        record_count = 0
        total_amount = 0
        file_data = ''

        file_data = 'HEADER'+","
        #====== Current Dat time=========
        currect_time = datetime.today()
        file_data +=str(currect_time.year)
        file_data +=str(currect_time.month).zfill(2)
        file_data +=str(currect_time.day).zfill(2)
        file_data +=str(currect_time.hour).zfill(2)
        file_data +=str(currect_time.minute).zfill(2)
        file_data +=str(currect_time.second).zfill(2)
        
        file_data += ","+"1"
        file_data +="\r\n"                
        
        for payment in self.payment_ids:
            record_count += 1
            #========Input Type=======#
            file_data += 'P'
            file_data += ','
            #========Receipt Method=======#
            file_data += 'ATR'
            file_data += ','
            # ======== BIC /SWIFT ======
            if payment.journal_id and payment.journal_id.bank_id:
                bank_code = ''
                if payment.journal_id.bank_id.bic:
                    bank_code = payment.journal_id.bank_id.bic
                file_data += bank_code            
            file_data += ','

            #==========Bank Account ========#
            if payment.journal_id.bank_account_id and payment.journal_id.bank_account_id.acc_number:
                file_data +=payment.journal_id.bank_account_id.acc_number
            file_data += ','
            #======= N/A=========#
            file_data += ' ,'
            #====== Currency Data =========
            if payment.currency_id:
                file_data += payment.currency_id.name
                file_data += ','
            else:
                file_data += ' ,'
            #====== Amount Data =========#
            total_amount += payment.amount
            amount = "%.2f" % payment.amount
            amount = str(amount).split('.')
            file_data +=str(amount[0])
            file_data +='.'
            file_data +=str(amount[1])
            file_data += ','
            #====== N/A ========= 8 to 12#
            file_data += ' , , , , ,'
            #======== Payment Date =========
            if payment.payment_date:
                file_data +=str(payment.payment_date.year)
                file_data +=str(payment.payment_date.month).zfill(2)
                file_data +=str(payment.payment_date.day).zfill(2)
            file_data += ','
            #======= N/A========= 14 to 39#
            file_data += ' , , , , , , , , , , , , , , , , , , , , , , , , , ,'
            #===== ID Type =======#
#             if payment.jp_drawdown_type and payment.jp_drawdown_type=='Drawdown':
#                 if payment.journal_id and payment.journal_id.bank_id:
#                     bank_code = ''
#                     if payment.journal_id.bank_id.bic:
#                         bank_code = payment.journal_id.bank_id.bic
#                     file_data += bank_code            
#                 file_data += ','
#             else:
#                 file_data += ' ,'
#            if payment.journal_id and payment.journal_id.bank_id and payment.journal_id.bank_id.country:
#                if payment.journal_id.bank_id.country.code and payment.journal_id.bank_id.country.code=='MX':
#                    file_data += payment.journal_id.bank_id.bic or ''
#                else:
#                   file_data += payment.payment_bank_id.bic or ''
#            file_data += ',' 
            if payment.payment_bank_id:
                bank_code = ''
                if payment.payment_bank_id.bic:
                    bank_code = payment.payment_bank_id.bic
                file_data += bank_code            
                file_data += ','
            else:
                file_data += ' ,'    
            #====== ID Value =======#
            file_data += ' ,'
            #======== Bank of receipt of payment =======#
            if payment.payment_bank_id:
                file_data += payment.payment_bank_id.name
                file_data += ','
            else:
                file_data += ' ,'
            #===== Address 1 ========#
            file_data += ' ,'
            #===== Address 2 ========#
            file_data += ' ,'
            #===== Address 3 ========#
            file_data += ' ,'
            # ==== Country Name =======
            country_code = ''
            if payment.payment_bank_id and payment.payment_bank_id.country:
                country_code = payment.payment_bank_id.country.code or ''
            file_data +=country_code   
            file_data += ','
            #========== N/A========= 47 to 55 ====#
            file_data += ' , , , , , , , , ,'
            #========= Name========3
            file_data += 'UNIVERSIDAD NACIONAL AUTÓNOMA DE MÉXICO'
            file_data += ','
            #====== Address 1 =======#
            file_data += ' ,'
            #====== Address 2 =======#
            file_data += ' ,'
            #====== City =======#
            file_data += ' ,'
            #====== Country =======#
            file_data += ' ,'
            #====== N/A ======= 61 to 73#
            file_data += ' , , , , , , , , , , , , ,'
            #====== N/A =======#
            file_data += ' ,'
            #======Internal Reference =======#
            file_data += ' ,'
            #======N/A ======= 76 to 116#
            file_data += ' , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , ,'
            #======= Note ========#
            file_data += ' ,'
            #======== N/A =======#
            file_data += ' ,'
            file_data += '\r\n'   

        #===============TRAILER=================#
        file_data += 'TRAILER'
        file_data += ' ,'
        #------Total Number of Records --------#
        total_rec = len(self.payment_ids)
        file_data +=str(total_rec)
        file_data += ' ,'
        #------Total amount --------#
        amount = "%.2f" % total_amount
        amount = str(amount).split('.')
        file_data +=str(amount[0])
        file_data +='.'
        file_data +=str(amount[1])
        file_data += ','
        file_data += '\r\n'
            
        gentextfile = base64.b64encode(bytes(file_data,'utf-8'))
        self.file_data = gentextfile
        self.file_name = TXT_FILE_FORMAT % self.sit_file_key

    def _get_complete_address(self,address):
        info=""
        info+=''+ (address.street+' ' if address.street else '')
        info+=''+ (address.street2+' ' if address.street2 else '')    
        info+=''+ (address.city+' ' if address.city else '')
        info+=''+ (address.state.name+' ' if address.state.name else '')
        info+=''+ (address.zip+' ' if address.zip else '')
        info+=''+ (address.country.name+' ' if address.country.name else '')
        info+=''+ (address.l10n_mx_edi_vat+' ' if address.l10n_mx_edi_vat else '')
        return info.translate(str.maketrans('áéíóúäëïöüñÁÉÍÓÚÄËÏÖÜÑ', 'aeiouaeiou&AEIOUAEIOUN'))
        
    def _get_date(self):
        info = 'BECA '
        mes_txt1 =''
        mes_txt2 =''
        mes = ['ENE','FEB','MAR','ABR','MAY','JUN','JUL','AGO','SEP','OCT','NOV','DIC']
        
        ini_dia=self.start_date.strftime('%d')
        fin_dia=self.end_date.strftime('%d')

        ini_mes=int(self.start_date.month)
        fin_mes=int(self.end_date.month)

        mes_txt1=mes[ini_mes-1]
        if ini_mes==fin_mes:
            info += str(ini_dia)+' AL '+str(fin_dia)+' '+mes_txt1
        else:
            mes_txt2=mes[fin_mes-1]
            info += str(ini_dia)+' '+mes_txt1+' AL '+str(fin_dia)+' '+mes_txt2
        return info
    
    def _get_iban_account(self,pay_req_id):
        benef=pay_req_id.partner_id.bank_ids
        if len(benef)!=0:
            return str(benef[0].iban_account if benef[0].iban_account else '')
        return ''
    
    def citi_banamex_file_format(self):
        file_name = 'CITI_Banamex.txt'
        file_data = ''
        if self.payment_ids and self.payment_ids[0].payment_type == 'outbound':
            for payment in self.payment_ids:
                # Payment Method
                file_data += 'TRN'
                # Company Name
                file_data += 'UNIVERSIDAD NACIONAL AUTONOMA DE ME'

                pay_req_id = payment.payment_request_id
                # Debit Account
                bank_acc_id = pay_req_id.payment_issuing_bank_acc_id
                file_data += str(bank_acc_id.acc_number).ljust(34)

                # Payment Currency
                file_data += str(pay_req_id.currency_id.name).ljust(3)
                # Payment Amount
                total=str(pay_req_id.amount_total)+'0'
                file_data += str(total).rjust(14)
                # Equivalent Amount
                file_data += '0.00'.rjust(14)
                # Value Date
                file_data += ''.ljust(8)
                
                
                # Beneficiary name
                file_data += str(payment.partner_id.name[:35]).ljust(35)
                # Beneficiary Address 1
                file_data += ''.ljust(35)
                # Beneficiary Address 2
                file_data += ''.ljust(35)
                # Beneficiary Address 3
                file_data += ''.ljust(35)

                
                bank = pay_req_id.payment_bank_account_id
                if bank:
                    # Beneficiary Account Number
                    file_data += str(bank.acc_number).ljust(34)
                    # Beneficiary Bank Name
                    file_data += str(bank.bank_id.name).ljust(35)
                    
                    # Beneficiary Bank Address 1,2,3
                    file_data += str(self._get_complete_address(bank.bank_id)).ljust(105)
                    file_data += 'IS'
                    file_data += str(bank.bic_swift).ljust(11)
                else:
                    file_data+=''.ljust(34)
                    file_data+=''.ljust(35)
                    file_data+=''.ljust(105)
                    # Beneficiary Bank Routing Method
                    file_data += 'IS'
                    # Beneficiary Bank Routing ID
                    file_data += ''.ljust(11)
               
                if self.inter_bank_id:
                    # Intermediary Bank Name
                    file_data += str(self.inter_bank_id.name).ljust(35)
                    # Intermediary Bank Address 1,2,3
                    file_data += str(self._get_complete_address(self.inter_bank_id)[:105]).ljust(105)
                else:
                    # Intermediary Bank Name
                    file_data += ''.ljust(35)
                    # Intermediary Bank Address 1,2,3
                    file_data += ''.ljust(105)
                
                # Intermediary Bank Routing Method
                file_data += ''.ljust(2)
                # Intermediary Bank Routing ID
                file_data += ''.ljust(11)

                # Ordering Party Name
                
                file_data += str(self._get_date()).ljust(35)

                # Ordering Party Address 1
                dep = str(payment.dependancy_id.dependency)
                sd = str(payment.sub_dependancy_id.sub_dependency)
                folio = str(payment.folio.replace('/',''))
                folio_total =str('UNAM '+dep+sd+' '+folio)
                file_data += folio_total.ljust(35) 
                

                # Ordering Party Address 2
                file_data += ''.ljust(35)
                # Ordering Party Address 3
                file_data += ''.ljust(35)
                # Payment Details 1-4 (4x35)
                iban = 'IBAN '+self._get_iban_account(pay_req_id)
                file_data += iban.ljust(140)
                # Bank Details (6x35)
                file_data += ''.ljust(210)
                file_data +="\r\n" 

                
        gentextfile = base64.b64encode(bytes(file_data,'utf-8'))
        self.file_data = gentextfile
        self.file_name = file_name
     
    def check_payments(self,payments):
        
        for payment in payments:
            if self.sit_file_key:
                payment.sit_file_key = self.sit_file_key 
            if payment.journal_id.id != self.journal_id.id:
                raise UserError(_("The selected layout does NOT match the bank of the selected payments"))
           
    #Función para generar el Layout bancario     
    def generate_bank_layout(self):
        self.check_payments(self.payment_ids)
        if self.journal_id.bank_format == 'banamex':
            self.banamex_file_format()
        elif self.journal_id.bank_format == 'bbva_tnn_ptc':
            self.bbva_tnn_ptc_file_format()
        elif self.journal_id.bank_format == 'bbva_tsc_pcs':
            self.bbva_tsc_pcs_file_format()
        elif self.journal_id.bank_format == 'bbva_sit':
            self.bbva_sit_file_format()
        elif self.journal_id.bank_format == 'santander':
            self.santander_file_format()
        elif self.journal_id.bank_format == 'hsbc':
            self.hsbc_file_format()
        elif self.journal_id.bank_format == 'jpmw':
            self.jp_morgan_wires_file_format()
        elif self.journal_id.bank_format == 'jpma':
            self.jp_morgan_advice_to_receive_file_format()
        elif self.journal_id.bank_format == 'jpmu':
            self.jp_morgan_us_drawdowns_file_format()
        elif self.journal_id.bank_format == 'citibank':
            self.citi_banamex_file_format()
        else:
            self.file_data = False
            self.file_name = False


        return {
            'name': _(TXT_GEN_BANK_LAYOUT),
            'res_model': GEN_BANK_LAYOUT,
            'res_id' : self.id,
            'view_mode': 'form',
            'target': 'new',
            'view_id': self.env.ref('jt_supplier_payment.view_generate_bank_layout').id,
            'type': IR_ACT_WINDOW,
        }    
    #====== Payroll Payment Format ========#
# -------------
    def payroll_payment_santander_file_format(self):
        file_data = ''
        file_data_error = ''
        employee_objs = self.env['hr.employee']
        # ===== Type Of Registation ======#
        file_data += '1'
        # ===== Sequence Number =======#
        file_data += '00001'
        # ====== Sense =========#
        file_data += 'E'
        # ====== Generation Date =========#
        currect_time = datetime.today()
        file_data += str(currect_time.month).zfill(2)
        file_data += str(currect_time.day).zfill(2)
        file_data += str(currect_time.year)
        # ======= Account ========= #
        if self.journal_id and self.journal_id.bank_account_id:
            file_data += self.journal_id and self.journal_id.bank_account_id.acc_number.ljust(16)
        else:
            file_data += "".ljust(16)
        # ===== Application Date ======= #
        currect_time = datetime.today()
        if self.payment_ids and self.payment_ids[0].payment_date:
            currect_time = self.payment_ids[0].payment_date

        file_data += str(currect_time.month).zfill(2)
        file_data += str(currect_time.day).zfill(2)
        file_data += str(currect_time.year)
        file_data += "\r\n"
        next_no = 2
        total_record = len(self.payment_ids)
        total_amount = 0
        for payment in self.payment_ids:
            file_data_aux = ''
            error_message = ''
            correct_data = True
            # ===== Type Of Records ======#
            file_data_aux += "2"
            # ======= Sequence Number ======#
            file_data_aux += str(next_no).zfill(5)
            # ===== Employee Number =======#
            if payment.payroll_request_type == 'alimony':
                if payment.partner_id.worker_number:
                    file_data_aux += str(payment.partner_id.worker_number)[:7]
                else:
                    file_data_aux += ''.zfill(7)
            elif payment.payroll_request_type == 'payroll_payment' or payment.payroll_request_type == 'special_payroll_payment':
                emp_id = employee_objs.search([('emp_partner_id','=',payment.partner_id.id)],limit=1)
                if emp_id:
                    file_data_aux += str(emp_id.worker_number)[:7]
            partner_name = str(payment.partner_id.name).split(' ',2)
            # Apellido Paterno
            file_data_aux += partner_name[0].ljust(30)
            # Apellido Materno
            file_data_aux += partner_name[1].ljust(20)
            # Nombre
            file_data_aux += partner_name[2].ljust(30)
            # ===== Bank account ======= #
            if payment.payment_bank_account_id:
                file_data_aux += str(payment.payment_bank_account_id.acc_number).ljust(16)
            else:
                file_data_aux += str(payment.partner_id.bank_ids[0].acc_number).ljust(16)

            if correct_data:
                # ========= Amount =======#
                total_amount += payment.amount
                amount = round(payment.amount, 2)
                amount = "%.2f" % payment.amount
                amount = str(amount).split('.')
                file_data_aux += str(amount[0]).zfill(16)
                file_data_aux += str(amount[1])

                file_data_aux += "\r\n"
                file_data += file_data_aux
                next_no += 1
            else:
                file_data_error += error_message

        # ===== Type Of Reg=========#
        file_data += "3"
        # ===== Sequence Number =======#
        # file_data += "00000"
        file_data += str(next_no).zfill(5)
        # ====== total_record =======#
        file_data += str(total_record).zfill(5)
        # ====== Total Amount ======== #
        amount = round(total_amount, 2)
        amount = "%.2f" % total_amount
        amount = str(amount).split('.')
        file_data += str(amount[0]).zfill(16)
        file_data += str(amount[1])

        gentextfile = base64.b64encode(bytes(file_data, 'utf-8'))
        gentextfile_error = base64.b64encode(bytes(file_data_error, 'utf-8'))
        self.file_data = gentextfile
        self.file_name = str(self.journal_id.name)+'_layout'+'.txt'
        self.file_data_error = gentextfile_error
        self.file_name_error = 'SANTANDER_ERROR.txt'

    def payroll_payment_hsbc_file_format(self):
        ascii_non_accepted = '!@#$%&/()\'*+;:[]=^`{|}~,".?^<\'".-'
        file_data = ''
        filename = ''
        #======= File Format Indication =======#
        file_data += 'MXPRLF'
        #====== Authorization Level ======#
        file_data += 'F'
        #====== Charge Account ==== ====#
        if self.journal_id and self.journal_id.bank_account_id:
            ac_num = self.journal_id.bank_account_id.acc_number
            file_data += ac_num.lstrip(ac_num[0])
        else:
            file_data += "".ljust(9)
        #===== Total Amount ===== TODO#
        total_amount = sum(x.amount for x in self.payment_ids)
        amount = "%.2f" % total_amount
        amount = float(amount)
        file_data += str(int(amount * 100)).zfill(14)
        #===== Total Operation =====#
        total_record = len(self.payment_ids)
        file_data += str(total_record).zfill(7)
        #===== Value Date ===== #
        currect_time = datetime.today()
        timez = timezone('America/Mexico_City')
        currect_time = currect_time.astimezone(timez)
        file_data +=str(currect_time.day).zfill(2)
        file_data +=str(currect_time.month).zfill(2)
        file_data +=str(currect_time.year)
        #===== Application Time =======#
        file_data += currect_time.strftime("%H:%M")
        #===== Batch Reference =====#
        file_data += ''.ljust(34)
        file_data += "\r\n"
        for payment in self.payment_ids:
            #===== Beneficiary Account =======#
            if payment.payment_bank_account_id:
                file_data += payment.payment_bank_account_id.acc_number.zfill(10)
            else:
                file_data += "".zfill(10)
 
            #===== Amount ====== #
            amount = round(payment.amount, 2)
            amount = "%.2f" % payment.amount
            amount = str(amount).split('.')
            file_data +=str(amount[0]).zfill(12)
            file_data +=str(amount[1])
            
            #===== Reference for the account ref ===#
            payment_ref = 'PAGO DE NOMINA QNA '
            fornight = '00 '
            if payment.fornight:
                fornight = payment.fornight+" "
            payment_ref += fornight
            currect_time = datetime.today()
            payment_ref += str(currect_time.year)
            
            file_data += payment_ref.ljust(34)
            #===== Name of the Benefiaciry =====#
            ascii_partner_name = ''
            if payment.partner_id:
                ascii_partner_name = regex_str.sub(r'[^\x00-\x7F]+', '', payment.partner_id.name.translate(str.maketrans('áéíóúüñÁÉÍÓÚÜÑ', 'aeiouunAEIOUUN')))
                for special_character in ascii_non_accepted:
                    ascii_partner_name = ascii_partner_name.replace(special_character, '')
            ascii_partner_name = ''.join(letter for letter in ascii_partner_name if not letter.isdigit())
            file_data += (' '.join(ascii_partner_name.split()))[:35].ljust(35) #(payment.partner_id.name.translate(str.maketrans('áéíóúüñÁÉÍÓÚÜÑ', 'aeiouunAEIOUUN')).replace('-',''))[:35].ljust(35)
            file_data += "\r\n"

        filename += 'HS'
        today = datetime.today()
        filename += str(today.year)[-2:]
        filename += str(self.payment_ids[0].fornight)
        if self.payment_ids[0].payroll_request_type == 'additional_benefits':
            filename += 'PA'
        elif self.payment_ids[0].payroll_request_type == 'alimony':
            filename += 'PN'
        elif self.payment_ids[0].payroll_request_type == 'payroll_payment':
            filename += 'SD'
        filename += '.txt'
            
        gentextfile = base64.b64encode(bytes(file_data,'ascii',errors='replace'))
        self.file_data = gentextfile
        self.file_name = filename


    def payroll_paymentbbva_bancomer_bbva_nomina_file_format(self):
        file_data = ''
        file_name = 'BBVA_BANCOMER_PAYROLL.txt'
        #====== Identifier ========#
        file_data += '1'
        #====== Total Line ==== #
        total_record = len(self.payment_ids)
        file_data += str(total_record).zfill(7)
        #===== Total Amount =====  #
        total_amount = sum(x.amount for x in self.payment_ids)
        amount = "%.2f" % total_amount
        amount = str(amount).split('.')
        file_data +=str(amount[0]).zfill(13)
        file_data +=str(amount[1])
        
        #===== Constant Records ======#
        file_data += ''.zfill(7)
        #===== Constant Amount ======#
        file_data += ''.zfill(15)
        #===== Contract ======#
        file_data += '001800001316'
        #===== Contract ======#
        file_data += '4200106119R05'
        #===== Type Of Service ======#
        file_data += '101'
        #===== Constant ======#
        file_data += '1'
        #==== Issue Date ======#
        currect_time = datetime.today()
        file_data +=str(currect_time.year)[2:]
        file_data +=str(currect_time.month).zfill(2)
        file_data +=str(currect_time.day).zfill(2)
        file_data += "\r\n"
        next_number = 1
        nombre = ''
        lista = []
        for payment in self.payment_ids:
            lista.append(payment.partner_id.name)
            #====== Record identifier ======#
            file_data += "3"
            #====== Sequential =========#
            if nombre == '':
                file_data += str(next_number).zfill(2)
            elif nombre == lista[0]:
                next_number += 1
                file_data += str(next_number).zfill(2)
            else:
                next_number = 1
                file_data += str(next_number).zfill(2)
            lista.clear()
            nombre = payment.partner_id.name
            #=== Main reference / Employee Beneficiary ====  #
            partner_name_data = payment.partner_id.name.translate(str.maketrans('áéíóúüñÁÉÍÓÚÜÑ', 'aeiouunAEIOUUN')) if payment.partner_id and payment.partner_id.name else ''
            partner_name_data = ''.join(correct_data for correct_data in partner_name_data if correct_data.isalpha() or correct_data.isspace())
            file_data += partner_name_data[:20].ljust(20)
            #==== Movement Amount ==========#
            amount = round(payment.amount, 2)
            amount = "%.2f" % payment.amount
            amount = str(amount).split('.')
            file_data +=str(amount[0]).zfill(13)
            file_data +=str(amount[1])
            
            #==== PaymentDate Date ==========#
            if payment.payment_date:
                file_data +=str(payment.payment_date.year)[2:]
                file_data +=str(payment.payment_date.month).zfill(2)
                file_data +=str(payment.payment_date.day).zfill(2)
            else:
                file_data += '      '
            #==== Ban account payment receiving  ===#
            if payment.payment_bank_account_id:
                file_data += payment.payment_bank_account_id.acc_number.zfill(16)
            else:
                file_data += "".zfill(16)
            
            #===== Payroll payment request type / Fortnight TODO====#
            request_type = ''
            #if payment.payroll_request_type == 'university':
            #3,938,393,813,154-006-M
            if payment.payroll_request_type == 'payroll_benefit_payment':
                request_type = 'SUELDO QN'
            elif payment.payroll_request_type == 'add_benifit':
                request_type = 'PRESTA QN'
            elif payment.payroll_request_type == 'alimony':
                request_type = 'PENSION Q'
            fornight = '00 '
            if payment.fornight:
                fornight = payment.fornight+" "
            request_type += fornight
            currect_time = datetime.today()
            request_type += str(currect_time.year)[2:]
             
            file_data += request_type.ljust(14)
            #==== Status ======#
            file_data += '00'
            #==== Filler =====#
            file_data += ''.ljust(4)
            
            file_data += "\r\n"
            
        gentextfile = base64.b64encode(bytes(file_data,'utf-8'))
        self.file_data = gentextfile
        self.file_name = TXT_FILE_FORMAT % self.sit_file_key

    def bbva_bancomer_payroll_232_file_format(self):
        file_data = ''
        file_name = 'BBVA_BANCOMER_DISPERSION_PAYROLL_232.txt'
        #====== Identification No =======#
        file_data += '1'
        #====== Total Line ==== #
        total_record = len(self.payment_ids)
        file_data += str(total_record).zfill(7)
        #===== Total Amount =====  #
        total_amount = sum(x.amount for x in self.payment_ids)
        amount = "%.2f" % total_amount
        amount = str(amount).split('.')
        file_data +=str(amount[0]).zfill(13)
        file_data +=str(amount[1])
        #==== Number of NOK Records ====#
        file_data += ''.zfill(7)
        #==== Total amount NOK Records ====#
        file_data += ''.zfill(15)
        #========= Contract Page ===========#
        file_data += ''.zfill(12)
        #========= Number ===========#
        file_data += '4200106119'
        #========= Legend Code ===========#
        file_data += 'R23'
        #========= Type Of Service ===========#
        file_data += '101'
        #========= Entry Key ===========#
        file_data += '1'
        #========= File Creation Date ===========#
        currect_time = datetime.today()
        file_data +=str(currect_time.year)
        file_data +=str(currect_time.month).zfill(2)
        file_data +=str(currect_time.day).zfill(2)
        #==== Date OF Payment TODO =======#
        currect_time = datetime.today()
        if self.payment_ids and self.payment_ids[0].payment_date:
            currect_time = self.payment_ids[0].payment_date
        file_data +=str(currect_time.year)
        file_data +=str(currect_time.month).zfill(2)
        file_data +=str(currect_time.day).zfill(2)
        
        #===== Filler ========#
        file_data += ''.ljust(142)
        file_data += '\r\n'
        
        for payment in self.payment_ids:
            #======== Identifier ====#
            file_data += '3'
            #=== Type Of Request for payment of payroll TODO =====#
            req_payroll = '0000001'
            #if payment.payroll_request_type == 'university':
            #3,938,393,813,155-006-M
            if payment.payroll_request_type == 'payroll_benefit_payment':
                req_payroll = '0000001'
            if payment.payroll_request_type == 'add_benifit':
                req_payroll = '0000002'
                 
            file_data += req_payroll
             
            #==== CURP / RFC TODO ======#
            vat = ''
            emp_id = self.env['hr.employee'].search([('emp_partner_id','=',payment.partner_id.id)],limit=1)
            if emp_id:
                vat = emp_id.rfc
            elif payment.partner_id.vat:
                vat = payment.partner_id.vat
                
            file_data += vat.ljust(18)
            #==== Type Of account TODO=======#
            bank_account_code = '00'
            if payment.partner_id and payment.partner_id.bank_ids:
                if payment.partner_id.bank_ids[0].account_type=='card':
                    bank_account_code = '03'
                elif payment.partner_id.bank_ids[0].account_type=='clabe_acc':
                    bank_account_code = '40'
                elif payment.partner_id.bank_ids[0].account_type=='checks':
                    bank_account_code = '01'
                elif payment.partner_id.bank_ids[0].account_type=='payment_card':
                    bank_account_code = '98'
                    
            file_data +=  bank_account_code

            bank_key_value = '000'
            if payment.payment_bank_account_id and payment.payment_bank_account_id.bank_id and payment.payment_bank_account_id.bank_id.l10n_mx_edi_code:
                bank_key_value = payment.payment_bank_account_id.bank_id.l10n_mx_edi_code
            elif payment.payment_bank_account_id and payment.payment_bank_account_id.l10n_mx_edi_clabe:
                bank_key_value = payment.payment_bank_account_id.l10n_mx_edi_clabe[0:3]

            #===== Target Bank ========#
            #file_data += '012'
            file_data += bank_key_value

            clabe_place_key = '000'
            if payment.payment_bank_account_id and payment.payment_bank_account_id.bank_id and payment.payment_bank_account_id.bank_id.l10n_mx_edi_code != '012' and payment.payment_bank_account_id.l10n_mx_edi_clabe:
                clabe_place_key =  payment.payment_bank_account_id.l10n_mx_edi_clabe[3:6]

            #==== Destination Sequence ======#
            #file_data += '000'
            file_data += clabe_place_key

            #===== Beneficiary Account =======#
            if payment.payment_bank_account_id:
                file_data += payment.payment_bank_account_id.acc_number.zfill(16)
            else:
                file_data += "".zfill(16)
 
            #===== Amount ====== #
            amount = round(payment.amount, 2)
            amount = "%.2f" % payment.amount
            amount = str(amount).split('.')
            file_data +=str(amount[0]).zfill(13)
            file_data +=str(amount[1])
            #======= Payment Status Code =====#
            file_data += ''.zfill(7)
            #====== Payment status Description =====#
            file_data += ''.ljust(80)
            #====== Deposite account holder TODO=====#
            #file_data += payment.partner_id.name.ljust(40)
            partner_name_data = payment.partner_id.name.translate(str.maketrans('áéíóúüñÁÉÍÓÚÜÑ', 'aeiouunAEIOUUN')) if payment.partner_id and payment.partner_id.name else ''
            partner_name_data = ''.join(correct_data for correct_data in partner_name_data if correct_data.isalpha() or correct_data.isspace())
            file_data += partner_name_data[:40].ljust(40)
            #==== Reason for payment ======= #
            file_data += ''.ljust(40)
            
            file_data += '\r\n'
        gentextfile = base64.b64encode(bytes(file_data,'utf-8'))
        self.file_data = gentextfile
        self.file_name = TXT_FILE_FORMAT % self.sit_file_key

    def _get_fornight(self,payments):
        
        fornight=''
        for i in payments:
            if i.fornight and fornight == '':
                fornight = str(i.fornight)

        return fornight

    def payroll_payment_banamex_file_format(self):

        file_data = ''
        file_name = 'BANAMEX'
        name=False
        #===== Type Of Records =======#
        file_data += '1'
        #===== Customer Identification Number =======#
        file_data += '000008505585'
        #===== Payment Date =======#
        currect_time = datetime.today()
        if self.payment_ids and self.payment_ids[0].payment_date:
            currect_time = self.payment_ids[0].payment_date
        
        file_data +=str(currect_time.day).zfill(2)
        file_data +=str(currect_time.month).zfill(2)
        file_data +=str(currect_time.year)[2:]
        
        #====== Sequence of the file TODO =====#
        file_data += '0001'
        #===== Company Name ======#
        file_data += 'UNIVERSIDAD NACIONAL AUTONOMA MEXICO'
        #====Description the file of   ====#
        file_data += 'DEPOSITO SUELDOS Q'
        fornight_file = self._get_fornight(self.payment_ids)
        file_data += fornight_file
        #===== Nature of the file =======#
        file_data += '05'
        #===== Instructions for payment orders ===#
        file_data += ''.ljust(40)
        #===== Layout Version =======#
        file_data += 'C'
        #===== Volume =======#
        file_data += '0'
        #===== File characteristics =======#
        file_data += '1'
        #===== File Status =======#
        file_data += '  '
        #===== File authorization number =======#
        file_data += ''.ljust(12)
        
        file_data += '\r\n'
        #======== Second  Heading ========#
        #==== Records Type ====#
        file_data += "2"
        #==== Operation Type ====#
        file_data += "1"
        #==== Currency Key ====#
        file_data += "001"
        #==== Total amount  TODO====#
        total_amount = sum(x.amount for x in self.payment_ids)
        amount = "%.2f" % total_amount
        amount = str(amount).split('.')
        file_data +=str(amount[0]).zfill(16)
        file_data +=str(amount[1])
        
        #==== Account Type ====#
        file_data += "01"
        
        #==== Branch Number TODO====#
        branch_no = ''
        if self.journal_id.branch_number:
            branch_no = self.journal_id.branch_number
        file_data += branch_no.zfill(4)
        #==== Account Number ====#
        bank_account_no = ''
        if self.journal_id.bank_account_id:
            bank_account_no = self.journal_id.bank_account_id.acc_number
            
        file_data += bank_account_no.zfill(20)
        #==== Blank Space====#
        file_data += "".ljust(20)
        #==== Return Amount====#
        file_data += "".ljust(18)
        
        file_data += "\r\n"
        row_count = 1
        for payment in self.payment_ids:
            #==== Record Type ======#
            file_data += '3'
            #==== Operation Key ======#
            file_data += '0'
            #==== Currency Key ======#
            file_data += '001'
            #===== Amount ========#
            amount = round(payment.amount, 2)
            amount = "%.2f" % amount
            amount = str(amount).split('.')
            file_data +=str(amount[0]).zfill(16)
            file_data +=str(amount[1])
            #file_name=payment.journal_id.name
            #===== Account Type ======#
            bank_account_code = '01'
            if payment.partner_id and payment.partner_id.bank_ids:
                if payment.partner_id.bank_ids[0].account_type=='card':
                    bank_account_code = '03'
                elif payment.partner_id.bank_ids[0].account_type=='clabe_acc':
                    bank_account_code = '40'
                elif payment.partner_id.bank_ids[0].account_type=='checks':
                    bank_account_code = '01'
                elif payment.partner_id.bank_ids[0].account_type=='payment_order':
                    bank_account_code = '04'
                elif payment.partner_id.bank_ids[0].account_type=='con_acc':
                    bank_account_code = '15'
            
            file_data += bank_account_code
            #======== Account Number =====#
            account_no = ''
            if payment.partner_id and payment.partner_id.bank_ids:
                logging.critical(payment.partner_id.bank_ids[0].branch_number)
                if payment.partner_id.bank_ids[0].branch_number:
                    account_no += payment.partner_id.bank_ids[0].branch_number
                logging.critical(payment.partner_id.bank_ids[0].acc_number)
                if payment.partner_id.bank_ids[0].acc_number:
                    account_no += payment.partner_id.bank_ids[0].acc_number
                    
            file_data += account_no.zfill(20)
            #======= Operation Reference ======#
            file_data += '0000000010'
            file_data += ''.ljust(30)
            #====== Beneficiary  ====#
            file_data += payment.partner_id.name.ljust(55)
            #====== Instructions ====#
            file_data += ''.ljust(40)
            #====== Description TODO====#
            request_type = ''
            if payment.payroll_request_type:
                #if payment.payroll_request_type == 'university':
                #3,938,393,813,156-006-M
                if payment.payroll_request_type == 'payroll_payment':
                    request_type = 'PAGO DE NOMINA QNA'
                    if not name:
                        name=True
                        file_name = payment.journal_id.name+'__NOMINA_Q'+fornight_file
                        file_name = file_name.replace(' (','__')
                        file_name = file_name.replace(')','')
                elif payment.payroll_request_type == 'additional_benifit':
                    request_type = 'PRESTA QN'
                    if not name:
                        name=True
                        file_name = payment.journal_id.name+ '__PRESTACIONES_Q'+fornight_file
                        file_name = file_name.replace(' (','__')
                        file_name = file_name.replace(')','')
                elif payment.payroll_request_type == 'alimony':
                    request_type = 'PAGO PENSION A QNA'
                    if not name:
                        name=True
                        file_name = payment.journal_id.name+ '__PENSION_Q'+fornight_file
                        file_name = file_name.replace(' (','__')
                        file_name = file_name.replace(')','')
                fornight = '00'
                if payment.fornight:
                    fornight = payment.fornight
                request_type += fornight
                currect_time = datetime.today()
                request_type += str(currect_time.year)
                 
                file_data += request_type.ljust(24)
            else:
                if payment.payment_request_type=='pension_payment' and payment.payment_request_id:
                    request_type = 'PAGO PENSION A QNA'
                    fornight = '00'
                    if payment.payment_request_id.fornight:
                        fornight = str(payment.payment_request_id.fornight)
                    request_type += fornight
                    
                    if payment.payment_request_id.invoice_date:
                        request_type += str(payment.payment_request_id.invoice_date.year)
                    file_data += request_type.ljust(24)    
                else:
                    file_data += ''.ljust(24)
            #====== Bank code ====#
            file_data += '0000'
            #====== Reference Low Value ====#
            file_data += str(row_count).zfill(7)
            row_count += 1
            
            #======= Term =======#
            file_data += '00'
            file_data += "\r\n"
        
        #====== Total Records Data========#
        #===== Record Type =======#
        file_data += '4'
        #===== Currency Key =======#
        file_data += '001'
        #===== Number of subscriptions=======#
        total_rec= len(self.payment_ids)
        file_data += str(total_rec).zfill(6)
        #===== Total Amount TODO=======#
        total_amount = sum(x.amount for x in self.payment_ids)
        amount = "%.2f" % total_amount
        amount = str(amount).split('.')
        file_data +=str(amount[0]).zfill(16)
        file_data +=str(amount[1])
        
        #===== Number of charges=======#
        file_data += '000001'
        #===== Total Amount of charges TODO=======#
        total_amount = sum(x.amount for x in self.payment_ids)
        amount = "%.2f" % total_amount
        amount = str(amount).split('.')
        file_data +=str(amount[0]).zfill(16)
        file_data +=str(amount[1])
        file_data += "\r\n"
        
        gentextfile = base64.b64encode(bytes(file_data,'utf-8'))
        self.file_data = gentextfile
        self.file_name = TXT_FILE_FORMAT % file_name

    def scotiabank_file_format(self):
        file_data = ''
        file_name = 'SCOTIABANK.txt'
        #====== Header First Line =====#
        #==== File Type ======#
        file_data += 'EE'
        #==== Type Of Registration ======#
        file_data += 'HA'
        #==== Contract Number ======#
        file_data += '06852'
        #==== Sequence ======#
        file_data += '01'
        #==== File Generation Date ======#
        file_data += ''.zfill(8)
        #==== Initial Date ======#
        file_data += ''.zfill(8)
        #==== End Date ======#
        file_data += ''.zfill(8)
        #==== Code-status-registration ======#
        file_data += ''.zfill(3)
        #==== Filler ======#
        file_data += ''.ljust(332)
        
        file_data += '\r\n'
        #======= Block ​ header (second header, second row of the file) ====#
        #=== File Type =======#
        file_data += 'EE'
        #=== File Type =======#
        file_data += 'HB'
        #=== Charge Currency =======#
        file_data += '00'
        #=== Fature Type =======#
        file_data += '0000'
        #=== Charge Account =======#
        if self.journal_id and self.journal_id.bank_account_id:
            file_data += self.journal_id and self.journal_id.bank_account_id.acc_number.zfill(11)
        else:
            file_data += "".zfill(11)
        
        #=== Company Reference =======#
        file_data += '0000000000'
        #=== Code status registration =======#
        file_data += '000'
        #==== Filler =======#
        file_data += ''.ljust(336)
        
        file_data += '\r\n'
        sequence_no = 1
        for payment in self.payment_ids:
            file_data_row_one = ''
            file_data_row_two = ''
            # For second detail
            # File type EE(2 positions) and recod type DM(2 positions) are constant values
            file_data_row_two += 'EEDM'
            #===== File Type ========#
            file_data_row_one += 'EE'
            #===== Record Type ========#
            file_data_row_one += 'DA'
            #===== Movement Type ========#
            file_data_row_one += '04'
            #===== Cve currency payment ========#
            file_data_row_one += '00'
            #===== Amount TODO ========#
            amount = round(payment.amount, 2)
            amount = "%.2f" % payment.amount
            amount = str(amount).split('.')
            file_data_row_one +=str(amount[0]).zfill(13)
            file_data_row_one +=str(amount[1])
            
            #===== Application Date========#
            if payment.payment_date:
                file_data_row_one +=str(payment.payment_date.year)
                file_data_row_one +=str(payment.payment_date.month).zfill(2)
                file_data_row_one +=str(payment.payment_date.day).zfill(2)
            else:
                file_data_row_one += '00000000'
            
            #===== Service Concept========#
            file_data_row_one += '01'
            #===== Cve-beneficiary ========#
            file_data_row_one += str(sequence_no).zfill(20)
            sequence_no += 1
            #===== RFC TODO========#
            vat = ''
            emp_id = self.env['hr.employee'].search([('emp_partner_id','=',payment.partner_id.id)],limit=1)
            if emp_id:
                vat = emp_id.rfc
            elif payment.partner_id.vat:
                vat = payment.partner_id.vat
            file_data_row_one += vat.ljust(13)
            # For second detail
            # Beneficiary email(100 position) empty if email doesn't exist
            file_data_row_two += emp_id.work_email.ljust(100) if emp_id and emp_id.work_email else ''.ljust(100)
            # For second detail
            # Future use(34 positions) and trace key(30 positions)
            file_data_row_two += ''.zfill(34) + ''.ljust(30)
            # For second detail
            # Banxico credit's reference, currently undefined
            file_data_row_two += ''.zfill(20)
            # For second detail
            # Operation type is 00(2 positions) and filler(180 positions), 370 positions in file
            file_data_row_two += ''.zfill(2) + ''.ljust(180)
            #===== Employee/Beneficiary========#
            file_data_row_one += str(payment.partner_id.name).ljust(40)
            #===== Payment Date ========#
            file_data_row_one += ''.zfill(8)
            if payment.payment_date:
                file_data_row_one +=str(payment.payment_date.month).zfill(2)
                file_data_row_one +=str(payment.payment_date.day).zfill(2)
                file_data_row_one +=str(payment.payment_date.year)
                
            else:
                file_data_row_one += '00000000'

            #===== SBI Payment Place========#
            file_data_row_one += '00000'
            #===== Payment Branch========#
            file_data_row_one += '00000'
            #===== Bank account receiving payment========#
            # se cambio a Numero de cuenta del beneficiario
            if emp_id.bank_ids:
                file_data_row_one += str(emp_id.bank_ids[0].acc_number).zfill(20)
            elif payment.partner_id.bank_ids:
                file_data_row_one += str(payment.partner_id.bank_ids[0].acc_number).zfill(20)
            else:
                raise ValidationError(_(f'El beneficiario {payment.partner_id.name} no tiene cuenta bancaria asignada'))
            
            #===== Country========#
            file_data_row_one += '00000'
            #===== City / State========#
            file_data_row_one += ''.ljust(40)
            #===== Account Type========#
            file_data_row_one += '1'
            #===== Digit Exchange========#
            file_data_row_one += ' '
            #===== Plaza-cuenta-banco========#
            file_data_row_one += ''.zfill(5)
            #===== Issuer-bank-number========#
            file_data_row_one += '044'
            #===== Num-banktransceiver========#
            file_data_row_one += '044'
            #===== Days-Valid========#
            file_data_row_one += '001'
            #===== Concept-Payment========#
            file_data_row_one += 'SUELDO QUINCENAL'.ljust(50)
            #===== Field-use-company-1========#
            file_data_row_one += ''.ljust(20)
            #===== Field-use-company-2========#
            file_data_row_one += ''.ljust(20)
            #===== Field-use-company-3========#
            file_data_row_one += ''.ljust(20)
            #===== Code-status-registration========#
            file_data_row_one += ''.zfill(3)
            #===== Cve-change-inst========#
            file_data_row_one += ''.zfill(1)
            #===== Code-status-change-inst========#
            file_data_row_one += ''.zfill(3)
            #===== Payment date========#
            file_data_row_one += ''.zfill(8)
            #===== Payment place========#
            file_data_row_one += ''.zfill(5)
            #===== Branch OF Payment========#
            file_data_row_one += ''.zfill(5)
            #===== Filler ========#
            file_data_row_one += ''.ljust(22)
            file_data_row_one += '\r\n'
            file_data_row_two += '\r\n'

            file_data += file_data_row_one
            # file_data += file_data_row_two
            
        #====== Block trailer does ​ (penultimate row of the file) =====#

        #==== File Type =====#
        file_data += 'EE' 
        #==== Record type =====#
        file_data += 'TB' 
        #===== Amount of high movements TODO =====#
        total_rec = len(self.payment_ids)
        file_data += str(total_rec).zfill(7)
        #==== Total Amount TODO =====#
        total_amount = sum(x.amount for x in self.payment_ids)
        amount = "%.2f" % total_amount
        amount = str(amount).split('.')
        file_data +=str(amount[0]).zfill(15)
        file_data +=str(amount[1])
        #==== Number of low movements=====#
        file_data += ''.zfill(7)
        #==== Amount of low movements=====#
        file_data += ''.zfill(17)
        #==== Data filled in by the bank=====#
        file_data += ''.zfill(195)
        #==== Filler=====#
        file_data += ''.ljust(123)
        file_data += "\r\n"
        #======= Trailer file does ​ (last row of the file) =======#
        
        #==== File Type =====#
        file_data += 'EE' 
        #==== Record type =====#
        file_data += 'TA' 
        #===== Amount of high movements TODO =====#
        total_rec = len(self.payment_ids)
        file_data += str(total_rec).zfill(7)
        #==== Total Amount TODO =====#
        total_amount = sum(x.amount for x in self.payment_ids)
        amount = "%.2f" % total_amount
        amount = str(amount).split('.')
        file_data +=str(amount[0]).zfill(15)
        file_data +=str(amount[1])
        #==== Number of low movements=====#
        file_data += ''.zfill(7)
        #==== Amount of low movements=====#
        file_data += ''.zfill(17)
        #==== Data filled in by the bank=====#
        file_data += ''.zfill(198)
        #==== Filler=====#
        file_data += ''.ljust(120)
        file_data += '\r\n'

        gentextfile = base64.b64encode(bytes(file_data,'ascii',errors='replace'))
        self.file_data = gentextfile
        self.file_name = str(self.journal_id.name)+'_layout'+'.txt'

    def banorte_file_format(self):
        file_data = ''
        # Get payment type
        payment_type = set(p.payroll_request_type for p in self.payment_ids)
        sequence = {
            'payroll_payment': '01',
            'additional_benefits': '02',
            'alimony': '03',
        }[list(payment_type)[0]]

        file_name = 'NI20052%s.PAG' % sequence

        #==== Record type =====#
        file_data += 'H'
        #====== Service code ========#
        file_data += 'NE'
        #====== Issuer ========#
        file_data += '20052'
        #====== Process Date TODO========#
        currect_time = datetime.today()
        if self.payment_ids and self.payment_ids[0].payment_date:
            currect_time = self.payment_ids[0].payment_date
        
        file_data +=str(currect_time.year)
        file_data +=str(currect_time.month).zfill(2)
        file_data +=str(currect_time.day).zfill(2)
        
        #====== Consecutive Number TODO========#
        file_data += sequence
        #====== Total No of records ========#
        total_rec = len(self.payment_ids)
        file_data += str(total_rec).zfill(6)
        #====== Total Amount of Records ========#
        total_amount = sum(x.amount for x in self.payment_ids)
        amount = "%.2f" % total_amount
        amount = str(amount).split('.')
        file_data +=str(amount[0]).zfill(13)
        file_data +=str(amount[1])
        
        #====== Total Number of HIGH Sent ========#
        file_data += ''.zfill(6)
        #====== Total Amount of HIGH Sent========#
        file_data += '000000000000000'
        #====== Total number of DEPARTURES Sent========#
        file_data += ''.zfill(6)
        #====== Total Amount of DEPARTURES Sent========#
        file_data += ''.zfill(15)
        #====== Total number of account verify Sent========#
        file_data += ''.zfill(6)
        #====== Action========#
        file_data += ''.zfill(1)
        #====== Filler========#
        file_data += ''.rjust(77, ' ')
        file_data += "\r\n"

        for payment in self.payment_ids:
            #====== Type of record ========#
            file_data += 'D'
            #====== Application date=======#
            if payment.payment_date:
                file_data +=str(payment.payment_date.year)
                file_data +=str(payment.payment_date.month).zfill(2)
                file_data +=str(payment.payment_date.day).zfill(2)
                
            else:
                file_data += '00000000'
            #========= Partner Bank Account==============#
#             bank_account = ''
#             if payment.payment_bank_account_id and payment.payment_bank_account_id.acc_number:
#                 bank_account = payment.payment_bank_account_id.acc_number
#             file_data += bank_account
            #====== Employee number ========#
            benific = ''
            emp_id = self.env['hr.employee'].search([('emp_partner_id','=',payment.partner_id.id)],limit=1)
            if emp_id:
                benific = emp_id.worker_number
#             if payment.partner_id.password_beneficiary:
#                 benific = payment.partner_id.password_beneficiary
            file_data += benific.zfill(10)
            
            #====== Service reference========#
            file_data += ''.ljust(40)
            #====== Fortnight ========#
            fortnight_msg = 'PAGO NOMINA QNA '
            fornight = '00'
            if payment.fornight:
                fornight = payment.fornight
            fortnight_msg += fornight
            currect_time = datetime.today()
            fortnight_msg += str(currect_time.year)[2:]
            fortnight_msg += " DE LA EMISORA 20052"
            
            file_data += fortnight_msg.ljust(40)
            #====== Amount========#
            amount = round(payment.amount, 2)
            amount = "%.2f" % payment.amount
            amount = str(amount).split('.')
            file_data +=str(amount[0]).zfill(13)
            file_data +=str(amount[1])

            #====== Type of account ========#
            bank_account_code = '00'
            aux_bank_code = '000'
            if payment.payment_bank_account_id and payment.payment_bank_account_id.bank_id and payment.payment_bank_account_id.bank_id.l10n_mx_edi_code == '072':
                bank_account_code = '01'
                aux_bank_code = '072'
            elif payment.payment_bank_account_id and payment.payment_bank_account_id.bank_id and payment.payment_bank_account_id.bank_id.l10n_mx_edi_code:
                bank_account_code = '40'
                aux_bank_code = payment.payment_bank_account_id.bank_id.l10n_mx_edi_code

            #====== Receiving bank number========#
            file_data += aux_bank_code
            file_data +=  bank_account_code
            
            #====== Payment receipt bank account   TODO========#
            if payment.payment_bank_account_id:
                file_data += payment.payment_bank_account_id.acc_number.zfill(18)
            else:
                temp =''
                file_data +=temp.zfill(18)
            
            #====== Movement type========#
            file_data += ''.zfill(1)
            #====== Action========#
            file_data += ''.ljust(1)
            #====== VAT amount of the transaction========#
            file_data += ''.zfill(8)
            #===== Filler =====#
            file_data += ''.ljust(18)
            file_data += '\r\n'
    
        gentextfile = base64.b64encode(bytes(file_data,'utf-8'))
        self.file_data = gentextfile
        self.file_name = file_name

    def santander_h2h_file_format(self):
        fecha_actual = datetime.now()
        # ------------------ENCABEZADO------------------------
        # 01 0000001 60 014 E 2 21 00001 2023 08 22 01 00 1
        num_sec = 0
        num_op = 0
        imp_tot_op = 0
        file_data = ''
        file_err_dat = ''
        type_req = ''

        # Tipo de Registro
        file_data += '01'
        # numero de secuencia
        num_sec += 1
        file_data += str(num_sec).zfill(7)
        # Código de Operación
        file_data += '60'
        # Numero de Banco
        file_data += '014'
        # sentido
        file_data += 'E'
        # servicio
        file_data += '2'
        # Numero de Bloque
        if self.payment_ids[0].payroll_request_type == 'additional_benefits':
            type_req = 'PA'
        elif self.payment_ids[0].payroll_request_type == 'payroll_payment':
            type_req = 'SDO'
        file_data += str(fecha_actual.day).zfill(2)
        consecutivo = self.env['layouts.consecutive'].obtener_consecutivo(fecha_actual.date(),
                                                                          self.payment_ids[0].payment_bank_id.name,
                                                                          type_req)
        if consecutivo:
            file_data += str(consecutivo).zfill(5)
        else:
            raise ValidationError(_('Solo se pueden generar 99999 consecutivos por Dia'))
        # fecha de presentación
        file_data +=str(fecha_actual.year)
        file_data +=str(fecha_actual.month).zfill(2)
        file_data +=str(fecha_actual.day).zfill(2)
        # código de divisa
        file_data += '01'
        # Causa de rechazo de bloque
        file_data += '00'
        # Modalidad
        file_data += '1'
        file_data += "\r\n"

        # ------------------DETALLES-----------------------------
        for payment in self.payment_ids:
            #   consectivo
            # 02 0000002 60 01 20230822 014 021 000000000568393
            # Tipo de Registro
            file_data += '02'
            # Numero de Secuencia 
            num_sec += 1
            file_data += str(num_sec).zfill(7)
            # Código de Operación
            file_data += '60'
            # Código de Divisa
            file_data += '01'
            # Fecha de Transferencia
            file_data +=str(fecha_actual.year)
            file_data +=str(fecha_actual.month).zfill(2)
            file_data +=str(fecha_actual.day).zfill(2)
            # Banco Presentador
            file_data += '014'
            # Banco Reseptor
            file_data += str(payment.payment_bank_id.l10n_mx_edi_code)
            # importe 
            importe = str(int(payment.amount * 100)).zfill(15)
            imp_tot_op += payment.amount
            file_data += importe

            # 16 ESPACIOS
            file_data += ''.ljust(16)

            # 02 20230825 01 00000000065501008998
            # Tipo de operación
            if payment.payment_bank_id.l10n_mx_edi_code == '014':
                file_data += '99'
            else:
                file_data += '02'
            # Fecha de Aplicación 
            if payment.payment_date:
                fec_pres_in= payment.payment_date
            file_data +=str(fec_pres_in.year)
            file_data +=str(fec_pres_in.month).zfill(2)
            file_data +=str(fec_pres_in.day).zfill(2)
            # Tipo Cuenta Ordenante
            file_data += '01'
            # Numero de cuenta ordenante 
            file_data += '00000000065501008998'

            # BANCO SANTANDER SAB DE CV               BSM750823IH5

            # nombre ordenante 
            file_data += 'BANCO SANTANDER SAB DE CV'.ljust(40)
            # RFC Ordenante 
            file_data += 'BSM750823IH5'.ljust(18)

            # 40 00021180061555517588 ALTAMIRANO BAUTISTA ADRIANA             AABA630302QX2

            if payment.payment_bank_id.l10n_mx_edi_code == '014':       
                # Tipo Cuenta receptor
                file_data += '01'                        #de santantander a santander debe de ser 01         
                # Numero de Cuenta receptor
                file_data += str(payment.payment_bank_account_id.acc_number).zfill(20)                 #11 digitos 01 y llenando con 0s a la izq
            else:
                # Tipo Cuenta receptor
                file_data += '40'                         #...y a otros bancos es 40 
                if payment.payment_bank_account_id.l10n_mx_edi_clabe:
                    # Numero de Cuenta receptor
                    file_data += str(payment.payment_bank_account_id.l10n_mx_edi_clabe).zfill(20)     #40 debe ser 18 digitos cuenta clabe y llenar con 0s a la izq
                # else:
                #     raise ValidationError(_('La Cuenta Bancaria de Recepción de Pago no tiene Clabe Interbancaria'))
            # Nombre del Beneficiario
            file_data += str(payment.partner_id.name).ljust(40)
            # RFC del Beneficiario
            file_data += str(payment.partner_id.vat).ljust(18)
            # Referencia Servicio Emisor
            file_data += ''.ljust(40)

            # BANCO SANTANDER SAB DE CV               0000000000000010000001PAGO NOMINA  QN2316 44501009 

            # Nombre Titular Servicio
            file_data += 'BANCO SANTANDER SAB DE CV'.ljust(40)
            # Importe IVA de Operación
            file_data += '000000000000000'     #15 ceros
            # Referencia Numero de Ordenante
            file_data += '1000001'
            # Referencia Leyenda del Ordenante
            year = str(fecha_actual.year)
            # en la consulta ORM no se concidera la condicion si el empleado esta archivado, el sistema solo devuelve empleados que esten "activos"
            emp_id = self.env['hr.employee'].search([('emp_partner_id','=',payment.partner_id.id)],limit=1).id
            if emp_id:
                st = str(datetime.today().year) + '-01-01'
                en = str(datetime.today().year) + '-12-31'
                emp_pay_file_id = self.env['employee.payroll.file'].search([('fornight', '=', payment.fornight),('employee_id', '=', emp_id),('period_start','>=',st),('period_start','<=',en)],limit=1)
                if emp_pay_file_id:
                    lugar_pago = emp_pay_file_id.payment_place_id.name
                    file_data += str('PAGO NOMINA  QN'+year[-2:]+str(payment.fornight)+' '+lugar_pago).ljust(40)          #24
                else:
                    file_err_dat += f'no se encontro el proceso de nomina de la quincena: {payment.fornight}, el empleado: {payment.partner_id.name}, del año: {str(datetime.today().year)}'
                    file_err_dat += "\r\n"
            else:
                file_err_dat += f'El empleado: {payment.partner_id.name} registrado en el pago no se encontro en el modelo de empleados o el empleado esta como ARCHIVADO'
                file_err_dat += "\r\n"
            # clave de rastreo
            file_data += ''.ljust(30)

            # 0020230822

            # Motivo de devolución
            file_data += '00'
            # Fecha de Presentación Inicial 
            file_data +=str(fecha_actual.year)
            file_data +=str(fecha_actual.month).zfill(2)
            file_data +=str(fecha_actual.day).zfill(2)
            # Uso Futuro 
            file_data += ''.ljust(12)
            # Referencia Cliente 
            file_data += ''.ljust(30)
            # Descripción Referencia Pago
            file_data += ''.ljust(30)
            # Email a Beneficiarios
            file_data += ''.ljust(500)
            # Linea de Captura 
            file_data += ''.ljust(100)
            file_data += "\r\n"


        # ------------------SUMARIO-----------------------------
        # 09 0000488 60 2100001 0000486 000000000476688293
        # Tipo de Registro 
        file_data += '09'
        # Numero de Secuencia
        num_sec += 1
        file_data += str(num_sec).zfill(7)
        # Código de Operación
        file_data += '60'
        # Numero de Bloque
        file_data += str(fecha_actual.day).zfill(2)
        file_data += str(consecutivo).zfill(5)
        # Numero de Operación
        file_data += str(num_op).zfill(7)
        # Importe total Operaciónes 
        file_data += str(int(imp_tot_op * 100)).zfill(18)
        file_data += "\r\n"

        gentextfile = base64.b64encode(bytes(file_data,'utf-8'))
        self.file_data = gentextfile
        # H2HSANTANDER_202316_SANTANDER_SDO.txt
        filename = 'H2HSANTANDER_'+str(fecha_actual.year)+str(self.payment_ids[0].fornight)+'_'+str(self.payment_ids[0].payment_bank_id.name).upper()+'_'+type_req
        self.file_name = filename
        if file_err_dat:
            gentextfileerror = base64.b64encode(bytes(file_err_dat,'utf-8'))
            self.file_data_error = gentextfileerror
            self.file_name_error = 'SANTANDER_H2H_ERRORS.txt'

    def inbursa_file_format(self):
        currect_time = datetime.today()
        file_data = ''
        cont = 0

        for payment in self.payment_ids:
            # Num. Registro
            cont += 1
            file_data += str(cont)
            file_data += ','
            # Número de trabajador
            emp_id = self.env['hr.employee'].search([('emp_partner_id','=',payment.partner_id.id)],limit=1)
            if emp_id.worker_number:
                num_trabajador = str(emp_id.worker_number)
                file_data += num_trabajador.lstrip('0')
                file_data += ','
            else:
                raise ValidationError(_('El empleado no cuenta con Número de trabajador'))
            # Nombre del Trabajador
            file_data += payment.partner_id.name
            file_data += ','

            # El los registros el beneficiario no tiene cuenta bancaria asignada
            # Cuenta Beneficiario
            if payment.payment_bank_account_id.acc_number:
                file_data +=str(payment.payment_bank_account_id.acc_number).zfill(12)
                file_data += ','
            else:
                raise ValidationError(_('El pago no cuenta con Cuenta Bancaria de Recepción de Pago o la Cuenta Bancaria de Recepción de Pago no tiene clabe bancaria'))
            
            # Importe
            file_data += str(payment.amount)
            file_data += "\r\n"

        gentextfile = base64.b64encode(bytes(file_data,'ascii',errors='replace'))
        self.file_data = gentextfile
        self.file_name = 'IN'+str(currect_time.year)[-2:]+str(self.payment_ids[0].fornight)+'TG'+'.txt'

    

    #Generar Layout para pagos de nómina
    def generate_payroll_payment_bank_layout(self):
        for payment in self.payment_ids:
            if payment.journal_id.id != self.journal_id.id:
                raise UserError(_("The selected layout does NOT match the bank of the selected payments"))
            
        #type_beneficiary=self.check_type_beneficiary(self.payment_ids)

        if self.journal_id.payroll_bank_format == 'santander':
            self.payroll_payment_santander_file_format()
        elif self.journal_id.payroll_bank_format == 'hsbc':
            self.payroll_payment_hsbc_file_format()
        elif self.journal_id.payroll_bank_format == 'bbva_nomina':
            self.payroll_paymentbbva_bancomer_bbva_nomina_file_format()
        elif self.journal_id.payroll_bank_format == 'bbva_232':
            self.bbva_bancomer_payroll_232_file_format()
        elif self.journal_id.payroll_bank_format == 'banamex':
            self.payroll_payment_banamex_file_format()
        elif self.journal_id.payroll_bank_format == 'scotiabank':
            self.scotiabank_file_format()
        elif self.journal_id.payroll_bank_format == 'banorte':
            self.banorte_file_format()
        elif self.journal_id.payroll_bank_format == 'santander_h2h':
            self.santander_h2h_file_format()
        elif self.journal_id.payroll_bank_format == 'inbursa':
            self.inbursa_file_format()
  
            


        return {
            'name': _(TXT_GEN_BANK_LAYOUT),
            'res_model': GEN_BANK_LAYOUT,
            'res_id' : self.id,
            'view_mode': 'form',
            'target': 'new',
            'view_id': self.env.ref('jt_supplier_payment.view_generate_payroll_payment_bank_layout').id,
            'type': IR_ACT_WINDOW,
        }    
    

'''
Esta clase se usara para generar los consecutivos en base a la fecha para los Layouts
se pide que se genere un consegutivo del 1 al 99999, cada que se ejecute dentro del mismo día, 
si el día es diferente, el consecutivo se reinicie a 1
para mantener la persistencia de este dato, se creara una clase donde guarde la cantidad de consecutivos por dia
'''
class Consecutive(models.Model):
    _name = 'layouts.consecutive'
    _description = 'Modelo para generar consegutivos en base a la fecha y permitir guardar el consecutivo'

    consecutive = fields.Integer(string='Consecutivo', default=1)
    date_consecutive = fields.Date(string='Date', default=fields.Date.today())
    bank = fields.Char(string='Bank')
    req_type = fields.Char(string='Request Type')

    def obtener_consecutivo(self,fecha, banco, rt):
        n = 0
        today = self.env['layouts.consecutive'].search([('date_consecutive','=',fecha),('bank','=',banco),('req_type','=',rt)])
        if today:
            n = today.consecutive + 1
            today.write({'consecutive': n})
        else:
            self.env['layouts.consecutive'].create({'consecutive':1, 
                                                    'date_consecutive': fecha,
                                                    'bank':banco,
                                                    'req_type':rt})
            n = 1
        if n < 100000:
            return n
        else:
            return None
