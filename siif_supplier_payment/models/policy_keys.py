# -*- coding: utf-8 -*-
import logging
from odoo import models, fields, api, _

class PolicyKeys(models.Model):
    _inherit = 'policy.keys'

    allowed_users_payment = fields.Many2many('res.users','allowed_users_upa_payment_rel', string="User Allowed")
