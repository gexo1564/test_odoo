# -*- coding: utf-8 -*-
{
  'name': 'SIIF Security',
  'summary': "SIIF Security is used to implement security between modules that share models",
  'version': '13.0.0.1.0',
  'category': 'Security',
  'author': 'SIIF UNAM',
  'maintainer': 'SIIF UNAM',
  'website': '',
  'license': 'AGPL-3',
  'depends': ['account'],
  'data': [
    'security/security.xml',

    'views/template.xml',
    'views/menus.xml',
  ],
  'application': False,
  'installable': True,
  'auto_install': True,
}