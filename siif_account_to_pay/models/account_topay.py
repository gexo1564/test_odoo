# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
import json
from xml import dom
from odoo.exceptions import UserError, ValidationError
from datetime import datetime, date
import logging
import re

from odoo.addons.siif_supplier_payment.tools.utils import Dictionary
from odoo.addons.siif_security.tools.utils import check_authorization

logger = logging.getLogger(__name__)

ACC_MOVE='account.move'
REJECT_REASON='rejection.reason'
IR_WINDOW='ir.actions.act_window'

class AccountPayable(models.Model):
  _name = 'account.topay'
  _description = 'Account Payable'
  #Heredar modelos para Bitácora
  _inherit = ['mail.thread', 'mail.activity.mixin']
  state = fields.Selection(
    [
      ('draft', 'Draft'),
      ('rejected_by_insufficiency', 'Rejected by Insufficiency'),
      ('rejected_by_documentation', 'Rejected by Documentation'),
      ('published', 'Published'),
      ('revised', 'Revised'),
      ('approved', 'Approved'),
      ('rejected', 'Rejected'),
      ('canceled', 'Canceled'),
    ], default='draft', tracking=True, string="State"
  )#Estado de la cuenta por pagar

  name = fields.Char(string='Name', required=False, tracking=True, copy=False)#Nombre o Identificador
  payment_type = fields.Char(string='Payment Type')#Tipo de Paog

  #Función para obtener los tipos de cuentas por pagar
  def get_domain_type_provision(self):
    return json.dumps([('id', 'in', self.env.user.permission_type_of_account_payable.ids)])


  type_provision = fields.Many2one('account.topay.type',"Type of Provision", domain=get_domain_type_provision)#Tipo de cuenta por pagar
  is_papiit = fields.Boolean(related='type_provision.is_papiit', readonly=True, store=False)#Si es de PAPIIT
  is_fondos = fields.Boolean(related='type_provision.is_fondos', readonly=True, store=False)#Sí es de fondo
  is_obras = fields.Boolean(related='type_provision.is_obras', readonly=True, store=False)#Sí es de obras

  partner_id = fields.Many2one('res.partner', string="Beneficiary of the Payment",tracking=True)#Beneficiario del pago
  beneficiary_key = fields.Char( string="Beneficiary Key")#Clave del beneficiario
  rfc = fields.Char(string='RFC')#RFC del beneficiario

  #Función para obtener las dependencias permitidas
  def get_domain_dependency(self):
    dep = self.env.user.dependency_ids.ids
    dep += [d.id for d in self.env.user.sub_dependency_ids.dependency_id]
    return json.dumps([('id', 'in', dep)])
  
  dependency_id = fields.Many2one('dependency', string='Dependency',tracking=True, domain=get_domain_dependency)#Dependencia
  sub_dependency_id = fields.Many2one('sub.dependency', string='Sub Dependency',tracking=True)#Subdependencia
  domain_sub_dependency = fields.Char(compute="_compute_domain_sub_dependency", store=False)#Dominio de la subdependencia

  date_receipt = fields.Date('Date Receipt',default=date.today())#Fecha de recibo
  invoice_date = fields.Date('Invoice Date',default=date.today())#Fecha de Factura/REcibo
  folio_dependency = fields.Char("Folio Dependency")#Folio de SOlicitud

  no_of_document = fields.Integer('No of Documents')#Número de Documentos

  previous_number = fields.Char('Previous Number')#Número Previo
  currency_id = fields.Many2one('res.currency',string='Currency',default=lambda self: self.env.company.currency_id)#Moneda


  secretary_admin = fields.Char('Secretary Admin',tracking=True)              #Nombre del secretario (Nombre)
  secretary_admin_email = fields.Text('Secretary Admin Email',tracking=True)  #Correo del secretario 

  #Función para obtener los secretarios
  def get_domain_secretary(self):
    cr = self._cr
    cr.execute("""
        SELECT id FROM hr_employee 
          WHERE job_id in (
          SELECT id FROM hr_job hj WHERE name LIKE '%ADMIN%' OR name LIKE '%SECRETARI%'
          );
      """)
    secretary_domain = []
    for secretary_id in cr.fetchall():
      secretary_domain.append(secretary_id[0])
    return json.dumps([('id','in',secretary_domain)])


  secretary_admin_info_id= fields.Many2one('hr.employee','Secretary Admin_ID', domain=get_domain_secretary)#Nombre del secretario administrativo (Referencia)

  #Tipo de documento UPA
  def _default_upa_document_type(self):
    return self.env['upa.document.type'].search([('document_number', '=', '02')], limit=1).id
  
  upa_document_type = fields.Many2one('upa.document.type','UPA Document Type', default=_default_upa_document_type)#upa
  document_type = fields.Selection(
    [('national', 'National Currency'), ('foreign', 'Foreign Currency')], string="Document Type", default='national')#Tipo de Documento
  operation_type_id = fields.Many2one('operation.type', "Operation Type")#Type de Operación
  folio = fields.Char(string='Folio')#Folio contra recibo
  project_type_id = fields.Many2one('project.type.custom','Project Type',tracking=True)#Tipo de proyecto
  stage_id = fields.Many2one('project.custom.stage','Stage',tracking=True)#Tipo de estado del proyecto
  project_pappit = fields.Many2one('project.project','PAPIIT Project', domain=[('is_papiit_project', '=', True)],tracking=True)#Proyecto Papiit asociado
  project_pappit_responsible= fields.Many2one(related="project_pappit.responsible_name", string="Project PAPIIT Responsible",tracking=True)#Responsable del proyecto papiiit
  fif_type_operation = fields.Char('FIFI Type Operation')#Tipo FIF
  fif_numero = fields.Char('FIFI number')#Numero FIF
  observations = fields.Char('Observations')#Observaciones
  user_registering_id = fields.Many2one('res.users','User registering',default=lambda self: self.env.user)#Usuario del registro

  #Campo para las lineas de a cuenta por pagar
  invoice_line_ids = fields.One2many('account.topay.line', 'account_topay_id', string='Invoice lines',tracking=True)
  move_line_ids = fields.One2many("account.move.line", 'account_to_pay_id', string="Journal Items",tracking=True)
  register_date = fields.Date('Register Date',default=date.today())#Fecha de Registro
  reason_rejection = fields.Many2one(REJECT_REASON, string="Reason for Rejection",tracking=True)#Campos de Rechazo
  reason_rejection_desc = fields.Text(string="Description of Reason for Rejection",tracking=True)
  docs_files = fields.Many2many("ir.attachment",'docs_attachment_rel', string="Add Document Files")#Documentos
  payment_request_ids = fields.Many2many(ACC_MOVE, string='Payment Request')#Pagos asociados
  payment_request_number = fields.Integer(string='Payment Request Number', compute="_compute_payment_request_number",store=True)

  #Campo para edición de CCS de la vista
  css_structure = fields.Html(string='CSS', sanitize=False, compute='_compute_css', store=False)

  from_web_service = fields.Boolean(default=False) #Revisión de Servicio Web
  available = fields.Float("Available")
  is_for_ministration = fields.Boolean(default=False)

  @api.model
  def create(self, vals):
    res = super(AccountPayable, self).create(vals)
    if res.state == 'draft':
      res.name = self.env['ir.sequence'].next_by_code('account.topay')
    return res

  #Función para eliminar cuenta por pagar
  def unlink(self):
    for rec in self:
      if rec.state in ['draft']:
        #Elimina las lineas de factura
        for lines in rec.invoice_line_ids:
          lines.unlink()        
      else:
        raise UserError(_('You cannot delete an entry which has been not draft state.'))
    return super(AccountPayable, self).unlink()

  #Función para obtener os parámteros de subdependencia
  @api.depends('dependency_id')
  def _compute_domain_sub_dependency(self):
    for record in self:
      domain = [('dependency_id', '=', record.dependency_id.id)]
      if record.dependency_id.id not in self.env.user.dependency_ids.ids:
        domain.append(('id', 'in', self.env.user.sub_dependency_ids.ids))
      record.domain_sub_dependency = json.dumps(domain)

  #Función para cambiar los parametros de la cuenta dependiendo el tipo
  @api.onchange('type_provision')
  def _onchange_operation_type(self):
    param=self.env['account.to.pay.parameters']
    for record in self:
      if record.type_provision:
        if record.type_provision.is_papiit:
          record.operation_type_id = param.get_param('op_cxp_papiit')
        else:
          record.operation_type_id = param.get_param('op_cxp_general')
      else:
        record.operation_type_id = None

  #Obtiene la cantidad de solicitudes de pago
  @api.depends('payment_request_ids')
  def _compute_payment_request_number(self):
    for record in self:
      record.payment_request_number = len(self.payment_request_ids)

  #Función para revisar el beneficiario
  @api.onchange('partner_id')
  def partner_onchange(self):
    if self.partner_id:
      self.beneficiary_key = self.partner_id.password_beneficiary
      self.rfc = self.partner_id.vat
      if "UNAM/" in self.partner_id.name and self.rfc:
        dep = self.env['dependency'].search([('dependency', '=', self.rfc[5:8])])
        if dep:
          self.dependency_id = dep
          self.sub_dependency_id = self.env['sub.dependency'].search([
            ('dependency_id', '=', dep.id),
            ('sub_dependency', '=', self.rfc[8:10]),
          ])

  #Obtener la cantidad de documentación
  @api.onchange('docs_files')
  def _set_docs_count(self):
    if self.from_web_service ==False:
      self.no_of_document = len(self.docs_files)

  #Detectar si se tiene proyecto
  @api.onchange('project_type_id', 'stage_id')
  def onchange_project_type_stage(self):
    self.project_pappit = None
    self.project_pappit_responsible = None

  #Eliminar info para eliminar la info si se cambia el tipo de cuenta por pagar
  @api.onchange('type_provision')
  def onchange_type_provision(self):
    self.project_type_id = None
    self.stage_id = None
    self.project_pappit = None
    self.project_pappit_responsible= None
    self.fif_type_operation = None
    self.fif_numero = None

  #Revisión de Cantidad de Número de Documentos
  @api.constrains('no_of_document')
  def _check_no_of_document(self):
    if self.from_web_service==False and self.no_of_document < 1:
      raise ValidationError(_('The Account to Pay needs Document Files'))

  #Checar si las cuentas por pagar se pueden crear dependiendo del dia
  @api.constrains('register_date', 'type_provision')
  def _check_date(self):
    start_date = self.type_provision.recording_accounting_period_start
    end_date = self.type_provision.recording_accounting_period_end
    register_date = self.register_date
    if start_date and end_date and (register_date < start_date or register_date > end_date):
      raise ValidationError(_("No esta permitido crear cuentas por pagar fuera del rango de fechas permitido."))


  #Revisión del Origen del recurso, partida y código programático
  @api.constrains('invoice_line_ids')
  def _check_origin_resource_lines(self):
    provision = self.type_provision
    is_pa = provision.is_allocated_budget_allowed
    is_ie = provision.is_nonrecurring_income_allowed
    is_cc = provision.is_account_allowed
    items = provision.specific_items_allowed_ids
    item_groups = []
    if provision.item_groups_allowed:
      item_groups = provision.item_groups_allowed.split(",")
    for line in self.invoice_line_ids:
      # Validación del Origen del Recurso
      origen_recurso = line.origin_resource_sel.key_origin
      if not ((is_pa and origen_recurso == '00') or (is_ie and origen_recurso == '01') or (is_cc and origen_recurso == '05')):
        raise ValidationError("El Origen del Recurso no coincide con la configuración de la cuenta, revise las líneas de la cuenta.")
      # Validación del Código programatico
      program_code = line.program_code_id
      if (origen_recurso == '00' or  origen_recurso == '01') and not program_code:
        raise ValidationError("El código programático es requerido para las cuentas con origen del recurso 00 y 01, revise las líneas de la cuenta.")
      elif origen_recurso == '05' and program_code:
        raise ValidationError("Las Cuentas por Pagar a partir de Cuentas Contables no requieren que se asocie con un código programático, revise las líneas de la cuenta.")
      # El código programatico existe con las combinaciones de OR 00 y 01
      if program_code:
        # Validación de las partidas
        item = program_code.item_id
        if items or item_groups:
          if not (item in items or (item.item[0] + '00') in item_groups):
            raise ValidationError("La partida (%s) del código programático (%s) no esta permitida por el tipo de Cuenta por Pagar" % (item.item, program_code.program_code))
        if not line.account_id:
          raise ValidationError("La partida (%s) del código programático (%s) no tiene asociada ninguna cuenta contable" % (item.item, program_code.program_code))
        if program_code.resource_origin_id.key_origin != origen_recurso:
          raise ValidationError("El origen del recurso del código programático no coincide con el de la línea de la Cuenta por Pagar.")
        if origen_recurso == '01':
          if not line.account_ie:
            raise ValidationError("No se encontró una cuenta de ingresos extraordinarios, revise las líneas de la cuenta.")
          accounts = line.account_ie.ie_account_line_ids.filtered(lambda x:x.main_account == True)
          if not accounts:
            raise ValidationError("La cuenta de Ingresos Extraordinarios (%s) no tiene configurada una cuenta principal." % line.account_ie.ie_key)
          if not accounts.account_id.revenue_recognition_account_id:
            raise ValidationError("La cuenta de principal de Ingresos Extraordinarios (%s) no tiene configurada una cuenta de ingresos extraordinarios." % line.account_ie.ie_key)
      if origen_recurso == '05':
        if not line.account_id:
          raise ValidationError("No selecciono nínguna Cuenta Contable de donde se obtendra el recurso, revise las líneas de la cuenta.")
        if line.account_id.dep_subdep_flag and not line.dependency_id:
          raise ValidationError("No selecciono nínguna Dependencia para la cuenta contable, revise las líneas de la cuenta.")
        if line.account_id.dep_subdep_flag and not line.sub_dependency_id:
          raise ValidationError("No selecciono nínguna Sub Dependencia para la cuenta contable, revise las líneas de la cuenta.")
      if line.origin_money <= 0:
        raise ValidationError("El monto debe ser mayor a cero, revise las líneas de la cuenta.")


  #Revision del nombre del secretario
  @api.constrains('secretary_admin')
  def check_secretary_admin(self):
    if not self.secretary_admin and self.is_for_ministration:
      raise UserError(_("Please assign the name of the Administrative Secretary."))

  #Revision del nombre del secretario, caso de la ministración
  @api.constrains('secretary_admin_info_id')
  def secretary_info(self):
    if not self.secretary_admin_info_id and not self.is_for_ministration:
      raise UserError(_("Please assign the name of the Administrative Secretary."))


  #Revisión de correo de Secretario
  @api.constrains('secretary_admin_email')
  def check_secretary_admin_emails(self):
    regex="[\w\.]+@([\w]+\.)+[\w]{2,}"
    if self.secretary_admin_email:
      aux_mails=self.secretary_admin_email.split(',')
      for mail in aux_mails:
        if not re.search(regex,mail):
          raise UserError("Correos invalido: "+mail+", Por favor ingrese un correo con formato válido. Multiples correos separados solo coma.")
    else:
      raise UserError(_('It didnt entry the Secretary Admin Email. Please entry a valid email (mail@mail.com)'))
      

  def name_get(self):
    res = []
    if self.env.context.get('show_previous_number', False):
      for account in self:
        res.append((account.id, account.previous_number or ''))
    else:
      res = super().name_get()
    return res

  def _get_current_quarter(self):
    month = self.invoice_date.strftime('%m')
    return [1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4][int(month)-1]

  def validate_available_origin_resource(self):
    move = self.env[ACC_MOVE]
    quarter = self._get_current_quarter()
    msgs = []
    lines_pa = []
    lines_ie = []
    lines_cc = []
    for line in self.invoice_line_ids:
      amount = line.mx_money
      resource_origin = line.origin_resource_sel.key_origin
      # Línea de Presupuesto Asignado
      if resource_origin == '00':
        lines_pa.append((line.program_code_id, amount))
      # Línea de Ingresos Extraordinarios
      elif resource_origin == '01':
        lines_ie.append((line.account_ie, line.program_code_id, amount))
      # Línea de Ingresos Extraordinarios
      elif resource_origin == '05':
        lines_cc.append((line.account_id, line.dependency_id, line.sub_dependency_id, amount))
    # Validación de los disponibles de códigos de PA
    msgs += move.validate_get_available_program_code_pa(lines_pa, quarter)
    # Validación de los disponibles de códigos de IE
    msgs += move.validate_available_program_code_ie(lines_ie, quarter, self.invoice_date)
    # Validación de los disponibles de cuentas contables
    msgs += move.validate_available_account(lines_cc)
    return msgs

  #Publicar provisión
  def action_validate(self):
    if self.no_of_document!=0:
      self.ensure_one()

      # Verificación de la disponibilidad
      msgs = self.validate_available_origin_resource()
      if msgs:
        self.reason_rejection = self.env[REJECT_REASON].search([('name', '=', 'Insuficiencia Presupuestal')])
        self.reason_rejection_desc = "\r\n".join(msgs)
        self.state = 'rejected_by_insufficiency'
      else:
        if self.is_for_ministration:
          self._check_ministration_info(self)
        else:  
          self.state = 'published'
    else:
      raise ValidationError(_("The account payable need to assign the necessary documents. Add them in the 'Files and Documents' tab."))
    
  def action_approve(self):
    return {
      'name': _('Aprobación de Cuenta'),
      'type': IR_WINDOW,
      'res_model': 'account.topay.approve',
      'view_mode': 'form',
      'view_type': 'form',
      'context': {'current_account_topay_id': self.id},
      'views': [(False, 'form')],
      'target': 'new'
    }

  def approve(self,exercise=False):
    if not exercise:
      # Se asigna el Número de la Cuenta por Pagar
      self.assign_number()
      # Reducción Presupuestal
      # Se asigna el número de previo
      self.assign_previous_number()
      msgs = self.budget_reduction()
      if msgs:
        self.reason_rejection = self.env[REJECT_REASON].search([('name', '=', 'Insuficiencia Presupuestal')])
        self.reason_rejection_desc = "\r\n".join(msgs)
        self.state = 'rejected_by_insufficiency'

        reject = self.env['account.topay.rejection'].browse()
        if not self.from_web_service:
          if not self.is_for_ministration:
            reject.email_format(self.state,self.secretary_admin,self.secretary_admin_email,self.reason_rejection,self.reason_rejection_desc)
          else:
            reject.email_format(self.state,self.secretary_admin_info_id.name,self.secretary_admin_email,self.reason_rejection,self.reason_rejection_desc)
      else:
        # Registro de los asientos contables
        self.create_accounting_entries()
        # Actualización del status
        self.state = 'approved'
        # Actualización del disponible de la cuenta
        self.available = sum([l.mx_money for l in self.invoice_line_ids])
    else:
      self.assign_number()
      # Se asigna el número de previo
      self.assign_previous_number()
      # Registro de los asientos contables
      #self.create_accounting_entries()
      # Actualización del status
      self.state = 'approved'
      # Actualización del disponible de la cuenta
      #self.available = sum([l.mx_money for l in self.invoice_line_ids])

  def assign_number(self):
    self.name = self.type_provision.sequence_id.next_by_id()

  #Asignación del numero de previo para los que no son datos del servicio web
  def assign_previous_number(self):
    if not self.from_web_service:
      self.previous_number = self.name.replace('/', '')

  def capitalizacion(self):
    # Busca todas las líneas de egresos con codigo programático
    lines = []
    if self.type_provision.is_capitalized:
      uma = self.env['payment.parameters'].get_param('uma_capitalization')
      # Partidas que capitalizan sin importar el monto de la UMA
      items_cap = self.env['account.to.pay.parameters'].get_param('item_capitalization')
      for line in self.invoice_line_ids.filtered(lambda x: x.program_code_id):
        if line.program_code_id.item_id in items_cap or line.mx_money >= uma:
          # Buscamos la equivalencia que corresponde
          eq = self.env['siif_budget_mgmt_2.countability_equivalence'].search([
            ('origin_resource', '=', line.program_code_id.resource_origin_id.id),
            ('batch', '=', line.program_code_id.item_id.id)
          ])
          if eq:
            lines.append((0, 0, {
              'account_to_pay_id': self.id,
              'account_to_pay_invoice_id': self.id,
              'account_id': eq.account_fixed_asset.id,
              'account_internal_type': 'other',
              'tax_exigible': False,
              'dependency_id': line.dependency_id.id,
              'sub_dependency_id': line.sub_dependency_id.id,
              'quantity': 1,
              'debit': line.mx_money,
              'name': 'Capitalización',
              'program_code_id': line.program_code_id.id
            }))
            lines.append((0, 0, {
              'account_to_pay_id': self.id,
              'account_to_pay_invoice_id': self.id,
              'account_id': eq.account_heritage.id,
              'account_internal_type': 'other',
              'tax_exigible': False,
              'dependency_id': line.dependency_id.id,
              'sub_dependency_id': line.sub_dependency_id.id,
              'quantity': 1,
              'credit': line.mx_money,
              'name': 'Capitalización',
              'program_code_id': line.program_code_id.id
            }))
    return lines

  def get_account_lines(self, revert=False):
    lines = []
    journal = self.type_provision.journal_id
    cri = False
    for line in self.invoice_line_ids:
      amount = line.mx_money
      if revert:
        amount = round(line.mx_money - line.amount_used, 4)
        if amount <= 0:
          continue
      resource_origin = line.origin_resource_sel.key_origin
      # Linea de Presupuesto Asignado
      if resource_origin == '00':
        # Gastos de Operación
        lines.append((0, 0, {
          'account_id': line.program_code_id.item_id.unam_account_id and line.program_code_id.item_id.unam_account_id.id or False,
          'coa_conac_id': line.program_code_id.item_id.cog_id and line.program_code_id.item_id.cog_id.id or False,
          'debit': amount,
          'conac_move' : True,
          'partner_id': self.partner_id.id,
          'dependency_id': line.program_code_id.dependency_id.id if line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
          'sub_dependency_id': line.program_code_id.sub_dependency_id.id if line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
          'program_code_id': line.program_code_id.id,
          'account_to_pay_id': self.id,
          'account_to_pay_invoice_id': self.id,
        }))
        # Cuenta por pagar
        lines.append((0, 0, {
          'account_id': self.type_provision.account_for_balance_on_account_payable_id and self.type_provision.account_for_balance_on_account_payable_id.id or False,
          'coa_conac_id': False,
          'credit': amount,
          'conac_move' : False,
          'partner_id': self.partner_id.id,
          'dependency_id':line.program_code_id.dependency_id.id if self.type_provision.account_for_balance_on_account_payable_id.dep_subdep_flag else False,
          'sub_dependency_id':line.program_code_id.sub_dependency_id.id if self.type_provision.account_for_balance_on_account_payable_id.dep_subdep_flag else False,
          'program_code_id': line.program_code_id.id,
          'account_to_pay_id': self.id,
          'account_to_pay_invoice_id': self.id,
        }))
        # Cuentas Presupuestales
        lines.append((0, 0, {
          'account_id': journal.default_credit_account_id and journal.default_credit_account_id.id or False,
          'coa_conac_id': journal.conac_credit_account_id and journal.conac_credit_account_id.id or False,
          'credit': amount,
          'conac_move' : True,
          'partner_id': self.partner_id.id,
          'dependency_id':line.program_code_id.dependency_id.id if line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
          'sub_dependency_id':line.program_code_id.sub_dependency_id.id if line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
          'program_code_id': line.program_code_id.id,
          'account_to_pay_id': self.id,
        }))
        lines.append((0, 0, {
          'account_id': journal.default_debit_account_id and journal.default_debit_account_id.id or False,
          'coa_conac_id': journal.conac_debit_account_id and journal.conac_debit_account_id.id or False,
          'debit': amount,
          'conac_move' : True,
          'partner_id': self.partner_id.id,
          'dependency_id':line.program_code_id.dependency_id.id if line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
          'sub_dependency_id':line.program_code_id.sub_dependency_id.id if line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
          'program_code_id': line.program_code_id.id,
          'account_to_pay_id': self.id,
        }))
        lines.append((0, 0, {
          'account_id': journal.accured_credit_account_id and journal.accured_credit_account_id.id or False,
          'coa_conac_id': journal.conac_accured_credit_account_id and journal.conac_accured_credit_account_id.id or False,
          'credit': amount,
          'conac_move' : True,
          'partner_id': self.partner_id.id,
          'dependency_id':line.program_code_id.dependency_id.id if line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
          'sub_dependency_id':line.program_code_id.sub_dependency_id.id if line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
          'program_code_id': line.program_code_id.id,
          'account_to_pay_id': self.id,
        }))
        lines.append((0, 0, {
          'account_id': journal.accured_debit_account_id and journal.accured_debit_account_id.id or False,
          'coa_conac_id': journal.conac_accured_debit_account_id and journal.conac_accured_debit_account_id.id or False,
          'debit': amount,
          'conac_move' : True,
          'partner_id': self.partner_id.id,
          'dependency_id':line.program_code_id.dependency_id.id if line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
          'sub_dependency_id':line.program_code_id.sub_dependency_id.id if line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
          'program_code_id': line.program_code_id.id,
          'account_to_pay_id': self.id,
        }))
        lines.append((0, 0, {
          'account_id': journal.execercise_credit_account_id and journal.execercise_credit_account_id.id or False,
          'coa_conac_id': journal.conac_exe_credit_account_id and journal.conac_exe_credit_account_id.id or False,
          'credit': amount,
          'conac_move' : True,
          'partner_id': self.partner_id.id,
          'dependency_id':line.program_code_id.dependency_id.id if line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
          'sub_dependency_id':line.program_code_id.sub_dependency_id.id if line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
          'program_code_id': line.program_code_id.id,
          'account_to_pay_id': self.id,
        }))
        lines.append((0, 0, {
          'account_id': journal.execercise_debit_account_id and journal.execercise_debit_account_id.id or False,
          'coa_conac_id': journal.conac_exe_debit_account_id and journal.conac_exe_debit_account_id.id or False,
          'debit': amount,
          'conac_move' : True,
          'partner_id': self.partner_id.id,
          'dependency_id':line.program_code_id.dependency_id.id if line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
          'sub_dependency_id':line.program_code_id.sub_dependency_id.id if line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
          'program_code_id': line.program_code_id.id,
          'account_to_pay_id': self.id,
        }))
      # Línea de Ingresos Extraordinarios
      elif resource_origin == '01':
        # Gastos de Operación
        lines.append((0, 0, {
          'account_id': line.program_code_id.item_id.unam_account_id and line.program_code_id.item_id.unam_account_id.id or False,
          'coa_conac_id': line.program_code_id.item_id.cog_id and line.program_code_id.item_id.cog_id.id or False,
          'debit': amount,
          'conac_move' : True,
          'partner_id': self.partner_id.id,
          'dependency_id': line.program_code_id.dependency_id.id if line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
          'sub_dependency_id': line.program_code_id.sub_dependency_id.id if line.program_code_id.item_id.unam_account_id.dep_subdep_flag else False,
          'program_code_id': line.program_code_id.id,
          'account_to_pay_id': self.id,
          'account_to_pay_invoice_id': self.id,
        }))
        # Cuenta por pagar
        lines.append((0, 0, {
          'account_id': self.type_provision.account_for_balance_on_account_payable_id and self.type_provision.account_for_balance_on_account_payable_id.id or False,
          'coa_conac_id': False,
          'credit': amount,
          'conac_move' : False,
          'partner_id': self.partner_id.id,
          'dependency_id': line.program_code_id.dependency_id.id if self.type_provision.account_for_balance_on_account_payable_id.dep_subdep_flag else False,
          'sub_dependency_id': line.program_code_id.sub_dependency_id.id if self.type_provision.account_for_balance_on_account_payable_id.dep_subdep_flag else False,
          'program_code_id': line.program_code_id.id,
          'account_to_pay_id': self.id,
          'account_to_pay_invoice_id': self.id,
        }))
        account_ie = line.account_ie.ie_account_line_ids.filtered(lambda x:x.main_account == True).account_id
        # Pasivo Diferido
        # Busqueda de CRI
        account_code_debit = account_ie.id
        account_code_credit = account_ie.revenue_recognition_account_id.id

        cri = self.env['affectation.income.budget'].search([]).associate_cri_account(account_code_debit,account_code_credit)
        
        lines.append((0, 0, {
          # cuentas 400
          'account_id': account_ie and account_ie.id or False,
          'coa_conac_id': False,
          'debit': amount,
          'conac_move' : False,
          'partner_id': self.partner_id.id,
          'dependency_id': line.program_code_id.dependency_id.id if  account_ie.dep_subdep_flag else False,
          'sub_dependency_id': line.program_code_id.sub_dependency_id.id if  account_ie.dep_subdep_flag else False,
          'account_to_pay_id': self.id,
        }))
        # Ingresos
        lines.append((0, 0, {
          # cuentas 400
          'account_id': account_ie.revenue_recognition_account_id and account_ie.revenue_recognition_account_id.id or False,
          'coa_conac_id': False,
          'credit': amount,
          'conac_move' : False,
          'partner_id': self.partner_id.id,
          'dependency_id':line.program_code_id.dependency_id.id if account_ie.revenue_recognition_account_id.dep_subdep_flag else False,
          'sub_dependency_id':line.program_code_id.sub_dependency_id.id if account_ie.revenue_recognition_account_id.dep_subdep_flag else False,
          'account_to_pay_id': self.id,
          'cri' : cri.name if cri else False
        }))
        # Cuentas de Ingresos
        lines.append((0, 0, {
          # cuentas 800
          'account_id': journal.accrued_income_debit_account_id and journal.accrued_income_debit_account_id.id or False,
          'coa_conac_id': journal.conac_accrued_income_debit_account_id and journal.conac_accrued_income_debit_account_id.id or False,
          'debit': amount,
          'conac_move' : True,
          'partner_id': self.partner_id.id,
          'dependency_id':line.program_code_id.dependency_id.id if journal.accrued_income_debit_account_id.dep_subdep_flag else False,
          'sub_dependency_id':line.program_code_id.sub_dependency_id.id if journal.accrued_income_debit_account_id.dep_subdep_flag else False,
          'account_to_pay_id': self.id,
          'cri' : cri.name if cri else False
        }))
        lines.append((0, 0, {
          # cuentas 800
          'account_id': journal.accrued_income_credit_account_id and journal.accrued_income_credit_account_id.id or False,
          'coa_conac_id': journal.conac_accrued_income_credit_account_id and journal.conac_accrued_income_credit_account_id.id or False,
          'credit': amount,
          'conac_move' : True,
          'partner_id': self.partner_id.id,
          'dependency_id':line.program_code_id.dependency_id.id if journal.accrued_income_credit_account_id.dep_subdep_flag else False,
          'sub_dependency_id':line.program_code_id.sub_dependency_id.id if journal.accrued_income_credit_account_id.dep_subdep_flag else False,
          'account_to_pay_id': self.id,
          'cri' : cri.name if cri else False
        }))
        lines.append((0, 0, {
          # cuentas 800
          'account_id': journal.recover_income_debit_account_id and journal.recover_income_debit_account_id.id or False,
          'coa_conac_id': journal.conac_recover_income_debit_account_id and journal.conac_recover_income_debit_account_id.id or False,
          'debit': amount,
          'conac_move' : True,
          'partner_id': self.partner_id.id,
          'dependency_id':line.program_code_id.dependency_id.id if journal.recover_income_debit_account_id.dep_subdep_flag else False,
          'sub_dependency_id':line.program_code_id.sub_dependency_id.id if journal.recover_income_debit_account_id.dep_subdep_flag else False,
          'account_to_pay_id': self.id,
          'cri' : cri.name if cri else False
        }))
        lines.append((0, 0, {
          # cuentas 800
          'account_id': journal.recover_income_credit_account_id and journal.recover_income_credit_account_id.id or False,
          'coa_conac_id': journal.conac_recover_income_credit_account_id and journal.conac_recover_income_credit_account_id.id or False,
          'credit': amount,
          'conac_move' : True,
          'partner_id': self.partner_id.id,
          'dependency_id':line.program_code_id.dependency_id.id if journal.recover_income_credit_account_id.dep_subdep_flag else False,
          'sub_dependency_id':line.program_code_id.sub_dependency_id.id if journal.recover_income_credit_account_id.dep_subdep_flag else False,
          'account_to_pay_id': self.id,
          'cri' : cri.name if cri else False
        }))
        # Cuentas Presupuestales
        lines.append((0, 0, {
          'account_id': journal.default_credit_account_id and journal.default_credit_account_id.id or False,
          'coa_conac_id': journal.conac_credit_account_id and journal.conac_credit_account_id.id or False,
          'credit': amount,
          'conac_move' : True,
          'partner_id': self.partner_id.id,
          'dependency_id':line.program_code_id.dependency_id.id if journal.default_credit_account_id.dep_subdep_flag else False,
          'sub_dependency_id':line.program_code_id.sub_dependency_id.id if journal.default_credit_account_id.dep_subdep_flag else False,
          'program_code_id': line.program_code_id.id,
          'account_to_pay_id': self.id,
        }))
        lines.append((0, 0, {
          'account_id': journal.default_debit_account_id and journal.default_debit_account_id.id or False,
          'coa_conac_id': journal.conac_debit_account_id and journal.conac_debit_account_id.id or False,
          'debit': amount,
          'conac_move' : True,
          'partner_id': self.partner_id.id,
          'dependency_id':line.program_code_id.dependency_id.id if journal.default_debit_account_id.dep_subdep_flag else False,
          'sub_dependency_id':line.program_code_id.sub_dependency_id.id if journal.default_debit_account_id.dep_subdep_flag else False,
          'program_code_id': line.program_code_id.id,
          'account_to_pay_id': self.id,
        }))
        lines.append((0, 0, {
          'account_id': journal.accured_credit_account_id and journal.accured_credit_account_id.id or False,
          'coa_conac_id': journal.conac_accured_credit_account_id and journal.conac_accured_credit_account_id.id or False,
          'credit': amount,
          'conac_move' : True,
          'partner_id': self.partner_id.id,
          'dependency_id':line.program_code_id.dependency_id.id if journal.accured_credit_account_id.dep_subdep_flag else False,
          'sub_dependency_id':line.program_code_id.sub_dependency_id.id if journal.accured_credit_account_id.dep_subdep_flag else False,
          'program_code_id': line.program_code_id.id,
          'account_to_pay_id': self.id,
        }))
        lines.append((0, 0, {
          'account_id': journal.accured_debit_account_id and journal.accured_debit_account_id.id or False,
          'coa_conac_id': journal.conac_accured_debit_account_id and journal.conac_accured_debit_account_id.id or False,
          'debit': amount,
          'conac_move' : True,
          'partner_id': self.partner_id.id,
          'dependency_id':line.program_code_id.dependency_id.id if journal.accured_debit_account_id.dep_subdep_flag else False,
          'sub_dependency_id':line.program_code_id.sub_dependency_id.id if journal.accured_debit_account_id.dep_subdep_flag else False,
          'program_code_id': line.program_code_id.id,
          'account_to_pay_id': self.id,
        }))
        lines.append((0, 0, {
          'account_id': journal.execercise_credit_account_id and journal.execercise_credit_account_id.id or False,
          'coa_conac_id': journal.conac_exe_credit_account_id and journal.conac_exe_credit_account_id.id or False,
          'credit': amount,
          'conac_move' : True,
          'partner_id': self.partner_id.id,
          'dependency_id':line.program_code_id.dependency_id.id if journal.execercise_credit_account_id.dep_subdep_flag else False,
          'sub_dependency_id':line.program_code_id.sub_dependency_id.id if journal.execercise_credit_account_id.dep_subdep_flag else False,
          'program_code_id': line.program_code_id.id,
          'account_to_pay_id': self.id,
        }))
        lines.append((0, 0, {
          'account_id': journal.execercise_debit_account_id and journal.execercise_debit_account_id.id or False,
          'coa_conac_id': journal.conac_exe_debit_account_id and journal.conac_exe_debit_account_id.id or False,
          'debit': amount,
          'conac_move' : True,
          'partner_id': self.partner_id.id,
          'dependency_id':line.program_code_id.dependency_id.id if journal.execercise_debit_account_id.dep_subdep_flag else False,
          'sub_dependency_id':line.program_code_id.sub_dependency_id.id if journal.execercise_debit_account_id.dep_subdep_flag else False,
          'program_code_id': line.program_code_id.id,
          'account_to_pay_id': self.id,
        }))
      # Linea de Cuenta Contable
      elif resource_origin == '05':
        # Gastos de Operación
        lines.append((0, 0, {
          'account_id': line.account_id and line.account_id.id or False,
          'coa_conac_id': False,
          'debit': amount,
          'conac_move' : False,
          'partner_id': self.partner_id.id,
          'dependency_id': line.dependency_id.id if line.account_id.dep_subdep_flag else False,
          'sub_dependency_id': line.sub_dependency_id.id if line.account_id.dep_subdep_flag else False,
          'account_to_pay_id': self.id,
          'account_to_pay_invoice_id': self.id,
        }))
        # Cuenta por pagar
        lines.append((0, 0, {
          'account_id': self.type_provision.account_for_balance_on_account_payable_id and self.type_provision.account_for_balance_on_account_payable_id.id or False,
          'coa_conac_id': False,
          'credit': amount,
          'conac_move' : False,
          'partner_id': self.partner_id.id,
          'dependency_id': line.dependency_id.id if self.type_provision.account_for_balance_on_account_payable_id.dep_subdep_flag else False,
          'sub_dependency_id': line.sub_dependency_id.id if self.type_provision.account_for_balance_on_account_payable_id.dep_subdep_flag else False,
          'account_to_pay_id': self.id,
          'account_to_pay_invoice_id': self.id,
        }))
      if cri:
        date_affectation = ''
        if self.from_web_service:
          date_affectation = self.register_date
        else:
          date_affectation = datetime.now()
        # Objetos
        obj_affetation = self.env['affectation.income.budget']
        obj_logbook = self.env['income.adequacies.function']
        obj_fiscal_year = self.env['account.fiscal.year']
        # Afectación al presupuesto
        type_affectation = 'eje-rec'
        date_accrued = False
        # Mandar datos a la clase de afectación
        cri_id = cri.id
        
        obj_affetation.search([]).affectation_income_budget(type_affectation,cri_id,date_accrued,date_affectation,amount)
        # Guarda la bitacora de la adecuación
        # Busca el id del año fiscal
        year = date_affectation.year
        fiscal_year_id = obj_fiscal_year.search([('name', '=', year)])
        type_form = 'income'
        movement = 'for_exercising_collected'
        origin_type = 'income-cpp'
        name_id = self.name
        journal_id = journal.id
        update_date_record = datetime.today()
        update_date_income = datetime.today()
        obj_logbook.search([]).logbook_income_budget(fiscal_year_id.id,cri_id,date_affectation,type_form,movement,origin_type,name_id,journal_id,update_date_record,amount,update_date_income) 
    if revert:
      for line in lines:
        debit = line[2].get('debit', 0)
        credit = line[2].get('credit', 0)
        line[2]['debit'] = credit
        line[2]['credit'] = debit
    return lines

  def create_accounting_entries(self):
    journal = self.type_provision.journal_id
    if not journal:
      raise ValidationError(_("It didn't stablishe a journal on the Account to Pay Type Settings."))
    if not self.type_provision.account_for_balance_on_account_payable_id:
      raise ValidationError(_("It didn't stablishe an account on the Account to Pay Type Settings."))
    lines = self.get_account_lines()
    for line in lines:
      line[2].update({'name': 'Creación de la Cuenta por Pagar %s' % self.previous_number})
    # Capitalización
    lines += self.capitalizacion()
    if self.from_web_service:
      unam_move_val = {
        'ref': self.display_name,
        'conac_move': True,
        'dependancy_id' : self.dependency_id and self.dependency_id.id or False,
        'sub_dependancy_id': self.sub_dependency_id and self.sub_dependency_id.id or False,
        'date': self.register_date,
        'journal_id': journal.id,
        'company_id': self.env.user.company_id.id,
        'line_ids': lines
      }
    else:
      unam_move_val = {
        'ref': self.display_name,
        'conac_move': True,
        'dependancy_id' : self.dependency_id and self.dependency_id.id or False,
        'sub_dependancy_id': self.sub_dependency_id and self.sub_dependency_id.id or False,
        'date': datetime.now(),
        'journal_id': journal.id,
        'company_id': self.env.user.company_id.id,
        'line_ids': lines
      }
    # Creación de los apuntes contables
    move_obj = self.env[ACC_MOVE]
    unam_move = move_obj.create(unam_move_val).with_context(check_move_validity=False)
    unam_move.action_post()

  def budget_reduction(self):
    # Se reduce la disponiblidad de todos los códigos programáticos registrados
    move = self.env[ACC_MOVE]
    quarter = self._get_current_quarter()
    lines_pa = []
    lines_ie = []
    lines_cc = []
    for line in self.invoice_line_ids:
      amount = line.mx_money
      resource_origin = line.origin_resource_sel.key_origin
      # Línea de Presupuesto Asignado
      if resource_origin == '00':
        lines_pa.append((line.program_code_id, amount))
      # Línea de Ingresos Extraordinarios
      elif resource_origin == '01':
        lines_ie.append((line.account_ie, line.program_code_id, amount))
      # Línea de Ingresos Extraordinarios
      elif resource_origin == '05':
        lines_cc.append((line.account_id, line.dependency_id, line.sub_dependency_id, amount))
    # Validación de la disponibilidad por cuenta contable
    res = move.validate_available_account(lines_cc)
    if res:
      return res
    # Disminución del disponible de códigos programáticos de PA
    ok, res = move.update_available_program_code_pa(lines_pa, quarter)
    if not ok:
      return res
    else:
      # Notificación del mensaje de sobregiro
      if res:
        self.message_post(body=res)
    # Disminución del disponible de códigos programáticos de IE
    ok, res = move.update_available_program_code_ie(lines_ie, quarter, self.invoice_date, cxp_id=self)
    if not ok:
      self.env.cr.rollback()
      return res
    return []

  #Función para llamar ventana emergente de la validación de documentación
  def validate_documentation(self):
    return {
      'name': _('Validación de Documentación'),
      'type': IR_WINDOW,
      'res_model': 'account.topay.documents',
      'view_mode': 'form',
      'view_type': 'form',
      'context': {'current_account_topay_id': self.id},
      'views': [(False, 'form')],
      'target': 'new'
    }

  #Rechazar provisión
  #Uso para Opción de Rechazo general
  def action_rejected(self):
    self.rejection("rejected")

  def rejection(self, state):
    self.state = 'draft'
    return {
      'name': _('Reject Motivos'),
      'type': IR_WINDOW,
      'res_model': 'account.topay.rejection',
      'view_mode': 'form',
      'view_type': 'form',
      'views': [(False, 'form')],
      'context': {'account_topay_id': self.id,'next_state':state},
      'target': 'new'
    }

  def action_draft(self):
    self.state = 'draft'

  def _get_line(self, program_code_id, dependency_id, sub_dependency_id):
    lines = []
    # Linea de PA o IE
    if program_code_id:
      lines = self.invoice_line_ids.filtered(lambda x:x.program_code_id == program_code_id)
      if not lines:
        raise UserError(_("The program code is not found in the Payable Account."))
    # Línea de CC
    else:
      if not (self.dependency_id == dependency_id and self.sub_dependency_id == sub_dependency_id):
        raise UserError(_("The dependency/sub-dependency does not match that of the accounts payable."))
      lines = self.invoice_line_ids.filtered(lambda x:x.origin_resource_sel.key_origin == '05')
      if not lines:
        raise UserError(_("Account Payable has no ledger account lines."))
    # Se busca la primer línea con disponibilidad
    for line in lines:
      if (line.mx_money - line.amount_used) > 0:
        return line
    # Si no encontro ninguna, se devuelve la primera
    return line[0]

      # # Se busca que alguna linea haga match con los valores de dep/sd
      # tmp_lines = []
      # for line in lines:
      #     if line.account_id.dep_subdep_flag:
      #         if line.dependency_id == dependency_id and line.sub_dependency_id == sub_dependency_id:
      #             tmp_lines.append(line)
      # if not tmp_lines:
      #     # Si ninguna hizo match, se devulve la primer línea que no considere
      #     # a la dep/sd
      #     for line in lines:
      #         if not line.account_id.dep_subdep_flag:
      #             tmp_lines.append(line)
      # # Llegado a este punto no se encontró ninguna línea que coincida
      # if not tmp_lines:
      #     raise ValidationError("No se encontró una cuenta contable que cumpla con el criterio Dependencia/Subdependencia.")
      # for line in tmp_lines:
      #     if (line.mx_money - line.amount_used) > 0:
      #         return line
      # # Si no encontro ninguna, se devuelve la primera
      # return tmp_lines[0]

  def get_available_pa(self, lines):
    msgs = []
    for program_code_id, amount in lines:
      lines_pa = self.invoice_line_ids.filtered(lambda x, program_code_id=program_code_id :x.program_code_id == program_code_id)
      msgs += self._get_available(lines_pa, amount)
    return msgs

  def get_available_ie(self, lines):
    msgs = []
    for (program_code_id, account_ie), amount in lines:
      lines_ie = self.invoice_line_ids.filtered(
        lambda x, program_code_id=program_code_id, account_ie=account_ie:
          x.program_code_id == program_code_id and x.account_ie == account_ie
      )
      msgs += self._get_available(lines_ie, amount)
    return msgs

  def get_available_cc(self, lines):
    msgs = []
    for (account_id, dependency_id, sub_dependency_id), amount in lines:
      lines_cc = self.invoice_line_ids.filtered(
        lambda x, account_id=account_id, dependency_id=dependency_id, sub_dependency_id=sub_dependency_id:
        x.account_id == account_id and
        x.dependency_id == dependency_id and
        x.sub_dependency_id == sub_dependency_id
      )
      msgs += self._get_available(lines_cc, amount)
    return msgs

  def _get_available(self, lines, amount):
    move = self.env[ACC_MOVE]
    msg = []
    available = sum([l.mx_money - l.amount_used for l in lines])
    if available < amount:
      msg.append(move._get_insufficiency_message(
        "la cuenta por pagar {0}".format(self.previous_number),
        available,
        amount
      ))
    return msg

  def update_available_pa(self, lines, move_id):
    msgs = []
    for program_code_id, amount in lines:
      lines_pa = self.invoice_line_ids.filtered(lambda x, program_code_id=program_code_id :x.program_code_id == program_code_id)
      msgs += self._update_available(lines_pa, amount, move_id)
    return msgs

  def update_available_ie(self, lines, move_id):
    msgs = []
    for (program_code_id, account_ie), amount in lines:
      lines_ie = self.invoice_line_ids.filtered(
        lambda x, program_code_id=program_code_id, account_ie=account_ie:
          x.program_code_id == program_code_id and x.account_ie == account_ie
      )
      msgs += self._update_available(lines_ie, amount, move_id)
    return msgs

  def update_available_cc(self, lines, move_id):
    msgs = []
    for (account_id, dependency_id, sub_dependency_id), amount in lines:
      lines_cc = self.invoice_line_ids.filtered(
        lambda x, account_id=account_id, dependency_id=dependency_id, sub_dependency_id=sub_dependency_id:
        x.account_id == account_id and
        x.dependency_id == dependency_id and
        x.sub_dependency_id == sub_dependency_id
      )
      msgs += self._update_available(lines_cc, amount, move_id)
    return msgs


  def _update_available(self, lines, amount, move_id):

    def get_link_line(line, amount, move_id):
      return (0, 0, {
        'account_to_pay_line_id': line.id,
        'account_move_id': move_id.id,
        'amount': amount
      })

    move = self.env[ACC_MOVE]
    # Se obtiene el disponible de las lineas
    available = sum([l.mx_money - l.amount_used for l in lines])
    # Si no hay disponible se regresa un mensaje con los errores
    if available < amount:
      return [move._get_insufficiency_message(
        "la cuenta por pagar {0}".format(self.previous_number),
        available,
        amount
      )]
    # Si hay disponible, se incrementa el monto usado de la línea
    else:
      total_amount = amount
      account_to_pay_links = []
      for line in lines:
        available_line = round(line.mx_money - line.amount_used, 4)
        if available_line >= amount:
          line.amount_used = round(line.amount_used + amount, 4)
          account_to_pay_links.append(get_link_line(line, amount, move_id))
          amount = 0
          break
        else:
          line.amount_used = line.mx_money
          amount -= available_line
          account_to_pay_links.append(get_link_line(line, available_line, move_id))
      # Se actualiza el monto total de la cuenta por pagar
      if amount == 0:
        self.available = round(self.available - total_amount, 4)
        move_id.account_to_pay_link_ids = account_to_pay_links
        self.link_payment_request(move_id.id)
      return []

  # get_available Obtiene el disponible de una linea
  def get_available(self, program_code_id = None,
    dependency_id = None, sub_dependency_id = None
  ):
    line = self._get_line(program_code_id, dependency_id, sub_dependency_id)
    return line.mx_money - line.amount_used

  # update_available Actualiza el disponible de una línea
  def update_available(self, amount, program_code_id = None,
    dependency_id = None, sub_dependency_id = None
  ):
    amount=round(amount,4)
    line = self._get_line(program_code_id, dependency_id, sub_dependency_id)
    available = round(line.mx_money-line.amount_used,4)
    if available >= amount:
      line.amount_used = round(line.amount_used+amount,4)
      self.available = round(self.available - amount, 4)
    else:
      raise ValidationError(
        "La cuenta por pagar con previo %s tiene un disponible de: $%s y se requiere de: $%s"
        % (self.previous_number, format(available, ",.2f"), format(amount, ",.2f"))
      )
    return line

  # update_available Actualiza el disponible de una línea
  def return_available(self, move_id):
    account_to_pay = Dictionary()
    # Actualización del monto utilizado de cada linea
    for line in move_id.account_to_pay_link_ids:
      line.account_to_pay_line_id.amount_used -= line.amount
      account_to_pay.add_sum(line.account_to_pay_line_id.account_topay_id, line.amount)
    # Actualización del monto total del disponible de la cuenta por pagar
    for account_to_pay_id, amount in account_to_pay.items():
      available = account_to_pay_id.available
      account_to_pay_id.available = round(available + amount, 4)
      account_to_pay_id.unlink_payment_request(move_id.id)

  # update_available Actualiza el disponible de una línea
  def link_payment_request(self, payment_request_id):
    if payment_request_id not in self.payment_request_ids.ids:
      self.payment_request_ids = [(4, payment_request_id)]

  # update_available Actualiza el disponible de una línea
  def unlink_payment_request(self, payment_request_id):
    self.payment_request_ids = [(3, payment_request_id)]

  def show_payment_request(self):
    action = self.env.ref('siif_account_to_pay.supplier_payment_action').read()[0]
    action['limit'] = 100
    action['domain'] = [('id', 'in', self.payment_request_ids.ids)]
    action['search_view_id'] = (self.env.ref('jt_supplier_payment.payment_req_tree_view').id,)
    return action

  #Función para asignar visibilidad al boton Editar del formulario.
  #Solamente tendrá el acceso al botón Editar en el estado de borrador
  @api.depends('state')
  def _compute_css(self):
     #- Método que modifica el css de la vista en Certificados de deposito
    for record in self:
      record.css_structure = ""
      if record.state not in ('draft'):
        record.css_structure = """
          <style>
            .o_form_view .o_form_statusbar > .o_statusbar_status{
            max-width:85%;
            }
            .o_form_button_edit {
              display: none !important;
            }

          </style>
        """
      # En cualquiera de los estados, se limita el tamaño del statusbar para evitar tener 
      else:
        record.css_structure += """
          <style>
            .o_form_view .o_form_statusbar > .o_statusbar_status{
              max-width:85%;
            }
          </style>
        """

  #Función para revisar la cantidad de dinero para las ministraciones
  def _check_ministration_info(self,rec):
    if rec.is_for_ministration:
      suma=0
      for line in rec.invoice_line_ids:
        suma=suma+line.mx_money
      ministration = self.env['money.funds.request.ministration'].search([('account_topay_id','=',rec.id)])
      if ministration.amount_request!=suma:
        raise UserError('El dinero asignado de la cuenta por pagar, siendo de $'+str(suma)+', no coincide con el monto de la ministración '+str(ministration.name)+',\
                         el cual es de $'+str(ministration.amount_request)+'. Se necesita corregir la cuenta por pagar.')
      else:
        rec.state = 'published'
  
  #FUnción para importar lineas
  def import_lines(self):
    return self.env['import.account.to.pay.lines'].open_wizard(
      default_type_provision=self.type_provision.id,
      default_account_to_pay_id=self.id,
    )

  #Función para devolver el presupuesto, en caso de devolución o cancelación
  def _return_budget(self):
    lines = Dictionary()
    for line in self.invoice_line_ids.filtered(lambda l: l.program_code_id):
      # Monto a devolver
      amount = round(line.mx_money - line.amount_used, 4)
      lines.add_sum(line.program_code_id.id, amount)
    # Devuelve el presupuesto disponible de la cuenta por pagar
    self.env['expenditure.budget.line'].return_available(lines.items())

  #Invertir lineas de las cuentas para la cencelación
  def _revert_account_lines(self):
    lines = self.get_account_lines(revert=True)
    for line in lines:
      line[2].update({'name': 'Cancelación la Cuenta por Pagar %s' % self.previous_number})
    # Se obtiene la poliza de los apuntes contables (Nueva Poliza)
    unam_move_val = {
        'ref': self.display_name,
        'conac_move': True,
        'dependancy_id' : self.dependency_id and self.dependency_id.id or False,
        'sub_dependancy_id': self.sub_dependency_id and self.sub_dependency_id.id or False,
        'date': datetime.now(),
        'journal_id': self.type_provision.journal_id.id,
        'company_id': self.env.user.company_id.id,
        'line_ids': lines
      }
    # Creación de los apuntes contables
    unam_move = self.env[ACC_MOVE].create(unam_move_val).with_context(check_move_validity=False)
    unam_move.action_post()
    # Obtener lineas para la cuenta por pagar
    move_line = self.env['account.move.line'].search([('account_to_pay_id', '=', self.id)], limit=1)
    move_id = move_line.move_id
    # Se agregan las lineas de cancelación
    move_id.line_ids = move_id.line_ids + unam_move.line_ids
    

  #Función para la cancelación de cuenta por pagar.
  def cancel_used_account(self):
    # if self.state != 'approved':
    #   raise UserError("Solo se pueden cancelar cuentas por pagar que se encuentren aprobadas.")
    # Devuelve el presupuesto disponible de la cuenta por pagar
    self._return_budget()
    # Revierte los apuntes contables con el monto aún disponibles
    self._revert_account_lines()
    self.available = 0
    self.state = 'canceled'