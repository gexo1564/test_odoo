import io
from ..layouts import Layout, ResultLine
import logging

class LayoutBanamex(Layout):

    def load_file(self, file_data):
        self.file = io.StringIO(file_data.decode("ISO-8859-1"))

    def load_body(self):
        return self.file.readlines()[2:-1]

    def close_file(self):
        self.file.close()

    def read_line(self, line):
        concepts = {
            "PAGO DE NOMINA QNA": "nomina",
            "PAGO  PENSION  QNA": "pension",
        }
        payment_type = concepts.get(line[180:198], "unknown")
        amount = float("%s.%s" % (line[5:22], line[22:24]))
        bank_account = line[25:45]
        beneficiary = line[85:140]
        status = line[229]
        ok = status == "3"

        logging.critical([ok, payment_type, {'beneficiary': beneficiary, 'bank_account': bank_account, 'amount': amount}, status])
        return ResultLine(ok, payment_type, {'beneficiary': beneficiary, 'bank_account': bank_account, 'amount': amount}, status)

    def get_ids_search_employee_payroll_file(self, payroll_processing_id: int, lines: list):
        payment_method_id = self.env.ref('l10n_mx_edi.payment_method_transferencia').id
        query = """
            select substring(rpad(replace(name, 'Ñ', 'N'), 20, ' '), 1, 20) as name,
                lpad(b.acc_number, 16, '0') account,
                p.id partner_id
            from res_partner_bank b, res_partner p, pension_payment_line l, employee_payroll_file r
            where p.id = b.partner_id
            and l.partner_id = p.id
            and l.payroll_id = r.id
            and r.payroll_processing_id = %s
            and beneficiary_type = 'alimony'
            and l.l10n_mx_edi_payment_method_id = %s
        """
        self.env.cr.execute(query, (payroll_processing_id, payment_method_id))
        result_set = {(r[0], r[1]): r[2:] for r in self.env.cr.fetchall()}
        lines_ok, lines_err = [], []
        for line in lines:
            if line.payment_type in ["pension","nomina"]:
                # Busqueda para Pensión y Nomina
                beneficiary = line.columns.get('beneficiary')
                bank_account = line.columns.get('bank_account')
                res = result_set.get((beneficiary, bank_account), None)
                if res:
                    line.res_id = res[0]
                    lines_ok.append(line)
                else:
                    line.res_id = None
                    line.error_message = "No se encontró un registro para el beneficiario %s con cuenta %s" % (
                        beneficiary,
                        bank_account
                    )
                    lines_err.append(line)
            else:
                line.error_message = "Error de linea de respuesta bancaria."
                lines_err.append(line)
        return lines_ok, lines_err

    def get_ids_search_account_payment(self, batch_folio: str, lines: list):
        # Se obtienen los ids que hacen match con el criterio de búsqueda
        query = """
            select lpad(a.branch_number || a.acc_number, 20, '0') bank_account, p.id, p.amount
            from account_payment p, res_partner_bank a
            where p.payment_bank_account_id = a.id
            and batch_folio = %s
        """
        self.env.cr.execute(query, (batch_folio,))
        result_set = {r[0]: r[1:] for r in self.env.cr.fetchall()}
        lines_ok, lines_err = [], []
        for line in lines:
            beneficiary = line.columns.get('beneficiary')
            res = result_set.get(line.columns.get('bank_account'), None)
            if res:
                if res[1] == line.columns.get('amount'):
                    line.res_id = res[0]
                    lines_ok.append(line)
                else:
                    line.res_id = None
                    line.error_message = f"El monto del pago ({line.columns.get('amount'):,.2f}) del beneficiario {beneficiary} no coincide con el registrado ({res[1]:,.2f})."
                    lines_err.append(line)
            else:
                line.res_id = None
                line.error_message = f"No se encontró un registro para el beneficiario {beneficiary}"
                lines_err.append(line)
        return lines_ok, lines_err