import logging
from odoo import models, fields, api,_
from lxml import etree

class ConfigurationJournals(models.Model):
    _name = 'configuration.journals'
    _description = 'Cuenta para reconocimientos parciales'
    _rec_name = 'journal_id'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    journal_id = fields.Many2one('account.journal', string='Journal')
    group = fields.Selection(
        [
            ('admin', 'Income Administrator'),
            ('coordinator', 'Project Coordinator'),
        ],  "Group"
    )
    is_active = fields.Boolean('Active', default = True)
    finance_user = fields.Boolean('Finance User') 
    coordinator_user = fields.Boolean('Coordinator User')    
    
    @api.model
    def create(self, vals):
        is_group_income_admin_user = self.env.user.has_group('jt_income.group_income_admin_user')

        if is_group_income_admin_user:
            vals['group'] =  'admin'

        res = super(ConfigurationJournals, self).create(vals)

        return res 
  
    def logged_user(self):
        is_group_income_admin_user = self.env.user.has_group('jt_income.group_income_admin_user')
        is_group_income_project_coordinator_user = self.env.user.has_group('jt_income.group_income_project_coordinator_user')
        is_group_income_finance_user = self.env.user.has_group('jt_income.group_income_finance_user')
        views = [(self.env.ref('siif_income.income_recognition_account_configuration_view_tree').id, 'tree'), (self.env.ref('siif_income.income_recognition_account_configuration_view_form').id, 'form')]
        domain = []
       
        if is_group_income_admin_user or is_group_income_finance_user:       
            domain = []
                  
        if is_group_income_project_coordinator_user:
            domain = [('coordinator_user', '=', True)]
      
        return {
            'name': 'Cuentas para reconocimientos parciales',
            "res_model": "configuration.journals",
            "domain": domain,
            "type": "ir.actions.act_window",
            "view_mode": "tree,form",
            "views": views,
        } 

     